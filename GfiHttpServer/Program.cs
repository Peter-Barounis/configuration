﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GFIHTTPServer;
using System.Threading;
using System.Diagnostics;

namespace GFIHttpServer
{

   public class Program
   {
      static void Main(string[] args)
      {
         if (PriorProcess() != null)
         {
            return;
         }

         using (GfiHTTPServer srvr = new GfiHTTPServer())
         {
            srvr.Start();
            while (!srvr.StopServerRequested())
            {
               Thread.Sleep(100);
            }
         }
      }

      public static Process PriorProcess()
      // Returns a System.Diagnostics.Process pointing to
      // a pre-existing process with the same name as the
      // current one, if any; or null if the current process
      // is unique.
      {
         Process curr = Process.GetCurrentProcess();
         Process[] procs = Process.GetProcessesByName(curr.ProcessName);
         foreach (Process p in procs)
         {
            if ((p.Id != curr.Id) &&
                (p.MainModule.FileName == curr.MainModule.FileName))
               return p;
         }
         return null;
      }
   }
}
