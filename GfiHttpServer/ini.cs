﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Security.Permissions;
using System.Collections;
using System.IO;
using GFIHTTPServer;

// Create a New INI file to store or load data
static public class IniFile
{
   public static string path;
   public static Hashtable FbxList = new Hashtable();
   [DllImport("kernel32")]
   private static extern long WritePrivateProfileString(string section,
       string key, string val, string filePath);
   [DllImport("kernel32")]
   private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
       int size, string filePath);

   // INIFile Constructor.
   public static void InitializeIniFile(string INIPath)
   {
      path = INIPath;
   }

   // Write Data to the INI File
   public static void IniWriteValue(string Section, string Key, string Value)
   {
      WritePrivateProfileString(Section, Key, Value, path);
   }

   // Read Data Value From the Ini File
   static public string IniReadValue(string Section, string Key, string defaultValue)
   {
      StringBuilder temp = new StringBuilder(255);
      int i = GetPrivateProfileString(Section, Key, defaultValue, temp, 255, path);
      return temp.ToString();
   }

   public static void ReadXmlFile(string xmlFile)
   {
      int fbxid = 0;
      bool KeywordFound = false;
      string KeywordString = "";
      string FbxString = "";
      string LastSection = "";
      FbxList.Clear();
      XmlTextReader reader = new XmlTextReader(xmlFile);

      try
      {
         while (reader.Read())
         {
            switch (reader.NodeType)
            {
               case XmlNodeType.Element: // The node is an element.
                  if (reader.Name.ToUpper().StartsWith("FBX") == true)
                     FbxString = "";
                  LastSection = reader.Name.ToUpper();
                  KeywordFound = false;
                  break;
               case XmlNodeType.Text: //Display the text in each element.
                  KeywordFound = true;
                  KeywordString += LastSection + "=" + reader.Value.ToString() + "\n";
                  if (LastSection.ToUpper().CompareTo("ID") == 0)  // Get farebox id
                     fbxid = Convert.ToInt32(reader.Value);
                  break;
               case XmlNodeType.EndElement: //Display the end of the element.
                  if (reader.Name.ToUpper().StartsWith("FBX") == true)
                  {
                     if (FbxList[fbxid] != null) // Check if index already used
                     {
                        Logger.LogEntry("Error: Farebox " + fbxid + " already in use.");
                     }
                     else
                        FbxList.Add(fbxid, FbxString);
                     fbxid = 0;
                  }
                  else
                  {
                     if (!KeywordFound)
                     {
                        KeywordString = "[" + reader.Name.ToUpper() + "]\n" + KeywordString;
                        FbxString += KeywordString;
                        KeywordString = "";
                     }
                  }
                  KeywordFound = false;
                  break;
            }
         }
      }
      catch
      {
         Logger.LogEntry("Error reading file " + xmlFile);
      }
      reader.Close();
   }

   // Get ini string for specified farebox id
   public static string GetFbxConfig(int fbxNumber)
   {
      string FbxIniStr;
      try
      {
         if ((FbxIniStr = FbxList[fbxNumber].ToString()) == null) // Check if index already used
            FbxIniStr = string.Format("Error: Missing INI file for Farebox {0}.", fbxNumber);
      }
      catch (Exception ex)
      {
         FbxIniStr = null;
      }
      return FbxIniStr;
   }
}


