﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Microsoft.Win32;
using CSCheckOSBitness;
namespace GFIHTTPServer
{
   public class GfiHTTPServer 
   {
      public int _GFIPort;
      public int _GFIMaxConnections;
      private string _GFIDefaultFolder;
      bool StopServer = false;

      private static IDictionary<string, string> _mimeTypeMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) 
      {
        #region extension to MIME type list
        {".asf", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".avi", "video/x-msvideo"},
        {".bin", "application/octet-stream"},
        {".cco", "application/x-cocoa"},
        {".crt", "application/x-x509-ca-cert"},
        {".css", "text/css"},
        {".deb", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dll", "application/octet-stream"},
        {".dmg", "application/octet-stream"},
        {".ear", "application/java-archive"},
        {".eot", "application/octet-stream"},
        {".exe", "application/octet-stream"},
        {".flv", "video/x-flv"},
        {".gif", "image/gif"},
        {".hqx", "application/mac-binhex40"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".ico", "image/x-icon"},
        {".img", "application/octet-stream"},
        {".iso", "application/octet-stream"},
        {".jar", "application/java-archive"},
        {".jardiff", "application/x-java-archive-diff"},
        {".jng", "image/x-jng"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".mml", "text/mathml"},
        {".mng", "video/x-mng"},
        {".mov", "video/quicktime"},
        {".mp3", "audio/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpg", "video/mpeg"},
        {".msi", "application/octet-stream"},
        {".msm", "application/octet-stream"},
        {".msp", "application/octet-stream"},
        {".pdb", "application/x-pilot"},
        {".pdf", "application/pdf"},
        {".pem", "application/x-x509-ca-cert"},
        {".pl", "application/x-perl"},
        {".pm", "application/x-perl"},
        {".png", "image/png"},
        {".prc", "application/x-pilot"},
        {".ra", "audio/x-realaudio"},
        {".rar", "application/x-rar-compressed"},
        {".rpm", "application/x-redhat-package-manager"},
        {".rss", "text/xml"},
        {".run", "application/x-makeself"},
        {".sea", "application/x-sea"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".swf", "application/x-shockwave-flash"},
        {".tcl", "application/x-tcl"},
        {".tk", "application/x-tcl"},
        {".txt", "text/plain"},
        {".war", "application/java-archive"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wmv", "video/x-ms-wmv"},
        {".xml", "text/xml"},
        {".xpi", "application/x-xpinstall"},
        {".zip", "application/zip"},
        {".",    "text/plain"},
        #endregion
      };
      private Thread _serverThread;
      private HttpListener _listener;

      public int GFIPort
      {
         get { return _GFIPort; }
         private set { _GFIPort = value; }
      }

      public string GFIDefaultFolder
      {
         get { return _GFIDefaultFolder; }
         private set { _GFIDefaultFolder = value; }
      }

      public int GFIMaxConnections
      {
         get { return _GFIMaxConnections; }
         private set { _GFIMaxConnections = value; }
      }

      /// <summary>
      /// Construct server with given port.
      /// </summary>
      /// <param name="path">Directory path to serve.</param>
      /// <param name="port">Port of the server.</param>
      public GfiHTTPServer()
      {
         // Get GFIPort, GFIMaxConnections and GFIDefaultFolder
         Initialize();
         Logger.LogEntry("Starting Asset Management HTTP Server on Port:" + GFIPort + ".");
         _serverThread = new Thread(this.Listen);
         _serverThread.Start();
      }

      public void Stop()
      {
         _serverThread.Abort();
         _listener.Stop();
      }

      private void Listen()
      {
         _listener = new HttpListener();
         _listener.Prefixes.Add("http://*:" + _GFIPort.ToString() + "/");
         _listener.Start();
         while (!StopServer)
         {
            try
            {
               HttpListenerContext context = _listener.GetContext();
               if (context != null)
                  Process(context);
            }
            catch (Exception ex)
            {
               Logger.LogEntry("Listener Exception:" + ex.Message);
            }
         }
      }

      public void Process(HttpListenerContext context)
      {
         bool fbxValid;
         int fbxNumber;

         string filename = context.Request.Url.AbsolutePath;
         filename = filename.Substring(1);

         if (filename.CompareTo("StopAssetManagementService") != 0)
         {
            filename = GFIDefaultFolder + filename;
            if (File.Exists(filename))
            {
               try
               {
                  SendFile(context, filename);
                  context.Response.StatusCode = (int)HttpStatusCode.OK;
               }
               catch (Exception ex)
               {
                  filename = GFIDefaultFolder + "ErrorPages\\serverError.html";
                  SendFile(context, filename);
                  context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
               }
            }
            else
            {
               filename = GFIDefaultFolder + "\\ErrorPages\\fileNotFound.html";
               SendFile(context, filename);
               context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            context.Response.OutputStream.Close();
         }
         else
         {
            filename = GFIDefaultFolder + "ErrorPages\\stopServerRequested.html";
            SendFile(context, filename);
            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            context.Response.OutputStream.Close();
            StopServer = true;
         }
      }

      private void SendFile(HttpListenerContext context, string filename)
      {
         if (File.Exists(filename))
         {
            Stream input = new FileStream(filename, FileMode.Open);
            //Adding permanent http response headers
            string mime;
            // Get the current mapping, if one does not exist then use the default 
            context.Response.ContentType = _mimeTypeMappings.TryGetValue(Path.GetExtension(filename), out mime) ? mime : "application/octet-stream";
            context.Response.ContentLength64 = input.Length;
            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
            context.Response.AddHeader("Last-Modified", System.IO.File.GetLastWriteTime(filename).ToString("r"));

            byte[] buffer = new byte[1024 * 16];
            int nbytes;
            while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
               context.Response.OutputStream.Write(buffer, 0, nbytes);
            input.Close();
            context.Response.OutputStream.Flush();
            context.Response.StatusCode = (int)HttpStatusCode.OK;
         }
      }

      private void SendText(HttpListenerContext context, string text)
      {
         context.Response.ContentEncoding = System.Text.Encoding.UTF8;
         byte[] buffer = context.Response.ContentEncoding.GetBytes(text);
         context.Response.ContentLength64 = buffer.Length;
         context.Response.OutputStream.Write(buffer, 0, buffer.Length);
         context.Response.OutputStream.Flush();
         context.Response.StatusCode = (int)HttpStatusCode.OK;
      }

      public void Initialize()
      {
         bool f64bitOS = CheckOS.Is64BitOperatingSystem();
         // Solution 2. Is64BitOperatingSystem (WMI)
         // Determine whether the current operating system is a 64 bit 
         // operating system through WMI. The function is also able to 
         // query the bitness of OS on a remote machine.
         //try
         //{
         //   f64bitOS = CheckOS.Is64BitOperatingSystem(".", null, null, null);
         //   Console.WriteLine("The current operating system {0} 64-bit.",
         //       f64bitOS ? "is" : "is not");
         //}
         //catch (Exception ex)
         //{
         //   Console.WriteLine("Is64BitOperatingSystem throws the exception: {0}",
         //       ex.Message);
         //}

         // Get the GFI path

         // Set defaults
         // The name of the key must include a valid root. 
         //var view32 = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser,
         //                            RegistryView.Registry32);
         //using (var clsid32 = view32.OpenSubKey(@"Software\Classes\CLSID\", false))
         //{
         //   // actually accessing Wow6432Node 
         //}
         
         const string userRoot = "HKEY_LOCAL_MACHINE";
         string subkey;
         string keyName;

         if (f64bitOS)
         {
            subkey = "Software\\Wow6432Node\\GFI Genfare\\AssetManagement";
         }
         else
            subkey = "Software\\GFI Genfare\\AssetManagement";
         keyName = userRoot + "\\" + subkey;
         Logger.Initialize("AssetManagement"); // Initialize Logger and get path, etc.
         try
         {
            GFIPort = Convert.ToInt32(Registry.GetValue(keyName, "port", "80"));
            GFIMaxConnections = Convert.ToInt32(Registry.GetValue(keyName, "MaxConnections", "100"));
            GFIDefaultFolder = Logger.GfiRootDirectory + "\\" + Registry.GetValue(keyName, "Folder", "AssetManagement").ToString()+ "\\";
         }
         catch
         {
            GFIPort = 80;
            GFIMaxConnections = 100;
            GFIDefaultFolder = "AssetManagement";
            Logger.LogEntry("Error: Port, MaxConnections or Folder not specified in Registry.");
         }
         Logger.LogEntry("GFI Root=" + Logger.GfiRootDirectory + " Port=" + GFIPort + " MaxConnections=" + GFIMaxConnections + " AssetManagement Folder=" + GFIDefaultFolder);
      }

      public bool StopServerRequested()
      {
         if (StopServer)
            Logger.LogEntry("HTTP Server on Port:" + GFIPort + " Stopped.");
         return StopServer;
      }

      public byte[] GetBytes(string str)
      {
         byte[] bytes = new byte[str.Length * sizeof(char)];
         System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
         return bytes;
      }

      public string GetString(byte[] bytes)
      {
         char[] chars = new char[bytes.Length / sizeof(char)];
         System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
         return new string(chars);
      }

   }
}


// MultiThreaded Server Samples

///////////////////////////////////////////
// Method #1: Multithreaded Server
//////////////////////////////////////////
//class TestHttp
//{
//    static void Main()
//    {
//        using (HttpServer srvr = new HttpServer(5))
//        {
//            srvr.Start(8085);
//            Console.WriteLine("Press [Enter] to quit.");
//            Console.ReadLine();
//        }
//    }
//}


class HttpServer : IDisposable
{
    private readonly int _maxThreads;
    private readonly HttpListener _listener;
    private readonly Thread _listenerThread;
    private readonly ManualResetEvent _stop, _idle;
    private readonly Semaphore _busy;

    public HttpServer(int maxThreads)
    {
        _maxThreads = maxThreads;
        _stop = new ManualResetEvent(false);
        _idle = new ManualResetEvent(false);
        _busy = new Semaphore(maxThreads, maxThreads);
        _listener = new HttpListener();
        _listenerThread = new Thread(HandleRequests);
    }

    public void Start(int port)
    {
        _listener.Prefixes.Add(String.Format(@"http://+:{0}/", port));
        _listener.Start();
        _listenerThread.Start();
    }

    public void Dispose()
    { Stop(); }

    public void Stop()
    {
        _stop.Set();
        _listenerThread.Join();
        _idle.Reset();

        //aquire and release the semaphore to see if anyone is running, wait for idle if they are.
        _busy.WaitOne();
        if(_maxThreads != 1 + _busy.Release())
            _idle.WaitOne();

        _listener.Stop();
    }

    private void HandleRequests()
    {
        while (_listener.IsListening)
        {
            var context = _listener.BeginGetContext(ListenerCallback, null);

            if (0 == WaitHandle.WaitAny(new[] { _stop, context.AsyncWaitHandle }))
                return;
        }
    }

    private void ListenerCallback(IAsyncResult ar)
    {
        _busy.WaitOne();
        try
        {
            HttpListenerContext context;
            try
            { context = _listener.EndGetContext(ar); }
            catch (HttpListenerException)
            { return; }

            if (_stop.WaitOne(0, false))
                return;

            Console.WriteLine("{0} {1}", context.Request.HttpMethod, context.Request.RawUrl);
            context.Response.SendChunked = true;
            using (TextWriter tw = new StreamWriter(context.Response.OutputStream))
            {
                tw.WriteLine("<html><body><h1>Hello World</h1>");
                for (int i = 0; i < 5; i++)
                {
                    tw.WriteLine("<p>{0} @ {1}</p>", i, DateTime.Now);
                    tw.Flush();
                    Thread.Sleep(1000);
                }
                tw.WriteLine("</body></html>");
            }
        }
        finally
        {
            if (_maxThreads == 1 + _busy.Release())
                _idle.Set();
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// Second Sample
///////////////////////////////////////////////////////////////////////////////////////
//public void Stop() 
//{
//    lock (locker)
//    {
//        isStopping = true;
//    }
//    resetEvent.WaitOne(); //initially set to true
//    listener.Stop();
//}

//private void ListenerCallback(IAsyncResult ar)
//{
//    lock (locker) //locking on this is a bad idea, but I forget about it before
//    {
//        if (isStopping)
//            return;

//        resetEvent.Reset();
//        numberOfRequests++;
//    }

//    try
//    {
//        var listener = ar.AsyncState as HttpListener;

//        var context = listener.EndGetContext(ar);

//        //do some stuff
//    }
//    finally //to make sure that bellow code will be executed
//    {
//        lock (locker)
//        {
//            if (--numberOfRequests == 0)
//                resetEvent.Set();
//        }
//    }
//}



///////////////////////////////////////////
// Method #2: Another Sample
//////////////////////////////////////////
//using System;
//using System.IO;
//using System.Net;
//using System.Text;
//using System.Threading;

//class WebServer {
//    HttpListener _listener;
//    string _baseFolder;     

//    public WebServer(string uriPrefix, string baseFolder) {
//        System.Threading.ThreadPool.SetMaxThreads(50, 1000);
//        System.Threading.ThreadPool.SetMinThreads(50, 50);
//        _listener = new HttpListener();
//        _listener.Prefixes.Add(uriPrefix);
//        _baseFolder = baseFolder;
//    }

//    public void Start() {                       
//        _listener.Start();
//        while (true)
//            try {
//                HttpListenerContext request = _listener.GetContext();
//                ThreadPool.QueueUserWorkItem(ProcessRequest, request);
//            } catch (HttpListenerException) { break; }  
//            catch (InvalidOperationException) { break; }
//    }

//    public void Stop() { _listener.Stop(); }

//    void ProcessRequest(object listenerContext) {
//        try {
//            var context = (HttpListenerContext)listenerContext;
//            string filename = Path.GetFileName(context.Request.RawUrl);
//            string path = Path.Combine(_baseFolder, filename);
//            byte[] msg;
//            if (!File.Exists(path)) {
//                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
//                msg = Encoding.UTF8.GetBytes("Sorry, that page does not exist");
//            } else {
//                context.Response.StatusCode = (int)HttpStatusCode.OK;
//                msg = File.ReadAllBytes(path);
//            }
//            context.Response.ContentLength64 = msg.Length;
//            using (Stream s = context.Response.OutputStream)
//                s.Write(msg, 0, msg.Length);
//        } catch (Exception ex) { Console.WriteLine("Request error: " + ex); }
//    }
//}


////////////////////////////////////////////////
// Method #3: Microsoft Method
///////////////////////////////////////////////
//public static void NonblockingListener(string [] prefixes)
//{
//    HttpListener listener = new HttpListener();
//    foreach (string s in prefixes)
//    {
//        listener.Prefixes.Add(s);
//    }
//    listener.Start();
//    IAsyncResult result = listener.BeginGetContext(new AsyncCallback(ListenerCallback),listener);
//    // Applications can do some work here while waiting for the  
//    // request. If no work can be done until you have processed a request, 
//    // use a wait handle to prevent this thread from terminating 
//    // while the asynchronous operation completes.
//    Console.WriteLine("Waiting for request to be processed asyncronously.");
//    result.AsyncWaitHandle.WaitOne();
//    Console.WriteLine("Request processed asyncronously.");
//    listener.Close();
//}

//public static void ListenerCallback(IAsyncResult result)
//{
//    HttpListener listener = (HttpListener) result.AsyncState;
//    // Call EndGetContext to complete the asynchronous operation.
//    HttpListenerContext context = listener.EndGetContext(result);
//    HttpListenerRequest request = context.Request;
//    // Obtain a response object.
//    HttpListenerResponse response = context.Response;
//    // Construct a response. 
//    string responseString = "<HTML><BODY> Hello world!</BODY></HTML>";
//    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
//    // Get a response stream and write the response to it.
//    response.ContentLength64 = buffer.Length;
//    System.IO.Stream output = response.OutputStream;
//    output.Write(buffer,0,buffer.Length);
//    // You must close the output stream.
//    output.Close();
//}

