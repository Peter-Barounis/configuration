﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;

public static class Logger
{
   public  static string GfiRootDirectory {get;set;}
   public  static string ApplicationName { get; set; }
   private static System.Object logLock = new System.Object();

   public static void Initialize(string appName, string defaultPath)
   {
      ApplicationName = appName;
      if (defaultPath == null)
         GfiRootDirectory = @"C:\gfi";
      else
          GfiRootDirectory = defaultPath;
   }

   public static void LogEntry(string logStr)
   {
      string msg;
      string path = GfiRootDirectory;
      lock (logLock)
      {
         path += "\\log\\" + DateTime.Now.ToString("yyyyMMdd") + ".stl";
         // This text is added only once to the file. 
         msg = DateTime.Now.ToString("HH:mm:ss") + " " + ApplicationName + ": " + logStr;
         if (!File.Exists(path))
         {
            try
            {
               // Create a file to write to. 
               using (StreamWriter sw = File.CreateText(path))
               {
                  sw.WriteLine(msg);
               }
            }
            catch
            {
            }
         }
         else
         {
            // This text is always added, making the file longer over time 
            // if it is not deleted. 
            try
            {
               using (StreamWriter sw = File.AppendText(path))
               {
                  sw.WriteLine(msg);
               }
            }
            catch
            {
            }
         }
      }
   }
}