﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Microsoft.Win32;
using GfiUtilities;

namespace GFIHTTPServer
{
    class GfiHTTPServer : IDisposable
    {
        public int GFIPort { get; set; }
        public int GFIMaxConnections { get; set; }
        public string GFIHTTPFolder { get; set; }
        public string GFIBaseDirectory { get; set; }
        public string GFIBackupDirectory { get; set; }
        private readonly int _maxThreads;
        private readonly HttpListener _listener;
        private readonly Thread _listenerThread;
        private readonly ManualResetEvent _stop, _idle;
        private readonly Semaphore _busy;
        static bool StopServer = false;
        private static IDictionary<string, string> _mimeTypeMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) 
        {
        #region extension to MIME type list
        {".asf", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".avi", "video/x-msvideo"},
        {".bin", "application/octet-stream"},
        {".cco", "application/x-cocoa"},
        {".crt", "application/x-x509-ca-cert"},
        {".css", "text/css"},
        {".deb", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dll", "application/octet-stream"},
        {".dmg", "application/octet-stream"},
        {".ear", "application/java-archive"},
        {".eot", "application/octet-stream"},
        {".exe", "application/octet-stream"},
        {".flv", "video/x-flv"},
        {".gif", "image/gif"},
        {".hqx", "application/mac-binhex40"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".ico", "image/x-icon"},
        {".img", "application/octet-stream"},
        {".iso", "application/octet-stream"},
        {".jar", "application/java-archive"},
        {".jardiff", "application/x-java-archive-diff"},
        {".jng", "image/x-jng"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".mml", "text/mathml"},
        {".mng", "video/x-mng"},
        {".mov", "video/quicktime"},
        {".mp3", "audio/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpg", "video/mpeg"},
        {".msi", "application/octet-stream"},
        {".msm", "application/octet-stream"},
        {".msp", "application/octet-stream"},
        {".pdb", "application/x-pilot"},
        {".pdf", "application/pdf"},
        {".pem", "application/x-x509-ca-cert"},
        {".pl", "application/x-perl"},
        {".pm", "application/x-perl"},
        {".png", "image/png"},
        {".prc", "application/x-pilot"},
        {".ra", "audio/x-realaudio"},
        {".rar", "application/x-rar-compressed"},
        {".rpm", "application/x-redhat-package-manager"},
        {".rss", "text/xml"},
        {".run", "application/x-makeself"},
        {".sea", "application/x-sea"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".swf", "application/x-shockwave-flash"},
        {".tcl", "application/x-tcl"},
        {".tk", "application/x-tcl"},
        {".txt", "text/plain"},
        {".war", "application/java-archive"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wmv", "video/x-ms-wmv"},
        {".xml", "text/xml"},
        {".xpi", "application/x-xpinstall"},
        {".zip", "application/zip"},
        {".",    "text/plain"},
        #endregion
        };


        /// <summary>
        ///  GFI Http Server Performs the following functions
        ///  1. Retrieves configuration files from remote Garage Computers for editing
        ///     a. gfi.ini
        ///     b. inittab
        ///     c. netconfig.cfg
        ///     
        ///  2. Update configuration files on remote garages
        ///  
        ///  3. Command Syntax as follows (no paths allowed in file names)
        ///     a. CNFGET  [filename]       - only gfi.ini, inittab and netconfig.cfg allowed
        ///     b. CNFPUT  [filename]       - only gfi.ini, inittab and netconfig.cfg allowed
        ///     c. EXEC    [command]        - allow execution of command line parameter
        ///     d. StopGfiHttpServerService - This will stop the GFIHttpServer
        ///     
        /// NOTE: Usage of this application may be expanded 
        /// </summary>
        
        public GfiHTTPServer()
        {
            Initialize();
            _maxThreads = GFIMaxConnections;
            _stop = new ManualResetEvent(false);
            _idle = new ManualResetEvent(false);
            _busy = new Semaphore(_maxThreads, _maxThreads);
            _listener = new HttpListener();
            _listenerThread = new Thread(HandleRequests);
        }

        private void Initialize()
        {
            GFIBaseDirectory = Util.GetGfiKeyValueString("Global", "Base Directory", "C:\\GFI\\bin");
            GFIHTTPFolder = Util.GetGfiKeyValueString("Global", "HttpFolder", "C:\\GFI\\bin");
            GFIBackupDirectory = Util.GetGfiKeyValueString("Global", "ConfigBackup", "C:\\GFI\\cnf\\ConfigBackup");
            GFIPort = Util.GetGfiKeyValueDword("Global", "port", 80);
            GFIMaxConnections = Util.GetGfiKeyValueDword("Global", "HttpMaxConnections", 10);
            Logger.Initialize("GfiHttpServer", GFIBaseDirectory);

            Logger.LogEntry("Base Directory=" + GFIBaseDirectory);
            Logger.LogEntry("HttpFolder=" + GFIHTTPFolder);
            Logger.LogEntry("ConfigBackup=" + GFIBackupDirectory);
            Logger.LogEntry("Port=" + GFIPort);
            Logger.LogEntry("HttpMaxConnections=" + GFIMaxConnections);

            if (GFIMaxConnections <= 0)
                GFIMaxConnections=1;
            try
            {
                Directory.CreateDirectory(GFIBackupDirectory);
            }
            catch (Exception ex)
            {
                Logger.LogEntry("Unable to create backup folder <"+GFIBackupDirectory + ">:"+ex.ToString());
            }
            Logger.LogEntry("GfiHttpServer started");
        }

        public void Start()
        {
            _listener.Prefixes.Add(String.Format(@"http://+:{0}/", GFIPort));
            _listener.Start();
            _listenerThread.Start();
        }

        public void Dispose()
        {
            Stop();
        }

        public void Stop()
        {
            _stop.Set();
            _listenerThread.Join();
            _idle.Reset();

            //aquire and release the semaphore to see if anyone is running, wait for idle if they are.
            _busy.WaitOne();
            if (_maxThreads != 1 + _busy.Release())
                _idle.WaitOne();

            _listener.Stop();
            StopServer = true;
        }

        private void HandleRequests()
        {
            while (_listener.IsListening)
            {
                var context = _listener.BeginGetContext(ListenerCallback, null);

                if (0 == WaitHandle.WaitAny(new[] { _stop, context.AsyncWaitHandle }))
                    return;
            }
        }
        private void ListenerCallback(IAsyncResult ar)
        {
            bool err=false;
            _busy.WaitOne();
            HttpListenerContext context;
            try
            {
                context = _listener.EndGetContext(ar);
            }

            catch (HttpListenerException ex)
            {
                Logger.LogEntry("Error: " + ex.ToString());
                return;
            }

            try
            {
                char[] delimiterChars = {'+'};
                string cmd, filename;
                string cmdLine = context.Request.Url.AbsolutePath;
                cmdLine = cmdLine.Substring(1);
                string[] words = cmdLine.Split(delimiterChars);    // Format is : command|name
                if (words.Count() > 0)
                    cmd = words[0].ToUpper();
                else
                    cmd="ERROR";

                switch (cmd)
                {
                    case "STOPGFIHTTPSERVERSERVICE":    // request to stop service from remote computer
                        filename = GFIHTTPFolder + "\\ErrorPages\\stopServerRequested.html";
                        SendFile(context, filename);
                        context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        context.Response.OutputStream.Close();
                        StopServer = true;
                        break;
                    case "CNFPUT":
                        if (words.Count() > 1)
                        {
                            filename = words[1];
                            GetFile(context, filename);
                        }
                        else
                            err=true;
                        break;
                    case "CNFGET":
                        if (words.Count() > 1)
                        {
                            filename = words[1];
                            PutFile(context, filename);
                        }
                        else
                            err=true;
                        break;
                    default:
                        err=true;
                        break;
                }

                if (err)
                {
                    filename = GFIHTTPFolder + "\\ErrorPages\\fileNotFound.html";
                    SendFile(context, filename);
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    Logger.LogEntry("Error: File [" + filename + "] not found.");
                }
            }

            finally
            {
                if (_maxThreads == 1 + _busy.Release())
                    _idle.Set();
            }
        }

        private void PutFile(HttpListenerContext context, string filename)
        {
            filename = GFIBaseDirectory + "\\cnf\\" + filename;
            if (File.Exists(filename))
            {
                try
                {
                    SendFile(context, filename);
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    filename = GFIHTTPFolder + "\\ErrorPages\\serverError.html";
                    SendFile(context, filename);
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    Logger.LogEntry("Error: File " + filename + " not found.[" + ex.ToString() + "]");
                }
            }
            context.Response.OutputStream.Close();
        }

        private void BackupFile(string filename)
        {
            string newfilename;
            string datetime;
            if (File.Exists(filename))
            {
                DateTime dt = DateTime.Now;
                datetime = String.Format("_{0:yyyyMMdd_HHmmss}", dt);
                //newfilename = Path.GetDirectoryName(filename) + "\\" +
                //                Path.GetFileNameWithoutExtension(filename) +
                //                datetime +
                //                Path.GetExtension(filename);

				newfilename = GFIBackupDirectory + "\\" +
                                Path.GetFileNameWithoutExtension(filename) +
                                datetime.ToString() +
                                Path.GetExtension(filename);
                try
                {
                    File.Copy(filename, newfilename);
                }
                catch (Exception ex)
                {
                    Logger.LogEntry("Unable to backup file [" + filename + "] to [" + newfilename + "]:"+ex.ToString());
                }
            }
        }

        private void GetFile(HttpListenerContext context, string filename)
        {
            try
            {
                string line;
                long length=0;
                // Indicate that request was accepted
                context.Response.StatusCode = (int)HttpStatusCode.Accepted;

                // Get the request
                HttpListenerRequest request = context.Request;

                // Combne path and file name
                filename = GFIBaseDirectory + "\\cnf\\" + filename;

                // Backup original file
                BackupFile(filename);

                // Create a reader and writer for the file
                StreamReader sr = new StreamReader(request.InputStream, System.Text.Encoding.Default);
                StreamWriter sw = new StreamWriter(filename);

                // Read the file
                while ((line = sr.ReadLine()) != null)
                {
                    string cleaned = line.Replace("\0", string.Empty);
                    sw.WriteLine(cleaned);
                }

                // Close input stream
                sr.Close();

                // Close output stream
                sw.Close();

                Logger.LogEntry("Data: [" + filename + "] Length=" + length.ToString());
            }

            catch (Exception e)
            {
                Logger.LogEntry("Error: " + e.ToString());
                return;
            }
        }

        // Generic Send File
        private void SendFile(HttpListenerContext context, string filename)
        {
            if (File.Exists(filename))
            {
                Stream input = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                //Adding permanent http response headers
                string mime;
                // Get the current mapping, if one does not exist then use the default 
                context.Response.ContentType = _mimeTypeMappings.TryGetValue(Path.GetExtension(filename), out mime) ? mime : "application/octet-stream";
                context.Response.ContentLength64 = input.Length;
                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                context.Response.AddHeader("Last-Modified", System.IO.File.GetLastWriteTime(filename).ToString("r"));

                byte[] buffer = new byte[1024 * 16];
                int nbytes;
                while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                    context.Response.OutputStream.Write(buffer, 0, nbytes);
                input.Close();
                context.Response.OutputStream.Flush();
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                Logger.LogEntry("File " + filename + " sent to remote computer. Length=" + context.Response.ContentLength64.ToString());

            }
            else
            {
                Logger.LogEntry("Error: File " + filename + " requested but not found!");
            }
        }

        private void GetInittabFile()
        {
        }

        // This will process a request to retrieve a remote file and place
        // the file into a specific folder
        private void GetNetConfigFile()
        {
        }

        public bool StopServerRequested()
        {
            if (StopServer)
                Logger.LogEntry("HTTP Server on Port:" + GFIPort + " Stopped.");
            return StopServer;
        }
    }
}