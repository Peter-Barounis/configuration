REM Echo off

REM ===========================================================
REM Build solution
REM
REM NOTE: You can build the solution by simply calling devenv but you can also
REM       Build with MSBuild (which is preferable) as it give you a betting 
REM       logging facility as well as color coded display for warnings and errors
REM       and a lot of other stuff.
REM
REM	03/01/16 - MGM - Change to pull from DSBR_V2061119_20160422 branch instead of V7_Albany
REM ===========================================================

REM ===========================================================
REM Set up the Microsoft Development environment
REM ===========================================================
REM call "C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat" 
call "C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\Tools\vsvars32.bat" 

REM ===========================================================
REM Remove any existing files in preparation for checking out projects from GIT
REM ===========================================================
rmdir c:\git\data-system /S /Q

REM Get source code
REM ==============================================================
REM *** Use the builder user for getting the source code
REM ==============================================================
REM

echo Will now get the C Source Code
cd \git
REM Get Data System Source From Git Repository
git clone -b develop https://data-deployments:1oDP-0h[KF@bitbucket.org/genfare/data-system.git
cd data-system
git fetch && git checkout DR-160-VS2012

pause
echo Make sure that branch was successfully retrieved.

REM ===========================================================
REM Remove log file if using MSBuild
REM ===========================================================
del \git\data-system\MasterBuild\ConfigurationWinForm\msbuild.log

REM ===========================================================
REM Build the Release versions 
REM ===========================================================
cd \git\data-system\ConfigurationWinForm\ConfigurationWinForm
md bin
cd bin
md Release
cd ..\..
msbuild ConfigurationWinForm.sln /t:Build /p:Configuration=Release /FileLogger /verbosity:normal
pause


echo off
REM  echo the log file to the screen
type \git\data-system\ConfigurationWinForm\msbuild.log
REM
echo =============================================================================
echo Please check the log file for errors (/git/data-system/MasterBuild/msbuild.log)
echo =============================================================================

:CreateInstallation
set /p CONFIRMTAG=Create Installation (Y/N)?
if %CONFIRMTAG%==Y goto :Installshield
if %CONFIRMTAG%==y goto :Installshield
set /p CONFIRMTAG=Confirm Abort Installation (Y/N)?
if %CONFIRMTAG%==Y goto :End
if %CONFIRMTAG%==y goto :End
goto CreateInstallation
pause Will now start installshield
:Installshield
"C:\Program Files (x86)\InstallShield\2011\System\IscmdBld.exe" -p "C:\InstallShield Projects\Configuration\Configuration.ism"
:End
echo Installation Complete!
copy /Y "C:\InstallShield Projects\Configuration\Configuration\Media\SINGLE_EXE_IMAGE\Package\setup.exe" "\\10.1.2.204\Software\GenfareSoftware\ConfigurationTools\EXE"
pause DONE








