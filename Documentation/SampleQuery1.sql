--cnf
--tr
--ev


select * from sys.extended_properties where 
major_id=(select object_id from sys.tables where name='cnf') 
and sys.extended_properties.minor_id=0 
AND sys.extended_properties.class=1;

select * from sys.extended_properties where 
major_id=(select object_id from sys.tables where name='tr') 
and sys.extended_properties.minor_id=0 
AND sys.extended_properties.class=1;

select * from sys.extended_properties where 
major_id=(select object_id from sys.tables where name='ev') 
and sys.extended_properties.minor_id=0 
AND sys.extended_properties.class=1;

Funtion Types:


SQL_TABLE_VALUED_FUNCTION
SQL_SCALAR_FUNCTION
AGGREGATE_FUNCTION
SYSTEM_FUNCTION ???


//////////////////////////////////////////////////////////////////
// This will get you the function Names
//////////////////////////////////////////////////////////////////
SELECT name 
,SCHEMA_NAME(schema_id) AS schema_name
,type_desc
FROM sys.objects
WHERE name LIKE 'fn%' and type_desc='SQL_SCALAR_FUNCTION';

//////////////////////////////////////////////////////////////////
// This will get the Source Code for a specific funtion name
//////////////////////////////////////////////////////////////////
select ROUTINE_NAME, ROUTINE_DEFINITION, LAST_ALTERED 
from INFORMATION_SCHEMA.ROUTINES where SPECIFIC_NAME = 'fnmunint'

//////////////////////////////////////////////////////////////////
// You can also use sp_hlptext to get the source 
//////////////////////////////////////////////////////////////////
sp_helptext 'dbo.fnmunint'