﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using FastColoredTextBoxNS;
using GfiUtilities;
using System.Runtime.InteropServices;

namespace GfiLogView
{
    public partial class LazyLoadingSample : Form
    {
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);
        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        Style KeywordsStyle = new TextStyle(Brushes.Green, null, FontStyle.Regular);
        Style FunctionNameStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        bool bAlwaysOnTop = false;

        // Current Style Array
        public Style[] currentStyles = new Style[6];
        public int highlightConfig = 0;    // 0=default, 1=custom, 2=both
        public bool bhighlightChanged=false;
        // Custom styles
        public bool[]   bCusomEnable = new bool[10];
        public int[]    customStyle = new int[10];
        public string[] customregEx = new string[10];
        public string[] customComment = new string[10];
        public bool[] bIgnoreCase = new bool[10];
        public string gfiPath = @"C:\gfi";
        public string gfiIni = "gfi.ini";        
        public string filename;
        public int UseAlternateColor = 1;
        public int BackgroundColor = 163;
        bool   bAutoReloadCurrentFile = true;
        string appName = "";
        public LazyLoadingSample()
        {
            InitializeComponent();
            timer1.Interval = 4000;
        }

        public LazyLoadingSample(string fname)
        {
            FileVersionInfo fvi;
            filename = fname;
            InitializeComponent();
            if (fname.Length > 0)
            {
                fctb.OpenBindingFile(filename, Encoding.ASCII);
                fctb.IsChanged = false;
                fctb.ClearUndo();
                fctb.WordWrapIndent = 9;
                fctb.WordWrap = false;
                fctb.CurrentLineColor=Color.CadetBlue;
                GC.Collect();
                GC.GetTotalMemory(true);
                timer1.Enabled = true;
            }
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            appName = assembly.Location;
            fvi = FileVersionInfo.GetVersionInfo(appName);

            this.Text = "Genfare Quick Log Viewer v"+fvi.FileVersion+" - [" + filename + "]";
            for (int i = 0; i < 10; i++)
            {
                bCusomEnable[i] = false;
                customStyle[i] = 0;
                customregEx[i] = "";
                bIgnoreCase[i] = false;
            }
            gfiPath = Util.GetGfiKeyValueString("Global", "Base Directory", "C:\\GFI");
            // Read default configuration
            if (File.Exists(gfiPath + "\\cnf\\" + gfiIni))
                ReadConfiguration(gfiPath + "\\cnf\\" + gfiIni);
            else
                gfiIni = "";
            SetButtonStatus();
        }
        public int AlternateColors()
        {
            return UseAlternateColor;
        }
        public string IniRead(string Key, string Section, string dflt, string inifname)
        {
            var RetVal = new StringBuilder(255);
            if (GetPrivateProfileString(Section, Key, "", RetVal, 255, inifname) > 0)
                dflt = RetVal.ToString();
            return dflt;
        }

        public void IniWrite(string Key, string Section, string Value, string inifname)
        {
            if ((Value == null) || Value.Trim().Length == 0)
                Value = "";
            WritePrivateProfileString(Section, Key, Value, inifname);
        }
        private void SetButtonStatus()
        {
            if ( (filename != null) && (filename.Length > 0) )
            {
                stopLoadingLog.Enabled=timer1.Enabled;
                continueLoadingLog.Enabled = !timer1.Enabled;
                pauseToolStripMenuItem.Enabled=timer1.Enabled;
                restartToolStripMenuItem.Enabled = !timer1.Enabled;

                closeFileToolStripMenuItem.Enabled = true;
                findInLog.Enabled = true;
                saveLogFile.Enabled = true;
                printLogFile.Enabled = true;

                // Enable menu items when file is open
                miSave.Enabled = true;
                printLogFileToolStripMenuItem.Enabled = true;
                refreshToolStripMenuItem.Enabled = true;
            }
            else
            {
                stopLoadingLog.Enabled = false;
                continueLoadingLog.Enabled = false;
                pauseToolStripMenuItem.Enabled = false;
                restartToolStripMenuItem.Enabled = false;
                
                findInLog.Enabled = false;
                saveLogFile.Enabled=false;
                printLogFile.Enabled = false;

                // Disable menu items when file is closed
                closeFileToolStripMenuItem.Enabled = false;
                miSave.Enabled=false;
                printLogFileToolStripMenuItem.Enabled=false;
                refreshToolStripMenuItem.Enabled = false;
            }
            TopMost = bAlwaysOnTop;
            if (bAlwaysOnTop)
                alwaysOnTopToolStripMenuItem.Image = gfilogview.Properties.Resources.ontop;
            else
                alwaysOnTopToolStripMenuItem.Image = null;
            
            if (fctb.WordWrap)
                enableWordWrapToolStripMenuItem.Image = gfilogview.Properties.Resources.ontop;
            else
                enableWordWrapToolStripMenuItem.Image = null;

            if (bAutoReloadCurrentFile)
                AutoReloadDailyFileMnu.Image = gfilogview.Properties.Resources.ontop;
            else
                AutoReloadDailyFileMnu.Image = null;
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            ofd.Title="Select Log File";
            ofd.Filter = "Log files (*.stl)|*.stl|All Files (*.*)|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Close existing file
                if ((filename != null) && (filename.Length > 0))
                    fctb.CloseBindingFile();
                // Open New file
                fctb.OpenBindingFile(ofd.FileName, Encoding.ASCII);
                fctb.IsChanged = false;
                fctb.ClearUndo();
                fctb.WordWrapIndent = 9;
                fctb.WordWrap = false;
                GC.Collect();
                GC.GetTotalMemory(true);
                filename = ofd.FileName;
                timer1.Enabled = true;
                this.Text = "Genfare Quick Log Viewer - [" + filename + "]";
            }
            SetButtonStatus();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.RefreshBindingFile();
            fctb.DoSelectionVisible();
        }

        private void fctb_VisibleRangeChangedDelayed(object sender, EventArgs e)
        {
            HighlightVisibleRange();
        }

        private void fctb_TextChangedDelayed(object sender, TextChangedEventArgs e)
        {
            HighlightVisibleRange();
        }

        const int margin = 2000;

        public Color GetColorFromValue(int value)
        {
            System.Array knowncolors = Enum.GetValues(typeof(KnownColor));
            int i = 0;
            foreach (var a in knowncolors)
            {
                if (i == value)
                {
                    return Color.FromKnownColor((KnownColor)a);
                }
                i++;
            }
            return Color.Red;
        }

        private void HighlightVisibleRange()
        {
            fctb.AlternateBkColor = GetColorFromValue(BackgroundColor);
            fctb.AlternateBackgroundColorEnabled = (UseAlternateColor>0?true:false);

            //expand visible range (+- margin)
            var startLine = Math.Max(0, fctb.VisibleRange.Start.iLine - margin);
            var endLine = Math.Min(fctb.LinesCount - 1, fctb.VisibleRange.End.iLine + margin);
            var range = new Range(fctb, 0, startLine, 0, endLine);
            //clear folding markers
            range.ClearFoldingMarkers();

            //set markers for folding
            range.SetFoldingMarkers(@"N\d\d00", @"N\d\d99");

            range.ClearStyle(StyleIndex.All);
            //range.SetStyle(fctb.SyntaxHighlighter.BlueStyle, @"N\d+");
            //range.SetStyle(fctb.SyntaxHighlighter.RedStyle, @"[+\-]?[\d\.]+\d+");
            currentStyles[0] = fctb.SyntaxHighlighter.RedBoldStyle;
            currentStyles[1] = fctb.SyntaxHighlighter.GreenBoldStyle;
            currentStyles[2] = fctb.SyntaxHighlighter.MagentaStyle;
            currentStyles[3] = fctb.SyntaxHighlighter.MaroonStyle;
            currentStyles[4] = fctb.SyntaxHighlighter.BlueStyle;
            currentStyles[5] = fctb.SyntaxHighlighter.BrownStyleRegular;
            if (highlightConfig == 0 || highlightConfig == 2)
            {
                range.SetStyle(fctb.SyntaxHighlighter.BrownStyleRegular, @"\d\d:\d\d:\d\d ");
                range.SetStyle(fctb.SyntaxHighlighter.GreenStyleRegular, @"(\<|\().*(\>|\))");
                range.SetStyle(fctb.SyntaxHighlighter.GreenStyleRegular, @"\[.*\]");

                range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\b/home.*\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\D.\\.*\.raw");
                range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\D.\\.*\.acf");
                range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\D.\\.*\.zip");
                range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\b(TC_.*)\b");

                range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"error", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"fail", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"invalid", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                range.SetStyle(fctb.SyntaxHighlighter.MaroonStyle, @"warning", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"====.*====");
                range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"\b(gfildr|gfirun|gfi ds|gfimon|filearc|gfisync)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"\b(Calcsummary|Loader|MUX[1-4]|VLT[1-5]|init|srvr|cron|MobTcktSync|vndldr)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"\b(rfsrvr|probeserver|gfihttpsclient|ds_webreq|rfdevmgr|hcvload|hcvsync)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"Clnt\d\d\d\d\d:", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            }
            if (highlightConfig == 1 || highlightConfig == 2)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (bCusomEnable[i])
                    {
                        if (bIgnoreCase[i])
                            range.SetStyle(currentStyles[customStyle[i]], customregEx[i], System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                        else
                            range.SetStyle(currentStyles[customStyle[i]], customregEx[i]);
                    }
                }
            }
        }

        private void closeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            fctb.CloseBindingFile();
            filename = "";
            this.Text = "Genfare Quick Log Viewer";
            SetButtonStatus();
        }

        private void LazyLoadingSample_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
            fctb.CloseBindingFile();
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if(sfd.ShowDialog() == DialogResult.OK)
            {
                fctb.SaveToFile(sfd.FileName, Encoding.UTF8);
            }
            SetButtonStatus();
        }

        //private void collapseAllFoldingBlocksToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    fctb.CollapseAllFoldingBlocks();
        //    fctb.DoSelectionVisible();
        //}

        //private void expandAllCollapsedBlocksToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    fctb.ExpandAllFoldingBlocks();
        //    fctb.DoSelectionVisible();

        //}

        private void removeEmptyLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var iLines = fctb.FindLines(@"^\s*$", RegexOptions.None);
            fctb.RemoveLines(iLines);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (bAutoReloadCurrentFile)
            {
                DateTime now = DateTime.Now;
                string currentFile;
                currentFile = gfiPath + "\\log\\" + String.Format("{0:yyyyMMdd}.stl", now);
                if (string.Compare(currentFile, filename, true) != 0)
                {
                    Hide();
                    try
                    {
                        using (Process myProcess = new Process())
                        {
                            myProcess.StartInfo.UseShellExecute = false;
                            // You can start any process, HelloWorld is a do-nothing example.
                            myProcess.StartInfo.FileName = appName;
                            myProcess.StartInfo.CreateNoWindow = true;
                            myProcess.Start();
                            // This code assumes the process you are starting will terminate itself. 
                            // Given that is is started without a window so you cannot terminate it 
                            // on the desktop, it must terminate itself or you can do it programmatically
                            // from this application using the Kill method.
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Warning:"+ex.Message);
                    }
                    Close();

                    //timer1.Enabled = false;
                    //fctb.Enabled = false;
                    //fctb.CloseBindingFile();
                    //filename = currentFile;
                    //// Open New file
                    //ofd.FileName = filename;
                    //fctb.OpenBindingFile(ofd.FileName, Encoding.ASCII);
                    //fctb.IsChanged = false;
                    //fctb.ClearUndo();
                    //fctb.WordWrapIndent = 9;
                    //fctb.WordWrap = false;
                    //fctb.Enabled = false;
                    //GC.Collect();
                    //GC.GetTotalMemory(true);
                    //fctb.Enabled = true;
                    //timer1.Enabled = true;
                    //this.Text = "Genfare Quick Log Viewer - [" + filename + "]";
                }
            }
            timer1.Enabled = false;
            fctb.RefreshBindingFile();
            timer1.Enabled = true;
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            SetButtonStatus();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            SetButtonStatus();
        }

        private void enableWordWrapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.WordWrap = !fctb.WordWrap;
            SetButtonStatus();
        }

        private void searchLog(object sender, EventArgs e)
        {
            fctb.ShowFindDialog();
            SetButtonStatus();
        }

        private void printLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (!fctb.Selection.IsEmpty)
                fctb.Print(fctb.Selection, new PrintDialogSettings() { ShowPrintPreviewDialog = true });
            else
            {
                if (fctb.LinesCount > 1500)
                {
                    if (MessageBox.Show("This may take a long time (#pages=" + (fctb.LinesCount / 120).ToString() + "). Are you sure you wish to print this", "Print Log", 
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        return;
                }
                fctb.Print(new PrintDialogSettings() { ShowPrintPreviewDialog = true });
            }
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bAlwaysOnTop = !bAlwaysOnTop;
            TopMost = bAlwaysOnTop;
            SetButtonStatus();
        }

        private void AutoReloadDailyFileMnu_Click(object sender, EventArgs e)
        {
            bAutoReloadCurrentFile = !bAutoReloadCurrentFile;
            SetButtonStatus();
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fontDlg = new FontDialog();
            fontDlg.Font = fctb.Font;
            fontDlg.ShowColor = false;
            if (fontDlg.ShowDialog() != DialogResult.Cancel)
            {
                fctb.SetFont(fontDlg.Font);
                //fctb.Font = fontDlg.Font;
                fctb.Invalidate();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool bSaveTimerEnabled = timer1.Enabled;
            TopMost = false;
            bhighlightChanged = false;
            timer1.Enabled = false;
            Form config = new TextHighlighting(this);
            config.ShowDialog();
            if (bhighlightChanged)
                HighlightVisibleRange();
            bhighlightChanged = false;
            timer1.Enabled = bSaveTimerEnabled;
            SetButtonStatus();
        }

        public void ReadConfiguration(string filename)
        {
            int value;
            string temp;

            temp = IniRead("UseAlternateColor", "GFILOG", "1", filename);
            Int32.TryParse(temp, out value);
            if (value < 0 || value > 2)
                value = 0;
            UseAlternateColor = value;

            temp = IniRead("BackgroundColor", "GFILOG", "164", filename);
            Int32.TryParse(temp, out value);
            BackgroundColor = value;

            temp = IniRead("Custom", "GFILOG", "0", filename);
            Int32.TryParse(temp, out value);
            if (value < 0 || value > 2)
                value = 0;
            highlightConfig = value;

            for (int i = 0; i < 10; i++)
            {
                // Custom Flag
                value = 0;
                temp = IniRead("Enabled" + (i + 1).ToString(), "GFILOG", "0", filename);
                // Checked
                value = 0;
                Int32.TryParse(temp, out value);
                bCusomEnable[i] = (value > 0 ? true : false);

                // Color
                temp = IniRead("Color" + (i + 1).ToString(), "GFILOG", "0", filename);
                value = 0;
                Int32.TryParse(temp, out value);
                if ((value < 0) || (value > 9))
                    value = 0;
                customStyle[i] = value;

                // Regex
                temp = IniRead("Regex" + (i + 1).ToString(), "GFILOG", "", filename);
                value = 0;
                Int32.TryParse(temp, out value);
                customregEx[i] = temp;

                // IgnoreCase
                temp = IniRead("IgnoreCase" + (i + 1).ToString(), "GFILOG", "0", filename);
                value = 0;
                Int32.TryParse(temp, out value);
                this.bIgnoreCase[i] = (value > 0 ? true : false);
            }
            SetButtonStatus();
        }

        private void miOpenCurrentLog_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            string currentFile;
            currentFile = gfiPath + "\\log\\" + String.Format("{0:yyyyMMdd}.stl", now);

            // Open New file
            fctb.OpenBindingFile(currentFile, Encoding.ASCII);
            fctb.IsChanged = false;
            fctb.ClearUndo();
            fctb.WordWrapIndent = 9;
            fctb.WordWrap = false;
            GC.Collect();
            GC.GetTotalMemory(true);
            filename = ofd.FileName;
            timer1.Enabled = true;
            this.Text = "Genfare Quick Log Viewer - [" + filename + "]";
            SetButtonStatus();
        }

        private void reloadTimenumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)reloadTimenumericUpDown.Value*1000;
        }
    }
}
