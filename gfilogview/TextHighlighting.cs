﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GfiUtilities;
using System.IO;
namespace GfiLogView
{
    public partial class TextHighlighting : Form
    {
        public LazyLoadingSample lazyLoader;
        public int highlightConfig = 0;    // 0=default, 1=custom, 2=both
        public Color[] colors = new Color[6];
        public CheckBox[] cbxEnableList= new CheckBox[10];
        public CheckBox[] cbxIgnoreCaseList = new CheckBox[10]; 
        public ComboBox[] colorcomboList= new ComboBox[10]; 
        public TextBox[] regextextboxList= new TextBox[10];
        public System.Array knowncolors = Enum.GetValues(typeof(KnownColor));
        Color[] colorArray = new Color[256];
        public TextHighlighting()
        {
            InitializeComponent();
            defaultRadioButton.Checked = true;
        }

        public TextHighlighting(LazyLoadingSample lls)
        {
            lazyLoader = lls;
            InitializeComponent();

            colorComboBox1.Items.Clear();

            colors[0] = Color.Red;
            colors[1] = Color.Green;
            colors[2] = Color.Magenta;
            colors[3] = Color.Maroon;
            colors[4] = Color.Blue;
            colors[5] = Color.Brown;

            cbxEnableList[0] = enableCheckBox1;
            cbxEnableList[1] = enableCheckBox2;
            cbxEnableList[2] = enableCheckBox3;
            cbxEnableList[3] = enableCheckBox4;
            cbxEnableList[4] = enableCheckBox5;
            cbxEnableList[5] = enableCheckBox6;
            cbxEnableList[6] = enableCheckBox7;
            cbxEnableList[7] = enableCheckBox8;
            cbxEnableList[8] = enableCheckBox9;
            cbxEnableList[9] = enableCheckBox10;

            cbxIgnoreCaseList[0] = ignoreCaseCheckbox1;
            cbxIgnoreCaseList[1] = ignoreCaseCheckbox2;
            cbxIgnoreCaseList[2] = ignoreCaseCheckbox3;
            cbxIgnoreCaseList[3] = ignoreCaseCheckbox4;
            cbxIgnoreCaseList[4] = ignoreCaseCheckbox5;
            cbxIgnoreCaseList[5] = ignoreCaseCheckbox6;
            cbxIgnoreCaseList[6] = ignoreCaseCheckbox7;
            cbxIgnoreCaseList[7] = ignoreCaseCheckbox8;
            cbxIgnoreCaseList[8] = ignoreCaseCheckbox9;
            cbxIgnoreCaseList[9] = ignoreCaseCheckbox10;

            
            colorcomboList[0] = colorComboBox1;
            colorcomboList[1] = colorComboBox2;
            colorcomboList[2] = colorComboBox3;
            colorcomboList[3] = colorComboBox4;
            colorcomboList[4] = colorComboBox5;
            colorcomboList[5] = colorComboBox6;
            colorcomboList[6] = colorComboBox7;
            colorcomboList[7] = colorComboBox8;
            colorcomboList[8] = colorComboBox9;
            colorcomboList[9] = colorComboBox10;

            regextextboxList[0] = regexTextBox1;
            regextextboxList[1] = regexTextBox2;
            regextextboxList[2] = regexTextBox3;
            regextextboxList[3] = regexTextBox4;
            regextextboxList[4] = regexTextBox5;
            regextextboxList[5] = regexTextBox6;
            regextextboxList[6] = regexTextBox7;
            regextextboxList[7] = regexTextBox8;
            regextextboxList[8] = regexTextBox9;
            regextextboxList[9] = regexTextBox10;

            
            for (int i = 0; i < 10; i++)
            {
                colorcomboList[i].Items.Add("RedBoldStyle");
                colorcomboList[i].Items.Add("GreenBoldStyle");
                colorcomboList[i].Items.Add("MagentaStyle");
                colorcomboList[i].Items.Add("MaroonStyle");
                colorcomboList[i].Items.Add("BlueStyle");
                colorcomboList[i].Items.Add("BrownStyleRegular");

                cbxEnableList[i].Checked = lazyLoader.bCusomEnable[i];
                colorcomboList[i].SelectedIndex = lazyLoader.customStyle[i];
                regextextboxList[i].Text = lazyLoader.customregEx[i];
                cbxIgnoreCaseList[i].Checked = lazyLoader.bIgnoreCase[i];
            }

            LoadColorPicker();
            if (lazyLoader.BackgroundColor >= colorComboPicker.Items.Count)
                lazyLoader.BackgroundColor = 7;
            colorComboPicker.SelectedIndex = lazyLoader.BackgroundColor;
            if (lazyLoader.UseAlternateColor>0)
                alternateColors.Checked=true;
            else
                alternateColors.Checked=false;
            highlightConfig = lazyLoader.highlightConfig;            
            UpdateRadioButtons();
            SetButtonStatus();
        }

        public void UpdateRadioButtons()
        {
            switch (highlightConfig)
            {
                case 0:
                    defaultRadioButton.Checked = true;
                    break;
                case 1:
                    customRadioButton.Checked = true;
                    break;
                case 2:
                    bothRadioButton.Checked = true;
                    break;
            }
        }


        protected void DrawCustomColor(int type, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                // Get this color
                //ColorInfo color = (ColorInfo)Items[e.Index];
                Color color;
                if (type == 0)
                {
                    color = colors[e.Index];
                }
                else
                {
                    colorComboPicker.BackColor = SystemColors.Window;
                    color = colorArray[e.Index];
                }

                // Fill background
                
                e.DrawBackground();

                // Draw color box
                Rectangle rect = new Rectangle();
                rect.X = e.Bounds.X + 2;
                rect.Y = e.Bounds.Y + 2;
                rect.Width = 18;
                rect.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color), rect);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect);

                // Write color name
                Brush brush;
                if ((e.State & DrawItemState.Selected) != DrawItemState.None)
                    brush = SystemBrushes.HighlightText;
                else
                    brush = SystemBrushes.WindowText;
                if (type == 0)
                {
                    e.Graphics.DrawString(colorComboBox1.Items[e.Index].ToString(), Font, brush,
                        e.Bounds.X + rect.X + rect.Width + 2,
                        e.Bounds.Y + ((e.Bounds.Height - Font.Height) / 2));
                }
                else
                {
                    e.Graphics.DrawString(colorComboPicker.Items[e.Index].ToString(), Font, brush,
                        e.Bounds.X + rect.X + rect.Width + 2,
                        e.Bounds.Y + ((e.Bounds.Height - Font.Height) / 2));

                }

                // Draw the focus rectangle if appropriate
                if ((e.State & DrawItemState.NoFocusRect) == DrawItemState.None)
                    e.DrawFocusRectangle();
            }
        }

        protected void OnDrawHighlight(object sender, DrawItemEventArgs e)
        {
            DrawCustomColor(0,e);
        }

        private void OnDrawColorPicker(object sender, DrawItemEventArgs e)
        {
            DrawCustomColor(1,e);
        }

        public void SetButtonStatus()
        {

            // default or both checked
            if (defaultRadioButton.Checked)     // default or both checked
            {
                for (int i = 0; i < 10; i++)
                {
                    cbxEnableList[i].Enabled = false;
                    colorcomboList[i].Enabled = false;
                    regextextboxList[i].Enabled = false;
                    cbxIgnoreCaseList[i].Enabled = false;
                }
                highlightConfig = 0;
            }
            else
            {
                for (int i = 0; i < 10; i++)
                {
                    cbxEnableList[i].Enabled = true;
                    colorcomboList[i].Enabled = true;
                    regextextboxList[i].Enabled = true;
                    cbxIgnoreCaseList[i].Enabled = true;
                }

                if (bothRadioButton.Checked)    // both enabled
                {
                    highlightConfig = 2;
                }
                else                            // only custom checked
                {
                    highlightConfig = 1;
                }
            }
            if (lazyLoader.gfiIni.Length > 0)
                saveConfigurationButton.Enabled = true;
            else
                saveConfigurationButton.Enabled = false;
           colorComboPicker.Enabled = alternateColors.Checked;
        }
        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            lazyLoader.highlightConfig = highlightConfig;
            lazyLoader.bhighlightChanged = true;
            lazyLoader.BackgroundColor = colorComboPicker.SelectedIndex;
            lazyLoader.UseAlternateColor = (alternateColors.Checked ? 1 : 0);
            
            for (int i = 0; i < 10; i++)
            {
                lazyLoader.bCusomEnable[i] = cbxEnableList[i].Checked;
                lazyLoader.customStyle[i] = colorcomboList[i].SelectedIndex;
                lazyLoader.customregEx[i] = regextextboxList[i].Text;
                lazyLoader.bIgnoreCase[i] = cbxIgnoreCaseList[i].Checked;
            }

            Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            highlightConfig = 0;
            SetButtonStatus();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            highlightConfig = 1;
            SetButtonStatus();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            highlightConfig = 2;
            SetButtonStatus();
        }

        private void colorComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                if (colorcomboList[i].SelectedIndex >= 0)
                {
                    colorcomboList[i].ForeColor = colors[colorcomboList[i].SelectedIndex];
                    regextextboxList[i].ForeColor = colors[colorcomboList[i].SelectedIndex];
                }
            }
        }
        private void colorComboPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            colorComboPicker.BackColor = colorArray[colorComboPicker.SelectedIndex];
        }
        //private void ReadConfiguration(string filename)
        //{
        //    int value;
        //    string temp;
        //    temp = lazyLoader.IniRead("Custom", "GFILOG", "0", filename);
        //    Int32.TryParse(temp, out value);
        //    if (value < 0 || value > 2)
        //        value = 0;
        //    highlightConfig = value;

        //    for (int i = 0; i < 10; i++)
        //    {
        //        // Custom Flag
        //        value = 0;
        //        temp = IniRead("Enabled" + (i + 1).ToString(), "GFILOG", "0", filename);
        //        // Checked
        //        value = 0;
        //        Int32.TryParse(temp, out value);
        //        cbxEnableList[i].Checked = (value > 0 ? true : false);

        //        // Color
        //        temp = IniRead("Color" + (i + 1).ToString(), "GFILOG", "0", filename);
        //        value = 0;
        //        Int32.TryParse(temp, out value);
        //        if ((value < 0) || (value > 9))
        //            value = 0;
        //        colorcomboList[i].SelectedIndex = value;

        //        // Regex
        //        temp = IniRead("Regex" + (i + 1).ToString(), "GFILOG", "", filename);
        //        value = 0;
        //        Int32.TryParse(temp, out value);
        //        regextextboxList[i].Text = temp;

        //        // IgnoreCase
        //        temp = IniRead("IgnoreCase" + (i + 1).ToString(), "GFILOG", "0", filename);
        //        value = 0;
        //        Int32.TryParse(temp, out value);
        //        this.cbxIgnoreCaseList[i].Checked = (value > 0 ? true : false);
        //    }
        //    UpdateRadioButtons();
        //    SetButtonStatus();
        //}

        private void readButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title="Select Configuration File";
            ofd.Filter = "Configuration files (*.ini)|*.ini|All Files (*.*)|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                lazyLoader.ReadConfiguration(ofd.FileName);
        }

        private void saveConfiguration(string filename)
        {
            //lazyLoader.IniWrite("UseAlternateColor", "GFILOG", lazyLoader.UseAlternateColor.ToString(), filename);           
            lazyLoader.IniWrite("UseAlternateColor", "GFILOG", lazyLoader.AlternateColors().ToString(), filename);           
            //AlternateColors
            lazyLoader.IniWrite("BackgroundColor", "GFILOG", colorComboPicker.SelectedIndex.ToString(), filename);
            lazyLoader.IniWrite("Custom", "GFILOG", highlightConfig.ToString(), filename);
            for (int i = 0; i < 10; i++)
            {
                // Custom Flag
                lazyLoader.IniWrite("Enabled" + (i + 1).ToString(), "GFILOG", (cbxEnableList[i].Checked ? "1" : "0"), filename);
                // Color
                lazyLoader.IniWrite("Color" + (i + 1).ToString(), "GFILOG", colorcomboList[i].SelectedIndex.ToString(), filename);
                // Regex
                lazyLoader.IniWrite("Regex" + (i + 1).ToString(), "GFILOG", regextextboxList[i].Text, filename);
                // IgnoreCase
                lazyLoader.IniWrite("IgnoreCase" + (i + 1).ToString(), "GFILOG", (cbxIgnoreCaseList[i].Checked ? "1" : "0"), filename);
            }
        }
        private void saveConfigurationButton_Click(object sender, EventArgs e)
        {
            saveConfiguration(lazyLoader.gfiPath+"\\cnf\\"+lazyLoader.gfiIni);
        }

        private void saveAsConfigurationButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Save Configuration File";
            ofd.Filter = "Configuration files (*.ini)|*.ini|All Files (*.*)|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                saveConfiguration(ofd.FileName);
        }
        public void LoadColorPicker()
        {
            int i=0;
            colorComboPicker.Items.Clear();
            foreach (var a in knowncolors)
            {
                colorArray[i] = Color.FromKnownColor((KnownColor)a);
                colorComboPicker.Items.Add(colorArray[i].Name);
                i++;
            }
            colorComboPicker.SelectedIndex = 0;
        }

        private void alternateColors_CheckedChanged(object sender, EventArgs e)
        {
            lazyLoader.UseAlternateColor = (alternateColors.Checked?1:0);
            SetButtonStatus();
        }
    }
}
