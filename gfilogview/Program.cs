﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;

namespace GfiLogView
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LazyLoadingSample lazyLoader;
            //Application.Run(new Form1());
            // If GDS or locations not loaded
            string fname = GetCurrentLogFileName();
            if (File.Exists(fname))
                lazyLoader = new LazyLoadingSample(fname);
            else
                lazyLoader = new LazyLoadingSample("");
            lazyLoader.ShowDialog();
        }

        static string GetCurrentLogFileName()
        {
            string txtFileName;
            DateTime now = DateTime.Now;
            Cursor.Current = Cursors.WaitCursor;
            // Get path to log folder
            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            if (rk != null)
            {
                string Base = (string)rk.GetValue("Base Directory");
                rk.Close();
                // Create extracted File to temp file name
                txtFileName = now.ToString("yyyyMMdd") + ".stl"; // get file name with extension
                return (Base.ToString() + "\\log\\" + txtFileName); // get full path (same result as dlg.FileName)
            }
            else
                return "";
        }
    }
}
