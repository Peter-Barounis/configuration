﻿namespace GfiLogView
{
    partial class TextHighlighting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.alternateColors = new System.Windows.Forms.CheckBox();
            this.colorComboPicker = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox10 = new System.Windows.Forms.CheckBox();
            this.regexTextBox10 = new System.Windows.Forms.TextBox();
            this.enableCheckBox10 = new System.Windows.Forms.CheckBox();
            this.colorComboBox10 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox9 = new System.Windows.Forms.CheckBox();
            this.regexTextBox9 = new System.Windows.Forms.TextBox();
            this.enableCheckBox9 = new System.Windows.Forms.CheckBox();
            this.colorComboBox9 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox8 = new System.Windows.Forms.CheckBox();
            this.regexTextBox8 = new System.Windows.Forms.TextBox();
            this.enableCheckBox8 = new System.Windows.Forms.CheckBox();
            this.colorComboBox8 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox7 = new System.Windows.Forms.CheckBox();
            this.regexTextBox7 = new System.Windows.Forms.TextBox();
            this.enableCheckBox7 = new System.Windows.Forms.CheckBox();
            this.colorComboBox7 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox6 = new System.Windows.Forms.CheckBox();
            this.regexTextBox6 = new System.Windows.Forms.TextBox();
            this.enableCheckBox6 = new System.Windows.Forms.CheckBox();
            this.colorComboBox6 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox5 = new System.Windows.Forms.CheckBox();
            this.regexTextBox5 = new System.Windows.Forms.TextBox();
            this.enableCheckBox5 = new System.Windows.Forms.CheckBox();
            this.colorComboBox5 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox4 = new System.Windows.Forms.CheckBox();
            this.regexTextBox4 = new System.Windows.Forms.TextBox();
            this.enableCheckBox4 = new System.Windows.Forms.CheckBox();
            this.colorComboBox4 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox3 = new System.Windows.Forms.CheckBox();
            this.regexTextBox3 = new System.Windows.Forms.TextBox();
            this.enableCheckBox3 = new System.Windows.Forms.CheckBox();
            this.colorComboBox3 = new System.Windows.Forms.ComboBox();
            this.ignoreCaseCheckbox2 = new System.Windows.Forms.CheckBox();
            this.regexTextBox2 = new System.Windows.Forms.TextBox();
            this.enableCheckBox2 = new System.Windows.Forms.CheckBox();
            this.colorComboBox2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ignoreCaseCheckbox1 = new System.Windows.Forms.CheckBox();
            this.regexTextBox1 = new System.Windows.Forms.TextBox();
            this.enableCheckBox1 = new System.Windows.Forms.CheckBox();
            this.colorComboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bothRadioButton = new System.Windows.Forms.RadioButton();
            this.customRadioButton = new System.Windows.Forms.RadioButton();
            this.defaultRadioButton = new System.Windows.Forms.RadioButton();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.readButton = new System.Windows.Forms.Button();
            this.saveConfigurationButton = new System.Windows.Forms.Button();
            this.saveAsConfigurationButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.alternateColors);
            this.groupBox1.Controls.Add(this.colorComboPicker);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox10);
            this.groupBox1.Controls.Add(this.regexTextBox10);
            this.groupBox1.Controls.Add(this.enableCheckBox10);
            this.groupBox1.Controls.Add(this.colorComboBox10);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox9);
            this.groupBox1.Controls.Add(this.regexTextBox9);
            this.groupBox1.Controls.Add(this.enableCheckBox9);
            this.groupBox1.Controls.Add(this.colorComboBox9);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox8);
            this.groupBox1.Controls.Add(this.regexTextBox8);
            this.groupBox1.Controls.Add(this.enableCheckBox8);
            this.groupBox1.Controls.Add(this.colorComboBox8);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox7);
            this.groupBox1.Controls.Add(this.regexTextBox7);
            this.groupBox1.Controls.Add(this.enableCheckBox7);
            this.groupBox1.Controls.Add(this.colorComboBox7);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox6);
            this.groupBox1.Controls.Add(this.regexTextBox6);
            this.groupBox1.Controls.Add(this.enableCheckBox6);
            this.groupBox1.Controls.Add(this.colorComboBox6);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox5);
            this.groupBox1.Controls.Add(this.regexTextBox5);
            this.groupBox1.Controls.Add(this.enableCheckBox5);
            this.groupBox1.Controls.Add(this.colorComboBox5);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox4);
            this.groupBox1.Controls.Add(this.regexTextBox4);
            this.groupBox1.Controls.Add(this.enableCheckBox4);
            this.groupBox1.Controls.Add(this.colorComboBox4);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox3);
            this.groupBox1.Controls.Add(this.regexTextBox3);
            this.groupBox1.Controls.Add(this.enableCheckBox3);
            this.groupBox1.Controls.Add(this.colorComboBox3);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox2);
            this.groupBox1.Controls.Add(this.regexTextBox2);
            this.groupBox1.Controls.Add(this.enableCheckBox2);
            this.groupBox1.Controls.Add(this.colorComboBox2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ignoreCaseCheckbox1);
            this.groupBox1.Controls.Add(this.regexTextBox1);
            this.groupBox1.Controls.Add(this.enableCheckBox1);
            this.groupBox1.Controls.Add(this.colorComboBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(23, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(734, 374);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text Highlighting Properties";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(411, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Alternate line colors:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(412, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Background Color:";
            // 
            // alternateColors
            // 
            this.alternateColors.AutoSize = true;
            this.alternateColors.Location = new System.Drawing.Point(521, 15);
            this.alternateColors.Name = "alternateColors";
            this.alternateColors.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.alternateColors.Size = new System.Drawing.Size(15, 14);
            this.alternateColors.TabIndex = 41;
            this.alternateColors.UseVisualStyleBackColor = true;
            this.alternateColors.CheckedChanged += new System.EventHandler(this.alternateColors_CheckedChanged);
            // 
            // colorComboPicker
            // 
            this.colorComboPicker.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboPicker.FormattingEnabled = true;
            this.colorComboPicker.Location = new System.Drawing.Point(519, 39);
            this.colorComboPicker.Name = "colorComboPicker";
            this.colorComboPicker.Size = new System.Drawing.Size(203, 21);
            this.colorComboPicker.TabIndex = 40;
            this.colorComboPicker.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawColorPicker);
            this.colorComboPicker.SelectedIndexChanged += new System.EventHandler(this.colorComboPicker_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox10
            // 
            this.ignoreCaseCheckbox10.AutoSize = true;
            this.ignoreCaseCheckbox10.Location = new System.Drawing.Point(52, 345);
            this.ignoreCaseCheckbox10.Name = "ignoreCaseCheckbox10";
            this.ignoreCaseCheckbox10.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox10.TabIndex = 37;
            this.ignoreCaseCheckbox10.UseVisualStyleBackColor = true;
            // 
            // regexTextBox10
            // 
            this.regexTextBox10.Location = new System.Drawing.Point(188, 342);
            this.regexTextBox10.Name = "regexTextBox10";
            this.regexTextBox10.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox10.TabIndex = 39;
            // 
            // enableCheckBox10
            // 
            this.enableCheckBox10.AutoSize = true;
            this.enableCheckBox10.Location = new System.Drawing.Point(12, 345);
            this.enableCheckBox10.Name = "enableCheckBox10";
            this.enableCheckBox10.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox10.TabIndex = 36;
            this.enableCheckBox10.UseVisualStyleBackColor = true;
            // 
            // colorComboBox10
            // 
            this.colorComboBox10.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox10.FormattingEnabled = true;
            this.colorComboBox10.Location = new System.Drawing.Point(85, 342);
            this.colorComboBox10.Name = "colorComboBox10";
            this.colorComboBox10.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox10.TabIndex = 38;
            this.colorComboBox10.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox10.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox9
            // 
            this.ignoreCaseCheckbox9.AutoSize = true;
            this.ignoreCaseCheckbox9.Location = new System.Drawing.Point(52, 319);
            this.ignoreCaseCheckbox9.Name = "ignoreCaseCheckbox9";
            this.ignoreCaseCheckbox9.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox9.TabIndex = 33;
            this.ignoreCaseCheckbox9.UseVisualStyleBackColor = true;
            // 
            // regexTextBox9
            // 
            this.regexTextBox9.Location = new System.Drawing.Point(188, 316);
            this.regexTextBox9.Name = "regexTextBox9";
            this.regexTextBox9.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox9.TabIndex = 35;
            // 
            // enableCheckBox9
            // 
            this.enableCheckBox9.AutoSize = true;
            this.enableCheckBox9.Location = new System.Drawing.Point(12, 319);
            this.enableCheckBox9.Name = "enableCheckBox9";
            this.enableCheckBox9.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox9.TabIndex = 32;
            this.enableCheckBox9.UseVisualStyleBackColor = true;
            // 
            // colorComboBox9
            // 
            this.colorComboBox9.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox9.FormattingEnabled = true;
            this.colorComboBox9.Location = new System.Drawing.Point(85, 316);
            this.colorComboBox9.Name = "colorComboBox9";
            this.colorComboBox9.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox9.TabIndex = 34;
            this.colorComboBox9.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox9.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox8
            // 
            this.ignoreCaseCheckbox8.AutoSize = true;
            this.ignoreCaseCheckbox8.Location = new System.Drawing.Point(52, 293);
            this.ignoreCaseCheckbox8.Name = "ignoreCaseCheckbox8";
            this.ignoreCaseCheckbox8.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox8.TabIndex = 29;
            this.ignoreCaseCheckbox8.UseVisualStyleBackColor = true;
            // 
            // regexTextBox8
            // 
            this.regexTextBox8.Location = new System.Drawing.Point(188, 290);
            this.regexTextBox8.Name = "regexTextBox8";
            this.regexTextBox8.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox8.TabIndex = 31;
            // 
            // enableCheckBox8
            // 
            this.enableCheckBox8.AutoSize = true;
            this.enableCheckBox8.Location = new System.Drawing.Point(12, 293);
            this.enableCheckBox8.Name = "enableCheckBox8";
            this.enableCheckBox8.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox8.TabIndex = 28;
            this.enableCheckBox8.UseVisualStyleBackColor = true;
            // 
            // colorComboBox8
            // 
            this.colorComboBox8.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox8.FormattingEnabled = true;
            this.colorComboBox8.Location = new System.Drawing.Point(85, 290);
            this.colorComboBox8.Name = "colorComboBox8";
            this.colorComboBox8.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox8.TabIndex = 30;
            this.colorComboBox8.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox8.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox7
            // 
            this.ignoreCaseCheckbox7.AutoSize = true;
            this.ignoreCaseCheckbox7.Location = new System.Drawing.Point(52, 267);
            this.ignoreCaseCheckbox7.Name = "ignoreCaseCheckbox7";
            this.ignoreCaseCheckbox7.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox7.TabIndex = 25;
            this.ignoreCaseCheckbox7.UseVisualStyleBackColor = true;
            // 
            // regexTextBox7
            // 
            this.regexTextBox7.Location = new System.Drawing.Point(188, 264);
            this.regexTextBox7.Name = "regexTextBox7";
            this.regexTextBox7.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox7.TabIndex = 27;
            // 
            // enableCheckBox7
            // 
            this.enableCheckBox7.AutoSize = true;
            this.enableCheckBox7.Location = new System.Drawing.Point(12, 267);
            this.enableCheckBox7.Name = "enableCheckBox7";
            this.enableCheckBox7.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox7.TabIndex = 24;
            this.enableCheckBox7.UseVisualStyleBackColor = true;
            // 
            // colorComboBox7
            // 
            this.colorComboBox7.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox7.FormattingEnabled = true;
            this.colorComboBox7.Location = new System.Drawing.Point(85, 264);
            this.colorComboBox7.Name = "colorComboBox7";
            this.colorComboBox7.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox7.TabIndex = 26;
            this.colorComboBox7.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox7.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox6
            // 
            this.ignoreCaseCheckbox6.AutoSize = true;
            this.ignoreCaseCheckbox6.Location = new System.Drawing.Point(52, 241);
            this.ignoreCaseCheckbox6.Name = "ignoreCaseCheckbox6";
            this.ignoreCaseCheckbox6.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox6.TabIndex = 21;
            this.ignoreCaseCheckbox6.UseVisualStyleBackColor = true;
            // 
            // regexTextBox6
            // 
            this.regexTextBox6.Location = new System.Drawing.Point(188, 238);
            this.regexTextBox6.Name = "regexTextBox6";
            this.regexTextBox6.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox6.TabIndex = 23;
            // 
            // enableCheckBox6
            // 
            this.enableCheckBox6.AutoSize = true;
            this.enableCheckBox6.Location = new System.Drawing.Point(12, 241);
            this.enableCheckBox6.Name = "enableCheckBox6";
            this.enableCheckBox6.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox6.TabIndex = 20;
            this.enableCheckBox6.UseVisualStyleBackColor = true;
            // 
            // colorComboBox6
            // 
            this.colorComboBox6.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox6.FormattingEnabled = true;
            this.colorComboBox6.Location = new System.Drawing.Point(85, 238);
            this.colorComboBox6.Name = "colorComboBox6";
            this.colorComboBox6.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox6.TabIndex = 22;
            this.colorComboBox6.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox6.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox5
            // 
            this.ignoreCaseCheckbox5.AutoSize = true;
            this.ignoreCaseCheckbox5.Location = new System.Drawing.Point(52, 215);
            this.ignoreCaseCheckbox5.Name = "ignoreCaseCheckbox5";
            this.ignoreCaseCheckbox5.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox5.TabIndex = 17;
            this.ignoreCaseCheckbox5.UseVisualStyleBackColor = true;
            // 
            // regexTextBox5
            // 
            this.regexTextBox5.Location = new System.Drawing.Point(188, 212);
            this.regexTextBox5.Name = "regexTextBox5";
            this.regexTextBox5.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox5.TabIndex = 19;
            // 
            // enableCheckBox5
            // 
            this.enableCheckBox5.AutoSize = true;
            this.enableCheckBox5.Location = new System.Drawing.Point(12, 215);
            this.enableCheckBox5.Name = "enableCheckBox5";
            this.enableCheckBox5.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox5.TabIndex = 16;
            this.enableCheckBox5.UseVisualStyleBackColor = true;
            // 
            // colorComboBox5
            // 
            this.colorComboBox5.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox5.FormattingEnabled = true;
            this.colorComboBox5.Location = new System.Drawing.Point(85, 212);
            this.colorComboBox5.Name = "colorComboBox5";
            this.colorComboBox5.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox5.TabIndex = 18;
            this.colorComboBox5.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox5.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox4
            // 
            this.ignoreCaseCheckbox4.AutoSize = true;
            this.ignoreCaseCheckbox4.Location = new System.Drawing.Point(52, 189);
            this.ignoreCaseCheckbox4.Name = "ignoreCaseCheckbox4";
            this.ignoreCaseCheckbox4.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox4.TabIndex = 13;
            this.ignoreCaseCheckbox4.UseVisualStyleBackColor = true;
            // 
            // regexTextBox4
            // 
            this.regexTextBox4.Location = new System.Drawing.Point(188, 186);
            this.regexTextBox4.Name = "regexTextBox4";
            this.regexTextBox4.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox4.TabIndex = 15;
            // 
            // enableCheckBox4
            // 
            this.enableCheckBox4.AutoSize = true;
            this.enableCheckBox4.Location = new System.Drawing.Point(12, 189);
            this.enableCheckBox4.Name = "enableCheckBox4";
            this.enableCheckBox4.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox4.TabIndex = 12;
            this.enableCheckBox4.UseVisualStyleBackColor = true;
            // 
            // colorComboBox4
            // 
            this.colorComboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox4.FormattingEnabled = true;
            this.colorComboBox4.Location = new System.Drawing.Point(85, 186);
            this.colorComboBox4.Name = "colorComboBox4";
            this.colorComboBox4.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox4.TabIndex = 14;
            this.colorComboBox4.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox4.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox3
            // 
            this.ignoreCaseCheckbox3.AutoSize = true;
            this.ignoreCaseCheckbox3.Location = new System.Drawing.Point(52, 163);
            this.ignoreCaseCheckbox3.Name = "ignoreCaseCheckbox3";
            this.ignoreCaseCheckbox3.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox3.TabIndex = 9;
            this.ignoreCaseCheckbox3.UseVisualStyleBackColor = true;
            // 
            // regexTextBox3
            // 
            this.regexTextBox3.Location = new System.Drawing.Point(188, 160);
            this.regexTextBox3.Name = "regexTextBox3";
            this.regexTextBox3.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox3.TabIndex = 11;
            // 
            // enableCheckBox3
            // 
            this.enableCheckBox3.AutoSize = true;
            this.enableCheckBox3.Location = new System.Drawing.Point(12, 163);
            this.enableCheckBox3.Name = "enableCheckBox3";
            this.enableCheckBox3.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox3.TabIndex = 8;
            this.enableCheckBox3.UseVisualStyleBackColor = true;
            // 
            // colorComboBox3
            // 
            this.colorComboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox3.FormattingEnabled = true;
            this.colorComboBox3.Location = new System.Drawing.Point(85, 160);
            this.colorComboBox3.Name = "colorComboBox3";
            this.colorComboBox3.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox3.TabIndex = 10;
            this.colorComboBox3.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox3.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // ignoreCaseCheckbox2
            // 
            this.ignoreCaseCheckbox2.AutoSize = true;
            this.ignoreCaseCheckbox2.Location = new System.Drawing.Point(52, 137);
            this.ignoreCaseCheckbox2.Name = "ignoreCaseCheckbox2";
            this.ignoreCaseCheckbox2.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox2.TabIndex = 5;
            this.ignoreCaseCheckbox2.UseVisualStyleBackColor = true;
            // 
            // regexTextBox2
            // 
            this.regexTextBox2.Location = new System.Drawing.Point(188, 134);
            this.regexTextBox2.Name = "regexTextBox2";
            this.regexTextBox2.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox2.TabIndex = 7;
            // 
            // enableCheckBox2
            // 
            this.enableCheckBox2.AutoSize = true;
            this.enableCheckBox2.Location = new System.Drawing.Point(12, 137);
            this.enableCheckBox2.Name = "enableCheckBox2";
            this.enableCheckBox2.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox2.TabIndex = 4;
            this.enableCheckBox2.UseVisualStyleBackColor = true;
            // 
            // colorComboBox2
            // 
            this.colorComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.colorComboBox2.FormattingEnabled = true;
            this.colorComboBox2.Location = new System.Drawing.Point(85, 134);
            this.colorComboBox2.Name = "colorComboBox2";
            this.colorComboBox2.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox2.TabIndex = 6;
            this.colorComboBox2.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox2.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ignore \r\n case";
            // 
            // ignoreCaseCheckbox1
            // 
            this.ignoreCaseCheckbox1.AutoSize = true;
            this.ignoreCaseCheckbox1.Location = new System.Drawing.Point(52, 111);
            this.ignoreCaseCheckbox1.Name = "ignoreCaseCheckbox1";
            this.ignoreCaseCheckbox1.Size = new System.Drawing.Size(15, 14);
            this.ignoreCaseCheckbox1.TabIndex = 1;
            this.ignoreCaseCheckbox1.UseVisualStyleBackColor = true;
            // 
            // regexTextBox1
            // 
            this.regexTextBox1.Location = new System.Drawing.Point(188, 108);
            this.regexTextBox1.Name = "regexTextBox1";
            this.regexTextBox1.Size = new System.Drawing.Size(534, 20);
            this.regexTextBox1.TabIndex = 3;
            // 
            // enableCheckBox1
            // 
            this.enableCheckBox1.AutoSize = true;
            this.enableCheckBox1.Location = new System.Drawing.Point(12, 111);
            this.enableCheckBox1.Name = "enableCheckBox1";
            this.enableCheckBox1.Size = new System.Drawing.Size(15, 14);
            this.enableCheckBox1.TabIndex = 0;
            this.enableCheckBox1.UseVisualStyleBackColor = true;
            // 
            // colorComboBox1
            // 
            this.colorComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.colorComboBox1.FormattingEnabled = true;
            this.colorComboBox1.Location = new System.Drawing.Point(85, 108);
            this.colorComboBox1.Name = "colorComboBox1";
            this.colorComboBox1.Size = new System.Drawing.Size(93, 21);
            this.colorComboBox1.TabIndex = 2;
            this.colorComboBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawHighlight);
            this.colorComboBox1.SelectedIndexChanged += new System.EventHandler(this.colorComboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Regular Expression";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Color";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enable";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bothRadioButton);
            this.groupBox2.Controls.Add(this.customRadioButton);
            this.groupBox2.Controls.Add(this.defaultRadioButton);
            this.groupBox2.Location = new System.Drawing.Point(6, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(255, 44);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Option";
            // 
            // bothRadioButton
            // 
            this.bothRadioButton.AutoSize = true;
            this.bothRadioButton.Location = new System.Drawing.Point(200, 19);
            this.bothRadioButton.Name = "bothRadioButton";
            this.bothRadioButton.Size = new System.Drawing.Size(47, 17);
            this.bothRadioButton.TabIndex = 2;
            this.bothRadioButton.TabStop = true;
            this.bothRadioButton.Text = "Both";
            this.bothRadioButton.UseVisualStyleBackColor = true;
            this.bothRadioButton.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // customRadioButton
            // 
            this.customRadioButton.AutoSize = true;
            this.customRadioButton.Location = new System.Drawing.Point(101, 19);
            this.customRadioButton.Name = "customRadioButton";
            this.customRadioButton.Size = new System.Drawing.Size(84, 17);
            this.customRadioButton.TabIndex = 1;
            this.customRadioButton.TabStop = true;
            this.customRadioButton.Text = "Custom Only";
            this.customRadioButton.UseVisualStyleBackColor = true;
            this.customRadioButton.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // defaultRadioButton
            // 
            this.defaultRadioButton.AutoSize = true;
            this.defaultRadioButton.Location = new System.Drawing.Point(17, 19);
            this.defaultRadioButton.Name = "defaultRadioButton";
            this.defaultRadioButton.Size = new System.Drawing.Size(83, 17);
            this.defaultRadioButton.TabIndex = 0;
            this.defaultRadioButton.TabStop = true;
            this.defaultRadioButton.Text = "Default Only";
            this.defaultRadioButton.UseVisualStyleBackColor = true;
            this.defaultRadioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(467, 401);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(140, 23);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "OK";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(615, 401);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(140, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // readButton
            // 
            this.readButton.Location = new System.Drawing.Point(23, 401);
            this.readButton.Name = "readButton";
            this.readButton.Size = new System.Drawing.Size(140, 23);
            this.readButton.TabIndex = 1;
            this.readButton.Text = "Read Configuration File ...";
            this.readButton.UseVisualStyleBackColor = true;
            this.readButton.Click += new System.EventHandler(this.readButton_Click);
            // 
            // saveConfigurationButton
            // 
            this.saveConfigurationButton.Location = new System.Drawing.Point(171, 401);
            this.saveConfigurationButton.Name = "saveConfigurationButton";
            this.saveConfigurationButton.Size = new System.Drawing.Size(140, 23);
            this.saveConfigurationButton.TabIndex = 2;
            this.saveConfigurationButton.Text = "Save Configuration ...";
            this.saveConfigurationButton.UseVisualStyleBackColor = true;
            this.saveConfigurationButton.Click += new System.EventHandler(this.saveConfigurationButton_Click);
            // 
            // saveAsConfigurationButton
            // 
            this.saveAsConfigurationButton.Location = new System.Drawing.Point(319, 401);
            this.saveAsConfigurationButton.Name = "saveAsConfigurationButton";
            this.saveAsConfigurationButton.Size = new System.Drawing.Size(140, 23);
            this.saveAsConfigurationButton.TabIndex = 4;
            this.saveAsConfigurationButton.Text = "Save As Configuration ...";
            this.saveAsConfigurationButton.UseVisualStyleBackColor = true;
            this.saveAsConfigurationButton.Click += new System.EventHandler(this.saveAsConfigurationButton_Click);
            // 
            // TextHighlighting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 436);
            this.Controls.Add(this.saveAsConfigurationButton);
            this.Controls.Add(this.saveConfigurationButton);
            this.Controls.Add(this.readButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "TextHighlighting";
            this.Text = "Text Highlighting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox1;
        private System.Windows.Forms.TextBox regexTextBox1;
        private System.Windows.Forms.CheckBox enableCheckBox1;
        private System.Windows.Forms.ComboBox colorComboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton bothRadioButton;
        private System.Windows.Forms.RadioButton customRadioButton;
        private System.Windows.Forms.RadioButton defaultRadioButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox10;
        private System.Windows.Forms.TextBox regexTextBox10;
        private System.Windows.Forms.CheckBox enableCheckBox10;
        private System.Windows.Forms.ComboBox colorComboBox10;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox9;
        private System.Windows.Forms.TextBox regexTextBox9;
        private System.Windows.Forms.CheckBox enableCheckBox9;
        private System.Windows.Forms.ComboBox colorComboBox9;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox8;
        private System.Windows.Forms.TextBox regexTextBox8;
        private System.Windows.Forms.CheckBox enableCheckBox8;
        private System.Windows.Forms.ComboBox colorComboBox8;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox7;
        private System.Windows.Forms.TextBox regexTextBox7;
        private System.Windows.Forms.CheckBox enableCheckBox7;
        private System.Windows.Forms.ComboBox colorComboBox7;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox6;
        private System.Windows.Forms.TextBox regexTextBox6;
        private System.Windows.Forms.CheckBox enableCheckBox6;
        private System.Windows.Forms.ComboBox colorComboBox6;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox5;
        private System.Windows.Forms.TextBox regexTextBox5;
        private System.Windows.Forms.CheckBox enableCheckBox5;
        private System.Windows.Forms.ComboBox colorComboBox5;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox4;
        private System.Windows.Forms.TextBox regexTextBox4;
        private System.Windows.Forms.CheckBox enableCheckBox4;
        private System.Windows.Forms.ComboBox colorComboBox4;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox3;
        private System.Windows.Forms.TextBox regexTextBox3;
        private System.Windows.Forms.CheckBox enableCheckBox3;
        private System.Windows.Forms.ComboBox colorComboBox3;
        private System.Windows.Forms.CheckBox ignoreCaseCheckbox2;
        private System.Windows.Forms.TextBox regexTextBox2;
        private System.Windows.Forms.CheckBox enableCheckBox2;
        private System.Windows.Forms.ComboBox colorComboBox2;
        private System.Windows.Forms.Button readButton;
        private System.Windows.Forms.Button saveConfigurationButton;
        private System.Windows.Forms.Button saveAsConfigurationButton;
        private System.Windows.Forms.ComboBox colorComboPicker;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox alternateColors;
    }
}