﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using FastColoredTextBoxNS;

namespace MdiEdit
{
    public partial class LazyLoadingSample : Form
    {
        Style KeywordsStyle = new TextStyle(Brushes.Green, null, FontStyle.Regular);
        Style FunctionNameStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
        public string filename;

        public LazyLoadingSample()
        {
            InitializeComponent();
        }

        public LazyLoadingSample(string fname)
        {
            filename = fname;
            InitializeComponent();
            fctb.OpenBindingFile(filename, Encoding.ASCII);
            fctb.IsChanged = false;
            fctb.ClearUndo();
            fctb.WordWrapIndent = 9;
            fctb.WordWrap = false;
            GC.Collect();
            GC.GetTotalMemory(true);
            //filename = ofd.FileName;
            timer1.Enabled = true;
            SetButtonStatus();
        }

        private void SetButtonStatus()
        {
            if ( (filename != null) && (filename.Length > 0) )
            {
                stopLoadingLog.Enabled=timer1.Enabled;
                continueLoadingLog.Enabled = !timer1.Enabled;
                pauseToolStripMenuItem.Enabled=timer1.Enabled;
                restartToolStripMenuItem.Enabled = !timer1.Enabled;
                closeFileToolStripMenuItem.Enabled = true;
                findInLog.Enabled = true;
                saveLogFile.Enabled = true;
                printLogFile.Enabled = true;
            }
            else
            {
                stopLoadingLog.Enabled = false;
                continueLoadingLog.Enabled = false;
                pauseToolStripMenuItem.Enabled = false;
                restartToolStripMenuItem.Enabled = false;
                closeFileToolStripMenuItem.Enabled = false;
                findInLog.Enabled = false;
                saveLogFile.Enabled=false;
                printLogFile.Enabled = false;
            }
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if ( (filename != null) && (filename.Length > 0))
                fctb.CloseBindingFile();
            ofd.Title="Select Log File";
            ofd.Filter = "Log files (*.stl)|*.stl|All Files (*.*)|*.*";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fctb.OpenBindingFile(ofd.FileName, Encoding.ASCII);
                fctb.IsChanged = false;
                fctb.ClearUndo();
                fctb.WordWrapIndent = 9;
                fctb.WordWrap = false;
                GC.Collect();
                GC.GetTotalMemory(true);
                filename = ofd.FileName;
                timer1.Enabled = true;
            }
            else
                filename = "";
            SetButtonStatus();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.RefreshBindingFile();
        }

        private void fctb_VisibleRangeChangedDelayed(object sender, EventArgs e)
        {
            HighlightVisibleRange();
        }

        private void fctb_TextChangedDelayed(object sender, TextChangedEventArgs e)
        {
            HighlightVisibleRange();
        }

        const int margin = 2000;

        private void HighlightVisibleRange()
        {

            //expand visible range (+- margin)
            var startLine = Math.Max(0, fctb.VisibleRange.Start.iLine - margin);
            var endLine = Math.Min(fctb.LinesCount - 1, fctb.VisibleRange.End.iLine + margin);
            var range = new Range(fctb, 0, startLine, 0, endLine);
            //clear folding markers
            range.ClearFoldingMarkers();

            //set markers for folding
            range.SetFoldingMarkers(@"N\d\d00", @"N\d\d99");

            range.ClearStyle(StyleIndex.All);
            //range.SetStyle(fctb.SyntaxHighlighter.BlueStyle, @"N\d+");
            //range.SetStyle(fctb.SyntaxHighlighter.RedStyle, @"[+\-]?[\d\.]+\d+");

            range.SetStyle(fctb.SyntaxHighlighter.BrownStyle, @"[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}");
            range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"====.*====");
            range.SetStyle(fctb.SyntaxHighlighter.GreenStyle, @"\b[A-Z]:(\\|/).*\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.GreenStyle, @"\b/home.*\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\[.*\]");
            range.SetStyle(fctb.SyntaxHighlighter.GreenBoldStyle, @"\b(TC_.*)\b");
            range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"error", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"fail", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.RedBoldStyle, @"invalid", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.MaroonStyle,  @"warning", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"\b(gfildr|gfirun|gfi ds|gfimon|filearc)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            range.SetStyle(fctb.SyntaxHighlighter.BlueBoldStyle, @"\b(Calcsummary|Loader|MUX[1-4]|VLT[1-5]|init|srvr|cron|probeserver|gfihttpsclient|mobtckt_sync|vndldr)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }

        private void closeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            fctb.CloseBindingFile();
            filename = "";
            SetButtonStatus();
        }

        private void LazyLoadingSample_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
            fctb.CloseBindingFile();
        }

        private void miSave_Click(object sender, EventArgs e)
        {
            if(sfd.ShowDialog() == DialogResult.OK)
            {
                fctb.SaveToFile(sfd.FileName, Encoding.UTF8);
            }
            SetButtonStatus();
        }

        private void collapseAllFoldingBlocksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.CollapseAllFoldingBlocks();
            fctb.DoSelectionVisible();
        }

        private void expandAllCollapsedBlocksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.ExpandAllFoldingBlocks();
            fctb.DoSelectionVisible();

        }

        private void removeEmptyLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var iLines = fctb.FindLines(@"^\s*$", RegexOptions.None);
            fctb.RemoveLines(iLines);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            fctb.RefreshBindingFile();
            fctb.DoSelectionVisible();
            timer1.Enabled = true;
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            SetButtonStatus();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            SetButtonStatus();
        }

        private void enableWordWrapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.WordWrap = !fctb.WordWrap;
            SetButtonStatus();

        }

        private void searchLog(object sender, EventArgs e)
        {
            fctb.ShowFindDialog();
            SetButtonStatus();
        }

        private void printLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.Print(new PrintDialogSettings() { ShowPrintPreviewDialog = true });
        }
    }
}
