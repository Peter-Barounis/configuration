﻿namespace MdiEdit
{
    partial class FindDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonFindNext = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelFindWhat = new System.Windows.Forms.Label();
            this.controlMatchCaseCheckbox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.controlDownRadioButton = new System.Windows.Forms.RadioButton();
            this.controlUpRadioButton = new System.Windows.Forms.RadioButton();
            this.controlTextBox = new System.Windows.Forms.TextBox();
            this.buttonFindAllInAllDoc = new System.Windows.Forms.Button();
            this.buttonFindAllinCurrentDoc = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.searchFile = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonFindNext
            // 
            this.buttonFindNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFindNext.Location = new System.Drawing.Point(429, 15);
            this.buttonFindNext.Margin = new System.Windows.Forms.Padding(4);
            this.buttonFindNext.Name = "buttonFindNext";
            this.buttonFindNext.Size = new System.Drawing.Size(154, 28);
            this.buttonFindNext.TabIndex = 4;
            this.buttonFindNext.Text = "&Find Next";
            this.buttonFindNext.UseVisualStyleBackColor = true;
            this.buttonFindNext.Click += new System.EventHandler(this.buttonFindNext_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(429, 166);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(154, 28);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelFindWhat
            // 
            this.labelFindWhat.AutoSize = true;
            this.labelFindWhat.Location = new System.Drawing.Point(11, 22);
            this.labelFindWhat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFindWhat.Name = "labelFindWhat";
            this.labelFindWhat.Size = new System.Drawing.Size(72, 17);
            this.labelFindWhat.TabIndex = 0;
            this.labelFindWhat.Text = "Fi&nd what:";
            // 
            // controlMatchCaseCheckbox
            // 
            this.controlMatchCaseCheckbox.AutoSize = true;
            this.controlMatchCaseCheckbox.Location = new System.Drawing.Point(13, 68);
            this.controlMatchCaseCheckbox.Margin = new System.Windows.Forms.Padding(4);
            this.controlMatchCaseCheckbox.Name = "controlMatchCaseCheckbox";
            this.controlMatchCaseCheckbox.Size = new System.Drawing.Size(99, 21);
            this.controlMatchCaseCheckbox.TabIndex = 2;
            this.controlMatchCaseCheckbox.Text = "Match &case";
            this.controlMatchCaseCheckbox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.controlDownRadioButton);
            this.groupBox1.Controls.Add(this.controlUpRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(263, 50);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(145, 58);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Direction";
            // 
            // controlDownRadioButton
            // 
            this.controlDownRadioButton.AutoSize = true;
            this.controlDownRadioButton.Checked = true;
            this.controlDownRadioButton.Location = new System.Drawing.Point(68, 23);
            this.controlDownRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.controlDownRadioButton.Name = "controlDownRadioButton";
            this.controlDownRadioButton.Size = new System.Drawing.Size(61, 21);
            this.controlDownRadioButton.TabIndex = 1;
            this.controlDownRadioButton.TabStop = true;
            this.controlDownRadioButton.Text = "&Down";
            this.controlDownRadioButton.UseVisualStyleBackColor = true;
            // 
            // controlUpRadioButton
            // 
            this.controlUpRadioButton.AutoSize = true;
            this.controlUpRadioButton.Location = new System.Drawing.Point(8, 23);
            this.controlUpRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.controlUpRadioButton.Name = "controlUpRadioButton";
            this.controlUpRadioButton.Size = new System.Drawing.Size(44, 21);
            this.controlUpRadioButton.TabIndex = 0;
            this.controlUpRadioButton.TabStop = true;
            this.controlUpRadioButton.Text = "&Up";
            this.controlUpRadioButton.UseVisualStyleBackColor = true;
            // 
            // controlTextBox
            // 
            this.controlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.controlTextBox.Location = new System.Drawing.Point(80, 21);
            this.controlTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.controlTextBox.Name = "controlTextBox";
            this.controlTextBox.Size = new System.Drawing.Size(328, 22);
            this.controlTextBox.TabIndex = 1;
            this.controlTextBox.TextChanged += new System.EventHandler(this.controlTextBox_TextChanged);
            this.controlTextBox.Enter += new System.EventHandler(this.controlTextBox_Enter);
            // 
            // buttonFindAllInAllDoc
            // 
            this.buttonFindAllInAllDoc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonFindAllInAllDoc.Location = new System.Drawing.Point(429, 52);
            this.buttonFindAllInAllDoc.Name = "buttonFindAllInAllDoc";
            this.buttonFindAllInAllDoc.Size = new System.Drawing.Size(154, 51);
            this.buttonFindAllInAllDoc.TabIndex = 6;
            this.buttonFindAllInAllDoc.Text = "Find All in All Opened Documents";
            this.buttonFindAllInAllDoc.UseVisualStyleBackColor = true;
            this.buttonFindAllInAllDoc.Click += new System.EventHandler(this.buttonFindAllInAllDoc_Click);
            // 
            // buttonFindAllinCurrentDoc
            // 
            this.buttonFindAllinCurrentDoc.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonFindAllinCurrentDoc.Location = new System.Drawing.Point(429, 108);
            this.buttonFindAllinCurrentDoc.Name = "buttonFindAllinCurrentDoc";
            this.buttonFindAllinCurrentDoc.Size = new System.Drawing.Size(154, 51);
            this.buttonFindAllinCurrentDoc.TabIndex = 7;
            this.buttonFindAllinCurrentDoc.Text = "Find All in Current Document";
            this.buttonFindAllinCurrentDoc.UseVisualStyleBackColor = true;
            this.buttonFindAllinCurrentDoc.Click += new System.EventHandler(this.buttonFindAllinCurrentDoc_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Enabled = false;
            this.progressBar1.Location = new System.Drawing.Point(14, 128);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(393, 21);
            this.progressBar1.TabIndex = 8;
            // 
            // searchFile
            // 
            this.searchFile.AutoSize = true;
            this.searchFile.Enabled = false;
            this.searchFile.Location = new System.Drawing.Point(12, 108);
            this.searchFile.Name = "searchFile";
            this.searchFile.Size = new System.Drawing.Size(83, 17);
            this.searchFile.TabIndex = 9;
            this.searchFile.Text = "Search File ";
            // 
            // progressBar2
            // 
            this.progressBar2.Enabled = false;
            this.progressBar2.Location = new System.Drawing.Point(15, 172);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(393, 21);
            this.progressBar2.TabIndex = 10;
            // 
            // FindDialog
            // 
            this.AcceptButton = this.buttonFindNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(595, 207);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.searchFile);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonFindAllinCurrentDoc);
            this.Controls.Add(this.buttonFindAllInAllDoc);
            this.Controls.Add(this.controlTextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.controlMatchCaseCheckbox);
            this.Controls.Add(this.labelFindWhat);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonFindNext);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FindDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Find";
            this.Load += new System.EventHandler(this.FindDialog_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FindDialog_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFindNext;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelFindWhat;
        private System.Windows.Forms.CheckBox controlMatchCaseCheckbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton controlDownRadioButton;
        private System.Windows.Forms.RadioButton controlUpRadioButton;
        private System.Windows.Forms.TextBox controlTextBox;
        private System.Windows.Forms.Button buttonFindAllInAllDoc;
        private System.Windows.Forms.Button buttonFindAllinCurrentDoc;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label searchFile;
        private System.Windows.Forms.ProgressBar progressBar2;
    }
}