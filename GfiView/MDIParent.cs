using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Collections.Generic;

namespace MdiEdit
{
    public enum CHILDTYPE { Rawfile, Autoload, Farestructure, BadList, FIND, TextFile, BinaryFile, ProbeLog, StatusView, LogView, ExtractFiles, 
                            ListFiles, ACCOUNT_ACF,ACCOUNT_ASL,VAULT_FILE, CheckRawFiles, Unknown };
    public enum PRINTTYPE { TextData, RTFData, ExcelData, Unknown };

    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MDIParent : System.Windows.Forms.Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 msg,
                                            Int32 wParam, IntPtr lParam);
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem5;
        private IContainer components;
        private System.Windows.Forms.MenuItem fileMenuItem;
        private System.Windows.Forms.MenuItem winMenuItem;
        private System.Windows.Forms.MenuItem loadRawFile;
        private System.Windows.Forms.MenuItem exitMenuItem;
        private System.Windows.Forms.MenuItem cascadeMenuItem;
        private System.Windows.Forms.MenuItem horizonMenuItem;
        private System.Windows.Forms.MenuItem verticalMenuItem;
        private MenuItem SaveAs;
        private MenuItem SaveAll;
        private MenuItem fileClose;
        private MenuItem loadAutoloadFile;
        private MenuItem loadFareStructure;
        public int childCount = 0;
        private MenuItem editMenuItem;
        private MenuItem findMenuItem;
        private MenuItem loadBadList;
        private MenuItem menuItem1;
        private StatusStrip statusStrip1;
        public ToolStripStatusLabel controlCaretPositionLabel;
        private MDIChildEx currentchild;
        private MenuItem menuItem3;
        private MenuItem menuItemEditGoTo;
        private MenuItem menuItemCut;
        private MenuItem menuItemCopy;
        private MenuItem menuItemPaste;
        private MenuItem menuItem8;
        private MenuItem menuItemUndo;
        private MenuItem menuItem4;

        //bool createTestFareStructure;
        public Thread tFindDialog;
        private MenuItem menuItemCloseAll;
        private MenuItem menuItem6;

        private FindDialog _FindDialog;
        
        private MenuItem loadTextFile;
        private MenuItem openBinaryFile;
        private MenuItem menuItem9;
        private MenuItem menuItem7;

        private string lastPath = "";
        public int SystemType = 1;
        public string location;

        public int NumberOfLocations = 0;
        public string strLocations = "";
        public string Base;
        public List<string> CurrentLocations = new List<string>();
        private MenuItem openGFIListFiles;
        private MenuItem menuItem10;
        private MenuItem printDocument;
        private MenuItem printPreviewDocument;
        private MenuItem menuItem12;
        private MenuItem extractProbings;
        private MenuItem menuItem14;
        private MenuItem menuOpenLogFile;
        private MenuItem menuOpenProbeLog;
        private MenuItem menuOpenCurrentLog;
        private MenuItem menuOpenSelectedLog;
        private MenuItem menuEncrypt;
        private MenuItem menuItem2;
        private string[] installTypes = new string[] { "Unknown", "Garage Data System", "Network Manager", "Garage Workstation", "Network Manager Workstation", "Data SYstem 8", null };
        public string gfiVersion;
        private MenuItem loadACF;
        private MenuItem LoadASL;
        private MenuItem menuItem11;
        private MenuItem menuItem13;
        private MenuItem menuItem15;
        //int NumLocations = 0;
        private MenuItem menuItem16;
        string NMDBMS = "";

        public MDIParent()
        {
            // Required for Windows Form Designer support
            //
            EncryptForm abc = new EncryptForm();
            InitializeComponent();
            GetSystemType();    // Get System Type: 1=Garage Computer, 2=Network Manager

            if (SystemType == 2)  // For Network Manager, Get the Locations
            {
                if (NMDBMS.ToUpper() == "MSS")
                    GetLocations();
            }
            else
                CurrentLocations.Add("aa");
            // TODO: Add any constructor code after InitializeComponent call
            // Center form on screen
            this.CenterToScreen();
            SaveAs.Enabled = false;
            SaveAll.Enabled = false;
            fileClose.Enabled = false;
            controlCaretPositionLabel.Tag = controlCaretPositionLabel.Text;
            if (SystemType < 0 || SystemType > 5)
            {
                Text = "Genfare File Viewer";
                SystemType = 0;
            }
            else
                Text = "Genfare File Viewer - " + installTypes[SystemType] + " Ver. " + gfiVersion;
            UpdateMenuItems();
        }

        void  GetSystemType()
        {
            SystemType = 1;
            gfiVersion = "3.1";
            NMDBMS = "MSS";
            // Get system Type
            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            object Base = rk.GetValue("System Type");
            if (Base != null)
                SystemType = (int)Base;
            rk.Close();

            //if ((SystemType == 1) || (SystemType==3))
            //    dbms = "NM DBMS";
            //else
            //    dbms = "DBMS";
            // Get version
            rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\");
            if (Base != null)
            Base = rk.GetValue("Version");
                gfiVersion = (string)Base;
            rk.Close();

            // Get system Type
            rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            Base = rk.GetValue("DBMS");
            if (Base != null)
                NMDBMS = Base.ToString();
            else
            rk.Close();
        }

        // Find all of the locations and build a location string
        void GetLocations()
        {
            byte[] buffer=new byte[2048];
            string connetionString = BuildConnectionString();

            OdbcConnection cnn;
            try
            {
                cnn = new OdbcConnection(connetionString);
                cnn.Open();
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
                cnn = null;
            }
            OdbcCommand DbCommand = cnn.CreateCommand();
            DbCommand.CommandText = "SELECT loc_n FROM cnf;";
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            //NumLocations = 0;
            Byte[] bytes = new Byte[1];
            ASCIIEncoding ascii = new ASCIIEncoding();
            string s;
            int locNumber;
            while (DbReader.Read())
            {
                locNumber = Convert.ToInt16(DbReader.GetString(0));
                NumberOfLocations++;
                bytes[0] = (byte)(96 + NumberOfLocations);
                s = ascii.GetString(bytes);
                CurrentLocations.Add("a" + s);
            }
            DbReader.Close();
            DbCommand.Dispose();
            cnn.Close();
        }

        // Get the Sybase connection string 
        public string GetASAConnectionString()
        {
            string connstr;
            string sysconEng;

            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global");
            object Base = rk.GetValue("SYSCON ENG");
            sysconEng = (string)Base;
            rk.Close();

            string ENG = sysconEng;
            string DBN = sysconEng;
            connstr = "LINKS=tcpip(Host=Localhost;DoBroadcast=Direct);ENG=" + ENG + ";DBN=" + DBN + ";UID=dba;PWD=gfi314159gfi;ASTOP=NO;Driver=Adaptive Server Anywhere 9.0;App=ConfigurationAssistant";
            return connstr;
        }

        ///////////////////////////////////////////////////////////////////
        // This will get the SQL Server Driver Name
        ///////////////////////////////////////////////////////////////////
        public string GetSQLServerDriverName()
        {
            string keyValue = "";
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers");
            if (regKey != null)
            {
                foreach (var value in regKey.GetValueNames())
                {
                    string data = Convert.ToString(regKey.GetValue(value)).ToLower();
                    if (data.CompareTo("installed") == 0)
                    {
                        keyValue = Convert.ToString(value);
                        if (keyValue.ToLower().Contains("sql server native client"))
                            break;
                    }
                }

            }
            return keyValue;
        }

        public string BuildConnectionString()
        {
            string connString = "";

            string gfiUser = "UID";
            string gfiPwd = "PWD";

            string globalKey = "SOFTWARE\\gfi genfare\\global";	// Key
            string dsn = "SYSCON DSN";					// Network Manager IP address or Computer Name

            string dsnData;
            string gfiUserData = "";
            string gfiPwdData = "";
            dsn = "SYSCON DSN";

            switch (SystemType)
            {
                case 1: // Garage Computer
                    connString = GetASAConnectionString();
                    break;
                case 2: // Network Manager:
                    RegistryKey regKey = Registry.LocalMachine.OpenSubKey(globalKey);
                    object obj = regKey.GetValue(gfiUser);
                    if (obj != null)
                        gfiUserData = (string)obj;
                    else
                        gfiUserData = "gfi";
                    obj = regKey.GetValue(gfiPwd);
                    if (obj != null)
                        gfiPwdData = (string)obj;
                    else
                        gfiPwdData = "gfi";

                    obj = regKey.GetValue(dsn);
                    if (obj != null)
                        dsnData = (string)obj;
                    else
                        dsnData="Localhost";
                    connString = "DRIVER={" + GetSQLServerDriverName() + "};Server=" + dsnData + ";Database=gfi;UID=" + gfiUserData + ";PWD=" + gfiPwdData;
                    break;
            }
            return connString;
        }

        // Form load event or a similar place
        private void MDIParent_Load(object sender, EventArgs e)
        {
            // Enable drag and drop for this form
            // (this can also be applied to any controls)
            this.AllowDrop = true;

            // Add event handlers for the drag & drop functionality
            this.DragEnter += new DragEventHandler(Form_DragEnter);
            this.DragDrop += new DragEventHandler(Form_DragDrop);
            string[] str = Environment.GetCommandLineArgs();
            if (str.Length > 1)
            {
                if (String.Compare("ProbeLog", str[1]) == 0)
                {
                    openCurrentProbeLogFile("", "");
                }
            }
        }

        // This event occurs when the user drags over the form with 
        // the mouse during a drag drop operation 
        void Form_DragEnter(object sender, DragEventArgs e)
        {
            // Check if the Dataformat of the data can be accepted
            // (we only accept file drops from Explorer, etc.)
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy; // Okay
            else
                e.Effect = DragDropEffects.None; // Unknown data, ignore it
        }

        // Occurs when the user releases the mouse over the drop target 
        void Form_DragDrop(object sender, DragEventArgs e)
        {
            // Extract the data from the DataObject-Container into a string list
            string errorList="";
            string[] FileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            foreach (string rawfile in FileList)
            {
                if (String.Compare(rawfile.Substring(rawfile.Length - 3, 3), "raw", true) == 0)
                    openIndividualRawFile(rawfile, CHILDTYPE.Rawfile);
                else
                {
                    //if (String.Compare(rawfile.Substring(rawfile.Length - 3, 3), "txt", true) == 0)
                        openIndividualRawFile(rawfile, CHILDTYPE.TextFile);
                    //else
                    //{
                      //  errorList += rawfile;
                        //errorList += "\r\n";
                    //}
                }
            }

            if (errorList.Length > 0)
            {
                MessageBox.Show("The following files must be opened from the File Menu.\r\n" + errorList, "Unable to Open files with ambiguous file extensions.");
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            e.Cancel = false;
            if (!e.Cancel)
                Dispose(true);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.loadRawFile = new System.Windows.Forms.MenuItem();
            this.loadAutoloadFile = new System.Windows.Forms.MenuItem();
            this.loadFareStructure = new System.Windows.Forms.MenuItem();
            this.loadBadList = new System.Windows.Forms.MenuItem();
            this.openGFIListFiles = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.loadTextFile = new System.Windows.Forms.MenuItem();
            this.openBinaryFile = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.loadACF = new System.Windows.Forms.MenuItem();
            this.LoadASL = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem15 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.fileClose = new System.Windows.Forms.MenuItem();
            this.menuItemCloseAll = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.SaveAs = new System.Windows.Forms.MenuItem();
            this.SaveAll = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.printDocument = new System.Windows.Forms.MenuItem();
            this.printPreviewDocument = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.exitMenuItem = new System.Windows.Forms.MenuItem();
            this.editMenuItem = new System.Windows.Forms.MenuItem();
            this.findMenuItem = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItemUndo = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItemCut = new System.Windows.Forms.MenuItem();
            this.menuItemCopy = new System.Windows.Forms.MenuItem();
            this.menuItemPaste = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItemEditGoTo = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.extractProbings = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.menuOpenLogFile = new System.Windows.Forms.MenuItem();
            this.menuOpenProbeLog = new System.Windows.Forms.MenuItem();
            this.menuOpenCurrentLog = new System.Windows.Forms.MenuItem();
            this.menuOpenSelectedLog = new System.Windows.Forms.MenuItem();
            this.menuEncrypt = new System.Windows.Forms.MenuItem();
            this.winMenuItem = new System.Windows.Forms.MenuItem();
            this.cascadeMenuItem = new System.Windows.Forms.MenuItem();
            this.horizonMenuItem = new System.Windows.Forms.MenuItem();
            this.verticalMenuItem = new System.Windows.Forms.MenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.controlCaretPositionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(842, 24);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Visible = false;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            this.tabControl1.Enter += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuItem,
            this.editMenuItem,
            this.menuItem12,
            this.winMenuItem});
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.loadRawFile,
            this.loadAutoloadFile,
            this.loadFareStructure,
            this.loadBadList,
            this.openGFIListFiles,
            this.menuItem2,
            this.menuItem9,
            this.loadTextFile,
            this.openBinaryFile,
            this.menuItem7,
            this.loadACF,
            this.LoadASL,
            this.menuItem13,
            this.menuItem15,
            this.menuItem11,
            this.fileClose,
            this.menuItemCloseAll,
            this.menuItem6,
            this.SaveAs,
            this.SaveAll,
            this.menuItem10,
            this.printDocument,
            this.printPreviewDocument,
            this.menuItem5,
            this.exitMenuItem});
            this.fileMenuItem.Text = "&File";
            // 
            // loadRawFile
            // 
            this.loadRawFile.Index = 0;
            this.loadRawFile.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
            this.loadRawFile.Text = "&Open Raw File(s) ...";
            this.loadRawFile.Click += new System.EventHandler(this.loadRawFile_Click);
            // 
            // loadAutoloadFile
            // 
            this.loadAutoloadFile.Index = 1;
            this.loadAutoloadFile.Text = "Open Autoload File(s) ...";
            this.loadAutoloadFile.Click += new System.EventHandler(this.loadAutoloadFile_Click);
            // 
            // loadFareStructure
            // 
            this.loadFareStructure.Index = 2;
            this.loadFareStructure.Text = "Open Fare Structure Files (s)...";
            this.loadFareStructure.Click += new System.EventHandler(this.loadFareStructure_Click);
            // 
            // loadBadList
            // 
            this.loadBadList.Index = 3;
            this.loadBadList.Text = "Open Bad List File (s)...";
            this.loadBadList.Click += new System.EventHandler(this.openBanList_Click);
            // 
            // openGFIListFiles
            // 
            this.openGFIListFiles.Index = 4;
            this.openGFIListFiles.Text = "Open List File File (s)...";
            this.openGFIListFiles.Click += new System.EventHandler(this.openGFIListFiles_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 5;
            this.menuItem2.Text = "Open Zone Table File (s) ...";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click_1);
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 6;
            this.menuItem9.Text = "-";
            // 
            // loadTextFile
            // 
            this.loadTextFile.Index = 7;
            this.loadTextFile.Text = "Open Text File (s) ...";
            this.loadTextFile.Click += new System.EventHandler(this.loadTextFile_Click);
            // 
            // openBinaryFile
            // 
            this.openBinaryFile.Index = 8;
            this.openBinaryFile.Text = "Open Binary File (s)...";
            this.openBinaryFile.Click += new System.EventHandler(this.openBinaryFile_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 9;
            this.menuItem7.Text = "-";
            // 
            // loadACF
            // 
            this.loadACF.Index = 10;
            this.loadACF.Text = "Open Account Based ACF Transaction File (s) ...";
            this.loadACF.Click += new System.EventHandler(this.loadACF_Click);
            // 
            // LoadASL
            // 
            this.LoadASL.Index = 11;
            this.LoadASL.Text = "Open Account Based ASL List File (s) ...";
            this.LoadASL.Click += new System.EventHandler(this.LoadASL_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 12;
            this.menuItem13.Text = "-";
            // 
            // menuItem15
            // 
            this.menuItem15.Index = 13;
            this.menuItem15.Text = "Open WMATA Vault File (s) ...";
            this.menuItem15.Click += new System.EventHandler(this.menuItem15_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Index = 14;
            this.menuItem11.Text = "-";
            // 
            // fileClose
            // 
            this.fileClose.Index = 15;
            this.fileClose.Text = "Close";
            this.fileClose.Click += new System.EventHandler(this.fileClose_Click);
            // 
            // menuItemCloseAll
            // 
            this.menuItemCloseAll.Index = 16;
            this.menuItemCloseAll.Text = "Close All";
            this.menuItemCloseAll.Click += new System.EventHandler(this.menuItemCloseAll_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 17;
            this.menuItem6.Text = "-";
            // 
            // SaveAs
            // 
            this.SaveAs.Index = 18;
            this.SaveAs.Text = "Save As ...";
            this.SaveAs.Click += new System.EventHandler(this.saveAs_Click);
            // 
            // SaveAll
            // 
            this.SaveAll.Index = 19;
            this.SaveAll.Text = "Save All";
            this.SaveAll.Click += new System.EventHandler(this.saveAll_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 20;
            this.menuItem10.Text = "-";
            // 
            // printDocument
            // 
            this.printDocument.Index = 21;
            this.printDocument.Text = "Print";
            this.printDocument.Click += new System.EventHandler(this.printDocument_click);
            // 
            // printPreviewDocument
            // 
            this.printPreviewDocument.Index = 22;
            this.printPreviewDocument.Text = "Print Preview";
            this.printPreviewDocument.Click += new System.EventHandler(this.printPreviewDocument_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 23;
            this.menuItem5.Text = "-";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Index = 24;
            this.exitMenuItem.Text = "Exit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // editMenuItem
            // 
            this.editMenuItem.Index = 1;
            this.editMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.findMenuItem,
            this.menuItem1,
            this.menuItem3,
            this.menuItemUndo,
            this.menuItem4,
            this.menuItemCut,
            this.menuItemCopy,
            this.menuItemPaste,
            this.menuItem8,
            this.menuItemEditGoTo});
            this.editMenuItem.Text = "Edit";
            // 
            // findMenuItem
            // 
            this.findMenuItem.Index = 0;
            this.findMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
            this.findMenuItem.Text = "&Find...         [CTRL/F]";
            this.findMenuItem.Click += new System.EventHandler(this.findMenuItem_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Enabled = false;
            this.menuItem1.Index = 1;
            this.menuItem1.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.menuItem1.Text = "Replace...   [CTRL/R]";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 2;
            this.menuItem3.Text = "-";
            // 
            // menuItemUndo
            // 
            this.menuItemUndo.Index = 3;
            this.menuItemUndo.Shortcut = System.Windows.Forms.Shortcut.CtrlZ;
            this.menuItemUndo.Text = "&Undo";
            this.menuItemUndo.Click += new System.EventHandler(this.menuItemUndo_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 4;
            this.menuItem4.Text = "-";
            // 
            // menuItemCut
            // 
            this.menuItemCut.Index = 5;
            this.menuItemCut.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.menuItemCut.Text = "&Cut     [Ctrl/X]";
            this.menuItemCut.Click += new System.EventHandler(this.menuItemCut_Click);
            // 
            // menuItemCopy
            // 
            this.menuItemCopy.Index = 6;
            this.menuItemCopy.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.menuItemCopy.Text = "C&opy  [Ctrl/C]";
            this.menuItemCopy.Click += new System.EventHandler(this.menuItemCopy_Click);
            // 
            // menuItemPaste
            // 
            this.menuItemPaste.Index = 7;
            this.menuItemPaste.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
            this.menuItemPaste.Text = "&Paste  [Ctrl/V]";
            this.menuItemPaste.Click += new System.EventHandler(this.menuItemPaste_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 8;
            this.menuItem8.Text = "-";
            // 
            // menuItemEditGoTo
            // 
            this.menuItemEditGoTo.Index = 9;
            this.menuItemEditGoTo.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.menuItemEditGoTo.Text = "&Goto... ";
            this.menuItemEditGoTo.Click += new System.EventHandler(this.menuItemEditGoTo_Click);
            // 
            // menuItem12
            // 
            this.menuItem12.Index = 2;
            this.menuItem12.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.extractProbings,
            this.menuItem16,
            this.menuItem14,
            this.menuOpenLogFile,
            this.menuOpenProbeLog,
            this.menuEncrypt});
            this.menuItem12.Text = "Tools";
            // 
            // extractProbings
            // 
            this.extractProbings.Index = 0;
            this.extractProbings.Text = "Extract Probings from Raw File ...";
            this.extractProbings.Click += new System.EventHandler(this.extractProbingsFromRawFile_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.Index = 1;
            this.menuItem16.Text = "Check Raw Files (s)";
            this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
            // 
            // menuItem14
            // 
            this.menuItem14.Index = 2;
            this.menuItem14.Text = "-";
            // 
            // menuOpenLogFile
            // 
            this.menuOpenLogFile.Index = 3;
            this.menuOpenLogFile.Text = "Open Log File";
            this.menuOpenLogFile.Click += new System.EventHandler(this.openLogFile_Click);
            // 
            // menuOpenProbeLog
            // 
            this.menuOpenProbeLog.Index = 4;
            this.menuOpenProbeLog.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuOpenCurrentLog,
            this.menuOpenSelectedLog});
            this.menuOpenProbeLog.Text = "Open Probe Log";
            // 
            // menuOpenCurrentLog
            // 
            this.menuOpenCurrentLog.Index = 0;
            this.menuOpenCurrentLog.Text = "CurrentLog";
            this.menuOpenCurrentLog.Click += new System.EventHandler(this.openCurrentProbeLog_Click);
            // 
            // menuOpenSelectedLog
            // 
            this.menuOpenSelectedLog.Index = 1;
            this.menuOpenSelectedLog.Text = "Select Log...";
            this.menuOpenSelectedLog.Click += new System.EventHandler(this.openSelectedProbeLog_Click);
            // 
            // menuEncrypt
            // 
            this.menuEncrypt.Index = 5;
            this.menuEncrypt.Text = "Encrypt/Decrypt";
            this.menuEncrypt.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // winMenuItem
            // 
            this.winMenuItem.Index = 3;
            this.winMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.cascadeMenuItem,
            this.horizonMenuItem,
            this.verticalMenuItem});
            this.winMenuItem.Text = "&Window";
            // 
            // cascadeMenuItem
            // 
            this.cascadeMenuItem.Index = 0;
            this.cascadeMenuItem.Text = "&Cascade";
            this.cascadeMenuItem.Click += new System.EventHandler(this.cascadeMenuItem_Click);
            // 
            // horizonMenuItem
            // 
            this.horizonMenuItem.Index = 1;
            this.horizonMenuItem.Text = "Tile &Horizontal";
            this.horizonMenuItem.Click += new System.EventHandler(this.horizonMenuItem_Click);
            // 
            // verticalMenuItem
            // 
            this.verticalMenuItem.Index = 2;
            this.verticalMenuItem.Text = "Tile &Vertical";
            this.verticalMenuItem.Click += new System.EventHandler(this.verticalMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.controlCaretPositionLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 577);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(842, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "controlStatusBar";
            // 
            // controlCaretPositionLabel
            // 
            this.controlCaretPositionLabel.Name = "controlCaretPositionLabel";
            this.controlCaretPositionLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.controlCaretPositionLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // MDIParent
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(842, 599);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Menu = this.mainMenu1;
            this.Name = "MDIParent";
            this.Text = "Genfare File Viewer";
            this.Load += new System.EventHandler(this.MDIParent_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new MDIParent());
        }

        public MDIChildEx CreateNewWindow(string tabTitle, string childTitle, string windowText, CHILDTYPE type)
        {

            //Creating MDI child form and initialize its fields
            MDIChildEx childForm = new MDIChildEx(this, childTitle, type);
            childForm.richTextBox1.Text = windowText;
            childForm.TabFileName = tabTitle;
            childForm.MdiParent = this;
            childForm.Type = type;

            //child Form will now hold a reference value to the tab control
            childForm.TabCtrl = tabControl1;

            //Add a Tabpage and enables it
            TabPage tp = new TabPage();
            tp.Parent = tabControl1;
            tp.Text = tabTitle;
            tp.Show();
            //child Form will now hold a reference value to a tabpage
            childForm.TabPag = tp;

            //Activate the MDI child form
            currentchild = childForm;
            childForm.Show();
            childForm.TabPag.Show();

            //Activate the newly created Tabpage
            tabControl1.SelectedTab = tp;
            SaveAs.Enabled = true;
            SaveAll.Enabled = true;

            return childForm;
        }

        private void loadRawFile_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Raw File",
                            "All files (*.*)|*.*|Raw Files (*.raw)|*.raw",
                            " -f ",
                            CHILDTYPE.Rawfile);
        }

        private void exitMenuItem_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void GetCurrentChild()
        {
            childCount = 0;
            SaveAs.Enabled = false;
            SaveAll.Enabled = false;

            fileClose.Enabled = false;
            currentchild = null;
            foreach (MDIChildEx childForm in this.MdiChildren)
            {
                //Check for its corresponding MDI child form
                if (childForm.TabPag.Equals(tabControl1.SelectedTab))
                {
                    //Activate the MDI child form
                    childForm.Select();
                    childCount++;
                    SaveAs.Enabled = true;
                    SaveAll.Enabled = false;
                    fileClose.Enabled = true;
                    if (_FindDialog != null)
                    {
                        _FindDialog.childWindow = childForm;
                        childForm.richTextBox1.SelectionStart = 0;
                    }

                    currentchild = childForm;
                    break;
                }
            }


            if (currentchild == null)
            {
                controlCaretPositionLabel.Tag = controlCaretPositionLabel.Text;
                tabControl1.Visible = false;
            }
            else
            {
                currentchild.UpdateStatusBar();
                tabControl1.Visible = true;
            }

            UpdateMenuItems();
        }

        private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            GetCurrentChild();

        }

        private void cascadeMenuItem_Click(object sender, System.EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void horizonMenuItem_Click(object sender, System.EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalMenuItem_Click(object sender, System.EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void saveAs_Click(object sender, EventArgs e)
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog fdlg = new SaveFileDialog();
            fdlg.FileName = tabControl1.SelectedTab.Text;
            fdlg.FileName = fdlg.FileName.Replace(".raw", ".txt");
            fdlg.Filter = "Text File|*.txt|All Files (*.*)|*.*";
            fdlg.Title = "Select File Name to Save";
            fdlg.OverwritePrompt = true;
            if ((fdlg.FilterIndex == 1) && (!fdlg.FileName.ToLower().EndsWith(".txt")))
                fdlg.FileName += ".txt";

            // If the file name is not an empty string open it for saving.
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                if (fdlg.FileName != "")
                {
                    childCount = 0;
                    foreach (MDIChildEx child in this.MdiChildren)
                    {
                        //Check for its corresponding MDI child form
                        if (child.TabPag.Equals(tabControl1.SelectedTab))
                        {
                            //Activate the MDI child form
                            child.Select();
                            using (StreamWriter sw = new StreamWriter(fdlg.FileName))
                            {
                                foreach (string l in child.richTextBox1.Lines)
                                    sw.WriteLine(l);
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            //childCount = 0;
        }

        private void fileClose_Click(object sender, EventArgs e)
        {
            if (childCount >= 0)
            {
                if (ActiveMdiChild != null)
                {
                    ActiveMdiChild.Close();
                    GetCurrentChild();
                }
            }
        }

        private void loadAutoloadFile_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Autoload File",
                            "All files (*.*)|*.*|Autoload Files (*.bin)|*.bin",
                            "-t ",
                            CHILDTYPE.Autoload);
            UpdateMenuItems();
        }

        private void loadFareStructure_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Fare Structure File",
                            "All files (*.*)|*.*|Fare Structure Files (*.bin)|*.bin",
                            " -v ",
                            CHILDTYPE.Farestructure);
        }

        private void openBanList_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Bad List File",
                            "All files (*.*)|*.*|Bad List Files (*.bin)|*.bin",
                            " -w ",
                            CHILDTYPE.Autoload);
        }

        private void openCurrentProbeLogFile(string loc, string txtLocation)
        {
            string txtFileName;
            string txtFilePath;
            DateTime now = DateTime.Now;
            Cursor.Current = Cursors.WaitCursor;
            location = loc;

            // Get path to log folder
            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            string Base = (string)rk.GetValue("Base Directory");
            rk.Close();

            // Create extracted File to temp file name
            txtFileName = "ProbeLog_" + now.ToString("yyyyMMdd") + ".log"; // get file name with extension
            if (location.Length > 0)
            {
                txtFilePath = Base.ToString() + "\\" + location + "\\" + txtFileName; // get full path (same result as dlg.FileName)
            }
            else
            {
                txtFilePath = Base.ToString() + "\\log\\" + txtFileName; // get full path (same result as dlg.FileName)
            }
            //Creating MDI child form and initialize its fields
            MDIChildEx childForm = new MDIChildEx(this, txtFilePath, CHILDTYPE.ProbeLog);

            //childForm.Text = output;
            childForm.TabFileName = txtLocation + txtFileName;
            childForm.txtFilePath = txtFilePath;
            childForm.MdiParent = this;
            childForm.Type = CHILDTYPE.ProbeLog;

            //child Form will now hold a reference value to the tab control
            childForm.TabCtrl = tabControl1;

            //Add a Tabpage and enables it
            TabPage tp = new TabPage();
            tp.Parent = tabControl1;
            tp.Text = txtLocation + txtFileName;
            tp.Show();
            //child Form will now hold a reference value to a tabpage
            childForm.TabPag = tp;

            //Activate the MDI child form
            currentchild = childForm;
            childForm.Show();

            //Activate the newly created Tabpage
            tabControl1.SelectedTab = tp;
            SaveAs.Enabled = true;
            SaveAll.Enabled = false;

            childForm.richTextBox1.WordWrap = false;

            // Set current window as find window
            if (_FindDialog != null)
            {
                _FindDialog.childWindow = childForm;
            }
            UpdateMenuItems();

            Cursor.Current = Cursors.Default;
            tabControl1.Visible = true;
        }


        private void openGenericFile(string title, string filter, string parm, CHILDTYPE type)
        {
            string txtFileName;
            string txtFilePath;
            string tmpFilePath;
            MDIChildEx childForm=null;
            //string output = "";
            Process p;
            // Browse for File
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Filter = filter;
            fdlg.FilterIndex = 0;
            fdlg.RestoreDirectory = true;
            fdlg.Multiselect = true;
            fdlg.Title = title;

            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                if (type == CHILDTYPE.CheckRawFiles)
                {
                    //Creating MDI child form and initialize its fields
                    childForm = new MDIChildEx(this, "Check Raw Files", type);
                    childForm.WindowState = FormWindowState.Normal;
                }
                foreach (String file in fdlg.FileNames)
                {
                    // Create extracted File to temp file name
                    txtFileName = System.IO.Path.GetFileName(file); // get file name with extension
                    txtFilePath = System.IO.Path.GetFullPath(file); // get full path (same result as dlg.FileName)
                    if (type != CHILDTYPE.CheckRawFiles)
                    {
                    //Creating MDI child form and initialize its fields
                        childForm = new MDIChildEx(this, txtFilePath, type);
                    childForm.WindowState = FormWindowState.Normal;
                    }
                    switch (type)
                    {
                        case CHILDTYPE.ExtractFiles:
                            // Select Target Folder
                            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
                            DialogResult result = folderDlg.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                // redirect the output stream of the child process.
                                p = new Process();
                                p.StartInfo.CreateNoWindow = true;
                                p.StartInfo.UseShellExecute = false;
                                p.StartInfo.RedirectStandardOutput = true;
                                p.StartInfo.FileName = "dumprawdata.exe";
                                p.StartInfo.Arguments = " " + parm + "\"" + txtFilePath + "\"" + " -F " + "\"" + folderDlg.SelectedPath + "\"";
                                p.Start();
                                string output = p.StandardOutput.ReadToEnd();
                                p.WaitForExit();
                                childForm.richTextBox1.Text = output;
                            }
                            break;
                        case CHILDTYPE.ProbeLog:
                            //childForm.richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                            //childForm.richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                            break;
                        case CHILDTYPE.TextFile:
                            childForm.WindowState = FormWindowState.Normal;
                            childForm.richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                            break;
                        case CHILDTYPE.LogView:
                            //childForm.richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                            break;
                        case CHILDTYPE.BinaryFile:
                            // Redirect the output stream of the child process.
                            tmpFilePath = txtFilePath; 
                            tmpFilePath+=".tmp";
                            p = new Process();
                            p.StartInfo.CreateNoWindow = true;
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = false;
                            p.StartInfo.FileName = "cmd.exe";
                            p.StartInfo.Arguments =
                                "/c DumpRawData.exe  -h " + "\"" + txtFilePath + "\"" + " >" + "\"" + tmpFilePath + "\"";
                            p.Start();
                            p.WaitForExit();
                            childForm.richTextBox1.LoadFile(tmpFilePath, RichTextBoxStreamType.PlainText);
                            File.Delete(tmpFilePath);
                            break;
                        case CHILDTYPE.CheckRawFiles:
                            tmpFilePath = txtFilePath;
                            tmpFilePath += ".tmp";
                            p = new Process();
                            p.StartInfo.CreateNoWindow = true;
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = false;
                            p.StartInfo.FileName = "cmd.exe";
                            p.StartInfo.Arguments =
                                " /c DumpRawData.exe " + parm + " \"" + txtFilePath + "\"" + " >" + "\"" + tmpFilePath + "\"";
                            p.Start();
                            p.WaitForExit();
                            string temp = childForm.richTextBox1.Text;
                            childForm.richTextBox1.LoadFile(tmpFilePath, RichTextBoxStreamType.PlainText);
                            childForm.richTextBox1.Text = temp + childForm.richTextBox1.Text;
                            File.Delete(tmpFilePath);
                            break;
                        default:
                            tmpFilePath = txtFilePath;
                            tmpFilePath += ".tmp";
                            p = new Process();
                            p.StartInfo.CreateNoWindow = true;
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.RedirectStandardOutput = false;
                            p.StartInfo.FileName = "cmd.exe";
                            p.StartInfo.Arguments =
                                " /c DumpRawData.exe " + parm + " \"" + txtFilePath + "\"" + " >" + "\"" + tmpFilePath + "\"";
                            p.Start();
                            p.WaitForExit();
                            childForm.richTextBox1.LoadFile(tmpFilePath, RichTextBoxStreamType.PlainText);
                            File.Delete(tmpFilePath);
                            // Redirect the output stream of the child process.
                            //p = new Process();
                            //p.StartInfo.CreateNoWindow = true;
                            //p.StartInfo.UseShellExecute = false;
                            //p.StartInfo.RedirectStandardOutput = true;
                            //p.StartInfo.FileName = "DumpRawData.exe";
                            //p.StartInfo.Arguments = " " + parm + "\"" + txtFilePath + "\"";
                            //p.Start();
                            //output = p.StandardOutput.ReadToEnd();
                            //p.WaitForExit();
                            //childForm.richTextBox1.Text = output;
                            break;
                    }
   
                    if (type != CHILDTYPE.CheckRawFiles)
                    {
                    //childForm.Text = output;
                    childForm.TabFileName = txtFileName;
                    childForm.txtFilePath = txtFilePath;
                    childForm.MdiParent = this;
                    childForm.Type = type;

                    //child Form will now hold a reference value to the tab control
                    childForm.TabCtrl = tabControl1;
                    
                    //Add a Tabpage and enables it
                    TabPage tp = new TabPage();
                    tp.Parent = tabControl1;
                    tp.Text = txtFileName;
                    tp.Show();
                    //child Form will now hold a reference value to a tabpage
                    childForm.TabPag = tp;

                    //Activate the MDI child form
                    currentchild = childForm;
                    childForm.Show();

                    //Activate the newly created Tabpage
                    tabControl1.SelectedTab = tp;
                    SaveAs.Enabled = true;
                    SaveAll.Enabled = false;

                    childForm.richTextBox1.WordWrap = false;
                    childForm.CreateFlag = true;

                        // Set current window as find window
                        if (_FindDialog != null)
                        {
                            _FindDialog.childWindow = childForm;
                        }
                        UpdateMenuItems();
                    }
                }

                if (type == CHILDTYPE.CheckRawFiles)
                {
                    //childForm.Text = output;
                    childForm.TabFileName = "Check Raw Files";
                    childForm.txtFilePath = "";
                    childForm.MdiParent = this;
                    childForm.Type = type;

                    //child Form will now hold a reference value to the tab control
                    childForm.TabCtrl = tabControl1;

                    //Add a Tabpage and enables it
                    TabPage tp = new TabPage();
                    tp.Parent = tabControl1;
                    tp.Text = "Check Raw Files";
                    tp.Show();
                    //child Form will now hold a reference value to a tabpage
                    childForm.TabPag = tp;

                    //Activate the MDI child form
                    currentchild = childForm;
                    childForm.Show();

                    //Activate the newly created Tabpage
                    tabControl1.SelectedTab = tp;
                    SaveAs.Enabled = true;
                    SaveAll.Enabled = false;

                    childForm.richTextBox1.WordWrap = false;
                    childForm.CreateFlag = true;

                    // Set current window as find window
                    if (_FindDialog != null)
                    {
                        _FindDialog.childWindow = childForm;
                    }
                    UpdateMenuItems();
                }
                Cursor.Current = Cursors.Default;
                tabControl1.Visible = true;
            }
        }

        private void openListFiles()
        {
            string txtFileName = "";
            string txtFilePath = "";
            string output = "";
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            // Get path to log folder
            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            string Base = (string)rk.GetValue("Base Directory");
            rk.Close();
            txtFilePath = Base + "\\cnf";
            fbd.SelectedPath = txtFilePath;
            fbd.Description = "Select list file path:";
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                lastPath = fbd.SelectedPath;
                txtFilePath = fbd.SelectedPath;
                txtFileName = "Lists in " + txtFilePath;
            }

            // Redirect the output stream of the child process.
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "DumpRawData.exe";
            p.StartInfo.Arguments = " -a " + "\"" + txtFilePath + "\"";
            p.Start();
            output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            //Creating MDI child form and initialize its fields
            MDIChildEx childForm = new MDIChildEx(this, txtFilePath, CHILDTYPE.ListFiles);
            childForm.richTextBox1.Text = output;
            //childForm.Text = output;
            childForm.TabFileName = txtFileName;
            childForm.MdiParent = this;
            childForm.Type = CHILDTYPE.ListFiles;

            //child Form will now hold a reference value to the tab control
            childForm.TabCtrl = tabControl1;

            //Add a Tabpage and enables it
            TabPage tp = new TabPage();
            tp.Parent = tabControl1;
            tp.Text = txtFileName;
            tp.Show();
            //child Form will now hold a reference value to a tabpage
            childForm.TabPag = tp;

            //Activate the MDI child form
            currentchild = childForm;
            childForm.Show();

            //Activate the newly created Tabpage
            tabControl1.SelectedTab = tp;
            SaveAs.Enabled = true;
            SaveAll.Enabled = false;

            UpdateMenuItems();
        }

        private void openIndividualRawFile(string filename, CHILDTYPE type)
        {
            string txtFileName="";
            string txtFilePath="";
            string output ="";
            // Create extracted File to temp file name
            txtFileName = System.IO.Path.GetFileName(filename); // get file name with extension
            txtFilePath = System.IO.Path.GetFullPath(filename); // get full path (same result as dlg.FileName)
            if (type == CHILDTYPE.TextFile)
            {
                // build filename
                StreamReader sr = new StreamReader(txtFilePath);
                output = sr.ReadToEnd();
                sr.Close();
            }
            else
            {
                // Redirect the output stream of the child process.
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = "DumpRawData.exe";
                p.StartInfo.Arguments = " -f " + "\"" + txtFilePath + "\"";
                p.Start();
                output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            //Creating MDI child form and initialize its fields
            MDIChildEx childForm = new MDIChildEx(this, txtFilePath, type);
            childForm.richTextBox1.Text = output;
            //childForm.Text = output;
            childForm.TabFileName = txtFileName;
            childForm.MdiParent = this;
            childForm.Type = type;

            //child Form will now hold a reference value to the tab control
            childForm.TabCtrl = tabControl1;

            //Add a Tabpage and enables it
            TabPage tp = new TabPage();
            tp.Parent = tabControl1;
            tp.Text = txtFileName;
            tp.Show();
            //child Form will now hold a reference value to a tabpage
            childForm.TabPag = tp;

            //Activate the MDI child form
            currentchild = childForm;
            childForm.Show();

            //Activate the newly created Tabpage
            tabControl1.SelectedTab = tp;
            SaveAs.Enabled = true;
            SaveAll.Enabled = false;

            UpdateMenuItems();
        }

        private void findMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MDIChildEx childForm in this.MdiChildren)
            {
                //Check for its corresponding MDI child form
                if (childForm.TabPag.Equals(tabControl1.SelectedTab))
                {
                    //Activate the MDI child form
                    childForm.Select();
                    Find(tabControl1);
                    break;
                }
            }
        }

        private void Find(System.Windows.Forms.TabControl tabControl)
        {
            if (_FindDialog == null)
            {
                _FindDialog = new FindDialog();
                GetCurrentChild();
            }
            _FindDialog.Left = this.Left + 56;
            _FindDialog.Top = this.Top + 160;

            if (!_FindDialog.Visible)
                _FindDialog.Show(this);
            else
                _FindDialog.Show();
            _FindDialog.Triggered();

        }

        //private void FindThreadProc()
        //{
        //    _FindDialog = new FindDialog();
        //    GetCurrentChild();
        //    _FindDialog.Left = this.Left + 56;
        //    _FindDialog.Top = this.Top + 160;
        //    _FindDialog.ShowDialog();
        //}

        private void menuItemEditGoTo_Click(object sender, EventArgs e)
        {
            var GoToLinePrompt = new GoToLinePrompt(currentchild.LinePos+1);
            GoToLinePrompt.Left = Left + 5;
            GoToLinePrompt.Top = Top + 44;

            if (GoToLinePrompt.ShowDialog(this) != DialogResult.OK) return;

            var TargetLineIndex = GoToLinePrompt.LineNumber;

            currentchild.GotoLine(TargetLineIndex-1);
        }

        private void menuItemCut_Click(object sender, EventArgs e)
        {
            currentchild.richTextBox1.Cut();
        }

        private void menuItemCopy_Click(object sender, EventArgs e)
        {
            currentchild.richTextBox1.Copy();
        }

        private void menuItemPaste_Click(object sender, EventArgs e)
        {
            currentchild.richTextBox1.Paste();
        }

        private void menuItemDel_Click(object sender, EventArgs e)
        {
            if (currentchild.richTextBox1.SelectionLength == 0)
            {
                currentchild.richTextBox1.SelectionLength = 1;
            }

            currentchild.richTextBox1.SelectedText = "";
        }

        private void menuItemUndo_Click(object sender, EventArgs e)
        {
            currentchild.richTextBox1.Undo();
        }

        private void UpdateMenuItems()
        {
            bool bEnable = false;
            if (currentchild != null)
            {
                bEnable = true;
            }
            menuItemCut.Enabled = bEnable;
            menuItemCopy.Enabled = bEnable;
            menuItemPaste.Enabled = bEnable;
            menuItemUndo.Enabled = bEnable;
            menuItemEditGoTo.Enabled=bEnable;
            findMenuItem.Enabled = bEnable;
            fileClose.Enabled = bEnable;
            SaveAs.Enabled = bEnable;
            SaveAll.Enabled = bEnable;
            menuItemCloseAll.Enabled = bEnable;
            exitMenuItem.Enabled = true ;
            if (currentchild != null)
            {
                if (currentchild.Type == CHILDTYPE.ProbeLog)
                    editMenuItem.Enabled = false;
                else
                    editMenuItem.Enabled = true;
                this.printDocument.Enabled = true;
                this.printPreviewDocument.Enabled = true;
            }
            else
            {
                this.printDocument.Enabled = false;
                this.printPreviewDocument.Enabled = false;
            }
        }

        private void menuItemCloseAll_Click(object sender, EventArgs e)
        {
            foreach (MDIChildEx childForm in this.MdiChildren)
            {
                childForm.Close();
            }
        }

        private void saveAll_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = lastPath;
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                lastPath = fbd.SelectedPath;
                foreach (MDIChildEx child in this.MdiChildren)
                {
                    //Check for its corresponding MDI child form
                    // build filename
                    string FileName = fbd.SelectedPath + "\\" + child.TabPag.Text;
                    // Make sure it has a .txt extension

                    if (!FileName.ToLower().EndsWith(".txt"))
                        FileName += ".txt";
                    //FileName = FileName.Replace(".raw", ".txt");
                    using (StreamWriter sw = new StreamWriter(FileName))
                    {
                        foreach (string l in child.richTextBox1.Lines)
                            sw.WriteLine(l);
                    }
                }
            }
        }

        private void loadTextFile_Click(object sender, EventArgs e)
        {
            //Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|All Files|*.*
            openGenericFile("Select Text File",
                            "All File (*.*) |*.*|Text Files (*.txt)|*.txt|Ini Files(*.ini)|*.ini",
                            "",
                            CHILDTYPE.TextFile);
        }

        private void openBinaryFile_Click(object sender, EventArgs e)
        {
            openGenericFile("Select File",
                            "All files (*.*)|*.*|Bin Files (*.bin)|*.*",
                            "",
                            CHILDTYPE.BinaryFile);
        }

        private void openCurrentProbeLog_Click(object sender, EventArgs e)
        {
            // If GDS or locations not loaded
            if ((SystemType == 1) || (CurrentLocations.Count == 0))
                openCurrentProbeLogFile("", "");
            else
            {
                int i = 1;
                foreach (string loc in CurrentLocations)
                {
                    openCurrentProbeLogFile(loc, "L" + i.ToString() + "-");
                    i++;
                }
            }
        }

        private void openSelectedProbeLog_Click(object sender, EventArgs e)
        {
            openGenericFile("Select File",
                            "Log files (*.log)|*.log|All Files (*.*)|*.*",
                            "",
                            CHILDTYPE.ProbeLog);
        }

        private void extractProbingsFromRawFile_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Raw File",
                            "All files (*.*)|*.*|Raw Files (*.raw)|*.raw",
                            " -X ",
                            CHILDTYPE.ExtractFiles);
        }

        private void openGFIListFiles_Click(object sender, EventArgs e)
        {
            if (SystemType == 1)
                openListFiles();
        }

        private void openLogFile_Click(object sender, EventArgs e)
        {
            openGenericFile("Select File",
                            "Log files (*.stl)|*.stl|All Files (*.*)|*.*",
                            "",
                            CHILDTYPE.LogView);
        }

        private void printDocument_click(object sender, EventArgs e)
        {
            MdiPrint pdoc;
            GetCurrentChild();
            if (currentchild != null)
            {
                switch (currentchild.type)
                {
                    case CHILDTYPE.ProbeLog:
                        pdoc = new MdiPrint(currentchild.fileName, currentchild.dataGridView1);
                        break;
                    default:
                        pdoc = new MdiPrint(currentchild.fileName, currentchild.richTextBox1.Text);
                        break;
                }
                pdoc.PrintFile();
            }
        }

        private void printPreviewDocument_Click(object sender, EventArgs e)
        {
            MdiPrint pdoc;
            GetCurrentChild();
            if (currentchild != null)
            {
                switch (currentchild.type)
                {
                    case CHILDTYPE.ProbeLog:
                        pdoc = new MdiPrint(currentchild.fileName, currentchild.dataGridView1);
                        break;
                    default:
                        pdoc = new MdiPrint(currentchild.fileName, currentchild.richTextBox1.Text);
                        break;
                }
                pdoc.PrintPreview();
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {

            EncryptForm abc = new EncryptForm();
            abc.Show();
        }

        private void menuItem2_Click_1(object sender, EventArgs e)
        {
            openGenericFile("Select Zone Table File",
                            "All files (*.*)|*.*|Zone Table Files (*.bin)|*.bin",
                            " -b ",
                            CHILDTYPE.Rawfile);
        }

        private void loadACF_Click(object sender, EventArgs e)
        {
            //Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|All Files|*.*
            openGenericFile("Select Account Based ACF File",
                            "All File (*.*) |*.*|ACF Files (*.acf)|*.acf",
                            "-d",
                            CHILDTYPE.ACCOUNT_ACF);
        }

        private void LoadASL_Click(object sender, EventArgs e)
        {
            //Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt|Office Files|*.doc;*.xls;*.ppt|All Files|*.*
            openGenericFile("Select Account Based ASL File",
                            "All File (*.*) |*.*|ASL Files (*.bin)|*.bin",
                            "-c",
                            CHILDTYPE.ACCOUNT_ASL);
        }

        private void menuItem15_Click(object sender, EventArgs e)
        {
            openGenericFile("Select File",
                            "All files (*.*)|*.*",
                            " -V ",
                            CHILDTYPE.VAULT_FILE);
        }

        private void menuItem16_Click(object sender, EventArgs e)
        {
            openGenericFile("Select Raw File",
                            "All files (*.*)|*.*|Raw Files (*.raw)|*.raw",
                            " -e ",
                            CHILDTYPE.CheckRawFiles);
        }
    }
}
