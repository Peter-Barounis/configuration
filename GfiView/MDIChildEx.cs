﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Security.Permissions;
using System.Runtime.InteropServices; // DllImport
using Microsoft.Win32;

namespace MdiEdit
{
    
    public partial class MDIChildEx : System.Windows.Forms.Form
    {
        //bool reloadFlag = false;
        private TabControl tabCtrl;
        private TabPage tabPag;
        public string fileName;
        public string txtFilePath;
        public CHILDTYPE type;
        public MDIParent _parent;
        public int CurrentPosition;
        public int LinePos, ColPos;
        public int maxrows = 0;

        // Variables needed for search
        public int CurrentLine;
        public int FirstPos;
        public int LastPos;
        public string currTextLine;
        public string FindText;
        public Thread tUpdateStatusBar;

        // Misc variables
        public bool NoStatusBar;
        FileSystemWatcher ProbeLogWatcher;
        FileSystemWatcher LogFileWatcher;

        bool monitorStarted = false;
        RegistryMonitor monitor;
        CHILDTYPE SelectedView;
        string Base;
        int SystemType = 1;
        public bool CreateFlag = false;
        // Define the event handlers.
        delegate void SetUpdateStatusBar(MDIChildEx child);
        delegate void FileHasChanged(MDIChildEx child);
        delegate void SetChangedVaultStatus(MDIChildEx child);
        delegate void LogFileHasChanged(MDIChildEx child);


        private void OnLogViewChanged(object source, FileSystemEventArgs e)
        {
            LogFileHasChanged dFileHasChanged = new LogFileHasChanged(reloadLog);
            try
            {
                LogFileWatcher.EnableRaisingEvents = false;
                Thread.Sleep(500);
                this.Invoke(dFileHasChanged, new object[] { this });
                LogFileWatcher.EnableRaisingEvents = true;

            }
            catch
            {
            }
        }
        private void OnLogChanged(object source, FileSystemEventArgs e)
        {
            FileHasChanged dFileHasChanged = new FileHasChanged(reloadFile);
            try
            {
                this.Invoke(dFileHasChanged, new object[] { this });
            }
            catch
            {
            }
        }

        private void LoadCsvFile(string filename)
        {
            int i,j,rowsin,gridrows;
            Int32[] cbx = new Int32[4];
            Int32[] bin = new Int32[4];

            // Crete file if it does not exist
            if (!File.Exists(filename))
                File.Create(filename).Dispose();

            var reader = new StreamReader(File.OpenRead(filename));
            i = 0;
            rowsin = 0;
            //i = NativeMethods.IsApplicationRunning("notepad");
            for (i = 0; i < 4; i++)
            {
                j = NativeMethods.GetVltInfo(i, cbx, bin);
            }
            i = 0;
            while (!reader.EndOfStream)
            {
                var line    = reader.ReadLine();
                var values  = line.Split(',');

                // Is the probing line have the correct # of items?
                if (values.Count() >= maxrows)
                {
                    rowsin++;
                    gridrows = dataGridView1.RowCount;

                    // Expand # rows to accommodate the number of items
                    while (rowsin > dataGridView1.RowCount)
                        dataGridView1.Rows.Add(1);

                    // Load the data
                    j = 0;
                    dataGridView1.Rows[i].HeaderCell.Value = (i + 1).ToString();

                    foreach (string a in values)
                    {
                        if (j != 0)
                        {
                            dataGridView1.Rows[i].Cells[j - 1].Value = a.ToString();
                            if ((j == 7) && (a.ToString().Contains("Y")))
                                dataGridView1.Rows[i].Cells[j - 1].Style.BackColor = Color.Red;
                            else
                                dataGridView1.Rows[i].Cells[j - 1].Style.BackColor = Color.White;
                            if ((j == 8) && (Convert.ToInt32(a) >3999))
                                dataGridView1.Rows[i].Cells[j - 1].Style.BackColor = Color.Yellow;
                            else
                                dataGridView1.Rows[i].Cells[j - 1].Style.BackColor = Color.White;
                        }
                        j++;
                        if (j > maxrows)
                            break;
                    }
                    i++;
                }
            }
            reader.Close();
            while ((dataGridView1.RowCount>rowsin) && (rowsin>0))
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.RowCount - 1);
            }
        }

        private void reloadLog(MDIChildEx child)
        {
            if (this.type == CHILDTYPE.LogView)
            {
                richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
        }

        private void reloadFile(MDIChildEx child)
        {
            if (this.type == CHILDTYPE.ProbeLog)
            {
                LoadCsvFile(txtFilePath);
                dataGridView1.Visible = true;
                dataGridStatus.Visible = true;

            }
            else
            {
                dataGridView1.Visible = false;
                dataGridStatus.Visible = false;

                richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
            }
        }

        public MDIChildEx()
        {
            InitializeComponent();
            dataGridView1.Visible = false;
            dataGridStatus.Visible = false;
            monitorStarted = false;
            this.WindowState = FormWindowState.Normal;

        }

        public void SetupStatus()
        {
            int i;
            // Set status grid
            dataGridStatus.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridStatus.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridStatus.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            // Set the column headers
            for (i = 1; i <= 10; i++)
            {
                dataGridStatus.Columns.Add(i.ToString(), "Vault/Probe " + i.ToString());
                dataGridStatus.Columns[i-1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }


            foreach(DataGridViewColumn column in dataGridStatus.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.ReadOnly = true;
                column.Width = 100;
            }
            //dataGridStatus.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            // If this is a log file, then subscribe to the changes in the log
            // so we can redisplay the file
            dataGridStatus.Visible = true;

            // Set the row headers
            dataGridStatus.Rows.Add(1);
            dataGridStatus.Rows[0].HeaderCell.Value = "Bin";

            dataGridStatus.Rows.Add(1);
            dataGridStatus.Rows[1].HeaderCell.Value = "Cashbox";

            dataGridStatus.Rows.Add(1);
            dataGridStatus.Rows[2].HeaderCell.Value = "Vlt Status";

            dataGridStatus.Rows.Add(1);
            dataGridStatus.Rows[3].HeaderCell.Value = "Probe Status";
            redisplayVaultStatus(this);
            //Thread tUpdateProbeVaultStatus = new Thread(new ThreadStart(UpdateProbeVaultStatus));
            //tUpdateProbeVaultStatus.Start();

        }

        private void CheckVaultRegistry()
        {
            // Move the registry data to the Vault Status 
            for (int i = 0; i < 10; i++)
            {
                object Value;
                RegistryKey rk = Registry.LocalMachine.CreateSubKey(
                    "Software\\GFI Genfare\\Vaults\\" + String.Format("{0:D2}", i + 1));
                Value = rk.GetValue("Bin",null);
                if (Value == null)
                {
                    Value = 0;
                    rk.SetValue("Bin", Value, RegistryValueKind.DWord);
                }
                Value = rk.GetValue("Cashbox", null);
                if (Value == null)
                {
                    Value = 0;
                    rk.SetValue("Cashbox", Value, RegistryValueKind.DWord);
                } 
                Value = rk.GetValue("Vlt Status", null);
                if (Value == null)
                {
                    Value = "";
                    rk.SetValue("Vlt Status", Value, RegistryValueKind.String);
                } 
                Value = rk.GetValue("Probe Status", null);
                if (Value == null)
                {
                    Value = "";
                    rk.SetValue("Probe Status", Value, RegistryValueKind.String);
                }
                rk.Close();
            }

        }

        private void redisplayVaultStatus(MDIChildEx child)
        {
            string strLine;
            if (SystemType == 1)
            {
                // Move the registry data to the Vault Status 
                for (int i = 0; i < 10; i++)
                {
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey(
                        "Software\\GFI Genfare\\Vaults\\" + String.Format("{0:D2}", i + 1));
                    object Value = rk.GetValue("Bin");
                    if ((Int32)Value == 0)
                        dataGridStatus.Rows[0].Cells[i].Value = "[Out]";
                    else
                        dataGridStatus.Rows[0].Cells[i].Value = Value;
                    Value = rk.GetValue("Cashbox");
                    if ((Int32)Value == 0)
                        dataGridStatus.Rows[1].Cells[i].Value = "...";
                    else
                        dataGridStatus.Rows[1].Cells[i].Value = Value;
                    dataGridStatus.Rows[2].Cells[i].Value = rk.GetValue("Vlt Status");
                    dataGridStatus.Rows[3].Cells[i].Value = rk.GetValue("Probe Status");
                    rk.Close();
                }

                // If this is a GC, then export the status to a csv file which
                // will be imported by the NM for status display
                string strCsvFile = Base + "\\log\\" + "vltstatus.csv";
                StreamWriter sw = new StreamWriter(strCsvFile);
                for (int j = 0; j < 4; j++)
                {
                    strLine = "";
                    // Build Line
                    for (int i = 0; i < 10; i++)
                    {
                        if (i != 0)
                            strLine += ",";
                        strLine += dataGridStatus.Rows[j].Cells[i].Value.ToString();
                    }
                    strLine += "\n";
                    sw.WriteLine(strLine);
                }
                sw.Close();
            }
        }


        private void OnVaultRegChanged(object sender, EventArgs e)
        {
            SetChangedVaultStatus dVaultStatusHasChanged = new SetChangedVaultStatus(redisplayVaultStatus);
            try
            {
                this.Invoke(dVaultStatusHasChanged, new object[] { this });
            }
            catch
            {
            }

        }

        public void MonitorVaults()
        {
            monitorStarted = true;
            monitor = new RegistryMonitor(RegistryHive.LocalMachine, "Software\\GFI Genfare\\Vaults");

            monitor.RegChanged += new EventHandler(OnVaultRegChanged);
            monitor.Start();
        }

        
        public MDIChildEx(MDIParent parent, string fname, CHILDTYPE ftype)
        {
            // Required for Windows Form Designer support
            LinePos = 0;
            ColPos = 0;
            _parent = parent;
            type = ftype;
            InitializeComponent();
            Text = fname;
            this.txtFilePath = fname;
            richTextBox1.WordWrap = false;
            SystemType = _parent.SystemType;
            // Get the base path in case it is needed
            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\GFI Genfare\\Global\\");
            object obj = rk.GetValue("Base Directory");
            if (obj != null)
                Base = (string)obj;
            else
                Base = "C:\\gfi";
            rk.Close();
            this.WindowState = FormWindowState.Normal;
            switch (this.type)
            {
                case CHILDTYPE.ProbeLog:
                    // CHeck if registry is populated only on GC
                    if (SystemType == 1)
                    {
                        CheckVaultRegistry();
                        // Monitor Vaults in Registry
                        MonitorVaults();
                    }
                    // Setup Status worksheet
                    SetupStatus();

                    // Columkn Header Values
                    //dataGridView1.Columns.Add("ID", "ID");
                    dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dataGridView1.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dataGridView1.Columns.Add("ProbeID", "Probe Number");
                    dataGridView1.Columns.Add("Bus", "Bus");
                    dataGridView1.Columns.Add("Events", "Event Count");
                    dataGridView1.Columns.Add("Revenue", "Revenue (Cumulative)");
                    dataGridView1.Columns.Add("CurrTime", "Timestamp");
                    dataGridView1.Columns.Add("FBXTime", "Farebox Timestamp");
                    dataGridView1.Columns.Add("Discrepency", "Time Discrepency");
                    dataGridView1.Columns.Add("CBXID", "CBX ID");
                    dataGridView1.Columns.Add("FBXID", "FBX ID");
                    dataGridView1.Columns.Add("FBXVer", "FBX Ver.");
                    dataGridView1.Columns.Add("COIN_C", "Coin Counts (Cumulative)");
                    dataGridView1.Columns.Add("BILL_C", "Bill Counts (Cumulative)");
                    dataGridView1.Columns.Add("WARM", "Warm Start");
                    dataGridView1.Columns.Add("COLD", "Cold Start");
                    //dataGridView1.Columns[13].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    maxrows = 14;
                    // Don't let the user sort the columns
                    foreach (DataGridViewColumn column in dataGridView1.Columns)
                    {
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                        column.ReadOnly = true;
                    }

                    // If this is a log file, then subscribe to the changes in the log
                    // so we can redisplay the file
                    NoStatusBar = true;
                    dataGridView1.Visible = true;

                    //childForm.richTextBox1.LoadFile(txtFilePath, RichTextBoxStreamType.PlainText);
                    ProbeLogWatcher = new FileSystemWatcher();
                    ProbeLogWatcher.Path = System.IO.Path.GetDirectoryName(fname);
                    ProbeLogWatcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastAccess | NotifyFilters.LastWrite
                               | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime;
                    ProbeLogWatcher.Filter = System.IO.Path.GetFileName(fname); // "ProbeLog_20160509";
                    ProbeLogWatcher.Changed += new FileSystemEventHandler(OnLogChanged);
                    //ProbeLogWatcher.Changed += new FileSystemEventHandler(OnLogChanged);
                    // Begin watching.
                    ProbeLogWatcher.EnableRaisingEvents = true;
                    reloadFile(this);
                    break;
                case CHILDTYPE.LogView:
                    LogFileWatcher = new FileSystemWatcher();
                    LogFileWatcher.Path = System.IO.Path.GetDirectoryName(fname);
                    //LogFileWatcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastAccess | NotifyFilters.LastWrite
                    //           | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime;
                    LogFileWatcher.NotifyFilter = NotifyFilters.Size;// | NotifyFilters.LastAccess | NotifyFilters.LastWrite
                               //| NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime;
                    LogFileWatcher.Filter = System.IO.Path.GetFileName(fname); // "ProbeLog_20160509";
                    LogFileWatcher.Changed += new FileSystemEventHandler(OnLogViewChanged);
                    //LogFileWatcher.Changed += new FileSystemEventHandler(OnLogViewChanged);
                    // Begin watching.
                    LogFileWatcher.EnableRaisingEvents = true;
                    reloadLog(this);
                    break;
                default:
                    NoStatusBar = false;
                    dataGridView1.Visible = false;
                    dataGridStatus.Visible = false;
                    break;
            }


            this.Font = new Font("Courier New", 8);
            FindText = "";

        }

        public string text
        {
            get
            {
               return richTextBox1.Text;
            }
            set
            {
               richTextBox1.Text = value;
            }
        }
        public TabControl TabCtrl
        {
            set
            {
                tabCtrl = value;
            }
        }

        public string TabFileName
        {
            set
            {
                fileName = value;
            }
        }

        public TabPage TabPag
        {
            get
            {
                return tabPag;
            }
            set
            {
                tabPag = value;
            }
        }

        public CHILDTYPE Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public void UpdateStatusBar()
        {
            if (!NoStatusBar)
            {

                Thread tUpdateStatusBar = new Thread(new ThreadStart(UpdateStatusBarThread));
                tUpdateStatusBar.Start();
            }
        }

        //public void UpdateProbeVaultStatus()
        //{
        //    while (running)
        //    {
        //        dataGridStatus.Rows[0].Cells[1].Value = "aaaaaaa";
        //        Thread.Sleep(200);
        //    }
        //}

        public void UpdateStatusBarThread()
        {
            try
            {
                SetUpdateStatusBar dUpdateStatusBar = new SetUpdateStatusBar(UpdateCaretPosFromThread);
                this.Invoke(dUpdateStatusBar, new object[] { this });
            }
            catch
            {
            }
        }

        private void UpdateCaretPosFromThread(MDIChildEx child)
        {
            Point pt;
            int index;

            // get the current line 
            index = child.richTextBox1.SelectionStart;
            child.LinePos = child.richTextBox1.GetLineFromCharIndex(index);
            // get the caret position in pixel coordinates 
            pt = child.richTextBox1.GetPositionFromCharIndex(index);
            // now get the character index at the start of the line, and 
            // subtract from the current index to get the column 
            pt.X = 0;
            child.ColPos = index - child.richTextBox1.GetCharIndexFromPosition(pt);
            if (child._parent.controlCaretPositionLabel.Tag == null)
            {
                child._parent.controlCaretPositionLabel.Tag = child._parent.controlCaretPositionLabel.Text;
            }
            child._parent.controlCaretPositionLabel.Text = String.Format("Ln {0}, Col {1}", child.LinePos + 1, child.ColPos + 1);
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        private void MDIChild_Activated(object sender, System.EventArgs e)
        {
            //Activate the corresponding Tabpage
            tabCtrl.SelectedTab = tabPag;
            if (CreateFlag)
            {
                this.WindowState = FormWindowState.Normal;
                CreateFlag = false;
            }
           
            if (!tabCtrl.Visible)
            {
                tabCtrl.Visible = true;
            }

            UpdateStatusBar();

            // If this is a log file, then subscribe to the changes in the log
            // so we can redisplay the file
            if (this.type == CHILDTYPE.ProbeLog)
            {
                NoStatusBar = true;
                dataGridView1.Visible = true;
                dataGridStatus.Visible = true;
            }
            else
            {
                NoStatusBar = false;
                dataGridView1.Visible = false;
                dataGridStatus.Visible = false;
            }
        }

        private void OnLeave(object sender, EventArgs e)
        {
            _parent.controlCaretPositionLabel.Text = "";
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (monitorStarted)
                monitor.Stop();
            TabPag.Dispose();
        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            CurrentPosition = richTextBox1.SelectionStart;
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            // Check if Right Click - Select menu
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                rightclickMenu.Show(PointToScreen(e.Location));
            else
                if (e.Button == MouseButtons.Left)
                    UpdateStatusBar();
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            UpdateStatusBar();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            UpdateStatusBar();
        }

        public void GotoLine(int LinePos)
        {
            int end;
            int index = richTextBox1.GetFirstCharIndexFromLine(LinePos);

            end = richTextBox1.Text.IndexOf('\n', index + 1);
            if (index < 0)
            {
                index = richTextBox1.Text.Length;
                LinePos = richTextBox1.GetLineFromCharIndex(index);
                index   = richTextBox1.GetFirstCharIndexFromLine(LinePos);
            }
            richTextBox1.Select(index, end - index);
            UpdateStatusBar();
        }

        private void richTextBox1_DoubleClick(object sender, EventArgs e)
        {
            int lineno;
            if (this.Type == CHILDTYPE.FIND)
            {
                int start = richTextBox1.GetFirstCharIndexFromLine(LinePos);
                //int pos = lineStart = richTextBox1.SelectionStart;
                int end = richTextBox1.Text.IndexOf('\n', start + 1);
                if (end < 0)
                    end = 0;
                // Make sure lenght > 0
                if (end - start <= 0) 
                    return;
                // Get line of text
                string strText = richTextBox1.Text.Substring(start, end - start);

                // Check if the length of the string is too small
                if (strText.Length < 5)
                    return;

                // Get the line number to goto                
                start = strText.IndexOf(":", 0) + 1;
                end = strText.IndexOf(" ", 0);

                // Check the length of the line number 
                if (end - start <= 0)
                    return;

                // Extract Line Number
                if (!Int32.TryParse(strText.Substring(start, end-start).Trim(), out lineno))
                    return;
                // Position to start of file name
                start = end;
                end = strText.IndexOf(":", start + 1);

                // Get the file (window) name
                if (start >0 && start <=end)
                     strText = strText.Substring(start+1, end-start-1);
                else
                    return;
                foreach (MDIChildEx childForm in _parent.MdiChildren)
                {
                    if (string.Compare(childForm.fileName, strText) == 0)
                    {
                        childForm.Select();
                        if (lineno > 0)
                        {
                            childForm.GotoLine(lineno-1);
                        }
                        break;
                    }
                }
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Type == CHILDTYPE.ProbeLog)
            {
                if (SelectedView == CHILDTYPE.StatusView)
                {
                    if (dataGridStatus.GetCellCount(DataGridViewElementStates.Selected) > 0)
                    {
                        try
                        {
                            // Add the selection to the clipboard.
                            Clipboard.SetDataObject(dataGridStatus.GetClipboardContent());
                        }
                        catch (System.Runtime.InteropServices.ExternalException)
                        {
                        }
                    }
                }
                else
                {
                    if (dataGridView1.GetCellCount(DataGridViewElementStates.Selected) > 0)
                    {
                        try
                        {
                            // Add the selection to the clipboard.
                            Clipboard.SetDataObject(dataGridView1.GetClipboardContent());
                        }
                        catch (System.Runtime.InteropServices.ExternalException)
                        {
                        }
                    }
                }
            }
            else
            {
                richTextBox1.Copy();
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {

            richTextBox1.Paste();
        }

        private void selectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        class NativeMethods
        {
            [DllImport("gfipb32.dll",
               SetLastError = false, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
            public static extern int IsApplicationRunning(string appname);

            [DllImport("gfipb32.dll",
               SetLastError = false, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
            public static extern int GetVltInfo(int vlt_no, Int32[] cbx, Int32[] bin);
        }

        private void dataGridStatus_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelectedView = CHILDTYPE.StatusView;
            // Check if Right Click - Select menu
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                rightclickMenu.Show(PointToScreen(e.Location));
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelectedView = CHILDTYPE.LogView;
            // Check if Right Click - Select menu
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                rightclickMenu.Show(PointToScreen(e.Location));
        }
     }
}
