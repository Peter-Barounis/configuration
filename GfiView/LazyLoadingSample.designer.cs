﻿namespace MdiEdit
{
    partial class LazyLoadingSample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LazyLoadingSample));
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.ms = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miSave = new System.Windows.Forms.ToolStripMenuItem();
            this.printLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.pauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collapseAllFoldingBlocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expandAllCollapsedBlocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.removeEmptyLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.enableWordWrapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.fctb = new FastColoredTextBoxNS.FastColoredTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.openLogFile = new System.Windows.Forms.ToolStripButton();
            this.saveLogFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.printLogFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.findInLog = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.stopLoadingLog = new System.Windows.Forms.ToolStripButton();
            this.continueLoadingLog = new System.Windows.Forms.ToolStripButton();
            this.ms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fctb)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofd
            // 
            this.ofd.DefaultExt = "txt";
            this.ofd.Filter = "Text file|*.txt|All files|*.*";
            // 
            // ms
            // 
            this.ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.ms.Location = new System.Drawing.Point(0, 0);
            this.ms.Name = "ms";
            this.ms.Size = new System.Drawing.Size(647, 24);
            this.ms.TabIndex = 1;
            this.ms.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpen,
            this.miSave,
            this.printLogFileToolStripMenuItem,
            this.closeFileToolStripMenuItem,
            this.toolStripSeparator4,
            this.refreshToolStripMenuItem,
            this.toolStripSeparator5,
            this.pauseToolStripMenuItem,
            this.restartToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // miOpen
            // 
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(152, 22);
            this.miOpen.Text = "Open Log File";
            this.miOpen.Click += new System.EventHandler(this.miOpen_Click);
            // 
            // miSave
            // 
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(152, 22);
            this.miSave.Text = "Save Log File";
            this.miSave.Click += new System.EventHandler(this.miSave_Click);
            // 
            // printLogFileToolStripMenuItem
            // 
            this.printLogFileToolStripMenuItem.Name = "printLogFileToolStripMenuItem";
            this.printLogFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printLogFileToolStripMenuItem.Text = "Print Log File";
            this.printLogFileToolStripMenuItem.Click += new System.EventHandler(this.printLogFileToolStripMenuItem_Click);
            // 
            // closeFileToolStripMenuItem
            // 
            this.closeFileToolStripMenuItem.Name = "closeFileToolStripMenuItem";
            this.closeFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeFileToolStripMenuItem.Text = "Close Log File";
            this.closeFileToolStripMenuItem.Click += new System.EventHandler(this.closeFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(149, 6);
            // 
            // pauseToolStripMenuItem
            // 
            this.pauseToolStripMenuItem.Name = "pauseToolStripMenuItem";
            this.pauseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pauseToolStripMenuItem.Text = "Pause";
            this.pauseToolStripMenuItem.Click += new System.EventHandler(this.pauseToolStripMenuItem_Click);
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            this.restartToolStripMenuItem.Click += new System.EventHandler(this.restartToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collapseAllFoldingBlocksToolStripMenuItem,
            this.expandAllCollapsedBlocksToolStripMenuItem,
            this.toolStripSeparator6,
            this.removeEmptyLinesToolStripMenuItem,
            this.toolStripSeparator7,
            this.enableWordWrapToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // collapseAllFoldingBlocksToolStripMenuItem
            // 
            this.collapseAllFoldingBlocksToolStripMenuItem.Name = "collapseAllFoldingBlocksToolStripMenuItem";
            this.collapseAllFoldingBlocksToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.collapseAllFoldingBlocksToolStripMenuItem.Text = "Collapse all folding blocks";
            this.collapseAllFoldingBlocksToolStripMenuItem.Click += new System.EventHandler(this.collapseAllFoldingBlocksToolStripMenuItem_Click);
            // 
            // expandAllCollapsedBlocksToolStripMenuItem
            // 
            this.expandAllCollapsedBlocksToolStripMenuItem.Name = "expandAllCollapsedBlocksToolStripMenuItem";
            this.expandAllCollapsedBlocksToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.expandAllCollapsedBlocksToolStripMenuItem.Text = "Expand all collapsed blocks";
            this.expandAllCollapsedBlocksToolStripMenuItem.Click += new System.EventHandler(this.expandAllCollapsedBlocksToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(214, 6);
            // 
            // removeEmptyLinesToolStripMenuItem
            // 
            this.removeEmptyLinesToolStripMenuItem.Name = "removeEmptyLinesToolStripMenuItem";
            this.removeEmptyLinesToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.removeEmptyLinesToolStripMenuItem.Text = "Remove empty lines";
            this.removeEmptyLinesToolStripMenuItem.Click += new System.EventHandler(this.removeEmptyLinesToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(214, 6);
            // 
            // enableWordWrapToolStripMenuItem
            // 
            this.enableWordWrapToolStripMenuItem.Name = "enableWordWrapToolStripMenuItem";
            this.enableWordWrapToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.enableWordWrapToolStripMenuItem.Text = "Enable WordWrap";
            this.enableWordWrapToolStripMenuItem.Click += new System.EventHandler(this.enableWordWrapToolStripMenuItem_Click);
            // 
            // sfd
            // 
            this.sfd.DefaultExt = "txt";
            this.sfd.Filter = "Text file|*.txt|All files|*.*";
            // 
            // fctb
            // 
            this.fctb.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.fctb.AutoScrollMinSize = new System.Drawing.Size(25, 15);
            this.fctb.BackBrush = null;
            this.fctb.CharHeight = 15;
            this.fctb.CharWidth = 7;
            this.fctb.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fctb.DelayedEventsInterval = 300;
            this.fctb.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fctb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fctb.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.fctb.IsReplaceMode = false;
            this.fctb.Location = new System.Drawing.Point(0, 49);
            this.fctb.Name = "fctb";
            this.fctb.Paddings = new System.Windows.Forms.Padding(0);
            this.fctb.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fctb.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("fctb.ServiceColors")));
            this.fctb.Size = new System.Drawing.Size(647, 288);
            this.fctb.TabIndex = 0;
            this.fctb.Zoom = 100;
            this.fctb.TextChangedDelayed += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.fctb_TextChangedDelayed);
            this.fctb.VisibleRangeChangedDelayed += new System.EventHandler(this.fctb_VisibleRangeChangedDelayed);
            // 
            // timer1
            // 
            this.timer1.Interval = 4000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openLogFile,
            this.saveLogFile,
            this.toolStripSeparator2,
            this.printLogFile,
            this.toolStripSeparator1,
            this.findInLog,
            this.toolStripSeparator3,
            this.stopLoadingLog,
            this.continueLoadingLog});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(647, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // openLogFile
            // 
            this.openLogFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openLogFile.Image = ((System.Drawing.Image)(resources.GetObject("openLogFile.Image")));
            this.openLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openLogFile.Name = "openLogFile";
            this.openLogFile.Size = new System.Drawing.Size(23, 22);
            this.openLogFile.Text = "Open Log File";
            this.openLogFile.Click += new System.EventHandler(this.miOpen_Click);
            // 
            // saveLogFile
            // 
            this.saveLogFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveLogFile.Image = ((System.Drawing.Image)(resources.GetObject("saveLogFile.Image")));
            this.saveLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveLogFile.Name = "saveLogFile";
            this.saveLogFile.Size = new System.Drawing.Size(23, 22);
            this.saveLogFile.Text = "Save Log File";
            this.saveLogFile.Click += new System.EventHandler(this.miSave_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // printLogFile
            // 
            this.printLogFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printLogFile.Image = ((System.Drawing.Image)(resources.GetObject("printLogFile.Image")));
            this.printLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printLogFile.Name = "printLogFile";
            this.printLogFile.Size = new System.Drawing.Size(23, 22);
            this.printLogFile.Text = "Print log";
            this.printLogFile.Click += new System.EventHandler(this.printLogFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // findInLog
            // 
            this.findInLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.findInLog.Image = global::gfiview.Properties.Resources.Search;
            this.findInLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.findInLog.Name = "findInLog";
            this.findInLog.Size = new System.Drawing.Size(23, 22);
            this.findInLog.Text = "toolStripButton6";
            this.findInLog.ToolTipText = "Search log file";
            this.findInLog.Click += new System.EventHandler(this.searchLog);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // stopLoadingLog
            // 
            this.stopLoadingLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopLoadingLog.Image = global::gfiview.Properties.Resources.multimediaPauselog1;
            this.stopLoadingLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopLoadingLog.Name = "stopLoadingLog";
            this.stopLoadingLog.Size = new System.Drawing.Size(23, 22);
            this.stopLoadingLog.Text = "Pause the log";
            this.stopLoadingLog.Click += new System.EventHandler(this.pauseToolStripMenuItem_Click);
            // 
            // continueLoadingLog
            // 
            this.continueLoadingLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.continueLoadingLog.Image = ((System.Drawing.Image)(resources.GetObject("continueLoadingLog.Image")));
            this.continueLoadingLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.continueLoadingLog.Name = "continueLoadingLog";
            this.continueLoadingLog.Size = new System.Drawing.Size(23, 22);
            this.continueLoadingLog.Text = "Start Log";
            this.continueLoadingLog.Click += new System.EventHandler(this.restartToolStripMenuItem_Click);
            // 
            // LazyLoadingSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 337);
            this.Controls.Add(this.fctb);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.ms);
            this.MainMenuStrip = this.ms;
            this.Name = "LazyLoadingSample";
            this.Text = "Genfare Quick Log Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LazyLoadingSample_FormClosing);
            this.ms.ResumeLayout(false);
            this.ms.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fctb)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FastColoredTextBoxNS.FastColoredTextBox fctb;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.MenuStrip ms;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.ToolStripMenuItem miSave;
        private System.Windows.Forms.ToolStripMenuItem closeFileToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collapseAllFoldingBlocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expandAllCollapsedBlocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeEmptyLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem pauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableWordWrapToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openLogFile;
        private System.Windows.Forms.ToolStripButton saveLogFile;
        private System.Windows.Forms.ToolStripButton printLogFile;
        private System.Windows.Forms.ToolStripButton continueLoadingLog;
        private System.Windows.Forms.ToolStripButton stopLoadingLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton findInLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem printLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
    }
}