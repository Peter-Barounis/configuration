﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Windows.Forms;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace MdiEdit
{
    public partial class EncryptForm : Form
    {
        [DllImport("gfipb32")]
        private unsafe static extern int Encrypt(byte[] dst, byte[] src, int max);

        [DllImport("gfipb32")]
        private unsafe static extern int Decrypt(byte[] dst, byte[] src, int max);

        public EncryptForm()
        {
            InitializeComponent();
        }


        private void encrypt_Exit(object sender, EventArgs e)
        {
            this.Hide();

        }
        private void decryptButton_Click(object sender, EventArgs e)
        {
            textBoxEncryptString.Text = DecryptString(textBoxEncryptedString.Text);
        }

        private void btn_Encrypt(object sender, EventArgs e)
        {
            //int i;
            //byte[] buffer1 = new byte[128];
            //byte[] buffer2 = new byte[128];
            //buffer2 = Encoding.ASCII.GetBytes(this.textBoxEncryptString.Text);
            //Encrypt(buffer1, buffer2, 33);
            //Array.Resize(ref buffer1, 33);
            //this.textBoxEncryptedString.Text = "\\x" + BitConverter.ToString(buffer1).Replace("-", "\\x");
            textBoxEncryptedString.Text = EncryptString(textBoxEncryptString.Text);
        }

        public string DecryptString(string src)
        {
            int j;
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            buffer2 = Encoding.ASCII.GetBytes(src);
            src = src.PadRight(33);

            if (src.Trim().Length > 0)
            {
                Decrypt(buffer1, buffer2, 32);
                for (j = 0; j < 33; j++)
                {
                    if (buffer1[j] == 0x00)
                        break;
                }
                Array.Resize(ref buffer1, j);
                return System.Text.Encoding.Default.GetString(buffer1).Trim();
            }
            else
                return ("");
        }

        public string EncryptString(string src)//, ref string str)
        {
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            src = src.PadRight(33);
            buffer2 = Encoding.ASCII.GetBytes(src);
            Encrypt(buffer1, buffer2, 32);
            Array.Resize(ref buffer1, 32);
            var table = (Encoding.Default.GetString(
                             buffer1,
                             0,
                             src.Trim().Length)).Split(new string[] { "\r\n", "\r", "\n" },
                             StringSplitOptions.None);
            //str = table[0];
            return "\\x" + BitConverter.ToString(buffer1).Replace("-", "\\x");
        }
    }
}
