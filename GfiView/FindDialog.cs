﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace MdiEdit
{
    public partial class FindDialog : Form
    {
        public MDIChildEx childWindow;
        private string strLine = "";
        public Thread tFindAllInAllDocThread;
        public Thread tFindAllInDocThread;
        public bool bCancel;

        delegate int SetFindInForm(MDIChildEx child, string pSearchText, bool pMatchCase, bool pSearchDown, int position, bool bShow);
        delegate void SetInitializeProgressControl(MDIChildEx child);
        delegate void SetUpdateFindWindowText(MDIChildEx findWindow, string text, bool bDone);
        delegate void SetUpdateProgressBar1(MDIChildEx child, MDIChildEx findWindow, int maxWindow);
        delegate void SetUpdateProgressBar2(MDIChildEx child, MDIChildEx findWindow);
        delegate void SetClearProgressBar();

 
        public FindDialog()
        {
            InitializeComponent();
            KeyPreview = true;
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(KeyEvent);
            bCancel = false;
        }
        private void KeyEvent(object sender, KeyEventArgs e) //Keyup Event 
        {
            if (e.KeyCode == Keys.F3)
            {
               buttonFindNextValue();
            }
            if (e.KeyCode == Keys.F6)
            {
                MessageBox.Show("Function F6");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (bCancel == true)
            {
                bCancel = false;
                Hide();
            }
            else
            {
                bCancel = true;
                Hide();
            }
        }

        private void FindDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void controlTextBox_TextChanged(object sender, EventArgs e)
        {
            UpdateFindNextButton();
        }

        private void UpdateFindNextButton()
        {
            buttonFindNext.Enabled = controlTextBox.Text.Length > 0;
            buttonFindAllInAllDoc.Enabled = controlTextBox.Text.Length > 0;
            buttonFindAllinCurrentDoc.Enabled = controlTextBox.Text.Length > 0;
        }

        private void FindDialog_Load(object sender, EventArgs e)
        {
            UpdateFindNextButton();
        }
        private void buttonFindNextValue()
        {
            var SearchText = controlTextBox.Text;
            var MatchCase = controlMatchCaseCheckbox.Checked;
            var bSearchDown = controlDownRadioButton.Checked;
            childWindow.CurrentPosition = FindAndSelect(childWindow, SearchText, MatchCase, bSearchDown, childWindow.CurrentPosition, true);
            bCancel = false;

            if (childWindow.CurrentPosition < 0)
            {
                MessageBox.Show(this, "Cannot find: '" + SearchText + "'");
            }
            else
            {
                if (bSearchDown)
                    childWindow.CurrentPosition += SearchText.Length;
                else
                    childWindow.CurrentPosition -= SearchText.Length;
                if (childWindow.CurrentPosition < 0)
                    childWindow.CurrentPosition = 0;
            }
        }

        private void buttonFindNext_Click(object sender, EventArgs e)
        {
            buttonFindNextValue();
        }

        public int FindAndSelect(MDIChildEx child, string pSearchText, bool pMatchCase, bool pSearchDown, int position, bool bShow)
        {
            int Index = -1;
            childWindow = child;
            if (child.richTextBox1.Text.Length == 0)
            {
                return -1;
            }
            RichTextBoxFinds options=0;
            child.Select();
            if (pMatchCase)
                options |= RichTextBoxFinds.MatchCase;
            if (position < 0)
                position = 0;
            if (!pSearchDown)
            {
                options |= RichTextBoxFinds.Reverse;
                Index = child.richTextBox1.Find(pSearchText, 0, position, options);
            }
            else
                Index = child.richTextBox1.Find(pSearchText, position, options);

            child.CurrentPosition = Index;
            if (Index >= 0)
            {
                child.CurrentLine = child.richTextBox1.GetLineFromCharIndex(child.CurrentPosition);
                child.FirstPos = child.richTextBox1.GetFirstCharIndexFromLine(child.CurrentLine);
                child.LastPos = child.richTextBox1.Text.IndexOf('\n', child.CurrentPosition + 1);
                child.currTextLine = child.richTextBox1.Text.Substring(child.FirstPos, child.LastPos - child.FirstPos);
            }
            else
            {
                return Index;
            }
            return Index;
        }

        public void Triggered()
        {
            controlTextBox.Focus();
        }

        private void controlTextBox_Enter(object sender, EventArgs e)
        {
            var Sender = (TextBox)sender;
            Sender.SelectAll();
        }

        public new void Show(IWin32Window window)
        {
            controlTextBox.Focus();
            controlTextBox.SelectAll();

            if (window == null)
                base.Show();
            else
                base.Show(window);
        }

        private void buttonFindAllInDocument(MDIChildEx child, MDIChildEx FindWindow)
        {
            WindowList wList = new WindowList();
            wList.CurrentWindow = child;
            wList.FindWindow = FindWindow;
            wList.SearchText = controlTextBox.Text;
            wList.MatchCase = this.controlMatchCaseCheckbox.Checked;
            wList.bSearchDown = this.controlDownRadioButton.Checked;
            tFindAllInDocThread = new Thread(FindAllInDocThread);
            tFindAllInDocThread.Start(wList);
        }

        private void buttonFindAllinCurrentDoc_Click(object sender, EventArgs e)
        {
            bCancel = false;
            Cursor.Current = Cursors.WaitCursor;
            MDIChildEx saveChild = childWindow;
            MDIChildEx FindWindow = childWindow._parent.CreateNewWindow("Find", "Find ALL in "+childWindow.fileName, "", CHILDTYPE.FIND);
            buttonFindAllInDocument(saveChild, FindWindow);
            Cursor.Current = Cursors.Default;
        }

        private void FindAllInDocThread(object wList)
        {
            try
            {
                MDIChildEx child = ((WindowList)wList).CurrentWindow;
                MDIChildEx FindWindow = ((WindowList)wList).FindWindow;
                string currTextLine = "";
                int nLinesFound = 0;
                bool MatchCase = ((WindowList)wList).MatchCase;
                bool bSearchDown = ((WindowList)wList).bSearchDown;
                string SearchText = ((WindowList)wList).SearchText;

                strLine = "";
                child.CurrentPosition = 0;

                SetFindInForm dFindAndSelect = new SetFindInForm(FindAndSelect);
                SetInitializeProgressControl pgInit = new SetInitializeProgressControl(InitializeProgressControl);
                SetUpdateProgressBar2 pgUpdate = new SetUpdateProgressBar2(UpdateProgressBar2);
                SetUpdateFindWindowText fwText = new SetUpdateFindWindowText(UpdateFindWindowText);

                string strText = "Find " + "\"" + SearchText + "\" in " + child.fileName + "\r\n";
                this.Invoke(fwText, new object[] { FindWindow, strText, true });
                this.Invoke(dFindAndSelect, new object[] { child, SearchText, MatchCase, bSearchDown, child.CurrentPosition, false });
                strText = "";

                if ((child.CurrentPosition >= 0) && (!bCancel))
                {
                    this.Invoke(pgInit, new object[] { child });

                    while ((child.CurrentPosition > 0) && (!bCancel))
                    {
                        nLinesFound++;
                        if (child.LastPos < 0)
                            child.LastPos = 0;
                        currTextLine = child.currTextLine;
                        strLine = String.Format("{0:D5}:{1:D3} {2}: ",
                                nLinesFound, child.CurrentLine + 1, child.fileName);
                        strText = strLine.ToString();
                        strText += currTextLine;
                        strText += "\r\n";
                        FindWindow.FindText += strText;
                        if (bSearchDown)
                            child.CurrentPosition += SearchText.Length;
                        else
                            child.CurrentPosition -= SearchText.Length;
                        try
                        {
                            this.Invoke(dFindAndSelect, new object[] { child, SearchText, MatchCase, bSearchDown, child.CurrentPosition, false });
                            this.Invoke(pgUpdate, new object[] { child, FindWindow });
                            this.Invoke(fwText, new object[] { FindWindow, strText, false });
                        }
                        catch
                        {
                            return;
                        }
                    }
                }
                if (strLine.Length == 0)
                    strText = "Search Failed!\r\n\r\n";
                else
                    strText = String.Format("Matching lines: {0}\r\n\r\n", nLinesFound);
                this.Invoke(fwText, new object[] { FindWindow, strText, true });
            }
            catch
            {
                return;
            }
        }

        public void UpdateFindWindowText(MDIChildEx child, string text, bool bDone)
        {
            child.richTextBox1.Text += text;
            child.richTextBox1.SelectionStart = child.richTextBox1.Text.Length;
            child.richTextBox1.ScrollToCaret();
        }

        public void InitializeProgressControl(MDIChildEx child)
        {
            progressBar2.Minimum = 0;
            progressBar2.Maximum = child.richTextBox1.Text.Length / 100;
            progressBar2.Show();
            progressBar2.Value = 0;
        }



        private void buttonFindAllInAllDoc_Click(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            bCancel = false;
            progressBar1.Value = 0;

            //int pct = 0;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = childWindow._parent.MdiChildren.Length;
            progressBar1.Show();
            searchFile.Show();
            String strText = "";
            MDIChildEx FindWindow = childWindow._parent.CreateNewWindow("Find All", "Find All In All Open Files", strText, CHILDTYPE.FIND);
            WindowList wList = new WindowList();
            wList.CurrentWindow = childWindow;
            wList.FindWindow = FindWindow;
            wList.SearchText = controlTextBox.Text;
            wList.MatchCase = controlMatchCaseCheckbox.Checked;
            wList.bSearchDown = controlDownRadioButton.Checked;

            tFindAllInAllDocThread = new Thread(FindAllInAllDocThread);
            tFindAllInAllDocThread.Start(wList);
        }



        private void FindAllInAllDocThread(object wList)
        {
            MDIChildEx child = ((WindowList)wList).CurrentWindow;
            MDIChildEx FindWindow = ((WindowList)wList).FindWindow;
            int nLinesFound = 0;
            int maxWindows = 0;
            string strText = "";
            string currTextLine = "";
            bool MatchCase = ((WindowList)wList).MatchCase;
            bool bSearchDown = ((WindowList)wList).bSearchDown;
            string SearchText = ((WindowList)wList).SearchText;


            SetFindInForm dFindAndSelect = new SetFindInForm(FindAndSelect);
            SetInitializeProgressControl pgInit = new SetInitializeProgressControl(InitializeProgressControl);
            SetUpdateFindWindowText fwText = new SetUpdateFindWindowText(UpdateFindWindowText);
            SetUpdateProgressBar1 dProgressBar1 = new SetUpdateProgressBar1(UpdateProgressBar1);
            SetClearProgressBar dClearProgressBar = new SetClearProgressBar(ClearProgressBar);

            // Count the actual number of Windows w/o the Find windows
            foreach (MDIChildEx childForm in child._parent.MdiChildren)
            {
                if (childForm.Type != CHILDTYPE.FIND)
                    maxWindows++;
            }

            foreach (MDIChildEx childForm in child._parent.MdiChildren)
            {
                try
                {
                    nLinesFound = 0;
                    if (bCancel)
                        break;
                    if (childForm.Type != CHILDTYPE.FIND)
                    {
                        strLine = "";

                        childForm.CurrentPosition = 0;
                        this.Invoke(dProgressBar1, new object[] { childForm, FindWindow, maxWindows });
                        this.Invoke(dFindAndSelect, new object[] { childForm, SearchText, MatchCase, bSearchDown, childForm.CurrentPosition, false });
                        strText = "Find " + "\"" + SearchText + "\" in " + childForm.fileName + "\r\n";
                        this.Invoke(fwText, new object[] { FindWindow, strText, true });

                        if ((childForm.CurrentPosition >= 0) && (!bCancel))
                        {
                            this.Invoke(pgInit, new object[] { child });
                            while ((childForm.CurrentPosition > 0) && (!bCancel))
                            {
                                nLinesFound++;
                                if (childForm.LastPos < 0)
                                    childForm.LastPos = 0;
                                currTextLine = childForm.currTextLine;
                                strLine = String.Format("{0:D5}:{1:D3} {2}: ",
                                        nLinesFound, childForm.CurrentLine + 1, childForm.fileName);
                                strText = strLine.ToString();
                                strText += currTextLine;
                                strText += "\r\n";
                                FindWindow.FindText += strText;
                                if (bSearchDown)
                                    childForm.CurrentPosition += SearchText.Length;
                                else
                                    childForm.CurrentPosition -= SearchText.Length;
                                this.Invoke(dFindAndSelect, new object[] { childForm, SearchText, MatchCase, bSearchDown, childForm.CurrentPosition, false });
                                this.Invoke(fwText, new object[] { FindWindow, strText, false });
                            }
                        }
                        if (strLine.Length == 0)
                            strText = "Search Failed!\r\n\r\n";
                        else
                            strText += String.Format("Matching lines: {0}\r\n\r\n", nLinesFound);
                        this.Invoke(fwText, new object[] { FindWindow, strText, false });
                    }
                }
                catch
                {
                    return;
                }
            }
            try
            {
                this.Invoke(dClearProgressBar);
            }
            catch
            {
            }
            bCancel = false;
        }

        public void ClearProgressBar()
        {
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 0;
            progressBar2.Minimum = 0;
            progressBar2.Maximum = 0;
            searchFile.Text = "";
            progressBar1.Refresh();
            progressBar2.Refresh();
            searchFile.Refresh();
        }

        public void UpdateProgressBar1(MDIChildEx child, MDIChildEx findWindow, int maxWindows)
        {
            progressBar1.Minimum = 0;
            progressBar1.Maximum = maxWindows;
            searchFile.Text = child.fileName;
            progressBar1.Value++;
            progressBar1.Refresh();
            searchFile.Refresh();
        }

        public void UpdateProgressBar2(MDIChildEx child, MDIChildEx findWindow)
        {
            if (child.CurrentPosition / 100 > progressBar2.Maximum)
                progressBar2.Value = progressBar2.Maximum;
            else
                progressBar2.Value = child.CurrentPosition / 100;
            progressBar2.Refresh();
            child.richTextBox1.SelectionStart = findWindow.richTextBox1.Text.Length;
            child.richTextBox1.ScrollToCaret();
        }

    }

    public class WindowList
    {
        public MDIChildEx CurrentWindow;
        public MDIChildEx FindWindow;
        public string SearchText;
        public bool MatchCase;
        public bool bSearchDown;
    }
}
