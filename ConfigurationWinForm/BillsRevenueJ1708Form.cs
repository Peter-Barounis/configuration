﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class BillsRevenueJ1708Form : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public uint BillsTakenFlag;
        public uint RevStatusFlag;
        public uint J1708StatusFlag;

        public uint RevStatusTime;
        public uint J1708PollIntervalTime;

        bool formLoaded = false;
        private CheckBox[] billsTakenArray = new CheckBox[7];
        private CheckBox[] RevStatusArray = new CheckBox[4];

        public BillsRevenueJ1708Form()
        {
            InitializeComponent();
        }
        public BillsRevenueJ1708Form(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();
            LoadData();
            formLoaded = true;
        }

        public void LoadData()
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Bills section
            billsTakenArray[0] = Bill1;
            billsTakenArray[1] = Bill2;
            billsTakenArray[2] = Bill5;
            billsTakenArray[3] = Bill10;
            billsTakenArray[4] = Bill20;
            billsTakenArray[5] = Bill50;
            billsTakenArray[6] = Bill100;
            
            BillsTakenFlag = Util.ParseValue(splitForm.iniConfig.inifileData.BillsTaken);
            for (int i = 0; i < 7; i++)
            {
                if ((BillsTakenFlag & (uint)1 << i) != 0)
                    billsTakenArray[i].Checked = true;
                else
                    billsTakenArray[i].Checked = false;
            }
            setBillsHexValue();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // RevStatus Status and Flags Section
            // 0x80 – Driver login via DRIVER card only 0 
            // 0x40 – 24 hour format of time on OCU display 0 Time is displayed in HH:MM:SS format. Remove seconds by setting bit 2 (0x0004) in OptionFlags21 
            // 0x20 – Farebox Host (Farebox controls processing/encoding/printing). 1 Requires LAMetroOption bit 1 be set (0x02). Refer to OF06 bit 0 for additional setting 
            // 0x10 – J1708 PID 501 enabled (Montreal) 0 
            // 0x0F – Time in 5 min increments of when to store transaction. 0 = OFF 0 Example: 1 = every 5 min, 2 = every 10 min, etc..
            RevStatusArray[0] = RevEnableJ1708;
            RevStatusArray[1] = RevFbxHostToTrim;
            RevStatusArray[2] = RevUse24HourClock;
            RevStatusArray[3] = RevDriverLoginViaCard;

            RevStatusFlag = Util.ParseValue(splitForm.iniConfig.inifileData.revstatus);

            for (int i = 0; i < 4; i++)
            {
                if ((RevStatusFlag & (uint)0x10 << i) != 0)
                    RevStatusArray[i].Checked = true;
                else
                    RevStatusArray[i].Checked = false;
            }
            RevStatusTime = RevStatusFlag & 0x0f;
            revenueStatusIntervalcombo.SelectedIndex = (int)RevStatusTime;
            SetRevStatusHexValue();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // J1708 Pollng and Flags Section
            // Revenue Status and Flags Section
            // 0x80 – Enable request for data sent. 0 
            // 0x40 – Poll for position. If “Poll Time”, below, is zero, then this should also be zero (0) 0 
            // 0x20 – Poll for stop. If “Poll Time”, below, is zero, then this should also be zero (0) 0 
            // 0x1F – Poll time in 10 second increments. 0 = OFF 0 Example: 1 = 10 sec., 2 = 20 sec. … F = 150 sec, 1F = 310 sec
            J1708StatusFlag = Util.ParseValue(splitForm.iniConfig.inifileData.J1708Poll);
            if ((J1708StatusFlag & (uint)0x20) != 0)
                J1708PollStop.Checked = true;
            else
                J1708PollStop.Checked = false;

            if ((J1708StatusFlag & (uint)0x40) != 0)
                J1708PollLatLong.Checked = true;
            else
                J1708PollLatLong.Checked = false;

            if ((J1708StatusFlag & (uint)0x80) != 0)
                J1708EnableSendReq.Checked = true;
            else
                J1708EnableSendReq.Checked = false;
            J1708PollIntervalTime = J1708StatusFlag & 0x1f;
            J1708PollInterval.SelectedIndex = (int)J1708PollIntervalTime;
            J1708Mid.Text = splitForm.iniConfig.inifileData.J1708MID;

            SetJ1708HexValue();
        }


        /////////////////////////////////////////////////////////////////////////////////////////
        // Read/Write Data
        public void SaveData()
        {
            splitForm.iniConfig.inifileData.BillsTaken = "0x" + BillsTakenFlag.ToString("X2");
            splitForm.iniConfig.inifileData.revstatus = "0x" + RevStatusFlag.ToString("X2");;
            splitForm.iniConfig.inifileData.J1708Poll = "0x" + J1708StatusFlag.ToString("X2");
            splitForm.iniConfig.inifileData.J1708MID = J1708Mid.Text;
        }

        public void UpdateData()
        {
            if (formLoaded)
            {
                setBillsHexValue();
                SetRevStatusHexValue();
                SetJ1708HexValue();
                SaveData();
                splitForm.SetChanged();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////
        // Bills Section
        private void setBillsHexValue()
        {
            billsAcceptedText.Text = "BillsTaken - [0x" + BillsTakenFlag.ToString("X2") + "]";
        }

        private void Bill_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                int index = ((CheckBox)sender).TabIndex;
                if (billsTakenArray[index].Checked)
                    BillsTakenFlag |= ((uint)1 << index);
                else
                    BillsTakenFlag &= ~((uint)1 << index);
                UpdateData();
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////
        // Rev Status Section
        public void SetRevStatusHexValue()
        {
            revenueStatusText.Text = "RevStatus - [0x" + RevStatusFlag.ToString("X2") + "]";
        }

        private void RevStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                int index = ((CheckBox)sender).TabIndex;
                if (RevStatusArray[index].Checked)
                    RevStatusFlag |= ((uint)0x10 << index);
                else
                    RevStatusFlag &= ~((uint)0x10 << index);
                UpdateData();
            }
        }

        private void revenueStatusIntervalcombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            RevStatusTime = (uint)(((ComboBox)sender).SelectedIndex);
            if (RevStatusTime > 0x0f)
                RevStatusTime = 0x0f;
            RevStatusFlag = RevStatusFlag & 0xF0 | RevStatusTime;
            UpdateData();
        }

        /////////////////////////////////////////////////////////////////////////////////////////
        // J1708 Polling Section
        public void SetJ1708HexValue()
        {
            j1708PollingText.Text = "J1708Poll and J1708MID - [0x" + J1708StatusFlag.ToString("X2") + "]";
        }

        public void UpdateJ1708()
        {
            if (formLoaded)
            {
                J1708StatusFlag = J1708StatusFlag & 0xF0 | J1708PollIntervalTime;
                UpdateData();
            }
        }

        private void J1708PollStop_CheckedChanged(object sender, EventArgs e)
        {
            if (this.J1708PollStop.Checked)
                J1708StatusFlag |= 0x20;
            else
                J1708StatusFlag &= ~(uint)0x20;
            UpdateJ1708();
        }

        private void J1708PollLatLong_CheckedChanged(object sender, EventArgs e)
        {
            if (this.J1708PollLatLong.Checked)
                J1708StatusFlag |= 0x40;
            else
                J1708StatusFlag &= ~(uint)0x40;
            UpdateJ1708();
        }

        private void J1708EnableSendReq_CheckedChanged(object sender, EventArgs e)
        {
            if (this.J1708EnableSendReq.Checked)
                J1708StatusFlag |= 0x80;
            else
                J1708StatusFlag &= ~(uint)0x80;
            UpdateJ1708();
        }

        private void J1708PollInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            J1708PollIntervalTime = (uint)(((ComboBox)sender).SelectedIndex);
            //if (J1708PollIntervalTime > 0)
            //    J1708PollIntervalTime--;
            if (J1708PollIntervalTime > 0x1f)
                J1708PollIntervalTime = 0x1f;
            J1708StatusFlag = J1708StatusFlag & 0xE0 | J1708PollIntervalTime;
            UpdateData();
        }

        private void J1708ManufacId_TextChanged(object sender, EventArgs e)
        {
            UpdateData();
        }
    }
}
