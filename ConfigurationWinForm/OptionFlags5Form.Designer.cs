﻿namespace ConfigurationWinForm
{
    partial class OptionFlags5Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.smartCardReaderCombo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(16, 72);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(447, 21);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "0x0010 - Look for second Period Pass product if first one is invalid";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(16, 162);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(382, 21);
            this.checkBox3.TabIndex = 3;
            this.checkBox3.Text = "0x0040 - If only 1 fare set, don’t display on log in screen";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(16, 191);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(275, 21);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Text = "0x0080 – No Fare set changing allowed";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(16, 216);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(376, 21);
            this.checkBox5.TabIndex = 5;
            this.checkBox5.Text = "0x0100 – Display Zone information only on main screen";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(16, 242);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(462, 21);
            this.checkBox6.TabIndex = 6;
            this.checkBox6.Text = "0x0200 – Display both Fare set and Zone information on main screen";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(16, 269);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(231, 21);
            this.checkBox7.TabIndex = 7;
            this.checkBox7.Text = "0x0400 – Change TRIP to ZONE";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(16, 296);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(566, 21);
            this.checkBox8.TabIndex = 8;
            this.checkBox8.Text = "0x0800 – LCD Idle. Don’t toggle text from D.S. and “Insert, Coins, Bills, etc.” m" +
    "essages";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(16, 323);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(425, 21);
            this.checkBox9.TabIndex = 9;
            this.checkBox9.Text = "0x1000 – Require an “Unlock Code” to access the login screen.";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.smartCardReaderCombo);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox12);
            this.optionflagsGridTitle.Controls.Add(this.checkBox11);
            this.optionflagsGridTitle.Controls.Add(this.checkBox10);
            this.optionflagsGridTitle.Controls.Add(this.checkBox4);
            this.optionflagsGridTitle.Controls.Add(this.checkBox5);
            this.optionflagsGridTitle.Controls.Add(this.checkBox6);
            this.optionflagsGridTitle.Controls.Add(this.checkBox7);
            this.optionflagsGridTitle.Controls.Add(this.checkBox3);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(12, 11);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(777, 467);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Select Smart Card Reader:";
            // 
            // smartCardReaderCombo
            // 
            this.smartCardReaderCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.smartCardReaderCombo.FormattingEnabled = true;
            this.smartCardReaderCombo.Items.AddRange(new object[] {
            "Disable Smart Card Reader",
            "Cubic Tri Reader",
            "HID 5553 Smart Card Reader (Only being used in Detroit, MI)",
            "HID veriCLASS Card Reader (Odyssey+/Fast Fare/Fast Fare-e)",
            "GenProx C2 Smart Card reader (Only available on older version of Odyssey)",
            "ACS Smart Card reader (Montreal Canada only)",
            "OTI 6000/6500 Smart Card Reader (CENTSaBILL/Odyssey)",
            "OTI 3000 Smart Card reader (Only available on early versions of D25718 Odyssey)",
            "FEIG Card Reader (Fast Fare/ Fast Fare-e)",
            "Mixed Fleet Mode. Make sure that Auto Smart Card Detect (OptionFlag6 0x400) is ch" +
                "ecked!"});
            this.smartCardReaderCombo.Location = new System.Drawing.Point(197, 28);
            this.smartCardReaderCombo.Name = "smartCardReaderCombo";
            this.smartCardReaderCombo.Size = new System.Drawing.Size(574, 24);
            this.smartCardReaderCombo.TabIndex = 17;
            this.smartCardReaderCombo.SelectedIndexChanged += new System.EventHandler(this.smartCardReaderChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 430);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(16, 102);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(401, 55);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Text = "0x0020 - TRiM is a receipt printer only, no magnetics. \r\nSetting this bit forces " +
    "the entire fleet to be set 0 as \'LUCC\'. \r\nIf not set, then device type is config" +
    "ured at farebox.";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(16, 403);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(608, 21);
            this.checkBox12.TabIndex = 12;
            this.checkBox12.Text = "0x8000 – Allow patron button “Read Card” feature if enabled for OCU (saves on TTP" +
    "’s used)";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(16, 376);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(608, 21);
            this.checkBox11.TabIndex = 11;
            this.checkBox11.Text = "0x4000 – “All Directions”. (North, South, East, West, Inbound, Outbound, and “No " +
    "Direction”)";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(16, 351);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(411, 21);
            this.checkBox10.TabIndex = 10;
            this.checkBox10.Text = "0x2000 – Change CITY to ZONE. (Set = ZONE, Clear = CITY)";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags5Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 489);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags5Form";
            this.Text = "OptionFlag5";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox smartCardReaderCombo;


    }
}