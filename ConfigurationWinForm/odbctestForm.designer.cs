﻿namespace ConfigurationWinForm
{
    partial class odbctestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(odbctestForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.queryTabControl = new System.Windows.Forms.TabControl();
            this.resultsText = new System.Windows.Forms.TextBox();
            this.resultsTabControl = new System.Windows.Forms.TabControl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.addLineNumbers = new System.Windows.Forms.CheckBox();
            this.enginesCombo = new System.Windows.Forms.ComboBox();
            this.comboDBType = new System.Windows.Forms.ComboBox();
            this.userTextBox = new System.Windows.Forms.TextBox();
            this.clearDataBtn = new System.Windows.Forms.Button();
            this.mssDriverCombo = new System.Windows.Forms.ComboBox();
            this.maxRecordsText = new System.Windows.Forms.TextBox();
            this.refreshEng = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.pwdTextBox = new System.Windows.Forms.TextBox();
            this.refreshMSS = new System.Windows.Forms.Button();
            this.MSSInstanceCombo = new System.Windows.Forms.ComboBox();
            this.dbNameCombo = new System.Windows.Forms.ComboBox();
            this.btnExecuteSQL = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.odbcMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.clearQueryResultsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectAlltoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshAll = new System.Windows.Forms.ToolStripSeparator();
            this.saveDatabaseConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDatabaseConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printPreviewGridMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printGridMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatCurrentTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floatAllTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dockAllQueriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripEnableToolsWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripEnableStatusWIndow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.collectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.columnsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.indexesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewForeignKeysForTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.fontCombo = new System.Windows.Forms.ComboBox();
            this.zoomOutBtn = new System.Windows.Forms.Button();
            this.zoomInBtn = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.databaseTableView = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.engLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dbNameLabel = new System.Windows.Forms.Label();
            this.serverLabel = new System.Windows.Forms.Label();
            this.browseDatabaseFile = new System.Windows.Forms.Button();
            this.groupToolbox = new System.Windows.Forms.GroupBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.portLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.odbcMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupToolbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Location = new System.Drawing.Point(9, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(0, 0);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Location = new System.Drawing.Point(9, 185);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(0, 0);
            this.panel2.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.resultsTabControl);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(946, 354);
            this.splitContainer1.SplitterDistance = 160;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.queryTabControl);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.resultsText);
            this.splitContainer2.Size = new System.Drawing.Size(946, 160);
            this.splitContainer2.SplitterDistance = 407;
            this.splitContainer2.SplitterWidth = 3;
            this.splitContainer2.TabIndex = 0;
            // 
            // queryTabControl
            // 
            this.queryTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.queryTabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.queryTabControl.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold);
            this.queryTabControl.Location = new System.Drawing.Point(0, 0);
            this.queryTabControl.Name = "queryTabControl";
            this.queryTabControl.SelectedIndex = 0;
            this.queryTabControl.Size = new System.Drawing.Size(407, 160);
            this.queryTabControl.TabIndex = 0;
            this.queryTabControl.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.queryTabDrawItem);
            this.queryTabControl.Click += new System.EventHandler(this.queryTabControl_Click);
            this.queryTabControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.queryTabControl_MouseDown);
            // 
            // resultsText
            // 
            this.resultsText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsText.Location = new System.Drawing.Point(0, 0);
            this.resultsText.Margin = new System.Windows.Forms.Padding(2);
            this.resultsText.MaxLength = 0;
            this.resultsText.Multiline = true;
            this.resultsText.Name = "resultsText";
            this.resultsText.ReadOnly = true;
            this.resultsText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.resultsText.Size = new System.Drawing.Size(536, 160);
            this.resultsText.TabIndex = 0;
            this.toolTip1.SetToolTip(this.resultsText, "Status and Error Messages Window.");
            this.resultsText.WordWrap = false;
            this.resultsText.Click += new System.EventHandler(this.resultsText_Click);
            // 
            // resultsTabControl
            // 
            this.resultsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsTabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.resultsTabControl.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultsTabControl.Location = new System.Drawing.Point(0, 0);
            this.resultsTabControl.Margin = new System.Windows.Forms.Padding(2);
            this.resultsTabControl.Name = "resultsTabControl";
            this.resultsTabControl.SelectedIndex = 0;
            this.resultsTabControl.ShowToolTips = true;
            this.resultsTabControl.Size = new System.Drawing.Size(946, 191);
            this.resultsTabControl.TabIndex = 0;
            this.toolTip1.SetToolTip(this.resultsTabControl, "This is a tooltip");
            this.resultsTabControl.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.resultsTabDrawItem);
            this.resultsTabControl.SelectedIndexChanged += new System.EventHandler(this.resultsTabControl_SelectedIndexChanged);
            this.resultsTabControl.Click += new System.EventHandler(this.resultsTabControl_Click);
            this.resultsTabControl.DoubleClick += new System.EventHandler(this.resultsTabControl_DoubleClick);
            this.resultsTabControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.resultsTabControl_MouseDown);
            // 
            // addLineNumbers
            // 
            this.addLineNumbers.AutoSize = true;
            this.addLineNumbers.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.addLineNumbers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addLineNumbers.Location = new System.Drawing.Point(412, 58);
            this.addLineNumbers.Margin = new System.Windows.Forms.Padding(2);
            this.addLineNumbers.Name = "addLineNumbers";
            this.addLineNumbers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.addLineNumbers.Size = new System.Drawing.Size(120, 17);
            this.addLineNumbers.TabIndex = 12;
            this.addLineNumbers.Text = "Insert Line Numbers";
            this.addLineNumbers.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.addLineNumbers, "Add line numbers to the records retrieved.");
            this.addLineNumbers.UseVisualStyleBackColor = true;
            this.addLineNumbers.CheckedChanged += new System.EventHandler(this.addLineNumbers_CheckedChanged);
            // 
            // enginesCombo
            // 
            this.enginesCombo.FormattingEnabled = true;
            this.enginesCombo.IntegralHeight = false;
            this.enginesCombo.ItemHeight = 13;
            this.enginesCombo.Location = new System.Drawing.Point(283, 7);
            this.enginesCombo.Margin = new System.Windows.Forms.Padding(2);
            this.enginesCombo.Name = "enginesCombo";
            this.enginesCombo.Size = new System.Drawing.Size(225, 21);
            this.enginesCombo.Sorted = true;
            this.enginesCombo.TabIndex = 4;
            this.toolTip1.SetToolTip(this.enginesCombo, "Select Sybase database engine name.");
            this.enginesCombo.SelectionChangeCommitted += new System.EventHandler(this.enginesCombo_SelectedIndexChanged);
            // 
            // comboDBType
            // 
            this.comboDBType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDBType.FormattingEnabled = true;
            this.comboDBType.Items.AddRange(new object[] {
            "Sybase",
            "SQL Server"});
            this.comboDBType.Location = new System.Drawing.Point(63, 7);
            this.comboDBType.Margin = new System.Windows.Forms.Padding(2);
            this.comboDBType.Name = "comboDBType";
            this.comboDBType.Size = new System.Drawing.Size(178, 21);
            this.comboDBType.TabIndex = 0;
            this.toolTip1.SetToolTip(this.comboDBType, "Select the database type (MSS,Sybase or SQLite).");
            this.comboDBType.SelectedIndexChanged += new System.EventHandler(this.comboDBType_SelectedIndexChanged);
            // 
            // userTextBox
            // 
            this.userTextBox.Location = new System.Drawing.Point(608, 7);
            this.userTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.userTextBox.Name = "userTextBox";
            this.userTextBox.Size = new System.Drawing.Size(218, 20);
            this.userTextBox.TabIndex = 6;
            this.toolTip1.SetToolTip(this.userTextBox, "User required to establish connection to database.");
            this.userTextBox.TextChanged += new System.EventHandler(this.userTextBox_TextChanged);
            // 
            // clearDataBtn
            // 
            this.clearDataBtn.Location = new System.Drawing.Point(8, 54);
            this.clearDataBtn.Margin = new System.Windows.Forms.Padding(2);
            this.clearDataBtn.Name = "clearDataBtn";
            this.clearDataBtn.Size = new System.Drawing.Size(95, 24);
            this.clearDataBtn.TabIndex = 5;
            this.clearDataBtn.Text = "Close All Results windows.";
            this.toolTip1.SetToolTip(this.clearDataBtn, "Cloase all windows including Queries,Tables,Views,Ttriggers and Procedures.");
            this.clearDataBtn.UseVisualStyleBackColor = true;
            this.clearDataBtn.Click += new System.EventHandler(this.clearDataDtn_Click);
            // 
            // mssDriverCombo
            // 
            this.mssDriverCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mssDriverCombo.FormattingEnabled = true;
            this.mssDriverCombo.Location = new System.Drawing.Point(608, 54);
            this.mssDriverCombo.Margin = new System.Windows.Forms.Padding(2);
            this.mssDriverCombo.Name = "mssDriverCombo";
            this.mssDriverCombo.Size = new System.Drawing.Size(218, 21);
            this.mssDriverCombo.TabIndex = 12;
            this.toolTip1.SetToolTip(this.mssDriverCombo, "Select the Device Drive to use for accessing MSS datbase.");
            this.mssDriverCombo.SelectedIndexChanged += new System.EventHandler(this.mssDriverCombo_SelectedIndexChanged);
            // 
            // maxRecordsText
            // 
            this.maxRecordsText.Location = new System.Drawing.Point(355, 56);
            this.maxRecordsText.Margin = new System.Windows.Forms.Padding(2);
            this.maxRecordsText.Name = "maxRecordsText";
            this.maxRecordsText.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.maxRecordsText.Size = new System.Drawing.Size(43, 20);
            this.maxRecordsText.TabIndex = 11;
            this.maxRecordsText.Text = "1500";
            this.toolTip1.SetToolTip(this.maxRecordsText, "Enter the maximum number of records to retrieve in any query (Default=1500).");
            this.maxRecordsText.TextChanged += new System.EventHandler(this.maxRecordsChanged);
            // 
            // refreshEng
            // 
            this.refreshEng.Image = global::ConfigurationWinForm.Properties.Resources.refresh;
            this.refreshEng.Location = new System.Drawing.Point(513, 7);
            this.refreshEng.Name = "refreshEng";
            this.refreshEng.Size = new System.Drawing.Size(22, 20);
            this.refreshEng.TabIndex = 28;
            this.toolTip1.SetToolTip(this.refreshEng, "Refresh the Sybase Engine Instances");
            this.refreshEng.UseVisualStyleBackColor = true;
            this.refreshEng.Click += new System.EventHandler(this.refreshEng_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(240, 56);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown1.TabIndex = 9;
            this.toolTip1.SetToolTip(this.numericUpDown1, "Connection timeout in seconds (default=15).");
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pwdTextBox
            // 
            this.pwdTextBox.Location = new System.Drawing.Point(608, 30);
            this.pwdTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.pwdTextBox.Name = "pwdTextBox";
            this.pwdTextBox.PasswordChar = '#';
            this.pwdTextBox.Size = new System.Drawing.Size(218, 20);
            this.pwdTextBox.TabIndex = 7;
            this.toolTip1.SetToolTip(this.pwdTextBox, "Password required to establish connection to dtabase.");
            this.pwdTextBox.TextChanged += new System.EventHandler(this.pwdTextBox_TextChanged);
            // 
            // refreshMSS
            // 
            this.refreshMSS.Image = global::ConfigurationWinForm.Properties.Resources.refresh1;
            this.refreshMSS.Location = new System.Drawing.Point(513, 30);
            this.refreshMSS.Name = "refreshMSS";
            this.refreshMSS.Size = new System.Drawing.Size(22, 20);
            this.refreshMSS.TabIndex = 29;
            this.toolTip1.SetToolTip(this.refreshMSS, "Refresh the SQL Server Instances available.");
            this.refreshMSS.UseVisualStyleBackColor = true;
            this.refreshMSS.Click += new System.EventHandler(this.refreshMSS_Click);
            // 
            // MSSInstanceCombo
            // 
            this.MSSInstanceCombo.FormattingEnabled = true;
            this.MSSInstanceCombo.IntegralHeight = false;
            this.MSSInstanceCombo.ItemHeight = 13;
            this.MSSInstanceCombo.Location = new System.Drawing.Point(283, 30);
            this.MSSInstanceCombo.Name = "MSSInstanceCombo";
            this.MSSInstanceCombo.Size = new System.Drawing.Size(225, 21);
            this.MSSInstanceCombo.Sorted = true;
            this.MSSInstanceCombo.TabIndex = 5;
            this.toolTip1.SetToolTip(this.MSSInstanceCombo, "Select Host machine or IP address where datbase resides.");
            // 
            // dbNameCombo
            // 
            this.dbNameCombo.FormattingEnabled = true;
            this.dbNameCombo.IntegralHeight = false;
            this.dbNameCombo.Location = new System.Drawing.Point(63, 30);
            this.dbNameCombo.Name = "dbNameCombo";
            this.dbNameCombo.Size = new System.Drawing.Size(152, 21);
            this.dbNameCombo.TabIndex = 1;
            this.toolTip1.SetToolTip(this.dbNameCombo, "Select the database name.");
            this.dbNameCombo.TextChanged += new System.EventHandler(this.dbNameCombo_TextChanged);
            // 
            // btnExecuteSQL
            // 
            this.btnExecuteSQL.BackColor = System.Drawing.SystemColors.Window;
            this.btnExecuteSQL.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExecuteSQL.Image = ((System.Drawing.Image)(resources.GetObject("btnExecuteSQL.Image")));
            this.btnExecuteSQL.Location = new System.Drawing.Point(116, 0);
            this.btnExecuteSQL.Name = "btnExecuteSQL";
            this.btnExecuteSQL.Size = new System.Drawing.Size(26, 21);
            this.btnExecuteSQL.TabIndex = 36;
            this.toolTip1.SetToolTip(this.btnExecuteSQL, "Run SQL [F5]");
            this.btnExecuteSQL.UseVisualStyleBackColor = false;
            this.btnExecuteSQL.Click += new System.EventHandler(this.btnExecuteSQL_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Window;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(91, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(18, 20);
            this.button1.TabIndex = 37;
            this.toolTip1.SetToolTip(this.button1, "Connect to Database [ALT+R]");
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 175);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(150, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(150, 25);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightToolStripPanel.Location = new System.Drawing.Point(150, 25);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 150);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 25);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 150);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(150, 150);
            // 
            // odbcMenuStrip
            // 
            this.odbcMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.viewToolStripMenuItem});
            this.odbcMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.odbcMenuStrip.Name = "odbcMenuStrip";
            this.odbcMenuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.odbcMenuStrip.Size = new System.Drawing.Size(1146, 24);
            this.odbcMenuStrip.TabIndex = 3;
            this.odbcMenuStrip.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.clearQueryResultsMenu,
            this.toolStripSeparator2,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem5,
            this.toolStripSeparator5,
            this.toolStripMenuItem6,
            this.disconnectAlltoolStripMenuItem,
            this.refreshAll,
            this.saveDatabaseConfiguration,
            this.loadDatabaseConfiguration,
            this.toolStripSeparator1,
            this.printPreviewGridMenuItem,
            this.printGridMenuItem,
            this.exportMenuItem});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.toolStripMenuItem4.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem4.Text = "Close Current Results Window";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // clearQueryResultsMenu
            // 
            this.clearQueryResultsMenu.Name = "clearQueryResultsMenu";
            this.clearQueryResultsMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.clearQueryResultsMenu.Size = new System.Drawing.Size(271, 22);
            this.clearQueryResultsMenu.Text = "Close All Results Windows";
            this.clearQueryResultsMenu.Click += new System.EventHandler(this.clearDataDtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(268, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem2.Text = "Export Selected Results to Text";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.exportMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Enabled = false;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem3.Text = "Export Selected Results to CSV";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem5.Text = "Export Status Messages";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(268, 6);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.toolStripMenuItem6.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem6.Text = "Connect to Database";
            this.toolStripMenuItem6.ToolTipText = "Connect to Database - Refresh all lists (Tables, Views, Triggers and Procedures)";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // disconnectAlltoolStripMenuItem
            // 
            this.disconnectAlltoolStripMenuItem.Name = "disconnectAlltoolStripMenuItem";
            this.disconnectAlltoolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.disconnectAlltoolStripMenuItem.Text = "Disconnect All Databases";
            this.disconnectAlltoolStripMenuItem.Click += new System.EventHandler(this.disconnectAlltoolStripMenuItem_Click);
            // 
            // refreshAll
            // 
            this.refreshAll.Name = "refreshAll";
            this.refreshAll.Size = new System.Drawing.Size(268, 6);
            // 
            // saveDatabaseConfiguration
            // 
            this.saveDatabaseConfiguration.Name = "saveDatabaseConfiguration";
            this.saveDatabaseConfiguration.Size = new System.Drawing.Size(271, 22);
            this.saveDatabaseConfiguration.Text = "Save Configuration";
            this.saveDatabaseConfiguration.Click += new System.EventHandler(this.saveDatabaseConfiguration_Click);
            // 
            // loadDatabaseConfiguration
            // 
            this.loadDatabaseConfiguration.Name = "loadDatabaseConfiguration";
            this.loadDatabaseConfiguration.Size = new System.Drawing.Size(271, 22);
            this.loadDatabaseConfiguration.Text = "Open Configuration";
            this.loadDatabaseConfiguration.Click += new System.EventHandler(this.loadDatabaseConfiguration_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(268, 6);
            // 
            // printPreviewGridMenuItem
            // 
            this.printPreviewGridMenuItem.Name = "printPreviewGridMenuItem";
            this.printPreviewGridMenuItem.Size = new System.Drawing.Size(271, 22);
            this.printPreviewGridMenuItem.Text = "Print Preview Results Tab";
            this.printPreviewGridMenuItem.Click += new System.EventHandler(this.printPreviewGridMenuItem_Click);
            // 
            // printGridMenuItem
            // 
            this.printGridMenuItem.Name = "printGridMenuItem";
            this.printGridMenuItem.Size = new System.Drawing.Size(271, 22);
            this.printGridMenuItem.Text = "Print Results Tab";
            this.printGridMenuItem.Click += new System.EventHandler(this.printGrid_Click);
            // 
            // exportMenuItem
            // 
            this.exportMenuItem.Name = "exportMenuItem";
            this.exportMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exportMenuItem.Size = new System.Drawing.Size(271, 22);
            this.exportMenuItem.Text = "Exit";
            this.exportMenuItem.Click += new System.EventHandler(this.exportMenuItem_Click_1);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.floatCurrentTabToolStripMenuItem,
            this.floatAllTabsToolStripMenuItem,
            this.dockAllQueriesToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripEnableToolsWindow,
            this.toolStripEnableStatusWIndow,
            this.toolStripSeparator3,
            this.collectionToolStripMenuItem,
            this.tablesToolStripMenuItem,
            this.columnsToolStripMenuItem1,
            this.viewsToolStripMenuItem1,
            this.indexesToolStripMenuItem1,
            this.viewForeignKeysForTableToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // floatCurrentTabToolStripMenuItem
            // 
            this.floatCurrentTabToolStripMenuItem.Name = "floatCurrentTabToolStripMenuItem";
            this.floatCurrentTabToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.floatCurrentTabToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.floatCurrentTabToolStripMenuItem.Text = "Float Current Result Tab";
            this.floatCurrentTabToolStripMenuItem.Click += new System.EventHandler(this.floatCurrentTabToolStripMenuItem_Click);
            // 
            // floatAllTabsToolStripMenuItem
            // 
            this.floatAllTabsToolStripMenuItem.Name = "floatAllTabsToolStripMenuItem";
            this.floatAllTabsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.floatAllTabsToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.floatAllTabsToolStripMenuItem.Text = "Float All Results";
            this.floatAllTabsToolStripMenuItem.Click += new System.EventHandler(this.floatAllTabsToolStripMenuItem_Click);
            // 
            // dockAllQueriesToolStripMenuItem
            // 
            this.dockAllQueriesToolStripMenuItem.Name = "dockAllQueriesToolStripMenuItem";
            this.dockAllQueriesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.dockAllQueriesToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.dockAllQueriesToolStripMenuItem.Text = "Dock All Results";
            this.dockAllQueriesToolStripMenuItem.Click += new System.EventHandler(this.dockAllQueriesToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(281, 6);
            // 
            // toolStripEnableToolsWindow
            // 
            this.toolStripEnableToolsWindow.Name = "toolStripEnableToolsWindow";
            this.toolStripEnableToolsWindow.Size = new System.Drawing.Size(284, 22);
            this.toolStripEnableToolsWindow.Text = "Hide Tools";
            this.toolStripEnableToolsWindow.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripEnableStatusWIndow
            // 
            this.toolStripEnableStatusWIndow.Name = "toolStripEnableStatusWIndow";
            this.toolStripEnableStatusWIndow.Size = new System.Drawing.Size(284, 22);
            this.toolStripEnableStatusWIndow.Text = "Hide Status Window";
            this.toolStripEnableStatusWIndow.Click += new System.EventHandler(this.toolStripEnableStatusWIndow_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(281, 6);
            // 
            // collectionToolStripMenuItem
            // 
            this.collectionToolStripMenuItem.Name = "collectionToolStripMenuItem";
            this.collectionToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.collectionToolStripMenuItem.Text = "View Database Collection";
            this.collectionToolStripMenuItem.Click += new System.EventHandler(this.collectionToolStripMenuItem_Click);
            // 
            // tablesToolStripMenuItem
            // 
            this.tablesToolStripMenuItem.Name = "tablesToolStripMenuItem";
            this.tablesToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.tablesToolStripMenuItem.Text = "View Database Tables";
            this.tablesToolStripMenuItem.Click += new System.EventHandler(this.tablesToolStripMenuItem_Click);
            // 
            // columnsToolStripMenuItem1
            // 
            this.columnsToolStripMenuItem1.Name = "columnsToolStripMenuItem1";
            this.columnsToolStripMenuItem1.Size = new System.Drawing.Size(284, 22);
            this.columnsToolStripMenuItem1.Text = "View Columns for Selected Table";
            this.columnsToolStripMenuItem1.Click += new System.EventHandler(this.columnsToolStripMenuItem1_Click);
            // 
            // viewsToolStripMenuItem1
            // 
            this.viewsToolStripMenuItem1.Name = "viewsToolStripMenuItem1";
            this.viewsToolStripMenuItem1.Size = new System.Drawing.Size(284, 22);
            this.viewsToolStripMenuItem1.Text = "View Database Views";
            this.viewsToolStripMenuItem1.Click += new System.EventHandler(this.viewsToolStripMenuItem1_Click);
            // 
            // indexesToolStripMenuItem1
            // 
            this.indexesToolStripMenuItem1.Name = "indexesToolStripMenuItem1";
            this.indexesToolStripMenuItem1.Size = new System.Drawing.Size(284, 22);
            this.indexesToolStripMenuItem1.Text = "View Database Indexes for Current Table";
            this.indexesToolStripMenuItem1.Click += new System.EventHandler(this.indexesToolStripMenuItem1_Click);
            // 
            // viewForeignKeysForTableToolStripMenuItem
            // 
            this.viewForeignKeysForTableToolStripMenuItem.Name = "viewForeignKeysForTableToolStripMenuItem";
            this.viewForeignKeysForTableToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.viewForeignKeysForTableToolStripMenuItem.Text = "View Foreign Keys for Table";
            this.viewForeignKeysForTableToolStripMenuItem.Click += new System.EventHandler(this.viewForeignKeysForTableToolStripMenuItem_Click);
            // 
            // fontCombo
            // 
            this.fontCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontCombo.FormattingEnabled = true;
            this.fontCombo.Location = new System.Drawing.Point(9, 62);
            this.fontCombo.Name = "fontCombo";
            this.fontCombo.Size = new System.Drawing.Size(77, 21);
            this.fontCombo.TabIndex = 59;
            this.fontCombo.Visible = false;
            this.fontCombo.TextChanged += new System.EventHandler(this.selectedFontChanged);
            // 
            // zoomOutBtn
            // 
            this.zoomOutBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.zoomOutBtn.Image = global::ConfigurationWinForm.Properties.Resources.ShrinkPoints;
            this.zoomOutBtn.Location = new System.Drawing.Point(39, 28);
            this.zoomOutBtn.Name = "zoomOutBtn";
            this.zoomOutBtn.Size = new System.Drawing.Size(28, 24);
            this.zoomOutBtn.TabIndex = 34;
            this.zoomOutBtn.UseVisualStyleBackColor = true;
            this.zoomOutBtn.Visible = false;
            this.zoomOutBtn.Click += new System.EventHandler(this.zoomOutBtn_Click);
            // 
            // zoomInBtn
            // 
            this.zoomInBtn.BackColor = System.Drawing.SystemColors.Control;
            this.zoomInBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.zoomInBtn.Image = global::ConfigurationWinForm.Properties.Resources.GrowPoints;
            this.zoomInBtn.Location = new System.Drawing.Point(9, 28);
            this.zoomInBtn.Name = "zoomInBtn";
            this.zoomInBtn.Size = new System.Drawing.Size(24, 24);
            this.zoomInBtn.TabIndex = 33;
            this.zoomInBtn.UseVisualStyleBackColor = false;
            this.zoomInBtn.Visible = false;
            this.zoomInBtn.Click += new System.EventHandler(this.zoomInBtn_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 89);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.databaseTableView);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer3.Size = new System.Drawing.Size(1146, 354);
            this.splitContainer3.SplitterDistance = 196;
            this.splitContainer3.TabIndex = 62;
            // 
            // databaseTableView
            // 
            this.databaseTableView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.databaseTableView.Location = new System.Drawing.Point(0, 0);
            this.databaseTableView.Name = "databaseTableView";
            this.databaseTableView.ShowNodeToolTips = true;
            this.databaseTableView.Size = new System.Drawing.Size(196, 354);
            this.databaseTableView.TabIndex = 0;
            this.databaseTableView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.databaseTableView_BeforeExpand);
            this.databaseTableView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.databaseTableView_AfterSelect);
            this.databaseTableView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.databaseTableView_NodeMouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(300, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Max. Rec:";
            // 
            // engLabel
            // 
            this.engLabel.AutoSize = true;
            this.engLabel.Location = new System.Drawing.Point(252, 11);
            this.engLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.engLabel.Name = "engLabel";
            this.engLabel.Size = new System.Drawing.Size(29, 13);
            this.engLabel.TabIndex = 2;
            this.engLabel.Text = "Eng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "DB Type:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(551, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "User:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(551, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(109, 60);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Conn. Timeout (seconds):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(551, 59);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Driver:";
            // 
            // dbNameLabel
            // 
            this.dbNameLabel.AutoSize = true;
            this.dbNameLabel.Location = new System.Drawing.Point(6, 34);
            this.dbNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dbNameLabel.Name = "dbNameLabel";
            this.dbNameLabel.Size = new System.Drawing.Size(56, 13);
            this.dbNameLabel.TabIndex = 8;
            this.dbNameLabel.Text = "DB Name:";
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Location = new System.Drawing.Point(248, 34);
            this.serverLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(32, 13);
            this.serverLabel.TabIndex = 3;
            this.serverLabel.Text = "Host:";
            // 
            // browseDatabaseFile
            // 
            this.browseDatabaseFile.BackColor = System.Drawing.SystemColors.Window;
            this.browseDatabaseFile.Enabled = false;
            this.browseDatabaseFile.FlatAppearance.BorderSize = 0;
            this.browseDatabaseFile.ForeColor = System.Drawing.SystemColors.Window;
            this.browseDatabaseFile.Image = ((System.Drawing.Image)(resources.GetObject("browseDatabaseFile.Image")));
            this.browseDatabaseFile.Location = new System.Drawing.Point(217, 30);
            this.browseDatabaseFile.Name = "browseDatabaseFile";
            this.browseDatabaseFile.Size = new System.Drawing.Size(26, 20);
            this.browseDatabaseFile.TabIndex = 24;
            this.browseDatabaseFile.UseVisualStyleBackColor = false;
            this.browseDatabaseFile.Click += new System.EventHandler(this.browseDatabaseFile_Click);
            // 
            // groupToolbox
            // 
            this.groupToolbox.Controls.Add(this.portTextBox);
            this.groupToolbox.Controls.Add(this.portLabel);
            this.groupToolbox.Controls.Add(this.dbNameCombo);
            this.groupToolbox.Controls.Add(this.MSSInstanceCombo);
            this.groupToolbox.Controls.Add(this.refreshMSS);
            this.groupToolbox.Controls.Add(this.pwdTextBox);
            this.groupToolbox.Controls.Add(this.numericUpDown1);
            this.groupToolbox.Controls.Add(this.refreshEng);
            this.groupToolbox.Controls.Add(this.maxRecordsText);
            this.groupToolbox.Controls.Add(this.browseDatabaseFile);
            this.groupToolbox.Controls.Add(this.serverLabel);
            this.groupToolbox.Controls.Add(this.dbNameLabel);
            this.groupToolbox.Controls.Add(this.mssDriverCombo);
            this.groupToolbox.Controls.Add(this.label7);
            this.groupToolbox.Controls.Add(this.label6);
            this.groupToolbox.Controls.Add(this.clearDataBtn);
            this.groupToolbox.Controls.Add(this.userTextBox);
            this.groupToolbox.Controls.Add(this.label5);
            this.groupToolbox.Controls.Add(this.label4);
            this.groupToolbox.Controls.Add(this.label3);
            this.groupToolbox.Controls.Add(this.comboDBType);
            this.groupToolbox.Controls.Add(this.engLabel);
            this.groupToolbox.Controls.Add(this.enginesCombo);
            this.groupToolbox.Controls.Add(this.addLineNumbers);
            this.groupToolbox.Controls.Add(this.label1);
            this.groupToolbox.Location = new System.Drawing.Point(150, -3);
            this.groupToolbox.Name = "groupToolbox";
            this.groupToolbox.Size = new System.Drawing.Size(996, 89);
            this.groupToolbox.TabIndex = 61;
            this.groupToolbox.TabStop = false;
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(868, 7);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(71, 20);
            this.portTextBox.TabIndex = 31;
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(835, 11);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(29, 13);
            this.portLabel.TabIndex = 30;
            this.portLabel.Text = "Port:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1146, 65);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // odbctestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1146, 443);
            this.Controls.Add(this.btnExecuteSQL);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.splitContainer3);
            this.Controls.Add(this.groupToolbox);
            this.Controls.Add(this.fontCombo);
            this.Controls.Add(this.zoomOutBtn);
            this.Controls.Add(this.zoomInBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.odbcMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "odbctestForm";
            this.Text = "Genfare Simple Query Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.odbctestForm_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.odbcMenuStrip.ResumeLayout(false);
            this.odbcMenuStrip.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupToolbox.ResumeLayout(false);
            this.groupToolbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.MenuStrip odbcMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clearQueryResultsMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exportMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.TabControl resultsTabControl;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatCurrentTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem floatAllTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dockAllQueriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem collectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem columnsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem indexesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator refreshAll;
        private System.Windows.Forms.Button zoomInBtn;
        private System.Windows.Forms.Button zoomOutBtn;
        private System.Windows.Forms.ComboBox fontCombo;
        private System.Windows.Forms.ToolStripMenuItem toolStripEnableStatusWIndow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripEnableToolsWindow;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TabControl queryTabControl;
        private System.Windows.Forms.TextBox resultsText;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TreeView databaseTableView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox addLineNumbers;
        private System.Windows.Forms.ComboBox enginesCombo;
        private System.Windows.Forms.Label engLabel;
        private System.Windows.Forms.ComboBox comboDBType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox userTextBox;
        private System.Windows.Forms.Button clearDataBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox mssDriverCombo;
        private System.Windows.Forms.Label dbNameLabel;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.Button browseDatabaseFile;
        private System.Windows.Forms.TextBox maxRecordsText;
        private System.Windows.Forms.Button refreshEng;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TextBox pwdTextBox;
        private System.Windows.Forms.Button refreshMSS;
        private System.Windows.Forms.ComboBox MSSInstanceCombo;
        private System.Windows.Forms.ComboBox dbNameCombo;
        private System.Windows.Forms.GroupBox groupToolbox;
        private System.Windows.Forms.Button btnExecuteSQL;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveDatabaseConfiguration;
        private System.Windows.Forms.ToolStripMenuItem loadDatabaseConfiguration;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printGridMenuItem;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.ToolStripMenuItem disconnectAlltoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewForeignKeysForTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewGridMenuItem;
    }
}