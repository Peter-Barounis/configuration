﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class RfsrvrForm : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public ToolTip toolTip1 = new ToolTip();
        public uint RfPrbFlags = 0;
        public RfsrvrForm()
        {
            InitializeComponent();
        }

        public RfsrvrForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm();
        }
        public void loadForm()
        {
            if (splitForm.iniConfig.inifileData.RFSRVR_DebugLevel.Trim().Length == 0)
                splitForm.iniConfig.inifileData.RFSRVR_DebugLevel = "0";
            if (splitForm.iniConfig.inifileData.RFSRVR_MaxWifiConnections.Trim().Length == 0)
                splitForm.iniConfig.inifileData.RFSRVR_MaxWifiConnections = "0";
            RfPrbFlags = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.RFSRVR_RfPrbFlags);

            this.numericUpDown1.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.RFSRVR_DebugLevel);
            this.numericUpDown2.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.RFSRVR_MaxWifiConnections);
            this.textBox1.Text = splitForm.iniConfig.inifileData.RFSRVR_FbxPollTime;
            this.textBox2.Text = splitForm.iniConfig.inifileData.RFSRVR_PowerOffTime;
            //this.textBox3.Text = splitForm.iniConfig.inifileData.RFSRVR_RfPrbFlags;
            this.textBox4.Text = splitForm.iniConfig.inifileData.RFSRVR_PrbTimeThreshold;
            this.textBox5.Text = splitForm.iniConfig.inifileData.RFSRVR_PrbEvntThreshold;
            this.textBox6.Text = splitForm.iniConfig.inifileData.RFSRVR_PrbTrnsThreshold;
            this.textBox7.Text = splitForm.iniConfig.inifileData.RFSRVR_KtFareThreshold;
            this.textBox8.Text = splitForm.iniConfig.inifileData.RFSRVR_BusProbeList;
            if ((RfPrbFlags & 0x00000001) > 0)
                this.radioButton1.Checked = true;
            else
                this.radioButton2.Checked = true;

            if ((RfPrbFlags & 0x00000002) > 0)
                this.saveWifiStatusYes.Checked = true;
            else
                this.SaveWifiStatusNo.Checked = true;

            formLoaded = true;
        }

        public void saveForm(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.RFSRVR_FbxPollTime = this.textBox1.Text;
                splitForm.iniConfig.inifileData.RFSRVR_PowerOffTime = this.textBox2.Text;
                //splitForm.iniConfig.inifileData.RFSRVR_RfPrbFlags = this.textBox3.Text;
                splitForm.iniConfig.inifileData.RFSRVR_PrbTimeThreshold = this.textBox4.Text;
                splitForm.iniConfig.inifileData.RFSRVR_PrbEvntThreshold = this.textBox5.Text;
                splitForm.iniConfig.inifileData.RFSRVR_PrbTrnsThreshold = this.textBox6.Text;
                splitForm.iniConfig.inifileData.RFSRVR_KtFareThreshold = this.textBox7.Text;
                splitForm.iniConfig.inifileData.RFSRVR_BusProbeList = this.textBox8.Text;
                splitForm.iniConfig.inifileData.RFSRVR_DebugLevel = this.numericUpDown1.Value.ToString();
                splitForm.iniConfig.inifileData.RFSRVR_MaxWifiConnections = this.numericUpDown2.Value.ToString();

                RfPrbFlags = 0x00000000;
                if (radioButton1.Checked)
                    RfPrbFlags |= 0x00000001;
                if (this.saveWifiStatusYes.Checked)
                    RfPrbFlags |= 0x00000002;
                splitForm.iniConfig.inifileData.RFSRVR_RfPrbFlags = "0x" + RfPrbFlags.ToString("X8");
                splitForm.SetChanged();
            }
        }

        private void wifiSettingsBtn_Click(object sender, EventArgs e)
        {
            splitForm.OpenFormByName("wifi general settings");
        }
    }
}
