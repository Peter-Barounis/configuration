﻿namespace ConfigurationWinForm
{
    partial class OptionFlags1Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGrid = new System.Windows.Forms.GroupBox();
            this.optionflagsGridTitle = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(39, 26);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(208, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - Prompt for ROUTE";
            this.toolTip2.SetToolTip(this.checkBox1, "Prompt for ROUTE");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(39, 52);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(189, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x0002 - Prompt for RUN";
            this.toolTip2.SetToolTip(this.checkBox2, "0x0002 - Prompt for RUN");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(39, 104);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(377, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x0008 - Prompt for TRIP (requires a non-zero entry) 1";
            this.toolTip2.SetToolTip(this.checkBox4, "0x0008 - Prompt for TRIP (requires a non-zero entry) 1");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(39, 78);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(482, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x0004 - Change RUN prompt to BLOCK (Set = BLOCK, Clear = RUN) 0";
            this.toolTip2.SetToolTip(this.checkBox3, "0x0004 - Change RUN prompt to BLOCK (Set = BLOCK, Clear = RUN) 0");
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(39, 130);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(319, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x0010 - Prompt for TRIP (zero entry allowed)";
            this.toolTip2.SetToolTip(this.checkBox5, "0x0010 - Prompt for TRIP (zero entry allowed)");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(39, 182);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(456, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x0040 - Set DIRECTION type to NSEW otherwise defaults to In/Out";
            this.toolTip2.SetToolTip(this.checkBox7, "0x0040 - Set DIRECTION type to NSEW otherwise defaults to In/Out");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(39, 208);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(188, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Prompt for CITY";
            this.toolTip2.SetToolTip(this.checkBox8, "0x0080 – Prompt for CITY");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(39, 234);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(572, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – Issue a “Stock Added” ticket from TRiM when Transfer stock has been adde" +
    "d";
            this.toolTip2.SetToolTip(this.checkBox9, "0x0100 – Issue a “Stock Added” ticket from TRiM when Transfer stock has been adde" +
        "d");
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(39, 260);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(253, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Use “Friendly Agency” list.";
            this.toolTip2.SetToolTip(this.checkBox10, "0x0200 – Use “Friendly Agency” list.");
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(39, 286);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(215, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – Allow 4-digit fares. 1";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(39, 312);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(281, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – OCU operates in “Menu” mode";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(39, 338);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(451, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Issue a “Test” card from the TRiM when farebox is probed";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGrid
            // 
            this.optionflagsGrid.Controls.Add(this.optionflagsGridTitle);
            this.optionflagsGrid.Controls.Add(this.button1);
            this.optionflagsGrid.Controls.Add(this.checkBox6);
            this.optionflagsGrid.Controls.Add(this.checkBox16);
            this.optionflagsGrid.Controls.Add(this.checkBox15);
            this.optionflagsGrid.Controls.Add(this.checkBox14);
            this.optionflagsGrid.Controls.Add(this.checkBox4);
            this.optionflagsGrid.Controls.Add(this.checkBox8);
            this.optionflagsGrid.Controls.Add(this.checkBox1);
            this.optionflagsGrid.Controls.Add(this.checkBox9);
            this.optionflagsGrid.Controls.Add(this.checkBox2);
            this.optionflagsGrid.Controls.Add(this.checkBox10);
            this.optionflagsGrid.Controls.Add(this.checkBox3);
            this.optionflagsGrid.Controls.Add(this.checkBox11);
            this.optionflagsGrid.Controls.Add(this.checkBox7);
            this.optionflagsGrid.Controls.Add(this.checkBox12);
            this.optionflagsGrid.Controls.Add(this.checkBox5);
            this.optionflagsGrid.Controls.Add(this.checkBox13);
            this.optionflagsGrid.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGrid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGrid.Name = "optionflagsGrid";
            this.optionflagsGrid.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGrid.Size = new System.Drawing.Size(655, 452);
            this.optionflagsGrid.TabIndex = 0;
            this.optionflagsGrid.TabStop = false;
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.AutoSize = true;
            this.optionflagsGridTitle.Location = new System.Drawing.Point(21, 0);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Size = new System.Drawing.Size(148, 17);
            this.optionflagsGridTitle.TabIndex = 17;
            this.optionflagsGridTitle.Text = "OptionFlag 1-[0x0000]";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(455, 411);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(39, 156);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(233, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x0020 - Prompt for DIRECTION";
            this.toolTip2.SetToolTip(this.checkBox6, "0x0020 - Force version 2 Configuration. Used to ensure that the box was getting t" +
        "he system 7.2 configuration");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(39, 416);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(234, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Enable “OptionFlags1”.";
            this.toolTip2.SetToolTip(this.checkBox16, "This bit MUST be set to enable the OptionFlags1 features");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(39, 390);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(277, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 – Disable farebox “Hibernation”.";
            this.toolTip2.SetToolTip(this.checkBox15, "If not set, default is 60 seconds after last activity.");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(39, 364);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(595, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Turn on OCU Lockout feature. When enabled, the OCU to ignore button pres" +
    "ses";
            this.toolTip2.SetToolTip(this.checkBox14, "This feature is used in conjunction with card (smart card or magnetic) to enable/" +
        "disable OCU");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags1Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 477);
            this.Controls.Add(this.optionflagsGrid);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags1Form";
            this.Text = "OptionFlag1";
            this.optionflagsGrid.ResumeLayout(false);
            this.optionflagsGrid.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGrid;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label optionflagsGridTitle;


    }
}