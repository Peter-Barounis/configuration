﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags13Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        int lmt1,lmt2,lmt3,lmt4;

        public OptionFlags13Form()
        {
            InitializeComponent();            
        }

        public OptionFlags13Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[12]);
            setNumericUpDownDefaults();
            setHexValue();
            formLoaded = true;
        }

        public void setHexValue()
        {
            int tmp;
            // Clear high nibble
            optionflag &= 0x0fff;
            // Re-read high nibble
            tmp = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[12]);
            tmp &= 0xf000;
            optionflag |= tmp;
            splitForm.iniConfig.inifileData.optionFlags[12] = "0x" + tmp.ToString("X4");

            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[12] = "0x" + optionflag.ToString("X4");
            if (formLoaded)
                splitForm.SetChanged();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0800;
            setNumericUpDownDefaults();
            setHexValue();
        }
        private void setNumericUpDownDefaults()
        {
            numericUpDown1.Value = (optionflag & 0x000F);
            numericUpDown2.Value = ((optionflag >> 4) & 0x003F);
            numericUpDown3.Value = ((optionflag >> 10) & 0x0001);
            numericUpDown4.Value = ((optionflag >> 11) & 0x0001);
            //numericUpDown5.Value = ((optionflag >> 12) & 0x000F);        
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFFF0) | (lmt1 & 0x000F));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }

        private void numericUpDown2_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown2_ValueChanged(sender, e);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            lmt2 = ((int)numericUpDown2.Value);
            optionflag = ((optionflag & 0xFC0F) | ((lmt2 << 4) & 0x03F0));
            setHexValue();
        }

        private void numericUpDown3_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown3_ValueChanged(sender, e);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            lmt3 = ((int)numericUpDown3.Value);
            optionflag = ((optionflag & 0xFBFF) | ((lmt3 << 10) & 0x0400));
            setHexValue(); 
        }
        private void numericUpDown4_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown4_ValueChanged(sender, e);
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            lmt4 = ((int)numericUpDown4.Value);
            optionflag = ((optionflag & 0xF7FF) | ((lmt4 << 11) & 0x0800));
            setHexValue(); 
        }
        //private void numericUpDown5_EnabledChanged(object sender, EventArgs e)
        //{
        //    numericUpDown5_ValueChanged(sender, e);
        //}

        //private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        //{
        //    lmt5 = ((int)numericUpDown5.Value);
        //    optionflag = ((optionflag & 0x0FFF) | ((lmt5 << 12) & 0xF000));
        //    setHexValue();
        //}
    }
}
