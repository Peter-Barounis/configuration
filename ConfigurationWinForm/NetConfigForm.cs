﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class NetConfigForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formLoaded = false;
        public NetConfigForm()
        {
            InitializeComponent();
        }

        public NetConfigForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadConfigForm();
            formLoaded = true;
            if (splitForm.globalSettings.NetConfigChanged)
                SaveConfiguration();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Control parent = Parent;
            while (!(parent is SplitForm))
                parent = parent.Parent;
            ((SplitForm)parent).CloseTabbedForm(this.Text);
        }


        private void loadConfigForm()
        {
            int value = 0;
            //GEN Section
            this.textBox1.Text = splitForm.netConfig.inifileData.Gen_Version;
            this.textBox2.Text = splitForm.netConfig.inifileData.Gen_ISPrbIntvlInMin;
            this.textBox3.Text = splitForm.netConfig.inifileData.Gen_OOSPrbIntvlInMin;
            this.textBox4.Text = splitForm.netConfig.inifileData.Gen_ProbeTimeOutInSec;
            
            //GDS Section
            this.textBox5.Text = splitForm.netConfig.inifileData.GDS_GCHostName;
            this.gfipAddressControl.Text = splitForm.netConfig.inifileData.GDS_GCIP;
            this.gctcpPortTextBox.Text = splitForm.netConfig.inifileData.GDS_GCTCPPort;

            //Wifi_GEN Section      
            this.comboBox1.Text = splitForm.netConfig.inifileData.WIFI_GEN_APConIntvlStatus;
            this.comboBox2.Text = splitForm.netConfig.inifileData.WIFI_WifiNetMode;
            this.comboBox3.Text = splitForm.netConfig.inifileData.WIFI_GEN_IBBSMode;

            if (splitForm.netConfig.inifileData.GenLink_HttpsStat.CompareTo("Enabled") == 0)
                comboBox11.SelectedIndex = 0;
            else
                comboBox11.SelectedIndex = 1;

            //this.comboBox11.Text = comboBox11.Items[this.comboBox11.SelectedIndex];
            numericUpDown1.Value = Util.ParseValue(splitForm.netConfig.inifileData.WIFI_GEN_WifiAPCount);
            numericUpDown2.Value = Util.ParseValue(splitForm.netConfig.inifileData.WIFI_GEN_APScanTimoutInMin);
            value = (int)Util.ParseValue(splitForm.netConfig.inifileData.WIFI_GEN_Min_RSSIThreshold);
            if (value > 70)
            {
                value = 70;
                splitForm.SetNetConfigChanged();
            }
            if (value < 40)
            {
                value = 40;
                splitForm.SetNetConfigChanged();
            }
            numericUpDown4.Value = value;
            value = (int)Util.ParseValue(splitForm.netConfig.inifileData.WIFI_GEN_Max_RSSIThreshold); ;
            if (value > 80)
            {
                value = 80;
                splitForm.SetNetConfigChanged();
            }
            if (value < 50)
            {
                value = 50;
                splitForm.SetNetConfigChanged();
            }
            numericUpDown3.Value = value;

            this.textBox12.Text = splitForm.netConfig.inifileData.WIFI_GEN_CapabilityFlags1;

            //WIFI_AP1 Section
            this.textBox17.Text = splitForm.netConfig.inifileData.WIFI_AP1_SSID;
            this.textBox18.Text = splitForm.netConfig.inifileData.WIFI_AP1_Passkey;
            this.comboBox4.Text = splitForm.netConfig.inifileData.WIFI_AP1_SecType;
            this.comboBox5.Text = splitForm.netConfig.inifileData.WIFI_AP1_DHCPMode;
            this.ipAddressControl1.Text = splitForm.netConfig.inifileData.WIFI_AP1_FBXIP;
            this.ipAddressControl2.Text = splitForm.netConfig.inifileData.WIFI_AP1_DefaultGateway;
            this.ipAddressControl3.Text = splitForm.netConfig.inifileData.WIFI_AP1_DNSPri;
            this.ipAddressControl4.Text = splitForm.netConfig.inifileData.WIFI_AP1_SubNetMask;
            this.ipAddressControl5.Text = splitForm.netConfig.inifileData.WIFI_AP1_DNSSec;

            //WIFI_AP2 Section
            this.textBox51.Text = splitForm.netConfig.inifileData.WIFI_AP2_SSID;
            this.textBox52.Text = splitForm.netConfig.inifileData.WIFI_AP2_Passkey;
            this.comboBox7.Text = splitForm.netConfig.inifileData.WIFI_AP2_SecType;
            this.comboBox6.Text = splitForm.netConfig.inifileData.WIFI_AP2_DHCPMode;
            this.ipAddressControl6.Text = splitForm.netConfig.inifileData.WIFI_AP2_FBXIP;
            this.ipAddressControl8.Text = splitForm.netConfig.inifileData.WIFI_AP2_DefaultGateway;
            this.ipAddressControl9.Text = splitForm.netConfig.inifileData.WIFI_AP2_DNSPri;
            this.ipAddressControl7.Text = splitForm.netConfig.inifileData.WIFI_AP2_SubNetMask;
            this.ipAddressControl10.Text = splitForm.netConfig.inifileData.WIFI_AP2_DNSSec;
            
            //ETHERNET Section
            this.comboBox8.Text = splitForm.netConfig.inifileData.ETHERNET_DHCPMode;
            this.textBox76.Text = splitForm.netConfig.inifileData.ETHERNET_CapabilityFlags1;
            this.ipAddressControl11.Text = splitForm.netConfig.inifileData.FETHERNE_TBXIP;
            this.ipAddressControl13.Text = splitForm.netConfig.inifileData.ETHERNET_DefaultGateway;
            this.ipAddressControl14.Text = splitForm.netConfig.inifileData.ETHERNET_DNSPri;
            this.ipAddressControl12.Text = splitForm.netConfig.inifileData.ETHERNET_SubNetMask;
            this.ipAddressControl15.Text = splitForm.netConfig.inifileData.ETHERNET_DNSSec;

            //CELLMODEM Section
            this.comboBox12.Text = splitForm.netConfig.inifileData.CellModem_Type;
            this.textBox99.Text = splitForm.netConfig.inifileData.CellSignal_Threshold;
            this.textBox6.Text = splitForm.netConfig.inifileData.CellModem_Status;

            //GPS Section
            this.ipAddressControl17.Text = splitForm.netConfig.inifileData.GPS_IP;
            this.comboBox9.Text = splitForm.netConfig.inifileData.GPS_NetProto;
            this.comboBox10.Text = splitForm.netConfig.inifileData.GPS_MsgFormat;
            this.textBox84.Text = splitForm.netConfig.inifileData.GPS_Port;
            this.textBox85.Text = splitForm.netConfig.inifileData.GPS_MaxTransTimeDelta;
            this.textBox86.Text = splitForm.netConfig.inifileData.GPS_MaxTransPositionDelta;
            this.textBox7.Text = splitForm.netConfig.inifileData.GPS_ModemStatus;
            
            //CLOUD Section
            this.textBox87.Text = splitForm.netConfig.inifileData.MbTkHost;
            this.textBox88.Text = splitForm.netConfig.inifileData.MbTkPath;
            this.textBox89.Text = splitForm.netConfig.inifileData.MbTkCredential;
            this.textBox90.Text = splitForm.netConfig.inifileData.GenLink_Host;
            this.textBox91.Text = splitForm.netConfig.inifileData.GenLink_SetupPath;
            this.textBox92.Text = splitForm.netConfig.inifileData.GenLink_SetupCredential;
            this.textBox93.Text = splitForm.netConfig.inifileData.GenLink_CapabilityFlags1;
            this.textBox94.Text = splitForm.netConfig.inifileData.GenLink_GDSUpgradeCredential;
            this.textBox95.Text = splitForm.netConfig.inifileData.GenLink_GDSUpgradeHost;
            this.textBox8.Text = splitForm.netConfig.inifileData.GenLink_GDSUpgradePath;
            this.textBox96.Text = splitForm.netConfig.inifileData.GenLink_Tenant;
            this.textBox97.Text = splitForm.netConfig.inifileData.GenLink_Env;
            this.textBox98.Text = splitForm.netConfig.inifileData.GenLink_DeviceAuthPath;
            formLoaded = true;
        }

        private void saveData(object sender, EventArgs e)
        {
            SaveConfiguration();
        }

        private void SaveConfiguration()
        {
            if (formLoaded)
            {
                //GEN Section
                splitForm.netConfig.inifileData.Gen_Version = this.textBox1.Text;
                splitForm.netConfig.inifileData.Gen_ISPrbIntvlInMin = this.textBox2.Text;
                splitForm.netConfig.inifileData.Gen_OOSPrbIntvlInMin = this.textBox3.Text;
                splitForm.netConfig.inifileData.Gen_ProbeTimeOutInSec = this.textBox4.Text;

                //GDS Section	
                splitForm.netConfig.inifileData.GDS_GCHostName = textBox5.Text;
                splitForm.netConfig.inifileData.GDS_GCIP = gfipAddressControl.Text;
                splitForm.netConfig.inifileData.GDS_GCTCPPort = gctcpPortTextBox.Text;
                splitForm.netConfig.inifileData.GDS_GCIP = gfipAddressControl.Text;
                splitForm.netConfig.inifileData.GDS_GCTCPPort = gctcpPortTextBox.Text;

                // SAVE IP and Port in the probserver section also
                splitForm.iniConfig.inifileData.Prb_GCIP = gfipAddressControl.Text;
                splitForm.iniConfig.inifileData.Prb_GCTCPPort = gctcpPortTextBox.Text;

                //Wifi_GEN Section      	
                splitForm.netConfig.inifileData.WIFI_GEN_APConIntvlStatus = this.comboBox1.Text;
                splitForm.netConfig.inifileData.WIFI_WifiNetMode = this.comboBox2.Text;
                splitForm.netConfig.inifileData.WIFI_GEN_IBBSMode = this.comboBox3.Text;
                splitForm.netConfig.inifileData.GenLink_HttpsStat = this.comboBox11.Text;

                splitForm.netConfig.inifileData.WIFI_GEN_WifiAPCount       = this.numericUpDown1.Value.ToString();
                splitForm.netConfig.inifileData.WIFI_GEN_APScanTimoutInMin = this.numericUpDown2.Value.ToString();
                splitForm.netConfig.inifileData.WIFI_GEN_Max_RSSIThreshold = this.numericUpDown3.Value.ToString();
                splitForm.netConfig.inifileData.WIFI_GEN_Min_RSSIThreshold = this.numericUpDown4.Value.ToString();
                splitForm.netConfig.inifileData.WIFI_GEN_CapabilityFlags1 = this.textBox12.Text;

                //WIFI_AP1 Section	
                splitForm.netConfig.inifileData.WIFI_AP1_SSID = this.textBox17.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_Passkey = this.textBox18.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_SecType = this.comboBox4.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_DHCPMode = this.comboBox5.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_FBXIP = this.ipAddressControl1.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_DefaultGateway = this.ipAddressControl2.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_DNSPri = this.ipAddressControl3.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_SubNetMask = this.ipAddressControl4.Text;
                splitForm.netConfig.inifileData.WIFI_AP1_DNSSec = this.ipAddressControl5.Text;

                //WIFI_AP2 Section	
                splitForm.netConfig.inifileData.WIFI_AP2_SSID = this.textBox51.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_Passkey = this.textBox52.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_SecType = this.comboBox7.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_DHCPMode = this.comboBox6.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_FBXIP = this.ipAddressControl6.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_DefaultGateway = this.ipAddressControl8.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_DNSPri = this.ipAddressControl9.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_SubNetMask = this.ipAddressControl7.Text;
                splitForm.netConfig.inifileData.WIFI_AP2_DNSSec = this.ipAddressControl10.Text;

                //ETHERNET Section	
                splitForm.netConfig.inifileData.ETHERNET_DHCPMode = this.comboBox8.Text;
                splitForm.netConfig.inifileData.ETHERNET_CapabilityFlags1 = this.textBox76.Text;
                splitForm.netConfig.inifileData.FETHERNE_TBXIP = this.ipAddressControl11.Text;
                splitForm.netConfig.inifileData.ETHERNET_DefaultGateway = this.ipAddressControl13.Text;
                splitForm.netConfig.inifileData.ETHERNET_DNSPri = this.ipAddressControl14.Text;
                splitForm.netConfig.inifileData.ETHERNET_SubNetMask = this.ipAddressControl12.Text;
                splitForm.netConfig.inifileData.ETHERNET_DNSSec = this.ipAddressControl15.Text;

                //CELLMODEM Section	
                splitForm.netConfig.inifileData.CellModem_Type = this.comboBox12.Text;
                splitForm.netConfig.inifileData.CellSignal_Threshold = this.textBox99.Text;
                splitForm.netConfig.inifileData.CellModem_Status = this.textBox6.Text;

                //GPS Section	
                splitForm.netConfig.inifileData.GPS_IP = this.ipAddressControl17.Text;
                splitForm.netConfig.inifileData.GPS_NetProto = this.comboBox9.Text;
                splitForm.netConfig.inifileData.GPS_MsgFormat = this.comboBox10.Text;
                splitForm.netConfig.inifileData.GPS_Port = this.textBox84.Text;
                splitForm.netConfig.inifileData.GPS_MaxTransTimeDelta = this.textBox85.Text;
                splitForm.netConfig.inifileData.GPS_MaxTransPositionDelta = this.textBox86.Text;
                splitForm.netConfig.inifileData.GPS_ModemStatus = this.textBox7.Text;

                //CLOUD Section	
                splitForm.netConfig.inifileData.MbTkHost = this.textBox87.Text;
                splitForm.netConfig.inifileData.MbTkPath = this.textBox88.Text;
                splitForm.netConfig.inifileData.MbTkCredential = this.textBox89.Text;
                splitForm.netConfig.inifileData.GenLink_Host = this.textBox90.Text;
                splitForm.netConfig.inifileData.GenLink_SetupPath = this.textBox91.Text;
                splitForm.netConfig.inifileData.GenLink_SetupCredential = this.textBox92.Text;
                splitForm.netConfig.inifileData.GenLink_CapabilityFlags1 = this.textBox93.Text;
                splitForm.netConfig.inifileData.GenLink_GDSUpgradeCredential = this.textBox94.Text;
                splitForm.netConfig.inifileData.GenLink_GDSUpgradeHost = this.textBox95.Text;
                splitForm.netConfig.inifileData.GenLink_GDSUpgradePath = this.textBox8.Text;
                splitForm.netConfig.inifileData.GenLink_Tenant = this.textBox96.Text;
                splitForm.netConfig.inifileData.GenLink_Env = this.textBox97.Text;
                splitForm.netConfig.inifileData.GenLink_DeviceAuthPath = this.textBox98.Text;
                splitForm.SetNetConfigChanged();
            }                  
        }

        private void gfipAddressControl_TextChanged(object sender, EventArgs e)
        {
            splitForm.globalSettings.dirty = true;
            saveData(sender, e);
        }

        private void gctcpPortTextBox_TextChanged(object sender, EventArgs e)
        {
            splitForm.globalSettings.dirty = true;
            saveData(sender, e);
        }
    }
}
