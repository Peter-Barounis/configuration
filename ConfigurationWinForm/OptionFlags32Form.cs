﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags32Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;

        public OptionFlags32Form()
        {
            InitializeComponent();
        }
        public OptionFlags32Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            this.option_Flag32txt.Text = splitForm.iniConfig.inifileData.optionFlags[31];
            formLoaded = true;
         }

        private void button1_Click(object sender, EventArgs e)
        {
            Control parent = Parent;
            while (!(parent is SplitForm))
                parent = parent.Parent;
            ((SplitForm)parent).CloseTabbedForm(this.Text);
        }

        private void optionFlag32txt_TextChanged(object sender, EventArgs e)
        {
            splitForm.iniConfig.inifileData.optionFlags[31] = this.option_Flag32txt.Text;
            if(formLoaded)
                splitForm.SetChanged();
        }
    }
}
