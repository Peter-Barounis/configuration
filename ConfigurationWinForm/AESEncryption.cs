﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using System.Diagnostics;
using System.Windows.Forms;
namespace ConfigurationWinForm
{
    public class ManagedAes
    {
        private AesManaged aesEncryption = new AesManaged();

        public ManagedAes()
        {
        }

        public ManagedAes(string key)
        {
            SetKey(key);
        }

        public void SetKey(string key)
        {
            try
            {
                aesEncryption.Key = Encoding.ASCII.GetBytes(key);// max length is 256 bits
            }
            catch (Exception ex)
            {
                MessageBox.Show("Inavlid Key: "+ ex.ToString());
            }
        }

        private byte[] StringToByteArray(string hex)
        {
            byte[] arr = new byte[hex.Length >> 1];
            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }
            return arr;
        }

        private int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        private string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] data = new byte[hexString.Length / 2];
            for (int index = 0; index < data.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                data[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return data;
        }

        public string Encrypt(string plainText)
        {
            byte[] encrypted;
            // Create a new AesManaged.    
            using (AesManaged aes = new AesManaged())
            {
                aes.Key = aesEncryption.Key;
                aes.IV = aesEncryption.IV;
                aes.Padding = PaddingMode.Zeros;
                // Create encryptor    
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (StreamWriter sw = new StreamWriter(cs))
                            sw.Write(plainText);
                        encrypted = ms.ToArray();
                    }
                }
            }
            // Return encrypted data    
            return ByteArrayToString(encrypted);
        }

        public string Decrypt(string cipherString)
        {

            string plaintext = null;
            byte[] cipherArray = ConvertHexStringToByteArray(cipherString);
            // Create AesManaged    
            using (AesManaged aes = new AesManaged())
            {
                aes.Key = aesEncryption.Key;
                aes.IV = aesEncryption.IV;
                aes.Padding = PaddingMode.Zeros;
                //aes.Key = Encoding.ASCII.GetBytes("12345678901234567890123456789012");// max length is 256 bits
                // Create a decryptor    
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                // Create the streams used for decryption.    
                using (MemoryStream ms = new MemoryStream(cipherArray))
                {
                    // Create crypto stream    
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        // Read crypto stream    
                        using (StreamReader reader = new StreamReader(cs))
                            plaintext = reader.ReadToEnd();
                    }
                }
            }

            // Remove trailing blanks
            StringBuilder decryptedStringc = new StringBuilder();
            for (int i = 0; i < plaintext.Length; i++)
            {
                if (plaintext[i] != 0)
                    decryptedStringc.Append(plaintext[i]);
            }
            return decryptedStringc.ToString();
        }
    }
}
