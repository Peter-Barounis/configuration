﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class ClntForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public ToolTip toolTip1 = new ToolTip();
        private bool formLoaded=false;

        public ClntForm()
        {
            InitializeComponent();
        }

        public ClntForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm(sf);
        }

        //private void Cancel_button_Click(object sender, EventArgs e)
        //{
        //    Control parent = Parent;
        //    while (!(parent is SplitForm))
        //        parent = parent.Parent;
        //    ((SplitForm)parent).CloseTabbedForm(this.Text);
        //}
        public void ValidateNumericTextBox(TextBox textbox)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textbox.Text, "[^0-9]"))
            {
                System.Media.SystemSounds.Hand.Play();
                textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                textbox.SelectionStart = textbox.Text.Length;
            }
        }

        public void loadForm(SplitForm sf)
        {
            splitForm = sf;
            if (splitForm.iniConfig.inifileData.Clnt_DebugLevel.Trim().Length == 0)
                splitForm.iniConfig.inifileData.Clnt_DebugLevel = numericUpDown1.Minimum.ToString();
            numericUpDown1.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.Clnt_DebugLevel);
            if (splitForm.iniConfig.inifileData.Clnt_MaxConnRetry.Trim().Length == 0)
                splitForm.iniConfig.inifileData.Clnt_MaxConnRetry = numericUpDown2.Minimum.ToString();
            numericUpDown2.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.Clnt_MaxConnRetry);
            textBox1.Text = splitForm.iniConfig.inifileData.Clnt_WaitTime;
            formLoaded = true;
        }
        public void saveForm(Object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.Clnt_DebugLevel = this.numericUpDown1.Value.ToString();
                splitForm.iniConfig.inifileData.Clnt_MaxConnRetry = this.numericUpDown2.Value.ToString();
                splitForm.SetChanged();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ValidateNumericTextBox((TextBox)sender);
            splitForm.iniConfig.inifileData.Clnt_WaitTime = this.textBox1.Text;
        }

    }
}
