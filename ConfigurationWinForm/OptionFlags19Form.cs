﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags19Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags19Form()
        {
            InitializeComponent();
         }

        public OptionFlags19Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[18].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[18]);
            setCheckboxChecked();            
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[18] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag(((CheckBox)sender));
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0001;
            setCheckboxChecked();
            setHexValue();
        }
        private void setCheckboxChecked()
        {
            this.checkBox1.Checked = Convert.ToBoolean((optionflag >> 0) & 0x0001);
            this.checkBox2.Checked = Convert.ToBoolean((optionflag >> 1) & 0x0001);
            this.checkBox3.Checked = Convert.ToBoolean((optionflag >> 2) & 0x0001);
            this.checkBox4.Checked = Convert.ToBoolean((optionflag >> 3) & 0x0001);
            this.checkBox5.Checked = Convert.ToBoolean((optionflag >> 4) & 0x0001);
            this.checkBox6.Checked = Convert.ToBoolean((optionflag >> 5) & 0x0001);
            this.checkBox7.Checked = Convert.ToBoolean((optionflag >> 6) & 0x0001);
            this.checkBox8.Checked = Convert.ToBoolean((optionflag >> 7) & 0x0001);
            this.checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            this.checkBox10.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            this.numericUpDown1.Value = ((optionflag >> 10) & 0x003F);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0x03FF) | ((lmt1 << 10) & 0xFC00));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }
    }
}
