﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class LocationForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formLoaded = false;
        public LocationForm()
        {
            InitializeComponent();
        }

        public LocationForm(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();
            LoadData();
            formLoaded = true;
        }


        public void SetVaultCountBtn()
        {
            if (rbVault2.Checked)
            {
                this.cbVlt4.Enabled = true;
                this.cbVlt5.Enabled = true;
            }
            else
            {
                this.cbVlt4.Enabled = false;
                this.cbVlt5.Enabled = false;
            }
        }

        public void UpdateLocation()
        {
            locationNameText.Text = splitForm.globalSettings.location_name;
            locationNumberText.Text = splitForm.globalSettings.locationID.ToString();
        }

        public void LoadData()
        {
            int value = 0;
            enableWifiProbing.Checked = splitForm.iniConfig.inifileData.enableWifiProbing;
            enableVndldr.Checked = splitForm.iniConfig.inifileData.enableVndldr;
            enableDatabase.Checked = splitForm.iniConfig.inifileData.enableDatabase;
            cbMux1.Checked = splitForm.iniConfig.inifileData.muxEnabled[0];
            cbMux2.Checked = splitForm.iniConfig.inifileData.muxEnabled[1];
            cbMux3.Checked = splitForm.iniConfig.inifileData.muxEnabled[2];

            cbVlt1.Checked = splitForm.iniConfig.inifileData.vltEnabled[0];
            cbVlt2.Checked = splitForm.iniConfig.inifileData.vltEnabled[1];
            cbVlt3.Checked = splitForm.iniConfig.inifileData.vltEnabled[2];
            cbVlt4.Checked = splitForm.iniConfig.inifileData.vltEnabled[3];
            cbVlt5.Checked = splitForm.iniConfig.inifileData.vltEnabled[4];

            if (Util.ParseValue(splitForm.iniConfig.inifileData.Init_NoDatabase) > 0)
                cbStartdb.Checked = false;
            else
                cbStartdb.Checked = true;
            enableMobileTicketing.Checked = splitForm.iniConfig.inifileData.enableMobileTicketing;

            // Select 2 vaults/Controller
            value = (int)Util.ParseValue(splitForm.iniConfig.inifileData.Init_nVaultsPerController);
            if (value == 4)
            {
                rbVault2.Checked = false;
                rbVault4.Checked = true;
            }
            else
            {
                rbVault2.Checked = true;
                rbVault4.Checked = false;
            }

            // Set radio buttons
            SetVaultCountBtn();
            ds_rf_ip.Text = splitForm.iniConfig.inifileData.DS_RF_IP;
            dsport.Text = splitForm.iniConfig.inifileData.DsPort;
            fbxport.Text = splitForm.iniConfig.inifileData.FbxPort;
            if (splitForm.globalSettings.systemType != splitForm.NETWORK_MANAGER)
            {
                locationNameText.Text = splitForm.globalSettings.location_name;
                locationNumberText.Text = splitForm.globalSettings.locationID.ToString();
            }
            else
            {
                locationNameText.Text = "Network Manager";
                locationNumberText.Text = "N/A";
            }
        }

        public void SaveData()
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.enableWifiProbing = enableWifiProbing.Checked;
                splitForm.iniConfig.inifileData.enableMobileTicketing = enableMobileTicketing.Checked;
                splitForm.iniConfig.inifileData.enableVndldr = enableVndldr.Checked;
                splitForm.iniConfig.inifileData.enableDatabase = enableDatabase.Checked;
                dsport.Enabled = enableWifiProbing.Checked;
                ds_rf_ip.Enabled = enableWifiProbing.Checked;
                fbxport.Enabled = enableWifiProbing.Checked; 

                splitForm.iniConfig.inifileData.muxEnabled[0] = cbMux1.Checked;
                splitForm.iniConfig.inifileData.muxEnabled[1] = cbMux2.Checked;
                splitForm.iniConfig.inifileData.muxEnabled[2] = cbMux3.Checked;

                splitForm.iniConfig.inifileData.vltEnabled[0] = cbVlt1.Checked;
                splitForm.iniConfig.inifileData.vltEnabled[1] = cbVlt2.Checked;
                splitForm.iniConfig.inifileData.vltEnabled[2] = cbVlt3.Checked;
                splitForm.iniConfig.inifileData.vltEnabled[3] = cbVlt4.Checked;
                splitForm.iniConfig.inifileData.vltEnabled[4] = cbVlt5.Checked;
                if (rbVault2.Checked)
                    splitForm.iniConfig.inifileData.Init_nVaultsPerController = "2";
                else
                    splitForm.iniConfig.inifileData.Init_nVaultsPerController = "4";

                if (cbStartdb.Checked)
                    splitForm.iniConfig.inifileData.Init_NoDatabase = "0";
                else
                    splitForm.iniConfig.inifileData.Init_NoDatabase = "1";
                splitForm.iniConfig.inifileData.DS_RF_IP = ds_rf_ip.Text;
                splitForm.iniConfig.inifileData.DsPort = dsport.Text;
                splitForm.iniConfig.inifileData.FbxPort = fbxport.Text;
                SetVaultCountBtn();
                splitForm.UpdateTree();
            }
        }

        private void UpdateMux1(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if ((cbMux1.Checked == false) &&
                    (cbMux2.Checked || cbMux3.Checked))
                {
                    cbMux1.Checked = true;
                    MessageBox.Show("Cannot disable Mux1 if Mu2 or Mux3 are enabled!", "Mux1 Selection Warning");
                }
                else
                    splitForm.SetChanged();
            }

            SaveData();
        }

        private void UpdateMuxVault(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if ((cbMux2.Checked || cbMux3.Checked) && !cbMux1.Checked)
                {
                    cbMux1.Checked = true;
                    MessageBox.Show("Cannot enable Mux2 or Mux3 if Mux1 is not enabled!", "Mux2-3 Selection Warning");
                }
                splitForm.SetChanged();
            }
            SaveData();
        }

        private void LocationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveData();
        }

        private void enableWifiProbing_CheckedChanged(object sender, EventArgs e)
        {
            SetChanged();
            splitForm.EnableWifi(enableWifiProbing.Checked);
           
        }

        private void dsport_TextChanged(object sender, EventArgs e)
        {
            SetChanged();
        }

        private void SetChanged()
        {
            if (formLoaded)
            {
                SaveData();
                splitForm.SetChanged();
            }
        }

        private void enableMobileTicket_CheckedChanged(object sender, EventArgs e)
        {
            //splitForm.iniConfig.inifileData.enableMobileTicketing = enableMobileTicketing.Checked;
            SetChanged();
        }

        private void enableVndldr_CheckedChanged(object sender, EventArgs e)
        {
            SetChanged();
        }
    }
}
