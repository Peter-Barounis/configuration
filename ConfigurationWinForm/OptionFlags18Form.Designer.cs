﻿namespace ConfigurationWinForm
{
    partial class OptionFlags18Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(16, 129);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(732, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x0010 - ZigBee Enabled. Allows inter unit communication for when more than one u" +
    "nit  on a bus. (Fast Fare –e)";
            this.toolTip2.SetToolTip(this.checkBox5, "ZigBee Enabled. Allows inter unit communication for when more than one unit\r\non a" +
        " bus. (Fast Fare -e)");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(16, 183);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(702, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x0040 - Print ‘X’ on stored VALUE card when no more value or card used up and  g" +
    "oing to issue new one.";
            this.toolTip2.SetToolTip(this.checkBox7, "Print ‘X’ on stored VALUE card when no more value or card used up and 1 0 \r\ngoing" +
        " to issue new one.");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(16, 212);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(681, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Print ‘X’ on stored RIDE card when no more rides or card used up and goi" +
    "ng to issue new one.";
            this.toolTip2.SetToolTip(this.checkBox8, "Print ‘X’ on stored RIDE card when no more rides or card used up and going to 1 0" +
        " \r\nissue new one.");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(16, 345);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(431, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Enter “power save” immediately when driver is logs off.";
            this.toolTip2.SetToolTip(this.checkBox13, "Enter “power save” immediately when driver is logs off.");
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label2);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown2);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox6);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox7);
            this.optionflagsGridTitle.Controls.Add(this.checkBox5);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(810, 486);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 269);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(636, 34);
            this.label2.TabIndex = 20;
            this.label2.Text = "0x0100-0x0F00 - Passenger display backlight intensity when OOS (0->15).(Fast Fare" +
    " / Fast Fare -e)\r\nA value of ‘0’ provides the best power saving. \r\n";
            this.toolTip2.SetToolTip(this.label2, "0x0100-0x0F00 - Passenger display backlight intensity when OOS (0->15).(Fast Fare" +
        " / Fast Fare -e)\r\nA value of ‘0’ provides the best power saving. \r\n\r\n");
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(16, 276);
            this.numericUpDown2.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(55, 22);
            this.numericUpDown2.TabIndex = 19;
            this.toolTip2.SetToolTip(this.numericUpDown2, "0x0100-0x0F00 - Passenger display backlight intensity when OOS (0à15).(Fast Fare " +
        "/ Fast Fare -e)\r\nA value of ‘0’ provides the best power saving. \r\n");
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(535, 68);
            this.label1.TabIndex = 18;
            this.label1.Text = "0x0001 - 0x000F - Color of Fast Fare lid LED’s during normal operation. (Fast Far" +
    "e) \r\n0 = Blue, 1 = Orange, 2 = Dark Green, 3 = Cyan, 4 = Magenta, 5 = Yellow, \r\n" +
    "6 = Gray, 7 = White, F = Off. \r\n\r\n";
            this.toolTip2.SetToolTip(this.label1, "0x0001 - 0x000F - Color of Fast Fare lid LED’s during normal operation. (Fast Far" +
        "e) \r\n0 = Blue, 1 = Orange, 2 = Dark Green, 3 = Cyan, 4 = Magenta, 5 = Yellow, \r\n" +
        "6 = Gray, 7 = White, F = Off. \r\n\r\n\r\n");
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(16, 49);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(55, 22);
            this.numericUpDown1.TabIndex = 17;
            this.toolTip2.SetToolTip(this.numericUpDown1, "0x0001 - 0x000F - Color of Fast Fare lid LED’s during normal operation. (Fast Far" +
        "e) \r\n0 = Blue, 1 = Orange, 2 = Dark Green, 3 = Cyan, 4 = Magenta, 5 = Yellow, \r\n" +
        "6 = Gray, 7 = White, F = Off. \r\n\r\n");
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 452);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(16, 156);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(788, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x0020 - Single packet (old format + 4 bytes). Encode single data packet centered" +
    " on mag stripe.  *San Antonio, TX only";
            this.toolTip2.SetToolTip(this.checkBox6, "Single packet (old format + 4 bytes). Encode single data packet centered on mag\r\n" +
        "stripe San Antonio, TX only");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(16, 425);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(517, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Enable passback for Value cards (follow same rules as Period Pass)";
            this.toolTip2.SetToolTip(this.checkBox16, "Enable passback for Value cards (follow same rules as Period Pass) \r\nPertains to " +
        "ALL Smart cards. This setting is for SMART CARDS only. \r\nMagnetic cards are hand" +
        "led in OptionFlags24. ");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(16, 397);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(510, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 – Enable passback for Ride cards (follow same rules as Period Pass)";
            this.toolTip2.SetToolTip(this.checkBox15, "Enable passback for Ride cards (follow same rules as Period Pass)\r\nPertains to AL" +
        "L Smart cards. This setting is for SMART CARDS only. \r\nMagnetic cards are handle" +
        "d in OptionFlags24. ");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(16, 372);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(373, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Enable laser TRiM stock indicator (Fast Fare)";
            this.toolTip2.SetToolTip(this.checkBox14, "Enable laser TRiM stock indicator (Fast Fare)");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags18Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 512);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags18Form";
            this.Text = "OptionFlag18";
            this.toolTip2.SetToolTip(this, "(Recommended: 0x10C0, Default: 0x0000)");
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}