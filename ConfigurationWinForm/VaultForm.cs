﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class VaultForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public int  LockOutId = 0;
        int vltid = 0;
        public uint hexValue, vltFlags;
        bool formLoaded = false;
        public VaultForm()
        {
            InitializeComponent();
        }

        public VaultForm(SplitForm sf)
        {
            int value;
            this.splitForm = sf;
            InitializeComponent();
            switch (splitForm.activeNode.Text.ToLower())
            {

                case "vlt2":
                    vltid = 1;
                    break;
                case "vlt3":
                    vltid = 2;
                    break;
                case "vl41":
                    vltid = 3;
                    break;
                case "vlt5":
                    vltid = 4;
                    break;
                case "vlt1":
                default:
                    vltid = 0;
                    break;
            }
            splitForm.GetComPortList(Port);
            Port.Text = splitForm.iniConfig.inifileData.vlt[vltid].Port;
            hexValue = Util.ParseValue(splitForm.iniConfig.inifileData.vlt[vltid].PortEnbMask);
            channel1cbx.Checked = false;
            channel2cbx.Checked = false;
            channel3cbx.Checked = false;
            channel4cbx.Checked = false;
            if ((hexValue & 0x01) > 0)
                channel1cbx.Checked = true;
            if ((hexValue & 0x02) > 0)
                channel2cbx.Checked = true;
            if ((hexValue & 0x04) > 0)
                channel3cbx.Checked = true;
            if ((hexValue & 0x08) > 0)
                channel4cbx.Checked = true;
            this.ChannelText.Text = "Channel [" + "0x" + hexValue.ToString("X2") + "]:";
            value = 0;
            Int32.TryParse(splitForm.iniConfig.inifileData.vlt[vltid].DebugLevel, out value);
            this.DebugLevel.Value = value;

            this.VaultFull.Text = splitForm.iniConfig.inifileData.vlt[vltid].VltFull;

            value = 1;
            Int32.TryParse(splitForm.iniConfig.inifileData.vlt[vltid].LockOut, out value);
            switch (value)
            {
                case 2:
                    LockOutId = 1;
                    break;
                case 3:
                    LockOutId = 2;
                    break;
                default:        
                    LockOutId = 0;
                    break;
            }
            LockOut.Text = (string)this.LockOut.Items[LockOutId];
            hexValue = Util.ParseValue(splitForm.iniConfig.inifileData.vlt[vltid].VltFlags);
            DebugProtocol.Checked = ((hexValue & 0x02) > 0);
            DebugFrames.Checked = ((hexValue & 0x04) > 0);
            WriteRawFile.Checked = ((hexValue & 0x10) > 0);
            formLoaded = true;
            // Update the hex value
            SetVaultFlagsHexValue();
        }

        void SetVaultFlagsHexValue()
        {
            if (formLoaded)
            {
                vaultFlagsText.Text = "Vault Flags  - [" + "0x" + hexValue.ToString("X2") + "]:";
            }
        }

        public void UpdateData(object sender, EventArgs e)
        {
            UpdateData();
        }

        public void UpdateData()
        {
            int value = 0;
            if (formLoaded)
            {
                // Port
                splitForm.iniConfig.inifileData.vlt[vltid].Port = Port.Text;

                // PortEnbMask
                hexValue=0;
                if (channel1cbx.Checked)
                    hexValue |= 0x01;
                if (channel2cbx.Checked)
                    hexValue |= 0x02;
                if (channel3cbx.Checked)
                    hexValue |= 0x04;
                if (channel4cbx.Checked)
                    hexValue |= 0x08;
                splitForm.iniConfig.inifileData.vlt[vltid].PortEnbMask = "0x" + hexValue.ToString("X2");
                ChannelText.Text = "Channel [" + "0x" + hexValue.ToString("X2") + "]:";
                splitForm.iniConfig.inifileData.vlt[vltid].DebugLevel = DebugLevel.Value.ToString();
                splitForm.iniConfig.inifileData.vlt[vltid].VltFull = VaultFull.Text;

                // Lockout ID
                value = LockOut.SelectedIndex;
                switch (value)
                {
                    case 1:
                        LockOutId = 2;
                        break;
                    case 2:
                        LockOutId = 3;
                        break;
                    default:
                        LockOutId = 0;
                        break;
                }
                LockOut.Text = (string)this.LockOut.Items[value];
                splitForm.iniConfig.inifileData.vlt[vltid].LockOut = LockOutId.ToString(); ;

                hexValue = 0;
                if (this.DebugProtocol.Checked)
                    hexValue |= 0x02;
                if (this.DebugFrames.Checked)
                    hexValue |=  0x04;
                if (this.WriteRawFile.Checked)
                    hexValue |= 0x10;
                splitForm.iniConfig.inifileData.vlt[vltid].VltFlags = "0x" + hexValue.ToString("X2");
                SetVaultFlagsHexValue();
                splitForm.SetChanged();
                splitForm.UpdateTree();
            }
        }
    }
}
