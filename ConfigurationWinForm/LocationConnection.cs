﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using Microsoft.Win32;

namespace ConfigurationWinForm
{
    public class MachineLocation
    {
        [DllImport("advapi32.dll")]
        public static extern bool LogonUser(string name, string domain, string pass,
                                            int logType, int logpv, out IntPtr pht);

        public string username;            // User
        public string password;            // Password
        public string remoteMachine;       // Machine Name or IP address
        public string localPath;
        public string remotePath;             // Path to 
        public string remoteDrive;
        public string fname;
        public bool   bRememberMe;

        public MachineLocation()
        {
        }

        public MachineLocation(int location)
        {
            username="Administrator";            // User
            password="P@ssword";            // Password
            remoteMachine=@"10.1.1.179";       // Machine Name or IP address
            localPath = @"C:\GFI\cnf";             // Path to 
            remotePath = @"\GFI\cnf";
            remoteDrive=@"$C";
            fname=@"gfi.ini";
            bRememberMe = false;    //Remember user/password, etc.

            //ReadRemoteConfiguration();
            test();
        }


        private void ReadRemoteConfiguration()
        {
            IntPtr ptr;
            RegistryKey registryKey;            // LogonUser("username", "remotemachine", "password", 2, 0, out ptr);
            RegistryKey rk;
            string remotesrc, remotebak, localsrc;
            //LogonUser("Administrator", "10.1.1.179", "P@ssword", 9, 0, out ptr);
            try
            {
                //password = "hello";
                if (!LogonUser(username, remoteMachine, password, 9, 0, out ptr))
                {
                    MessageBox.Show("Logon Error: ");
                    return;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Logon Error: " + e.Message);
                return;
            }
            try
            {

                WindowsIdentity windowsIdentity = new WindowsIdentity(ptr);
                var impersonationContext = windowsIdentity.Impersonate();
            }
            catch (Exception e)
            {
                MessageBox.Show("Impersonation Error: " + e.Message);
                return;
            }
            try
            {
                rk = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, remoteMachine);
            }
            catch (Exception e)
            {
                MessageBox.Show("Remote Registry Open Error: " + e.Message);
                return;
            }            
            try
            {
                localPath = "";
                registryKey = rk.OpenSubKey(@"Software\Wow6432Node\GFI Genfare\Global");
                localPath = registryKey.GetValue("Base Directory").ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show("Remote Key Read Error: " + e.Message);
                return;
            }
            remotesrc= @"\\"+remoteMachine+@"\"+ remoteDrive+remotePath+@"\"+ fname;
            remotebak= @"\\"+remoteMachine+@"\"+ remoteDrive+remotePath+@"\"+ fname+".bak";
            localsrc =localPath + @"\" + fname;

            //File.Copy(remotesrc, remotebak);
            //File.Delete(remotesrc);
            //File.Copy(localsrc, remotesrc);


            //File.Copy(@"\\10.1.1.179\D$\temp\test.txt", @"\\10.1.1.179\D$\temp\test.bak");
            //File.Delete(@"\\10.1.1.179\D$\temp\test.txt");
            //File.Copy(@"C:\temp\test.txt", @"\\10.1.1.179\D$\temp\test.txt");

        }
/////////////////////////////////////////////////////////////////////////////
        private void test()
        {

            IntPtr ptr;
            // LogonUser("username", "remotemachine", "password", 2, 0, out ptr);
            LogonUser("Administrator", "10.1.1.179", "P@ssword", 9, 0, out ptr);
            WindowsIdentity windowsIdentity = new WindowsIdentity(ptr);
            var impersonationContext = windowsIdentity.Impersonate();

            createReg("10.1.1.179");
            File.Copy(@"\\10.1.1.179\D$\temp\test.txt", @"\\10.1.1.179\D$\temp\test.bak");
            File.Delete(@"\\10.1.1.179\D$\temp\test.txt");
            File.Copy(@"C:\temp\test.txt", @"\\10.1.1.179\D$\temp\test.txt");

            //File.Copy(@"C:\temp\test.txt", @"\\10.1.1.179\C$\temp\test.txt");
        }

        public void createReg(string machineName)
        {
            // key in the registry of this machine.
            RegistryKey rk = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, machineName);
            //RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
            // send the Installed software to database 
            getInstalledSoftware(rk);
        }

        public void getInstalledSoftware(RegistryKey rkey)
        {
            string strVal;
            RegistryKey registryKey;
            try
            {
                //registryKey = rkey.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
                registryKey = rkey.OpenSubKey(@"Software\GFI Genfare");
                strVal = registryKey.GetValue(@"Global\Base Directory").ToString();

            }

            catch (Exception e)
            {
                //display message and exit
                MessageBox.Show("Error: " + e.Message);
                return;
            }
        }
///////////////////////////////////////////////////////////////////////////
    }

}
