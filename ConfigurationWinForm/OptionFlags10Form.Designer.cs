﻿namespace ConfigurationWinForm
{
    partial class OptionFlags10Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.option_flagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.clearCbxCombo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.option_flagsGridTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(22, 26);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(348, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - Display possible captured card message.";
            this.toolTip2.SetToolTip(this.checkBox1, "Display possible captured card message.");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(22, 85);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(405, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x0008 - Say “Check Denomination” when bill gets rejected.";
            this.toolTip2.SetToolTip(this.checkBox4, "0x0008 - Say “Check Denomination” when bill gets rejected.");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(22, 110);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(585, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x0010 - Use the New 2011 format for UL smart cards. Does not convert from old fo" +
    "rmat";
            this.toolTip2.SetToolTip(this.checkBox5, "0x0010 - Use the New 2011 format for UL smart cards. Does not convert from old fo" +
        "rmat");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(22, 160);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(626, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x0040 - Disable coin and bill validators until a selection is made when in “Mini" +
    "-TVM” mode.nds";
            this.toolTip2.SetToolTip(this.checkBox7, "0x0040 - Disable coin and bill validators until a selection is made when in “Mini" +
        "-TVM” mode.nds");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(22, 185);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(422, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Allow a smart card to be used to unlock the CB door.;";
            this.toolTip2.SetToolTip(this.checkBox8, "0x0080 – Allow a smart card to be used to unlock the CB door.;");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(22, 210);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(236, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – “Warble” for pass back;";
            this.toolTip2.SetToolTip(this.checkBox9, "0x0100 – “Warble” for pass back;");
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(22, 235);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(373, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Say “No Value on Card” when pass is expired";
            this.toolTip2.SetToolTip(this.checkBox10, "0x0200 – Say “No Value on Card” when pass is expired");
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(22, 260);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(386, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – Enable passenger buttons to be used for fares.";
            this.toolTip2.SetToolTip(this.checkBox11, "0x0400 – Enable passenger buttons to be used for fares.");
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(22, 285);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(440, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – Change error display from \'weekend\' to \'inv sat\' \'inv sun\'";
            this.toolTip2.SetToolTip(this.checkBox12, "0x0800 – Change error display from \'weekend\' to \'inv sat\' \'inv sun\'");
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(22, 310);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(466, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Disable the “pre-staging” of cards from TRiM/LUCC cassette";
            this.toolTip2.SetToolTip(this.checkBox13, "0x1000 – Disable the “pre-staging” of cards from TRiM/LUCC cassette");
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // option_flagsGridTitle
            // 
            this.option_flagsGridTitle.Controls.Add(this.label1);
            this.option_flagsGridTitle.Controls.Add(this.clearCbxCombo);
            this.option_flagsGridTitle.Controls.Add(this.button1);
            this.option_flagsGridTitle.Controls.Add(this.checkBox6);
            this.option_flagsGridTitle.Controls.Add(this.checkBox16);
            this.option_flagsGridTitle.Controls.Add(this.checkBox15);
            this.option_flagsGridTitle.Controls.Add(this.checkBox14);
            this.option_flagsGridTitle.Controls.Add(this.checkBox4);
            this.option_flagsGridTitle.Controls.Add(this.checkBox8);
            this.option_flagsGridTitle.Controls.Add(this.checkBox1);
            this.option_flagsGridTitle.Controls.Add(this.checkBox9);
            this.option_flagsGridTitle.Controls.Add(this.checkBox10);
            this.option_flagsGridTitle.Controls.Add(this.checkBox11);
            this.option_flagsGridTitle.Controls.Add(this.checkBox7);
            this.option_flagsGridTitle.Controls.Add(this.checkBox12);
            this.option_flagsGridTitle.Controls.Add(this.checkBox5);
            this.option_flagsGridTitle.Controls.Add(this.checkBox13);
            this.option_flagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.option_flagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.option_flagsGridTitle.Name = "option_flagsGridTitle";
            this.option_flagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.option_flagsGridTitle.Size = new System.Drawing.Size(918, 447);
            this.option_flagsGridTitle.TabIndex = 0;
            this.option_flagsGridTitle.TabStop = false;
            this.option_flagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "When to clear cashbox totals:";
            // 
            // clearCbxCombo
            // 
            this.clearCbxCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clearCbxCombo.FormattingEnabled = true;
            this.clearCbxCombo.Items.AddRange(new object[] {
            "Clear cashbox totals when IR probed - (0x0006)",
            "Clear cashbox totals when door opens - (0x0004)",
            "Clear cashbox totals on door open fault - (0x0002)"});
            this.clearCbxCombo.Location = new System.Drawing.Point(225, 51);
            this.clearCbxCombo.Name = "clearCbxCombo";
            this.clearCbxCombo.Size = new System.Drawing.Size(407, 24);
            this.clearCbxCombo.TabIndex = 17;
            this.clearCbxCombo.SelectedIndexChanged += new System.EventHandler(this.clearCbxCombo_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 412);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(22, 135);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(514, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x0020 - Do not require pass code to unlock CB door if smart card was used.";
            this.toolTip2.SetToolTip(this.checkBox6, "0x0020 - Do not require pass code to unlock CB door if smart card was used.");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(22, 385);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(548, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – These 2bits are used to control the ability to “accept” cards into the T" +
    "RiM";
            this.toolTip2.SetToolTip(this.checkBox16, "0x8000 – These 2bits are used to control the ability to “accept” cards into the T" +
        "RiM");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(22, 360);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(544, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 –These 2bits are used to control the ability to “accept” cards into the TR" +
    "iM";
            this.toolTip2.SetToolTip(this.checkBox15, "0x4000 –These 2bits are used to control the ability to “accept” cards into the TR" +
        "iM");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(22, 335);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(888, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Set farebox as a “test box”, used to select the test keys.Tucson = “Test" +
    " Keys” else “Marketing Keys. Winnipeg logic is reversed.";
            this.toolTip2.SetToolTip(this.checkBox14, "0x2000 – Set farebox as a “test box”, used to select the test keys.\r\nTucson = “Te" +
        "st Keys” else “Marketing Keys. Winnipeg logic is reversed.");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags10Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 469);
            this.Controls.Add(this.option_flagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags10Form";
            this.Text = "OptionFlag10";
            this.option_flagsGridTitle.ResumeLayout(false);
            this.option_flagsGridTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox option_flagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox clearCbxCombo;
    }
}