﻿namespace ConfigurationWinForm
{
    partial class SplitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplitForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.configTreeView = new System.Windows.Forms.TreeView();
            this.configTabControl = new System.Windows.Forms.TabControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.openInNotepad = new System.Windows.Forms.ToolStripMenuItem();
            this.findInConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.findInConfigurationStop = new System.Windows.Forms.ToolStripMenuItem();
            this.compareConfigurationFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolUpdateLocation = new System.Windows.Forms.ToolStripMenuItem();
            this.querytoolStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.restartServicestoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startServiceTool = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServiceTool = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.showSingleFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.networkManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.location1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.location2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.location3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolSaveBtn = new System.Windows.Forms.ToolStripButton();
            this.toolClearBtn = new System.Windows.Forms.ToolStripButton();
            this.toolOpenInNotepad = new System.Windows.Forms.ToolStripButton();
            this.toolFind = new System.Windows.Forms.ToolStripButton();
            this.toolFindClose = new System.Windows.Forms.ToolStripButton();
            this.toolSearchDown = new System.Windows.Forms.ToolStripButton();
            this.toolSearchUp = new System.Windows.Forms.ToolStripButton();
            this.toolCompareFiles = new System.Windows.Forms.ToolStripButton();
            this.gfiIniSelected = new System.Windows.Forms.CheckBox();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.inittabSelected = new System.Windows.Forms.CheckBox();
            this.netconfigSelected = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.configTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.configTabControl);
            this.splitContainer1.Size = new System.Drawing.Size(1194, 603);
            this.splitContainer1.SplitterDistance = 195;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // configTreeView
            // 
            this.configTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configTreeView.Location = new System.Drawing.Point(0, 0);
            this.configTreeView.Margin = new System.Windows.Forms.Padding(2);
            this.configTreeView.Name = "configTreeView";
            this.configTreeView.Size = new System.Drawing.Size(195, 603);
            this.configTreeView.TabIndex = 0;
            this.configTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.configTreeView_AfterCheck);
            this.configTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.configTreeView_BeforeExpand);
            this.configTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.configTreeView_AfterSelect);
            this.configTreeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.configTreeView_NodeMouseClick);
            // 
            // configTabControl
            // 
            this.configTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configTabControl.Location = new System.Drawing.Point(0, 0);
            this.configTabControl.Margin = new System.Windows.Forms.Padding(2);
            this.configTabControl.Name = "configTabControl";
            this.configTabControl.SelectedIndex = 0;
            this.configTabControl.Size = new System.Drawing.Size(996, 603);
            this.configTabControl.TabIndex = 0;
            this.configTabControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.configTabControl_MouseClick);
            this.configTabControl.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.configTabControl_MouseDoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.locationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1194, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.saveToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.toolStripSeparator2,
            this.openInNotepad,
            this.findInConfiguration,
            this.findInConfigurationStop,
            this.compareConfigurationFiles,
            this.toolStripSeparator1,
            this.toolUpdateLocation,
            this.querytoolStripMenu,
            this.connectionMenu,
            this.toolStripSeparator5,
            this.restartServicestoolStripMenuItem,
            this.startServiceTool,
            this.stopServiceTool,
            this.toolStripMenuItem2,
            this.toolStripSeparator4,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.fileToolStripMenuItem.Text = "Configuration";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(281, 22);
            this.toolStripMenuItem1.Text = "Open";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolOpenFile_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.toolClearBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(278, 6);
            // 
            // openInNotepad
            // 
            this.openInNotepad.Name = "openInNotepad";
            this.openInNotepad.Size = new System.Drawing.Size(281, 22);
            this.openInNotepad.Text = "Open in Notepad";
            this.openInNotepad.Click += new System.EventHandler(this.openConfigInNotepad_Click);
            // 
            // findInConfiguration
            // 
            this.findInConfiguration.Name = "findInConfiguration";
            this.findInConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findInConfiguration.Size = new System.Drawing.Size(281, 22);
            this.findInConfiguration.Text = "Search";
            this.findInConfiguration.Click += new System.EventHandler(this.findTextInIniFile_Clock);
            // 
            // findInConfigurationStop
            // 
            this.findInConfigurationStop.Name = "findInConfigurationStop";
            this.findInConfigurationStop.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.findInConfigurationStop.Size = new System.Drawing.Size(281, 22);
            this.findInConfigurationStop.Text = "Quit search";
            // 
            // compareConfigurationFiles
            // 
            this.compareConfigurationFiles.Name = "compareConfigurationFiles";
            this.compareConfigurationFiles.Size = new System.Drawing.Size(281, 22);
            this.compareConfigurationFiles.Text = "Show Differences in Configuration Files";
            this.compareConfigurationFiles.Click += new System.EventHandler(this.compareConfiguration_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(278, 6);
            // 
            // toolUpdateLocation
            // 
            this.toolUpdateLocation.Name = "toolUpdateLocation";
            this.toolUpdateLocation.Size = new System.Drawing.Size(281, 22);
            this.toolUpdateLocation.Text = "Update Location Number";
            this.toolUpdateLocation.Click += new System.EventHandler(this.toolUpdateLocation_Click);
            // 
            // querytoolStripMenu
            // 
            this.querytoolStripMenu.Name = "querytoolStripMenu";
            this.querytoolStripMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.querytoolStripMenu.Size = new System.Drawing.Size(281, 22);
            this.querytoolStripMenu.Text = "Query Tool";
            this.querytoolStripMenu.Click += new System.EventHandler(this.querytoolStripMenu_Click);
            // 
            // connectionMenu
            // 
            this.connectionMenu.Name = "connectionMenu";
            this.connectionMenu.Size = new System.Drawing.Size(281, 22);
            this.connectionMenu.Text = "Connections";
            this.connectionMenu.Click += new System.EventHandler(this.connectionMenu_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(278, 6);
            // 
            // restartServicestoolStripMenuItem
            // 
            this.restartServicestoolStripMenuItem.Name = "restartServicestoolStripMenuItem";
            this.restartServicestoolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.restartServicestoolStripMenuItem.Text = "Restart GFI Init Service";
            this.restartServicestoolStripMenuItem.Click += new System.EventHandler(this.restartServicestoolStripMenuItem_Click);
            // 
            // startServiceTool
            // 
            this.startServiceTool.Name = "startServiceTool";
            this.startServiceTool.Size = new System.Drawing.Size(281, 22);
            this.startServiceTool.Text = "Start GFI Init Service";
            this.startServiceTool.Click += new System.EventHandler(this.startServiceTool_Click);
            // 
            // stopServiceTool
            // 
            this.stopServiceTool.Name = "stopServiceTool";
            this.stopServiceTool.Size = new System.Drawing.Size(281, 22);
            this.stopServiceTool.Text = "Stop GFI Init Service";
            this.stopServiceTool.Click += new System.EventHandler(this.stopServiceTool_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showSingleFormToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(281, 22);
            this.toolStripMenuItem2.Text = "Settings";
            // 
            // showSingleFormToolStripMenuItem
            // 
            this.showSingleFormToolStripMenuItem.Name = "showSingleFormToolStripMenuItem";
            this.showSingleFormToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.showSingleFormToolStripMenuItem.Text = "Show Single Form";
            this.showSingleFormToolStripMenuItem.Click += new System.EventHandler(this.showSingleFormToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(278, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(281, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // locationsToolStripMenuItem
            // 
            this.locationsToolStripMenuItem.Name = "locationsToolStripMenuItem";
            this.locationsToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.locationsToolStripMenuItem.Text = "Locations";
            // 
            // networkManagerToolStripMenuItem
            // 
            this.networkManagerToolStripMenuItem.Name = "networkManagerToolStripMenuItem";
            this.networkManagerToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // location1ToolStripMenuItem
            // 
            this.location1ToolStripMenuItem.Name = "location1ToolStripMenuItem";
            this.location1ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // location2ToolStripMenuItem
            // 
            this.location2ToolStripMenuItem.Name = "location2ToolStripMenuItem";
            this.location2ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // location3ToolStripMenuItem
            // 
            this.location3ToolStripMenuItem.Name = "location3ToolStripMenuItem";
            this.location3ToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.location3ToolStripMenuItem.Text = "Location 3";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolSaveBtn,
            this.toolClearBtn,
            this.toolOpenInNotepad,
            this.toolFind,
            this.toolFindClose,
            this.toolSearchDown,
            this.toolSearchUp,
            this.toolCompareFiles});
            this.toolStrip1.Location = new System.Drawing.Point(434, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(196, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolSaveBtn
            // 
            this.toolSaveBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolSaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("toolSaveBtn.Image")));
            this.toolSaveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSaveBtn.Name = "toolSaveBtn";
            this.toolSaveBtn.Size = new System.Drawing.Size(23, 22);
            this.toolSaveBtn.Text = "Write File";
            this.toolSaveBtn.ToolTipText = "Save Configuration";
            this.toolSaveBtn.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolClearBtn
            // 
            this.toolClearBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolClearBtn.Image = ((System.Drawing.Image)(resources.GetObject("toolClearBtn.Image")));
            this.toolClearBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolClearBtn.Name = "toolClearBtn";
            this.toolClearBtn.Size = new System.Drawing.Size(23, 22);
            this.toolClearBtn.Text = "toolStripButton1";
            this.toolClearBtn.ToolTipText = "Clear";
            this.toolClearBtn.Click += new System.EventHandler(this.toolClearBtn_Click);
            // 
            // toolOpenInNotepad
            // 
            this.toolOpenInNotepad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolOpenInNotepad.Image = ((System.Drawing.Image)(resources.GetObject("toolOpenInNotepad.Image")));
            this.toolOpenInNotepad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolOpenInNotepad.Name = "toolOpenInNotepad";
            this.toolOpenInNotepad.Size = new System.Drawing.Size(23, 22);
            this.toolOpenInNotepad.Text = "Open COnfiguration in Notepad";
            this.toolOpenInNotepad.ToolTipText = "Open in Notepad";
            this.toolOpenInNotepad.Click += new System.EventHandler(this.openConfigInNotepad_Click);
            // 
            // toolFind
            // 
            this.toolFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolFind.Image = ((System.Drawing.Image)(resources.GetObject("toolFind.Image")));
            this.toolFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFind.Name = "toolFind";
            this.toolFind.Size = new System.Drawing.Size(23, 22);
            this.toolFind.Text = "Search for text in Configuration";
            this.toolFind.Click += new System.EventHandler(this.toolFind_Click);
            // 
            // toolFindClose
            // 
            this.toolFindClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolFindClose.Image = ((System.Drawing.Image)(resources.GetObject("toolFindClose.Image")));
            this.toolFindClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFindClose.Name = "toolFindClose";
            this.toolFindClose.Size = new System.Drawing.Size(23, 22);
            this.toolFindClose.Text = "Quit search";
            this.toolFindClose.Click += new System.EventHandler(this.toolFindClose_Click);
            // 
            // toolSearchDown
            // 
            this.toolSearchDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolSearchDown.Enabled = false;
            this.toolSearchDown.Image = ((System.Drawing.Image)(resources.GetObject("toolSearchDown.Image")));
            this.toolSearchDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSearchDown.Name = "toolSearchDown";
            this.toolSearchDown.Size = new System.Drawing.Size(23, 22);
            this.toolSearchDown.Text = "Search down";
            this.toolSearchDown.Click += new System.EventHandler(this.toolSearchDown_Click);
            // 
            // toolSearchUp
            // 
            this.toolSearchUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolSearchUp.Enabled = false;
            this.toolSearchUp.Image = ((System.Drawing.Image)(resources.GetObject("toolSearchUp.Image")));
            this.toolSearchUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSearchUp.Name = "toolSearchUp";
            this.toolSearchUp.Size = new System.Drawing.Size(23, 22);
            this.toolSearchUp.Text = "Search up";
            this.toolSearchUp.Click += new System.EventHandler(this.toolSearchUp_Click);
            // 
            // toolCompareFiles
            // 
            this.toolCompareFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolCompareFiles.Image = ((System.Drawing.Image)(resources.GetObject("toolCompareFiles.Image")));
            this.toolCompareFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolCompareFiles.Name = "toolCompareFiles";
            this.toolCompareFiles.Size = new System.Drawing.Size(23, 22);
            this.toolCompareFiles.Text = "Show Differences in Configuration Files";
            this.toolCompareFiles.Click += new System.EventHandler(this.compareConfiguration_Click);
            // 
            // gfiIniSelected
            // 
            this.gfiIniSelected.AutoSize = true;
            this.gfiIniSelected.Checked = true;
            this.gfiIniSelected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gfiIniSelected.Location = new System.Drawing.Point(199, 1);
            this.gfiIniSelected.Margin = new System.Windows.Forms.Padding(2);
            this.gfiIniSelected.Name = "gfiIniSelected";
            this.gfiIniSelected.Size = new System.Drawing.Size(50, 17);
            this.gfiIniSelected.TabIndex = 3;
            this.gfiIniSelected.Text = "gfi.ini";
            this.gfiIniSelected.UseVisualStyleBackColor = true;
            this.gfiIniSelected.CheckedChanged += new System.EventHandler(this.gfiiniFile_Changed);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // inittabSelected
            // 
            this.inittabSelected.AutoSize = true;
            this.inittabSelected.Checked = true;
            this.inittabSelected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.inittabSelected.Location = new System.Drawing.Point(258, 1);
            this.inittabSelected.Margin = new System.Windows.Forms.Padding(2);
            this.inittabSelected.Name = "inittabSelected";
            this.inittabSelected.Size = new System.Drawing.Size(54, 17);
            this.inittabSelected.TabIndex = 4;
            this.inittabSelected.Text = "inittab";
            this.inittabSelected.UseVisualStyleBackColor = true;
            this.inittabSelected.CheckedChanged += new System.EventHandler(this.inittabFile_Changed);
            // 
            // netconfigSelected
            // 
            this.netconfigSelected.AutoSize = true;
            this.netconfigSelected.Checked = true;
            this.netconfigSelected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.netconfigSelected.Location = new System.Drawing.Point(321, 1);
            this.netconfigSelected.Margin = new System.Windows.Forms.Padding(2);
            this.netconfigSelected.Name = "netconfigSelected";
            this.netconfigSelected.Size = new System.Drawing.Size(88, 17);
            this.netconfigSelected.TabIndex = 5;
            this.netconfigSelected.Text = "netconfig.cfg";
            this.netconfigSelected.UseVisualStyleBackColor = true;
            this.netconfigSelected.CheckedChanged += new System.EventHandler(this.netconfigFile_Changed);
            // 
            // SplitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1194, 627);
            this.Controls.Add(this.netconfigSelected);
            this.Controls.Add(this.inittabSelected);
            this.Controls.Add(this.gfiIniSelected);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SplitForm";
            this.Text = " ";
            this.Activated += new System.EventHandler(this.SplitForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SplitForm_FormClosing);
            this.Load += new System.EventHandler(this.SplitForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        //private DropDownTreeView.DropDownTreeView configTreeView;
        private System.Windows.Forms.TreeView configTreeView;
        private System.Windows.Forms.TabControl configTabControl;
        private System.Windows.Forms.ToolStripMenuItem locationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem networkManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem location1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem location2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem location3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolSaveBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem showSingleFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolClearBtn;
        private System.Windows.Forms.ToolStripMenuItem openInNotepad;
        private System.Windows.Forms.ToolStripButton toolOpenInNotepad;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem findInConfiguration;
        private System.Windows.Forms.ToolStripButton toolFind;
        private System.Windows.Forms.ToolStripMenuItem compareConfigurationFiles;
        private System.Windows.Forms.ToolStripButton toolCompareFiles;
        private System.Windows.Forms.CheckBox gfiIniSelected;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.CheckBox netconfigSelected;
        private System.Windows.Forms.CheckBox inittabSelected;
        public System.Windows.Forms.ToolStripButton toolSearchDown;
        public System.Windows.Forms.ToolStripButton toolSearchUp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton toolFindClose;
        private System.Windows.Forms.ToolStripMenuItem findInConfigurationStop;
        private System.Windows.Forms.ToolStripMenuItem toolUpdateLocation;
        private System.Windows.Forms.ToolStripMenuItem querytoolStripMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem restartServicestoolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem startServiceTool;
        private System.Windows.Forms.ToolStripMenuItem stopServiceTool;
        private System.Windows.Forms.ToolStripMenuItem connectionMenu;
    }
}

