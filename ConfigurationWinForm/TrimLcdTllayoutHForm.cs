﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ConfigurationWinForm
{
    public partial class TrimLcdTllayoutHForm : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;

        public TrimLcdTllayoutHForm()
        {
            InitializeComponent();
        }
        public TrimLcdTllayoutHForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm(sf);
        }

        private void loadForm(SplitForm sf)
        {
            splitForm = sf;
            string pattern = @"(\\\S{1})";
            string[] header = splitForm.iniConfig.inifileData.LCDHeader.Split(',');
            if (header.Count()>0)
                this.lcdHeaderLine1txt.Text = header[0].ToString();
            else
                this.lcdHeaderLine1txt.Text="";
            if (header.Count() > 1)
                this.lcdHeaderLine2txt.Text = header[1].ToString();
            else
                this.lcdHeaderLine2txt.Text="";

            // set default values checked.
            this.radioButton13.Checked = true;
            this.radioButton4.Checked = true;
            this.radioButton8.Checked = true;
            this.radioButton9.Checked = true;
            this.radioButton11.Checked = true;
            this.radioButton12.Checked = true;

            // Read Theader1
            foreach (Match m in Regex.Matches(splitForm.iniConfig.inifileData.THeader1, pattern))
            {
                switch (m.Groups[1].Value)
                {
                    case "\\B": this.radioButton15.Checked=true;
                        break;
                    case "\\S": this.radioButton14.Checked=true;
                        break;
                    case "\\R": this.checkBox1.Checked=true;
                        break;
                    default: this.radioButton13.Checked = true;
                        break;
                } 
            }

            this.textBox1.Text = Regex.Replace(splitForm.iniConfig.inifileData.THeader1,pattern,"");

            // Read Theader2
            foreach (Match m in Regex.Matches(splitForm.iniConfig.inifileData.THeader2, pattern))
            {
                switch (m.Groups[1].Value)
                {
                    case "\\B": this.radioButton2.Checked = true;
                        break;
                    case "\\S": this.radioButton3.Checked = true;
                        break;
                    case "\\R": this.checkBox2.Checked = true;
                        break;
                    default: this.radioButton4.Checked = true;
                        break;
                }
            }

            this.textBox2.Text = Regex.Replace(splitForm.iniConfig.inifileData.THeader2, pattern, "");

            // Read Theader3
            foreach (Match m in Regex.Matches(splitForm.iniConfig.inifileData.THeader3, pattern))
            {
                switch (m.Groups[1].Value)
                {
                    case "\\B": this.radioButton6.Checked = true;
                        break;
                    case "\\S": this.radioButton7.Checked = true;
                        break;
                    case "\\R": this.checkBox3.Checked = true;
                        break;
                    default: this.radioButton8.Checked = true;
                        break;
                }
            }
            this.textBox3.Text = Regex.Replace(splitForm.iniConfig.inifileData.THeader3, pattern, "");

            // Read VErr_prnt
            foreach (Match m in Regex.Matches(splitForm.iniConfig.inifileData.VErr_Prnt, pattern))
            {
                switch (m.Groups[1].Value)
                {
                    case "\\N": this.radioButton9.Checked = true;
                        break;
                    case "\\S": this.radioButton5.Checked = true;
                        break;
                    case "\\L": this.radioButton11.Checked = true;
                        break;
                    case "\\P": this.radioButton10.Checked = true;
                        break;
                    case "\\V": this.radioButton12.Checked = true;
                        break;
                    case "\\R": this.radioButton1.Checked = true;
                        break;
                    default: this.radioButton9.Checked = true;
                        break;
                }
            }
            this.textBox4.Text = Regex.Replace(splitForm.iniConfig.inifileData.VErr_Prnt, pattern, "");
            this.textBox5.Text = splitForm.iniConfig.inifileData.TLayout;
            formLoaded = true;
        }

        private void saveForm(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                //lcdHeader
                splitForm.iniConfig.inifileData.LCDHeader = this.lcdHeaderLine1txt.Text + "," + this.lcdHeaderLine2txt.Text;
                //write Theader1
                if(this.radioButton15.Checked)
                    splitForm.iniConfig.inifileData.THeader1  = "\\B"+this.textBox1.Text;
                else if(this.radioButton14.Checked)
                    splitForm.iniConfig.inifileData.THeader1 = "\\S" + this.textBox1.Text;
                else
                    splitForm.iniConfig.inifileData.THeader1 = this.textBox1.Text;

                if (this.checkBox1.Checked)

                    splitForm.iniConfig.inifileData.THeader1 = "\\R" + splitForm.iniConfig.inifileData.THeader1;

                //write Theader2
                if (this.radioButton2.Checked)
                    splitForm.iniConfig.inifileData.THeader2 = "\\B" + this.textBox2.Text;
                else if (this.radioButton3.Checked)
                    splitForm.iniConfig.inifileData.THeader2 = "\\S" + this.textBox2.Text;
                else
                    splitForm.iniConfig.inifileData.THeader2 = this.textBox2.Text;

                if (this.checkBox2.Checked)

                    splitForm.iniConfig.inifileData.THeader2 = "\\R" + splitForm.iniConfig.inifileData.THeader2;

                //write Theader3
                if (this.radioButton6.Checked)
                    splitForm.iniConfig.inifileData.THeader3 = "\\B" + this.textBox3.Text;
                else if (this.radioButton7.Checked)        
                    splitForm.iniConfig.inifileData.THeader3 = "\\S" + this.textBox3.Text;
                else                                       
                    splitForm.iniConfig.inifileData.THeader3 = this.textBox3.Text;

                if (this.checkBox3.Checked)

                    splitForm.iniConfig.inifileData.THeader3 = "\\R" + splitForm.iniConfig.inifileData.THeader3;

                //write VErr_Prnt
                // Font size
                if (this.radioButton9.Checked)
                    splitForm.iniConfig.inifileData.VErr_Prnt = this.textBox4.Text + "\\N";
                else if (this.radioButton5.Checked)
                    splitForm.iniConfig.inifileData.VErr_Prnt = this.textBox4.Text + "\\S";
                else                
                    splitForm.iniConfig.inifileData.VErr_Prnt = this.textBox4.Text;

                //Orientation
                if (this.radioButton11.Checked)
                    splitForm.iniConfig.inifileData.VErr_Prnt += "\\L";
                else
                    splitForm.iniConfig.inifileData.VErr_Prnt += "\\P";
                
                //Video
                if (this.radioButton12.Checked)
                    splitForm.iniConfig.inifileData.VErr_Prnt += "\\V";
                else
                    splitForm.iniConfig.inifileData.VErr_Prnt += "\\R";

                splitForm.iniConfig.inifileData.TLayout = this.textBox5.Text;
                splitForm.SetChanged();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox5.Text = "_ I B R D E";
        }
    }
}
