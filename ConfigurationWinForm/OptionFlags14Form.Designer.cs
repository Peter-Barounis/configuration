﻿namespace ConfigurationWinForm
{
    partial class OptionFlags14Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.securityCodeText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.optionflagsGridTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.securityCodeText);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(572, 148);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            this.toolTip2.SetToolTip(this.optionflagsGridTitle, "0x0001 - 0xFFFF – Used for Ticket Security Code.");
            // 
            // securityCodeText
            // 
            this.securityCodeText.Location = new System.Drawing.Point(15, 36);
            this.securityCodeText.Name = "securityCodeText";
            this.securityCodeText.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.securityCodeText.Size = new System.Drawing.Size(123, 22);
            this.securityCodeText.TabIndex = 19;
            this.securityCodeText.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(381, 68);
            this.label1.TabIndex = 18;
            this.label1.Text = "0x0001 - 0xFFFF – Used for Ticket Security Code.\r\n\r\nNote: Option flag 13 (00xF000" +
    ") high order nibble is also set\r\n          by this option.                      " +
    "        ";
            // 
            // OptionFlags14Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 177);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags14Form";
            this.Text = "OptionFlag14";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.TextBox securityCodeText;


    }
}