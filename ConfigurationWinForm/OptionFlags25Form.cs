﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags25Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public uint optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        public OptionFlags25Form()
        {
            InitializeComponent();
        }

        public OptionFlags25Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[24]);

            numericUpDown1.Value = optionflag & 0x000F;
            numericUpDown2.Value = (optionflag & 0x00F0)>>4;
            numericUpDown3.Value = (optionflag & 0x0F00)>>8;
            numericUpDown4.Value = (optionflag & 0xF000)>>12;
            formLoaded = true;
            setHexValue();
        }

        private void setHexValue()
        {
            if (formLoaded)
            {
                optionflag = (uint)numericUpDown1.Value;
                optionflag |= (uint)(((uint)numericUpDown2.Value) << 4);
                optionflag |= (uint)(((uint)numericUpDown3.Value) << 8);
                optionflag |= (uint)(((uint)numericUpDown4.Value) << 12);

                optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
                splitForm.iniConfig.inifileData.optionFlags[24] = "0x" + optionflag.ToString("X4");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setHexValue();
        }

        private void UpdateSmartCardValues(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                setHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
