﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class ProbeServerWiFiSettingsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formLoaded = false;
        public ToolTip toolTip1 = new ToolTip();

        public ProbeServerWiFiSettingsForm()
        {
            InitializeComponent();
        }

        public ProbeServerWiFiSettingsForm(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();
            LoadData();
            formLoaded = true;
        }
        
        public void LoadData()
        {
            int value = 0;
            Prb_DebugLevel.Value = Util.ParseValue(splitForm.iniConfig.inifileData.Prb_DebugLevel);
            Prb_Command.Text=splitForm.iniConfig.inifileData.Prb_Command;
            PrbEvntThreshold.Text=splitForm.iniConfig.inifileData.Prb_PrbEvntThreshold;
            PrbTrnsThreshold.Text=splitForm.iniConfig.inifileData.Prb_PrbTrnsThreshold;
            NatTunnel.Text=splitForm.iniConfig.inifileData.Prb_NatTunnel;
            gcipAddressControl.Text = splitForm.netConfig.inifileData.GDS_GCIP;
            gctcpPortTextBox.Text = splitForm.netConfig.inifileData.GDS_GCTCPPort;
            Int32.TryParse(splitForm.iniConfig.inifileData.Prb_ProbeMode, out value);
            switch (value)
            {
                case 1:
                    OutOfService.Checked = false;
                    InService.Checked = true;
                    break;
                default:
                    OutOfService.Checked = true;
                    InService.Checked = false;
                    break;
            }
        }

        public void SaveData()
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.Prb_DebugLevel = Prb_DebugLevel.Value.ToString();
                splitForm.iniConfig.inifileData.Prb_Command = Prb_Command.Text;
                splitForm.iniConfig.inifileData.Prb_PrbEvntThreshold = PrbEvntThreshold.Text;
                splitForm.iniConfig.inifileData.Prb_PrbTrnsThreshold = PrbTrnsThreshold.Text;
                splitForm.iniConfig.inifileData.Prb_NatTunnel = NatTunnel.Text;
                // 0:This means that we will allow retrieving of transactions during wifi probing 
                // 1:This means that we will not allow retrieveing of transactions during wifi probing except
                //   after a power cycle of the farebox
                if (OutOfService.Checked)
                    splitForm.iniConfig.inifileData.Prb_ProbeMode = "0";
                else
                    splitForm.iniConfig.inifileData.Prb_ProbeMode = "1";
                splitForm.SetChanged();
            }
        }

        private void DebugLevel_ValueChanged(object sender, EventArgs e)
        {
            SaveData();
        }

        private void OutOfService_CheckedChanged(object sender, EventArgs e)
        {
            SaveData();
        }

        private void InService_CheckedChanged(object sender, EventArgs e)
        {
            SaveData();
        }

        private void PrbTextChanged(object sender, EventArgs e)
        {
            SaveData();
        }
    }
}
