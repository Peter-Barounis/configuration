﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags5Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public uint optionflag = 0;
        public uint smartCardType = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[21];
        public OptionFlags5Form()
        {
            InitializeComponent();            
        }

        public OptionFlags5Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();

            optionflag = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[4]);
            cbxArray[0] = checkBox1;
            cbxArray[1] = checkBox2;
            cbxArray[2] = checkBox3;
            cbxArray[3] = checkBox4;
            cbxArray[4] = checkBox5;
            cbxArray[5] = checkBox6;
            cbxArray[6] = checkBox7;
            cbxArray[7] = checkBox8;
            cbxArray[8] = checkBox9;
            cbxArray[9] = checkBox10;
            cbxArray[10] = checkBox11;
            cbxArray[11] = checkBox12;
            smartCardType = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[4]);
            smartCardType &= 0x000f;
            if (smartCardType > 9)
                smartCardReaderCombo.SelectedIndex = 0;
            else
            {
                if (smartCardType == 10)
                {
                    smartCardType = 10;
                }
                else
                {
                    if (smartCardType < 9)
                        smartCardReaderCombo.SelectedIndex = (int)((uint)9 - smartCardType);
                    else
                        smartCardReaderCombo.SelectedIndex = (int)((uint)10 - smartCardType);
                }
            }

            for (int i = 0; i < 12; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((optionflag  & 1<<(i + 4)) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            formLoaded = true;
            setHexValue();
        }

        private void setHexValue()
        {
            if (formLoaded)
            {
                smartCardType = (uint)smartCardReaderCombo.SelectedIndex;
                if (smartCardType == 0)
                    smartCardType = 0x000f;
                else
                {
                    if (smartCardType == 9)
                        smartCardType = 0x0000;
                    else
                    {
                        if (smartCardType == 1)
                            smartCardType = 0x0009;
                        else
                            smartCardType = 9 - smartCardType;
                    }
                }
                optionflag &= 0xfff0;
                optionflag |= smartCardType;
                optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
                splitForm.iniConfig.inifileData.optionFlags[4] = "0x" + optionflag.ToString("X4");
            }
        }

        private void updateFlag(CheckBox cbx)
        {
            if (formLoaded)
            {
                if (cbx.Checked)
                    optionflag |= ((uint)1 << (cbx.TabIndex + 3));
                else
                    optionflag &= ~((uint)1 << (cbx.TabIndex + 3));
                splitForm.SetChanged();
            }
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag((CheckBox)sender);
                setHexValue();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0006;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            this.checkBox1.Checked = Convert.ToBoolean((optionflag >> 4) & 0x0001);
            this.checkBox2.Checked = Convert.ToBoolean((optionflag >> 5) & 0x0001);
            this.checkBox3.Checked = Convert.ToBoolean((optionflag >> 6) & 0x0001);
            this.checkBox4.Checked = Convert.ToBoolean((optionflag >> 7) & 0x0001);
            this.checkBox5.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            this.checkBox6.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            this.checkBox7.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            this.checkBox8.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            this.checkBox9.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox10.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox11.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox12.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);
        }

        private void smartCardReaderChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.SetChanged();
                setHexValue();
            }
        }
    }
}
