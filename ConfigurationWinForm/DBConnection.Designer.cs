﻿namespace ConfigurationWinForm
{
    partial class DBConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConnectionType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Driver = new System.Windows.Forms.ComboBox();
            this.DBName = new System.Windows.Forms.TextBox();
            this.UserID = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxSingleLogin = new System.Windows.Forms.CheckBox();
            this.DisableSysconDatabase = new System.Windows.Forms.CheckBox();
            this.GDSGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SysconPassword = new System.Windows.Forms.TextBox();
            this.SysconUserID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SysconDBFFolderSelect = new System.Windows.Forms.Button();
            this.SysconDBFText = new System.Windows.Forms.TextBox();
            this.DBFFolderSelectBtn = new System.Windows.Forms.Button();
            this.DBFText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.startCmdTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NMSysconDBMScombo = new System.Windows.Forms.ComboBox();
            this.GDSSysconHost = new System.Windows.Forms.TextBox();
            this.GDSSysconEng = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.NMGroup = new System.Windows.Forms.GroupBox();
            this.foundLocations = new System.Windows.Forms.Label();
            this.NMGDSHost = new System.Windows.Forms.TextBox();
            this.NMGDSEngine = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gdsLocation = new System.Windows.Forms.NumericUpDown();
            this.NMLocations = new System.Windows.Forms.Label();
            this.NMSysconDSN = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DBFopenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.GDSGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.NMGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdsLocation)).BeginInit();
            this.SuspendLayout();
            // 
            // ConnectionType
            // 
            this.ConnectionType.FormattingEnabled = true;
            this.ConnectionType.Items.AddRange(new object[] {
            "ASA (Sybase)",
            "MSS (Microsoft SQL Server)"});
            this.ConnectionType.Location = new System.Drawing.Point(116, 9);
            this.ConnectionType.Margin = new System.Windows.Forms.Padding(2);
            this.ConnectionType.Name = "ConnectionType";
            this.ConnectionType.Size = new System.Drawing.Size(142, 21);
            this.ConnectionType.TabIndex = 0;
            this.ConnectionType.SelectedIndexChanged += new System.EventHandler(this.ConnectionType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Connection:";
            // 
            // databaseLabel
            // 
            this.databaseLabel.AutoSize = true;
            this.databaseLabel.Location = new System.Drawing.Point(5, 146);
            this.databaseLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(87, 13);
            this.databaseLabel.TabIndex = 2;
            this.databaseLabel.Text = "Database Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Driver:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "User ID:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 85);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Password";
            // 
            // Driver
            // 
            this.Driver.FormattingEnabled = true;
            this.Driver.Location = new System.Drawing.Point(67, 23);
            this.Driver.Margin = new System.Windows.Forms.Padding(2);
            this.Driver.Name = "Driver";
            this.Driver.Size = new System.Drawing.Size(272, 21);
            this.Driver.TabIndex = 8;
            this.Driver.SelectedIndexChanged += new System.EventHandler(this.Driver_SelectedIndexChanged);
            // 
            // DBName
            // 
            this.DBName.Location = new System.Drawing.Point(93, 142);
            this.DBName.Margin = new System.Windows.Forms.Padding(2);
            this.DBName.Name = "DBName";
            this.DBName.Size = new System.Drawing.Size(268, 20);
            this.DBName.TabIndex = 9;
            this.DBName.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // UserID
            // 
            this.UserID.Location = new System.Drawing.Point(67, 52);
            this.UserID.Margin = new System.Windows.Forms.Padding(2);
            this.UserID.Name = "UserID";
            this.UserID.Size = new System.Drawing.Size(272, 20);
            this.UserID.TabIndex = 10;
            this.UserID.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(67, 82);
            this.Password.Margin = new System.Windows.Forms.Padding(2);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(272, 20);
            this.Password.TabIndex = 11;
            this.Password.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(10, 415);
            this.Save.Margin = new System.Windows.Forms.Padding(2);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(56, 24);
            this.Save.TabIndex = 14;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(70, 415);
            this.Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(56, 24);
            this.Cancel.TabIndex = 15;
            this.Cancel.Text = "Close";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(131, 415);
            this.RefreshButton.Margin = new System.Windows.Forms.Padding(2);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(56, 24);
            this.RefreshButton.TabIndex = 16;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxSingleLogin);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DisableSysconDatabase);
            this.groupBox1.Controls.Add(this.Password);
            this.groupBox1.Controls.Add(this.UserID);
            this.groupBox1.Controls.Add(this.Driver);
            this.groupBox1.Location = new System.Drawing.Point(11, 41);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(387, 177);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection Parameters";
            // 
            // cbxSingleLogin
            // 
            this.cbxSingleLogin.AutoSize = true;
            this.cbxSingleLogin.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cbxSingleLogin.Location = new System.Drawing.Point(12, 108);
            this.cbxSingleLogin.Name = "cbxSingleLogin";
            this.cbxSingleLogin.Size = new System.Drawing.Size(334, 30);
            this.cbxSingleLogin.TabIndex = 21;
            this.cbxSingleLogin.Text = "SingleLogin:Use version 2 connection string. Data system version\r\n must be versio" +
    "n 3.8 or above.";
            this.cbxSingleLogin.UseVisualStyleBackColor = true;
            this.cbxSingleLogin.CheckedChanged += new System.EventHandler(this.cbxSingleLogin_CheckedChanged);
            // 
            // DisableSysconDatabase
            // 
            this.DisableSysconDatabase.AutoSize = true;
            this.DisableSysconDatabase.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.DisableSysconDatabase.Location = new System.Drawing.Point(13, 142);
            this.DisableSysconDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.DisableSysconDatabase.Name = "DisableSysconDatabase";
            this.DisableSysconDatabase.Size = new System.Drawing.Size(356, 30);
            this.DisableSysconDatabase.TabIndex = 20;
            this.DisableSysconDatabase.Text = "DisableSyscon: Do not use the Sybase \'syscon\' database for the \r\nGarage Data Syst" +
    "em. Security preferences stored in the \'gfi\' database.";
            this.DisableSysconDatabase.UseVisualStyleBackColor = true;
            this.DisableSysconDatabase.CheckedChanged += new System.EventHandler(this.DisableSysconDatabase_CheckedChanged);
            // 
            // GDSGroup
            // 
            this.GDSGroup.Controls.Add(this.groupBox2);
            this.GDSGroup.Controls.Add(this.DBFFolderSelectBtn);
            this.GDSGroup.Controls.Add(this.DBFText);
            this.GDSGroup.Controls.Add(this.label14);
            this.GDSGroup.Controls.Add(this.startCmdTxt);
            this.GDSGroup.Controls.Add(this.label2);
            this.GDSGroup.Controls.Add(this.NMSysconDBMScombo);
            this.GDSGroup.Controls.Add(this.GDSSysconHost);
            this.GDSGroup.Controls.Add(this.GDSSysconEng);
            this.GDSGroup.Controls.Add(this.label11);
            this.GDSGroup.Controls.Add(this.label10);
            this.GDSGroup.Controls.Add(this.label9);
            this.GDSGroup.Location = new System.Drawing.Point(11, 221);
            this.GDSGroup.Margin = new System.Windows.Forms.Padding(2);
            this.GDSGroup.Name = "GDSGroup";
            this.GDSGroup.Padding = new System.Windows.Forms.Padding(2);
            this.GDSGroup.Size = new System.Drawing.Size(779, 178);
            this.GDSGroup.TabIndex = 18;
            this.GDSGroup.TabStop = false;
            this.GDSGroup.Text = "GDS System Parameters";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SysconPassword);
            this.groupBox2.Controls.Add(this.SysconUserID);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.SysconDBFFolderSelect);
            this.groupBox2.Controls.Add(this.SysconDBFText);
            this.groupBox2.Location = new System.Drawing.Point(373, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(395, 119);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SYSCON Database";
            // 
            // SysconPassword
            // 
            this.SysconPassword.Location = new System.Drawing.Point(71, 76);
            this.SysconPassword.Margin = new System.Windows.Forms.Padding(2);
            this.SysconPassword.Name = "SysconPassword";
            this.SysconPassword.Size = new System.Drawing.Size(279, 20);
            this.SysconPassword.TabIndex = 30;
            this.SysconPassword.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // SysconUserID
            // 
            this.SysconUserID.Location = new System.Drawing.Point(71, 49);
            this.SysconUserID.Margin = new System.Windows.Forms.Padding(2);
            this.SysconUserID.Name = "SysconUserID";
            this.SysconUserID.Size = new System.Drawing.Size(279, 20);
            this.SysconUserID.TabIndex = 29;
            this.SysconUserID.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 80);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 53);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "User ID:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "DBF:";
            // 
            // SysconDBFFolderSelect
            // 
            this.SysconDBFFolderSelect.Location = new System.Drawing.Point(353, 17);
            this.SysconDBFFolderSelect.Name = "SysconDBFFolderSelect";
            this.SysconDBFFolderSelect.Size = new System.Drawing.Size(31, 30);
            this.SysconDBFFolderSelect.TabIndex = 26;
            this.SysconDBFFolderSelect.UseVisualStyleBackColor = true;
            this.SysconDBFFolderSelect.Click += new System.EventHandler(this.SysconDBFFolderSelect_Click);
            // 
            // SysconDBFText
            // 
            this.SysconDBFText.Location = new System.Drawing.Point(71, 22);
            this.SysconDBFText.Name = "SysconDBFText";
            this.SysconDBFText.Size = new System.Drawing.Size(279, 20);
            this.SysconDBFText.TabIndex = 25;
            this.SysconDBFText.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // DBFFolderSelectBtn
            // 
            this.DBFFolderSelectBtn.Location = new System.Drawing.Point(336, 94);
            this.DBFFolderSelectBtn.Name = "DBFFolderSelectBtn";
            this.DBFFolderSelectBtn.Size = new System.Drawing.Size(31, 30);
            this.DBFFolderSelectBtn.TabIndex = 23;
            this.DBFFolderSelectBtn.UseVisualStyleBackColor = true;
            this.DBFFolderSelectBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // DBFText
            // 
            this.DBFText.Location = new System.Drawing.Point(67, 99);
            this.DBFText.Name = "DBFText";
            this.DBFText.Size = new System.Drawing.Size(268, 20);
            this.DBFText.TabIndex = 22;
            this.DBFText.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "DBF:";
            // 
            // startCmdTxt
            // 
            this.startCmdTxt.Location = new System.Drawing.Point(93, 145);
            this.startCmdTxt.Name = "startCmdTxt";
            this.startCmdTxt.Size = new System.Drawing.Size(662, 20);
            this.startCmdTxt.TabIndex = 8;
            this.startCmdTxt.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Start Command:";
            // 
            // NMSysconDBMScombo
            // 
            this.NMSysconDBMScombo.FormattingEnabled = true;
            this.NMSysconDBMScombo.Items.AddRange(new object[] {
            "MSS",
            "ASA"});
            this.NMSysconDBMScombo.Location = new System.Drawing.Point(67, 73);
            this.NMSysconDBMScombo.Margin = new System.Windows.Forms.Padding(2);
            this.NMSysconDBMScombo.Name = "NMSysconDBMScombo";
            this.NMSysconDBMScombo.Size = new System.Drawing.Size(268, 21);
            this.NMSysconDBMScombo.TabIndex = 6;
            this.NMSysconDBMScombo.SelectedIndexChanged += new System.EventHandler(this.NMSysconDBMScombo_SelectedIndexChanged);
            // 
            // GDSSysconHost
            // 
            this.GDSSysconHost.Location = new System.Drawing.Point(67, 47);
            this.GDSSysconHost.Margin = new System.Windows.Forms.Padding(2);
            this.GDSSysconHost.Name = "GDSSysconHost";
            this.GDSSysconHost.Size = new System.Drawing.Size(268, 20);
            this.GDSSysconHost.TabIndex = 5;
            this.GDSSysconHost.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // GDSSysconEng
            // 
            this.GDSSysconEng.Location = new System.Drawing.Point(67, 20);
            this.GDSSysconEng.Margin = new System.Windows.Forms.Padding(2);
            this.GDSSysconEng.Name = "GDSSysconEng";
            this.GDSSysconEng.Size = new System.Drawing.Size(268, 20);
            this.GDSSysconEng.TabIndex = 4;
            this.GDSSysconEng.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 51);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "HOST:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 24);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "ENG:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 77);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "NM DBMS:";
            // 
            // NMGroup
            // 
            this.NMGroup.Controls.Add(this.DBName);
            this.NMGroup.Controls.Add(this.foundLocations);
            this.NMGroup.Controls.Add(this.databaseLabel);
            this.NMGroup.Controls.Add(this.NMGDSHost);
            this.NMGroup.Controls.Add(this.NMGDSEngine);
            this.NMGroup.Controls.Add(this.label13);
            this.NMGroup.Controls.Add(this.label12);
            this.NMGroup.Controls.Add(this.gdsLocation);
            this.NMGroup.Controls.Add(this.NMLocations);
            this.NMGroup.Controls.Add(this.NMSysconDSN);
            this.NMGroup.Controls.Add(this.label8);
            this.NMGroup.Location = new System.Drawing.Point(418, 41);
            this.NMGroup.Margin = new System.Windows.Forms.Padding(2);
            this.NMGroup.Name = "NMGroup";
            this.NMGroup.Padding = new System.Windows.Forms.Padding(2);
            this.NMGroup.Size = new System.Drawing.Size(373, 177);
            this.NMGroup.TabIndex = 19;
            this.NMGroup.TabStop = false;
            this.NMGroup.Text = "Network Manager Parameters";
            // 
            // foundLocations
            // 
            this.foundLocations.AutoSize = true;
            this.foundLocations.Location = new System.Drawing.Point(137, 56);
            this.foundLocations.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.foundLocations.Name = "foundLocations";
            this.foundLocations.Size = new System.Drawing.Size(107, 13);
            this.foundLocations.TabIndex = 11;
            this.foundLocations.Text = "3 Locations available";
            // 
            // NMGDSHost
            // 
            this.NMGDSHost.Location = new System.Drawing.Point(93, 112);
            this.NMGDSHost.Margin = new System.Windows.Forms.Padding(2);
            this.NMGDSHost.Name = "NMGDSHost";
            this.NMGDSHost.Size = new System.Drawing.Size(268, 20);
            this.NMGDSHost.TabIndex = 10;
            this.NMGDSHost.TextChanged += new System.EventHandler(this.Host_Changed);
            // 
            // NMGDSEngine
            // 
            this.NMGDSEngine.Location = new System.Drawing.Point(93, 82);
            this.NMGDSEngine.Margin = new System.Windows.Forms.Padding(2);
            this.NMGDSEngine.Name = "NMGDSEngine";
            this.NMGDSEngine.Size = new System.Drawing.Size(268, 20);
            this.NMGDSEngine.TabIndex = 9;
            this.NMGDSEngine.TextChanged += new System.EventHandler(this.engine_Changed);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(28, 116);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "HOST:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(34, 86);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "ENG:";
            // 
            // gdsLocation
            // 
            this.gdsLocation.Location = new System.Drawing.Point(92, 52);
            this.gdsLocation.Margin = new System.Windows.Forms.Padding(2);
            this.gdsLocation.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.gdsLocation.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.gdsLocation.Name = "gdsLocation";
            this.gdsLocation.Size = new System.Drawing.Size(40, 20);
            this.gdsLocation.TabIndex = 6;
            this.gdsLocation.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.gdsLocation.ValueChanged += new System.EventHandler(this.gdsLocation_ValueChanged);
            // 
            // NMLocations
            // 
            this.NMLocations.AutoSize = true;
            this.NMLocations.Location = new System.Drawing.Point(16, 56);
            this.NMLocations.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.NMLocations.Name = "NMLocations";
            this.NMLocations.Size = new System.Drawing.Size(51, 13);
            this.NMLocations.TabIndex = 5;
            this.NMLocations.Text = "Location:";
            // 
            // NMSysconDSN
            // 
            this.NMSysconDSN.Location = new System.Drawing.Point(93, 23);
            this.NMSysconDSN.Margin = new System.Windows.Forms.Padding(2);
            this.NMSysconDSN.Name = "NMSysconDSN";
            this.NMSysconDSN.Size = new System.Drawing.Size(268, 20);
            this.NMSysconDSN.TabIndex = 4;
            this.NMSysconDSN.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 27);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "DSN:";
            // 
            // DBFopenFileDialog
            // 
            this.DBFopenFileDialog.FileName = "openFileDialog1";
            // 
            // DBConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(806, 455);
            this.Controls.Add(this.NMGroup);
            this.Controls.Add(this.GDSGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ConnectionType);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "DBConnection";
            this.Text = "Database Connection Parameters";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GDSGroup.ResumeLayout(false);
            this.GDSGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.NMGroup.ResumeLayout(false);
            this.NMGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdsLocation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ConnectionType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox Driver;
        private System.Windows.Forms.TextBox DBName;
        private System.Windows.Forms.TextBox UserID;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox GDSGroup;
        private System.Windows.Forms.TextBox GDSSysconHost;
        private System.Windows.Forms.TextBox GDSSysconEng;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox NMGroup;
        private System.Windows.Forms.TextBox NMGDSHost;
        private System.Windows.Forms.TextBox NMGDSEngine;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown gdsLocation;
        private System.Windows.Forms.Label NMLocations;
        private System.Windows.Forms.TextBox NMSysconDSN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label foundLocations;
        private System.Windows.Forms.CheckBox DisableSysconDatabase;
        private System.Windows.Forms.ComboBox NMSysconDBMScombo;
        private System.Windows.Forms.TextBox startCmdTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DBFText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.OpenFileDialog DBFopenFileDialog;
        private System.Windows.Forms.Button DBFFolderSelectBtn;
        private System.Windows.Forms.CheckBox cbxSingleLogin;
        private System.Windows.Forms.Button SysconDBFFolderSelect;
        private System.Windows.Forms.TextBox SysconDBFText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox SysconPassword;
        private System.Windows.Forms.TextBox SysconUserID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
    }
}