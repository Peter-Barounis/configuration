﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class WaitDlg : Form
    {
        public bool quit = false;
        //public WaitDlg()
        //{
        //    InitializeComponent();
        //}

        //public WaitDlg(Point p, string title, string msg)
        public WaitDlg(WaitDlgParameters parm)
        {
            InitializeComponent();
            this.waitMsg.Text = parm.msg;
            this.Text = parm.hdr;
            this.Location = parm.location;
            CenterToParent();            
            timer1.Interval = 500;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.waitMsg.Visible = !this.waitMsg.Visible;
            if (quit == true)
                Close();
        }

        private void cancelQuery_Click(object sender, EventArgs e)
        {

        }
    }
}
