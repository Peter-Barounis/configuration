﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags22Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags22Form()
        {
            InitializeComponent();
        }

        public OptionFlags22Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[21].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[21]);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[21] = "0x" + optionflag.ToString("X4");
            if (formLoaded)
                splitForm.SetChanged();

        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag(((CheckBox)sender));
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();            
        }
        private void setCheckboxChecked()
        {
            this.numericUpDown1.Value = ((optionflag>>0) & 0x001F);
            this.numericUpDown2.Value = ((optionflag>>5) & 0x0007);            
            this.numericUpDown3.Value = ((optionflag>>8) & 0x0017);
            this.numericUpDown4.Value = ((optionflag>>13)& 0x0007);            
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFFE0) | ((lmt1 << 0) & 0x001F));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }

        private void numericUpDown2_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown2_ValueChanged(sender, e);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            int lmt2 = ((int)numericUpDown2.Value);
            optionflag = ((optionflag & 0xFF1F) | ((lmt2 << 5) & 0x00E0));
            setHexValue();
        }
        private void numericUpDown3_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown3_ValueChanged(sender, e);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            int lmt3 = ((int)numericUpDown3.Value);
            optionflag = ((optionflag & 0xE0FF) | ((lmt3 << 8) & 0x1F00));
            setHexValue();
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            int lmt4 = ((int)numericUpDown4.Value);
            optionflag = ((optionflag & 0x1FFF) | ((lmt4 << 13) & 0xE000));
            setHexValue();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
