﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags28Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags28Form()
        {
            InitializeComponent();            
        }

        public OptionFlags28Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[27].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[27]);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[27] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();
        }
        private void setCheckboxChecked()
        {
            this.numericUpDown1.Value = ((optionflag >> 0) & 0x00FF);
            this.numericUpDown2.Value = ((optionflag >> 8) & 0x00FF);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFF00) | ((lmt1 << 0) & 0x00FF));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }

        private void numericUpDown2_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown2_ValueChanged(sender, e);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            int lmt2 = ((int)numericUpDown2.Value);
            optionflag = ((optionflag & 0x00FF) | ((lmt2 << 8) & 0xFF00));
            setHexValue();
        }
    }
}
