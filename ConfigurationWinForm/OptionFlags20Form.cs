﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags20Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;

        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[7];
        public OptionFlags20Form()
        {
            InitializeComponent();            
        }

        public OptionFlags20Form(SplitForm sf)
        {
            int tmp;
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[19]);
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox8;
            cbxArray[3] = this.checkBox9;
            cbxArray[4] = this.checkBox10;
            cbxArray[5] = this.checkBox11;
            cbxArray[6] = this.checkBox12;

            tmp = (optionflag & 0x7c) >> 2;
            if (tmp < 0 || tmp > 20)
                tmp = 0;
            defaultMask.Value = tmp;

            tmp = (optionflag & 0xf000) >> 12;
            if (tmp < 1 || tmp > 15)
                tmp = 1;
            lowStockIndicator.Value = tmp;

            setCheckboxChecked();
            updateFlag();
            formLoaded = true;
            setHexValue();
        }

        private void setHexValue()
        {
            // Set Default Mask
            optionflag &= ~0x7c;
            if ((defaultMask.Value < 0) || (defaultMask.Value > 20))
                defaultMask.Value = 0;
            optionflag |= (((int)defaultMask.Value) << 2);

            // Set Low Stock Alarm
            optionflag &= 0x0fff;
            if ((lowStockIndicator.Value < 1) || (lowStockIndicator.Value > 15))
                defaultMask.Value = 1;
            optionflag |= (((int)lowStockIndicator.Value) << 12);

            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[19] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag()
        {
            if (formLoaded)
            {
                for (int i = 0; i < 7; i++)
                {
                    if (cbxArray[i].Checked)
                        optionflag |= (1 << cbxArray[i].TabIndex);
                    else
                        optionflag &= ~(1 << cbxArray[i].TabIndex);
                }
                if (cbxArray[6].Checked)
                    cbxArray[5].Checked=true;
            }
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag();
            setHexValue();
        }
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    optionflag = 0x0000;
        //    setCheckboxChecked();
        //    setHexValue();
        //}
        private void setCheckboxChecked()
        {
            //int vbool = 0;
            int tmp;
            for (int i = 0; i < 7; i++)
            {
                tmp = optionflag;
                tmp &= (1 << cbxArray[i].TabIndex);
                if (tmp > 0)
                    cbxArray[i].Checked = true;
                else
                    cbxArray[i].Checked = false;
            }
            this.checkBox1.Checked = cbxArray[0].Checked;
            this.checkBox2.Checked = cbxArray[1].Checked;
            this.checkBox8.Checked = cbxArray[2].Checked;
            this.checkBox9.Checked = cbxArray[3].Checked;
            this.checkBox10.Checked = cbxArray[4].Checked;
            this.checkBox11.Checked = cbxArray[5].Checked;
            this.checkBox12.Checked = cbxArray[6].Checked;
            if (checkBox12.Checked)
                checkBox11.Checked = cbxArray[5].Checked = true;
        }

        private void defaultMask_ValueChanged(object sender, EventArgs e)
        {
            if (formLoaded)
                setHexValue();
        }

        private void lowStockIndicator_ValueChanged(object sender, EventArgs e)
        {
            if (formLoaded)
                setHexValue();
        }
    }
}
