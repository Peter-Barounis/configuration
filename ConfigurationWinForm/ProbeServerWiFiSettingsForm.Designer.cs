﻿namespace ConfigurationWinForm
{
    partial class ProbeServerWiFiSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Prb_DebugLevel = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gctcpPortTextBox = new System.Windows.Forms.TextBox();
            this.gcipAddressControl = new IPAddressControlLib.IPAddressControl();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.InService = new System.Windows.Forms.RadioButton();
            this.OutOfService = new System.Windows.Forms.RadioButton();
            this.NatTunnel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Prb_Command = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PrbTrnsThreshold = new System.Windows.Forms.TextBox();
            this.PrbEvntThreshold = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Prb_DebugLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DebugLevel:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "PrbEvntThreshold:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 99);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "PrbTrnsThreshold:";
            // 
            // Prb_DebugLevel
            // 
            this.Prb_DebugLevel.Location = new System.Drawing.Point(119, 17);
            this.Prb_DebugLevel.Margin = new System.Windows.Forms.Padding(2);
            this.Prb_DebugLevel.Name = "Prb_DebugLevel";
            this.Prb_DebugLevel.Size = new System.Drawing.Size(34, 20);
            this.Prb_DebugLevel.TabIndex = 9;
            this.Prb_DebugLevel.ValueChanged += new System.EventHandler(this.DebugLevel_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gctcpPortTextBox);
            this.groupBox1.Controls.Add(this.gcipAddressControl);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.NatTunnel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Prb_Command);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.PrbTrnsThreshold);
            this.groupBox1.Controls.Add(this.PrbEvntThreshold);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Prb_DebugLevel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(503, 317);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // gctcpPortTextBox
            // 
            this.gctcpPortTextBox.Location = new System.Drawing.Point(119, 173);
            this.gctcpPortTextBox.Name = "gctcpPortTextBox";
            this.gctcpPortTextBox.ReadOnly = true;
            this.gctcpPortTextBox.Size = new System.Drawing.Size(87, 20);
            this.gctcpPortTextBox.TabIndex = 31;
            // 
            // gcipAddressControl
            // 
            this.gcipAddressControl.AllowInternalTab = false;
            this.gcipAddressControl.AutoHeight = true;
            this.gcipAddressControl.BackColor = System.Drawing.SystemColors.Window;
            this.gcipAddressControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gcipAddressControl.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.gcipAddressControl.Location = new System.Drawing.Point(119, 147);
            this.gcipAddressControl.MinimumSize = new System.Drawing.Size(87, 20);
            this.gcipAddressControl.Name = "gcipAddressControl";
            this.gcipAddressControl.ReadOnly = true;
            this.gcipAddressControl.Size = new System.Drawing.Size(87, 20);
            this.gcipAddressControl.TabIndex = 30;
            this.gcipAddressControl.Text = "...";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Location = new System.Drawing.Point(212, 147);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(283, 54);
            this.label11.TabIndex = 29;
            this.label11.Text = "NOTE: The Garage Computer IP address and the Garage \r\nComputer Port are defined i" +
    "n the NETCONFIG section. If \r\nyou need to change the IP address or Port, then ch" +
    "ange \r\nit in the NETCONFIG section.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 177);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "GCTCPPort:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "GCIP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, -2);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "ProbeServer General WiFi Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.InService);
            this.groupBox2.Controls.Add(this.OutOfService);
            this.groupBox2.Location = new System.Drawing.Point(13, 214);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(459, 83);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Probe Mode (Transactions and Events)";
            // 
            // InService
            // 
            this.InService.AutoSize = true;
            this.InService.Location = new System.Drawing.Point(4, 46);
            this.InService.Margin = new System.Windows.Forms.Padding(2);
            this.InService.Name = "InService";
            this.InService.Size = new System.Drawing.Size(325, 30);
            this.InService.TabIndex = 1;
            this.InService.TabStop = true;
            this.InService.Text = "Do not retrieve transactions from the farebox during wifi probing.\r\n(except after" +
    " a power cycle of the farebox).";
            this.InService.UseVisualStyleBackColor = true;
            this.InService.CheckedChanged += new System.EventHandler(this.InService_CheckedChanged);
            // 
            // OutOfService
            // 
            this.OutOfService.AutoSize = true;
            this.OutOfService.Location = new System.Drawing.Point(4, 21);
            this.OutOfService.Margin = new System.Windows.Forms.Padding(2);
            this.OutOfService.Name = "OutOfService";
            this.OutOfService.Size = new System.Drawing.Size(350, 17);
            this.OutOfService.TabIndex = 0;
            this.OutOfService.TabStop = true;
            this.OutOfService.Text = "Receive transctions from the farebox duriing wifi probing. (**Default**)";
            this.OutOfService.UseVisualStyleBackColor = true;
            this.OutOfService.CheckedChanged += new System.EventHandler(this.OutOfService_CheckedChanged);
            // 
            // NatTunnel
            // 
            this.NatTunnel.Location = new System.Drawing.Point(119, 121);
            this.NatTunnel.Margin = new System.Windows.Forms.Padding(2);
            this.NatTunnel.Name = "NatTunnel";
            this.NatTunnel.Size = new System.Drawing.Size(87, 20);
            this.NatTunnel.TabIndex = 22;
            this.NatTunnel.TextChanged += new System.EventHandler(this.PrbTextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 125);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "NAT Tunnel:";
            // 
            // Prb_Command
            // 
            this.Prb_Command.Location = new System.Drawing.Point(119, 43);
            this.Prb_Command.Margin = new System.Windows.Forms.Padding(2);
            this.Prb_Command.Name = "Prb_Command";
            this.Prb_Command.Size = new System.Drawing.Size(344, 20);
            this.Prb_Command.TabIndex = 20;
            this.Prb_Command.TextChanged += new System.EventHandler(this.PrbTextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Command:";
            // 
            // PrbTrnsThreshold
            // 
            this.PrbTrnsThreshold.Location = new System.Drawing.Point(119, 95);
            this.PrbTrnsThreshold.Margin = new System.Windows.Forms.Padding(2);
            this.PrbTrnsThreshold.Name = "PrbTrnsThreshold";
            this.PrbTrnsThreshold.Size = new System.Drawing.Size(87, 20);
            this.PrbTrnsThreshold.TabIndex = 15;
            this.PrbTrnsThreshold.TextChanged += new System.EventHandler(this.PrbTextChanged);
            // 
            // PrbEvntThreshold
            // 
            this.PrbEvntThreshold.Location = new System.Drawing.Point(119, 69);
            this.PrbEvntThreshold.Margin = new System.Windows.Forms.Padding(2);
            this.PrbEvntThreshold.Name = "PrbEvntThreshold";
            this.PrbEvntThreshold.Size = new System.Drawing.Size(87, 20);
            this.PrbEvntThreshold.TabIndex = 14;
            this.PrbEvntThreshold.TextChanged += new System.EventHandler(this.PrbTextChanged);
            // 
            // ProbeServerWiFiSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 335);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ProbeServerWiFiSettingsForm";
            this.Text = "PRB General Settings";
            ((System.ComponentModel.ISupportInitialize)(this.Prb_DebugLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown Prb_DebugLevel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox PrbTrnsThreshold;
        private System.Windows.Forms.TextBox PrbEvntThreshold;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton InService;
        private System.Windows.Forms.RadioButton OutOfService;
        private System.Windows.Forms.TextBox NatTunnel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Prb_Command;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private IPAddressControlLib.IPAddressControl gcipAddressControl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox gctcpPortTextBox;

    }
}