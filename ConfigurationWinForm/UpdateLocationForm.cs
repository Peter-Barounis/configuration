﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class UpdateLocationForm : Form
    {
        public int location= 1;
        public UpdateLocationForm()
        {
            InitializeComponent();
        }

        public UpdateLocationForm(int currentLocation, string locName)
        {
            location = currentLocation;
            InitializeComponent();
            try
            {
                numericUpDownLocation.Value = currentLocation;
                locationNameText.Text = locName;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Location Invalid: " + ex.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            location = (int)numericUpDownLocation.Value;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
