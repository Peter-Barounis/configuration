﻿namespace ConfigurationWinForm
{
    partial class InittabForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InittabForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colProcessid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAction = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colCmdLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInittabEntry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.muxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vltToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.srvrToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gfiwsaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gfildrToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clntToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cubicDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cubicDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.probeServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fbxHttpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rfsrvrToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.gfiHttpServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userDefinedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolInsertEntry = new System.Windows.Forms.ToolStripButton();
            this.toolDeleteEntry = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AccessibleRole = System.Windows.Forms.AccessibleRole.Dialog;
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colActive,
            this.colProcessid,
            this.colAction,
            this.colCmdLine,
            this.colInittabEntry});
            this.dataGridView1.Location = new System.Drawing.Point(0, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1219, 537);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // colActive
            // 
            this.colActive.FillWeight = 24.46184F;
            this.colActive.HeaderText = "Active";
            this.colActive.Name = "colActive";
            this.colActive.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colActive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colProcessid
            // 
            this.colProcessid.FillWeight = 58.70333F;
            this.colProcessid.HeaderText = "Process Id";
            this.colProcessid.Name = "colProcessid";
            // 
            // colAction
            // 
            this.colAction.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colAction.FillWeight = 122.5332F;
            this.colAction.HeaderText = "Action";
            this.colAction.Items.AddRange(new object[] {
            "respawn",
            "once",
            "wait",
            "off"});
            this.colAction.Name = "colAction";
            this.colAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colCmdLine
            // 
            this.colCmdLine.FillWeight = 123.2654F;
            this.colCmdLine.HeaderText = "Command Line";
            this.colCmdLine.Name = "colCmdLine";
            // 
            // colInittabEntry
            // 
            this.colInittabEntry.FillWeight = 171.0362F;
            this.colInittabEntry.HeaderText = "INITTAB Entry";
            this.colInittabEntry.Name = "colInittabEntry";
            this.colInittabEntry.ReadOnly = true;
            this.colInittabEntry.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInittabEntry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1219, 26);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionToolStripMenuItem
            // 
            this.actionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.addToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.actionToolStripMenuItem.Name = "actionToolStripMenuItem";
            this.actionToolStripMenuItem.Size = new System.Drawing.Size(59, 22);
            this.actionToolStripMenuItem.Text = "Action";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(114, 6);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.muxToolStripMenuItem,
            this.vltToolStripMenuItem,
            this.cronToolStripMenuItem,
            this.srvrToolStripMenuItem,
            this.gfiwsaToolStripMenuItem,
            this.gfildrToolStripMenuItem,
            this.clntToolStripMenuItem,
            this.cubicDataToolStripMenuItem,
            this.cubicDataToolStripMenuItem1,
            this.toolStripSeparator3,
            this.probeServerToolStripMenuItem,
            this.fbxHttpToolStripMenuItem,
            this.rfsrvrToolStripMenuItem,
            this.toolStripSeparator4,
            this.gfiHttpServerToolStripMenuItem,
            this.userDefinedToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // muxToolStripMenuItem
            // 
            this.muxToolStripMenuItem.Name = "muxToolStripMenuItem";
            this.muxToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.muxToolStripMenuItem.Text = "Mux";
            this.muxToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // vltToolStripMenuItem
            // 
            this.vltToolStripMenuItem.Name = "vltToolStripMenuItem";
            this.vltToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.vltToolStripMenuItem.Text = "Vlt";
            this.vltToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // cronToolStripMenuItem
            // 
            this.cronToolStripMenuItem.Name = "cronToolStripMenuItem";
            this.cronToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.cronToolStripMenuItem.Text = "Cron";
            this.cronToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // srvrToolStripMenuItem
            // 
            this.srvrToolStripMenuItem.Name = "srvrToolStripMenuItem";
            this.srvrToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.srvrToolStripMenuItem.Text = "Srvr";
            this.srvrToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // gfiwsaToolStripMenuItem
            // 
            this.gfiwsaToolStripMenuItem.Name = "gfiwsaToolStripMenuItem";
            this.gfiwsaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.gfiwsaToolStripMenuItem.Text = "Gfiwsa";
            this.gfiwsaToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // gfildrToolStripMenuItem
            // 
            this.gfildrToolStripMenuItem.Name = "gfildrToolStripMenuItem";
            this.gfildrToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.gfildrToolStripMenuItem.Text = "Gfildr";
            this.gfildrToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // clntToolStripMenuItem
            // 
            this.clntToolStripMenuItem.Name = "clntToolStripMenuItem";
            this.clntToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.clntToolStripMenuItem.Text = "Clnt(GC)";
            this.clntToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // cubicDataToolStripMenuItem
            // 
            this.cubicDataToolStripMenuItem.Name = "cubicDataToolStripMenuItem";
            this.cubicDataToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.cubicDataToolStripMenuItem.Text = "Clnt(TVM/PEM)";
            this.cubicDataToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // cubicDataToolStripMenuItem1
            // 
            this.cubicDataToolStripMenuItem1.Name = "cubicDataToolStripMenuItem1";
            this.cubicDataToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.cubicDataToolStripMenuItem1.Text = "CubicData";
            this.cubicDataToolStripMenuItem1.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(174, 6);
            // 
            // probeServerToolStripMenuItem
            // 
            this.probeServerToolStripMenuItem.Name = "probeServerToolStripMenuItem";
            this.probeServerToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.probeServerToolStripMenuItem.Text = "ProbeServer";
            this.probeServerToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // fbxHttpToolStripMenuItem
            // 
            this.fbxHttpToolStripMenuItem.Name = "fbxHttpToolStripMenuItem";
            this.fbxHttpToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.fbxHttpToolStripMenuItem.Text = "FbxHttp";
            this.fbxHttpToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // rfsrvrToolStripMenuItem
            // 
            this.rfsrvrToolStripMenuItem.Name = "rfsrvrToolStripMenuItem";
            this.rfsrvrToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.rfsrvrToolStripMenuItem.Text = "rfsrvr";
            this.rfsrvrToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(174, 6);
            // 
            // gfiHttpServerToolStripMenuItem
            // 
            this.gfiHttpServerToolStripMenuItem.Name = "gfiHttpServerToolStripMenuItem";
            this.gfiHttpServerToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.gfiHttpServerToolStripMenuItem.Text = "GfiHttpServer";
            this.gfiHttpServerToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // userDefinedToolStripMenuItem
            // 
            this.userDefinedToolStripMenuItem.Name = "userDefinedToolStripMenuItem";
            this.userDefinedToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.userDefinedToolStripMenuItem.Text = "User Defined";
            this.userDefinedToolStripMenuItem.Click += new System.EventHandler(this.btnAddEntry_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteCurrentRow);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolInsertEntry,
            this.toolDeleteEntry});
            this.toolStrip1.Location = new System.Drawing.Point(86, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(87, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolInsertEntry
            // 
            this.toolInsertEntry.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolInsertEntry.Image = ((System.Drawing.Image)(resources.GetObject("toolInsertEntry.Image")));
            this.toolInsertEntry.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolInsertEntry.Name = "toolInsertEntry";
            this.toolInsertEntry.Size = new System.Drawing.Size(23, 22);
            this.toolInsertEntry.Text = "Insert Line";
            this.toolInsertEntry.Click += new System.EventHandler(this.toolInsertEntry_Click);
            // 
            // toolDeleteEntry
            // 
            this.toolDeleteEntry.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolDeleteEntry.Image = ((System.Drawing.Image)(resources.GetObject("toolDeleteEntry.Image")));
            this.toolDeleteEntry.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolDeleteEntry.Name = "toolDeleteEntry";
            this.toolDeleteEntry.Size = new System.Drawing.Size(23, 22);
            this.toolDeleteEntry.Text = "Delete entry";
            this.toolDeleteEntry.Click += new System.EventHandler(this.toolDeleteEntry_Click);
            // 
            // InittabForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1219, 563);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "InittabForm";
            this.Text = "Inittab";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem muxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vltToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem srvrToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gfiwsaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gfildrToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clntToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cubicDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cubicDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem probeServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fbxHttpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rfsrvrToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProcessid;
        private System.Windows.Forms.DataGridViewComboBoxColumn colAction;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCmdLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInittabEntry;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem gfiHttpServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userDefinedToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolInsertEntry;
        private System.Windows.Forms.ToolStripButton toolDeleteEntry;


    }
}