﻿namespace ConfigurationWinForm
{
    partial class OptionFlags15Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(19, 26);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(210, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - printing on LUCC’s.";
            this.toolTip2.SetToolTip(this.checkBox1, "Printing on LUCC’s.\r\nIf this is not set, the setting at each individual farebox i" +
        "s used. ");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(19, 53);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(399, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x0002 - Make sound if driver logged in with DESFire card.";
            this.toolTip2.SetToolTip(this.checkBox2, "0x0002 - Make sound if driver logged in with DESFire card.");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(19, 107);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(619, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x0008 - Enable auto bad listing of a value card that exceeds value defined in Op" +
    "tionFlags11.";
            this.toolTip2.SetToolTip(this.checkBox4, "Enable auto bad listing of a value card that exceeds value defined in OptionFlags" +
        "11. \r\nThis is used with OptionFlags11 ");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(19, 80);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(453, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x0004 - Enable firmware update via WiFi (Fast Fare / Fast Fare –e)";
            this.toolTip2.SetToolTip(this.checkBox3, "0x0004 - Enable firmware update via WiFi (Fast Fare / Fast Fare –e)");
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(19, 134);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(576, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x0010 - Enable ability to turn on Error Reporting (must enable at farebox) (FF /" +
    " FF –e).";
            this.toolTip2.SetToolTip(this.checkBox5, "Enable ability to turn on Error Reporting (must enable at farebox) (FF / FF –e).");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(19, 188);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(486, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x0040 - Enable alternate “Clear Jam” key. If set 5th button down on left.";
            this.toolTip2.SetToolTip(this.checkBox7, "Enable alternate “Clear Jam” key. If set 5th button down on left. 1 \r\nReplaces “D" +
        "UMP + C” function. ");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(19, 217);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(517, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Prompt to put card that was “Misread” in swipe reader into the TRiM.";
            this.toolTip2.SetToolTip(this.checkBox8, "Prompt to put card that was “Misread” in swipe reader into the TRiM.");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label2);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown2);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox6);
            this.optionflagsGridTitle.Controls.Add(this.checkBox4);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox3);
            this.optionflagsGridTitle.Controls.Add(this.checkBox7);
            this.optionflagsGridTitle.Controls.Add(this.checkBox5);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(710, 411);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 341);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(580, 51);
            this.label2.TabIndex = 20;
            this.label2.Text = "0x1000 - 0xF000 : Time interval mask, in multiples of 10 seconds, for transmittin" +
    "g UDP’s for\r\nwireless data probing.\r\n\r\n";
            this.toolTip2.SetToolTip(this.label2, "0x1000 - 0xF000 : Time interval mask, in multiples of 10 seconds, for transmittin" +
        "g UDP’s for\r\nwireless data probing.\r\n\r\n\r\n");
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown2.Location = new System.Drawing.Point(19, 347);
            this.numericUpDown2.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(73, 22);
            this.numericUpDown2.TabIndex = 19;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 265);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(537, 34);
            this.label1.TabIndex = 18;
            this.label1.Text = "0x0100 - 0x0F00 : Senior citizen age mask. Minimum is 56 and maximum is 70 Years\r" +
    "\n\r\n";
            this.toolTip2.SetToolTip(this.label1, "0x0100 - 0x0F00 : Senior citizen age mask. 0 = 65, 1 à 15 = 55 à 70. \r\n0 defaults" +
        " to 65 \r\nExample: 1 = 55, 2 = 56, ..., 9 = 63, 10 = 64 11 = 66, ..., E (14) = 69" +
        ", F (15) = 70 \r\n");
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(19, 265);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            56,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(73, 22);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.Value = new decimal(new int[] {
            56,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(513, 303);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(19, 161);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(582, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x0020 - Print expire date on Ride & Value cards, see VErr_Prnt (Odyssey + / Fast" +
    " Fare).";
            this.toolTip2.SetToolTip(this.checkBox6, "Print expire date on Ride & Value cards, see VErr_Prnt (Odyssey + / Fast Fare).");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags15Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 433);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags15Form";
            this.Text = "OptionFlag15";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}