﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class GeneralSettingsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formLoaded = false;
        public ToolTip toolTip1 = new ToolTip();
        public int nUseLockCodeFile = 0;

        public GeneralSettingsForm()
        {
            InitializeComponent();
        }

        public GeneralSettingsForm(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();
            LoadData();
            formLoaded = true;
            //SetupToolTip(this.agencyCodetxt);
        }

        public void SetupToolTip(object sender)
        {
            if (sender is TextBox)
            {
                string temp;

                // Set up the delays for the ToolTip.
                toolTip1.AutoPopDelay = 20000;
                toolTip1.InitialDelay = 500;
                toolTip1.ReshowDelay = 500;
                // Force the ToolTip text to be displayed whether or not the form is active.
                toolTip1.ShowAlways = true;
                temp = (string)((TextBox)sender).Tag;
                if (temp != null)
                {
                    temp = temp.Replace("<r>", "\n");
                    toolTip1.SetToolTip((TextBox)sender, temp);
                }
            }
        }
        public void ValidateFloatTextBox(TextBox textbox)
        {
            float floatValue;
            if (textbox.Text.Length > 0)
            {
                if ((textbox.Text.Length == 1) &&
                    ((textbox.Text == "+") || (textbox.Text == "-")))
                    return;

                if (!float.TryParse(textbox.Text, out floatValue))
                {
                    System.Media.SystemSounds.Hand.Play();
                    textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                    textbox.SelectionStart = textbox.Text.Length;
                }
            }
        }

        public void ValidateNumericTextBox(TextBox textbox)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textbox.Text, "[^0-9]"))
            {
                System.Media.SystemSounds.Hand.Play();
                textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                textbox.SelectionStart = textbox.Text.Length;
            }
        }
        private void float_TextChanged(object sender, EventArgs e)
        {
            ValidateFloatTextBox((TextBox)sender);
            UpdateData(sender, e);
        }
        private void numeric_TextChanged(object sender, EventArgs e)
        {
            ValidateNumericTextBox((TextBox)sender);
            UpdateData(sender, e);
        }

        private void agencyCodetxt_MouseHover(object sender, EventArgs e)
        {
            SetupToolTip(sender);
        }

        private void loadToolTip_MouseHover(object sender, EventArgs e)
        {
            SetupToolTip(sender);
        }

        public void SaveData()
        {
            splitForm.iniConfig.inifileData.AgencyCode = AgencyCode.Text;
            splitForm.iniConfig.inifileData.OldAgencyCode=OldAgencyCode.Text;
            splitForm.iniConfig.inifileData.Passback=Passback.Text;
            splitForm.iniConfig.inifileData.MemNearFull=MemNearFull.Text;
            splitForm.iniConfig.inifileData.Dump=Dump.Text ;
            splitForm.iniConfig.inifileData.BillFull=BillFull.Text;
            splitForm.iniConfig.inifileData.MaxEvents=MaxEvents.Text;
            splitForm.iniConfig.inifileData.CoinFull=CoinFull.Text ;
            splitForm.iniConfig.inifileData.LEDTime=LEDTime.Text ;
            splitForm.iniConfig.inifileData.BillTimeOut=BillTimeOut.Text;
            splitForm.iniConfig.inifileData.EvTime=EventPeriod.Text;
            splitForm.iniConfig.inifileData.ProbeWatchDog=ProbeWatchDog.Text;
            splitForm.iniConfig.inifileData.DST=DST.Text ;
            splitForm.iniConfig.inifileData.STD=STD.Text;
            splitForm.iniConfig.inifileData.ChangeCardThreshold = ChangeCardThreshold.Text;
            splitForm.iniConfig.inifileData.MagSecurityCodes=MagSecurityCodes.Text;
            splitForm.iniConfig.inifileData.SCardSecurityCodes=SCardSecurityCodes.Text;
            splitForm.iniConfig.inifileData.StopPointID=StopPointID.Text;
            splitForm.iniConfig.inifileData.MCP=MCP.Text;
            splitForm.iniConfig.inifileData.CalcSummaryCount = calcSummaryCount.Text;
            splitForm.iniConfig.inifileData.GMTOffSet = gmtOffsetText.Text;
            splitForm.iniConfig.inifileData.FtoTime = ftoTime.Text;
            splitForm.iniConfig.inifileData.OperatorID = operatorIDText.Text;
            splitForm.iniConfig.inifileData.LockFile = lockFileText.Text;
            splitForm.iniConfig.inifileData.UseLockCodeFile = (nUseLockCodeFile >0 ? "1" : "0");
            if (AcctMinRechargeUpDown.Value > 0 && AcctMinRechargeUpDown.Value  < 5)
                AcctMinRechargeUpDown.Value = 5;
            if (PassportTTPUpDown.Value < 1 || PassportTTPUpDown.Value > 241)
                PassportTTPUpDown.Value = 0;

            splitForm.iniConfig.inifileData.AcctMinRecharge = AcctMinRechargeUpDown.Value.ToString();
            splitForm.iniConfig.inifileData.PassportTTP = PassportTTPUpDown.Value.ToString();
            switch (MixFleetMode.Text)
            {
                case "0-Motorola":
                    splitForm.iniConfig.inifileData.MixFleetMode = "0";
                    break;
                case "1-Intel":
                    splitForm.iniConfig.inifileData.MixFleetMode = "1";
                    break;
                default:
                    splitForm.iniConfig.inifileData.MixFleetMode = "2";
                    break;
            }
            UpdateKontronGroup();
        }

        public void LoadData()
        {
            int mixFleetValue = 0;
            int acctMinRecharge=0, passportTTP=0;
            AgencyCode.Text = splitForm.iniConfig.inifileData.AgencyCode;
            OldAgencyCode.Text = splitForm.iniConfig.inifileData.OldAgencyCode;
            Passback.Text = splitForm.iniConfig.inifileData.Passback;
            MemNearFull.Text = splitForm.iniConfig.inifileData.MemNearFull;
            Dump.Text = splitForm.iniConfig.inifileData.Dump;
            BillFull.Text = splitForm.iniConfig.inifileData.BillFull;
            MaxEvents.Text = splitForm.iniConfig.inifileData.MaxEvents;
            CoinFull.Text = splitForm.iniConfig.inifileData.CoinFull;
            LEDTime.Text = splitForm.iniConfig.inifileData.LEDTime;
            BillTimeOut.Text = splitForm.iniConfig.inifileData.BillTimeOut;
            EventPeriod.Text = splitForm.iniConfig.inifileData.EvTime;
            ProbeWatchDog.Text = splitForm.iniConfig.inifileData.ProbeWatchDog;
            DST.Text = splitForm.iniConfig.inifileData.DST;
            STD.Text = splitForm.iniConfig.inifileData.STD;
            ChangeCardThreshold.Text = splitForm.iniConfig.inifileData.ChangeCardThreshold;
            MagSecurityCodes.Text = splitForm.iniConfig.inifileData.MagSecurityCodes;
            SCardSecurityCodes.Text = splitForm.iniConfig.inifileData.SCardSecurityCodes;
            StopPointID.Text = splitForm.iniConfig.inifileData.StopPointID;
            MCP.Text = splitForm.iniConfig.inifileData.MCP;
            calcSummaryCount.Text = splitForm.iniConfig.inifileData.CalcSummaryCount;
            gmtOffsetText.Text = splitForm.iniConfig.inifileData.GMTOffSet;
            ftoTime.Text = splitForm.iniConfig.inifileData.FtoTime;
            operatorIDText.Text = splitForm.iniConfig.inifileData.OperatorID;
            lockFileText.Text = splitForm.iniConfig.inifileData.LockFile;
            Int32.TryParse(splitForm.iniConfig.inifileData.UseLockCodeFile, out nUseLockCodeFile);
            Int32.TryParse(splitForm.iniConfig.inifileData.MixFleetMode, out mixFleetValue);
            Int32.TryParse(splitForm.iniConfig.inifileData.AcctMinRecharge, out acctMinRecharge);
            Int32.TryParse(splitForm.iniConfig.inifileData.PassportTTP, out passportTTP);
            if (acctMinRecharge < 5)
                acctMinRecharge = 0;
            if (passportTTP < 1 || passportTTP > 241)
                passportTTP = 0;
            AcctMinRechargeUpDown.Value = acctMinRecharge;
            PassportTTPUpDown.Value = passportTTP;
            switch (mixFleetValue)
            {
                case 0:
                    MixFleetMode.Text =  "0-Motorola";
                    break;
                case 1:
                    MixFleetMode.Text = "1-Intel";
                    break;
                default:
                    MixFleetMode.Text = "2-Both";
                    break;
            }
            MixFleetMode.Text = splitForm.iniConfig.inifileData.MixFleetMode;
            UpdateKontronGroup();
            if (nUseLockCodeFile>0)
                useLockCodecbx.Checked=true;
            else
                useLockCodecbx.Checked=false;
        }

        public void UpdateKontronGroup()
        {
            if (nUseLockCodeFile > 0)
            {
                lockFileText.Enabled = true;
                operatorIDText.Enabled = true;
                lockFileBtn.Enabled = true;
            }
            else
            {
                lockFileText.Enabled = false;
                operatorIDText.Enabled = false;
                lockFileBtn.Enabled = false;
            }
        }

        public void UpdateData(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.SetChanged();
                SaveData();
            }
        }

        private void lockFileBtn_Click(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                lockFileText.Text = splitForm.SelectFile(this.splitForm.globalSettings.gfiPath, "All files (*.*)|*.*");
                splitForm.SetChanged();
                SaveData();
            }
        }

        private void useLockCodecbx_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (((CheckBox)sender).Checked)
                    nUseLockCodeFile = 1;
                else
                    nUseLockCodeFile = 0;
                splitForm.SetChanged();
                SaveData();
            }
        }
    }
}
