﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class DstForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public uint DayLight = 0;
        bool formLoaded = false;
        private CheckBox[] cbxArray = new CheckBox[8];

        public DstForm()
        {
            InitializeComponent();
        }
        public DstForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;
            cbxArray[6] = this.checkBox7;
            cbxArray[7] = this.checkBox8;
            DayLight = Util.ParseValue(splitForm.iniConfig.inifileData.daylight);

            for (int i = 0; i < 8; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((DayLight & 1 << i) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            setDaylightHexValue();
            formLoaded = true;
        }

        private void setDaylightHexValue()
        {
            dstText.Text = "Daylight (Daylight Savings Time) - [0x" + DayLight.ToString("X2") + "]";
        }

        private void updateFlag(int index)
        {
            if (cbxArray[index].Checked)
                DayLight |= ((uint)1 << index);
            else
                DayLight &= ~((uint)1 << index);
        }
        private void muxFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag(((CheckBox)sender).TabIndex);
                setDaylightHexValue();
                splitForm.iniConfig.inifileData.daylight = "0x" + DayLight.ToString("X2");
                splitForm.SetChanged();
            }
        }
    }
}
