﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Collections.Generic;


namespace ConfigurationWinForm
{
    public class MdiPrint
    {
        private Font printFont;

        // Print text only
        private StringReader reader;
        static string text;
        static string filename;
        static RichTextBoxEx myRichTextBoxEx;
        private PRINTTYPE doctype;

        // Print RichTextBox
        // variable to trace text to print for pagination
        private int m_nFirstCharOnPage;


        // Print grid
        StringFormat strFormat; //Used to format the grid rows.
        ArrayList arrColumnLefts = new ArrayList();//Used to save left coordinates of columns
        ArrayList arrColumnWidths = new ArrayList();//Used to save column widths
        int iCellHeight = 0; //Used to get/set the datagridview cell height
        int iTotalWidth = 0; //
        int iRow = 0;//Used as counter
        bool bFirstPage = false; //Used to check whether we are printing first page
        bool bNewPage = false;// Used to check whether we are printing a new page
        int iHeaderHeight = 0; //Used for the header height
        DataGridView dataGridView1;
        public MdiPrint(string fn, RichTextBoxEx richtextbox)
        {
            doctype = PRINTTYPE.RTFData;
            filename = fn;
            myRichTextBoxEx = richtextbox;
        }

        public MdiPrint(string fn, string docText)
        {
            doctype = PRINTTYPE.TextData;
            filename = fn;
            text = docText;
        }

        public MdiPrint(string fn, DataGridView dataGridView)
        {
            doctype = PRINTTYPE.ExcelData;
            filename = fn;
            dataGridView1 = dataGridView;
        }

        public void PrintFile()
        {
            switch (doctype)
            {
                case PRINTTYPE.TextData:
                    PrintText();
                    break;
                case PRINTTYPE.RTFData:
                    PrintRichTextContents();
                    break;
                case PRINTTYPE.ExcelData:
                    break;
            }
        }

        public void PrintPreview()
        {
            switch (doctype)
            {
                case PRINTTYPE.TextData:
                    PrintPreviewText();
                    break;
                case PRINTTYPE.RTFData:
                    PrintPreviewRTF();
                    break;
                case PRINTTYPE.ExcelData:
                    PrintPreviewGrid();
                    break;
            }
        }




        //////////////////////////// Print Text Only ///////////////////////////////////
        // The PrintPage event is raised for each page to be printed. 
        ///////////////////////////////////////////////////////////////////////////////
        // Print the file. 
        private void PrintText()
        {
            try
            {
                printFont = new Font("Courier New", 8);
                PrintDialog printDlg = new PrintDialog();
                PrintDocument pd = new PrintDocument();
                pd.DocumentName = filename;
                printDlg.Document = pd;
                printDlg.AllowSelection = true;
                printDlg.AllowSomePages = true;
                pd.BeginPrint += new PrintEventHandler(pd_BeginPrint);
                pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                if (printDlg.ShowDialog() == DialogResult.OK)
                {
                    //reader = new StringReader(text);
                    pd.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PrintPreviewText()
        {
            bool bOK = false;
            printFont = new Font("Courier New", 8);
            PrintDialog printDlg = new PrintDialog();
            PrintDocument pd = new PrintDocument();
            pd.DocumentName = filename;
            printDlg.Document = pd;
            printDlg.AllowPrintToFile = false;
            printDlg.AllowSomePages = false;
            printDlg.AllowSelection = false;
            printDlg.AllowCurrentPage = false;
            printDlg.ShowHelp = false;
            printDlg.ShowNetwork = true;

            pd.BeginPrint += new PrintEventHandler(pd_BeginPrint);
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            bOK = (printDlg.ShowDialog() == DialogResult.OK);
            if (bOK)
            {
                //reader = new StringReader(text);
                PrintPreviewDialog YPreview = new PrintPreviewDialog();
                YPreview.WindowState = FormWindowState.Maximized;
                YPreview.Document = pd;
                YPreview.ShowDialog();
            }
        }
        private void pd_BeginPrint(object sender,
            System.Drawing.Printing.PrintEventArgs e)
        {
            // Start at the beginning of the text
            reader = new StringReader(text);
        }
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            int ch;
            SizeF stringSize = new SizeF();            
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            float linewidth = ev.MarginBounds.Right - ev.MarginBounds.Left;
            
            String line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            //reader = new StringReader(text);
            while (count < linesPerPage)
            {
                line = null;
                // Read characters from buffer
                while( (ch = reader.Read()) >= 0)
                {
                    if (ch != 0x0a)  // Test for end of lidata or end of line
                    {
                        char c = Convert.ToChar(ch);
                        string s = Convert.ToString(c);
                        if (line != null)
                            line += s;
                        else
                            line = s;
                        stringSize = ev.Graphics.MeasureString(line, printFont);
                        if (stringSize.Width >= linewidth)
                            break;
                    }
                    else
                    {
                        if (line == null)
                            line = "";
                        break;
                    }
                }
                if (line != null)
                {

                    yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
                    ev.Graphics.DrawString(line, printFont, Brushes.Black,
                       leftMargin, yPos, new StringFormat());
                    count++;
                }
                else
                    break;

            }
            // If more lines exist, print another page. 
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }





        //////////////////////////// Print RTF Only ///////////////////////////////////
        // The PrintPage event is raised for each page to be printed. 
        ///////////////////////////////////////////////////////////////////////////////
        public void PrintRichTextContents()
        {
            PrintDocument printDoc = new PrintDocument();
            printDoc.BeginPrint += new PrintEventHandler(printDoc_BeginPrint);
            printDoc.PrintPage += new PrintPageEventHandler(printDoc_PrintPage);
            printDoc.EndPrint += new PrintEventHandler(printDoc_EndPrint);
            // Start printing process
            printDoc.Print();
        }
        private void PrintPreviewRTF()
        {
            bool bOK = false;
            printFont = new Font("Courier New", 8);
            PrintDialog printDlg = new PrintDialog();
            PrintDocument pd = new PrintDocument();
            pd.DocumentName = filename;
            printDlg.Document = pd;
            printDlg.AllowPrintToFile = false;
            printDlg.AllowSomePages = false;
            printDlg.AllowSelection = false;
            printDlg.AllowCurrentPage = false;
            printDlg.ShowHelp = false;
            printDlg.ShowNetwork = true;

            pd.PrintPage += new PrintPageEventHandler(printDoc_PrintPage);
            bOK = (printDlg.ShowDialog() == DialogResult.OK);
            if (bOK)
            {
                //reader = new StringReader(text);

                // make sure the paper is portrait
                //mPD.DefaultPageSettings.Landscape = False

                PrintPreviewDialog YPreview = new PrintPreviewDialog();
                YPreview.WindowState = FormWindowState.Maximized;
                YPreview.Document = pd;
                YPreview.ShowDialog();
            }
        }
        private void printDoc_BeginPrint(object sender,
            System.Drawing.Printing.PrintEventArgs e)
        {
            // Start at the beginning of the text
            m_nFirstCharOnPage = 0;
        }

        private void printDoc_PrintPage(object sender,
            System.Drawing.Printing.PrintPageEventArgs e)
        {
            // To print the boundaries of the current page margins
            // uncomment the next line:
            // e.Graphics.DrawRectangle(System.Drawing.Pens.Blue, e.MarginBounds);

            // make the RichTextBoxEx calculate and render as much text as will
            // fit on the page and remember the last character printed for the
            // beginning of the next page
            m_nFirstCharOnPage = myRichTextBoxEx.FormatRange(false,
                                                    e,
                                                    m_nFirstCharOnPage,
                                                    myRichTextBoxEx.TextLength);

            // check if there are more pages to print
            if (m_nFirstCharOnPage < myRichTextBoxEx.TextLength)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;
        }

        private void printDoc_EndPrint(object sender,
            System.Drawing.Printing.PrintEventArgs e)
        {
            // Clean up cached information
            myRichTextBoxEx.FormatRangeDone();
        }




        //////////////////////////// Print GRIDVIEW Only //////////////////////////////
        // The PrintPage event is raised for each page to be printed. 
        ///////////////////////////////////////////////////////////////////////////////
        public void PrintGridContents()
        {
            PrintDocument pd = new PrintDocument();
            pd.BeginPrint += new PrintEventHandler(gridDoc_BeginPrint);
            pd.PrintPage += new PrintPageEventHandler(gridDoc_PrintPage);
            //printDoc.EndPrint += new PrintEventHandler(printDoc_EndPrint);
            // Start printing process
            pd.Print();
        }
        private void PrintPreviewGrid()
        {
            bool bOK = false;
            printFont = new Font("Courier New", 8);
            PrintDialog printDlg = new PrintDialog();
            PrintDocument pd = new PrintDocument();
            pd.DocumentName = filename;
            printDlg.Document = pd;
            printDlg.AllowPrintToFile = false;
            printDlg.AllowSomePages = false;
            printDlg.AllowSelection = false;
            printDlg.AllowCurrentPage = false;
            printDlg.ShowHelp = false;
            printDlg.ShowNetwork = true;
            pd.BeginPrint += new PrintEventHandler(gridDoc_BeginPrint);
            pd.PrintPage += new PrintPageEventHandler(gridDoc_PrintPage);
            bOK = (printDlg.ShowDialog() == DialogResult.OK);
            if (bOK)
            {
                PrintPreviewDialog YPreview = new PrintPreviewDialog();
                YPreview.WindowState = FormWindowState.Maximized;
                YPreview.Document = pd;
                YPreview.ShowDialog();
            }
        }
         private void gridDoc_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                strFormat = new StringFormat();
                strFormat.Alignment = StringAlignment.Near;
                strFormat.LineAlignment = StringAlignment.Center;
                strFormat.Trimming = StringTrimming.EllipsisCharacter;

                arrColumnLefts.Clear();
                arrColumnWidths.Clear();
                iCellHeight = 0;
                iRow = 0;
                bFirstPage = true;
                bNewPage = true;

                // Calculating Total Widths
                iTotalWidth = 0;
                foreach (DataGridViewColumn dgvGridCol in dataGridView1.Columns)
                {
                    iTotalWidth += dgvGridCol.Width;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void gridDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                //Set the left margin
                int iLeftMargin = e.MarginBounds.Left;
                //Set the top margin
                int iTopMargin = e.MarginBounds.Top;
                //Whether more pages have to print or not
                bool bMorePagesToPrint = false;
                int iTmpWidth = 0;

                //For the first page to print set the cell width and header height
                if (bFirstPage)
                {
                    foreach (DataGridViewColumn GridCol in dataGridView1.Columns)
                    {
                        iTmpWidth = (int)(Math.Floor((double)((double)GridCol.Width /
                                       (double)iTotalWidth * (double)iTotalWidth *
                                       ((double)e.MarginBounds.Width / (double)iTotalWidth))));

                        iHeaderHeight = (int)(e.Graphics.MeasureString(GridCol.HeaderText,
                                    GridCol.InheritedStyle.Font, iTmpWidth).Height) + 11;

                        // Save width and height of headres
                        arrColumnLefts.Add(iLeftMargin);
                        arrColumnWidths.Add(iTmpWidth);
                        iLeftMargin += iTmpWidth;
                    }
                }
                //Loop till all the grid rows not get printed
                while (iRow <= dataGridView1.Rows.Count - 1)
                {
                    DataGridViewRow GridRow = dataGridView1.Rows[iRow];
                    //Set the cell height
                    iCellHeight = GridRow.Height + 5;
                    int iCount = 0;
                    //Check whether the current page settings allo more rows to print
                    if (iTopMargin + iCellHeight >= e.MarginBounds.Height + e.MarginBounds.Top)
                    {
                        bNewPage = true;
                        bFirstPage = false;
                        bMorePagesToPrint = true;
                        break;
                    }
                    else
                    {
                        if (bNewPage)
                        {
                            //Draw Header
                            e.Graphics.DrawString("Customer Summary", new Font(dataGridView1.Font, FontStyle.Bold),
                                    Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top -
                                    e.Graphics.MeasureString("Customer Summary", new Font(dataGridView1.Font,
                                    FontStyle.Bold), e.MarginBounds.Width).Height - 13);

                            String strDate = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString();
                            //Draw Date
                            e.Graphics.DrawString(strDate, new Font(dataGridView1.Font, FontStyle.Bold),
                                    Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width -
                                    e.Graphics.MeasureString(strDate, new Font(dataGridView1.Font,
                                    FontStyle.Bold), e.MarginBounds.Width).Width), e.MarginBounds.Top -
                                    e.Graphics.MeasureString("Customer Summary", new Font(new Font(dataGridView1.Font,
                                    FontStyle.Bold), FontStyle.Bold), e.MarginBounds.Width).Height - 13);

                            //Draw Columns                 
                            iTopMargin = e.MarginBounds.Top;
                            foreach (DataGridViewColumn GridCol in dataGridView1.Columns)
                            {
                                e.Graphics.FillRectangle(new SolidBrush(Color.LightGray),
                                    new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawRectangle(Pens.Black,
                                    new Rectangle((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight));

                                e.Graphics.DrawString(GridCol.HeaderText, GridCol.InheritedStyle.Font,
                                    new SolidBrush(GridCol.InheritedStyle.ForeColor),
                                    new RectangleF((int)arrColumnLefts[iCount], iTopMargin,
                                    (int)arrColumnWidths[iCount], iHeaderHeight), strFormat);
                                iCount++;
                            }
                            bNewPage = false;
                            iTopMargin += iHeaderHeight;
                        }
                        iCount = 0;
                        //Draw Columns Contents                
                        foreach (DataGridViewCell Cel in GridRow.Cells)
                        {
                            if (Cel.Value != null)
                            {
                                e.Graphics.DrawString(Cel.Value.ToString(), Cel.InheritedStyle.Font,
                                            new SolidBrush(Cel.InheritedStyle.ForeColor),
                                            new RectangleF((int)arrColumnLefts[iCount], (float)iTopMargin,
                                            (int)arrColumnWidths[iCount], (float)iCellHeight), strFormat);
                            }
                            //Drawing Cells Borders 
                            e.Graphics.DrawRectangle(Pens.Black, new Rectangle((int)arrColumnLefts[iCount],
                                    iTopMargin, (int)arrColumnWidths[iCount], iCellHeight));

                            iCount++;
                        }
                    }
                    iRow++;
                    iTopMargin += iCellHeight;
                }

                //If more lines exist, print another page.
                if (bMorePagesToPrint)
                    e.HasMorePages = true;
                else
                    e.HasMorePages = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

   }
}