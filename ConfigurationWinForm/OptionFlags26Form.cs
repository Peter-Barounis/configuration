﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags26Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public uint optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags26Form()
        {
            InitializeComponent();            
        }

        public OptionFlags26Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[25]);
            setCheckboxChecked();
            formLoaded = true;
            setHexValue();
        }

        private void setHexValue()
        {
            if (formLoaded)
            {
                optionflag &= 0xff00;
                optionflag |= (uint)numericUpDown1.Value;
                optionflag |= (uint)(((uint)numericUpDown2.Value) << 4);

                optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
                splitForm.iniConfig.inifileData.optionFlags[25] = "0x" + optionflag.ToString("X4");
            }
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (uint)(1 << cbx.TabIndex);
            else
                optionflag &= (uint)(~(1 << cbx.TabIndex));
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag((CheckBox)sender);
                setHexValue();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            this.checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            this.checkBox10.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            this.checkBox11.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            this.checkBox12.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            this.checkBox13.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox14.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox15.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox16.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);
            numericUpDown1.Value = optionflag & 0x000F;
            numericUpDown2.Value = (optionflag & 0x00F0) >> 4;
        }

        private void desfireComboChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                setHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
