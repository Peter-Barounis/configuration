﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags11Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        private int lmt1, lmt2, lmt3;
        
        public OptionFlags11Form()
        {
            InitializeComponent();            
        }

        public OptionFlags11Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[10].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[10]);
            setNumericUpDownDefault();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[10] = "0x" + optionflag.ToString("X4");
            if (formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag(((CheckBox)sender));
            setHexValue();            
        }

        private void optionflagsGridTitle_Enter(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setNumericUpDownDefault();
            setHexValue();            
        }
        private void setNumericUpDownDefault()
        {
            numericUpDown1.Value = (optionflag & 0x000F);
            numericUpDown2.Value = ((optionflag >> 4) & 0x01FF);
            numericUpDown3.Value = ((optionflag >> 13) & 0x0007)*20;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFFF0) | (lmt1 & 0x000F));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);            
        }

        private void numericUpDown2_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown2_ValueChanged(sender, e);            
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            lmt2 = ((int)numericUpDown2.Value);
            optionflag = ((optionflag & 0xE00F) | ((lmt2 << 4) & 0x1FF0));
            setHexValue();            
        }

        private void numericUpDown3_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown3_ValueChanged(sender, e);
            
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            lmt3 = ((int)numericUpDown3.Value/20);
            optionflag = ((optionflag & 0x1FFF) | ((lmt3 << 13) & 0xE000));
            setHexValue();            
        }
    }
}
