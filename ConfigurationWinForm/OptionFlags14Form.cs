﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags14Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags14Form()
        {
            InitializeComponent();            
        }

        public OptionFlags14Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[13]);
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            int tmp;
            int optionflag13;
            int optionflag14;

            // Get option flag from text box
            optionflag = (int)Util.ParseValue(securityCodeText.Text);

            // Set optionflag14
            optionflag14 = optionflag & 0x0ffff;
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag14.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[13] = "0x" + optionflag14.ToString("X4");

            // Now set high nibble of OptionFlag13
            optionflag13 = (optionflag & 0xf0000) >> 4; ;
            optionflag13 &= 0xf000;

            tmp=(int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[12]);
            tmp &= 0x0fff;
            tmp |= optionflag13;
            splitForm.iniConfig.inifileData.optionFlags[12] = "0x" + tmp.ToString("X4");

            if (formLoaded)
            {
                splitForm.SetChanged();
                splitForm.RefreshFlag13();
            }
        }

        public void ValidateNumericTextBox(TextBox textbox)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textbox.Text, "[^0-9]"))
            {
                System.Media.SystemSounds.Hand.Play();
                textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                textbox.SelectionStart = textbox.Text.Length;
            }
        }

        private void numeric_TextChanged(object sender, EventArgs e)
        {
            ValidateNumericTextBox((TextBox)sender);
            setHexValue();
        }
    }
}
