﻿namespace ConfigurationWinForm
{
    partial class PrbFlagsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrbFlagsForm));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.PRBFlagsGridTitle = new System.Windows.Forms.GroupBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.PRBFlagsGridTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(16, 19);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(259, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x00000001 - ** Currently Unused **";
            this.toolTip2.SetToolTip(this.checkBox1, "This is a tooltip");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(16, 45);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(259, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x00000002 - ** Currently Unused **";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(16, 114);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(259, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x00000008 - ** Currently Unused **";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(16, 71);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(581, 38);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x00000004 - Download Auto-load list.  This is the interface between the back end" +
    " AND\r\nDESFire smart cards. This MUST be set for customers who use DESFire Smart " +
    "Cards.";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(16, 140);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(259, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x00000010 - ** Currently Unused **";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(16, 192);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(259, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x00000040 - ** Currently Unused **";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(16, 218);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(543, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x00000080 – Download range lists. Associated with OptionFlags8 bit 10 (0x0400)";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(16, 244);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(259, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x00000100 - ** Currently Unused **";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(16, 270);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(259, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x00000200 - ** Currently Unused **";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(16, 296);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(259, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x00000400 - ** Currently Unused **";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(16, 322);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(301, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x00000800 – Enable 10,000 entry bad list.";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(16, 348);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(259, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x00001000 - ** Currently Unused **";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // PRBFlagsGridTitle
            // 
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox6);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox32);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox31);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox29);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox30);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox28);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox27);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox26);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox25);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox24);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox23);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox22);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox21);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox20);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox19);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox18);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox17);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox16);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox15);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox14);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox4);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox8);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox1);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox9);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox2);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox10);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox3);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox11);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox7);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox12);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox5);
            this.PRBFlagsGridTitle.Controls.Add(this.checkBox13);
            this.PRBFlagsGridTitle.Location = new System.Drawing.Point(8, 6);
            this.PRBFlagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PRBFlagsGridTitle.Name = "PRBFlagsGridTitle";
            this.PRBFlagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PRBFlagsGridTitle.Size = new System.Drawing.Size(1234, 519);
            this.PRBFlagsGridTitle.TabIndex = 0;
            this.PRBFlagsGridTitle.TabStop = false;
            this.PRBFlagsGridTitle.Text = "PRB Flags-[0x00000000]";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(16, 166);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(259, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x00000020 - ** Currently Unused **";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Enabled = false;
            this.checkBox32.Location = new System.Drawing.Point(610, 430);
            this.checkBox32.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(536, 55);
            this.checkBox32.TabIndex = 31;
            this.checkBox32.Text = resources.GetString("checkBox32.Text");
            this.checkBox32.UseVisualStyleBackColor = true;
            this.checkBox32.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Enabled = false;
            this.checkBox31.Location = new System.Drawing.Point(610, 371);
            this.checkBox31.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(663, 55);
            this.checkBox31.TabIndex = 30;
            this.checkBox31.Text = resources.GetString("checkBox31.Text");
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(610, 321);
            this.checkBox29.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(259, 21);
            this.checkBox29.TabIndex = 28;
            this.checkBox29.Text = "0x10000000 - ** Currently Unused **";
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(610, 346);
            this.checkBox30.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(233, 21);
            this.checkBox30.TabIndex = 29;
            this.checkBox30.Text = "0x20000000 – No Event Loading";
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(610, 296);
            this.checkBox28.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(259, 21);
            this.checkBox28.TabIndex = 27;
            this.checkBox28.Text = "0x08000000 - ** Currently Unused **";
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(610, 271);
            this.checkBox27.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(518, 21);
            this.checkBox27.TabIndex = 26;
            this.checkBox27.Text = "0x04000000 – Download Route List. Associated with OptionFlags4 bit 5 (0x20)";
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(610, 246);
            this.checkBox26.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(518, 21);
            this.checkBox26.TabIndex = 25;
            this.checkBox26.Text = "0x02000000 – Download Driver List. Associated with OptionFlags4 bit 0 (0x01)";
            this.checkBox26.UseVisualStyleBackColor = true;
            this.checkBox26.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(610, 187);
            this.checkBox25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(501, 55);
            this.checkBox25.TabIndex = 24;
            this.checkBox25.Text = "0x01000000 – Download Bad List. Used for Magnetics, Classic smart cards \r\nand old" +
    "er Ultralight formats.Associated with MuxFlags bit 11 for 10k list OR\r\nMuxFlags2" +
    " bit 5 for 100k list.";
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(610, 145);
            this.checkBox24.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(494, 38);
            this.checkBox24.TabIndex = 23;
            this.checkBox24.Text = "0x00800000 – Download track 2 definitions. Associated with OptionFlags4 \r\nbit 3 (" +
    "0x08). MUST be set for customers who use “Third Party” Cards.";
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(610, 120);
            this.checkBox23.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(521, 21);
            this.checkBox23.TabIndex = 22;
            this.checkBox23.Text = "0x00400000 – Download Trip List. Associated with OptionFlags8 bit 9 (0x0200)";
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(610, 95);
            this.checkBox22.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(522, 21);
            this.checkBox22.TabIndex = 21;
            this.checkBox22.Text = "0x00200000 – Download Run List. Associated with OptionFlags8 bit 8 (0x0100)";
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(610, 70);
            this.checkBox21.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(555, 21);
            this.checkBox21.TabIndex = 20;
            this.checkBox21.Text = "0x00100000 – Download Zone Table. For Golden Gate Transit (San Francisco only)";
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(610, 45);
            this.checkBox20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(259, 21);
            this.checkBox20.TabIndex = 19;
            this.checkBox20.Text = "0x00080000 - ** Currently Unused **";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(610, 20);
            this.checkBox19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(259, 21);
            this.checkBox19.TabIndex = 18;
            this.checkBox19.Text = "0x00040000 - ** Currently Unused **";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(16, 478);
            this.checkBox18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(259, 21);
            this.checkBox18.TabIndex = 17;
            this.checkBox18.Text = "0x00020000 - ** Currently Unused **";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(16, 452);
            this.checkBox17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(259, 21);
            this.checkBox17.TabIndex = 16;
            this.checkBox17.Text = "0x00010000 - ** Currently Unused **";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(16, 426);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(259, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x00008000 - ** Currently Unused **";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(16, 400);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(259, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x00004000 - ** Currently Unused **";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(16, 374);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(595, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x00002000 – Download key labels to farebox. Associated with LAMetroOption bit 0 " +
    "(0x01)";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.muxFlag_CheckedChanged);
            // 
            // PrbFlagsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 539);
            this.Controls.Add(this.PRBFlagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PrbFlagsForm";
            this.Text = "PRB Flags";
            this.PRBFlagsGridTitle.ResumeLayout(false);
            this.PRBFlagsGridTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox PRBFlagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;


    }
}