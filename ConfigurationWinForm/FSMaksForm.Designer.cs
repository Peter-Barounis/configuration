﻿namespace ConfigurationWinForm
{
    partial class FSMaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSMaskForm));
            this.groupBox1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.defaultFareset48 = new System.Windows.Forms.NumericUpDown();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupbox4 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.defaultFareset241 = new System.Windows.Forms.NumericUpDown();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.defaultFareset48)).BeginInit();
            this.groupbox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.defaultFareset241)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Location = new System.Drawing.Point(8, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 13);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.Text = "FSMask (48TTP Fare Structure)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Enabled (available) fare sets:";
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(440, 105);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(51, 17);
            this.checkBox12.TabIndex = 13;
            this.checkBox12.Text = "FS10";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(392, 105);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(45, 17);
            this.checkBox11.TabIndex = 12;
            this.checkBox11.Text = "FS9";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(344, 105);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(45, 17);
            this.checkBox10.TabIndex = 11;
            this.checkBox10.Text = "FS8";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(296, 105);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(45, 17);
            this.checkBox9.TabIndex = 10;
            this.checkBox9.Text = "FS7";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(248, 105);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(45, 17);
            this.checkBox8.TabIndex = 9;
            this.checkBox8.Text = "FS6";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(200, 105);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(45, 17);
            this.checkBox7.TabIndex = 8;
            this.checkBox7.Text = "FS5";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(152, 105);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(45, 17);
            this.checkBox6.TabIndex = 7;
            this.checkBox6.Text = "FS4";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(104, 105);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(45, 17);
            this.checkBox5.TabIndex = 6;
            this.checkBox5.Text = "FS3";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(56, 105);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(45, 17);
            this.checkBox4.TabIndex = 5;
            this.checkBox4.Text = "FS2";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(8, 105);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(45, 17);
            this.checkBox3.TabIndex = 4;
            this.checkBox3.Text = "FS1";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 54);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Default Fare Set (1-10)";
            // 
            // defaultFareset48
            // 
            this.defaultFareset48.Location = new System.Drawing.Point(8, 52);
            this.defaultFareset48.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.defaultFareset48.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.defaultFareset48.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.defaultFareset48.Name = "defaultFareset48";
            this.defaultFareset48.Size = new System.Drawing.Size(41, 20);
            this.defaultFareset48.TabIndex = 2;
            this.defaultFareset48.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.defaultFareset48.ValueChanged += new System.EventHandler(this.defaultFareset48_ValueChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 25);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(415, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x8000 - Enable Fare set mask. This must be set to ouse the FSMASK parameters.";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // groupbox4
            // 
            this.groupbox4.Controls.Add(this.groupBox2);
            this.groupbox4.Controls.Add(this.label4);
            this.groupbox4.Controls.Add(this.checkBox25);
            this.groupbox4.Controls.Add(this.checkBox26);
            this.groupbox4.Controls.Add(this.checkBox27);
            this.groupbox4.Controls.Add(this.checkBox28);
            this.groupbox4.Controls.Add(this.checkBox29);
            this.groupbox4.Controls.Add(this.checkBox30);
            this.groupbox4.Controls.Add(this.checkBox31);
            this.groupbox4.Controls.Add(this.checkBox32);
            this.groupbox4.Controls.Add(this.checkBox33);
            this.groupbox4.Controls.Add(this.checkBox34);
            this.groupbox4.Controls.Add(this.checkBox15);
            this.groupbox4.Controls.Add(this.label2);
            this.groupbox4.Controls.Add(this.checkBox16);
            this.groupbox4.Controls.Add(this.defaultFareset241);
            this.groupbox4.Controls.Add(this.checkBox17);
            this.groupbox4.Controls.Add(this.checkBox18);
            this.groupbox4.Controls.Add(this.checkBox14);
            this.groupbox4.Controls.Add(this.checkBox19);
            this.groupbox4.Controls.Add(this.checkBox21);
            this.groupbox4.Controls.Add(this.checkBox20);
            this.groupbox4.Controls.Add(this.checkBox24);
            this.groupbox4.Controls.Add(this.checkBox23);
            this.groupbox4.Controls.Add(this.checkBox22);
            this.groupbox4.Location = new System.Drawing.Point(12, 296);
            this.groupbox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupbox4.Name = "groupbox4";
            this.groupbox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupbox4.Size = new System.Drawing.Size(503, 157);
            this.groupbox4.TabIndex = 2;
            this.groupbox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Location = new System.Drawing.Point(9, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 13);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.Text = "FSMask2 (241 TTP Fare Structure)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Enabled (available) fare sets:";
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(443, 121);
            this.checkBox25.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(51, 17);
            this.checkBox25.TabIndex = 33;
            this.checkBox25.Text = "FS20";
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(395, 121);
            this.checkBox26.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(51, 17);
            this.checkBox26.TabIndex = 32;
            this.checkBox26.Text = "FS19";
            this.checkBox26.UseVisualStyleBackColor = true;
            this.checkBox26.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(347, 121);
            this.checkBox27.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(51, 17);
            this.checkBox27.TabIndex = 31;
            this.checkBox27.Text = "FS18";
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(299, 121);
            this.checkBox28.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(51, 17);
            this.checkBox28.TabIndex = 30;
            this.checkBox28.Text = "FS17";
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.ChangeUICues += new System.Windows.Forms.UICuesEventHandler(this.updateFlag);
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(251, 121);
            this.checkBox29.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(51, 17);
            this.checkBox29.TabIndex = 29;
            this.checkBox29.Text = "FS16";
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(203, 121);
            this.checkBox30.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(51, 17);
            this.checkBox30.TabIndex = 28;
            this.checkBox30.Text = "FS15";
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Location = new System.Drawing.Point(155, 121);
            this.checkBox31.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(51, 17);
            this.checkBox31.TabIndex = 27;
            this.checkBox31.Text = "FS14";
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Location = new System.Drawing.Point(107, 121);
            this.checkBox32.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(51, 17);
            this.checkBox32.TabIndex = 26;
            this.checkBox32.Text = "FS13";
            this.checkBox32.UseVisualStyleBackColor = true;
            this.checkBox32.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Location = new System.Drawing.Point(59, 121);
            this.checkBox33.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(51, 17);
            this.checkBox33.TabIndex = 25;
            this.checkBox33.Text = "FS12";
            this.checkBox33.UseVisualStyleBackColor = true;
            this.checkBox33.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Location = new System.Drawing.Point(11, 121);
            this.checkBox34.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(51, 17);
            this.checkBox34.TabIndex = 24;
            this.checkBox34.Text = "FS11";
            this.checkBox34.UseVisualStyleBackColor = true;
            this.checkBox34.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(443, 99);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(51, 17);
            this.checkBox15.TabIndex = 23;
            this.checkBox15.Text = "FS10";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Default Fare Set (1-20)";
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(395, 99);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(45, 17);
            this.checkBox16.TabIndex = 22;
            this.checkBox16.Text = "FS9";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // defaultFareset241
            // 
            this.defaultFareset241.Location = new System.Drawing.Point(11, 50);
            this.defaultFareset241.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.defaultFareset241.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.defaultFareset241.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.defaultFareset241.Name = "defaultFareset241";
            this.defaultFareset241.Size = new System.Drawing.Size(41, 20);
            this.defaultFareset241.TabIndex = 6;
            this.defaultFareset241.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.defaultFareset241.ValueChanged += new System.EventHandler(this.defaultFareset241_ValueChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(347, 99);
            this.checkBox17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(45, 17);
            this.checkBox17.TabIndex = 21;
            this.checkBox17.Text = "FS8";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(299, 99);
            this.checkBox18.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(45, 17);
            this.checkBox18.TabIndex = 20;
            this.checkBox18.Text = "FS7";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(11, 25);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(442, 17);
            this.checkBox14.TabIndex = 4;
            this.checkBox14.Text = "0x02000000- Enable Fare set mask. This must be set to ouse the FSMASK2 parameters" +
    ".";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox14_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(251, 99);
            this.checkBox19.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(45, 17);
            this.checkBox19.TabIndex = 19;
            this.checkBox19.Text = "FS6";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(155, 99);
            this.checkBox21.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(45, 17);
            this.checkBox21.TabIndex = 17;
            this.checkBox21.Text = "FS4";
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(203, 99);
            this.checkBox20.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(45, 17);
            this.checkBox20.TabIndex = 18;
            this.checkBox20.Text = "FS5";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(11, 99);
            this.checkBox24.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(45, 17);
            this.checkBox24.TabIndex = 14;
            this.checkBox24.Text = "FS1";
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(59, 99);
            this.checkBox23.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(45, 17);
            this.checkBox23.TabIndex = 15;
            this.checkBox23.Text = "FS2";
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(107, 99);
            this.checkBox22.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(45, 17);
            this.checkBox22.TabIndex = 16;
            this.checkBox22.Text = "FS3";
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.CheckedChanged += new System.EventHandler(this.updateFlag);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.checkBox5);
            this.groupBox3.Controls.Add(this.checkBox4);
            this.groupBox3.Controls.Add(this.checkBox12);
            this.groupBox3.Controls.Add(this.checkBox6);
            this.groupBox3.Controls.Add(this.checkBox3);
            this.groupBox3.Controls.Add(this.checkBox11);
            this.groupBox3.Controls.Add(this.checkBox7);
            this.groupBox3.Controls.Add(this.checkBox10);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.checkBox8);
            this.groupBox3.Controls.Add(this.checkBox9);
            this.groupBox3.Controls.Add(this.defaultFareset48);
            this.groupBox3.Location = new System.Drawing.Point(12, 139);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(500, 135);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Yellow;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(321, 119);
            this.label5.TabIndex = 18;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // FSMaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 475);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupbox4);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FSMaskForm";
            this.Text = "FSMask/FSMask2";
            ((System.ComponentModel.ISupportInitialize)(this.defaultFareset48)).EndInit();
            this.groupbox4.ResumeLayout(false);
            this.groupbox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.defaultFareset241)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown defaultFareset48;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupbox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.NumericUpDown defaultFareset241;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.Label groupBox1;
        private System.Windows.Forms.Label groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
    }
}