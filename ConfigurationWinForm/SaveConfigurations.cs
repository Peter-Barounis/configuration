﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class SaveConfigurations : Form
    {
        SplitForm splitForm;
        public bool saveIni = false;
        public bool saveInittab = false;
        public bool saveNetconfig = false;
        public DialogResult SaveOkCancel;
        public bool bSaveCleanIniFile = false;
        public bool editmode = false;
        public SaveConfigurations()
        {
            InitializeComponent();
        }

        public SaveConfigurations(SplitForm sf, bool bSave)
        {
            string path;
            splitForm = sf;
            editmode = bSave;
            InitializeComponent();
            locationName.Text = splitForm.globalSettings.currentLocation;

            // set file name in text box
            //path = splitForm.globalSettings.gfiPath + splitForm.globalSettings.cnfFolder + "\\";
            path = splitForm.globalSettings.gfiPath + splitForm.globalSettings.cnfFolder;
            iniFileNameText.Text = path+splitForm.globalSettings.iniFileName;
            inittabFileNameText.Text = path+splitForm.globalSettings.inittabFileName;
            netconfigFileNameText.Text = path+splitForm.globalSettings.netconfigFileName;

            // Enable/disable textbox
            iniFileNameText.Enabled = splitForm.globalSettings.dirty;
            inittabFileNameText.Enabled = splitForm.globalSettings.InittabChanged;
            netconfigFileNameText.Enabled = splitForm.globalSettings.NetConfigChanged;

            // Are we saving to a remote location?
            tcpIpPortText.Text = splitForm.globalSettings.httpPort.ToString();
            if (splitForm.globalSettings.location > 0)
            {
                tcpIpPortLabel.Show();
                tcpIpPortText.Show();
            }
            else
            {
                tcpIpPortLabel.Hide();
                tcpIpPortText.Hide();
            }

            if (bSave)
            {
                inifileBtn.Enabled = splitForm.globalSettings.dirty;
                inittabBtn.Enabled = splitForm.globalSettings.InittabChanged;
                netconfigBtn.Enabled = splitForm.globalSettings.NetConfigChanged;

                inifileBtn.Checked = splitForm.globalSettings.dirty;
                inittabBtn.Checked = splitForm.globalSettings.InittabChanged;
                netconfigBtn.Checked = splitForm.globalSettings.NetConfigChanged;
                allBtn.Checked = true;
                saveCleanConfiguration.Checked = false;
            }
            else
            {
                inifileBtn.Enabled = true;
                inittabBtn.Enabled = true;
                netconfigBtn.Enabled = true;

                inifileBtn.Checked = true;
                inittabBtn.Checked = true;
                netconfigBtn.Checked = true;

                if (splitForm.globalSettings.SinglefileOpened)
                {
                    inittabBtn.Checked = false;
                    netconfigBtn.Checked = false;
                    allBtn.Checked = false;
                }
                else
                    allBtn.Checked = true;
                saveCancel.Hide();
                Text = "Editor Configurations";
                saveYes.Text = "Open";
                saveCleanConfiguration.Checked = false;
                saveCleanConfiguration.Hide();
            }

            saveIni = inifileBtn.Checked;
            saveInittab = inittabBtn.Checked;
            saveNetconfig = netconfigBtn.Checked;
        }

        private void allBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (editmode)
            {
                //if (splitForm.globalSettings.dirty)
                //    inifileBtn.Checked = true;
                //if (splitForm.globalSettings.InittabChanged)
                //    inittabBtn.Checked = true;
                //if (splitForm.globalSettings.NetConfigChanged)
                //    netconfigBtn.Checked = true;
                if (splitForm.globalSettings.dirty)
                    inifileBtn.Checked = allBtn.Checked;
                if (splitForm.globalSettings.InittabChanged)
                    inittabBtn.Checked = allBtn.Checked;
                if (splitForm.globalSettings.NetConfigChanged)
                    netconfigBtn.Checked = allBtn.Checked;
            }
            else
            {
                inifileBtn.Checked = allBtn.Checked;
                inittabBtn.Checked = allBtn.Checked;
                netconfigBtn.Checked = allBtn.Checked;
            }
        }

        private void saveYes_Click(object sender, EventArgs e)
        {
            SaveOkCancel = DialogResult.Yes;
            bSaveCleanIniFile = this.saveCleanConfiguration.Checked;
            closeForm();
        }

        private void saveNo_Click(object sender, EventArgs e)
        {
            SaveOkCancel = DialogResult.No;
            closeForm();
        }

        private void saveCancel_Click(object sender, EventArgs e)
        {
            SaveOkCancel = DialogResult.Cancel;
            closeForm();
        }

        private void closeForm()
        {
            saveIni = inifileBtn.Checked;
            saveInittab = inittabBtn.Checked;
            saveNetconfig = netconfigBtn.Checked;
            Close();
        }

        private void updatePort(object sender, EventArgs e)
        {
            splitForm.globalSettings.httpPort = (int)Util.ParseValue(tcpIpPortText.Text);
        }
    }
}
