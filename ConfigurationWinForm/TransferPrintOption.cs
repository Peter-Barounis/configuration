﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class TransferPrintOptionForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public int TransferPrintOptionFlag;
        bool formLoaded = false;
        private CheckBox[] TransferPrintOptions = new CheckBox[16];

        public TransferPrintOptionForm()
        {
            InitializeComponent();
        }
        public TransferPrintOptionForm(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();
            TransferPrintOptions[0] = checkBox1;
            TransferPrintOptions[1] = checkBox2;
            TransferPrintOptions[2] = checkBox3;
            TransferPrintOptions[3] = checkBox4;
            TransferPrintOptions[4] = checkBox5;
            TransferPrintOptions[5] = checkBox6;
            TransferPrintOptions[6] = checkBox7;
            TransferPrintOptions[7] = checkBox8;
            TransferPrintOptions[8] = checkBox9;
            LoadData();
            setHexValue();
            formLoaded = true;
        }

        public void LoadData()
        {
            TransferPrintOptionFlag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.TransferPrintOption);
            for (int i = 0; i < 8; i++)
            {
                if ((TransferPrintOptionFlag & (uint)1 << i) != 0)
                    TransferPrintOptions[i].Checked = true;
                else
                    TransferPrintOptions[i].Checked = false;
            }

            if ((TransferPrintOptionFlag & 0x00C0) == 0xC0)
            {
                checkBox7.Checked = true;
                checkBox8.Checked = true;
                checkBox9.Checked = true;
            }
            else
            {
                checkBox9.Checked = false;
            }
        }

        private void setHexValue()
        {
            transferPrintOptionsText.Text = "Option Flags-[0x" + TransferPrintOptionFlag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.TransferPrintOption = "0x" + TransferPrintOptionFlag.ToString("X4");
            if (formLoaded)
                splitForm.SetChanged();
        }
        private void updateFlags()
        {
            if (formLoaded)
            {
                if (checkBox9.Checked == true)
                {
                    checkBox7.Checked = true;
                    checkBox8.Checked = true;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (TransferPrintOptions[i].Checked)
                        TransferPrintOptionFlag |= (1 << TransferPrintOptions[i].TabIndex);
                    else
                        TransferPrintOptionFlag &= ~(1 << TransferPrintOptions[i].TabIndex);
                }


                setHexValue();
            }
        }

        private void transferOptions_CheckedChanged(object sender, EventArgs e)
        {
            updateFlags();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TransferPrintOptionFlag = 0xD0;
            LoadData();
            setHexValue();
        }
    }
}
