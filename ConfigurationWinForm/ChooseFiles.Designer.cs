﻿namespace ConfigurationWinForm
{
    partial class ChooseFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.Source_button = new System.Windows.Forms.Button();
            this.Target_button = new System.Windows.Forms.Button();
            this.Compare_button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Cancel_button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source File:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Target File:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(116, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(440, 22);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(116, 87);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(440, 22);
            this.textBox2.TabIndex = 3;
            // 
            // Source_button
            // 
            this.Source_button.Location = new System.Drawing.Point(568, 37);
            this.Source_button.Name = "Source_button";
            this.Source_button.Size = new System.Drawing.Size(93, 28);
            this.Source_button.TabIndex = 4;
            this.Source_button.Text = "Browse...";
            this.Source_button.UseVisualStyleBackColor = true;
            this.Source_button.Click += new System.EventHandler(this.Source_button_Click);
            // 
            // Target_button
            // 
            this.Target_button.Location = new System.Drawing.Point(568, 80);
            this.Target_button.Name = "Target_button";
            this.Target_button.Size = new System.Drawing.Size(93, 28);
            this.Target_button.TabIndex = 5;
            this.Target_button.Text = "Browse...";
            this.Target_button.UseVisualStyleBackColor = true;
            this.Target_button.Click += new System.EventHandler(this.Target_button_Click);
            // 
            // Compare_button
            // 
            this.Compare_button.Location = new System.Drawing.Point(534, 134);
            this.Compare_button.Name = "Compare_button";
            this.Compare_button.Size = new System.Drawing.Size(127, 28);
            this.Compare_button.TabIndex = 6;
            this.Compare_button.Text = "Show Differences";
            this.Compare_button.UseVisualStyleBackColor = true;
            this.Compare_button.Click += new System.EventHandler(this.Compare_button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Target_button);
            this.groupBox1.Controls.Add(this.Cancel_button);
            this.groupBox1.Controls.Add(this.Source_button);
            this.groupBox1.Controls.Add(this.Compare_button);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(679, 180);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Files";
            // 
            // Cancel_button
            // 
            this.Cancel_button.Location = new System.Drawing.Point(415, 134);
            this.Cancel_button.Name = "Cancel_button";
            this.Cancel_button.Size = new System.Drawing.Size(93, 28);
            this.Cancel_button.TabIndex = 0;
            this.Cancel_button.Text = "Cancel";
            this.Cancel_button.UseVisualStyleBackColor = true;
            this.Cancel_button.Click += new System.EventHandler(this.Cancel_button_Click);
            // 
            // ChooseFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 241);
            this.Controls.Add(this.groupBox1);
            this.Name = "ChooseFiles";
            this.Text = "Select Files to Show Differences";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button Source_button;
        private System.Windows.Forms.Button Target_button;
        private System.Windows.Forms.Button Compare_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Cancel_button;
    }
}