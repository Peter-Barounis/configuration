﻿namespace ConfigurationWinForm
{
    partial class UDPListenerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.udpFoldertxt = new System.Windows.Forms.TextBox();
            this.udpUrltxt = new System.Windows.Forms.TextBox();
            this.udpTokentxt = new System.Windows.Forms.TextBox();
            this.cbx_SaveUDPXml = new System.Windows.Forms.CheckBox();
            this.cbx_SaveUDPRsp = new System.Windows.Forms.CheckBox();
            this.cbx_UDPTestOnly = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.wifiSettingsBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.udpFolderBrowseBtn = new System.Windows.Forms.Button();
            this.debugLevel = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.debugLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "UDPFolder:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "URL:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Token:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "SaveUdpXml:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(158, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "SaveUdpRsp:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(283, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "UDPTestOnly:";
            // 
            // udpFoldertxt
            // 
            this.udpFoldertxt.Location = new System.Drawing.Point(99, 26);
            this.udpFoldertxt.Name = "udpFoldertxt";
            this.udpFoldertxt.Size = new System.Drawing.Size(458, 20);
            this.udpFoldertxt.TabIndex = 0;
            this.toolTip2.SetToolTip(this.udpFoldertxt, "Folder where the messages received from the farebox \r\nwill be saved before sendng" +
        " to the server.");
            this.udpFoldertxt.TextChanged += new System.EventHandler(this.saveForm);
            // 
            // udpUrltxt
            // 
            this.udpUrltxt.Location = new System.Drawing.Point(99, 70);
            this.udpUrltxt.Name = "udpUrltxt";
            this.udpUrltxt.Size = new System.Drawing.Size(458, 20);
            this.udpUrltxt.TabIndex = 1;
            this.toolTip2.SetToolTip(this.udpUrltxt, "Thi sis the URL to be contacted for sending Alarm files.");
            this.udpUrltxt.TextChanged += new System.EventHandler(this.saveForm);
            // 
            // udpTokentxt
            // 
            this.udpTokentxt.Location = new System.Drawing.Point(99, 114);
            this.udpTokentxt.Name = "udpTokentxt";
            this.udpTokentxt.Size = new System.Drawing.Size(458, 20);
            this.udpTokentxt.TabIndex = 2;
            this.toolTip2.SetToolTip(this.udpTokentxt, "Security token required by the Server and used to validate user data (Alarms) to " +
        "be sent.");
            this.udpTokentxt.TextChanged += new System.EventHandler(this.saveForm);
            // 
            // cbx_SaveUDPXml
            // 
            this.cbx_SaveUDPXml.AutoSize = true;
            this.cbx_SaveUDPXml.Location = new System.Drawing.Point(90, 158);
            this.cbx_SaveUDPXml.Name = "cbx_SaveUDPXml";
            this.cbx_SaveUDPXml.Size = new System.Drawing.Size(15, 14);
            this.cbx_SaveUDPXml.TabIndex = 3;
            this.toolTip2.SetToolTip(this.cbx_SaveUDPXml, "Normally the alarms are sent to the server directly.\r\nCheck this box if you also " +
        "wish to save the data to\r\nan XML file.");
            this.cbx_SaveUDPXml.UseVisualStyleBackColor = true;
            this.cbx_SaveUDPXml.CheckedChanged += new System.EventHandler(this.saveForm);
            // 
            // cbx_SaveUDPRsp
            // 
            this.cbx_SaveUDPRsp.AutoSize = true;
            this.cbx_SaveUDPRsp.Location = new System.Drawing.Point(233, 158);
            this.cbx_SaveUDPRsp.Name = "cbx_SaveUDPRsp";
            this.cbx_SaveUDPRsp.Size = new System.Drawing.Size(15, 14);
            this.cbx_SaveUDPRsp.TabIndex = 4;
            this.toolTip2.SetToolTip(this.cbx_SaveUDPRsp, "Check this box if you wish to save the \r\nServer  responses.\r\n");
            this.cbx_SaveUDPRsp.UseVisualStyleBackColor = true;
            this.cbx_SaveUDPRsp.CheckedChanged += new System.EventHandler(this.saveForm);
            // 
            // cbx_UDPTestOnly
            // 
            this.cbx_UDPTestOnly.AutoSize = true;
            this.cbx_UDPTestOnly.Location = new System.Drawing.Point(358, 158);
            this.cbx_UDPTestOnly.Name = "cbx_UDPTestOnly";
            this.cbx_UDPTestOnly.Size = new System.Drawing.Size(15, 14);
            this.cbx_UDPTestOnly.TabIndex = 5;
            this.toolTip2.SetToolTip(this.cbx_UDPTestOnly, "Check this box if you do NOT want to sent the alarms\r\nto the server but are simpl" +
        "y testing the farebox alarms locally.\r\n");
            this.cbx_UDPTestOnly.UseVisualStyleBackColor = true;
            this.cbx_UDPTestOnly.CheckedChanged += new System.EventHandler(this.saveForm);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wifiSettingsBtn);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.udpFolderBrowseBtn);
            this.groupBox1.Controls.Add(this.debugLevel);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.udpUrltxt);
            this.groupBox1.Controls.Add(this.cbx_UDPTestOnly);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbx_SaveUDPRsp);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbx_SaveUDPXml);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.udpTokentxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.udpFoldertxt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(12, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(620, 276);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // wifiSettingsBtn
            // 
            this.wifiSettingsBtn.Location = new System.Drawing.Point(352, 210);
            this.wifiSettingsBtn.Name = "wifiSettingsBtn";
            this.wifiSettingsBtn.Size = new System.Drawing.Size(160, 23);
            this.wifiSettingsBtn.TabIndex = 15;
            this.wifiSettingsBtn.Text = "Go to WIFI Settings Page >>";
            this.wifiSettingsBtn.UseVisualStyleBackColor = true;
            this.wifiSettingsBtn.Click += new System.EventHandler(this.wifiSettingsBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(310, 39);
            this.label9.TabIndex = 14;
            this.label9.Text = "NOTE: The network settings (including ports, IP addresses, etc.)\r\nmust also be se" +
    "t. Please go to the wifi swttings page to make\r\nthese changes.\r\n";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "UDP Listener Section";
            // 
            // udpFolderBrowseBtn
            // 
            this.udpFolderBrowseBtn.Location = new System.Drawing.Point(561, 26);
            this.udpFolderBrowseBtn.Margin = new System.Windows.Forms.Padding(2);
            this.udpFolderBrowseBtn.Name = "udpFolderBrowseBtn";
            this.udpFolderBrowseBtn.Size = new System.Drawing.Size(58, 19);
            this.udpFolderBrowseBtn.TabIndex = 13;
            this.udpFolderBrowseBtn.Text = "Browse...";
            this.toolTip2.SetToolTip(this.udpFolderBrowseBtn, "Browse the computer for folder selection.");
            this.udpFolderBrowseBtn.UseVisualStyleBackColor = true;
            this.udpFolderBrowseBtn.Click += new System.EventHandler(this.udpFolderBrowseBtn_Click);
            // 
            // debugLevel
            // 
            this.debugLevel.Location = new System.Drawing.Point(525, 155);
            this.debugLevel.Margin = new System.Windows.Forms.Padding(2);
            this.debugLevel.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.debugLevel.Name = "debugLevel";
            this.debugLevel.Size = new System.Drawing.Size(32, 20);
            this.debugLevel.TabIndex = 6;
            this.toolTip2.SetToolTip(this.debugLevel, "Indicate the Debug Level for logging. 0=None, \r\nany number >4 indicates full debu" +
        "gging.");
            this.debugLevel.ValueChanged += new System.EventHandler(this.saveForm);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(444, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "DebugLevel:";
            // 
            // UDPListenerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 303);
            this.Controls.Add(this.groupBox1);
            this.Name = "UDPListenerForm";
            this.Text = "UDP Listener";
            this.toolTip2.SetToolTip(this, "Folder location of where to save Alarm files that are to be sent to the Server.");
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.debugLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox udpFoldertxt;
        private System.Windows.Forms.TextBox udpUrltxt;
        private System.Windows.Forms.TextBox udpTokentxt;
        private System.Windows.Forms.CheckBox cbx_SaveUDPXml;
        private System.Windows.Forms.CheckBox cbx_SaveUDPRsp;
        private System.Windows.Forms.CheckBox cbx_UDPTestOnly;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown debugLevel;
        private System.Windows.Forms.Button udpFolderBrowseBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button wifiSettingsBtn;
        private System.Windows.Forms.Label label9;
    }
}