﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags24Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags24Form()
        {
            InitializeComponent();            
        }

        public OptionFlags24Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[23].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[23]);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflag &= 0xFF0F;
            optionflag |= (int)((UInt16)numericUpDownMIFARE.Value & 0x000F) << 4;
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[23] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0010;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            int i = (optionflag & 0x00F0)>>4;
            if (i <= 0 || i > 15)
                i = 1;
            this.numericUpDownMIFARE.Value = Convert.ToDecimal(i);
            this.checkBox1.Checked = Convert.ToBoolean((optionflag >> 0) & 0x0001);
            this.checkBox2.Checked = Convert.ToBoolean((optionflag >> 1) & 0x0001);
            this.checkBox3.Checked = Convert.ToBoolean((optionflag >> 2) & 0x0001);
            this.checkBox4.Checked = Convert.ToBoolean((optionflag >> 3) & 0x0001);
            this.checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x000F);
            this.checkBox13.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox14.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox15.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox16.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);            
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            setHexValue();
        }

        private void numericUpDownMIFARE_ValueChanged(object sender, EventArgs e)
        {
            setHexValue();
        }
    }
}
