﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class RfdevmgrForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public ToolTip toolTip1 = new ToolTip();
        public bool formLoaded = false;
        public RfdevmgrForm()
        {
            InitializeComponent();
        }

        public RfdevmgrForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm();
        }
        public void loadForm()
        {
            if (splitForm.iniConfig.inifileData.RFDEVMGR_InserviceProbe == "1")
                this.radioButton1.Checked = true;
            else
                this.radioButton2.Checked = true;

            if (splitForm.iniConfig.inifileData.RFDEVMGR_SaveWifiStatus == "1")
                this.radioButton4.Checked = true;
            else
                this.radioButton3.Checked = true;
            if (splitForm.iniConfig.inifileData.RFDEVMGR_DebugLevel.Trim().Length == 0)
                splitForm.iniConfig.inifileData.RFDEVMGR_DebugLevel = "0";
            this.numericUpDown1.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.RFDEVMGR_DebugLevel);
            if (splitForm.iniConfig.inifileData.RFDEVMGR_RetryCount.Trim().Length == 0)
                splitForm.iniConfig.inifileData.RFDEVMGR_RetryCount = "3";            
            this.numericUpDown2.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.RFDEVMGR_RetryCount);
            this.textBox1.Text = splitForm.iniConfig.inifileData.RFDEVMGR_AUTOLOADFILE;
            this.textBox2.Text = splitForm.iniConfig.inifileData.RFDEVMGR_FBXCBFWFILE;
            this.textBox3.Text = splitForm.iniConfig.inifileData.RFDEVMGR_FBXLIDFWFILE;
            this.textBox4.Text = splitForm.iniConfig.inifileData.RFDEVMGR_PrbEvntThreshold;
            this.textBox5.Text = splitForm.iniConfig.inifileData.RFDEVMGR_PrbTrnsThreshold;
            this.textBox6.Text = splitForm.iniConfig.inifileData.RFDEVMGR_Command;
            this.textBox7.Text = splitForm.iniConfig.inifileData.RFDEVMGR_LanguageFontFile;
            this.textBox8.Text = splitForm.iniConfig.inifileData.RFDEVMGR_FBXFWUPGBUSES;
            this.textBox9.Text = splitForm.iniConfig.inifileData.RFDEVMGR_GCWIFIFLAGS;
            formLoaded = true;


        }

        public void saveForm(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (radioButton1.Checked)
                    splitForm.iniConfig.inifileData.RFDEVMGR_InserviceProbe = "1";
                else
                    splitForm.iniConfig.inifileData.RFDEVMGR_InserviceProbe = "0";
                
                if (radioButton4.Checked)
                    splitForm.iniConfig.inifileData.RFDEVMGR_SaveWifiStatus = "1";
                else
                    splitForm.iniConfig.inifileData.RFDEVMGR_SaveWifiStatus = "0";
                
                
                splitForm.iniConfig.inifileData.RFDEVMGR_DebugLevel = this.numericUpDown1.Value.ToString();
                splitForm.iniConfig.inifileData.RFDEVMGR_RetryCount = this.numericUpDown2.Value.ToString();
                splitForm.iniConfig.inifileData.RFDEVMGR_AUTOLOADFILE = this.textBox1.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_FBXCBFWFILE = this.textBox2.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_FBXLIDFWFILE = this.textBox3.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_PrbEvntThreshold = this.textBox4.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_PrbTrnsThreshold = this.textBox5.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_Command = this.textBox6.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_LanguageFontFile = this.textBox7.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_FBXFWUPGBUSES = this.textBox8.Text;
                splitForm.iniConfig.inifileData.RFDEVMGR_GCWIFIFLAGS = this.textBox9.Text;
                splitForm.SetChanged();
            }
        }

        private void wifiSettingsBtn_Click(object sender, EventArgs e)
        {
            splitForm.OpenFormByName("wifi general settings");
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        //private void Cancel_button_Click(object sender, EventArgs e)
        //{
        //    Control parent = Parent;
        //    while (!(parent is SplitForm))
        //        parent = parent.Parent;
        //    ((SplitForm)parent).CloseTabbedForm(this.Text);
        //}

    }
}
