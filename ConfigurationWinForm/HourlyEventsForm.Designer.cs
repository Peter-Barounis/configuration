﻿namespace ConfigurationWinForm
{
    partial class HourlyEventsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.quarterHourEvent = new System.Windows.Forms.RadioButton();
            this.halfHourEvent = new System.Windows.Forms.RadioButton();
            this.hourlyEvent = new System.Windows.Forms.RadioButton();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.hourlyEventText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hourlyEventText);
            this.groupBox1.Controls.Add(this.checkBox6);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.checkBox5);
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.checkBox3);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(672, 288);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(15, 250);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(514, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x80 – Probe ID enabled. Set if login of vault puller required to probe farebox.";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.quarterHourEvent);
            this.groupBox2.Controls.Add(this.halfHourEvent);
            this.groupBox2.Controls.Add(this.hourlyEvent);
            this.groupBox2.Location = new System.Drawing.Point(15, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(520, 62);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            // 
            // quarterHourEvent
            // 
            this.quarterHourEvent.AutoSize = true;
            this.quarterHourEvent.Location = new System.Drawing.Point(348, 21);
            this.quarterHourEvent.Name = "quarterHourEvent";
            this.quarterHourEvent.Size = new System.Drawing.Size(121, 21);
            this.quarterHourEvent.TabIndex = 2;
            this.quarterHourEvent.TabStop = true;
            this.quarterHourEvent.Text = "1/4 hour event";
            this.quarterHourEvent.UseVisualStyleBackColor = true;
            this.quarterHourEvent.CheckedChanged += new System.EventHandler(this.radioBtn_CheckedChanged);
            // 
            // halfHourEvent
            // 
            this.halfHourEvent.AutoSize = true;
            this.halfHourEvent.Location = new System.Drawing.Point(171, 21);
            this.halfHourEvent.Name = "halfHourEvent";
            this.halfHourEvent.Size = new System.Drawing.Size(121, 21);
            this.halfHourEvent.TabIndex = 1;
            this.halfHourEvent.TabStop = true;
            this.halfHourEvent.Text = "1/2 hour event";
            this.halfHourEvent.UseVisualStyleBackColor = true;
            this.halfHourEvent.CheckedChanged += new System.EventHandler(this.radioBtn_CheckedChanged);
            // 
            // hourlyEvent
            // 
            this.hourlyEvent.AutoSize = true;
            this.hourlyEvent.Location = new System.Drawing.Point(18, 21);
            this.hourlyEvent.Name = "hourlyEvent";
            this.hourlyEvent.Size = new System.Drawing.Size(109, 21);
            this.hourlyEvent.TabIndex = 0;
            this.hourlyEvent.TabStop = true;
            this.hourlyEvent.Text = "Hourly event";
            this.hourlyEvent.UseVisualStyleBackColor = true;
            this.hourlyEvent.CheckedChanged += new System.EventHandler(this.radioBtn_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(15, 223);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(642, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x40 – Smart card reader check. Show / Hide SC OFFLINE message. (Clear = HIDE, Se" +
    "t = SHOW)";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(15, 195);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(316, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x20 – Long response delay on J1708 for DRI";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(15, 167);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(639, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x10 – Ignore Expiration dates on Ride/Value cards and adjust expiration on rolli" +
    "ng period passes";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(15, 139);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(231, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x08 – J1708 Idle Line not used.";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(15, 111);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(559, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x04 – Show date and time of last list update information on center of “Login Scr" +
    "een”";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // hourlyEventText
            // 
            this.hourlyEventText.AutoSize = true;
            this.hourlyEventText.Location = new System.Drawing.Point(12, -3);
            this.hourlyEventText.Name = "hourlyEventText";
            this.hourlyEventText.Size = new System.Drawing.Size(140, 17);
            this.hourlyEventText.TabIndex = 17;
            this.hourlyEventText.Text = "Hourly Event - [0xFF]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Event Type";
            // 
            // HourlyEventsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 318);
            this.Controls.Add(this.groupBox1);
            this.Name = "HourlyEventsForm";
            this.Text = "Hourly Events";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton quarterHourEvent;
        private System.Windows.Forms.RadioButton halfHourEvent;
        private System.Windows.Forms.RadioButton hourlyEvent;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label hourlyEventText;
        private System.Windows.Forms.Label label1;
    }
}