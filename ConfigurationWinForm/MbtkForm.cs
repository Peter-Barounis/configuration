﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class MbtkForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public ToolTip toolTip1 = new ToolTip();
        private bool formLoaded = false;

        public MbtkForm()
        {
            InitializeComponent();
        }

        public MbtkForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm(sf);
        }

        public void loadForm(SplitForm sf)
        {
            UInt16 tls;
            UInt16 Mbtk_MbtkFlags;
            this.splitForm = sf;
            if (splitForm.iniConfig.inifileData.Mbtk_DebugLevel.Trim().Length == 0)
                splitForm.iniConfig.inifileData.Mbtk_DebugLevel = "0";
            this.numericUpDown1.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.Mbtk_DebugLevel);
            this.textBox1.Text = splitForm.iniConfig.inifileData.Mbtk_DomainName;
            this.textBox2.Text = splitForm.iniConfig.inifileData.Mbtk_PollInterval;
            this.textBox3.Text = splitForm.iniConfig.inifileData.Mbtk_TransitID;
            this.textBox4.Text = splitForm.iniConfig.inifileData.Mbtk_Authorization;
            Mbtk_MbtkFlags = (UInt16)Util.ParseValue(splitForm.iniConfig.inifileData.Mbtk_MbtkFlags);
            checkBox1.Checked = Convert.ToBoolean(Mbtk_MbtkFlags & 0x01);
            tls = (UInt16)Util.ParseValue(splitForm.iniConfig.inifileData.Mbtk_TLS);
            enableTLSSecurity.Checked = (tls > 0 ? true : false);
            enableSSLSecurity.Checked = !enableTLSSecurity.Checked;
            formLoaded = true;
        }
        public void saveForm(Object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.Mbtk_DebugLevel =  this.numericUpDown1.Value.ToString();
                splitForm.iniConfig.inifileData.Mbtk_DomainName    =this.textBox1.Text ;
                splitForm.iniConfig.inifileData.Mbtk_PollInterval  =this.textBox2.Text ;
                splitForm.iniConfig.inifileData.Mbtk_TransitID     =this.textBox3.Text ;
                splitForm.iniConfig.inifileData.Mbtk_Authorization =this.textBox4.Text ;
                if (this.checkBox1.Checked)
                    splitForm.iniConfig.inifileData.Mbtk_MbtkFlags= "1";
                else
                    splitForm.iniConfig.inifileData.Mbtk_MbtkFlags = "0";
                if (enableSSLSecurity.Checked)
                    splitForm.iniConfig.inifileData.Mbtk_TLS = "0";
                else
                    splitForm.iniConfig.inifileData.Mbtk_TLS = "1";
                splitForm.SetChanged();
            }
        }
    }
}
