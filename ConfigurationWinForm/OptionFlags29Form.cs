﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags29Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags29Form()
        {
            InitializeComponent();            
        }

        public OptionFlags29Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[28].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[28]);
            this.numericUpDown1.Value = ((optionflag >> 0) & 0x00FF);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[28] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex+7);
            else
                optionflag &= ~(1 << cbx.TabIndex+7);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag(((CheckBox)sender));
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();
        }
        private void setCheckboxChecked()
        {
            this.numericUpDown1.Value = ((optionflag >> 0) & 0x00FF);            
            this.checkBox1.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            this.checkBox2.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            this.checkBox3.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            this.checkBox4.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            this.checkBox5.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox6.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox7.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox8.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFF00) | ((lmt1 << 0) & 0x00FF));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }
    }
}
