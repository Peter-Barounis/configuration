﻿using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Diagnostics;
using Microsoft.Win32;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.Globalization;
using GfiUtilities;
using System.Data.Odbc;
using System.ServiceProcess;
using System.Threading;
using System.Data.SQLite;
using MySql.Data.MySqlClient;

namespace ConfigurationWinForm
{
    public enum PRINTTYPE { TextData, RTFData, ExcelData, Unknown };
    public partial class SplitForm : Form
    {
        public ManagedAes aesEncrypt = new ManagedAes(")J@NcRfUjXn2r5u8x/A?D(G+KbPdSgVk");
        //[DllImport("gfipb32.dll")]
        //private unsafe static extern int Get_binary_date(byte[] filename, byte[] d_date);

        [DllImport("gfipb32.dll")]
        private unsafe static extern int BuildConnectionString(byte[] connstr);

        //[DllImport("gfipb32.dll")]
        //private unsafe static extern int GetDatabaseConnection(int dflt, int type,  int garageId, byte[] connstr);

        //[DllImport("gfipb32.dll")]
        //[return: MarshalAs(UnmanagedType.BStr)]
        //unsafe public static extern StringBuilder GetASAConnection(int gdsid);

        //[DllImport("gfipb32.dll")]
        //private static extern int DbConnect();

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
        public int NETWORK_MANAGER = 2;
        public int GARAGE = 1;
        public int LOCATION_BOTH = 3;


        public int ASA_CONNECTION = 1;
        public int MSS_CONNECTION = 2;
        public int ORA_CONNECTION = 3;
        public int UNKNOWN_COMPUTER = -1;

        public class WindowList
        {
            public string name;
            public Form frm;
            public TabPage tabPage;
            public bool activated { get; set; }
        };


        // Member variables 
        public bool bnoLogin = false;
        private bool bRunOdbcOnly = false;
        public TreeNode activeNode = null;
        public bool FocusFlag = false;
        public GlobalSettings globalSettings = new GlobalSettings();
        public GlobalPrintSettings globalPrintSettings = new GlobalPrintSettings();
        public List<WindowList> frmList = new List<WindowList>();
        public IniConfiguration iniConfig;
        public NetConfiguration netConfig;
        FileVersionInfo fvi;
        GFILocations locations;
        public FindParamOrValue findForm=null;
        public OdbcConnection cnnODBC=null;
        public SQLiteConnection cnnSqlite = null;
        public MySqlConnection cnnMySql = null;
        public bool brunning;

        // Required in order to modify controls on the main thread
        delegate void dUpdateServiceControls();
        ContextMenuStrip mnu = new ContextMenuStrip();
        ToolStripMenuItem mnuEnable = new ToolStripMenuItem("Enable");
        ToolStripMenuItem mnuDisable = new ToolStripMenuItem("Disable");

        public string GetLocationFolder(int loc)
        {
            if (loc == 0)
                return "\\cnf\\";
            else
                return ("\\a" + Convert.ToString(Convert.ToChar(0x60 + loc)) + ".cnf\\");
        }

        // Remove trailing slash or backslash if necessary
        public string FixPath(string path)
        {
            string strVal;
            // Remove trailing slash or backslash if necessary
            if (path.Length > 0)
            {
                strVal = path.Substring(path.Length - 1, 1);
                if (strVal == "\\" || strVal == "/")
                    return (path.Substring(0, path.Length - 1));
            }
            return(path);
        }

        // Constructor
        public SplitForm()
        {
            //aesEncrypt.SetKey("abdhdjduebgtfsjlkjasldfdvsolmn22");
            //string temp1 = aesEncrypt.Encrypt("This is a test");
            //string temp2 = aesEncrypt.Decrypt(temp1);
            bRunOdbcOnly = false;
            bnoLogin = false;
            String[] arguments = Environment.GetCommandLineArgs();

            for (int i = 1; i < arguments.Count(); i++)
            {
                if (arguments[i].ToLower().Contains("nologin"))
                    bnoLogin = true;
                if (arguments[i].ToLower().Contains("odbc"))
                    bRunOdbcOnly = true;
            }
#if (DEBUG)
            bnoLogin = true;
#endif

            InitializeComponent();
            GetSQLServerDriverName();
            locations = new GFILocations(this); 
            ConfigureLocations();
            configTreeView.Enabled = false;
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            Text = "Configuration Assistant - Version: " + fvi.FileVersion;
            globalSettings.httpPort = Util.GetGfiKeyValueDword("Global", "HttpPort", 80);
            globalSettings.gfiPath = Util.GetGfiKeyValueString("Global", "Base Directory", "C:\\GFI");
            globalSettings.gfiBackupPath = Util.GetGfiKeyValueString("Global", "ConfigBackup", "C:\\GFI\\cnf\\ConfigBackup");
            globalSettings.gfiPath = FixPath(globalSettings.gfiPath);
            globalSettings.gfiBackupPath = FixPath(globalSettings.gfiBackupPath);
            globalSettings.dotNetVersion = (string)GetHKLMRegistryString("Software\\Microsoft\\Net Framework Setup\\NDP\\V4\\Client", "Version");
            ////////////////////////////////////////////////
            // Add the right click menu to the treeview for import/export feature
            // Create the cut/copy/paset menu

            //Assign event handlers
            mnuEnable.Click += new EventHandler(treeviewEnable_Click);
            mnuDisable.Click += new EventHandler(treeviewDisable_Click);

            //Add to main context menu
            mnu.Items.AddRange(new ToolStripItem[] { mnuEnable, mnuDisable });

            // END of Context Menu
            ////////////////////////////////////////////////

            // Create backup folder in case it does not exist
            Directory.CreateDirectory(globalSettings.gfiBackupPath);
            globalSettings.systemType = GetSystemType();
            UpdateServiceControls();

            // Try to open the connection, if anything goes wrong, make sure we set connectSuccess = false
            Thread t = new Thread(CheckInitServiceRunning);
            t.Start();
            if (bRunOdbcOnly)
            {
                this.Hide();
                this.WindowState = FormWindowState.Minimized;
                Form frm = new odbctestForm(this);
                frm.Show();
            }
        }

        private void treeviewEnable_Click(object sender, EventArgs e)
        {
            if (activeNode != null) 
            {
                CloseTabbedForm("Location");
                activeNode.ForeColor = SystemColors.MenuText;
                switch (activeNode.Text.ToLower())
                {
                    case "mux":
                        break;
                    case "vlt":
                        break;
                    case "mux1":
                        iniConfig.inifileData.muxEnabled[0] = true;
                        break;
                    case "mux2":
                        iniConfig.inifileData.muxEnabled[1] = true;
                        break;
                    case "mux3":
                        iniConfig.inifileData.muxEnabled[2] = true;
                        break;
                    case "vlt1":
                        iniConfig.inifileData.vltEnabled[0] = true;
                        break;
                    case "vlt2":
                        iniConfig.inifileData.vltEnabled[1] = true;
                        break;
                    case "vlt3":
                        iniConfig.inifileData.vltEnabled[2] = true;
                        break;
                    case "vlt4":
                        iniConfig.inifileData.muxEnabled[0] = true;
                        break;
                    case "vlt5":
                        iniConfig.inifileData.vltEnabled[4] = true;
                        break;
                    case "database":
                        iniConfig.inifileData.enableDatabase = true;
                        break;
                    case "probeserver (ff-ffe)":
                        iniConfig.inifileData.enableProbeServer = true;
                        break;
                    case "rfsrvr (odyssey)":
                        iniConfig.inifileData.enableRfsrvr = true;
                        break;
                    case "rfdevmgr(odyssey+)":
                        iniConfig.inifileData.enableRfdevmgr = true;
                        break;
                    case "clnt":
                        iniConfig.inifileData.enableClnt = true;
                        break;
                    case "vndldr":
                        iniConfig.inifileData.enableVndldr = true;
                        break;
                    case "mobile ticketing":
                        iniConfig.inifileData.enableMobileTicketing = true;
                        break;
                    case "udp listener":
                        iniConfig.inifileData.enableUDPListener = true;
                        break;
                    case "user defined actions":
                        iniConfig.inifileData.enableUserDefinedActions = true;
                        break;
                    case "wifi":
                        activeNode.ForeColor = SystemColors.MenuText;;
                        iniConfig.inifileData.enableProbeServer = true;
                        iniConfig.inifileData.enableRfsrvr = true;
                        iniConfig.inifileData.enableRfdevmgr = true;
                        iniConfig.inifileData.enableWifiProbing = true;
                        break;
                    default:
                        break;
                }
            }
            // Open the location form
            SelectForm(configTreeView.Nodes[0]);
            UpdateTree();
        }

        private void treeviewDisable_Click(object sender, EventArgs e)
        {
            // 1. set text to Gray
            // 2. Set any appropriate check boxes (mux aand vlt)
            // 3. close the window (if open)
            // 4. refresh nodes
            if (activeNode != null)
            {
                //activeNode.ForeColor = SystemColors.GrayText;
                string text = activeNode.FullPath;
                CloseTabbedForm(text);
                CloseTabbedForm("Location");

                switch (activeNode.Text.ToLower())
                {
                    case "mux":
                        iniConfig.inifileData.muxEnabled[0] = false;
                        iniConfig.inifileData.muxEnabled[1] = false;
                        iniConfig.inifileData.muxEnabled[2] = false;
                        activeNode.ForeColor = SystemColors.GrayText;
                        break;
                    case "vaults":
                        iniConfig.inifileData.vltEnabled[0] = false;
                        iniConfig.inifileData.vltEnabled[1] = false;
                        iniConfig.inifileData.vltEnabled[2] = false;
                        iniConfig.inifileData.vltEnabled[3] = false;
                        iniConfig.inifileData.vltEnabled[4] = false;
                        activeNode.ForeColor = SystemColors.GrayText;
                        break;
                    case "mux1":
                        iniConfig.inifileData.muxEnabled[0] = false;
                        break;
                    case "mux2":
                        iniConfig.inifileData.muxEnabled[1] = false;
                        break;
                    case "mux3":
                        iniConfig.inifileData.muxEnabled[2] = false;
                        break;
                    case "vlt1":
                        iniConfig.inifileData.vltEnabled[0] = false;
                        break;
                    case "vlt2":
                        iniConfig.inifileData.vltEnabled[1] = false;
                        break;
                    case "vlt3":
                        iniConfig.inifileData.vltEnabled[2] = false;
                        break;
                    case "vlt4":
                        iniConfig.inifileData.vltEnabled[3] = false;
                        break;
                    case "vlt5":
                        iniConfig.inifileData.vltEnabled[4] = false;
                        break;
                    case "database":
                        iniConfig.inifileData.enableDatabase = false;
                        break;
                    case "probeserver (ff-ffe)":
                        iniConfig.inifileData.enableProbeServer = false;
                        break;
                    case "rfsrvr (odyssey)":
                        iniConfig.inifileData.enableRfsrvr = false;
                        break;
                    case "rfdevmgr(odyssey+)":
                        iniConfig.inifileData.enableRfdevmgr = false;
                        break;
                    case "clnt":
                        iniConfig.inifileData.enableClnt = false;
                        break;
                    case "vndldr":
                        iniConfig.inifileData.enableVndldr = false;
                        break;
                    case "mobile ticketing":
                        iniConfig.inifileData.enableMobileTicketing = false;
                        break;
                    case "udp listener":
                        iniConfig.inifileData.enableUDPListener = false;
                        break;
                    case "user defined actions":
                        iniConfig.inifileData.enableUserDefinedActions = false;
                        break;
                    case "wifi":
                        iniConfig.inifileData.enableProbeServer = false;
                        iniConfig.inifileData.enableRfsrvr = false;
                        iniConfig.inifileData.enableRfdevmgr = false;
                        activeNode.ForeColor = SystemColors.GrayText;
                        iniConfig.inifileData.enableWifiProbing = false;
                        CloseTabbedForm("Wifi General Settings");
                        CloseTabbedForm("netconfig (ff-ffe)");

                        break;
                    default:
                        break;
                }
                CloseTabbedForm(activeNode.Text);
            }
            // Open the location form
            SelectForm(configTreeView.Nodes[0]);
            UpdateTree();
        }

        public void ExpandToLevel(TreeNodeCollection nodes, int level)
        {
            if (level > 0)
            {
                foreach (TreeNode node in nodes)
                {
                    node.Expand();
                    ExpandToLevel(node.Nodes, level - 1);
                }
            }
        }

        public object GetHKLMRegistryString(string key, string val)
        {
            object Base = null;
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey(key);

            try
            {
                Base = regKey.GetValue(val);
                regKey.Close();
            }

            catch 
            {
                Base = null;
                //MessageBox.Show(ex.ToString() + "\n\n - Please fix and retry!", "GFI Genfare registry missing");
            }
            return Base;
        }

        public string  GetGFIRegistryString(string key, string subkey, string dflt)
        {

            object Base=null;
            try
            {
                Base = GetHKLMRegistryString(key, subkey);
            }
            catch
            {
            }
            // If no registry entry, then assume Garage Computer
            if (Base == null)
                return(dflt);
            else
                return Base.ToString();
        }

        int GetSystemType()
        {
            int systemtype=8;
            object Base = null;
            try
            {
                Base = GetHKLMRegistryString("Software\\GFI Genfare\\Global", "System Type");

                // If no registry entry, then assume No Database (Use Data SYstem 8)
                if (Base != null)
                {
                    systemtype = (int)Util.ParseValue(Base.ToString());
                    // If garage, check if Data System 8
                    if (systemtype == 1)
                    {
                        Base = GetHKLMRegistryString("Software\\GFI Genfare", "Version");
                        if (Base.ToString().CompareTo("7") > 0)
                        {
                            systemtype = 8;
                        }
                    }
                }
            }
            catch
            {
            }
            return systemtype;
        }

        public void LoadDefaultConnection()
        {
            globalSettings.sysconEng = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON ENG", "");
            globalSettings.sysconHost = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON HOST", "Localhost");
            globalSettings.sysconDsn = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON DSN", "Localhost");
            if (globalSettings.systemType == 2)
            {
                globalSettings.sysconUser = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON USER", "gfi");
                globalSettings.sysconPassword = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON PWD", "gfi");
            }
            else
            {
                globalSettings.sysconUser = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON USER", "dba");
                globalSettings.sysconPassword = GetGFIRegistryString(globalSettings.HKLM_GFI_GLOBAL, "SYSCON PWD", "gfi314159gfi");
                globalSettings.sysconDsn = "gfi";
            }

        }

        void ConfigureLocations()
        {
            ToolStripMenuItem locationToolStripItem;
            globalSettings.systemType = GetSystemType();
            LoadDefaultConnection();
            globalSettings.locationType = NETWORK_MANAGER;

            if (globalSettings.systemType == NETWORK_MANAGER) 
            {
                locations.LoadNMLocations();
                locationToolStripItem = new ToolStripMenuItem();
                locationToolStripItem.Name = "NetworkManagerToolStripMenuItem";
                locationToolStripItem.Size = new System.Drawing.Size(191, 22);
                locationToolStripItem.Text = "Network Manager";
                locationsToolStripMenuItem.DropDownItems.Add(locationToolStripItem);
                locationToolStripItem.Click += new System.EventHandler(this.openToolStripNMLocation_Click);
                locationToolStripItem.Tag = 0;
                int locId = 1;
                foreach (var loc in locations.CurrentLocations)
                {
                    locationToolStripItem = new ToolStripMenuItem();
                    locationToolStripItem.Name = "Location " + loc.locNumber.ToString();
                    locationToolStripItem.Size = new System.Drawing.Size(191, 22);
                    locationToolStripItem.Text = loc.locName + ":" + loc.locTransitAuth;
                    locationsToolStripMenuItem.DropDownItems.Add(locationToolStripItem);
                    locationToolStripItem.Click += new System.EventHandler(this.openToolStripGCLocation_Click);
                    locationToolStripItem.Tag = locId;  // Use aa.cnf, ab.cnf, etc. 
                    locId++;
                }
            }
            else
            {
                locationToolStripItem = new ToolStripMenuItem();
                locationToolStripItem.Size = new System.Drawing.Size(191, 22);
                locationsToolStripMenuItem.DropDownItems.Add(locationToolStripItem);
                locationToolStripItem.Click += new System.EventHandler(this.openToolStripGCLocation_Click);
                locationToolStripItem.Tag = 0;                          
                globalSettings.sysconEng = (string)GetHKLMRegistryString("Software\\GFI Genfare\\Global", "SYSCON ENG");

                globalSettings.locationID = GetCurrentLocation();       
                locationToolStripItem.Text=GetCurrentLocationName();    
                globalSettings.location_name = locationToolStripItem.Text;
            }
           
            globalSettings.loaded = false;

            globalSettings.dirty = false;
            globalSettings.NetConfigChanged = false;
            globalSettings.InittabChanged = false;

            globalSettings.saveIni = false;
            globalSettings.saveInittab = false;
            globalSettings.saveNetconfig = false;

            if ((globalSettings.dotNetVersion == null) || (globalSettings.dotNetVersion.Length == 0))
                globalSettings.dotNetVersion = (string)GetHKLMRegistryString("Software\\Microsoft\\Net Framework Setup\\NDP\\V4\\Full", "Version");
            if ((globalSettings.dotNetVersion == null) || (globalSettings.dotNetVersion.CompareTo("4.6") < 0))
                MessageBox.Show(@".NET 4.6 is required for some features of this application to work properly. Some features will be disabled. Please Install .NET 4.6 to use all the features of this application.", "Configuration Assistant Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            globalSettings.database = true;
            UpdateMenuStatus();
        }

        public void SetChanged()
        {
            globalSettings.dirty = true;
            UpdateMenuStatus();
        }

        public void ClearChanged()
        {
            globalSettings.dirty = false;
            globalSettings.NetConfigChanged = false;
            globalSettings.InittabChanged = false;

            globalSettings.saveIni = false;
            globalSettings.saveInittab = false;
            globalSettings.saveNetconfig = false;

            UpdateMenuStatus();
        }

        public void SetInittabChanged()
        {
            globalSettings.InittabChanged = true;
            UpdateMenuStatus();
        }

        public void SetNetConfigChanged()
        {
            globalSettings.NetConfigChanged=true;
            UpdateMenuStatus();
        }


        public void UpdateMenuStatus()
        {
            // Open in Notepad only if loaded
            openInNotepad.Enabled = globalSettings.loaded;
            toolOpenInNotepad.Enabled = globalSettings.loaded;

            // Find Only if loaded
            findInConfiguration.Enabled = (globalSettings.loaded && (findForm == null));
            findInConfigurationStop.Enabled = (globalSettings.loaded && (findForm != null));

            toolFind.Enabled = findInConfiguration.Enabled;
            toolFindClose.Enabled = findInConfigurationStop.Enabled;

            // Save only if dirty
            saveToolStripMenuItem.Enabled = (globalSettings.dirty||globalSettings.InittabChanged||globalSettings.NetConfigChanged);
            toolSaveBtn.Enabled = saveToolStripMenuItem.Enabled;             

            // Clear only if loaded
            clearToolStripMenuItem.Enabled = globalSettings.loaded;
            toolClearBtn.Enabled = globalSettings.loaded;

            if (globalSettings.currentLocation == "Custom")
                ClearFileSections();
            if ((globalSettings.systemType == GARAGE) || (globalSettings.systemType == LOCATION_BOTH))
                toolUpdateLocation.Enabled = true;
            else
                toolUpdateLocation.Enabled = false;
            if (globalSettings.database == false)
            {
                querytoolStripMenu.Enabled = false;
                toolUpdateLocation.Enabled = false;
                connectionMenu.Enabled = false;
            }
        }

        private void openToolStripNMLocation_Click(object sender, EventArgs e)
        {
            globalSettings.locationType = NETWORK_MANAGER;
            openLocation_Click(sender, e);
        }

        private void openToolStripGCLocation_Click(object sender, EventArgs e)
        {
            globalSettings.locationType = GARAGE;
            openLocation_Click(sender, e);
        }
        private void openLocation_Click(object sender, EventArgs e)
        {
            openLocation((Int32)((ToolStripMenuItem)sender).Tag, sender.ToString());
        }

        private void openLocation(int locID, string locName )
        {
            globalSettings.SinglefileOpened = false;

            if (OktoClearData())
            {
                globalSettings.enableGfiini = true;
                globalSettings.enableNetconfig = true;
                globalSettings.enableInittab = true;
                // re-enable the file check boxes
                inittabSelected.Enabled = true;
                gfiIniSelected.Enabled = true;

                // Make sure that all of the path variables are set correctly
                globalSettings.gfiPath = Util.GetGfiKeyValueString("Global", "Base Directory", "C:\\GFI");
                globalSettings.gfiBackupPath = Util.GetGfiKeyValueString("Global", "ConfigBackup", "C:\\GFI\\cnf\\ConfigBackup");
                globalSettings.gfiPath = FixPath(globalSettings.gfiPath);
                globalSettings.gfiBackupPath = FixPath(globalSettings.gfiBackupPath);

                // set path and file name defaults
                globalSettings.cnfFolder = "\\cnf\\";
                globalSettings.inittabFileName = "inittab";
                globalSettings.iniFileName = "gfi.ini";
                globalSettings.netconfigFileName = "netconfig.cfg";

                globalSettings.location = locID;
                globalSettings.currentLocation = locName;

                if (locID > 0)
                    globalSettings.cnfFolder = GetLocationFolder(locations.CurrentLocations[locID - 1].locNumber); // Get actual Location Number
                else
                    globalSettings.cnfFolder = "\\cnf\\";
                globalSettings.dirty = false;
                globalSettings.InittabChanged = false;
                globalSettings.NetConfigChanged = false;

                globalSettings.saveIni = false;
                globalSettings.saveInittab = false;
                globalSettings.saveNetconfig = false;

                CloseAllTabbedForm();
                if (locID > 0)
                { 
                    Cursor = Cursors.WaitCursor;
                    if (ReceiveConfiguration("gfi.ini", globalSettings.location))
                    {
                        ReceiveConfiguration("inittab", globalSettings.location);
                        ReceiveConfiguration("netconfig.cfg", globalSettings.location);
                    }
                    else
                    {
                        Cursor = Cursors.Default; 
                        return;
                    }
                }
                Cursor = Cursors.Default;

                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                Text = "Configuration Assistant [" + globalSettings.currentLocation + "]   Version:" + fvi.FileVersion;

                // Create the ini file loader and configuration classes
                if (this.globalSettings.enableGfiini)
                {
                    iniConfig = new IniConfiguration(this, globalSettings.gfiPath + globalSettings.cnfFolder, "gfi.ini");
                    iniConfig.iniFile.bUseDefaults = false;
                    iniConfig.ReadConfig();
                }

                if (this.globalSettings.enableNetconfig)
                {
                    netConfig = new NetConfiguration(this, globalSettings.gfiPath + globalSettings.cnfFolder, "netconfig.cfg");
                    netConfig.iniFile.bUseDefaults= true;
                    netConfig.ReadConfig();
                }

                LoadNodes();
                configTreeView.Enabled = true;
                UpdateTree();

                globalSettings.loaded = true;
                globalSettings.saveIni = false;
                globalSettings.saveInittab = false;
                globalSettings.saveNetconfig = false;
                if (globalSettings.enableGfiini)
                {
                    // Open the location tab
                    SelectForm(configTreeView.Nodes[0]);
                    // Select the location tab
                    configTreeView.SelectedNode = configTreeView.Nodes[0];
                }
                UpdateMenuStatus();
            }
        }



        protected void SetNodeStatus(TreeNode node, bool bStatus)
        {
            if (!bStatus)
            {
                node.Collapse();
                node.ForeColor = SystemColors.GrayText;
            }
            else
                node.ForeColor = SystemColors.MenuText;
        }

        protected void TraverseNodes(TreeNodeCollection nodes, string action)
        {
            foreach (TreeNode node in nodes)
            {
                // Test for nodes other than mux/vlt
                switch (node.Text.ToLower())
                {
                    case "vndldr":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableVndldr);
                        break;
                    case "clnt":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableClnt);
                        break;
                    case "farebox configuration":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.muxEnabled[0]);
                        break;
                    case "option flags":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.muxEnabled[0]);
                        break;
                    case "database":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableDatabase);
                        break;
                    case "mobile ticketing":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableMobileTicketing);
                        break;
                    case "udp listener":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableUDPListener);
                        break;
                    case "wifi":
                        if (iniConfig != null)
                        {
                            SetNodeStatus(node, iniConfig.inifileData.enableWifiProbing);
                        }
                        break;
                    case "wifi general settings":
                        if (iniConfig != null)
                            SetNodeStatus(node, iniConfig.inifileData.enableWifiProbing);
                        break;
                    case "probeserver (ff-ffe)":
                        SetNodeStatus(node, iniConfig.inifileData.enableProbeServer);
                        break;
                    case "rfsrvr (odyssey)":
                        SetNodeStatus(node, iniConfig.inifileData.enableRfsrvr);
                        break;
                    case "rfdevmgr(odyssey+)":
                        SetNodeStatus(node, iniConfig.inifileData.enableRfdevmgr);
                        break;
                    case "netconfig (ff-ffe)":
                        SetNodeStatus(node, iniConfig.inifileData.enableWifiProbing && globalSettings.enableNetconfig);
                        break;
                    case "inittab":
                        SetNodeStatus(node, this.globalSettings.enableInittab);
                        break;
                    case "gfi.ini":
                        SetNodeStatus(node, this.globalSettings.enableGfiini);
                        break;
                    case "location":
                        SetNodeStatus(node, this.globalSettings.enableGfiini);
                        break;
                }

                if (node.GetNodeCount(true) >= 0)
                {
                    switch (action.ToLower())
                    {
                        case "mux":     // Update the mux nodes
                            if (iniConfig != null)
                            {
                                switch (node.Text.ToLower())
                                {
                                    case "mux1":
                                        SetNodeStatus(node, iniConfig.inifileData.muxEnabled[0]);
                                        break;
                                    case "mux2":
                                        SetNodeStatus(node, iniConfig.inifileData.muxEnabled[1]);
                                        break;
                                    case "mux3":
                                        SetNodeStatus(node, iniConfig.inifileData.muxEnabled[2]);
                                        break;
                                }
                            }
                            break;
                        case "vaults":     // Update the vlt nodes
                            if (iniConfig != null)
                            {
                                switch (node.Text.ToLower())
                                {
                                    case "vlt1":
                                        SetNodeStatus(node, iniConfig.inifileData.vltEnabled[0]);
                                        break;
                                    case "vlt2":
                                        SetNodeStatus(node, iniConfig.inifileData.vltEnabled[1]);
                                        break;
                                    case "vlt3":
                                        SetNodeStatus(node, iniConfig.inifileData.vltEnabled[2]);
                                        break;
                                    case "vlt4":
                                        SetNodeStatus(node, iniConfig.inifileData.vltEnabled[3]);
                                        break;
                                    case "vlt5":
                                        SetNodeStatus(node, iniConfig.inifileData.vltEnabled[4]);
                                        break;
                                }
                            }
                            break;
                    }
                    TraverseNodes(node.Nodes, action);
                }
            }
        }

        public void UpdateTree()
        {
            // This needs to disable and close up the muxs and vaults that are not used.
            TraverseNodes(configTreeView.Nodes, "mux");
            TraverseNodes(configTreeView.Nodes, "vaults");
        }

        private void configTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if ((e.Node.Parent == null) || (e.Node.GetNodeCount(true) == 0))
            {
                if (e.Node.ForeColor != SystemColors.GrayText)
                {
                    SelectForm(e.Node);
                    activeNode = e.Node;
                }
                configTreeView.Focus();
            }
        }

        private TreeNode BuildNode(string name, ContextMenuStrip  mnu)
        {
            TreeNode mnuNode = new TreeNode(name);
            mnuNode.Name = name;
            if (mnu != null)
                mnuNode.ContextMenuStrip = mnu;
            return mnuNode;
        }

        /// <summary>
        /// New LoadNodes function
        /// </summary>
        private void LoadNodes()
        {
            TreeNode[] options1;

            // Remove all nodes from the tree
            configTreeView.Nodes.Clear();

            // Add Location Main Node
            configTreeView.Nodes.Add(BuildNode("Location",null));
            configTreeView.Nodes.Add(BuildNode("Gfi.ini",null));
            configTreeView.Nodes.Add(BuildNode("Inittab",null));
            configTreeView.Nodes.Add(BuildNode("NetConfig (FF-FFE)",null));

            configTreeView.Nodes[1].Nodes.Add(BuildNode("Mux",mnu));
            for (int i = 0; i < 3; i++)
            {
                // Mux2/Mux3 only get new COM/Debug options
                configTreeView.Nodes[1].Nodes[0].Nodes.Add(BuildNode("Mux" + (i+1).ToString(),mnu));
                if (i > 0)
                {
                    configTreeView.Nodes[1].Nodes[0].Nodes[i].Nodes.Add(BuildNode("[MUX"+(i+1).ToString()+"]"+" COM-Debug Settings",null));
                }
                else
                {
                    configTreeView.Nodes[1].Nodes[0].Nodes[i].Nodes.Add(BuildNode("[MUX"+(i+1).ToString()+"]"+" COM-Debug Settings",null));
                    configTreeView.Nodes[1].Nodes[0].Nodes[i].Nodes.Add(BuildNode("MUX Flags",null));
                    configTreeView.Nodes[1].Nodes[0].Nodes[i].Nodes.Add(BuildNode("MUX Flags2",null));
                    configTreeView.Nodes[1].Nodes[0].Nodes[i].Nodes.Add(BuildNode("MUX Debug Flags",null));
                }
            }

            
            // Set up the Vaults
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Vaults",mnu));
            for (int i = 0; i < 5; i++)
                configTreeView.Nodes[1].Nodes[1].Nodes.Add(BuildNode("Vlt" + (i + 1).ToString(),mnu));

            // Setup the Farebox Configuration with Option Flags
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Farebox Configuration",null));
            options1 = new TreeNode[32];
            for (int j = 0; j < 32; j++)
                options1[j] = BuildNode("OptionFlag" + (j + 1).ToString(),null);
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("General Settings",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Bills-Revenue-J1708",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("TransferPrintOption",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("FSMask/FSMask2",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Driver Logon",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Hourly Events",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Daylight Savings Time",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Media Flags",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Metro Options",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("Coin-Bill Options",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(BuildNode("TRiM-LCD-TLayout",null));
            configTreeView.Nodes[1].Nodes[2].Nodes.Add(new TreeNode("Option Flags", options1));

            // Set up the rest of the sections
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Database",mnu));

            // Add Wifi Nodes
            options1 = new TreeNode[4];
            options1[0] = BuildNode("Wifi General Settings", null);

            options1[1] = BuildNode("ProbeServer (FF-FFE)",mnu);
            options1[1].Nodes.Add(BuildNode("PRB General Settings", null));
            options1[1].Nodes.Add(BuildNode("PRB Flags", null));
            options1[1].Nodes.Add(BuildNode("PRB Flags2", null));
            options1[2] = BuildNode("rfsrvr (Odyssey)",mnu);
            options1[3] = BuildNode("rfdevmgr(Odyssey+)",mnu);
            TreeNode wifi = new TreeNode("Wifi", options1);
            wifi.ContextMenuStrip = mnu;
            configTreeView.Nodes[1].Nodes.Add(wifi);
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Clnt", mnu));
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Vndldr", mnu));
            configTreeView.Nodes[1].Nodes.Add(BuildNode("Mobile Ticketing", mnu));
            configTreeView.Nodes[1].Nodes.Add(BuildNode("UDP Listener", mnu));
            configTreeView.Nodes[1].Nodes.Add(BuildNode("User Defined Actions", null));
            
            ExpandToLevel(configTreeView.Nodes, 2);
        }

        public void CloseTabbedForm(string nodePath)
        {
            foreach (WindowList item in frmList)
            {
                if (string.Compare(nodePath, item.name, true) == 0)
                {
                    item.frm.Close();
                    item.tabPage.Dispose();
                    frmList.Remove(item);
                    break;
                }
            }
        }


        public void CloseAllTabbedForm()
        {
            foreach (WindowList item in frmList)
            {
                item.frm.Close();
                item.tabPage.Dispose();
            }
            configTreeView.Enabled = false;
            activeNode = null;
            frmList.Clear();
            if (findForm != null)
                findForm.Close();
            findForm = null;
        }

        public void OpenFormByName(string formName)
        {
            
            for (int i = 0; i < configTreeView.Nodes[1].Nodes.Count; i++)
            {
                if (configTreeView.Nodes[1].Nodes[i].Name.ToLower().Trim() == formName.ToLower().Trim())
                {
                    SelectForm(configTreeView.Nodes[1].Nodes[i]);
                    break;
                }
                else
                {
                    foreach (TreeNode node in configTreeView.Nodes[1].Nodes[i].Nodes)
                    {
                        if (node.Name.ToLower().Trim() == formName.ToLower().Trim())
                        {
                            SelectForm(node);
                            break;
                        }
                    }
                }
            }
                
        }

        public void SelectForm(TreeNode node)
        {
            WindowList newForm;
            bool found = false;
            int tabid;
            if (node == null)
                return;

            if ((activeNode != null) && (globalSettings.showSingleForm) )
            {
                string text = activeNode.FullPath;
                CloseTabbedForm(text);
            }

            foreach (WindowList item in frmList)
            {
                if (string.Compare(node.FullPath, item.tabPage.Name, true) == 0)
                {
                    item.frm.Show();
                    item.activated = true;
                    found = true;
                }
            }

            if (!found)
            {
                newForm = new WindowList();         // create list item form
                newForm.name = node.FullPath;       // set name
                activeNode = node;

                switch (node.Text)
                {
                    case "Location":
                        newForm.frm = new LocationForm(this);
                        break;
                    case "Inittab":
                        newForm.frm = new InittabForm(this);    
                        break;
                    case "Wifi General Settings":
                        newForm.frm = new WifiGeneralSettingsForm(this);  
                        break;
                    case "Clnt":
                        newForm.frm = new ClntForm(this);    
                        break;
                    case "Vndldr":
                        newForm.frm = new VndldrForm(this);    
                        break;
                    case "rfsrvr (Odyssey)":
                        newForm.frm = new RfsrvrForm(this);    
                        break;
                    case "rfdevmgr(Odyssey+)":
                        newForm.frm = new RfdevmgrForm(this);    
                        break;
                    case "Mobile Ticketing":
                        newForm.frm = new MbtkForm(this);    
                        break;
                    case "UDP Listener":
                        newForm.frm = new UDPListenerForm(this);
                        break;
                    case "User Defined Actions":
                        newForm.frm = new UserDefinedActionsForm(this);    
                        break;
                    case "Database":
                        newForm.frm = new DatabaseForm(this);    
                        break;
                    case "[MUX1] COM-Debug Settings":
                        newForm.frm = new ComPortSettingsForm(this, "mux1");
                        break;
                    case "[MUX2] COM-Debug Settings":
                        newForm.frm = new ComPortSettingsForm(this, "mux2");
                        break;
                    case "[MUX3] COM-Debug Settings":
                        newForm.frm = new ComPortSettingsForm(this, "mux3");    
                        break;
                    case "MUX Flags":
                        newForm.frm = new MuxFlagsForm(this);    
                        break;
                    case "MUX Flags2":
                        newForm.frm = new MuxFlagsForm2(this);    
                        break;
                    case "MUX Debug Flags":
                        newForm.frm = new MuxDebugFlagsForm(this);    
                        break;
                    case "TRiM-LCD-TLayout":
                        newForm.frm = new TrimLcdTllayoutHForm(this);    
                        break;
                    case "General Settings":
                        newForm.frm = new GeneralSettingsForm(this);
                        break;
                    case "Bills-Revenue-J1708":
                        newForm.frm = new BillsRevenueJ1708Form(this);    
                        break;
                    case "TransferPrintOption":
                        newForm.frm = new TransferPrintOptionForm(this);    
                        break;
                    case "FSMask/FSMask2":
                        newForm.frm = new FSMaskForm(this);    
                        break;
                    case "Driver Logon":
                        newForm.frm = new DriverForm(this);    
                        break;
                    case "Hourly Events":
                        newForm.frm = new HourlyEventsForm(this);    
                        break;
                    case "Daylight Savings Time":
                        newForm.frm = new DstForm(this);    
                        break;
                    case "Media Flags":
                        newForm.frm = new MediaFlagsForm(this);    
                        break;
                    case "Metro Options":
                        newForm.frm = new MetrOptionsForm(this);    
                        break;
                    case "Coin-Bill Options":
                        newForm.frm = new CoinBillOptionsForm(this);    
                        break;
                    case "NetConfig (FF-FFE)":
                        newForm.frm = new NetConfigForm(this);    
                        break;
                    case "Vlt1":
                    case "Vlt2":
                    case "Vlt3":
                    case "Vlt4":
                    case "Vlt5":
                        newForm.frm = new VaultForm(this);
                        break;
                    case "PRB Flags":
                        newForm.frm = new PrbFlagsForm(this);
                        break;
                    case "PRB Flags2":
                        newForm.frm = new PrbFlags2Form(this);
                        break;
                    case "PRB General Settings":
                        newForm.frm = new ProbeServerWiFiSettingsForm(this);
                        break;
                 }

                // create form
                if (newForm.frm == null)
                    CreateOptionFlagForm(newForm, node.Name);   // Is this an option flag?

                if (newForm.frm != null)
                {
                    // Set form attributes
                    newForm.frm.ControlBox = false;
                    newForm.frm.AutoScroll = true;
                    newForm.frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    newForm.frm.Text = node.FullPath;   // set window title
                    newForm.frm.TopLevel = false;       // Required for attached form
                    newForm.frm.AutoScroll = true;      // Allow scrolling 

                    // Add Form to Tab Control
                    newForm.tabPage = new System.Windows.Forms.TabPage();
                    newForm.tabPage.AutoScroll = true;
                    this.configTabControl.Controls.Add(newForm.tabPage);
                    newForm.tabPage.Name = node.FullPath;
                    newForm.tabPage.Text = node.Text;
                    newForm.frm.Text = node.Text;
                    newForm.tabPage.Controls.Add(newForm.frm);
                    newForm.tabPage.Select();
                    frmList.Add(newForm);
                    newForm.frm.Show();
                }
            }

            // Now make sure that we select the proper tab
            for (tabid = 0; tabid < configTabControl.TabPages.Count; tabid++)
                if (string.Compare(node.FullPath, configTabControl.TabPages[tabid].Name) == 0)
                    break;
            if (tabid < configTabControl.TabPages.Count)
                configTabControl.SelectTab(tabid);
        }


        private void configTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.ForeColor == SystemColors.GrayText)
                e.Cancel = true;
        }

        private void configTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.ForeColor == SystemColors.GrayText)
                e.Node.Checked = false;
        }

        public void CreateOptionFlagForm(WindowList newform, string id)
        {
            switch (id)
            {
                case "OptionFlag1":
                    newform.frm = new OptionFlags1Form(this);
                    break;
                case "OptionFlag2":
                    newform.frm = new OptionFlags2Form(this);
                    break;
                case "OptionFlag3":
                    newform.frm = new OptionFlags3Form(this);
                    break;
                case "OptionFlag4":
                    newform.frm = new OptionFlags4Form(this);
                    break;
                case "OptionFlag5":
                    newform.frm = new OptionFlags5Form(this);
                    break;
                case "OptionFlag6":
                    newform.frm = new OptionFlags6Form(this);
                    break;
                case "OptionFlag7":
                    newform.frm = new OptionFlags7Form(this);
                    break;
                case "OptionFlag8":
                    newform.frm = new OptionFlags8Form(this);
                    break;
                case "OptionFlag9":
                    newform.frm = new OptionFlags9Form(this);
                    break;
                case "OptionFlag10":
                    newform.frm = new OptionFlags10Form(this);
                    break;
                case "OptionFlag11":
                    newform.frm = new OptionFlags11Form(this);
                    break;
                case "OptionFlag12":
                    newform.frm = new OptionFlags12Form(this);
                    break;
                case "OptionFlag13":
                    newform.frm = new OptionFlags13Form(this);
                    break;
                case "OptionFlag14":
                    newform.frm = new OptionFlags14Form(this);
                    break;
                case "OptionFlag15":
                    newform.frm = new OptionFlags15Form(this);
                    break;
                case "OptionFlag16":
                    newform.frm = new OptionFlags16Form(this);
                    break;
                case "OptionFlag17":
                    newform.frm = new OptionFlags17Form(this);
                    break;
                case "OptionFlag18":
                    newform.frm = new OptionFlags18Form(this);
                    break;
                case "OptionFlag19":
                    newform.frm = new OptionFlags19Form(this);
                    break;
                case "OptionFlag20":
                    newform.frm = new OptionFlags20Form(this);
                    break;
                case "OptionFlag21":
                    newform.frm = new OptionFlags21Form(this);
                    break;
                case "OptionFlag22":
                    newform.frm = new OptionFlags22Form(this);
                    break;
                case "OptionFlag23":
                    newform.frm = new OptionFlags23Form(this);
                    break;
                case "OptionFlag24":
                    newform.frm = new OptionFlags24Form(this);
                    break;
                case "OptionFlag25":
                    newform.frm = new OptionFlags25Form(this);
                    break;
                case "OptionFlag26":
                    newform.frm = new OptionFlags26Form(this);
                    break;
                case "OptionFlag27":
                    newform.frm = new OptionFlags27Form(this);
                    break;
                case "OptionFlag28":
                    newform.frm = new OptionFlags28Form(this);
                    break;
                case "OptionFlag29":
                    newform.frm = new OptionFlags29Form(this);
                    break;
                case "OptionFlag30":
                    newform.frm = new OptionFlags30Form(this);
                    break;
                case "OptionFlag31":
                    newform.frm = new OptionFlags31Form(this);
                    break;
                case "OptionFlag32":
                    newform.frm = new OptionFlags32Form(this);
                    break;
            }
        }



        public void configTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // This needs to be done here because, the form is not created for some
            // reason if the form is closed and the node has not changed.
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (e.Node.ContextMenuStrip != null)
                {
                    bool bEnabled = (e.Node.ForeColor != SystemColors.GrayText);
                    e.Node.ContextMenuStrip.Items[0].Enabled = !bEnabled;
                    e.Node.ContextMenuStrip.Items[1].Enabled = bEnabled;
                }

                if (e.Node != null)
                {
                    Control parent = e.Node.TreeView.Parent;
                    while (!(parent is SplitForm))
                        parent = parent.Parent;
                    this.activeNode = e.Node;
                }
            }
            else
            {
                if ((e.Node != null) &&
                    (e.Node.GetNodeCount(true) == 0) &&
                    (e.Node.ForeColor != SystemColors.GrayText))
                {
                    Control parent = e.Node.TreeView.Parent;
                    while (!(parent is SplitForm))
                        parent = parent.Parent;
                    ((SplitForm)parent).SelectForm(e.Node);
                    this.activeNode = e.Node;
                }
            }
        }

        private bool OktoClearData()
        {
            bool clearOK = true;
            if (globalSettings.dirty || globalSettings.InittabChanged || globalSettings.NetConfigChanged)
            {
                clearOK = false;

                globalSettings.saveIni = false;
                globalSettings.saveInittab = false;
                globalSettings.saveNetconfig = false;
                
                Form confirm = new SaveConfigurations(this,true);
                confirm.ShowDialog();
                DialogResult confirmSave = ((SaveConfigurations)confirm).SaveOkCancel;
                globalSettings.bSaveCleanIniFile = ((SaveConfigurations)confirm).bSaveCleanIniFile;
                confirm.Dispose();

                switch (confirmSave)
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        Cursor = Cursors.WaitCursor;
                        SaveConfiguration();
                        Cursor = Cursors.Default;
                        clearOK = true;
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        clearOK = true;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        break;
                }
            }
            return clearOK;
        }

        private void toolClearBtn_Click(object sender, EventArgs e)
        {
            if (OktoClearData())
            {
                CloseAllTabbedForm();
                configTreeView.Nodes.Clear();
                frmList.Clear();
                activeNode = null;
                globalSettings.dirty = false;
                globalSettings.InittabChanged = false;
                globalSettings.NetConfigChanged = false;

                globalSettings.saveIni = false;
                globalSettings.saveInittab = false;
                globalSettings.saveNetconfig = false;

                globalSettings.loaded = false;
                inittabSelected.Enabled = true;
                gfiIniSelected.Enabled = true;
                netconfigSelected.Enabled = true;

                UpdateMenuStatus();
                Text = "Configuration Assistant   Version:" + fvi.FileVersion;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs   e)
        {
            brunning = false;
            if (OktoClearData())
            {
                CloseAllTabbedForm();
                configTreeView.Nodes.Clear();
                frmList.Clear();
                activeNode = null;
                Close();
                if (cnnODBC != null)
                {
                    try
                    {
                        cnnODBC.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ODBC Error: " + ex.Message);
                    }
                }
                if (cnnSqlite != null)
                {
                    try
                    {
                        cnnSqlite.Close();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show("SQLite Error: " + ex.Message);
                    }
                }
            }
        }

        private void showSingleFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            globalSettings.showSingleForm = !globalSettings.showSingleForm;
            showSingleFormToolStripMenuItem.Checked=globalSettings.showSingleForm;
        }

        private void SaveConfiguration()
        {
            // PJB check if any of the sections need to be removed.
            if (globalSettings.dirty)
            {
                iniConfig.WriteConfig();
                SendConfiguration("gfi.ini", globalSettings.location);
            }
            if (globalSettings.NetConfigChanged)
            {
                netConfig.WriteConfig();
                SendConfiguration("NetConfig.cfg", globalSettings.location);
            }

            if (globalSettings.InittabChanged)
            {
                // Only backup if this is a Host system
                if (globalSettings.location == 0)
                    BackupFile(globalSettings.gfiPath + globalSettings.cnfFolder + globalSettings.inittabFileName);
                foreach (WindowList item in frmList)
                {
                    if ((item.frm != null) && (item.frm is InittabForm))
                    {
                        ((InittabForm)item.frm).SaveInittab();
                        break;
                    }
                }
                SendConfiguration("inittab", globalSettings.location);
                globalSettings.InittabChanged = false;
            }
            globalSettings.dirty = false;   // Indicate file was saved
            globalSettings.InittabChanged = false;
            globalSettings.NetConfigChanged = false;
            UpdateMenuStatus();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OktoClearData();
        }

        public void GetComPortList(ComboBox PortBox)
        {
            string[] PortNames;
            SerialPort port = new SerialPort();
            PortBox.Items.Add("None");
            // Is this the Network Manager Configuration?
            if (globalSettings.systemType == NETWORK_MANAGER)
            {
                // Add dummpy ports as we do not know what the 
                // garage comfiguration is at this point
                for (int i = 0; i < 10; i++)
                    PortBox.Items.Add("COM" + (i + 1).ToString());
            }
            else  // Get the actual ports on the local computer
            {
                PortNames = SerialPort.GetPortNames();
                if (PortNames.Count() > 0)
                {
                    foreach (string str in PortNames)
                        PortBox.Items.Add(str);
                }
            }
        }

        public void BackupFile(string filename)
        {
            string newfilename;
            string datetime;
            if (File.Exists(filename))
            {
                DateTime dt = DateTime.Now;
                datetime = String.Format("_{0:yyyyMMdd_HHmmss}",dt);
                newfilename = globalSettings.gfiBackupPath + "\\" + Path.GetFileNameWithoutExtension(filename) + datetime + Path.GetExtension(filename);
                try
                {
                    if (!Directory.Exists(globalSettings.gfiBackupPath))
                        Directory.CreateDirectory(globalSettings.gfiBackupPath);
                    File.Copy(filename, newfilename);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error saving Backup File: " + ex.ToString());
                }
            }
        }

        private void compareConfiguration_Click(object sender, EventArgs e)
        {
            ChooseFiles cf = new ChooseFiles(this);
            //CompareFilesForm compare = new CompareFilesForm(this,"gfi.ini","gfi.ini.bak");
            cf.Show();
        }

        private void StartFind()
        {
            FindParamOrValue findParamValue = new FindParamOrValue(this);
            findParamValue.Show();
            UpdateMenuStatus();
        }


        private void toolFind_Click(object sender, EventArgs e)
        {
            StartFind();
        }

        private void findTextInIniFile_Clock(object sender, EventArgs e)
        {
            StartFind();
        }

        private void openConfigInNotepad_Click(object sender, EventArgs e)
        {
            // Enter in the command line arguments, everything you would enter after the executable name itself
            //string iniPath = globalSettings.cnfFolder + "\\gfi.ini";
            //string inittabPath= globalSettings.cnfFolder +"\\inittab";
            //string netconfigPath=globalSettings.cnfFolder+"\\netconfig.cfg";

            string iniPath = globalSettings.cnfFolder + "\\"+globalSettings.iniFileName;
            string inittabPath = globalSettings.cnfFolder + "\\" + globalSettings.inittabFileName;
            string netconfigPath = globalSettings.cnfFolder + "\\" + globalSettings.netconfigFileName;

            //inittabFileName = "inittab";
            //iniFileName = "gfi.ini";
            //netconfigFileName = "netconfig.cfg";
            bool bIniFile = true;
            bool bInitTab = true;
            bool bNetConfig = true;

            SaveConfigurations confirm = new SaveConfigurations(this, false);
            confirm.ShowDialog();
            DialogResult confirmSave = confirm.SaveOkCancel;
            if (confirmSave == DialogResult.Yes)
            {
                bIniFile = confirm.saveIni;
                bInitTab = confirm.saveInittab;
                bNetConfig = confirm.saveNetconfig;
                confirm.Dispose();
                if (bIniFile)
                    OpenConfigFile(globalSettings.gfiPath + iniPath,0,0);
                if (bInitTab)
                    OpenConfigFile(globalSettings.gfiPath + inittabPath,50,50);
                if (bNetConfig)
                    OpenConfigFile(globalSettings.gfiPath + netconfigPath,100,100);
            }
        }

        private void OpenConfigFile(string arg, int xoffset, int yoffset)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = arg;
            // Enter the executable to run, including the complete path
            start.FileName = "notepad.exe";
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Normal;
            
            start.CreateNoWindow = true;
            Process proc = Process.Start(start);
            /*
            IntPtr id = proc.MainWindowHandle;
            proc.WaitForInputIdle();
            bool ok = MoveWindow(proc.MainWindowHandle, xoffset+50, yoffset+50, xoffset+450, yoffset+450, false);
            if (!ok)
            {
                throw new System.ComponentModel.Win32Exception();
            }
            this.Refresh();
             * */
        }

        private void gfiiniFile_Changed(object sender, EventArgs e)
        {
            globalSettings.enableGfiini = ((CheckBox)sender).Checked;
            if (globalSettings.loaded)
                openLocation(globalSettings.location, globalSettings.currentLocation);
        }

        private void inittabFile_Changed(object sender, EventArgs e)
        {
            globalSettings.enableInittab = ((CheckBox)sender).Checked;
            if (globalSettings.loaded)
                openLocation(globalSettings.location, globalSettings.currentLocation);
        }

        private void netconfigFile_Changed(object sender, EventArgs e)
        {
            globalSettings.enableNetconfig = ((CheckBox)sender).Checked;
            if (globalSettings.loaded)
                openLocation(globalSettings.location, globalSettings.currentLocation);
        }

        private void SplitForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            brunning = false;
            OktoClearData();
        }

        private void SplitForm_Load(object sender, EventArgs e)
        {
            string user, pwd;
            if ((Util.GetGfiKeyValueDword("Global", "HttpMode", 0) == 0) && bnoLogin == false)
            {
                for (int j = 0; j < 3; j++)
                {
                    UserLoginForm confirmLogin = new UserLoginForm();
                    confirmLogin.ShowDialog();
                    DialogResult confirmSave = confirmLogin.OkCancel;
                    user = confirmLogin.usr;
                    pwd = confirmLogin.pwd;
                    confirmLogin.Dispose();
                    if (confirmSave != DialogResult.OK)
                        Close();
                    else
                    {
                        if ((user.ToLower() != "gfi") || (pwd != "gfi314159gfi"))
                        {
                            MessageBox.Show("User or Password is invalid!", "User Login");
                            if (j == 2)
                                Close();
                        }
                        else
                            break;
                    }
                }
            }
        }

        private void SendConfiguration(string filename, int loc)
        {
            if ((globalSettings.systemType == 2) && (locations.CurrentLocations.Count() > 0) && loc > 0)
            {
                HttpConfigClient httpClient = new HttpConfigClient();
                httpClient.SendRemoteConfigurationFile(locations.CurrentLocations[loc-1], globalSettings.gfiPath, filename, globalSettings.httpPort);
            }
        }


        private bool ReceiveConfiguration(string filename, int loc)
        {
            if ((globalSettings.systemType == 2) && (this.globalSettings.location > 0) && loc > 0)
            {
                HttpConfigClient httpClient = new HttpConfigClient();
                return (httpClient.RetrieveRemoteConfigurationFile(locations.CurrentLocations[loc - 1], globalSettings.gfiPath, filename, globalSettings.httpPort));
            }
            else
                return false;   // Error
        }



        private void configTabControl_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string text = ((TabControl)sender).SelectedTab.Name;
            CloseTabbedForm(text);
        }

        public bool SelectTreeviewNode(string text)
        {
            bool found=false;
            if (found = SelectTreeviewNode(this.configTreeView.Nodes, text))
                SelectForm(configTreeView.SelectedNode);
            return found;
        }

        protected bool SelectTreeviewNode(TreeNodeCollection nodes, string text)
        {
            bool found = false;
            foreach (TreeNode node in nodes)
            {
                if (found)
                    break;
                if (node.Text == text)
                {
                    configTreeView.SelectedNode = node;
                    found = true;
                }
                
                if (!found && node.GetNodeCount(true) >= 0)
                {
                    found = SelectTreeviewNode(node.Nodes, text);
                }
            }
            return found;
        }

        private void configTabControl_MouseClick(object sender, EventArgs e)
        {
            string text = ((TabControl)sender).SelectedTab.Text;
            SelectTreeviewNode(configTreeView.Nodes, text);
        }

        public void SetArrowEnable(bool bEnable)
        {
            toolSearchDown.Enabled = bEnable;
            toolSearchUp.Enabled=bEnable;
        }

        private void toolSearchDown_Click(object sender, EventArgs e)
        {
            int ret_cd;
            if (findForm != null)
            {
                ret_cd = findForm.SelectNextForm();
            }
            else
            {
                ret_cd = 0;
            }
            if (ret_cd == 0)
            {
                toolSearchDown.Enabled = false;
                toolSearchUp.Enabled = true;
            }
        }

        private void toolSearchUp_Click(object sender, EventArgs e)
        {
            int ret_cd=findForm.SelectPrevForm();
            if (ret_cd == 0)
            {
                toolSearchUp.Enabled = false;
                toolSearchDown.Enabled = true;
            }
        }

        // Set bit in hex string
        // src:         source
        // bitValue:    bit position
        // bClear:      true=clear bit, false=set bit
        public string SetClearHexBit(string src, UInt32 bitValue, bool bClear)
        {
            UInt32 value;
            value = Util.ParseValue(src);
            if (bClear)
                value &= (~bitValue);
            else
                value |= bitValue;
            src = "0x" + value.ToString("X4");
            return src;
        }

        public void EnableWifi(bool bEnable)
        {
            // Set the 
            SetClearHexBit(iniConfig.inifileData.optionFlags[5], 0x80, bEnable);
            foreach (WindowList item in frmList)
            {
                if (string.Compare("OptionFlag6", item.frm.Text, true) == 0)
                {
                    if (item.frm != null)
                        ((OptionFlags6Form)item.frm).EnableWifi(bEnable);
                    break;
                }
            }
        }

        public void RefreshFlag13()
        {
            foreach (WindowList item in frmList)
            {
                if (string.Compare("OptionFlag13", item.frm.Text, true) == 0)
                {
                    if (item.frm != null)
                        ((OptionFlags13Form)item.frm).setHexValue();
                    break;
                }
            }
        }

        private void toolOpenFile_Click(object sender, EventArgs e)
        {
            string filename = "";
            string ext;
            string filepath="";
            globalSettings.SinglefileOpened = true;
            if (OktoClearData())
            {
                CloseAllTabbedForm();
                configTreeView.Nodes.Clear();
                frmList.Clear();
                activeNode = null;
                
                OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
                openFileDialog1.InitialDirectory = globalSettings.gfiPath+"\\cnf";
                //openFileDialog1.Filter = "Configuration Files (*.ini)|*.ini,*.cfg|cfg gfiles (*.cfg)|*.cfg|All files (*.*)|*.*";
                openFileDialog1.Filter = @"Config Files (*.ini, *.cfg, inittab)|*.ini;*.cfg;inittab*.*|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.ValidateNames = false;
                openFileDialog1.CheckFileExists = false;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    filename = openFileDialog1.FileName;
                    globalSettings.dirty = false;

                    if (!File.Exists(filename))
                    {
                        if (MessageBox.Show("[" + filename + "]" + " dos not exist.\nDo you wish to create a new file?",
                            "Open Configuration File", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                        {
                            return;
                        }
                        globalSettings.dirty = true;
                    }
                    ext = Path.GetExtension(filename);
                    
                    // Get file type and set flags accordingly
                    //globalSettings.locationType = GARAGE;
                    globalSettings.locationType = LOCATION_BOTH;
                    globalSettings.location = 0;
                    globalSettings.currentLocation = "Custom";
                    globalSettings.cnfFolder = "\\";
                    globalSettings.gfiPath = Path.GetDirectoryName(filename);
                    globalSettings.gfiBackupPath = globalSettings.gfiPath + "\\ConfigBackup";

                    globalSettings.InittabChanged = false;
                    globalSettings.NetConfigChanged = false;

                    globalSettings.saveIni = false;
                    globalSettings.saveInittab = false;
                    globalSettings.saveNetconfig = false;

                    inittabSelected.Enabled = false;
                    gfiIniSelected.Enabled = false;
                    netconfigSelected.Enabled = false;

                    inittabSelected.Checked = false;
                    gfiIniSelected.Checked = false;
                    netconfigSelected.Checked = false;

                    globalSettings.enableGfiini = false;
                    globalSettings.enableNetconfig = false;
                    globalSettings.enableInittab = false;
                    filepath = globalSettings.gfiPath+globalSettings.cnfFolder;
                    switch (ext.ToLower())
                    {
                        case ".ini":
                            globalSettings.enableGfiini = true;
                            gfiIniSelected.Checked = true;
                            globalSettings.iniFileName = Path.GetFileName(filename);
                            filepath += globalSettings.iniFileName;
                            break;
                        case ".cfg":
                            globalSettings.enableNetconfig = true;
                            netconfigSelected.Checked = true;
                            globalSettings.netconfigFileName = Path.GetFileName(filename);
                            filepath += globalSettings.netconfigFileName;
                            break;
                        case "":
                            globalSettings.enableInittab = true;
                            inittabSelected.Checked = true;
                            globalSettings.inittabFileName = Path.GetFileName(filename);
                            filepath += globalSettings.inittabFileName;
                            break;
                    }

                    System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                    filepath = "Configuration Assistant [" + filepath + "]   Version:" + fvi.FileVersion;
                    if (globalSettings.currentLocation == "Custom")
                        Text = filepath;
                    else
                        Text = "Configuration Assistant [" + globalSettings.currentLocation + "]   Version:" + fvi.FileVersion;

                    // Create the ini file loader and configuration classes
                    if (this.globalSettings.enableGfiini)
                    {
                        iniConfig = new IniConfiguration(this, globalSettings.gfiPath + globalSettings.cnfFolder, globalSettings.iniFileName);
                        iniConfig.iniFile.bUseDefaults = true;
                        iniConfig.ReadConfig();
                    }

                    if (this.globalSettings.enableNetconfig)
                    {
                        netConfig = new NetConfiguration(this, globalSettings.gfiPath + globalSettings.cnfFolder, "netconfig.cfg");
                        netConfig.iniFile.bUseDefaults= true;
                        netConfig.ReadConfig();
                    }

                    LoadNodes();
                    configTreeView.Enabled = true;
                    UpdateTree();

                    globalSettings.loaded = true;
                    globalSettings.saveIni = false;
                    globalSettings.saveInittab = false;
                    globalSettings.saveNetconfig = false;
                    if (globalSettings.enableGfiini)
                    {
                        // Open the location tab
                        SelectForm(configTreeView.Nodes[0]);
                        // Select the location tab
                        configTreeView.SelectedNode = configTreeView.Nodes[0];
                    }
                    UpdateMenuStatus();
                }
            }

        }
        public void ClearFileSections()
        {
            gfiIniSelected.Enabled = false;
            inittabSelected.Enabled = false;
            netconfigSelected.Enabled = false;
        }

        private void toolFindClose_Click(object sender, EventArgs e)
        {
            if (findForm != null)
            {
                findForm.Close();
                UpdateMenuStatus();
            }
        }

        private void toolUpdateLocation_Click(object sender, EventArgs e)
        {
            int loc = GetCurrentLocation();
            UpdateLocationForm frm = new UpdateLocationForm(loc, this.globalSettings.location_name);
            frm.ShowDialog();
            //loc = frm.location;
            if (loc != frm.location)
            {
                SetCurrentLocation(frm.location);
            }
        }

        public int GetCurrentLocation()
        {
            int fCount;
            string fName, col = "1";
            try
            {
                if (globalSettings.systemType < 3)
                {
                    if (cnnODBC != null)
                    {
                        odbcConnect(null);
                        OdbcCommand DbCommand = cnnODBC.CreateCommand();
                        DbCommand.CommandText = "SELECT loc_n from DS";
                        OdbcDataReader DbReader = DbCommand.ExecuteReader();
                        fCount = DbReader.FieldCount;
                        fName = DbReader.GetName(0);
                        DbReader.Read();
                        col = DbReader.GetString(0);
                        DbReader.Close();
                        DbCommand.Dispose();
                        odbcDisConnect(null);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }

            if (col.Length == 0)
                col = "1";
            return ((int)Util.ParseValue(col));
        }
        static public string threadError;
        public static void QuickOpen(OdbcConnection connection, int timeout)
        {
            threadError = "";
            // We'll use a Stopwatch here for simplicity. A comparison to a stored DateTime.Now value could also be used
            Stopwatch sw = new Stopwatch();
            bool connectSuccess = false;

            // Try to open the connection, if anything goes wrong, make sure we set connectSuccess = false
            Thread t = new Thread(delegate()
            {
                try
                {
                    sw.Start();
                    connection.Open();
                    connectSuccess = true;
                }
                catch (Exception ex)
                {
                    threadError = "Connection Error: " + ex.ToString();

                }
            });

            // Make sure it's marked as a background thread so it'll get cleaned up automatically
            t.IsBackground = true;
            t.Start();

            // Keep trying to join the thread until we either succeed or the timeout value has been exceeded
            while (timeout > sw.ElapsedMilliseconds)
                if (t.Join(1))
                    break;

            // If we didn't connect successfully, throw an exception
            if (!connectSuccess)
            {
                if (threadError.Length==0)
                    threadError = "Connection Invalid: " + connection.ConnectionString;
                throw new Exception(threadError);
            }
        }
        
        public void odbcConnect(TagConnection cnn)
        {
            int databaseType = 1;
            globalSettings.connected = true;
            //////////////////////////////////////////////////////////////////////////////////////
            // Test calls for Powerbuilder access
            //globalSettings.connectionString = BuildConnectionString_DLL(); //--- FOR TESTING ONLY
            //globalSettings.connectionString = BuildConnectionString_A(); //--- FOR TESTING ONLY

            // Use the following call for the Configuration Assistant
            if (cnn == null)
            {
                globalSettings.connectionString = globalSettings.connectionString = BuildConnectionStringEx();
                databaseType = globalSettings.systemType;

            }
            else
            {
                globalSettings.connectionString = cnn.connection;
                databaseType = cnn.databaseType;
            }

            //if (globalSettings.systemType==1)
            //    DbConnect();        // Make sure the server is started if this is a Sybase Database
            globalSettings.connectionError = null;

            switch (databaseType)
            {
                case 1: // Sybase
                case 2: // MSS
                    cnnODBC = new OdbcConnection(globalSettings.connectionString);
                    try
                    {
                        QuickOpen(cnnODBC, globalSettings.connectionTimeout * 1000);
                    }

                    catch (Exception ex)
                    {
                        globalSettings.connectionError = "ODBC Error: " + ex.Message;
                        MessageBox.Show(globalSettings.connectionError, "Database Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cnnODBC = null;
                        globalSettings.connected = false;
                    }
                    break;
                case 3: // SQLite
                    cnnSqlite = new SQLiteConnection(globalSettings.connectionString);
                    try
                    {
                        cnnSqlite.Open();
                    }
                    catch (SQLiteException ex)
                    {
                        globalSettings.connectionError = "ODBC Error: " + ex.Message;
                        MessageBox.Show(globalSettings.connectionError, "Database Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cnnSqlite = null;
                        globalSettings.connected = false;
                    }
                    break;
                case 4: // MySql
                    cnnMySql = new MySqlConnection(globalSettings.connectionString);
                    try
                    {
                        cnnMySql.Open();
                    }
                    catch (MySql.Data.MySqlClient.MySqlException ex)
                    {
                        globalSettings.connectionError = "MySql Error: " + ex.Message;
                        MessageBox.Show(globalSettings.connectionError, "Database Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cnnMySql = null;
                        globalSettings.connected = false;
                    }
                    break;
                default:
                    globalSettings.connected = false;
                    break;
            }
        }

        public void odbcDisConnect(TagConnection cnn)
        {
            if ((cnn == null) || (cnnODBC == null))
            {
                if (cnnODBC != null)
                    cnnODBC.Close();
            }
            else
            {
                switch (cnn.databaseType)
                {
                    case 1: // Sybase
                    case 2: // MSS
                        cnnODBC.Close();
                        break;
                    case 3: // SQLite
                        cnnSqlite.Close();
                        break;
                    case 4: // MySql
                        cnnMySql.Close();
                        break;
                }
            }
            globalSettings.connected = false;
        }
        
        public string GetCurrentLocationName()
        {
            int fCount;
            string fName, loc_name="GC Computer";
            try
            {
                if (globalSettings.systemType < 3)
                {
                    odbcConnect(null);
                    if (globalSettings.connected)
                    {
                        OdbcCommand DbCommand = cnnODBC.CreateCommand();
                        DbCommand.CommandText = "SELECT loc_name from cnf";
                        OdbcDataReader DbReader = DbCommand.ExecuteReader();
                        fCount = DbReader.FieldCount;
                        fName = DbReader.GetName(0);
                        DbReader.Read();
                        loc_name = DbReader.GetString(0);
                        DbReader.Close();
                        DbCommand.Dispose();
                        odbcDisConnect(null);
                    }
                }
                else
                {
                    loc_name = "Data System 8";
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }

            if (loc_name.Length == 0)
                loc_name = "GC Location";
            return (loc_name);
        }

        public void SetCurrentLocation(int locationID)
        {
            try
            {
                odbcConnect(null);
                OdbcCommand DbCommand = cnnODBC.CreateCommand();
                DbCommand.CommandText = "update ds set loc_n=" + locationID.ToString() + ";";
                OdbcDataReader DbReader = DbCommand.ExecuteReader();
                DbReader.Close();
                DbCommand.Dispose();
                globalSettings.locationID = locationID;
                if (frmList.Count > 0)
                {
                    if (frmList[0].frm != null)
                    {
                        ((LocationForm)frmList[0].frm).UpdateLocation();
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }
            odbcDisConnect(null);
        }



        private void querytoolStripMenu_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            Form frm = new odbctestForm(this);
            frm.Show();
        }

        private void restartServicestoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceController sc = new ServiceController("Initservice");
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) ||
                     (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    StartService("Initservice", 10000);
                }
                else
                {
                    StopService("Initservice", 10000);
                    StartService("Initservice", 10000);
                }
            }
            catch
            {
            }
        }

        public void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service Stop Error: " + ex.Message);
            }
        }

        public void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service Start Error: " + ex.Message);
            }
        }

        ///////////////////////////////////////////////////////////////////
        // This will get the SQL Server Driver Name
        ///////////////////////////////////////////////////////////////////
        public string GetSQLServerDriverName() 
        {
            string keyValue="";
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers");
            if (regKey != null)
            {
                foreach (var value in regKey.GetValueNames())
                {
                    string data = Convert.ToString(regKey.GetValue(value)).ToLower();
                    if (data.CompareTo("installed") == 0)
                    {
                        keyValue = Convert.ToString(value);
                        if (keyValue.ToLower().Contains("sql server native client"))
                        {
                            globalSettings.mssDriver = value;
                            break;
                        }
                    }
                }
            }
            return keyValue;
        }

        // Old version of Build Connection String
        
        public string BuildConnectionStringEx()
        {
            string connString = "";
            switch (globalSettings.systemType)
            {
                case 1: // Garage Computer
                    if ((globalSettings.sysconDsn.ToLower().CompareTo("gfi") == 0) || globalSettings.sysconDsn.Length==0)
                        globalSettings.sysconDsn = globalSettings.sysconEng;
                    connString = "LINKS=tcpip(Host=" + globalSettings.sysconHost +
                                 ";DoBroadcast=Direct);ENG=" + globalSettings.sysconEng +
                                 ";DBN=" + globalSettings.sysconDsn +
                                 ";UID=" + globalSettings.sysconUser +
                                 ";PWD=" + globalSettings.sysconPassword +
                                 ";Port=" + globalSettings.connectionPort.ToString() +
                                 ";ASTOP=NO;Driver=Adaptive Server Anywhere 9.0;App=ConfigurationAssistant";
                    break;
                case 2: // Network Manager:
                    connString = "DRIVER={" + globalSettings.mssDriver + "};" +
                                 "Server=" + globalSettings.sysconHost +
                                 ";Port=" + globalSettings.connectionPort.ToString() +
                                 ";Database="+globalSettings.dbName+";UID=" + globalSettings.sysconUser +
                                 ";PWD=" + globalSettings.sysconPassword + ";";
                    break;
                case 3:
                    if (globalSettings.sysconPassword.Length> 0)
                        connString = @"Data Source=" + globalSettings.dbName + "; Version=3; Password="+globalSettings.sysconPassword+"; FailIfMissing=True; Foreign Keys=True;";
                    else
                        connString = @"Data Source="+globalSettings.dbName+"; Version=3; FailIfMissing=True; Foreign Keys=True;";
                    break;
                case 4:
                    if (globalSettings.sysconPassword.Length == 0)
                        connString = @"Server=" + globalSettings.sysconHost + "; database=" + globalSettings.dbName + "; UID=" + globalSettings.sysconUser + ";Convert Zero Datetime=True;";
                    else
                        //connString = @"Server=" + globalSettings.sysconHost + "; database=" + globalSettings.dbName + "; UID=" + globalSettings.sysconUser + "; Password=" + globalSettings.sysconPassword + ";Convert Zero Datetime=True;";
                        connString = @"Server=" + globalSettings.sysconHost + "; database=" + globalSettings.dbName + "; UID=" + globalSettings.sysconUser + "; Password=" + globalSettings.sysconPassword + ";Convert Zero Datetime=True; Port=" + globalSettings.connectionPort.ToString() + ";";
                    break;
                default:
                    connString = "";
                    break;
            }
            return connString;
        }

        public string BuildConnectionString_DLL()
        {
            int i;
            byte[] buffer1 = new byte[512];
            for (i = 0; i < buffer1.Length; i++)
                buffer1[i] = 0;
           BuildConnectionString(buffer1);

            // Get the number of chareacters in the array
            for (i=0;i<buffer1.Length;i++)
                if (buffer1[i]==0)
                    break;
            // Resize the array
            Array.Resize(ref buffer1, i);

            // Build string and return
            return System.Text.Encoding.Default.GetString(buffer1).Trim();
        }

        // New version of BuildConnectionString - to be completed
        //public string BuildConnectionString_A()
        //{
        //    int i,j;
        //    byte[] buffer1 = new byte[512];
        //    for (i = 0; i < buffer1.Length; i++)
        //        buffer1[i] = 0;
        //    if (globalSettings.systemType==this.NETWORK_MANAGER)
        //        GetDatabaseConnection(0, MSS_CONNECTION, 0, buffer1);
        //    else
        //        GetDatabaseConnection(0, ASA_CONNECTION, 0, buffer1);

        //    // Get the number of chareacters in the array
        //    for (i=0;i<buffer1.Length;i++)
        //        if (buffer1[i]==0)
        //            break;
        //    // Resize the array
        //    Array.Resize(ref buffer1, i);

        //    // Build string and return
        //    return System.Text.Encoding.Default.GetString(buffer1).Trim();
        //}

        public bool SQLExecuteCommand(string cmd)
        {
            bool ret_cd=false;
            if (cnnODBC != null)
            {
                try
                {
                    OdbcCommand DbCommand = cnnODBC.CreateCommand();
                    DbCommand.CommandText = cmd;
                    OdbcDataReader DbReader = DbCommand.ExecuteReader();
                    DbReader.Close();
                    DbCommand.Dispose();
                    ret_cd=true;
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                    ret_cd = false;
                }
            }
            else
            {
                MessageBox.Show("Error: Connection not open.");
                ret_cd = false;
            }
            return ret_cd;
        }

        // Sybase requires authentiction for updates
        public void sybaseAuthenticate()
        {
            if (cnnODBC != null)
            {
                if (SQLExecuteCommand(@"SET OPTION PUBLIC.DATABASE_AUTHENTICATION='Company=GFI Genfare;Application=GFI Proprietary Software;Signature=010fa55157edb8e14d818eb4fe3db41447146f1571g39e25604f20163b2c7992f0434a898ce2abb8a00';"))
                    SQLExecuteCommand(@"SET TEMPORARY OPTION CONNECTION_AUTHENTICATION='Company=GFI Genfare;Application=GFI Proprietary Software;Signature=000fa55157edb8e14d818eb4fe3db41447146f1571g5ca629198a43a3e5015ca27d20030f205aac7a1a';");
            }
        }
        public void UpdateServiceControls()
        {
            if (globalSettings.bInitServiceInstalled)
            {
                if (globalSettings.bInitServiceRunning)
                {
                    startServiceTool.Enabled = false;
                    stopServiceTool.Enabled = true;
                    restartServicestoolStripMenuItem.Enabled = true;
                }
                else
                {
                    startServiceTool.Enabled = true;
                    stopServiceTool.Enabled = false;
                    restartServicestoolStripMenuItem.Enabled = false;
                }
            }
            else
            {
                startServiceTool.Enabled = false;
                stopServiceTool.Enabled = false;
                restartServicestoolStripMenuItem.Enabled = false;
            }
        }

        private void CheckInitServiceRunning()
        {
            brunning = true;
            try
            {
                ServiceController sc = new ServiceController("Initservice");
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) ||
                     (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                    globalSettings.bInitServiceRunning = false;
                else
                    globalSettings.bInitServiceRunning = true;
            }
            catch
            {
                globalSettings.bInitServiceInstalled = false;
                return;
            }
            globalSettings.bInitServiceInstalled = true;
            while (brunning)
            {
                if (brunning)
                {
                    try
                    {
                        dUpdateServiceControls d = new dUpdateServiceControls(UpdateServiceControls);
                        this.Invoke(d);
                        Thread.Sleep(1000);
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void startServiceTool_Click(object sender, EventArgs e)
        {
            StartService("Initservice", 10000);
        }

        private void stopServiceTool_Click(object sender, EventArgs e)
        {
            StopService("Initservice", 10000);
        }


        //private void SelectFile(TextBox text, string ext)
        public string SelectFile(string text, string filter)
        {
            OpenFileDialog openFileDialog1;
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.InitialDirectory = text;
            //openFileDialog1.Filter = ext + "|All files (*.*)|*.*";
            if (filter == null)
                openFileDialog1.Filter = "(*.ini, *.cfg)|*.ini;*.cfg|All files (*.*)|*.*";
            else
                openFileDialog1.Filter = filter;

            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = false;
            openFileDialog1.ValidateNames = false;
            openFileDialog1.CheckFileExists = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                return openFileDialog1.FileName;
            else
                return null;
        }

        private void connectionMenu_Click(object sender, EventArgs e)
        {
            Form frm = new DBConnection(this);
            frm.ShowDialog();
        }

        private void SplitForm_Activated(object sender, EventArgs e)
        {
            if (bRunOdbcOnly)
                Close();
        }
    }

    public class GlobalPrintSettings
    {
        // Printer settings
        public string title { get; set; }
        public string subTitle { get; set; }
        public string footer { get; set; }
        public bool pageNumbers { get; set; }
        public bool pageNumbersInHeader { get; set; }
        public bool bProportionalColumns { get; set; }
        public int footerSpacing { get; set; }

        public StringAlignment titleComboLCR { get; set; }
        public StringAlignment subTitleComboLCR { get; set; }
        public StringAlignment footerComboLCR { get; set; }
        public StringAlignment gridAlignment { get; set; }
        public GlobalPrintSettings()
        {
            // Printer settings
            title = "Title";
            subTitle = "";
            footer = "";
            pageNumbers = true;
            pageNumbersInHeader = false;
            bProportionalColumns = true;

            titleComboLCR = StringAlignment.Center;
            subTitleComboLCR = StringAlignment.Center;
            footerComboLCR = StringAlignment.Center;
            gridAlignment = StringAlignment.Near;

            //titleComboLCR = 1;
            //subTitleComboLCR = 1;
            //footerComboLCR = 2;
            footerSpacing = 15;
        }
    }

    public class GlobalSettings
    {
        public bool database = false;
        public string dotNetVersion = "";
        public string HKLM_GFI_GLOBAL = "Software\\GFI Genfare\\Global";
        public string HKLM_GFI_CONNECT = "Software\\GFI Genfare\\Global\\Connection";
        public string dbName { get; set; }
        public string sysconEng {get;set;}
        public string sysconHost { get; set; }
        public string sysconDsn { get; set; }
        public string sysconUser { get; set; }
        public string sysconPassword { get; set; }
        public string mssDriver {get; set; }
        public int locationID { get; set; }     // This is the actual location number if GC
        public string location_name;            // default GC location Name
        public string connectionString;
        public string connectionError;
        public int connectionTimeout;
        public string inittabFileName { get; set; }
        public string iniFileName { get; set; }
        public string netconfigFileName { get; set; }
        public bool bInitServiceRunning {get; set;}
        public string currentLocation { get; set; }
        public string gfiPath { get; set; }
        public string gfiBackupPath { get; set; }
        public string cnfFolder { get; set; }
        public int connectionPort { get; set; }
        public int httpPort { get; set; }
        public int location { get; set; }
        public bool dirty { get; set; }
        public bool InittabChanged { get; set; }
        public bool NetConfigChanged { get; set; }

        public bool saveIni { get; set; }
        public bool saveInittab { get; set; }
        public bool saveNetconfig { get; set; }

        public bool loaded { get; set; }
        public bool active { get; set; }
        public int systemType { get; set; }

        // configuration type 0=network managet, 1=garage
        public int locationType { get; set; }  
        public bool showSingleForm { get; set; }
        public bool enableGfiini { get; set; }
        public bool enableInittab { get; set; }
        public bool enableNetconfig { get; set; }
        public bool connected { get; set; }
        public bool bInitServiceInstalled;
        public bool bSaveCleanIniFile { get; set; }
        public bool SinglefileOpened { get; set; }


        public GlobalSettings()
        {
            SinglefileOpened = false;
            bSaveCleanIniFile = false;
            connected = false;
            connectionTimeout = 15;         // 15 second connection timeout is default
            locationID = 1;                 // Set location ID to default for GC
            dbName = "gfi";
            location_name="GC Location";    // Set default location name
            inittabFileName ="inittab";
            iniFileName ="gfi.ini";
            netconfigFileName = "netconfig.cfg";
            bInitServiceRunning = false;
            currentLocation = "Locations";
            gfiPath = "C:\\GFI";
            gfiBackupPath = "C:\\GFI\\cnf\\ConfigBackup";
            httpPort = 80;
            connectionPort = 3306;
            location = 0;
            dirty = false;
            //SearchMode = false;
            dirty = false;
            InittabChanged = false;
            NetConfigChanged = false;

            active = false;
            systemType =  1;
            showSingleForm = false;
            //enableWifiProbing = true;
            enableGfiini = true;
            enableInittab = true;
            enableNetconfig = true;
            loaded = false;
        }
    }
}
