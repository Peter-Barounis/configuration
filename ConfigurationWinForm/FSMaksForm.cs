﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class FSMaskForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public ToolTip toolTip1 = new ToolTip();
        private CheckBox[] cbxArrayFSMask= new CheckBox[10];
        private CheckBox[] cbxArrayFSMask2= new CheckBox[20];
        public uint fsmask, fsmask2;
        bool formLoaded = false;
        public FSMaskForm()
        {
            InitializeComponent();
        }

        public FSMaskForm(SplitForm sf)
        {
            int tmp;
            this.splitForm = sf;
            InitializeComponent();

            fsmask = Util.ParseValue(splitForm.iniConfig.inifileData.FSMask);
            fsmask2 = Util.ParseValue(splitForm.iniConfig.inifileData.FSMask2);
            // FSMAKS (48 TTP)
            cbxArrayFSMask[0] = this.checkBox3;
            cbxArrayFSMask[1] = this.checkBox4;
            cbxArrayFSMask[2] = this.checkBox5;
            cbxArrayFSMask[3] = this.checkBox6;
            cbxArrayFSMask[4] = this.checkBox7;
            cbxArrayFSMask[5] = this.checkBox8;
            cbxArrayFSMask[6] = this.checkBox9;
            cbxArrayFSMask[7] = this.checkBox10;
            cbxArrayFSMask[8] = this.checkBox11;
            cbxArrayFSMask[9] = this.checkBox12;

            // FSMASK2 (241 TTP)
            cbxArrayFSMask2[0] = this.checkBox24;
            cbxArrayFSMask2[1] = this.checkBox23;
            cbxArrayFSMask2[2] = this.checkBox22;
            cbxArrayFSMask2[3] = this.checkBox21;
            cbxArrayFSMask2[4] = this.checkBox20;
            cbxArrayFSMask2[5] = this.checkBox19;
            cbxArrayFSMask2[6] = this.checkBox18;
            cbxArrayFSMask2[7] = this.checkBox17;
            cbxArrayFSMask2[8] = this.checkBox16;
            cbxArrayFSMask2[9] = this.checkBox15;

            cbxArrayFSMask2[10] = this.checkBox34;
            cbxArrayFSMask2[11] = this.checkBox33;
            cbxArrayFSMask2[12] = this.checkBox32;
            cbxArrayFSMask2[13] = this.checkBox31;
            cbxArrayFSMask2[14] = this.checkBox30;
            cbxArrayFSMask2[15] = this.checkBox29;
            cbxArrayFSMask2[16] = this.checkBox28;
            cbxArrayFSMask2[17] = this.checkBox27;
            cbxArrayFSMask2[18] = this.checkBox26;
            cbxArrayFSMask2[19] = this.checkBox25;
            //UpdateFSMask();

            // FSMASK
            checkBox1.Checked = ((fsmask & (uint)0x8000) != 0);
            for (int i = 0; i < 10; i++)
                cbxArrayFSMask[i].Checked = ((fsmask & (uint)1 << i) != 0);
            tmp = (int)(fsmask & 0x3f00 >> 10)+1;
            if (tmp > 10 || tmp==0 )
                tmp=1;
            defaultFareset48.Value = tmp;

            // FSMAKS2
            checkBox14.Checked = ((fsmask2 & (uint)0x02000000) != 0);
            for (int i = 0; i < 20; i++)
                cbxArrayFSMask2[i].Checked = ((fsmask2 & (uint)1 << i) != 0);
            tmp = (int)(fsmask2 & 0x01F00000 >> 20)+1;
            if (tmp > 20 || tmp==0)
                tmp = 1;
            defaultFareset241.Value = tmp;
            formLoaded = true;

            UpdateFSMask();
            setFSMaskHexValue();
        }

        public void UpdateFSMask()
        {
            // FSMASK
            defaultFareset48.Enabled = checkBox1.Checked;
            for (int i=0;i<10;i++)
                cbxArrayFSMask[i].Enabled = checkBox1.Checked;
     
            // FSMASK2
            defaultFareset241.Enabled = checkBox14.Checked;
            for (int i = 0; i < 20; i++)
                cbxArrayFSMask2[i].Enabled = checkBox14.Checked;
        }

        private void setFSMaskHexValue()
        {
            splitForm.iniConfig.inifileData.FSMask = "0x"+ fsmask.ToString("X4");
            splitForm.iniConfig.inifileData.FSMask2 = "0x"+ fsmask2.ToString("X8");
            groupBox1.Text = "FSMask (48 TTP Fare Structure)-[0x" + splitForm.iniConfig.inifileData.FSMask + "]";
            groupBox2.Text = "FSMask2 (241 TTP Fare Structure)-[0x" + splitForm.iniConfig.inifileData.FSMask2 + "]";
        }

        private void updateFlag(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                int index = ((CheckBox)sender).TabIndex;
                if (index >= 14)   // FSMASK2
                {
                    index -= 14;
                    if (cbxArrayFSMask2[index].Checked)
                        fsmask2 |= ((uint)1 << index);
                    else
                        fsmask2 &= ~((uint)1 << index);
                }
                else                // FSMASK
                {
                    index -= 4;
                    if (cbxArrayFSMask[index].Checked)
                        fsmask |= ((uint)1 << index);
                    else
                        fsmask &= ~((uint)1 << index);
                }
                setFSMaskHexValue();
                splitForm.SetChanged();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (checkBox1.Checked)
                    fsmask |= ((uint)0x8000);
                else
                    fsmask &= ~((uint)0x8000);  
                UpdateFSMask();
                setFSMaskHexValue();
                splitForm.SetChanged();
            }
        }

        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (checkBox14.Checked)
                    fsmask2 |= ((uint)0x02000000);
                else
                    fsmask2 &= ~((uint)0x02000000);  
                UpdateFSMask();
                setFSMaskHexValue();
                splitForm.SetChanged();
            }
        }

        private void defaultFareset48_ValueChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                fsmask &= ~((uint)0x3C00);
                fsmask |= ((uint)(defaultFareset48.Value - 1) << 10);
                setFSMaskHexValue();
                splitForm.SetChanged();
            }
        }

        private void defaultFareset241_ValueChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                fsmask2 &= ~((uint)0x01F00000);
                fsmask2 |= ((uint)(defaultFareset241.Value - 1) << 20);
                setFSMaskHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
