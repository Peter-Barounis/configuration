﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace ConfigurationWinForm
{
    public partial class CompareFilesForm : Form
    {
        public SplitForm splitForm;
        public IniConfiguration iniConfigSrc;
        public IniConfiguration iniConfigDest;
        public NetConfiguration netConfigSrc, netConfigDest;
        public List<FieldInfo> fieldInfoList = new List<FieldInfo>();
        private string fileType;

        public CompareFilesForm()
        {
            InitializeComponent();
        }
        public CompareFilesForm(SplitForm sf, string src, string dest)
        {
            splitForm = sf;
            fileType = Path.GetExtension(src).ToLower();
            
            InitializeComponent();
            if (fileType == ".ini")
            {
                iniConfigSrc = new IniConfiguration(splitForm, Path.GetDirectoryName(src)+"\\", Path.GetFileName(src));
                iniConfigSrc.iniFile.bUseDefaults = false;
                iniConfigSrc.ReadConfig();

                iniConfigDest = new IniConfiguration(splitForm, Path.GetDirectoryName(dest)+"\\", Path.GetFileName(dest));
                iniConfigDest.iniFile.bUseDefaults = false;
                iniConfigDest.ReadConfig();

                dataGridView1.Columns[1].HeaderText = src;
                dataGridView1.Columns[2].HeaderText = dest;
            }
            else 
            {
                netConfigSrc = new NetConfiguration(splitForm, Path.GetDirectoryName(src)+"\\", Path.GetFileName(src));
                netConfigSrc.iniFile.bUseDefaults = false;
                netConfigSrc.ReadConfig();

                netConfigDest = new NetConfiguration(splitForm, Path.GetDirectoryName(dest)+"\\", Path.GetFileName(dest));
                netConfigDest.iniFile.bUseDefaults = false;
                netConfigDest.ReadConfig();

                dataGridView1.Columns[1].HeaderText = src;
                dataGridView1.Columns[2].HeaderText = dest;
            }
            populateDatagridView(fileType);
        }
        /// <summary>
        /// Utility class for comparing objects.
        /// </summary>
        public static class ObjectComparer
        {
            /// <summary>
            /// Compares the public properties of any 2 objects and determines if the properties of each
            /// all contain the same value.
            /// <para> 
            /// In cases where object1 and object2 are of different Types (both being derived from Type T) 
            /// we will cast both objects down to the base Type T to ensure the property comparison is only 
            /// completed on COMMON properties.
            /// (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
            /// both objects will be cast to Foo for comparison)
            /// </para>
            /// </summary>
            /// <typeparam name="T">Any class with public properties.</typeparam>
            /// <param name="object1">Object to compare to object2.</param>
            /// <param name="object2">Object to compare to object1.</param>
            /// <param name="propertyInfoList">A List of <see cref="PropertyInfo"/> objects that contain data on the properties
            /// from object1 that are not equal to the corresponding properties of object2.</param>
            /// <returns>A boolean value indicating whether or not the properties of each object match.</returns>
            public static bool GetDifferentProperties<T>(T object1, T object2, out List<PropertyInfo> propertyInfoList)
                where T : class
            {
                return GetDifferentProperties<T>(object1, object2, null, out propertyInfoList);
            }

            /// <summary>
            /// Compares the public properties of any 2 objects and determines if the properties of each
            /// all contain the same value.
            /// <para> 
            /// In cases where object1 and object2 are of different Types (both being derived from Type T) 
            /// we will cast both objects down to the base Type T to ensure the property comparison is only 
            /// completed on COMMON properties.
            /// (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
            /// both objects will be cast to Foo for comparison)
            /// </para>
            /// </summary>
            /// <typeparam name="T">Any class with public properties.</typeparam>
            /// <param name="object1">Object to compare to object2.</param>
            /// <param name="object2">Object to compare to object1.</param>
            /// <param name="ignoredProperties">A list of <see cref="PropertyInfo"/> objects
            /// to ignore when completing the comparison.</param>
            /// <param name="propertyInfoList">A List of <see cref="PropertyInfo"/> objects that contain data on the properties
            /// from object1 that are not equal to the corresponding properties of object2.</param>
            /// <returns>A boolean value indicating whether or not the properties of each object match.</returns>
            public static bool GetDifferentProperties<T>(T object1, T object2, List<PropertyInfo> ignoredProperties, out List<PropertyInfo> propertyInfoList)
                where T : class
            {
                propertyInfoList = new List<PropertyInfo>();

                // If either object is null, we can't compare anything
                if (object1 == null || object2 == null)
                {
                    return false;
                }

                Type object1Type = object1.GetType();
                Type object2Type = object2.GetType();

                // In cases where object1 and object2 are of different Types (both being derived from Type T) 
                // we will cast both objects down to the base Type T to ensure the property comparison is only 
                // completed on COMMON properties.
                // (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
                // both objects will be cast to Foo for comparison)
                if (object1Type != object2Type)
                {
                    object1Type = typeof(T);
                    object2Type = typeof(T);
                }

                // Remove any properties to be ignored
                List<PropertyInfo> comparisonProps =
                    RemoveProperties(object1Type.GetProperties(), ignoredProperties);

                foreach (PropertyInfo object1Prop in comparisonProps)
                {
                    Type propertyType = null;
                    object object1PropValue = null;
                    object object2PropValue = null;

                    // Rule out an attempt to check against a property which requires
                    // an index, such as one accessed via this[]
                    if (object1Prop.GetIndexParameters().GetLength(0) == 0)
                    {
                        // Get the value of each property
                        object1PropValue = object1Prop.GetValue(object1, null);
                        object2PropValue = object2Type.GetProperty(object1Prop.Name).GetValue(object2, null);

                        // As we are comparing 2 objects of the same type we know
                        // that they both have the same properties, so grab the
                        // first non-null value
                        if (object1PropValue != null)
                            propertyType = object1PropValue.GetType().GetInterface("IComparable");

                        if (propertyType == null)
                            if (object2PropValue != null)
                                propertyType = object2PropValue.GetType().GetInterface("IComparable");
                    }

                    // If both objects have null values or were indexed properties, don't continue
                    if (propertyType != null)
                    {
                        // If one property value is null and the other is not null, 
                        // they aren't equal; this is done here as a native CompareTo
                        // won't work with a null value as the target
                        if (object1PropValue == null || object2PropValue == null)
                        {
                            propertyInfoList.Add(object1Prop);
                        }
                        else
                        {
                            // Use the native CompareTo method
                            MethodInfo nativeCompare = propertyType.GetMethod("CompareTo");

                            // Sanity Check:
                            // If we don't have a native CompareTo OR both values are null, we can't compare;
                            // hence, we can't confirm the values differ... just go to the next property
                            if (nativeCompare != null)
                            {
                                // Return the native CompareTo result
                                bool equal = (0 == (int)(nativeCompare.Invoke(object1PropValue, new object[] { object2PropValue })));

                                if (!equal)
                                {
                                    propertyInfoList.Add(object1Prop);
                                }
                            }
                        }
                    }
                }
                return propertyInfoList.Count == 0;
            }

            /// <summary>
            /// Compares the public properties of any 2 objects and determines if the properties of each
            /// all contain the same value.
            /// <para> 
            /// In cases where object1 and object2 are of different Types (both being derived from Type T) 
            /// we will cast both objects down to the base Type T to ensure the property comparison is only 
            /// completed on COMMON properties.
            /// (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
            /// both objects will be cast to Foo for comparison)
            /// </para>
            /// </summary>
            /// <typeparam name="T">Any class with public properties.</typeparam>
            /// <param name="object1">Object to compare to object2.</param>
            /// <param name="object2">Object to compare to object1.</param>
            /// <returns>A boolean value indicating whether or not the properties of each object match.</returns>
            public static bool HasSamePropertyValues<T>(T object1, T object2)
                where T : class
            {
                return HasSamePropertyValues<T>(object1, object2, null);
            }

            /// <summary>
            /// Compares the public properties of any 2 objects and determines if the properties of each
            /// all contain the same value.
            /// <para> 
            /// In cases where object1 and object2 are of different Types (both being derived from Type T) 
            /// we will cast both objects down to the base Type T to ensure the property comparison is only 
            /// completed on COMMON properties.
            /// (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
            /// both objects will be cast to Foo for comparison)
            /// </para>
            /// </summary>
            /// <typeparam name="T">Any class with public properties.</typeparam>
            /// <param name="object1">Object to compare to object2.</param>
            /// <param name="object2">Object to compare to object1.</param>
            /// <param name="ignoredProperties">A list of <see cref="PropertyInfo"/> objects
            /// to ignore when completing the comparison.</param>
            /// <returns>A boolean value indicating whether or not the properties of each object match.</returns>
            public static bool HasSamePropertyValues<T>(T object1, T object2, List<PropertyInfo> ignoredProperties)
                where T : class
            {

                // If either object is null, we can't compare anything
                if (object1 == null || object2 == null)
                {
                    return false;
                }

                Type object1Type = object1.GetType();
                Type object2Type = object2.GetType();

                // In cases where object1 and object2 are of different Types (both being derived from Type T) 
                // we will cast both objects down to the base Type T to ensure the property comparison is only 
                // completed on COMMON properties.
                // (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
                // both objects will be cast to Foo for comparison)
                if (object1Type != object2Type)
                {
                    object1Type = typeof(T);
                    object2Type = typeof(T);
                }

                // Remove any properties to be ignored
                List<PropertyInfo> comparisonProps =
                    RemoveProperties(object1Type.GetProperties(), ignoredProperties);

                foreach (PropertyInfo object1Prop in comparisonProps)
                {
                    Type propertyType = null;
                    object object1PropValue = null;
                    object object2PropValue = null;

                    // Rule out an attempt to check against a property which requires
                    // an index, such as one accessed via this[]
                    if (object1Prop.GetIndexParameters().GetLength(0) == 0)
                    {
                        // Get the value of each property
                        object1PropValue = object1Prop.GetValue(object1, null);
                        object2PropValue = object2Type.GetProperty(object1Prop.Name).GetValue(object2, null);

                        // As we are comparing 2 objects of the same type we know
                        // that they both have the same properties, so grab the
                        // first non-null value
                        if (object1PropValue != null)
                            propertyType = object1PropValue.GetType().GetInterface("IComparable");

                        if (propertyType == null)
                            if (object2PropValue != null)
                                propertyType = object2PropValue.GetType().GetInterface("IComparable");
                    }

                    // If both objects have null values or were indexed properties, don't continue
                    if (propertyType != null)
                    {
                        // If one property value is null and the other is not null, 
                        // they aren't equal; this is done here as a native CompareTo
                        // won't work with a null value as the target
                        if (object1PropValue == null || object2PropValue == null)
                        {
                            return false;
                        }

                        // Use the native CompareTo method
                        MethodInfo nativeCompare = propertyType.GetMethod("CompareTo");

                        // Sanity Check:
                        // If we don't have a native CompareTo OR both values are null, we can't compare;
                        // hence, we can't confirm the values differ... just go to the next property
                        if (nativeCompare != null)
                        {
                            // Return the native CompareTo result
                            bool equal = (0 == (int)(nativeCompare.Invoke(object1PropValue, new object[] { object2PropValue })));

                            if (!equal)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }

            /// <summary>
            /// Removes any <see cref="PropertyInfo"/> object in the supplied List of 
            /// properties from the supplied Array of properties.
            /// </summary>
            /// <param name="allProperties">Array containing master list of 
            /// <see cref="PropertyInfo"/> objects.</param>
            /// <param name="propertiesToRemove">List of <see cref="PropertyInfo"/> objects to
            /// remove from the supplied array of properties.</param>
            /// <returns>A List of <see cref="PropertyInfo"/> objects.</returns>
            private static List<PropertyInfo> RemoveProperties(
                IEnumerable<PropertyInfo> allProperties, IEnumerable<PropertyInfo> propertiesToRemove)
            {
                List<PropertyInfo> innerPropertyList = new List<PropertyInfo>();

                // Add all properties to a list for easy manipulation
                foreach (PropertyInfo prop in allProperties)
                {
                    innerPropertyList.Add(prop);
                }

                // Sanity check
                if (propertiesToRemove != null)
                {
                    // Iterate through the properties to ignore and remove them from the list of 
                    // all properties, if they exist
                    foreach (PropertyInfo ignoredProp in propertiesToRemove)
                    {
                        if (innerPropertyList.Contains(ignoredProp))
                        {
                            innerPropertyList.Remove(ignoredProp);
                        }
                    }
                }

                return innerPropertyList;
            }
            private static List<FieldInfo> RemoveFields(
              IEnumerable<FieldInfo> allFields, IEnumerable<FieldInfo> fieldsToRemove)
            {
                List<FieldInfo> innerFieldList = new List<FieldInfo>();

                // Add all fields to a list for easy manipulation
                foreach (FieldInfo prop in allFields)
                {
                    innerFieldList.Add(prop);
                }

                // Sanity check
                if (fieldsToRemove != null)
                {
                    // Iterate through the fields to ignore and remove them from the list of 
                    // all fields, if they exist
                    foreach (FieldInfo ignoredProp in fieldsToRemove)
                    {
                        if (innerFieldList.Contains(ignoredProp))
                        {
                            innerFieldList.Remove(ignoredProp);
                        }
                    }
                }

                return innerFieldList;
            }
            public static bool GetDifferentFields<T>(T object1, T object2, out List<FieldInfo> FieldInfoList)
      where T : class
            {
                return GetDifferentFields<T>(object1, object2, null, out FieldInfoList);
            }
            public static bool GetDifferentFields<T>(T object1, T object2, List<FieldInfo> ignoredFields, out List<FieldInfo> FieldInfoList)
    where T : class
            {
                FieldInfoList = new List<FieldInfo>();
                Type fieldType = null;

                // If either object is null, we can't compare anything
                if (object1 == null || object2 == null)
                {
                    return false;
                }

                Type object1Type = object1.GetType();
                Type object2Type = object2.GetType();

                // In cases where object1 and object2 are of different Types (both being derived from Type T) 
                // we will cast both objects down to the base Type T to ensure the field comparison is only 
                // completed on COMMON fields.
                // (ex. Type T is Foo, object1 is GoodFoo and object2 is BadFoo -- both being inherited from Foo --
                // both objects will be cast to Foo for comparison)
                if (object1Type != object2Type)
                {
                    object1Type = typeof(T);
                    object2Type = typeof(T);
                }

                // Remove any fields to be ignored
                List<FieldInfo> comparisonProps =
                    RemoveFields(object1Type.GetFields(), ignoredFields);

                foreach (FieldInfo object1Prop in comparisonProps)
                {
                    object object1PropValue = null;
                    object object2PropValue = null;

                    // Rule out an attempt to check against a field which requires
                    // an index, such as one accessed via this[]
                    //if (object1Prop.GetIndexParameters().GetLength(0) == 0)
                    //{
                    // Get the value of each field
                    object1PropValue = object1Prop.GetValue(object1);
                    object2PropValue = object2Type.GetField(object1Prop.Name).GetValue(object2); //GetField(object1Prop.Name).GetValue(object2, null);

                    // As we are comparing 2 objects of the same type we know
                    // that they both have the same fields, so grab the
                    // first non-null value
                    if (object1PropValue != null)
                    {
                        fieldType = object1PropValue.GetType().GetInterface("IComparable");
                    }

                    if (fieldType == null)
                        if (object2PropValue != null)
                            fieldType = object2PropValue.GetType().GetInterface("IComparable");
                    // If both objects have null values or were indexed fields, don't continue
                    if (fieldType != null)
                    {
                        // If one field value is null and the other is not null, 
                        // they aren't equal; this is done here as a native CompareTo
                        // won't work with a null value as the target
                        if (object1PropValue == null || object2PropValue == null)
                        {
                            FieldInfoList.Add(object1Prop);
                        }
                        else
                        {
                            // Use the native CompareTo method
                            MethodInfo nativeCompare = fieldType.GetMethod("CompareTo");

                            // Sanity Check:
                            // If we don't have a native CompareTo OR both values are null, we can't compare;
                            // hence, we can't confirm the values differ... just go to the next field
                            if (nativeCompare != null)
                            {
                                // Return the native CompareTo result
                                bool equal = (0 == (int)(nativeCompare.Invoke(object1PropValue, new object[] { object2PropValue })));

                                if (!equal)
                                {
                                    FieldInfoList.Add(object1Prop);
                                }
                            }
                        }
                    }
                }
                return FieldInfoList.Count == 0;
            }
        }

        private void populateDatagridView(string fileType)
        {
            if (fileType == ".ini")
            {
                if (!ObjectComparer.GetDifferentFields<gfiIniFileData>(iniConfigSrc.inifileData, iniConfigDest.inifileData, out fieldInfoList))
                {
                    foreach (FieldInfo object1Prop in fieldInfoList)
                    {
                        object object1PropValue = null;
                        object object2PropValue = null;

                        // Get the value of each property
                        object1PropValue = object1Prop.GetValue(iniConfigSrc.inifileData);
                        object2PropValue = iniConfigDest.inifileData.GetType().GetField(object1Prop.Name).GetValue(iniConfigDest.inifileData);

                        this.dataGridView1.Rows.Add(object1Prop.Name.ToString(), object1PropValue.ToString(), object2PropValue.ToString());
                    }
                }
                // Check MUXCommon fields
                for (int i = 0; i < 3; i++)
                {
                    if (!ObjectComparer.GetDifferentFields<MuxCommon>(iniConfigSrc.inifileData.mux[i], iniConfigDest.inifileData.mux[i], out fieldInfoList))
                    {
                        foreach (FieldInfo object1Prop in fieldInfoList)
                        {
                            object object1PropValue = null;
                            object object2PropValue = null;

                            // Get the value of each property
                            object1PropValue = object1Prop.GetValue(iniConfigSrc.inifileData.mux[i]);
                            object2PropValue = iniConfigDest.inifileData.mux[i].GetType().GetField(object1Prop.Name).GetValue(iniConfigDest.inifileData.mux[i]);

                            this.dataGridView1.Rows.Add("MUX"+(i+1)+"."+object1Prop.Name.ToString(), object1PropValue.ToString(), object2PropValue.ToString());
                        }
                    }
                }
                // Now get the OptionFlags
                for (int i = 0; i < 32; i++)
                {
                    if (iniConfigSrc.inifileData.optionFlags[i] != iniConfigDest.inifileData.optionFlags[i])
                    {
                        this.dataGridView1.Rows.Add("MUX1.Optionflag" + (i + 1), iniConfigSrc.inifileData.optionFlags[i], iniConfigDest.inifileData.optionFlags[i]);
                    }
                }
                // Now get the User Defined Actions
                for (int i = 0; i < 10; i++)
                {
                    if (iniConfigSrc.inifileData.UDA_Lines[i] != iniConfigDest.inifileData.UDA_Lines[i])
                    {
                        this.dataGridView1.Rows.Add("User Defined Actions." + (i + 1), iniConfigSrc.inifileData.UDA_Lines[i], iniConfigDest.inifileData.UDA_Lines[i]);
                    }
                }
                // Check Vlt Common fields
                for (int i = 0; i < 5; i++)
                {
                    if (!ObjectComparer.GetDifferentFields<VltCommon>(iniConfigSrc.inifileData.vlt[i], iniConfigDest.inifileData.vlt[i], out fieldInfoList))
                    {
                        foreach (FieldInfo object1Prop in fieldInfoList)
                        {
                            object object1PropValue = null;
                            object object2PropValue = null;

                            // Get the value of each property
                            object1PropValue = object1Prop.GetValue(iniConfigSrc.inifileData.vlt[i]);
                            object2PropValue = iniConfigDest.inifileData.vlt[i].GetType().GetField(object1Prop.Name).GetValue(iniConfigDest.inifileData.vlt[i]);

                            this.dataGridView1.Rows.Add("VLT" + (i+1) + "." + object1Prop.Name.ToString(), object1PropValue.ToString(), object2PropValue.ToString());
                        }
                    }
                }

            }
            else if (fileType == ".cfg")
            {
                if (!ObjectComparer.GetDifferentFields<NetconfigData>(netConfigSrc.inifileData, netConfigDest.inifileData, out fieldInfoList))
                {
                    foreach (FieldInfo object1Prop in fieldInfoList)
                    {
                        object object1PropValue = null;
                        object object2PropValue = null;

                        // Get the value of each property
                        object1PropValue = object1Prop.GetValue(netConfigSrc.inifileData);
                        object2PropValue = netConfigDest.inifileData.GetType().GetField(object1Prop.Name).GetValue(netConfigDest.inifileData);

                        this.dataGridView1.Rows.Add(object1Prop.Name.ToString(), object1PropValue.ToString(), object2PropValue.ToString());
                    }
                }
            }
        }
    }
}
