﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
using System.Data.Odbc;
using Microsoft.Win32;
using System.Data.SQLite;
using Microsoft.VisualBasic;
using FastColoredTextBoxNS;
using Microsoft.SqlServer.Management.Smo;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MySql.Data.MySqlClient;
using DGVPrinterHelper;
namespace ConfigurationWinForm
{
    public partial class odbctestForm : Form
    {
        #region definitions
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
        private const int TCM_SETMINTABWIDTH = 0x1300 + 49;

        private const int treeTables = 0;
        private const int treeViews = 1;
        private const int treeProcedures = 2;
        private const int treeTriggers = 3;
        private const int treeFunctions = 4;
        private const int treeColumns = 5;
        private const int treeFolder = 6;
        private const int treeDatabase = 7;

        private const int treeColumnsPrimaryKey = 8;
        private const int maxBinaryDisplayString = 8000;

        public const int SYBASE_DB = 1;
        public const int MSS_DB = 2;
        public const int SQLITE_DB = 3;
        public const int MYSQL_DB = 4;

        public bool bAnchorResultsToQuery = false;

        public class DBEngines
        {
            public string eng;
            public string host;
            public string desc;
        };
        public int connectionID = -1;
        float defaultFontSize = 8.5f;
        //float currentFont = 8.5f;
        WaitDlgParameters waitParmData = new WaitDlgParameters();
        DataGridView currentDatagrid = null;
        public SplitForm splitForm;
        public int MaxRecords = 3000;
        public int displayLineNumbers = 0;
        public bool enginesLoaded = false;
        public List<DBEngines> engines = new List<DBEngines>();

        public int databaseType=1;    //0=Sybase, 1=MSS, 2=SQLite, 3=MySql
        //public string sysconEng;
        //public string sysconDsn;
        //public string sysconHost;
        public string mssDriver;
        public int collectionType = 0;
        public int currentTabIndex = 0;
        public int currentQueryIndex = 0;
        public long resultsLineNo = 0;
       
        string dbName_save;
        string sysconEng_save;
        string sysconDsn_save;
        string sysconHost_save;
        string sysconUser_save;
        string sysconPassword_save;
        string mssDriver_save;
        //long StoredProcedureId = 0;
        public int connectionTimeout_save;
        private bool busy = false;
        public static bool WaitDlgFlag = false;
        private WaitDlg WaitCompleteDlg = null;
        public List<Form> FloatingQueries = new List<Form>();
        public enum WindowTypes { QueryWindow, LogWindow, ResultsWindow, UnknownWindow };
        public WindowTypes currentFocus; // = UnknownWindow;  //0=Query window, 1=error window, 2=results window, 3=Unknown
        public bool bNoFontChange = false;
        public List<string> tableNames = new List<string>();
        public bool bEnableToolsWindow = true;
        public bool bEnableStatusWindow = true;
        // Treeview Functionality

        // Table Menus
        ContextMenuStrip mnuTreeTable = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeTableSelectRows = new ToolStripMenuItem("Select Top 1000 Rows");
        ToolStripMenuItem mnuTreeTableEditRows = new ToolStripMenuItem("Edit Top 200 Rows");
        ToolStripMenuItem mnuTreeTableSchema = new ToolStripMenuItem("View Table Schema");
        ToolStripMenuItem mnuTreeViewIndexes = new ToolStripMenuItem("View Indexes");
        ToolStripMenuItem mnuTreeViewForeignKeys = new ToolStripMenuItem("View Foreign Keys");
        ToolStripMenuItem mnuTreeTablePaste = new ToolStripMenuItem("Paste Table name in Query");

        // Views Menus
        ContextMenuStrip mnuTreeView = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeViewSelectRows = new ToolStripMenuItem("Select Top 1000 Rows");
        ToolStripMenuItem mnuTreeViewSchema = new ToolStripMenuItem("View Schema");
        ToolStripMenuItem mnuTreeViewViewSQL = new ToolStripMenuItem("View SQL");
        ToolStripMenuItem mnuTreeViewViewIndexes = new ToolStripMenuItem("View Indexes");
        ToolStripMenuItem mnuTreeViewPaste = new ToolStripMenuItem("Paste Table name in Query");

        //ContextMenuStrip mnuTreeView = new ContextMenuStrip();

        // Stored Procedure Menu
        ContextMenuStrip mnuTreeStoredProcedures = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeOpenProcedure = new ToolStripMenuItem("Open Stored Procedure");

        // Trigger Menu
        ContextMenuStrip mnuTreeTriggers = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeOpenTrigger = new ToolStripMenuItem("Open Trigger");

        // Trigger Menu
        ContextMenuStrip mnuTreeFunctions = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeOpenFunctions = new ToolStripMenuItem("Open Function");

        // Connection Menu
        ContextMenuStrip mnuTreeConnection = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeDisconnect = new ToolStripMenuItem("Disconnect");
        ToolStripMenuItem mnuTreeViewTables = new ToolStripMenuItem("View Database Tables");
        ToolStripMenuItem mnuTreeViewViews = new ToolStripMenuItem("View Database Views");
        ToolStripMenuItem mnuTreeViewCollection = new ToolStripMenuItem("View Database Collection");

        // Database Node Menu
        ContextMenuStrip mnuTreeDatabases = new ContextMenuStrip();
        ToolStripMenuItem mnuTreeDB_LoadConfig = new ToolStripMenuItem("Open Configuration");
        ToolStripMenuItem mnuTreeDB_SaveConfig = new ToolStripMenuItem("Save Configuration");
        ToolStripMenuItem mnuTreeDB_DisconnectAll = new ToolStripMenuItem("Disconnect All");
        bool bEnableMySqltoolStripMenuItem;
        string VersionNumber;
        #endregion
        

        #region Constructors
        public odbctestForm()
        {
            InitializeComponent();
        }

        public odbctestForm(SplitForm sf)
        {
            splitForm = sf;
            //MessageBox.Show("InitializeComponent");     // PJB
            InitializeComponent();

            this.resultsTabControl.HandleCreated += resultsTabControl_HandleCreated;

            //MessageBox.Show("Adding Fonts");        // PJB
            foreach (FontFamily font in System.Drawing.FontFamily.Families)
            {
                fontCombo.Items.Add(font.Name);
                if (font.Name.CompareTo("Microsoft Sans Serif") == 0)
                    fontCombo.Text = "Microsoft Sans Serif";
            }

            SetFont(0);

            // save global settings
            databaseType = splitForm.globalSettings.systemType;
            userTextBox.Text = "";
            pwdTextBox.Text = "";

            MSSInstanceCombo.Text = splitForm.globalSettings.sysconHost;
            enginesCombo.Text = splitForm.globalSettings.sysconEng;
            dbNameCombo.Text = splitForm.globalSettings.dbName;
            MaxRecords = (int)Util.ParseValue(maxRecordsText.Text);
            maxRecordsText.Text = MaxRecords.ToString();
            portTextBox.Text = "2638";

            if (databaseType == SYBASE_DB)   // For Sybase open new connection
            {
                //MessageBox.Show("enginesCombo.Show()");         // PJB

                enginesCombo.Show();
                engLabel.Show();
                comboDBType.SelectedIndex = 0;  // set dropdown to Sybase
            }
            else
                comboDBType.SelectedIndex = 1;  // set dropdown to MSS

            if (databaseType == MSS_DB)
            {
                if (enginesCombo.Items.Count > 0)
                    enginesCombo.SelectedIndex = 0;
            }

            if (splitForm.globalSettings.connectionTimeout < 1 || splitForm.globalSettings.connectionTimeout > 15)
                splitForm.globalSettings.connectionTimeout = 15;
            numericUpDown1.Value = splitForm.globalSettings.connectionTimeout;

            //MessageBox.Show("1-Populating Drivers");    // PJB
            PopulateMssDrivers();

            //MessageBox.Show("2-SetButtonStatus()");     // PJB
            SetButtonStatus();

            if (splitForm.globalSettings.dotNetVersion.CompareTo("4.6") >= 0)
            {
                comboDBType.Items.Add("SQLite");
                //comboDBType.Items.Add("MySql");
            }
            MSSInstanceCombo.Text = "Localhost";
            dbNameCombo.Text = splitForm.globalSettings.dbName;

            //MessageBox.Show("3-Add Query Page");        // PJB
            AddQueryPage();
            
            // Disable results Text Control
            resultsText.Parent.Visible = false;
            queryTabControl.Parent.Dock = DockStyle.Fill;

            // Enable results Text Control
            resultsText.Parent.Visible = true;
            resultsText.Parent.Dock = DockStyle.Right;
            queryTabControl.Parent.Dock = DockStyle.Left;
            ClearDatabaseTreeView();

            mnuTreeTableSelectRows.Click += new EventHandler(treeviewSelectRows_Click);
            mnuTreeTableEditRows.Click += new EventHandler(treeviewEditRows_Click);
            mnuTreeTableSchema.Click += new EventHandler(treeviewSchema_Click);
            mnuTreeTablePaste.Click += new EventHandler(treeviewPaste_Click);
            mnuTreeViewIndexes.Click += new EventHandler(treeviewIndexes_Click);
            mnuTreeViewForeignKeys.Click += new EventHandler(viewForeignKeysForTableToolStripMenuItem_Click);
            mnuTreeTable.Items.AddRange(new ToolStripItem[] { mnuTreeTableSelectRows, mnuTreeTableEditRows, mnuTreeTableSchema, mnuTreeViewIndexes, mnuTreeViewForeignKeys, mnuTreeTablePaste });

            mnuTreeViewSelectRows.Click += new EventHandler(treeviewSelectRows_Click);
            mnuTreeViewSchema.Click += new EventHandler(treeviewSchema_Click);
            mnuTreeViewPaste.Click += new EventHandler(treeviewPaste_Click);
            mnuTreeViewViewSQL.Click += new EventHandler(treeviewSQL_Click);
            mnuTreeViewViewIndexes.Click += new EventHandler(treeviewIndexes_Click);
            mnuTreeView.Items.AddRange(new ToolStripItem[] { mnuTreeViewSelectRows, mnuTreeViewSchema, mnuTreeViewViewSQL, mnuTreeViewViewIndexes, mnuTreeViewPaste });

            mnuTreeOpenProcedure.Click += new EventHandler(treeviewOpenStoredProcedure_Click);
            mnuTreeStoredProcedures.Items.AddRange(new ToolStripItem[] { mnuTreeOpenProcedure });

            mnuTreeOpenTrigger.Click += new EventHandler(treeviewOpenTriggerProcedure_Click);
            mnuTreeTriggers.Items.AddRange(new ToolStripItem[] { mnuTreeOpenTrigger });

            mnuTreeOpenFunctions.Click += new EventHandler (treeviewOpenFunction_Click);
            mnuTreeFunctions.Items.AddRange(new ToolStripItem[] { mnuTreeOpenFunctions });

            mnuTreeDB_LoadConfig.Click += new EventHandler(treeviewDB_LoadConfig);
            mnuTreeDB_SaveConfig.Click += new EventHandler(treeviewDB_SaveCOnfig);
            mnuTreeDB_DisconnectAll.Click += new EventHandler(treeviewDB_DisconnectAll);
            mnuTreeDatabases.Items.AddRange(new ToolStripItem[] { mnuTreeDB_LoadConfig, mnuTreeDB_SaveConfig, mnuTreeDB_DisconnectAll });

            mnuTreeDisconnect.Click += new EventHandler(treeviewDisconnect_Click);
            mnuTreeViewTables.Click += new EventHandler(treeviewTables_Click);
            mnuTreeViewViews.Click += new EventHandler(treeviewView_Click);
            mnuTreeViewCollection.Click += new EventHandler(treeviewCollection_Click);

            mnuTreeConnection.Items.AddRange(new ToolStripItem[] { mnuTreeDisconnect, mnuTreeViewTables, mnuTreeViewViews, mnuTreeViewCollection });

            databaseTableView.ImageList = new ImageList();
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.table);              // Tables
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.tableview);          // Views
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.procedure);          // Procedures
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.tablefcn);           // Triggers
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.procedure);          // Functions
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.column);             // Columns
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.folder);             // Folder
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.database_connect);   // Database
            databaseTableView.ImageList.Images.Add((Image)Properties.Resources.key_primary);        // ColumnsPrimaryKey


            // MessageBox.Show("4-Get version");   // PJB
            VersionNumber = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Text = "Genfare Simple Query Tool - Version: " + VersionNumber;

            bEnableMySqltoolStripMenuItem = true;

            //MessageBox.Show("5-Check Version");     // PJB
            if (splitForm.globalSettings.dotNetVersion.CompareTo("4.6") < 0)
                bEnableMySqltoolStripMenuItem = false;
            EnableMySql(bEnableMySqltoolStripMenuItem);            
        }
        #endregion

        #region ContextMenuStrip Class
        public partial class MyContextMenuStrip : ContextMenuStrip
        {
            public DataGridView datagrid;
            public string desc;
            public Form frm;
            public MyContextMenuStrip(Form gridForm, DataGridView dg, string title)
            {
                frm = gridForm;
                datagrid = dg;
                desc = title;
            }
        }
        #endregion

        #region Treeview Functions
        private void treeviewSQL_Click(object sender, EventArgs e)
        {
            TextBox spText = new TextBox();
            string spName = databaseTableView.SelectedNode.Text.Trim();
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            // This will add the query to the query tab control
            PowerfulSample frm = new PowerfulSample(this, false);
            OpenProcedure("V",spName, spText, cnn);
            spName = "View:" + spName;
            frm.SetText(spText.Text);
            AddProcToQueryTab(spName, spText.Text);
        }

        private void treeviewOpenStoredProcedure_Click(object sender, EventArgs e)
        {
            TextBox spText = new TextBox();
            string spName = databaseTableView.SelectedNode.Text.Trim();
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            // This will add the query to the query tab control
            PowerfulSample frm = new PowerfulSample(this, false);
            OpenProcedure("P",spName, spText, cnn);
            spName = "Proc:" + spName;
            frm.SetText(spText.Text);
            AddProcToQueryTab(spName, spText.Text);
        }

        private void treeviewOpenFunction_Click(object sender, EventArgs e)
        {
            string spName = databaseTableView.SelectedNode.Text.Trim();
            TextBox spText = new TextBox();
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            PowerfulSample frm = new PowerfulSample(this, false);
            OpenProcedure("F", spName, spText, cnn);
            spName = "Function:" + spName;
            frm.SetText(spText.Text);
            AddProcToQueryTab(spName, spText.Text);
        }

        private void treeviewOpenTriggerProcedure_Click(object sender, EventArgs e)
        {
            string spName = databaseTableView.SelectedNode.Text.Trim();
            TextBox spText = new TextBox();
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            PowerfulSample frm = new PowerfulSample(this, false);
            OpenProcedure("T", spName, spText, cnn);
            spName = "Trigger:" + spName;
            frm.SetText(spText.Text);
            AddProcToQueryTab(spName, spText.Text);
        }

        private void treeviewSelectRows_Click(object sender, EventArgs e)
        {
            TreeNode saveNode = databaseTableView.SelectedNode;
            Cursor = Cursors.WaitCursor;
            //BeginWaitDialogMessage(this.Location, "Running SQL", "Please wait while SQL is being processed.");
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            cnn.tablename = databaseTableView.SelectedNode.Text.Trim();
            cnn.resultsHeader = null;
            switch (cnn.databaseType)
            {
                case SYBASE_DB:
                case MSS_DB:
                    SelectData("Select top 1000 * from " + cnn.tablename + ";", null, cnn);
                    break;
                case SQLITE_DB:
                case MYSQL_DB:
                    SelectData("Select * from " + cnn.tablename + " LIMIT 1000;", null, cnn);
                    break;
                default:
                    break;
            }
            //EndWaitDialogMessage();
            Cursor = Cursors.Default;
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }

        private void treeviewEditRows_Click(object sender, EventArgs e)
        {
            TreeNode saveNode = databaseTableView.SelectedNode;
            string tablename = databaseTableView.SelectedNode.Text.Trim();
            tableEditRows(tablename);
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }

        private void treeviewSchema_Click(object sender, EventArgs e)
        {
            string tablename = databaseTableView.SelectedNode.Text.Trim();
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            cnn.tablename = databaseTableView.SelectedNode.Text.Trim();
            switch (cnn.databaseType)
            {
                case SQLITE_DB:
                    GetSchemaSQLite(cnn);
                    break;
                case MYSQL_DB:
                    GetSchemaMySql(cnn);
                    break;
                default:
                    GetSchema(cnn,"schema");
                    break;
            }
        }

        private void treeviewPaste_Click(object sender, EventArgs e)
        {
            string tablename = databaseTableView.SelectedNode.Text.Trim();
            if (queryTabControl.TabCount > 0)
                ((PowerfulSample)(queryTabControl.SelectedTab.Controls[0])).InsertText(tablename);
        }

        private void ClearDatabaseTreeView()
        {
            // Set default Nodes in Treeview
            databaseTableView.Nodes.Clear();
            databaseTableView.Nodes.Add(BuildNode(System.Environment.MachineName, null,treeDatabase));
            databaseTableView.Nodes[0].Nodes.Add(BuildNode("Databases", mnuTreeDatabases,treeFolder));
            connectionID = -1;
            SetButtonStatus();
        }

        private TreeNode BuildNode(string name, ContextMenuStrip mnu, int imageIndex)
        {
            TreeNode mnuNode = new TreeNode(name);
            mnuNode.Name = name;
            if (mnu != null)
                mnuNode.ContextMenuStrip = mnu;
            if (imageIndex >= 0)
            {
                mnuNode.ImageIndex = imageIndex;
                mnuNode.StateImageIndex = imageIndex;
                mnuNode.SelectedImageIndex = imageIndex;
            }
            return mnuNode;
        }

        private void treeviewDisconnect_Click(object sender, EventArgs e)
        {
            databaseTableView.SelectedNode.Remove();
            if (databaseTableView.Nodes[0].Nodes[0].Nodes.Count > 0)
            {
                connectionID--;
                if (connectionID<0)
                    connectionID=0;
                databaseTableView.SelectedNode = databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID];
            }
            else
                connectionID = -1;
            SetButtonStatus();
        }
        private void treeviewDB_Disconnect(object sender, EventArgs e)
        {
            ClearDatabaseTreeView();
        }

        private void treeviewDB_LoadConfig(object sender, EventArgs e)
        {
            loadDatabaseConfiguration_Click(sender, e);
        }

        private void treeviewDB_SaveCOnfig(object sender, EventArgs e)
        {
            saveDatabaseConfiguration_Click(sender, e);
        }

        private void treeviewDB_DisconnectAll(object sender, EventArgs e)
        {
            ClearDatabaseTreeView();
        }

        #endregion

        #region Results Tab Control Functionality
        private void resultsTabControl_HandleCreated(object sender, EventArgs e)
        {
            SendMessage(this.resultsTabControl.Handle, TCM_SETMINTABWIDTH, IntPtr.Zero, (IntPtr)16);
        }
        #endregion

        #region odbctestForm Class Methods
        public string GetDatabaseName()
        {
            return "[" + MSSInstanceCombo.Text + "]:" + this.dbNameCombo.Text;
        }

        public void EnableStatusToolsWindow()
        {
            resultsText.Parent.Visible = bEnableStatusWindow;
            if (bEnableStatusWindow)
            {
                resultsText.Parent.Dock = DockStyle.Right;
                queryTabControl.Parent.Dock = DockStyle.Left;
                toolStripEnableStatusWIndow.Text = "Hide Status Window";
            }
            else
            {
                queryTabControl.Parent.Dock = DockStyle.Fill;
                toolStripEnableStatusWIndow.Text = "Show Status Window";
            }
            groupToolbox.Visible = bEnableToolsWindow;
            menuStrip1.Visible = bEnableToolsWindow;
            groupToolbox.Visible = bEnableToolsWindow;
            if (bEnableToolsWindow)
                toolStripEnableToolsWindow.Text = "Hide Tools";
            else
                toolStripEnableToolsWindow.Text = "Show Tools";
        }

        private void SetFont(int height)
        {
            //if (bNoFontChange)
            //    return;
            //float emSize = 4;
            //switch (currentFocus)
            //{
            //    case WindowTypes.QueryWindow:
            //        //if (queryTabControl.TabPages.Count > 0)
            //        //{
            //        //    if (queryTabControl.SelectedTab.Controls[0].Font != null)
            //        //    {
            //        //        emSize = queryTabControl.SelectedTab.Controls[0].Font.Size + (float)height;
            //        //        if (emSize < 4)
            //        //            emSize = 4;
            //        //        if (emSize > 20)
            //        //            emSize = 20;
            //        //    }
            //        //    queryTabControl.SelectedTab.Controls[0].Font = new Font(fontCombo.Text, emSize);
            //        //}
            //        break;
            //    case WindowTypes.LogWindow:
            //        if (resultsText.Font != null)
            //        {
            //            emSize = resultsText.Font.Size + (float)height;
            //            if (emSize < 4)
            //                emSize = 4;
            //            if (emSize > 20)
            //                emSize = 20;
            //        }
            //        resultsText.Font = new Font(fontCombo.Text, emSize);
            //        break;
            //    case WindowTypes.ResultsWindow:
            //        if (resultsTabControl.TabPages.Count > 0)
            //        {
            //            if (resultsTabControl.SelectedTab.Font != null)
            //            {
            //                emSize = resultsTabControl.SelectedTab.Font.Size + (float)height;
            //                if (emSize < 4)
            //                {
            //                    emSize = 4;
            //                    height = 0;
            //                }
            //                if (emSize > 20)
            //                {
            //                    emSize = 20;
            //                    height = 0;
            //                }
            //            }
            //            resultsTabControl.SelectedTab.Font = new Font(fontCombo.Text, emSize);
            //            ((DataGridView)resultsTabControl.SelectedTab.Controls[0]).AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //            using (Graphics g = this.CreateGraphics())
            //            {
            //                var points = resultsTabControl.SelectedTab.Font.SizeInPoints;
            //                var pixels = points * g.DpiX / 72;
            //                ((DataGridView)resultsTabControl.SelectedTab.Controls[0]).ColumnHeadersHeight = (int)pixels + 15;
            //            }
            //            ((DataGridView)resultsTabControl.SelectedTab.Controls[0]).Refresh();
            //        }
            //        break;
            //}
        }

        //private void PrintPreviewText()
        //{
        //    bool bOK = false;
        //    printFont = new Font("Courier New", 8);
        //    PrintDialog printDlg = new PrintDialog();
        //    PrintDocument pd = new PrintDocument();
        //    pd.DocumentName = filename;
        //    printDlg.Document = pd;
        //    printDlg.AllowPrintToFile = false;
        //    printDlg.AllowSomePages = false;
        //    printDlg.AllowSelection = false;
        //    printDlg.AllowCurrentPage = false;
        //    printDlg.ShowHelp = false;
        //    printDlg.ShowNetwork = true;

        //    pd.BeginPrint += new PrintEventHandler(pd_BeginPrint);
        //    pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
        //    bOK = (printDlg.ShowDialog() == DialogResult.OK);
        //    if (bOK)
        //    {
        //        //reader = new StringReader(text);
        //        PrintPreviewDialog YPreview = new PrintPreviewDialog();
        //        YPreview.WindowState = FormWindowState.Maximized;
        //        YPreview.Document = pd;
        //        YPreview.ShowDialog();
        //    }
        //}

        private void odbcConnect(TagConnection cnn)
        {
            Cursor = Cursors.WaitCursor;
            switch (cnn.databaseType)
            {
                case SYBASE_DB:         // Sybase
                    splitForm.odbcConnect(cnn);
                    if (splitForm.globalSettings.connected)
                        splitForm.sybaseAuthenticate();
                    Cursor = Cursors.Default;
                    break;
                case MSS_DB:         // MSS
                    if (MSSInstanceCombo.Text.Trim().Length > 0)
                    {
                        splitForm.globalSettings.sysconDsn = MSSInstanceCombo.Text;
                        splitForm.globalSettings.dbName = dbNameCombo.Text;
                        splitForm.odbcConnect(cnn);
                        Cursor = Cursors.Default;
                    }
                    break;
                case SQLITE_DB:         // SQLite
                    splitForm.odbcConnect(cnn);
                    Cursor = Cursors.Default;
                    break;
                case MYSQL_DB:         // MySQL
                    splitForm.odbcConnect(cnn);
                    Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }

            if (splitForm.globalSettings.connected == false)
            {
                logText("Connection: " + splitForm.globalSettings.connectionString);
                logText("Connection: " + splitForm.globalSettings.connectionError);
            }
            else
            {
                switch (cnn.databaseType)
                {
                    case SYBASE_DB:
                        logText("Sybase Database connected: ENG=" + splitForm.globalSettings.sysconEng + ",  HOST=" + splitForm.globalSettings.sysconHost);
                        break;
                    case MSS_DB:
                        logText("MSS Database connected: DSN=" + splitForm.globalSettings.sysconDsn);
                        break;
                    case SQLITE_DB:
                        logText("SQLite Database connected: DSN=" + splitForm.globalSettings.sysconDsn + ",  HOST=" + splitForm.globalSettings.sysconHost);
                        break;
                    case MYSQL_DB:
                        logText("MySql Database connected: DSN=" + splitForm.globalSettings.sysconDsn + ",  HOST=" + splitForm.globalSettings.sysconHost);
                        break;
                    default:
                        logText("Unsupported connection:" + splitForm.globalSettings.connectionString);
                        break;
                }
            }
        }

        void odbcDisConnect(TagConnection cnn)
        {
            switch (cnn.databaseType)
            {
                case SYBASE_DB:         // Sybase
                case MSS_DB:            // MSS
                    splitForm.odbcDisConnect(null);
                    break;
                case SQLITE_DB:         // SQlite
                    if (splitForm.cnnSqlite != null)
                    {
                        splitForm.cnnSqlite.Close();
                        splitForm.cnnSqlite = null;
                    }
                    break;
                case MYSQL_DB:
                    if (splitForm.cnnMySql != null)
                    {
                        splitForm.cnnMySql.Close();
                        splitForm.cnnMySql = null;
                    }
                    break;
            }
            logText("Database disconnected.");
        }

        private bool BuildTableArray(string objectType, TagConnection cnn)
        {
            bool connected = false;
            int tableType = 0;
            bool showSystemTables = true;
            try
            {
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {
                    Cursor = Cursors.WaitCursor;
                    OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                            if (objectType.ToLower().Trim() != "view")
                            {
                                if (showSystemTables)
                                    DbCommand.CommandText = "SELECT TABLE_NAME, REMARKS FROM SYS.SYSTABLE where table_type='BASE' order by TABLE_NAME";
                                else
                                    DbCommand.CommandText = "SELECT TABLE_NAME, REMARKS FROM SYS.SYSTABLE where primary_root<>0 and creator=1 order by TABLE_NAME";
                                tableType = treeTables;
                            }
                            else
                            {
                                DbCommand.CommandText = "SELECT TABLE_NAME, REMARKS FROM SYS.SYSTABLE where table_type='VIEW' order by TABLE_NAME";
                                tableType = treeViews;
                            }
                            break;
                        case MSS_DB:
                            if (objectType.ToLower().Trim() != "view")
                            {
                                DbCommand.CommandText = "use " + splitForm.globalSettings.dbName + ";SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' order by TABLE_NAME;";
                                tableType = treeTables;
                            }
                            else
                            {
                                DbCommand.CommandText = "use " + splitForm.globalSettings.dbName + ";SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='VIEW' order by TABLE_NAME;";
                                tableType = treeViews;
                            }
                            break;
                        default:
                            connected = false;
                            break;
                    }

                    if (connected)
                    {
                        // Save Connection String
                        OdbcDataReader DbReader = DbCommand.ExecuteReader();
                        string tip = "";
                        string temp = "";
                        int tableItem = 0;
                        int tablecnt = 0;
                        while (DbReader.Read())
                        {
                            temp = DbReader.GetString(0);
                            if (cnn.databaseType == SYBASE_DB)
                            {
                                try
                                {
                                    tip = (DbReader.IsDBNull(1) ? "" : DbReader.GetString(1));
                                }
                                catch
                                {
                                    tip = "";
                                }
                            }

                            if (tableType == treeViews)
                            {
                                databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeView, tableType));
                            }
                            else
                            {
                                databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeTable, tableType));
                            }
                            databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes[tableItem++].Nodes.Add(BuildNode("+", null,-1));

                            cnn.tableCache.Add(new TableCache(temp, tableType));
                            MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, temp.ToLower());
                            switch (cnn.databaseType)
                            {
                                case SYBASE_DB:
                                case MSS_DB:
                                    item.ToolTipTitle = temp;
                                    item.ToolTipText = tip;
                                    break;
                                case SQLITE_DB:
                                    break;
                            }
                            cnn.autocompleteItems.Add(item);
                            tablecnt++;
                        }
                        databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Text = (tableType == treeViews ? "Views" : "Tables") + " [" + tablecnt.ToString() + "]";
                        DbCommand.Dispose();
                        DbReader.Close();
                        if (cnn.databaseType == MSS_DB)
                        {
                            GetMSSTips(cnn);
                        }
                    }
                }
                else
                {
                    logText("Error: No ODBC connection found.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        void GetMSSTips(TagConnection cnn)
        {
            OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
            DbCommand.CommandText =
                "SELECT " +
                "tbl.name AS TableName, " +
                "CAST(p.value AS sql_variant) AS ExtendedPropertyValue " +
                "FROM " +
                "sys.tables AS tbl " +
                "INNER JOIN sys.extended_properties AS p ON p.major_id=tbl.object_id AND p.minor_id=0 AND p.class=1";

            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            string tblName = "";
            string tblProperty = "";
            while (DbReader.Read())
            {
                tblName = DbReader.GetString(0);
                tblProperty = DbReader.GetString(1);
                if (tblProperty.Length > 0)
                {
                    AutocompleteItem abc = cnn.autocompleteItems.Find(listItem => listItem.Text == tblName);
                    if (abc != null)
                        abc.ToolTipTitle = tblProperty;
                }
                else
                {
                    tblProperty = "N/A";
                }
            }
            DbCommand.Dispose();
            DbReader.Close();
        }



        private bool BuildTableArraySQlite(string objectType, TagConnection cnn)
        {
            int tablecnt = 0;
            bool connected = false;
            int tableType;
            int tableItem = 0;
            try
            {
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {

                    if (objectType.ToLower().Trim() != "view")
                        tableType = treeTables;
                    else
                        tableType = treeViews;

                    Cursor = Cursors.WaitCursor;
                    string sql = "SELECT name FROM sqlite_master WHERE type='" + objectType + "' order by name";
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, splitForm.cnnSqlite))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string temp = reader.GetString(0);

                                if (tableType == treeViews)
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeView, tableType));
                                else
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeTable, tableType));
                                databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes[tableItem++].Nodes.Add(BuildNode("+", null, -1));
                                cnn.tableCache.Add(new TableCache(temp, tableType));


                                //databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeTable,tableType));
                                //databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes[tableItem++].Nodes.Add(BuildNode("+", null, -1));

                                cnn.tableCache.Add(new TableCache(temp, tableType));
                                MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, temp.ToLower());
                                //item.ToolTipTitle = temp;
                                //item.ToolTipText = tip;
                                item.ImageIndex = 0;
                                cnn.autocompleteItems.Add(item);
                                tablecnt++;
                                // End Autocomplete
                            }
                        }
                    }
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Text = (objectType == "view" ? "Views" : "Tables") + " [" + tablecnt.ToString() + "]";
                }
                else
                {
                    logText("Error: No ODBC connection found.");
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }

            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        private bool BuildTableArrayMySql(string objectType, TagConnection cnn)
        {
            int tablecnt = 0;
            bool connected = false;
            int tableType;
            int tableItem = 0;
            string sql;
            try
            {
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {

                    if (objectType.ToLower().Trim() != "view")
                    {
                        tableType = treeTables;
                        sql = "select table_name from information_schema.tables  where  table_schema='" + cnn.databaseName + "' order by table_name";
                        //sql = "SELECT table_name FROM information_schema.tables WHERE table_schema ="+cnn.databaseName + " order by  table_name";
                    }
                    else
                    {
                        tableType = treeViews;
                        sql = "SELECT table_name FROM information_schema.tables WHERE table_type LIKE 'VIEW' order by  table_name";
                    }

                    Cursor = Cursors.WaitCursor;

                    using (MySqlCommand cmd = new MySqlCommand(sql, splitForm.cnnMySql))
                    {
                        using (MySqlDataReader mysqlDbReader = cmd.ExecuteReader())
                        {
                            while (mysqlDbReader.Read())
                            {
                                string temp = mysqlDbReader.GetString(0);

                                if (tableType == treeViews)
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeView, tableType));
                                else
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes.Add(BuildNode(temp, mnuTreeTable, tableType));
                                databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Nodes[tableItem++].Nodes.Add(BuildNode("+", null, -1));
                                cnn.tableCache.Add(new TableCache(temp, tableType));
                                MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, temp.ToLower());
                                item.ImageIndex = 0;
                                cnn.autocompleteItems.Add(item);
                                tablecnt++;
                            }
                            databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Text = (tableType == treeViews ? "Views" : "Tables") + " [" + tablecnt.ToString() + "]";
                        }
                    }
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tableType].Text = (objectType == "view" ? "Views" : "Tables") + " [" + tablecnt.ToString() + "]";
                }
                else
                {
                    logText("Error: No MySql ODBC connection found.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("MySql ODBC Error: " + ex.Message);
            }

            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        void GetSchemaCollection(int type, TagConnection cnn)
        {
            TreeNode saveNode = databaseTableView.SelectedNode;
            string msgText = databaseTableView.SelectedNode.Text.Trim();
            odbcConnect(cnn);
            if (cnn != null)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    DataTable dt = new DataTable();
                    string[] views3 = { null, null, null };
                    string[] indexes4 = { null, null, cnn.databaseName, null };
                    string[] tables4 = { null, null, null };
                    string[] indexes5 = { null, null, msgText, null };

                    switch (type)
                    {
                        case 0: // Collections
                            msgText += ":Collections";
                            switch (cnn.databaseType)
                            {
                                case SQLITE_DB:
                                    dt = splitForm.cnnSqlite.GetSchema();
                                    break;
                                case MYSQL_DB:
                                    dt = splitForm.cnnMySql.GetSchema();
                                    break;
                                default:
                                    dt = splitForm.cnnODBC.GetSchema();
                                    break;
                            }
                            break;
                        case 1: // Tables
                            msgText += ":Tables";
                            switch (cnn.databaseType)
                            {
                                case SQLITE_DB:
                                    dt = splitForm.cnnSqlite.GetSchema("Tables", tables4);
                                    break;
                                case MYSQL_DB:
                                    dt = splitForm.cnnMySql.GetSchema("Tables", tables4);
                                    break;
                                default:
                                    dt = splitForm.cnnODBC.GetSchema("Tables", tables4);
                                    break;
                            }
                            break;
                        case 2: // Columns
                            msgText += ":Columns";
                            switch (cnn.databaseType)
                            {
                                case SQLITE_DB:
                                    dt = splitForm.cnnSqlite.GetSchema("Columns", indexes4);
                                    break;
                                case MYSQL_DB:
                                     dt = splitForm.cnnMySql.GetSchema("Columns", indexes5);
                                    break;
                                default:
                                    dt = splitForm.cnnODBC.GetSchema("Columns", indexes4);
                                    break;
                            }
                            break;
                        case 3: // Views
                            msgText += ":Views";
                            switch (cnn.databaseType)
                            {
                                case SQLITE_DB:
                                    dt = splitForm.cnnSqlite.GetSchema("views", views3);
                                    break;
                                case MYSQL_DB:
                                    dt = splitForm.cnnMySql.GetSchema("Views", views3);
                                    break;
                                default:
                                    dt = splitForm.cnnODBC.GetSchema("Views", views3);
                                    break;
                            }
                            break;
                        case 4: // Indexes
                            msgText += ":Indexes";
                            switch (cnn.databaseType)
                            {
                                case SQLITE_DB:
                                    dt = splitForm.cnnSqlite.GetSchema("indexes");
                                    break;
                                case MYSQL_DB:
                                    dt = splitForm.cnnMySql.GetSchema("indexes");
                                    break;
                                case SYBASE_DB:
                                    break;
                                default:
                                    dt = splitForm.cnnODBC.GetSchema("Indexes", indexes4);
                                    break;
                            }
                            break;
                    }
                    cnn.resultsHeader = msgText;
                    AddResults(false, false, cnn, dt, new ResultsTabPage(false, null, msgText));
                    odbcDisConnect(cnn);
                    SetButtonStatus();
                    Cursor = Cursors.Default;
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }
        #endregion

        private void databaseTableView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.GetNodeCount(false) == 1)
            {
                if (e.Node.Nodes[0].Text.Trim() == "+")
                {
                    e.Node.Nodes[0].Remove();
                    AddTreeviewColumnNames(e);
                }
            }
        }

        // Treeview Column Names CODE        
        public void AddTreeviewColumnNames(TreeViewCancelEventArgs e)
        {
            int fCount;
            int nConstraints = 0;
            List<object> constraints = new List<object>();
            object[] values = null;
            object[] constraintValues = null;
            OdbcCommand odbcDbCommand;
            OdbcDataReader odbcDbReader;
            OdbcCommand odbcDbCommandConstraint;
            OdbcDataReader odbcDbReaderConstraint;
            string strdecimal;
            TreeNode node = null;
            string tablename = e.Node.Text.Trim();
            TagConnection cnn = (TagConnection)e.Node.Parent.Parent.Tag;
            odbcConnect(cnn);
            try
            {
                switch (cnn.databaseType)
                {
                    case SQLITE_DB:
                        string query = "SELECT * FROM " + tablename + " LIMIT 1;";
                        SQLiteCommand sqliteDbCommand = new SQLiteCommand(query, splitForm.cnnSqlite);
                        SQLiteDataReader sqliteDbReader = sqliteDbCommand.ExecuteReader();
                        using (var schemaTable = sqliteDbReader.GetSchemaTable())
                        {
                            int i = 0;
                            foreach (DataRow row in schemaTable.Rows)
                            {
                                Type dbtype = row.Field<Type>("DataType");
                                string ColumnName = row.Field<string>("ColumnName");
                                bool bAllowNull = row.Field<bool>("AllowDBNull");
                                bool bAutoIncrement = row.Field<bool>("IsAutoIncrement");
                                bool bIsKey = row.Field<bool>("IsKey");
                                string t1 = ColumnName +                                // Column Name
                                        "  (" +
                                        (bIsKey ? "PK," : "") +                         // Is Key
                                        (bAutoIncrement ? "Auto," : "") +               // Is auto Increment
                                        sqliteDbReader.GetDataTypeName(i).ToLower() +   // Data Type     
                                        (bAllowNull ? "" : ",not null") +      // Is nullable
                                        ")";
                                node = BuildNode(t1, null, (bIsKey ? treeColumnsPrimaryKey : treeColumns));
                                e.Node.Nodes.Add(node);
                                i++;
                            }
                        }
                        break;
                    case SYBASE_DB:
                        values = null;
                        odbcDbCommand = splitForm.cnnODBC.CreateCommand();
                        odbcDbCommand.CommandText=  "SELECT column_name, column_id, pkey, nulls, width, scale, [default], remarks,"+
                                                    " (SELECT domain_name from sysdomain where domain_id=syscolumn.domain_id) "+
                                                    " FROM syscolumn WHERE table_id=(SELECT table_id FROM systable WHERE table_name='" + tablename + "');";
                        using (odbcDbReader = odbcDbCommand.ExecuteReader())
                        {
                            // Allocate object to hold data
                            fCount = odbcDbReader.FieldCount;
                            if (fCount > 0)
                                values = new object[fCount];
                            while (odbcDbReader.Read())
                            {
                                odbcDbReader.GetValues(values);
                                string ColumnName = values[0].ToString();
                                string IsKey = (values[2].ToString().ToUpper()=="Y"?"Key, ":"");
                                string AllowNull = (values[3].ToString().ToUpper()=="Y, "?"Null, ":"Not Null, ");
                                string ColumnSize = (values[4].ToString().Length > 0 ? "Len=" + values[4].ToString()+", " : "");
                                string dflt = (values[6].ToString().Length > 0 ? "Default=" + values[6].ToString()+", " : "");
                                string domain_id = values[8].ToString();

                                string t1 = ColumnName + " (" + IsKey + AllowNull + ColumnSize + dflt + domain_id + ")";
                                node = BuildNode(t1, null, (values[2].ToString().ToUpper() == "Y" ? treeColumnsPrimaryKey : treeColumns));
                                e.Node.Nodes.Add(node);
                            }
                        }
                        break;
                    case MSS_DB:
                        nConstraints = 0;
                        constraintValues = null;
                        odbcDbCommandConstraint = splitForm.cnnODBC.CreateCommand();
                        odbcDbCommandConstraint.CommandText = "select CONSTRAINT_NAME, ORDINAL_POSITION from information_schema.key_column_usage where table_name='" + tablename + "';";
                        using (odbcDbReaderConstraint = odbcDbCommandConstraint.ExecuteReader())
                        {
                            // Allocate object to hold data
                            nConstraints = odbcDbReaderConstraint.FieldCount;
                            if (nConstraints > 0)
                            {
                                while (odbcDbReaderConstraint.Read())
                                {
                                    constraintValues = new object[nConstraints];
                                    odbcDbReaderConstraint.GetValues(constraintValues);
                                    constraints.Add(constraintValues);
                                }
                            }
                        }

                        //foreach (object[] obj in constraints)
                        //{
                        //    string abc = obj[0].ToString();
                        //}

                        values = null;
                        odbcDbCommand = splitForm.cnnODBC.CreateCommand();
                        odbcDbCommand.CommandText = "SELECT COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX, NUMERIC_SCALE"+
                                                    " from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='"+ tablename + "';";
                        using (odbcDbReader = odbcDbCommand.ExecuteReader())
                        {
                            // Allocate object to hold data
                            fCount = odbcDbReader.FieldCount;
                            if (fCount > 0)
                                values = new object[fCount];
                            int i=1;
                            while (odbcDbReader.Read())
                            {
                                odbcDbReader.GetValues(values);
                                string ColumnName = values[0].ToString();
                                string IsKey = "";
                                if (nConstraints > 0)
                                {
                                    foreach (object[] obj in constraints)
                                    {
                                        if (Convert.ToInt32(obj[1].ToString()) == i)
                                            IsKey += obj[0].ToString() + ", ";
                                    }
                                }
                                if (IsKey.Length > 0)
                                    IsKey = ", " + IsKey;

                                string AllowNull = (values[3].ToString().ToUpper() == "YES"?"Null, " : "Not Null, ");
                                string domain_id = values[4].ToString();
                                string precision="";
                                if (values[5].ToString().Length > 0)
                                    precision = "(" + values[5].ToString() + "," + values[6].ToString() + ")";
                                string t1 = ColumnName + " (" + IsKey + AllowNull + domain_id + precision + ")";
                                node = BuildNode(t1, null, (IsKey.Length> 0?treeColumnsPrimaryKey:treeColumns));
                                e.Node.Nodes.Add(node);
                                i++;
                            }
                        }
                        constraints.Clear();
                        break;
                    //case MSS_DB:
                        ////select COLUMN_NAME, ORDINAL_POSITION, 
                        ////COLUMN_DEFAULT,IS_NULLABLE, DATA_TYPE, NUMERIC_PRECISION, 
                        ////NUMERIC_PRECISION_RADIX, NUMERIC_SCALE
                        ////from 
                        ////INFORMATION_SCHEMA.COLUMNS 
                        ////WHERE 
                        ////TABLE_NAME='ml'

                        ////select CONSTRAINT_NAME, ORDINAL_POSITION from information_schema.key_column_usage where table_name='ml'


                        //odbcDbCommand = splitForm.cnnODBC.CreateCommand();
                        //odbcDbCommand.CommandText = "SELECT top 1 * FROM " + tablename;
                        //odbcDbReader = odbcDbCommand.ExecuteReader();

                        //using (var schemaTable = odbcDbReader.GetSchemaTable())
                        //{
                        //    int i = 0;
                        //    foreach (DataRow row in schemaTable.Rows)
                        //    {
                        //        string ColumnName = row.Field<string>("ColumnName");
                        //        int ColumnSize = row.Field<int>("ColumnSize");
                        //        bool bAllowNull = row.Field<bool>("AllowDBNull");
                        //        bool bAutoIncrement = row.Field<bool>("IsAutoIncrement");
                        //        bool bIsKey = row.Field<bool>("IsKey");
                        //        Type dbtype = row.Field<Type>("DataType");
                        //        if (dbtype == typeof(System.Decimal))
                        //        {
                        //            short NumericPrecision = row.Field<short>("NumericPrecision");
                        //            short NumericScale = row.Field<short>("NumericScale");
                        //            strdecimal = "(" + NumericPrecision.ToString() + "," + NumericScale.ToString() + "),";
                        //        }
                        //        else
                        //        {
                        //            if ((dbtype == typeof(System.Char)) || (dbtype == typeof(System.String)))
                        //            {
                        //                short NumericPrecision = row.Field<short>("NumericPrecision");
                        //                strdecimal = "(" + NumericPrecision.ToString() + ")";
                        //            }
                        //            else
                        //                strdecimal = "";
                        //        }
                        //        string t1 = ColumnName +                                        // Column Name
                        //                    "  (" +
                        //                    (bIsKey ? "PK," : "") +                             // Is Key
                        //                    (bAutoIncrement ? "Auto," : "") +                   // Is auto Increment
                        //                    odbcDbReader.GetDataTypeName(i).ToLower() +         // Data Type     
                        //                    strdecimal +                                 
                        //                    (bAllowNull ? "" : ",not null") +                   // Is nullable
                        //                    ")";
                        //        node = BuildNode(t1, null, (bIsKey?treeColumnsPrimaryKey:treeColumns));
                        //        e.Node.Nodes.Add(node);
                        //        i++;
                        //    }
                        //}
                        //break;
                    case MYSQL_DB:
                        MySqlCommand mysqlDbCommand = new MySqlCommand(@"SELECT * FROM " + tablename + " Limit 1;", splitForm.cnnMySql);
                        MySqlDataReader mysqlDbReader = mysqlDbCommand.ExecuteReader();
                        using (var schemaTable = mysqlDbReader.GetSchemaTable())
                        {
                            int i=0;
                            foreach (DataRow row in schemaTable.Rows)
                            {
                                string ColumnName = row.Field<string>("ColumnName");
                                bool bAllowNull = row.Field<bool>("AllowDBNull");
                                bool bAutoIncrement = row.Field<bool>("IsAutoIncrement");
                                bool bIsKey = row.Field<bool>("IsKey");
                                Type dbtype = row.Field<Type>("DataType");
                                if (dbtype == typeof(System.Decimal))
                                {
                                    int NumericPrecision = row.Field<int>("NumericPrecision");
                                    int NumericScale = row.Field<int>("NumericScale");
                                    strdecimal = "(" + NumericPrecision.ToString() + "," + NumericScale.ToString() + "),";
                                }
                                else
                                {
                                    if ((dbtype == typeof(System.Char)) || (dbtype == typeof(System.String)))
                                    {
                                        int ColumnSize = row.Field<int>("ColumnSize");
                                        strdecimal = "(" + ColumnSize.ToString() + ")";
                                    }
                                    else
                                        strdecimal = "";
                                }
                                string t1 = ColumnName +                                // Column Name
                                            "  ("+
                                            (bIsKey ? "PK," : "") +                     // Is Key
                                            (bAutoIncrement ? "Auto," : "") +           // Is auto Increment
                                            mysqlDbReader.GetDataTypeName(i).ToLower() +                              // Data Type     
                                            strdecimal + 
                                            (bAllowNull ? "" : ",not null") +      // Is nullable
                                            ")";
                                node = BuildNode(t1, null, (bIsKey ? treeColumnsPrimaryKey : treeColumns));
                                e.Node.Nodes.Add(node);
                                i++;
                            }
                        }
                        break;
                }
            }

            catch 
            {

            }
            odbcDisConnect(cnn);
        }


        // AUTOCOMPLETE CODE        
        public void AddSchemaNames(string tablename)
        {
            string query;
            if (connectionID >= 0)
            {
                TagConnection cnn = (TagConnection)(databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Tag);
                // SQlite schema names
                switch (cnn.databaseType)
                {
                    case SQLITE_DB:
                        odbcConnect(cnn);
                        query = "SELECT * FROM " + tablename + " LIMIT 1;";
                        SQLiteCommand sqliteDbCommand = new SQLiteCommand(query, splitForm.cnnSqlite);
                        SQLiteDataReader sqliteDbReader = sqliteDbCommand.ExecuteReader();
                        for (int i = 0; i < sqliteDbReader.FieldCount; i++)
                        {
                            MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, tablename + "." + sqliteDbReader.GetName(i));
                            item.ToolTipTitle = "Column:" + sqliteDbReader.GetName(i) + "(" + sqliteDbReader.GetDataTypeName(i) + ")";
                            item.ImageIndex = 4;
                            cnn.autocompleteItems.Add(item);
                        }
                        odbcDisConnect(cnn);
                        break;
                    case MYSQL_DB:
                        odbcConnect(cnn);
                        MySqlCommand mysqlodbcDbCommand = splitForm.cnnMySql.CreateCommand();
                        mysqlodbcDbCommand.CommandText = "SELECT * FROM " + tablename+" LIMIT 1;";
                        MySqlDataReader mysqlodbcDbReader = mysqlodbcDbCommand.ExecuteReader();
                        for (int i = 0; i < mysqlodbcDbReader.FieldCount; i++)
                        {
                            MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, tablename + "." + mysqlodbcDbReader.GetName(i));
                            item.ToolTipTitle = "Column:" + mysqlodbcDbReader.GetName(i) + "(" + mysqlodbcDbReader.GetDataTypeName(i) + ")";
                            item.ImageIndex = 4;
                            cnn.autocompleteItems.Add(item);
                        }
                        odbcDisConnect(cnn);
                        break;
                    case SYBASE_DB:
                    case MSS_DB:
                        odbcConnect(cnn);
                        try
                        {
                            OdbcCommand odbcDbCommand = splitForm.cnnODBC.CreateCommand();
                            odbcDbCommand.CommandText = "SELECT top 1 * FROM " + tablename;
                            OdbcDataReader odbcDbReader = odbcDbCommand.ExecuteReader();
                            for (int i = 0; i < odbcDbReader.FieldCount; i++)
                            {
                                MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, tablename + "." + odbcDbReader.GetName(i));
                                item.ToolTipTitle = "Column:" + odbcDbReader.GetName(i) + "(" + odbcDbReader.GetDataTypeName(i) + ")";
                                item.ImageIndex = 4;
                                cnn.autocompleteItems.Add(item);
                            }
                        }
                        catch
                        {
                        }
                        odbcDisConnect(cnn);
                        break;
                }
            }
        }

        void GetSchemaMySql(TagConnection cnn)
        {
            TreeNode saveNode = databaseTableView.SelectedNode;
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    string sql = "SELECT * FROM " + cnn.tablename + " LIMIT 1;";
                    DataTable dt = new DataTable();
                    MySqlCommand DbCommand = new MySqlCommand(sql, splitForm.cnnMySql); ;
                    MySqlDataReader DbReader = DbCommand.ExecuteReader();

                    dt = DbReader.GetSchemaTable();
                    DbCommand.Dispose();
                    DbReader.Close();
                    odbcDisConnect(cnn);

                    Cursor = Cursors.Default;
                    string host = "";
                    cnn.resultsHeader = host + "[" + cnn.databaseName + "]:" + cnn.tablename + "[Schema]"; ;
 
                    cnn.resultsHeader = host + "[" + cnn.databaseName + "]:" + cnn.tablename + "[Schema]"; ;
                    AddResults(false, false, cnn, dt, new ResultsTabPage(false, null, cnn.tablename + " Schema"));
                    SetButtonStatus();
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }


        void GetSchema(TagConnection cnn,string schemaType)
        {
            TreeNode saveNode = databaseTableView.SelectedNode;
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    DataTable dt = new DataTable();
                    OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
                    OdbcDataReader DbReader;
                    if (cnn.databaseType == SYBASE_DB)
                    {
                        if (schemaType == "schema")
                            DbCommand.CommandText = "SELECT * FROM syscolumn WHERE table_id=(SELECT table_id FROM systable WHERE table_name='" + cnn.tablename + "');";
                        else
                        {
                            if (schemaType == "index")
                                DbCommand.CommandText = "SELECT * FROM sysindex WHERE table_id=(SELECT table_id FROM systable WHERE table_name='" + cnn.tablename + "');";
                            else
                            {
                                if (schemaType == "foreignkeys")
                                {
                                    // Simple
                                    //DbCommand.CommandText = "select f.* from sys.systable t " +
                                    //                       "join sys.SYSFOREIGNKEY f on t.table_id=f.primary_table_id " +
                                    //                       "where t.table_name='" + cnn.tablename + "';";
                                    // Advanced
                                    DbCommand.CommandText = "select t.table_name as Primary_Table, s.table_name as Table_Name, f.role, f.remarks, f.nulls " +
                                                            "from sys.systable t " +
                                                            "inner join sys.SYSFOREIGNKEY f on t.table_id=f.primary_table_id " +
                                                            "JOIN sys.systable s on f.foreign_table_id=s.table_id " +
                                                            "where t.table_name='" + cnn.tablename + "';";
                                }
                                else
                                {
                                    DbCommand.Dispose();
                                    return;
                                }
                            }
                        }
                        DbReader = DbCommand.ExecuteReader();
                        dt.Load(DbReader);
                    }
                    else
                    {

                        if (schemaType == "schema")
                        {
                            // Get the column definitions
                            DbCommand.CommandText = "exec sp_columns '" + cnn.tablename + "';";
                            DbReader = DbCommand.ExecuteReader();
                            dt.Load(DbReader);
                            dt.Columns[11].ReadOnly = false;

                            // Get the Constraints
                            DataTable dtConstraints = new DataTable();
                            OdbcCommand DbConsttraintCommand = splitForm.cnnODBC.CreateCommand();
                            OdbcDataReader DbConstraintReader;
                            DbConsttraintCommand.CommandText = "SELECT ORDINAL_POSITION, COLUMN_NAME, CONSTRAINT_NAME from INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME='" + cnn.tablename + "';";
                            DbConstraintReader = DbConsttraintCommand.ExecuteReader();
                            dtConstraints.Load(DbConstraintReader);
                            DbConsttraintCommand.Dispose();
                            DbConstraintReader.Close();
                            for (int i = 0; i < dtConstraints.Rows.Count; i++)
                            {
                                int ordinal = Convert.ToInt32(dtConstraints.Rows[i][0]);
                                if (ordinal > 0)
                                    ordinal--;
                                dt.Rows[ordinal][11] = dt.Rows[ordinal][11].ToString() + (dt.Rows[ordinal][11].ToString().Length > 0 ? "," : "") + dtConstraints.Rows[i][2].ToString();
                            }
                        }
                        else
                        {
                            if (schemaType == "index")
                            {
                                DbCommand.CommandText = "exec sp_helpindex '" + cnn.tablename + "';";
                                DbReader = DbCommand.ExecuteReader();
                                dt.Load(DbReader);
                            }
                            else
                            {
                                if (schemaType == "foreignkeys")
                                {
                                    DbCommand.CommandText = 
                                            "SELECT o2.name AS Referenced_Table_Name, " +
                                            "c2.name AS Referenced_Column_As_FK, " +
                                            "o1.name AS Referencing_Table_Name, " +
                                            "c1.name AS Referencing_Column_Name, " +
                                            "s.name AS Constraint_name " +
                                            "FROM  sysforeignkeys fk  " +
                                            "INNER JOIN sysobjects o1 ON fk.fkeyid = o1.id " +
                                            "INNER JOIN sysobjects o2 ON fk.rkeyid = o2.id and o2.name='" + cnn.tablename + "' " +
                                            "INNER JOIN syscolumns c1 ON c1.id = o1.id AND c1.colid = fk.fkey " +
                                            "INNER JOIN syscolumns c2 ON c2.id = o2.id AND c2.colid = fk.rkey " +
                                            "INNER JOIN sysobjects s ON fk.constid = s.id ";
                                    DbReader = DbCommand.ExecuteReader();
                                    dt.Load(DbReader);
                                }
                                else
                                {
                                    DbCommand.Dispose();
                                    return;
                                }
                            }
                        }

                    }
                    DbCommand.Dispose();
                    DbReader.Close();
                    odbcDisConnect(cnn);
                    Cursor = Cursors.Default;
                    string host = "";
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                            host = cnn.engine;
                            if (cnn.hostname.ToLower().Trim() != "localhost")
                                host += ":" + cnn.hostname;
                            break;
                        case MSS_DB:
                            host = cnn.hostname;
                            break;
                        case SQLITE_DB:
                            break;
                    }
                    cnn.resultsHeader = host + "[" + cnn.databaseName + "]:" + cnn.tablename + "[Schema]"; ;

                    AddResults(false, false, cnn, dt, new ResultsTabPage(false,null,cnn.tablename+" Schema"));
                    SetButtonStatus();
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }

        void GetSchemaSQLite(TagConnection cnn)
        {
            string query;
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    DataTable dt = new DataTable();

                    // Add first row
                    query = "SELECT * FROM " + cnn.tablename + " LIMIT 1;";
                    SQLiteCommand DbCommand = new SQLiteCommand(query, splitForm.cnnSqlite);
                    SQLiteDataReader DbReader = DbCommand.ExecuteReader();

                    dt = DbReader.GetSchemaTable();
                    DbCommand.Dispose();
                    DbReader.Close();
                    odbcDisConnect(cnn);
                    SetButtonStatus();
                    Cursor = Cursors.Default;
                    cnn.resultsHeader = "[" + cnn.databaseName + ":Schema]" + cnn.tablename;
                    AddResults(false, false, cnn, dt, new ResultsTabPage(false,null, cnn.databaseName+" Schema"));
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        void EditData(string query, TagConnection cnn)
        {
            logText("Query started.");
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    ResultsTabPage tabPage = new ResultsTabPage(true, null, query);
                    tabPage.dt = new DataTable();
                    switch (cnn.databaseType)
                    {
                        case SQLITE_DB:
                            tabPage.SQLiteadapter = new SQLiteDataAdapter(query, splitForm.cnnSqlite);
                            tabPage.SQLiteadapter.SelectCommand = new SQLiteCommand(query, splitForm.cnnSqlite);
                            tabPage.SQLitebuilder = new SQLiteCommandBuilder(tabPage.SQLiteadapter);
                            tabPage.SQLiteadapter.Fill(tabPage.dt);
                            tabPage.SQLitecnnODBC = splitForm.cnnSqlite;
                            break;
                        case MYSQL_DB:
                            tabPage.mysqladapter = new MySqlDataAdapter(query, splitForm.cnnMySql);
                            tabPage.mysqladapter.SelectCommand = new MySqlCommand(query, splitForm.cnnMySql);
                            tabPage.mysqlbuilder = new MySqlCommandBuilder(tabPage.mysqladapter);
                            tabPage.mysqladapter.Fill(tabPage.dt);
                            tabPage.mysqlcnnODBC = splitForm.cnnMySql;
                            break;
                        default:
                            tabPage.adapter = new OdbcDataAdapter(query, splitForm.cnnODBC);
                            tabPage.adapter.SelectCommand = new OdbcCommand(query, splitForm.cnnODBC);
                            tabPage.builder = new OdbcCommandBuilder(tabPage.adapter);
                            tabPage.adapter.Fill(tabPage.dt);
                            tabPage.cnnODBC = splitForm.cnnODBC;
                            break;
                    }

                    tabPage.databaseType = cnn.databaseType;
                    tabPage.Tag = null;
                    AddResults(false, true, cnn, tabPage.dt, tabPage);
                    SetButtonStatus();
                    Cursor = Cursors.Default;
                }
                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            else
            {
                logText("Error: No ODBC connection found.");
            }
            logText("Query completed.");
        }

        void SelectData(string query, TabPage tabPageItem, TagConnection cnn)
        {
            OdbcCommand DbCommand=null;
            OdbcDataReader DbReader=null;

            SQLiteCommand sqliteDbCommand=null;
            SQLiteDataReader sqliteDbReader=null;


            MySqlCommand mysqlDbCommand = null;
            MySqlDataReader mysqlDbReader = null;

            object[] values = null;
            object[] valuesCopy = null;
            int qryCount = 0;
            bool bret=false;
            int fCount=0;
            string strName="";
            int maxrowcount = MaxRecords;
            logText("Query started.");
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:     // Sybase
                        case MSS_DB:        // MSS
                            // Add first row
                            DbCommand = splitForm.cnnODBC.CreateCommand();
                            DbCommand.CommandText = query;
                            DbReader = DbCommand.ExecuteReader();
                            fCount = DbReader.FieldCount;
                            break;
                        case SQLITE_DB:     // SQLite
                            sqliteDbCommand = new SQLiteCommand(query, splitForm.cnnSqlite);
                            sqliteDbReader = sqliteDbCommand.ExecuteReader();
                            fCount = sqliteDbReader.FieldCount;
                            break;
                        case MYSQL_DB:  // MySql
                            mysqlDbCommand = new MySqlCommand(query,splitForm.cnnMySql);
                            mysqlDbReader= mysqlDbCommand.ExecuteReader();
                            fCount = mysqlDbReader.FieldCount;
                            break;
                    }
                    //logText(fCount.ToString() + " columns selected");

                    do
                    {
                        DataTable dt = new DataTable();
                        // Add column for line numbers
                        if (displayLineNumbers > 0)
                        {
                            DataColumn dc = new DataColumn("[Line]", typeof(Int32));
                            dt.Columns.Add(dc);
                        }
                        // Create the rest of the columns
                        for (int i = 0; i < fCount; i++)
                        {
                            switch (cnn.databaseType)
                            {
                                case SYBASE_DB:     // Sybase
                                case MSS_DB:        // MSS
                                    strName = DbReader.GetName(i);
                                    dt.Columns.Add(new DataColumn(strName, DbReader.GetFieldType(i)));
                                    fCount = DbReader.FieldCount;
                                    break;
                                case SQLITE_DB:     // SQLite
                                    // Add first row
                                    strName = sqliteDbReader.GetName(i);
                                    //sqliteDbReader.GetType();
                                    dt.Columns.Add(new DataColumn(strName, sqliteDbReader.GetFieldType(i)));
                                    fCount = sqliteDbReader.FieldCount;
                                    break;
                                case MYSQL_DB:  // MySql
                                    strName = mysqlDbReader.GetName(i);
                                    //mysqlDbReader.GetType();
                                    dt.Columns.Add(new DataColumn(strName, mysqlDbReader.GetFieldType(i)));
                                    fCount = mysqlDbReader.FieldCount;
                                    break;
                            }
                        }
                        logText(fCount.ToString() + " columns selected");

                        // Allocate object to hold data
                        if (fCount > 0)
                        {
                            values = new object[fCount];
                            // Add values with additional column
                            if (displayLineNumbers > 0)
                                valuesCopy = new object[fCount + 1];
                        }

                        // Initialize count to 0
                        int cnt = 0;
                        // Do not limit the number of rows if maxrowcount=0
                        if (MaxRecords == 0)
                            maxrowcount = 1;
                        while (true)
                        {
                            switch (cnn.databaseType)
                            {
                                case SYBASE_DB:     // Sybase
                                case MSS_DB:        // MSS
                                    if (bret = DbReader.Read())
                                        DbReader.GetValues(values);
                                    break;
                                case SQLITE_DB:     // SQLite
                                    if (bret = sqliteDbReader.Read())
                                        sqliteDbReader.GetValues(values);
                                    break;
                                case MYSQL_DB:  // MySql
                                    if (bret = mysqlDbReader.Read())
                                        mysqlDbReader.GetValues(values);
                                    break;
                            }
                            if (!bret || maxrowcount <= 0)
                                break;
                            for (int i = 0; i < fCount; i++)
                            {
                                // Copy value offset by one column
                                if (displayLineNumbers > 0)
                                    valuesCopy[i + displayLineNumbers] = values[i];  // bump row for counter
                            }

                            // add values to row
                            if (displayLineNumbers > 0)
                            {
                                valuesCopy[0] = (cnt + 1).ToString();
                                dt.Rows.Add(valuesCopy);
                            }
                            else
                                dt.Rows.Add(values);

                            if (MaxRecords > 0)
                                maxrowcount--;
                            cnt++;
                        }
                        if (MaxRecords > 0 && maxrowcount <= 0)
                            logText("Number of records selected exceeds maximum allowed of " + MaxRecords.ToString() + ".");
                        logText(cnt.ToString() + " records displayed.");

                        // TODO - How to handle multiple queries in the same window
                        // Perhaps multiple panels?

                        if ((tabPageItem != null) && (tabPageItem.Tag != null) && (qryCount == 0))
                        {
                            if (query.Length > 256)
                                ((ResultsTabPage)tabPageItem.Tag).ToolTipText = query.Substring(0, 256) + "....";
                            else
                                ((ResultsTabPage)tabPageItem.Tag).ToolTipText = query;
                            ReplaceResults(true, false, cnn, dt, (ResultsTabPage)tabPageItem.Tag);
                        }
                        else
                        {
                            ResultsTabPage resultsTab = new ResultsTabPage(false, tabPageItem, query);
                            AddResults(true, false, cnn, dt, resultsTab);
                            if (tabPageItem != null)
                                tabPageItem.Tag = resultsTab;
                        }
                        // Reset row count for next query
                        maxrowcount = MaxRecords;
                        qryCount++;
                        bret=false;
                        switch (cnn.databaseType)
                        {
                            case SYBASE_DB:     // Sybase
                            case MSS_DB:        // MSS
                                bret=DbReader.NextResult();
                                break;
                            case SQLITE_DB:     // SQLite
                                bret = sqliteDbReader.NextResult();
                                break;
                            case MYSQL_DB:  // MySql
                                bret = mysqlDbReader.NextResult();
                                break;
                        }
                    } while (bret);

                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:     // Sybase
                        case MSS_DB:        // MSS
                            DbCommand.Dispose();
                            DbReader.Close();
                            break;
                        case SQLITE_DB:     // SQLite
                            sqliteDbCommand.Dispose();
                            sqliteDbReader.Close();
                            break;
                        case MYSQL_DB:  // MySql
                            mysqlDbCommand.Dispose();
                            mysqlDbReader.Close();
                            break;
                    }

                    odbcDisConnect(cnn);
                    SetButtonStatus();
                    Cursor = Cursors.Default;
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            else
            {
                logText("Error: No ODBC connection found.");
            }
            logText("Query completed.");
        }

        private void saveConnection(TagConnection cnn)
        {
            // Save global settings
            dbName_save = splitForm.globalSettings.dbName;
            sysconEng_save = splitForm.globalSettings.sysconEng;
            sysconDsn_save = splitForm.globalSettings.sysconDsn;
            sysconHost_save = splitForm.globalSettings.sysconHost;
            sysconUser_save = splitForm.globalSettings.sysconUser;
            sysconPassword_save = splitForm.globalSettings.sysconPassword;
            connectionTimeout_save = splitForm.globalSettings.connectionTimeout;
            mssDriver_save = splitForm.globalSettings.mssDriver;

            splitForm.globalSettings.dbName = dbNameCombo.Text;
            splitForm.globalSettings.systemType = comboDBType.SelectedIndex + 1;
            splitForm.globalSettings.sysconEng = cnn.engine;
            splitForm.globalSettings.sysconDsn = cnn.databaseName;
            splitForm.globalSettings.connectionPort = cnn.port;
            splitForm.globalSettings.sysconHost = cnn.hostname;
            splitForm.globalSettings.connectionTimeout = (int)numericUpDown1.Value;
            splitForm.globalSettings.mssDriver = mssDriver;


            // Set default user/password
            if (userTextBox.Text.Length > 0)
                splitForm.globalSettings.sysconUser = userTextBox.Text;
            if (pwdTextBox.Text.Length > 0)
                splitForm.globalSettings.sysconPassword = pwdTextBox.Text;

            switch (cnn.databaseType)
            {
                case SYBASE_DB: // Sybase
                    splitForm.globalSettings.sysconUser = splitForm.GetGFIRegistryString(splitForm.globalSettings.HKLM_GFI_GLOBAL, "SYSCON USER", "dba");
                    splitForm.globalSettings.sysconPassword = splitForm.GetGFIRegistryString(splitForm.globalSettings.HKLM_GFI_GLOBAL, "SYSCON PWD", "gfi314159gfi");
                    break;
                case MSS_DB: // MSS
                    splitForm.globalSettings.sysconUser = splitForm.GetGFIRegistryString(splitForm.globalSettings.HKLM_GFI_GLOBAL, "SYSCON USER", "gfi");
                    splitForm.globalSettings.sysconPassword = splitForm.GetGFIRegistryString(splitForm.globalSettings.HKLM_GFI_GLOBAL, "SYSCON PWD", "gfi");
                    splitForm.globalSettings.sysconDsn = "localhost";
                    break;
                case SQLITE_DB: // SQLite
                    if (pwdTextBox.Text.Length > 0)
                        splitForm.globalSettings.sysconPassword = pwdTextBox.Text;
                    else
                        splitForm.globalSettings.sysconPassword = "";
                    break;
                case MYSQL_DB: // MySql
                    if (pwdTextBox.Text.Length > 0)
                        splitForm.globalSettings.sysconPassword = pwdTextBox.Text;
                    else
                        splitForm.globalSettings.sysconPassword = "";
                    break;
                default:
                    break;
            }

            if (userTextBox.Text.Length > 0)
                splitForm.globalSettings.sysconUser = userTextBox.Text;
            if (pwdTextBox.Text.Length > 0)
                splitForm.globalSettings.sysconPassword = pwdTextBox.Text;
        }

        public void executeFromPSMenu(PowerfulSample query)
        {
            int len = 0;
            if (connectionID >= 0)
            {
                string queryText = query.GetSelectedText();
                TagConnection cnn = (TagConnection)(databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Tag);
                if ((len = queryText.Length) == 0)              // No selected Text
                {
                    if ((len = query.GetTextLength()) > 0)
                        queryText = query.GetText();
                }
                if (len > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    //BeginWaitDialogMessage(this.Location, "Running SQL", "Please wait while SQL is being processed.");
                    cnn.tablename = queryTabControl.SelectedTab.Text;
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                        case MSS_DB:
                            SelectData(queryText, queryTabControl.SelectedTab, cnn);
                            break;
                        case SQLITE_DB:
                        case MYSQL_DB:
                            SelectData(queryText, queryTabControl.SelectedTab, cnn);
                            //SelectDataSqlite(queryText, queryTabControl.SelectedTab, cnn);
                            break;
                        default:
                            break;
                    }
                    Cursor = Cursors.Default;
                    //EndWaitDialogMessage();
                }
            }
        }

        private void tableEditRows(string tablename)
        {
            string queryText;
            bool saveCheckedState = addLineNumbers.Checked;
            TreeNode saveNode=databaseTableView.SelectedNode;
            addLineNumbers.Checked = false;
            TagConnection cnn = (TagConnection)databaseTableView.SelectedNode.Parent.Parent.Tag;
            switch(cnn.databaseType)
            {
                case SQLITE_DB:
                case MYSQL_DB:
                    queryText = "Select * from " + tablename.Trim() + " limit 200;";
                    break;
                default:
                //case SYBASE_DB:
                //case MSS_DB:
                    queryText = "Select top 200 * from " + tablename.Trim() + ";";
                    break;
            }

            //if (cnn.databaseType==3)
            //    queryText = "Select * from " + tablename.Trim() + " limit 200;";
            //else
            //    queryText = "Select top 200 * from " + tablename.Trim() + ";";
            Cursor = Cursors.WaitCursor;
            //BeginWaitDialogMessage(this.Location, "Running SQL", "Please wait while SQL is being processed.");
            cnn.tablename = tablename;
            EditData(queryText, cnn);
            Cursor = Cursors.Default;
            //EndWaitDialogMessage();
            addLineNumbers.Checked = saveCheckedState;
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }

        private void maxRecordsChanged(object sender, EventArgs e)
        {
            MaxRecords = (int)Util.ParseValue(maxRecordsText.Text);
            maxRecordsText.Text = MaxRecords.ToString();
        }

        private void addLineNumbers_CheckedChanged(object sender, EventArgs e)
        {
            displayLineNumbers = (addLineNumbers.Checked ? 1 : 0);
            if (addLineNumbers.Checked)
                addLineNumbers.BackColor = Color.Yellow;
            else
                addLineNumbers.BackColor = System.Drawing.SystemColors.Control;
        }

        private void odbctestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (resetQueries() < 0)
                e.Cancel = true;
            else
            {
                splitForm.LoadDefaultConnection();
                splitForm.globalSettings.connectionTimeout = 15;
                splitForm.WindowState = FormWindowState.Maximized;
            }
        }
        public void LoadMSSInstances()
        {
            string[] separatingChars = { "\n", " " };
            while (MSSInstanceCombo.Items.Count > 0)
                MSSInstanceCombo.Items.RemoveAt(0);
            MSSInstanceCombo.Text = "";

            DataTable dt = SmoApplication.EnumAvailableSqlServers(false);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Unable to locate Servers - Check permissions or firewall.");
            }
            else
            {
                MSSInstanceCombo.Items.Add("Localhost");
                // Load server names into combo box
                foreach (DataRow dr in dt.Rows)
                {
                    //only add if it doesn't exist
                    MSSInstanceCombo.Items.Add(dr["Name"].ToString());
                }
                MSSInstanceCombo.SelectedIndex = 0;
            }
        }

        public void LoadEngines()
        {
            string[] separatingChars = { "\n", " " };
            engines.Clear();
            enginesCombo.Items.Clear();
            DBEngines engItem;
            Process p;
            p = new Process();
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "dblocate.exe";
            p.StartInfo.Arguments = "-n";
            Cursor = Cursors.WaitCursor;
            p.Start();

            string output = p.StandardOutput.ReadToEnd();
            Cursor = Cursors.Default;
            string[] words = output.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < words.Count(); i++)
                words[i] = words[i].Trim();
            int engFound = -1;
            for (int i = 0, itemID = 0; i < words.Count(); i++)
            {
                string engine = words[i];
                if (engine.Length >= 4)
                    engine = words[i].Substring(0, 4).ToLower();
                else
                    continue;

                if (engine == "gfi_")
                {
                    engItem = new DBEngines();
                    engItem.eng = words[i++];
                    engItem.host = words[i];
                    engItem.desc = engItem.host + "::" + engItem.eng;
                    engines.Add(engItem);
                    enginesCombo.Items.Add(engItem.desc);
                    engFound = itemID;
                    itemID++;
                }
            }

            if (engFound < 0)
                engFound = 0;
            else
            {
                string txt = enginesCombo.Items[0].ToString();
                DBEngines eng = engines.Find(x => x.desc == txt);
                enginesCombo.SelectedIndex = 0;
                MSSInstanceCombo.Text = eng.host;
            }
            p.WaitForExit();
        }

        private void enginesCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int item = enginesCombo.SelectedIndex;
            string txt = enginesCombo.Items[enginesCombo.SelectedIndex].ToString();
            DBEngines eng = engines.Find(x => x.desc == txt);
            switch (databaseType)
            {
                case SYBASE_DB:
                    if (enginesLoaded)
                    {
                        MSSInstanceCombo.Text = eng.host;
                        logText("Selected ENG=" + eng.eng + "  HOST=" + eng.host);
                    }
                    break;
                case MSS_DB:
                    logText("Selected DSN=" + dbNameCombo.Text);
                    break;
            }
        }


        public void logText(string txt)
        {
            resultsLineNo++;
            resultsText.AppendText(resultsLineNo.ToString("D4"));
            resultsText.AppendText(": " + txt);
            resultsText.AppendText("\r\n");
            tb_Changed(resultsText);

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F7)
            {
                MessageBox.Show("Sorry - Help not currently available.");
                return true;    // indicate that you handled this keystroke
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void comboDBType_SelectedIndexChanged(object sender, EventArgs e)
        {
            mssDriverCombo.Enabled = false;
            ResetDatabase();
            browseDatabaseFile.Enabled = false;
            MSSInstanceCombo.Enabled = true;
            mssDriverCombo.Enabled = true;
            userTextBox.Enabled = true;
            pwdTextBox.Enabled = true;
            dbNameCombo.Enabled = true;
            refreshEng.Enabled = false;
            mssDriverCombo.Enabled = false;
            enginesCombo.Items.Clear();
            MSSInstanceCombo.Items.Clear();
            dbNameCombo.Items.Clear();

            switch (comboDBType.SelectedIndex)
            {
                case 0:     // Sybase
                    portTextBox.Visible = true;
                    portLabel.Visible = true;
                    portTextBox.Text = "2638";
                    databaseType = SYBASE_DB;
                    enginesCombo.Text = splitForm.globalSettings.sysconEng;
                    MSSInstanceCombo.Enabled = true;
                    enginesCombo.Enabled = true;
                    dbNameCombo.Text = "";
                    serverLabel.Text = "Host:";
                    refreshEng.Enabled = true;
                    refreshMSS.Enabled = false;
                    dbNameCombo.Items.Add("gfi");
                    dbNameCombo.Items.Add("syscon");
                    dbNameCombo.SelectedIndex = 0;
                    break;
                case 1:     // SQL Server
                    portTextBox.Visible = true;
                    portLabel.Visible = true;
                    portTextBox.Text = "1433";
                    databaseType = MSS_DB;
                    MSSInstanceCombo.Enabled = true;
                    enginesCombo.Enabled = false;
                    enginesLoaded = true;
                    if (mssDriverCombo.Items.Count >= 0)
                        mssDriverCombo.Enabled = true;
                    serverLabel.Text = "Server:";
                    refreshMSS.Enabled = true;
                    dbNameCombo.Text = "gfi";
                    break;
                case 2: // SQLite
                    portTextBox.Visible = false;
                    portLabel.Visible = false;
                    databaseType = SQLITE_DB; // Indicates that this is a SQLite or MySQL database
                    browseDatabaseFile.Enabled = true;
                    enginesCombo.Enabled = false;
                    MSSInstanceCombo.Enabled = false;
                    mssDriverCombo.Enabled = false;
                    userTextBox.Enabled = false;
                    pwdTextBox.Enabled = false;
                    dbNameCombo.Text = "";
                    break;
                case 3: // MySQL
                    enginesCombo.Enabled = false;
                    portTextBox.Visible = true;
                    portLabel.Visible = true;
                    portTextBox.Text = "3306";
                    break;
            }
            //MSSInstanceCombo.Text = "Localhost";
            SetButtonStatus();
        }
        private void clearDataDtn_Click(object sender, EventArgs e)
        {
            currentTabIndex = 0;
            // TODO:PJB Speed this up
            for (int i = 0; i < resultsTabControl.TabCount; i++)
            {
                TabPage qryPage = ((ResultsTabPage)resultsTabControl.TabPages[i]).queryTabPage;
                if (qryPage != null)
                    qryPage.Tag = null;
                resultsTabControl.TabPages[i].Dispose();
            }
            resultsTabControl.TabPages.Clear();
            //foreach (TabPage qryPage in queryTabControl.TabPages)
            //    qryPage.Tag = null;

            while (FloatingQueries.Count > 0)
            {
                FloatingQueries[0].Close();
                FloatingQueries.RemoveAt(0);
            }
            SetButtonStatus();
        }

        ///////////////////////////////////////////////////////////////////
        // This will get the SQL Server Driver Name
        ///////////////////////////////////////////////////////////////////
        public string PopulateMssDrivers()
        {
            string keyValue = "";
            mssDriverCombo.Items.Clear();
            //MessageBox.Show("Starting PopulateDrivers()");  // PJB

            RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers");
            if (regKey != null)
            {
                int i = 0;
                foreach (var value in regKey.GetValueNames())
                {
                    string data = Convert.ToString(regKey.GetValue(value)).ToLower();
                    if (data.CompareTo("installed") == 0)
                    {
                        keyValue = Convert.ToString(value);
                        if (keyValue.ToLower().Contains("sql server native client") ||
                            keyValue.ToLower().Contains("sql native client"))
                        {
                            mssDriverCombo.Items.Add(value);
                            i++;
                        }
                        if (keyValue.ToLower() == splitForm.globalSettings.mssDriver.ToLower())
                            mssDriverCombo.SelectedIndex = i - 1;
                    }
                }
                if ((mssDriverCombo.Items.Count >= 0) && (mssDriverCombo.SelectedIndex < 0) && (mssDriverCombo.Items.Count >= 0))
                    mssDriverCombo.SelectedIndex = 0;
                else
                    mssDriverCombo.Enabled = false;
            }
            else
            {
                keyValue = "No Driver Located";
                mssDriverCombo.Items.Add(keyValue);
                mssDriverCombo.SelectedIndex = 0;
                mssDriverCombo.Text = keyValue;
                mssDriverCombo.Enabled = false;
            }
            //MessageBox.Show("End PopulateDrivers()"); //PJB
            return keyValue;
        }

        private void mssDriverCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            mssDriver = mssDriverCombo.Text;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void exportMenuItem_Click(object sender, EventArgs e)
        {
            exportData(0, (DataGridView)resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0], resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Text.Trim());
        }

        private void saveQuery(PowerfulSample ps, string filename)
        {
            DialogResult result;
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = filename + ".sql";
            dialog.Filter = "SQL File|*.sql|All files (*.*)|*.*";
            result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            StreamWriter sw = new StreamWriter(dialog.FileName);
            sw.Write(ps.GetText());
            sw.Close();
        }

        private void exportData(int type, DataGridView grid, string fname)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = fname;
            switch (type)
            {
                case 0:     // datagridview as text
                    dialog.Filter = "Text File|*.txt;*.cfg|All files (*.*)|*.*";
                    break;
                case 1:     // datagridview as csv
                    dialog.Filter = "CSV File|*.csv;*.cfg|All files (*.*)|*.*";
                    break;
                case 2:     // Status Messages as text
                    dialog.Filter = "Text File|*.txt;*.cfg|All files (*.*)|*.*";
                    break;
            }

            var result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            if (type == 2)       // Export status window
            {
                StreamWriter sw = new StreamWriter(dialog.FileName);
                sw.Write(resultsText.Text);
                sw.Close();
            }
            else                // Export datagridview data
            {
                if (type == 0)
                {
                    // setup for export
                    grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
                    grid.SelectAll();
                    // hiding row headers to avoid extra \t in exported text
                    var rowHeaders = grid.RowHeadersVisible;
                    grid.RowHeadersVisible = false;

                    // ! creating text from grid values
                    string content = grid.GetClipboardContent().GetText();

                    // restoring grid state
                    grid.ClearSelection();
                    grid.RowHeadersVisible = rowHeaders;
                    System.IO.File.WriteAllText(dialog.FileName, content, Encoding.ASCII);
                }
                else
                    writeCSV(grid, dialog.FileName);
            }
        }

        public void writeCSV(DataGridView gridIn, string outputFile)
        {
            //test to see if the DataGridView has any rows
            if (gridIn.RowCount > 0)
            {
                string value = "";
                DataGridViewRow dr = new DataGridViewRow();
                StreamWriter swOut = new StreamWriter(outputFile);

                //write header rows to csv
                for (int i = 0; i <= gridIn.Columns.Count - 1; i++)
                {
                    if (i > 0)
                    {
                        swOut.Write(",");
                    }
                    swOut.Write(gridIn.Columns[i].HeaderText);
                }

                swOut.WriteLine();

                //write DataGridView rows to csv
                for (int j = 0; j <= gridIn.Rows.Count - 1; j++)
                {
                    if (j > 0)
                    {
                        swOut.WriteLine();
                    }

                    dr = gridIn.Rows[j];

                    for (int i = 0; i <= gridIn.Columns.Count - 1; i++)
                    {
                        if (i > 0)
                        {
                            swOut.Write(",");
                        }

                        if (dr.Cells[i].Value != null)
                        {
                            value = dr.Cells[i].Value.ToString();
                            //replace comma's with spaces
                            value = value.Replace(',', ' ');
                            //replace embedded newlines with spaces
                            value = value.Replace(Environment.NewLine, " ");
                        }
                        else
                            value = " ";
                        swOut.Write(value);
                    }
                }
                swOut.Close();
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            exportData(1, (DataGridView)resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0], resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Text.Trim());
        }

        public void RemoveResultsForQuery(TabPage queryTabPage)
        {
            for (int i = resultsTabControl.TabPages.Count - 1; i >= 0; i--)
            {
                if (((ResultsTabPage)resultsTabControl.TabPages[i]).queryTabPage == queryTabPage)
                {
                    if (((ResultsTabPage)resultsTabControl.TabPages[i]).CloseAndSave() == 0)
                    {
                        // Disconnect link from query page
                        TabPage qryPage = ((ResultsTabPage)resultsTabControl.TabPages[i]).queryTabPage;
                        if (qryPage != null)
                            qryPage.Tag = null;
                        resultsTabControl.TabPages[i].Dispose();
                        //resultsTabControl.Controls.RemoveAt(i);   // TODO:PJB:This is really slow!!!!!!!
                        if (i > 0)
                            i = resultsTabControl.TabPages.Count; // need top reset the index
                    }
                    else
                        break;
                }
            }
        }

        public void ReplaceResults(bool bQuery, bool bAllowEdit, TagConnection cnn, DataTable dt, ResultsTabPage tabPage)
        {
            string dbname = "";
            //RemoveResultsForQuery(tabPage.queryTabPage);

            tabPage.Controls.Clear();
            DataGridView tabGrid = new DataGridView();
            tabGrid.MouseClick += new System.Windows.Forms.MouseEventHandler(resultsGrid_Click);
            string[] words;
            string host = "";
            switch (cnn.databaseType)
            {
                case SYBASE_DB:
                    words = enginesCombo.Text.Split('@');
                    if (words.Count() > 0)
                        dbname = words[0].Trim();
                    else
                        dbname = enginesCombo.Text;
                    host = cnn.engine;
                    if (cnn.hostname.ToLower().Trim() != "localhost")
                        host += ":" + cnn.hostname;
                    break;
                case MSS_DB:
                    dbname = MSSInstanceCombo.Text;
                    host = cnn.hostname;
                    break;
                case SQLITE_DB:
                    dbname = dbNameCombo.Text;
                    break;
            }

            if (cnn.resultsHeader != null)
                tabPage.Name = cnn.resultsHeader;
            else
                tabPage.Name = host + "[" + cnn.databaseName + "]:" + cnn.tablename;

            tabPage.Name += "  ";
            tabPage.Padding = new System.Windows.Forms.Padding(3);
            tabPage.Size = new System.Drawing.Size(1320, 230);
            tabPage.TabIndex = 0;
            tabPage.Text = tabPage.Name;
            tabPage.UseVisualStyleBackColor = true;
            for (int i=0;i<tabPage.Controls.Count;i++)
                tabPage.Controls.RemoveAt(i);
            tabPage.Controls.Add(tabGrid);
            tabPage.Parent = resultsTabControl;
            tabGrid.DataSource = dt;

            if (bAllowEdit)
            {
                tabGrid.ReadOnly = false;
                tabGrid.EditMode = DataGridViewEditMode.EditOnKeystroke;
                tabPage.gridTag = 2;
            }
            else
            {
                tabGrid.ReadOnly = true;
                tabPage.gridTag = 1;
            }
            tabGrid.Dock = DockStyle.Fill;
            resultsTabControl.SelectedTab = tabPage;
            if (displayLineNumbers > 0)
                tabGrid.Columns["[Line]"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // Adjust time stamp to display seconds
            for (int i = 0; i < tabGrid.Columns.Count; i++)
            {
                if (tabGrid.Columns[i].ValueType.Name == "DateTime")
                    tabGrid.Columns[i].DefaultCellStyle.Format = "MM/dd/yyyy HH:mm:ss";
            }
            AddMenuToTabPage(tabPage);
            // Alternate Colors
            //tabGrid.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(dataGridView1_RowPrePaint);
            tabGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        }

        private DataTable FixBinaryColumnsForDisplay(DataTable t)
        {
            List<string> binaryColumnNames = t.Columns.Cast<DataColumn>().Where(col => col.DataType.Equals(typeof(byte[]))).Select(col => col.ColumnName).ToList();
            foreach (string binaryColumnName in binaryColumnNames)
            {
                // Create temporary column to copy over data
                string tempColumnName = "C" + Guid.NewGuid().ToString();
                t.Columns.Add(new DataColumn(tempColumnName, typeof(string)));
                t.Columns[tempColumnName].SetOrdinal(t.Columns[binaryColumnName].Ordinal);

                // Replace values in every row
                StringBuilder hexBuilder = new StringBuilder(maxBinaryDisplayString * 2 + 2);
                foreach (DataRow r in t.Rows)
                {
                    r[tempColumnName] = BinaryDataColumnToString(hexBuilder, r[binaryColumnName]);
                }

                t.Columns.Remove(binaryColumnName);
                t.Columns[tempColumnName].ColumnName = binaryColumnName;
            }
            return t;
        }

        private string BinaryDataColumnToString(StringBuilder hexBuilder, object columnValue)
        {
            const string hexChars = "0123456789ABCDEF";
            if (columnValue == DBNull.Value)
            {
                // Return special "(null)" value here for null column values
                return "(null)";
            }
            else
            {
                // Otherwise return hex representation
                byte[] byteArray = (byte[])columnValue;
                int displayLength = (byteArray.Length > maxBinaryDisplayString) ? maxBinaryDisplayString : byteArray.Length;
                hexBuilder.Length = 0;
                hexBuilder.Append("0x");
                for (int i = 0; i < displayLength; i++)
                {
                    hexBuilder.Append(hexChars[(int)byteArray[i] >> 4]);
                    hexBuilder.Append(hexChars[(int)byteArray[i] % 0x10]);
                }
                return hexBuilder.ToString();
            }
        }

        public void AddResults(bool bQuery, bool bAllowEdit, TagConnection cnn, DataTable datatable, ResultsTabPage tabPage)
        {
            string dbname = "";
            DataGridView tabGrid = new DataGridView();
            // Test
            tabGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.None);
            tabGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            //
            // Wordwrap requires the following code
            //
            //tabGrid.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //tabGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            //DataTable dt = new DataTable();
            //dt = FixBinaryColumnsForDisplay(datatable);
            FixBinaryColumnsForDisplay(datatable);
            tabGrid.MouseClick += new System.Windows.Forms.MouseEventHandler(resultsGrid_Click);
            string[] words;
            string host = "";
            tabPage.ToolTipText = tabPage.tip;
            switch (cnn.databaseType)
            {
                case SYBASE_DB:
                    words = enginesCombo.Text.Split('@');
                    if (words.Count() > 0)
                        dbname = words[0].Trim();
                    else
                        dbname = enginesCombo.Text;
                    host = cnn.engine;
                    if (cnn.hostname.ToLower().Trim() != "localhost")
                        host += ":" + cnn.hostname;
                    break;
                case MSS_DB:
                    dbname = MSSInstanceCombo.Text;
                    host = cnn.hostname;
                    break;
                case SQLITE_DB:
                    dbname = dbNameCombo.Text;
                    break;
            }

            tabPage.Location = new System.Drawing.Point(4, 25);
            currentTabIndex++;

            if (cnn.resultsHeader != null)
                tabPage.Name = cnn.resultsHeader;
            else
                tabPage.Name = host + "[" + cnn.databaseName + "]:" + cnn.tablename;

            tabPage.Name += "  ";
            tabPage.Padding = new System.Windows.Forms.Padding(3);
            tabPage.Size = new System.Drawing.Size(1320, 230);
            tabPage.TabIndex = 0;
            tabPage.Text = tabPage.Name;
            tabPage.UseVisualStyleBackColor = true;

            // PJB Test form in TabGrid
            tabPage.Controls.Add(tabGrid);
            tabPage.Parent = resultsTabControl;
            tabGrid.DataSource = datatable;

            // Resize Mode
            if (bAllowEdit)
            {
                tabGrid.ReadOnly = false;
                tabGrid.EditMode = DataGridViewEditMode.EditOnKeystroke;
                tabPage.gridTag = 2;
            }
            else
            {
                tabGrid.ReadOnly = true;
                tabPage.gridTag = 1;
            }

            tabGrid.Dock = DockStyle.Fill;
            resultsTabControl.SelectedTab = tabPage;
            if (displayLineNumbers > 0)
                tabGrid.Columns["[Line]"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // Adjust time stamp to display seconds
            for (int i = 0; i < tabGrid.Columns.Count; i++)
            {
                if (datatable.Columns[i].DataType == typeof(System.DateTime))
                {
                    tabGrid.Columns[i].DefaultCellStyle.Format = "MM/dd/yyyy HH:mm:ss";
                    tabGrid.Columns[i].Width = 150;
                }
                else
                {
                    if (datatable.Columns[i].DataType == typeof(System.Decimal))
                        tabGrid.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    else
                    {
                        if (datatable.Columns[i].DataType == typeof(System.Int16))
                            tabGrid.Columns[i].Width = 75;
                    }
                }
            }
            AddMenuToTabPage(tabPage);

            // Alternate Colors
            //tabGrid.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(dataGridView1_RowPrePaint);
            tabGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        }

        //private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        //{
        //    if (e.RowIndex % 2 == 0)
        //        ((DataGridView)sender).Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Beige;
        //}

        void AddMenuToTabPage(TabPage tabPage)
        {
            ContextMenuStrip mnu = new ContextMenuStrip();              // Create the cut/copy/paset menu
            if (tabPage.Controls[0].GetType() == typeof(PowerfulSample))
            {
                ToolStripMenuItem mnuSelectAll = new ToolStripMenuItem("Select All - CTRL+A");
                ToolStripMenuItem mnuCut = new ToolStripMenuItem("Cut - CTRL+X");
                ToolStripMenuItem mnuCopy = new ToolStripMenuItem("Copy - CTRL+C");
                ToolStripMenuItem mnuPaste = new ToolStripMenuItem("Paste - CTRL+V");

                mnuSelectAll.Click += new EventHandler(((PowerfulSample)tabPage.Controls[0]).SelectAll);    //Assign event handlers
                mnuCut.Click += new EventHandler(((PowerfulSample)tabPage.Controls[0]).Cut);                //Assign event handlers
                mnuCopy.Click += new EventHandler(((PowerfulSample)tabPage.Controls[0]).Copy);              //Assign event handlers
                mnuPaste.Click += new EventHandler(((PowerfulSample)tabPage.Controls[0]).Paste);            //Assign event handlers

                mnu.Items.AddRange(new ToolStripItem[] { mnuSelectAll });               //Add to main context menu
                mnu.Items.AddRange(new ToolStripItem[] { mnuCut });                     //Add to main context menu
                mnu.Items.AddRange(new ToolStripItem[] { mnuCopy });                    //Add to main context menu
                mnu.Items.AddRange(new ToolStripItem[] { mnuPaste });                   //Add to main context menu
                tabPage.ContextMenuStrip = mnu;                                         //Assign to datagridview
            }
            else
            {
                ToolStripMenuItem mnuSelectAll = new ToolStripMenuItem("Select All - CTRL+A");
                ToolStripMenuItem mnuCopy = new ToolStripMenuItem("Copy - CTRL+C");

                mnuSelectAll.Click += new EventHandler(dockmnuSelectAll_Click);         //Assign event handlers
                mnuCopy.Click += new EventHandler(dockmnuCopy_Click);                   //Assign event handlers

                mnu.Items.AddRange(new ToolStripItem[] { mnuSelectAll });               //Add to main context menu
                mnu.Items.AddRange(new ToolStripItem[] { mnuCopy });                    //Add to main context menu
                tabPage.ContextMenuStrip = mnu;                                         //Assign to datagridview
            }
        }

        private void saveCurrentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResultsTabPage tabPage = (ResultsTabPage)resultsTabControl.TabPages[resultsTabControl.SelectedIndex];
            tabPage.SaveEditResults();
        }

        private void closeCurrentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResultsTabPage tabPage = (ResultsTabPage)resultsTabControl.TabPages[resultsTabControl.SelectedIndex];
            if (tabPage.CloseAndSave() == 0)
            {
                // Disconnect link from query page
                TabPage qryPage = ((ResultsTabPage)resultsTabControl.TabPages[resultsTabControl.SelectedIndex]).queryTabPage;
                if (qryPage != null)
                    qryPage.Tag = null;
                resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Dispose();
                //resultsTabControl.Controls.RemoveAt(resultsTabControl.SelectedIndex);
            }
        }

        public void FloatQuery(int index)
        {
            ResultsTabPage tabPage = (ResultsTabPage)(resultsTabControl.TabPages[index]);
            // Clean up the Query and Results links
            TabPage qryTab = tabPage.queryTabPage;
            // If Result Query is related to a Query Page, then remove reference
            if (tabPage.queryTabPage != null)
                tabPage.queryTabPage.Tag = null;
            tabPage.queryTabPage = null;

            if (tabPage.Controls[0].GetType() == typeof(DataGridView))
            {
                Form frm = new Form();
                frm.Text = tabPage.Text;

                DataGridView grid = (DataGridView)tabPage.Controls[0];
                if (tabPage.Font != null)
                    grid.Font = tabPage.Font;
                else
                    grid.Font = new Font(fontCombo.Text, defaultFontSize);
                grid.Tag = tabPage.gridTag;
                tabPage.Controls.RemoveAt(0);
                resultsTabControl.TabPages.Remove(tabPage);
                frm.Controls.Add(grid);
                frm.Controls[0].DoubleClick += new System.EventHandler(resultsFormControl_DoubleClick);

                // Create the cut/copy/paset menu
                MyContextMenuStrip mnu = new MyContextMenuStrip(frm, grid, "test");
                ToolStripMenuItem mnuCopy = new ToolStripMenuItem("Copy");
                ToolStripSeparator mnusep = new ToolStripSeparator();
                ToolStripMenuItem mnuDock = new ToolStripMenuItem("Dock");
                ToolStripMenuItem mnuExportText = new ToolStripMenuItem("Export to Text");
                ToolStripMenuItem mnuExportCSV = new ToolStripMenuItem("Export to CSV");
                ToolStripMenuItem mnuClose = new ToolStripMenuItem("Close");
                //Assign event handlers
                mnuCopy.Click += new EventHandler(floatmnuCopy_Click);
                mnuDock.Click += new EventHandler(dockQueryToolStripMenuItem_Click);
                mnuExportText.Click += new EventHandler(exportToText_Click);
                mnuExportCSV.Click += new EventHandler(exportToCSV_Click);
                mnuClose.Click += new EventHandler(closeFormToolStripMenuItem_Click);
                //Add to main context menu
                mnu.Items.AddRange(new ToolStripItem[] { mnuCopy, mnusep, mnuDock, mnuExportText, mnuExportCSV, mnuClose, });
                //Assign to datagridview
                grid.ContextMenuStrip = mnu;
                currentDatagrid = grid;

                frm.FormClosing += new FormClosingEventHandler(f_FormClosing);
                FloatingQueries.Add(frm);
                frm.BringToFront();
                frm.Show();
            }
            else        // Must be a stored procedure/trigger/etc.
            {
                PowerfulSample ps = new PowerfulSample(this, true);
                ps.Text = tabPage.Text;
                ps.ControlBox = true;
                //ps.FormBorderStyle = FormBorderStyle.Sizable; // PJB
                ps.SetText(((PowerfulSample)tabPage.Controls[0]).GetText());
                resultsTabControl.TabPages.Remove(tabPage);
                FloatingQueries.Add(ps);
                ps.BringToFront();
                ps.Show();
            }

            SetButtonStatus();

        }

        private void exportToText_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MyContextMenuStrip toolStrip = (MyContextMenuStrip)menuItem.GetCurrentParent();
            if (toolStrip.datagrid != null)
                exportData(0, toolStrip.datagrid, "");
        }

        private void exportToCSV_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MyContextMenuStrip toolStrip = (MyContextMenuStrip)menuItem.GetCurrentParent();
            if (toolStrip.datagrid != null)
                exportData(1, toolStrip.datagrid, "");
        }

        private void dockmnuSelectAll_Click(object sender, EventArgs e)
        {
            if (resultsTabControl.SelectedIndex >= 0)
            {
                DataGridView datagrid = (DataGridView)resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0];
                if (datagrid != null)
                    datagrid.SelectAll();
            }
        }
        private void dockmnuCopy_Click(object sender, EventArgs e)
        {
            if (resultsTabControl.SelectedIndex >= 0)
            {
                DataGridView datagrid = (DataGridView)resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0];
                if (datagrid != null)
                {
                    if (datagrid.GetCellCount(DataGridViewElementStates.Selected) > 0)
                    {
                        try
                        {
                            // Add the selection to the clipboard.
                            Clipboard.SetDataObject(datagrid.GetClipboardContent());
                        }

                        catch (System.Runtime.InteropServices.ExternalException)
                        {
                        }
                    }
                }
            }
        }

        private void floatmnuCopy_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MyContextMenuStrip toolStrip = (MyContextMenuStrip)menuItem.GetCurrentParent();
            if (toolStrip.datagrid != null)
            {
                if (toolStrip.datagrid.GetCellCount(DataGridViewElementStates.Selected) > 0)
                {
                    try
                    {
                        // Add the selection to the clipboard.
                        Clipboard.SetDataObject(toolStrip.datagrid.GetClipboardContent());
                    }

                    catch (System.Runtime.InteropServices.ExternalException)
                    {
                    }
                }
            }
        }

        private void closeFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MyContextMenuStrip toolStrip = (MyContextMenuStrip)menuItem.GetCurrentParent();
            toolStrip.frm.Close();
        }

        private void dockQueryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MyContextMenuStrip toolStrip = (MyContextMenuStrip)menuItem.GetCurrentParent();
            DockQuery(toolStrip.frm, toolStrip.datagrid);
        }

        private void f_FormClosing(object sender, EventArgs e)
        {
            for (int i = 0; i < FloatingQueries.Count; i++)
            {
                if (FloatingQueries[i] == (Form)sender)
                {
                    FloatingQueries.RemoveAt(i);
                    break;
                }
            }
            SetButtonStatus();
        }

        private void resultsTabControl_DoubleClick(object sender, EventArgs e)
        {
            FloatQuery(resultsTabControl.SelectedIndex);
        }

        //private void DockQuery(Form frm, DataGridView grid)
        //{
        //    ResultsTabPage tabPage = new ResultsTabPage(false, null, "Dock Query " + frm.Text);
        //    DataGridView tabGrid = new DataGridView();
        //    tabGrid.MouseClick += new System.Windows.Forms.MouseEventHandler(resultsGrid_Click);
        //    tabPage.Location = new System.Drawing.Point(4, 25);
        //    tabPage.Name = frm.Text;
        //    tabPage.Padding = new System.Windows.Forms.Padding(3);
        //    tabPage.Size = new System.Drawing.Size(1320, 230);
        //    tabPage.TabIndex = 0;
        //    tabPage.gridTag = (int)grid.Tag;
        //    tabPage.Text = tabPage.Name;
        //    tabPage.UseVisualStyleBackColor = true;
        //    tabPage.Controls.Add(tabGrid);
        //    AddMenuToTabPage(tabPage);
        //    tabPage.Parent = resultsTabControl;
        //    tabGrid.DataSource = grid.DataSource;
        //    tabGrid.Dock = DockStyle.Fill;
        //    tabGrid.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        //    resultsTabControl.SelectedTab = tabPage;

        //    if (grid.Font != null)
        //        tabPage.Font = grid.Font;
        //    else
        //        tabPage.Font = new Font(fontCombo.Text, grid.Font.GetHeight()/*defaultFontSize*/);

        //    if (displayLineNumbers > 0)
        //        tabGrid.Columns["[Line]"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    for (int i = 0; i < FloatingQueries.Count; i++)
        //    {
        //        if (FloatingQueries[i] == frm)
        //        {
        //            FloatingQueries.RemoveAt(i);
        //            break;
        //        }
        //    }
        //    frm.Close();
        //    SetButtonStatus();
        //    this.currentFocus = WindowTypes.ResultsWindow;
        //    SetFont(1);
        //}

        // Don't need to Create a new DataGridView() - Just use the exzisting reference
        private void DockQuery(Form frm, DataGridView grid)
        {
            ResultsTabPage tabPage = new ResultsTabPage(false,null,"Dock Query "+frm.Text);
            grid.MouseClick += new System.Windows.Forms.MouseEventHandler(resultsGrid_Click);
            tabPage.Location = new System.Drawing.Point(4, 25);
            tabPage.Name = frm.Text;
            tabPage.Padding = new System.Windows.Forms.Padding(3);
            tabPage.Size = new System.Drawing.Size(1320, 230);
            tabPage.TabIndex = 0;
            tabPage.gridTag = (int)grid.Tag;
            tabPage.Text = tabPage.Name;
            tabPage.UseVisualStyleBackColor = true;
            tabPage.Controls.Add(grid);
            AddMenuToTabPage(tabPage);
            tabPage.Parent = resultsTabControl;
            grid.Dock = DockStyle.Fill;
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            resultsTabControl.SelectedTab = tabPage;

            if (grid.Font != null)
                tabPage.Font = grid.Font;
            else
                tabPage.Font = new Font(fontCombo.Text, grid.Font.GetHeight()/*defaultFontSize*/);

            if (displayLineNumbers > 0)
                grid.Columns["[Line]"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            for (int i = 0; i < FloatingQueries.Count; i++)
            {
                if (FloatingQueries[i] == frm)
                {
                    FloatingQueries.RemoveAt(i);
                    break;
                }
            }
            frm.Close();
            SetButtonStatus();
            this.currentFocus = WindowTypes.ResultsWindow;
            SetFont(1);
        }

        private void resultsFormControl_DoubleClick(object sender, EventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            Form frm = (Form)(grid.Parent);
            DockQuery(frm, grid);
        }

        private void floatCurrentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FloatQuery(resultsTabControl.SelectedIndex);
            SetButtonStatus();
        }

        private void floatAllTabsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            while (resultsTabControl.TabCount > 0)
                FloatQuery(0);
        }

        private void dockAllQueriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            while (FloatingQueries.Count > 0)
            {
                if (FloatingQueries[0].Controls[0].GetType() == typeof(DataGridView))
                {
                    DataGridView test1 = (DataGridView)FloatingQueries[0].Controls[0];
                    DockQuery(FloatingQueries[0], (DataGridView)FloatingQueries[0].Controls[0]);
                }
                else
                {
                    if ((FloatingQueries[0].GetType() == typeof(PowerfulSample)))
                        DockCurrentWindow((PowerfulSample)FloatingQueries[0]);
                    FloatingQueries.RemoveAt(0);
                }

            }
            SetButtonStatus();
        }

        public void SetButtonStatus()
        {
            bool bQueryEnabled = false;

            if (resultsTabControl.TabCount > 0 || FloatingQueries.Count > 0)
                bQueryEnabled = true;

            // Buttons related to enabled queries
            clearDataBtn.Enabled = bQueryEnabled;
            clearQueryResultsMenu.Enabled = bQueryEnabled;

            if ((resultsTabControl.TabCount > 0) && (resultsTabControl.SelectedIndex >= 0))
            {
                if (resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0].GetType() == typeof(PowerfulSample))
                {
                    toolStripMenuItem2.Enabled = false;
                    toolStripMenuItem3.Enabled = false;
                }
                else
                {
                    toolStripMenuItem2.Enabled = true;
                    toolStripMenuItem3.Enabled = true;
                }
                clearQueryResultsMenu.Enabled = true;
                toolStripMenuItem4.Enabled = true;
                clearDataBtn.Enabled = true;
                floatAllTabsToolStripMenuItem.Enabled = true;
            }
            else
            {
                toolStripMenuItem2.Enabled = false;
                toolStripMenuItem3.Enabled = false;
                clearQueryResultsMenu.Enabled = false;
                toolStripMenuItem4.Enabled = false;
                clearDataBtn.Enabled = false;
                floatAllTabsToolStripMenuItem.Enabled = false;
            }
            if (FloatingQueries.Count > 0)
                dockAllQueriesToolStripMenuItem.Enabled = true;
            else
                dockAllQueriesToolStripMenuItem.Enabled = false;
            if (resultsTabControl.TabPages.Count > 0)
                floatCurrentTabToolStripMenuItem.Enabled = true;
            else
                floatCurrentTabToolStripMenuItem.Enabled = false;
            if (resultsTabControl.TabPages.Count > 0)
                floatCurrentTabToolStripMenuItem.Enabled = true;
            else
                floatCurrentTabToolStripMenuItem.Enabled = false;
            
            if (connectionID >=0)
                bQueryEnabled=true;
            else
                bQueryEnabled=false;
            collectionToolStripMenuItem.Enabled = bQueryEnabled;
            tablesToolStripMenuItem.Enabled = bQueryEnabled;
            columnsToolStripMenuItem1.Enabled = bQueryEnabled;
            viewsToolStripMenuItem1.Enabled = bQueryEnabled;
            indexesToolStripMenuItem1.Enabled = bQueryEnabled;


            if (connectionID >= 0)
                btnExecuteSQL.Enabled = true;
            else
                btnExecuteSQL.Enabled = false;
            EnableStatusToolsWindow();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            resultsTabControl.Controls.RemoveAt(resultsTabControl.SelectedIndex);
            SetButtonStatus();
        }

        private void tb_Changed(TextBox tb)
        {
            if (busy) return;
            busy = true;
            Size tS = TextRenderer.MeasureText(tb.Text, tb.Font);
            bool Hsb = tb.ClientSize.Height < tS.Height + Convert.ToInt32(tb.Font.Size);
            bool Vsb = tb.ClientSize.Width < tS.Width;

            if (Hsb && Vsb)
                tb.ScrollBars = ScrollBars.Both;
            else if (!Hsb && !Vsb)
                tb.ScrollBars = ScrollBars.None;
            else if (Hsb && !Vsb)
                tb.ScrollBars = ScrollBars.Vertical;
            else if (!Hsb && Vsb)
                tb.ScrollBars = ScrollBars.Horizontal;
            busy = false;
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            exportData(2, null, "StatusMessages");
        }

        public bool refreshProcedureArrayMySql(string objectType, TagConnection cnn)
        {
            int tablecnt = 0;
            int tabletype=0;
            bool connected = false;
            string sql;
            try
            {
                //odbcConnect(cnn);
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {
                    Cursor = Cursors.WaitCursor;
                    if (objectType.ToLower() == "trigger")
                    {
                        sql = "select trigger_name from information_schema.triggers";
                        tabletype = treeTriggers;
                    }
                    else
                    {
                        if ((objectType.ToLower() == "procedure"))
                        {
                            sql = "SHOW PROCEDURE STATUS WHERE db = '" + cnn.databaseName + "';";
                            //sql = "select specific_name from mysql.proc where db='" + cnn.databaseName + "';";
                            tabletype = treeProcedures;
                        }
                        else
                        {
                            sql = "SELECT name FROM sys.objects WHERE type_desc='SQL_SCALAR_FUNCTION'";
                            tabletype = treeFunctions;
                        }
                    }

                    using (MySqlCommand cmd = new MySqlCommand(sql, splitForm.cnnMySql))
                    {
                        using (MySqlDataReader mysqlDbReader = cmd.ExecuteReader())
                        {
                            while (mysqlDbReader.Read())
                            {
                                string temp = mysqlDbReader.GetString(0);
                                if (tabletype == treeTriggers)
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tabletype].Nodes.Add(BuildNode(temp, mnuTreeTriggers, tabletype));
                                else
                                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tabletype].Nodes.Add(BuildNode(temp, mnuTreeStoredProcedures, tabletype));
                                cnn.tableCache.Add(new TableCache(temp, 4));
                                tablecnt++;
                            }
                        }
                    }
                    
                    if (objectType == "trigger")
                    {
                        objectType="Triggers"+" [" + tablecnt.ToString() + "]";
                    }
                    else
                    {
                        if (objectType == "procedures")
                        {
                            objectType = "Procedures"+" [" + tablecnt.ToString() + "]";
                        }
                        else
                        {
                            objectType = "Functions" + " [" + tablecnt.ToString() + "]";
                        }
                    }
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tabletype].Text = objectType;
                    //databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[tabletype].Text = (objectType == "trigger" ? "Triggers" : "Procedures") + " [" + tablecnt.ToString() + "]";
                    //odbcDisConnect(cnn);
                }
                else
                {
                    logText("Error: No MySqlODBC connection found.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("MySql ODBC Error: " + ex.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        public bool refreshProcedureArraySQLite(string objectType, TagConnection cnn)
        {
            int tablecnt = 0;
            //int treetype = 0;
            bool connected = false;
            try
            {
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {
                    Cursor = Cursors.WaitCursor;
                    string sql = "SELECT name FROM sqlite_master WHERE type='" + objectType + "' order by name";
                    using (SQLiteCommand cmd = new SQLiteCommand(sql, splitForm.cnnSqlite))
                    {
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string temp = reader.GetString(0);
                                databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeProcedures].Nodes.Add(BuildNode(temp, mnuTreeStoredProcedures, treeProcedures));
                                cnn.tableCache.Add(new TableCache(temp, 3));
                                cnn.autocompleteItems.Add(new MethodAutocompleteItem2(this, temp.ToLower()));
                                tablecnt++;
                            }
                        }
                    }
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeProcedures].Text = (objectType == "trigger" ? "Triggers" : "Procedures") + " [" + tablecnt.ToString() + "]";
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeProcedures].Text = objectType;
                }
                else
                {
                    logText("Error: No ODBC connection found.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        private bool refreshProcedureArray(string objectType, TagConnection cnn)
        {
            int tablecnt = 0;
            bool connected = false;
            string fName = "";
            int treeItem;

            if (objectType.ToLower().Trim() == "tr")
                treeItem = treeTriggers;
            else
            {
                if (objectType.ToLower().Trim() == "f")
                    treeItem = treeFunctions;
                else
                    treeItem = treeProcedures;
            }
            try
            {
                //odbcConnect(cnn);
                connected = splitForm.globalSettings.connected;
                if (splitForm.globalSettings.connected)
                {
                    Cursor = Cursors.WaitCursor;
                    OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                            DbCommand.CommandText = "SELECT name, type FROM sysobjects WHERE (type = '" + objectType + "') and uid=1 order by name;";
                            break;
                        case MSS_DB:
                            if ((objectType.ToLower() == "f"))
                                DbCommand.CommandText = "SELECT name FROM sys.objects WHERE type_desc='SQL_SCALAR_FUNCTION'";
                            else
                                DbCommand.CommandText = "use " + 
                                                        splitForm.globalSettings.dbName +
                                                        ";SELECT name, type FROM dbo.sysobjects WHERE (type = '" + objectType + "')  order by name";
                            break;
                        default:
                            connected = false;
                            break;
                    }

                    if (connected)
                    {
                        OdbcDataReader DbReader = DbCommand.ExecuteReader();
                        fName = DbReader.GetName(0);

                        while (DbReader.Read())
                        {
                            if (!DbReader.IsDBNull(0))
                            {
                                string temp = DbReader.GetString(0);
                                switch (treeItem)
                                {
                                    case treeTriggers:
                                        databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeItem].Nodes.Add(BuildNode(temp, mnuTreeStoredProcedures, treeTriggers));
                                        break;
                                    case treeProcedures:
                                        databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeItem].Nodes.Add(BuildNode(temp, mnuTreeStoredProcedures,treeProcedures));
                                        break;
                                    case treeFunctions:
                                        databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeItem].Nodes.Add(BuildNode(temp, mnuTreeFunctions, treeFunctions));
                                        break;

                                }
                                // 0=undefined, 1=table.2=View,3=Stored Procedure,4=Trigger,5=
                                cnn.tableCache.Add(new TableCache(temp, 3));
                                MethodAutocompleteItem2 item = new MethodAutocompleteItem2(this, temp.ToLower());
                                item.ToolTipTitle = "Table: " + temp;
                                item.ImageIndex = 3;
                                cnn.autocompleteItems.Add(item);
                                tablecnt++;
                            }
                        }
                        string strName = "<unknown>";
                        switch (treeItem)
                        {
                            case treeTriggers:
                                strName = "Triggers";
                                break;
                            case treeProcedures:
                                strName = "Procedures";
                                break;
                            case treeFunctions:
                                strName = "Functions";
                                break;
                        }
                        databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Nodes[treeItem].Text = strName + " [" + tablecnt.ToString() + "]";
                        DbCommand.Dispose();
                        DbReader.Close();
                        //odbcDisConnect(cnn);
                    }
                }
                else
                {
                    logText("Error: No ODBC connection found.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            return connected;
        }

        //void Sybase_GetCurrentStoredProcedureID(string spName, TagConnection cnn)
        //{
        //    try
        //    {
        //        //odbcConnect(cnn);
        //        DataTable dt = new DataTable();
        //        OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
        //        DbCommand.CommandText = "SELECT id FROM sysobjects where name = '" + spName + "'";
        //        OdbcDataReader DbReader = DbCommand.ExecuteReader();
        //        DbReader.Read();
        //        StoredProcedureId = DbReader.GetInt32(0);
        //        DbCommand.Dispose();
        //        DbReader.Close();
        //        //odbcDisConnect(cnn);
        //    }
        //    catch (Exception ex)
        //    {
        //        logText("Error: " + ex.Message);
        //    }
        //}

        void OpenProcedure(string type, string name, TextBox spText,TagConnection cnn)
        {
            OdbcCommand DbCommand = null;
            OdbcDataReader DbReader;

            MySqlCommand mysqlDbCommand = null;
            MySqlDataReader mysqlDbReader=null;

            string query="";
            odbcConnect(cnn);
            if (splitForm.globalSettings.connected)
            {
                Cursor = Cursors.WaitCursor;
                try
                {
                    DataTable dt = new DataTable();
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                            //Sybase_GetCurrentStoredProcedureID(name, cnn);
                            //DbCommand = splitForm.cnnODBC.CreateCommand();
                            //DbCommand.CommandText = "SELECT text FROM syscomments WHERE id=" + StoredProcedureId.ToString();

                            DbCommand = splitForm.cnnODBC.CreateCommand();
                            DbCommand.CommandText = "sp_helptext '" + name + "'";
                            break;
                        case MSS_DB:
                            DbCommand = splitForm.cnnODBC.CreateCommand();
                            DbCommand.CommandText = "SELECT OBJECT_DEFINITION (OBJECT_ID(N'" + name + "'))";
                            //if (procType==treeProcedures)
                            //    DbCommand.CommandText = "SELECT OBJECT_DEFINITION (OBJECT_ID(N'" + name + "'))";
                            //else
                            //    DbCommand.CommandText = 
                            //        "select definition from sys.objects o join sys.sql_modules m on m.object_id = o.object_id where o.object_id = object_id( 'dbo." + name + "') and o.type = 'V'";
                            break;
                        case MYSQL_DB:
                            if (type.ToLower()=="t")
                                query = "SELECT ACTION_STATEMENT FROM information_schema.triggers where TRIGGER_NAME = '" + name + "';";
                            else
                                query = "SELECT body FROM mysql.proc where db='" + cnn.databaseName +"' and name='" + name+"';";
                            mysqlDbCommand = new MySqlCommand(query, splitForm.cnnMySql);
                            break;
                        default:
                            return;
                    }

                    
                    spText.Text = "";
                    switch (cnn.databaseType)
                    {
                        case SYBASE_DB:
                        case MSS_DB:
                            DbReader = DbCommand.ExecuteReader();
                            while (DbReader.Read())
                            {
                                if (databaseType == SYBASE_DB)
                                    spText.Text += DbReader.GetString(0);
                                else
                                {
                                    if (!DbReader.IsDBNull(0))
                                        spText.Text += DbReader.GetString(0);
                                    else
                                        spText.Text += "Unable to read Procedure.";
                                }
                            }
                            DbCommand.Dispose();
                            DbReader.Close();
                            break;
                        case SQLITE_DB:
                            break;
                        case MYSQL_DB:
                            mysqlDbReader = mysqlDbCommand.ExecuteReader();
                            while (mysqlDbReader.Read())
                            {
                                if (databaseType == SYBASE_DB)
                                    spText.Text += mysqlDbReader.GetString(0);
                                else
                                {
                                    if (!mysqlDbReader.IsDBNull(0))
                                        spText.Text += mysqlDbReader.GetString(0);
                                    else
                                        spText.Text += "Unable to read Procedure.";
                                }
                            }
                            mysqlDbCommand.Dispose();
                            mysqlDbReader.Close();
                            break;
                    }

                    odbcDisConnect(cnn);
                    SetButtonStatus();
                    Cursor = Cursors.Default;
                }

                catch (Exception ex)
                {
                    logText("Error: " + ex.Message);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        public void DockCurrentWindow(PowerfulSample ps)
        {
            ps.ControlBox = false;
            ps.FormBorderStyle = FormBorderStyle.None;
            DockProcedure(ps);
        }

        public void DockProcedure(PowerfulSample frm)
        {
            ResultsTabPage tabPage = new ResultsTabPage(false,null,"Dock Procedure "+frm.Text);

            tabPage.Location = new System.Drawing.Point(4, 25);
            tabPage.Name = frm.Text;
            tabPage.Padding = new System.Windows.Forms.Padding(3);
            tabPage.Size = new System.Drawing.Size(1320, 230);
            tabPage.TabIndex = 0;
            tabPage.Text = tabPage.Name;
            tabPage.UseVisualStyleBackColor = true;


            frm.TopLevel = false;
            frm.DisableDockMenu();
            frm.Dock = DockStyle.Fill;
            frm.Refresh();
            tabPage.Controls.Add(frm);
            frm.Show();

            AddMenuToTabPage(tabPage);
            tabPage.Parent = resultsTabControl;
            resultsTabControl.SelectedTab = tabPage;
            SetButtonStatus();
        }

        public void ResetDatabase()
        {
            engines.Clear();            // remove engines from combo box
            enginesLoaded = false;
            enginesCombo.Items.Clear();
        }

        private void userTextBox_TextChanged(object sender, EventArgs e)
        {
            ResetDatabase();
        }

        private void pwdTextBox_TextChanged(object sender, EventArgs e)
        {
            ResetDatabase();
        }

        private void dbNameCombo_TextChanged(object sender, EventArgs e)
        {
            ResetDatabase();
        }

        private void treeviewCollection_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(0, GetConnectionTag());
        }
        private void treeviewTables_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(1, GetConnectionTag());
        }
        private void treeviewView_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(3, GetConnectionTag());
        }
        private void treeviewIndexes_Click(object sender, EventArgs e)
        {
            indexesToolStripMenuItem1_Click(sender, e);
            //GetSchemaCollection(4, GetConnectionTag());
        }
        
        private void collectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(0, GetConnectionTag());
        }

        private void tablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(1,GetConnectionTag());
        }

        private void columnsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(2, GetConnectionTag());
        }

        private void viewsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GetSchemaCollection(3, GetConnectionTag());
        }

        private void indexesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string tablename = databaseTableView.SelectedNode.Text.Trim();
            TagConnection cnn = GetConnectionTag();
            if (cnn != null)
            {
                cnn.tablename = databaseTableView.SelectedNode.Text.Trim();
                if ((cnn.databaseType == SYBASE_DB) || (cnn.databaseType == MSS_DB))
                    GetSchema(cnn, "index");
                else
                    GetSchemaCollection(4, GetConnectionTag());
            }
        }

        private void browseDatabaseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.InitialDirectory = this.splitForm.globalSettings.gfiPath + "\\cnf";
            openFileDialog1.Filter = @"SQLite Files(*.sqlite,*.db)|*.sqlite;*.db|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = false;
            openFileDialog1.ValidateNames = false;
            openFileDialog1.CheckFileExists = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                dbNameCombo.Text = openFileDialog1.FileName;
        }

        //////////////////////////////////////////////////////////////////////
        // Add a Query page
        /////////////////////////////////////////////////////////////////////
        private void AddQueryPage()
        {
            queryTabAddPage(null,null,null);
            queryTabAddEmptyPage();
            // Now select the first tab in the list
            queryTabControl.SelectedIndex = 0;
        }

        private void AddProcToQueryTab(string spname, string text)
        {
            // Select previous blank
            TreeNode saveNode = databaseTableView.SelectedNode;
            if (queryTabControl.TabPages.Count > 0)
            {
                queryTabControl.SelectTab(queryTabControl.TabPages.Count - 1);
                if (queryTabControl.SelectedTab.Controls.Count < 1)
                {
                    PowerfulSample ps = new PowerfulSample(this, false);
                    ps.TopLevel = false;
                    ps.Dock = DockStyle.Fill;
                    ps.SetText(text);
                    ps.Refresh();
                    queryTabControl.SelectedTab.Controls.Add(ps);
                    ps.ClearChanged();
                    ps.Show();
                    AddMenuToTabPage(queryTabControl.SelectedTab);
                    queryTabControl.SelectedTab.Text = spname + "  ";
                    // Now add blank tabpage
                    queryTabControl.TabPages.Add(new System.Windows.Forms.TabPage("  +"));
                }
            }
            databaseTableView.Select();
            databaseTableView.SelectedNode = saveNode;
        }

        private void queryTabControl_Click(object sender, EventArgs e)
        {
            TabPage tabPage = ((TabControl)sender).SelectedTab;
            if (tabPage.Tag != null)
                resultsTabControl.SelectedTab = (TabPage)tabPage.Tag;

            if (tabPage.Controls.Count == 0)
            {
                queryTabAddPage(tabPage, null, null);
                queryTabAddEmptyPage();
            }
            currentFocus = WindowTypes.QueryWindow;
            queryTabControl.SelectedTab = tabPage;
        }

        // tabPageIn: if null create new page else populate current tab
        private void queryTabAddPage(TabPage tabPage, string tabName, string tabText)
        {
            if (tabPage == null)
            {
                tabPage = new System.Windows.Forms.TabPage();
                queryTabControl.TabPages.Add(tabPage);
                queryTabControl.SelectedTab = tabPage;
            }
            PowerfulSample ps = new PowerfulSample(this, false);
            ps.SetAutoCompleteMenu(GetConnectionTag());
            ps.TopLevel = false;
            ps.Dock = DockStyle.Fill;
            ps.Refresh();
            tabPage.Controls.Add(ps);
            ps.Show();
            if (tabText != null)
                ps.SetText(tabText);
            AddMenuToTabPage(tabPage);
            currentQueryIndex++;
            if (tabName != null)
                tabPage.Text = tabName;
            else
                tabPage.Text = "Query-" + currentQueryIndex.ToString() + "  ";
            tabPage.Show();
            ps.ClearChanged();
        }

        // tabPageIn: if null create new page else populate current tab
        private void queryTabAddEmptyPage()
        {
            TabPage tabPage = new System.Windows.Forms.TabPage("  ");
            queryTabControl.TabPages.Add(tabPage);
            queryTabControl.SelectedTab = tabPage;
            currentFocus = WindowTypes.QueryWindow;
        }

        private int resetQueries()
        {
            int ret_cd = 0;
            while (queryTabControl.TabCount > 0)
            {
                if (queryTabControl.TabPages[queryTabControl.SelectedIndex].Controls.Count > 0)
                {
                    PowerfulSample ps = (PowerfulSample)(queryTabControl.TabPages[queryTabControl.SelectedIndex].Controls[0]);
                    if ((ret_cd = closeIndividualQuery(queryTabControl.SelectedIndex, ps)) < 0)
                        break;
                }
                else
                    queryTabControl.Controls.RemoveAt(queryTabControl.SelectedIndex);
            }
            currentQueryIndex = 0;
            return ret_cd;
        }

        private void closeAllQueries(object sender, EventArgs e)
        {
            int saveQueryIndex = currentQueryIndex;
            resetQueries();
            if (queryTabControl.TabPages.Count == 0)
            {
                currentQueryIndex = 0;
                queryTabAddEmptyPage();
            }
            else
                currentQueryIndex=saveQueryIndex;
            SetButtonStatus();
        }

        private int closeIndividualQuery(int index, PowerfulSample ps)
        {
            if (ps.IsChanged())
            {
                switch (MessageBox.Show("Do you want save " + queryTabControl.TabPages[index].Text + " ?", "Save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information))
                {
                    case System.Windows.Forms.DialogResult.Yes:     // Save Query Window
                        saveQuery(ps, queryTabControl.TabPages[index].Text.Trim());
                        break;
                    case DialogResult.Cancel:                       // Abort Closing the rest of the Windows
                        return -1;
                }
            }
            // Now remove the Query Window
            RemoveResultsForQuery(queryTabControl.TabPages[index]);
            queryTabControl.Controls.RemoveAt(index);

            SetButtonStatus();
            return 0;
        }

        private void closeQuerWindow()
        {
            PowerfulSample ps = (PowerfulSample)(queryTabControl.TabPages[queryTabControl.SelectedIndex].Controls[0]);
            if (closeIndividualQuery(queryTabControl.SelectedIndex, ps) < 0)
                return;
            if (queryTabControl.TabCount < 1)
            {
                currentQueryIndex = 0;
                queryTabAddPage(null,null,null);
                queryTabAddEmptyPage();
            }
        }

        private void closeCurrentQuery(object sender, EventArgs e)
        {
            closeQuerWindow();
        }

        private void renameCurrentQuery(object sender, EventArgs e)
        {
            string tabName = Interaction.InputBox("Enter new Results tab name:", "Rename Results Tab", queryTabControl.SelectedTab.Text.Trim(), -1, -1);
            if (tabName.Length > 0)
                queryTabControl.SelectedTab.Text = tabName + "  ";
        }

        private void queryTabControl_MouseDown(object sender, MouseEventArgs e)
        {
            int ix;
            if (e.Button == MouseButtons.Right)
            {
                for (ix = 0; ix < queryTabControl.TabCount; ++ix)
                {
                    if (queryTabControl.GetTabRect(ix).Contains(e.Location))
                        break;
                }

                queryTabControl.SelectedTab = queryTabControl.TabPages[ix];
                ContextMenuStrip mnu = new ContextMenuStrip();
                ToolStripMenuItem mnuClose = new ToolStripMenuItem("Close");
                ToolStripMenuItem mnuRename = new ToolStripMenuItem("Rename");
                ToolStripSeparator mnusep = new ToolStripSeparator();
                ToolStripMenuItem mnuCloseAll = new ToolStripMenuItem("Close ALL Queries");
                mnu.Items.AddRange(new ToolStripItem[] { mnuClose, mnuRename, mnusep, mnuCloseAll });

                //Assign event handlers
                mnuClose.Click += new EventHandler(closeCurrentQuery);
                mnuRename.Click += new EventHandler(renameCurrentQuery);
                mnuCloseAll.Click += new EventHandler(closeAllQueries);
                mnu.Show((Control)sender, queryTabControl.GetTabRect(ix).Left, queryTabControl.GetTabRect(ix).Bottom);
                currentFocus = WindowTypes.QueryWindow;
            }
            else
            {
                if (e.Button == MouseButtons.Left)
                {
                    TabPage tabPage = ((TabControl)sender).SelectedTab;
                    if (tabPage.Controls.Count != 0)
                    {
                        // Process MouseDown event only till (tabControl.TabPages.Count - 1) excluding the last TabPage
                        for (var i = 0; i < this.queryTabControl.TabPages.Count; i++)
                        {
                            var tabRect = this.queryTabControl.GetTabRect(i);
                            tabRect.Inflate(-2, -2);
                            var closeImage = new Bitmap((Image)Properties.Resources.Close3);
                            var imageRect = new Rectangle(
                                (tabRect.Right - closeImage.Width),
                                tabRect.Top + (tabRect.Height - closeImage.Height) / 2,
                                closeImage.Width,
                                closeImage.Height);
                            if (imageRect.Contains(e.Location))
                            {
                                closeQuerWindow();
                                break;
                            }
                        }
                    }
                    else
                        queryTabControl_Click(sender, e);
                }
            }
        }

        private void refreshEng_Click(object sender, EventArgs e)
        {
            enginesLoaded = false;
            Cursor = Cursors.WaitCursor;
            //BeginWaitDialogMessage(this.Location, "Locate Sybase Database Engines", "Please wait while Sybase Instances are being located.");
            MSSInstanceCombo.Text = "Localhost";
            LoadEngines();
            Cursor = Cursors.Default;
            //EndWaitDialogMessage();
            enginesLoaded = true;
        }

        private void refreshMSS_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //BeginWaitDialogMessage(this.Location, "Locate MSS Instances", "Please wait while SQL Server Instances are being located.");
            LoadMSSInstances();
            Cursor = Cursors.Default;
            //EndWaitDialogMessage();
        }

        private bool refreshtablesNames(string objectType, TagConnection cnn)
        {
            bool connected = false;
            switch (cnn.databaseType)
            {
                case SYBASE_DB:
                case MSS_DB:
                    connected = BuildTableArray(objectType,cnn);
                    break;
                case SQLITE_DB:
                    connected = BuildTableArraySQlite(objectType.ToLower().Trim(), cnn);
                    break;
                case MYSQL_DB:
                    connected = BuildTableArrayMySql(objectType.ToLower().Trim(), cnn);
                    break;
                default:
                    break;
            }
            return connected;
        }

        private bool refreshtriggerNames(TagConnection cnn)
        {
            bool connected = false;
            switch (cnn.databaseType)
            {
                case SYBASE_DB:
                case MSS_DB:
                    connected = refreshProcedureArray("TR", cnn);
                    break;
                case SQLITE_DB:
                    connected = refreshProcedureArraySQLite("trigger", cnn);
                    break;
                case MYSQL_DB:
                    connected = refreshProcedureArrayMySql("trigger", cnn);
                    break;
                default:
                    break;
            }
            return connected;
        }

        private void refreshAutoCompleteQueryWindows()
        {
            for (int i = 0; i < queryTabControl.TabCount; i++)
            {
                if (queryTabControl.TabPages[i].Controls.Count > 0)
                {
                    PowerfulSample ps = (PowerfulSample)(queryTabControl.TabPages[i].Controls[0]);
                    if (databaseTableView.SelectedNode != null)
                    {
                        TagConnection cnn = (TagConnection)(databaseTableView.SelectedNode.Tag);
                        ps.ReloadAutocompleteItems(cnn);
                    }
                }
            }
        }

        private int AddDatabaseNode(TagConnection cnn)
        {
            string nodeName;
            int nodeId = databaseTableView.Nodes[0].Nodes[0].Nodes.Count;
            if (nodeId >= 0)
            {
                switch (cnn.databaseType)
                {
                    case SYBASE_DB:
                        nodeName=cnn.engine+":"+cnn.databaseName;
                        break;
                    case MSS_DB:
                        nodeName=cnn.hostname+":"+cnn.databaseName;
                        break;
                    case SQLITE_DB:
                        nodeName = cnn.databaseName;
                        break;
                    case MYSQL_DB:
                        nodeName = cnn.databaseName;
                        break;
                    default:
                        nodeName="Unknown";
                        break;
                }
                databaseTableView.Nodes[0].Nodes[0].Nodes.Add(BuildNode(nodeName, mnuTreeConnection,treeDatabase));
                databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Tag = cnn;
                databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Nodes.Add(BuildNode("Tables", null,treeFolder));
                databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Nodes.Add(BuildNode("Views", null,treeFolder));

                if (cnn.databaseType != SQLITE_DB)
                    databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Nodes.Add(BuildNode("Procedures", null,treeFolder));

                // Add functions entry if MSS SQL Server
                if (cnn.databaseType == MSS_DB)
                    databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Nodes.Add(BuildNode("Functions", null, treeFolder));
                databaseTableView.Nodes[0].Nodes[0].Nodes[nodeId].Nodes.Add(BuildNode("Triggers", null,treeFolder));
            }
            else
                return - 1;
            return 0;
        }

        public void ConnectSpecificDatabase(TagConnection cnn)
        {
            int connectionID_save = connectionID;
            int i = 0, nConnections;
            saveConnection(cnn);
            connectionID = -1;
            if ((nConnections = databaseTableView.Nodes[0].Nodes[0].Nodes.Count) > 0)
            {
                for (i = 0; i < nConnections; i++)
                {
                    TagConnection cnnNode = (TagConnection)(databaseTableView.Nodes[0].Nodes[0].Nodes[i].Tag);
                    if (string.Compare(cnn.connection.ToLower().Trim(), cnnNode.connection.ToLower().Trim()) == 0)
                    {
                        databaseTableView.SelectedNode = databaseTableView.Nodes[0].Nodes[0].Nodes[i];
                        connectionID = i;
                        break;
                    }
                }
            }

            //saveConnection();
            if (connectionID < 0)
            {
                if (AddDatabaseNode(cnn) < 0)
                {
                    connectionID = connectionID_save;
                    return;
                }
            }

            // Set current connection ID
            if (connectionID < 0)
            {
                connectionID = i;
                odbcConnect(cnn);
                if (refreshtablesNames("table", cnn))
                {
                    if (refreshtablesNames("view", cnn))
                    {
                        if (refreshtriggerNames(cnn))
                        {
                            if (cnn.databaseType != SQLITE_DB)
                            {
                                if (cnn.databaseType == MYSQL_DB)
                                    refreshProcedureArrayMySql("procedure", cnn);
                                else
                                {
                                    refreshProcedureArray("P", cnn);
                                    if (cnn.databaseType == MSS_DB)
                                        refreshProcedureArray("F", cnn);
                                }
                            }
                        }
                    }
                }
                else
                {
                    databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Remove();
                    i = connectionID_save;
                }
                odbcDisConnect(cnn);
            }
            connectionID = i;
            if (connectionID >= 0)
            {
                // PJB -Test Code
                for (i = 0; i < databaseTableView.Nodes[0].Nodes[0].Nodes.Count; i++)
                {
                    if (i == connectionID)
                    {
                        databaseTableView.Nodes[0].Nodes[0].Nodes[i].ForeColor = Color.White;
                        databaseTableView.Nodes[0].Nodes[0].Nodes[i].BackColor = Color.Blue;
                    }
                    else
                    {
                        databaseTableView.Nodes[0].Nodes[0].Nodes[i].ForeColor = SystemColors.WindowText;
                        databaseTableView.Nodes[0].Nodes[0].Nodes[i].BackColor = SystemColors.Window;
                    }
                }
                databaseTableView.SelectedNode = databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID];
                refreshAutoCompleteQueryWindows();
            }
        }

        public void ConnectDatabase()
        {
            TagConnection cnn = new TagConnection();
            cnn.databaseType = comboDBType.SelectedIndex + 1;
            cnn.engine = enginesCombo.Text.Trim();
            cnn.hostname = MSSInstanceCombo.Text.Trim();
            cnn.databaseName = dbNameCombo.Text.Trim();
            switch (cnn.databaseType)
            {
                case SYBASE_DB:         // Sybase
                    int item = enginesCombo.SelectedIndex;
                    if (enginesLoaded)
                    {
                        DBEngines eng = engines.Find(x => x.desc == enginesCombo.Text);
                        if (eng != null)
                        {
                            cnn.hostname = eng.host;
                            cnn.engine = eng.eng;
                        }
                    }
                    break;
                case MSS_DB:         // MSS
                    if (MSSInstanceCombo.Text.Trim().Length > 0)
                        cnn.hostname = MSSInstanceCombo.Text;
                    else
                        cnn.hostname = "Localhost";
                    break;
                case MYSQL_DB:      // MySql
                    cnn.port = 3306;
                    if (MSSInstanceCombo.Text.Trim().Length > 0)
                        cnn.hostname = MSSInstanceCombo.Text;
                    else
                        cnn.hostname = "Localhost";
                    break;
            }
            Int32.TryParse(portTextBox.Text, out cnn.port);

            saveConnection(cnn);
            cnn.connection = splitForm.BuildConnectionStringEx();
            cnn.tablename = "";
            ConnectSpecificDatabase(cnn);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ConnectDatabase();
            SetButtonStatus();
        }

        public void showWaitMsgForm(object parm)
        {
            WaitCompleteDlg = new WaitDlg((WaitDlgParameters)parm);
            WaitCompleteDlg.ShowDialog();
        }

        private void BeginWaitDialogMessage(Point pt, string hdr, string msg)
        {
            waitParmData.location = pt;
            waitParmData.hdr = hdr;
            waitParmData.msg = msg;
            waitParmData.location.X += 100;
            waitParmData.location.Y += 100;
            Thread thread = new Thread(new ParameterizedThreadStart(showWaitMsgForm));
            thread.Start(waitParmData);
        }

        private void EndWaitDialogMessage()
        {
            if (WaitCompleteDlg != null)
                WaitCompleteDlg.quit = true;
        }

        private void zoomInBtn_Click(object sender, EventArgs e)
        {
            SetFont(1);
        }

        private void zoomOutBtn_Click(object sender, EventArgs e)
        {
            SetFont(-1);
        }

        private void selectedFontChanged(object sender, EventArgs e)
        {
            SetFont(0);
        }

        private void renameCurrentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tabName = Interaction.InputBox("Enter new Results tab name:", "Rename Results Tab", resultsTabControl.SelectedTab.Text.Trim(), -1, -1);
            if (tabName.Length > 0)
                resultsTabControl.SelectedTab.Text = tabName + "  ";
        }

        private void resultsTabControl_MouseDown(object sender, MouseEventArgs e)
        {
            int ix;
            if (e.Button == MouseButtons.Right)
            {
                for (ix = 0; ix < resultsTabControl.TabCount; ++ix)
                {
                    if (resultsTabControl.GetTabRect(ix).Contains(e.Location))
                        break;
                }

                resultsTabControl.SelectedTab = resultsTabControl.TabPages[ix];
                ContextMenuStrip mnu = new ContextMenuStrip();
                ToolStripMenuItem mnuRename = new ToolStripMenuItem("Rename");
                ToolStripSeparator mnusep1 = new ToolStripSeparator();
                ToolStripMenuItem mnuFloat = new ToolStripMenuItem("Float");
                ToolStripSeparator mnusep2 = new ToolStripSeparator();
                ToolStripMenuItem mnuprintpreview = new ToolStripMenuItem("Print Preview Results");
                ToolStripMenuItem mnuprint = new ToolStripMenuItem("Print Results");
                ToolStripSeparator mnusep3 = new ToolStripSeparator();
                ToolStripMenuItem mnuClose = new ToolStripMenuItem("Close");

                if (((ResultsTabPage)(resultsTabControl.SelectedTab)).AllowEdit)
                {
                    ToolStripMenuItem mnuSave = new ToolStripMenuItem("Save Changes");
                    mnuSave.Click += new EventHandler(saveCurrentTabToolStripMenuItem_Click);
                    mnu.Items.AddRange(new ToolStripItem[] { mnuRename, mnusep1, mnuFloat, mnuSave, mnusep2, mnuprintpreview, mnuprint,mnusep3, mnuClose });
                }
                else
                    mnu.Items.AddRange(new ToolStripItem[] { mnuRename, mnusep1, mnuFloat, mnusep2, mnuprintpreview, mnuprint,mnusep3, mnuClose });

                mnuRename.Click += new EventHandler(renameCurrentTabToolStripMenuItem_Click);
                mnuFloat.Click += new EventHandler(floatCurrentTabToolStripMenuItem_Click);

                mnuprintpreview.Click += new EventHandler(printPreviewGridMenuItem_Click);
                mnuprint.Click += new EventHandler(printGrid_Click);

                mnuClose.Click += new EventHandler(closeCurrentTabToolStripMenuItem_Click);
                mnu.Show((Control)sender, resultsTabControl.GetTabRect(ix).Left, resultsTabControl.GetTabRect(ix).Bottom);
            }
            else
            {
                // Process MouseDown event only till (tabControl.TabPages.Count - 1) excluding the last TabPage
                for (int i = 0; i < this.resultsTabControl.TabPages.Count; i++)
                {
                    var tabRect = this.resultsTabControl.GetTabRect(i);
                    tabRect.Inflate(-2, -2);
                    var closeImage = new Bitmap((Image)Properties.Resources.Close3);
                    var imageRect = new Rectangle(
                        (tabRect.Right - closeImage.Width),
                        tabRect.Top + (tabRect.Height - closeImage.Height) / 2,
                        closeImage.Width,
                        closeImage.Height);
                    if (imageRect.Contains(e.Location))
                    {
                        if (((ResultsTabPage)resultsTabControl.SelectedTab).AllowEdit)
                        {
                            // If this is an Edit Window, then see if user wants to close it
                            if (((ResultsTabPage)resultsTabControl.SelectedTab).CloseAndSave() != 0)
                                break;
                        }
                        // Disconnect link from query page
                        TabPage qryPage = ((ResultsTabPage)resultsTabControl.TabPages[i]).queryTabPage;
                        if (qryPage != null)
                            qryPage.Tag = null;
                        resultsTabControl.TabPages[i].Dispose();
                        //resultsTabControl.TabPages.RemoveAt(i);
                        break;
                    }

                }
            }
        }

        public void resultsGrid_Click(object sender, EventArgs e)
        {
            currentFocus = WindowTypes.ResultsWindow;
            bNoFontChange = true;
            fontCombo.Text = ((DataGridView)sender).Font.Name;
            bNoFontChange = false;
        }

        // We will get back to this later
        //private void resultsTabControl_MouseMove(object sender, MouseEventArgs e)
        //{
        //    // Process MouseDown event only till (tabControl.TabPages.Count - 1) excluding the last TabPage
        //    for (var i = 0; i < this.resultsTabControl.TabPages.Count; i++)
        //    {
        //        var tabRect = this.resultsTabControl.GetTabRect(i);
        //        tabRect.Inflate(-2, -2);
        //        var closeImage = new Bitmap((Image)Properties.Resources.Close3);
        //        var imageRect = new Rectangle(
        //            (tabRect.Right - closeImage.Width),
        //            tabRect.Top + (tabRect.Height - closeImage.Height) / 2,
        //            closeImage.Width,
        //            closeImage.Height);
        //        if (imageRect.Contains(e.Location))
        //        {
        //            MessageBox.Show("Found it");
        //            break;
        //        }
        //    }
        //}

        //private void resultsTabControl_MouseHover(object sender, EventArgs e)
        //{
        //    //Point pt = Control.MousePosition;
        //    //// Process MouseDown event only till (tabControl.TabPages.Count - 1) excluding the last TabPage
        //    //for (var i = 0; i < this.resultsTabControl.TabPages.Count; i++)
        //    //{
        //    //    var tabRect = this.resultsTabControl.GetTabRect(i);
        //    //    tabRect.Inflate(-2, -2);
        //    //    var closeImage = new Bitmap((Image)Properties.Resources.Close3);
        //    //    var imageRect = new Rectangle(
        //    //        (tabRect.Right - closeImage.Width),
        //    //        tabRect.Top + (tabRect.Height - closeImage.Height) / 2,
        //    //        closeImage.Width,
        //    //        closeImage.Height);
        //    //    if (imageRect.Contains(pt))
        //    //    {
        //    //        break;
        //    //    }
        //    //}
        //}

        private void resultsText_Click(object sender, EventArgs e)
        {
            currentFocus = WindowTypes.LogWindow;
            bNoFontChange = true;
            fontCombo.Text = resultsText.Font.Name;
            bNoFontChange = false;
        }

        private void resultsTabControl_Click(object sender, EventArgs e)
        {
            currentFocus = WindowTypes.ResultsWindow;
            bNoFontChange = true;
            fontCombo.Text = resultsTabControl.SelectedTab.Font.Name;
            bNoFontChange = false;

            if (resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Controls[0].GetType() == typeof(PowerfulSample))
            {
                toolStripMenuItem2.Enabled = false;
                toolStripMenuItem3.Enabled = false;
            }
            else
            {
                toolStripMenuItem2.Enabled = true;
                toolStripMenuItem3.Enabled = true;
            }
        }

        private void exportMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        // PJB - Test update of table using DataTable
        private void TestAdapterDataTable(TagConnection cnn)
        {
            odbcConnect(cnn);
            string queryString = "SELECT * from cnf";
            DataTable dt = new DataTable();
            //DataSet dataSet = new DataSet();
            OdbcDataAdapter adapter = new OdbcDataAdapter(queryString, splitForm.cnnODBC);
            OdbcCommandBuilder builder = new OdbcCommandBuilder(adapter);
            adapter.Fill(dt);
            // Make a change
            dt.Rows[0][1] = "Test Adapter"; // Update a column value
            //dataSet.Rows[9][2] = "CLOUD";
            adapter.UpdateCommand = builder.GetUpdateCommand();
            adapter.Update(dt);
            odbcDisConnect(cnn);
            dt.Dispose();
        }

        private void toolStripEnableStatusWIndow_Click(object sender, EventArgs e)
        {
            bEnableStatusWindow = !bEnableStatusWindow;
            SetButtonStatus();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            bEnableToolsWindow = !bEnableToolsWindow;
            SetButtonStatus();
        }

        private void resultsTabDrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                int i;
                ResultsTabPage tabPage = (ResultsTabPage)(resultsTabControl.TabPages[e.Index]);
                var tabRect = this.resultsTabControl.GetTabRect(e.Index);
                tabRect.Inflate(-2, -2);
                var closeImage = new Bitmap((Image)Properties.Resources.Close3);
                i = tabPage.gridTag;
                if (i > 0)
                {
                    Color col = Color.LightYellow;   // Selected Color
                    // PJB:Custom Color
                    switch (i)
                    {
                        case 1:         // Selected
                            break;
                        case 2:         // Edit-Mode
                            col = Color.LightPink;
                            break;
                    }
                    e.Graphics.FillRectangle(new SolidBrush(col), e.Bounds);
                }

                TextRenderer.DrawText(e.Graphics, tabPage.Text, tabPage.Font,
                    tabRect, tabPage.ForeColor, TextFormatFlags.Left);
                e.Graphics.DrawImage(closeImage,
                    (tabRect.Right - closeImage.Width),
                    tabRect.Top + (tabRect.Height - closeImage.Height) / 2);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void queryTabDrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                var tabPage = this.queryTabControl.TabPages[e.Index];
                var tabRect = this.queryTabControl.GetTabRect(e.Index);
                tabRect.Inflate(-2, -2);
                if (e.Index == this.queryTabControl.TabCount - 1)
                {
                    var addImage = Properties.Resources.Add;
                    e.Graphics.DrawImage(addImage,
                        tabRect.Left + (tabRect.Width - addImage.Width) / 2,
                        tabRect.Top + (tabRect.Height - addImage.Height) / 2);
                }
                else
                {
                    var closeImage = new Bitmap((Image)Properties.Resources.Close3);
                    e.Graphics.DrawImage(closeImage,
                        (tabRect.Right - closeImage.Width),
                        tabRect.Top + (tabRect.Height - closeImage.Height) / 2);
                    TextRenderer.DrawText(e.Graphics, tabPage.Text, tabPage.Font,
                        tabRect, tabPage.ForeColor, TextFormatFlags.Left);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void resultsTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (resultsTabControl.TabPages.Count > 0)
            {
                for (int i = 0; i < resultsTabControl.TabPages.Count; i++)
                {
                    if ((((ResultsTabPage)resultsTabControl.TabPages[i]).gridTag) == 1)
                        (((ResultsTabPage)resultsTabControl.TabPages[i]).gridTag) = 0;
                }

                if (resultsTabControl.SelectedIndex >= 0)
                {
                    ResultsTabPage resultsPage = (ResultsTabPage)resultsTabControl.TabPages[resultsTabControl.SelectedIndex];
                    if (resultsPage.gridTag == 0)
                        resultsPage.gridTag = 1;
                    if (resultsPage.queryTabPage != null)
                        queryTabControl.SelectedTab = resultsPage.queryTabPage;
                    resultsTabControl.Invalidate();
                }
            }
        }

        private void databaseTableView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            databaseTableView.SelectedNode = e.Node;
        }

        public void ExecuteQuery()
        {
            if ((queryTabControl.TabCount > 0) && (connectionID >=0 ))
            {
                executeFromPSMenu(((PowerfulSample)(queryTabControl.SelectedTab.Controls[0])));
            }
        }

        private void btnExecuteSQL_Click(object sender, EventArgs e)
        {
            if ((queryTabControl.TabCount > 0) && (connectionID >= 0) && (queryTabControl.SelectedTab.Controls.Count> 0))
            {
                executeFromPSMenu(((PowerfulSample)(queryTabControl.SelectedTab.Controls[0])));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectDatabase();
            SetButtonStatus();
        }


        private void databaseTableView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            int i = 0;
            int saveCurrentConnectionID = connectionID;
            TreeNode node = e.Node;
            while (node.Level > 2)
                node = node.Parent;
            if (node.Level == 2)
            {
                TagConnection tag = (TagConnection)node.Tag;
                dbNameCombo.Text = tag.databaseName;
                MSSInstanceCombo.Text = tag.hostname;
                userTextBox.Text = "";
                pwdTextBox.Text = "";
                portTextBox.Text = tag.port.ToString();
                comboDBType.SelectedIndex = tag.databaseType - 1;
                foreach (TreeNode tn in databaseTableView.Nodes[0].Nodes[0].Nodes)
                {
                    if (tn.Text == node.Text)
                        connectionID = i;
                    tn.ForeColor = SystemColors.WindowText;
                    tn.BackColor = SystemColors.Window;
                    i++;
                }
                //node.BackColor = Color.Blue;
                //node.ForeColor = Color.White;
                mnuTreeDisconnect.Text = "Disconnect [" + node.Text + "]";
            }
            // Check if the database has changed
            // If so, then re-load the context sensitive help
            if (saveCurrentConnectionID != connectionID)
            {

            }
        }

        public TagConnection GetConnectionTag()
        {
            if (connectionID >=0)
                return (TagConnection)(databaseTableView.Nodes[0].Nodes[0].Nodes[connectionID].Tag);
            else
                return null;
        }

        private void saveDatabaseConfiguration_Click(object sender, EventArgs e)
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog fdlg = new SaveFileDialog();
            fdlg.FileName = "DBConfig.dbc";
            fdlg.Filter = "DB Config|*.dbc|All Files (*.*)|*.*";
            fdlg.Title = "Select File Name to Save";
            fdlg.OverwritePrompt = true;

            // If the file name is not an empty string open it for saving.
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                if (fdlg.FileName != "")
                {
                    IFormatter formatter = new BinaryFormatter();
                    TagConnection2 tagDest = new TagConnection2();
                    tagDest.tooolsWindowEnabled = bEnableToolsWindow;
                    tagDest.statusWindowEnabled = bEnableStatusWindow;
                    tagDest.qrytabs = new List<QueryTab>();
                    for (int i = 0; i < queryTabControl.TabCount; i++)
                    {
                        if (queryTabControl.TabPages[i].Controls.Count > 0)
                        {
                            tagDest.qrytabs.Add(new QueryTab(queryTabControl.TabPages[i].Text,
                            ((PowerfulSample)(queryTabControl.TabPages[i].Controls[0])).GetText()));
                        }
                    }

                    using (Stream sw = new FileStream(fdlg.FileName, FileMode.Create, FileAccess.Write))
                    {
                        if (databaseTableView.Nodes[0].Nodes[0].Nodes.Count > 0)
                        {
                            for (int i = 0; i < databaseTableView.Nodes[0].Nodes[0].Nodes.Count; i++)
                            {
                                TagConnection tagSrc = (TagConnection)(databaseTableView.Nodes[0].Nodes[0].Nodes[i].Tag);
                                tagDest.connection = tagSrc.connection;
                                //tagDest.connection = splitForm.aesEncrypt.Encrypt(tagSrc.connection);
                                tagDest.engine = tagSrc.engine;
                                tagDest.hostname = tagSrc.hostname;
                                tagDest.databaseName = tagSrc.databaseName;
                                tagDest.tablename = tagSrc.tablename;
                                tagDest.resultsHeader = tagSrc.resultsHeader;
                                tagDest.databaseType = tagSrc.databaseType; 
                                tagDest.port = tagSrc.port;
                                formatter.Serialize(sw, tagDest);
                            }
                        }
                        else        // Write a dummy record so that it does not blow up when we re-serialize
                        {
                            tagDest.connection = "";
                            tagDest.engine = "";
                            tagDest.hostname = "";
                            tagDest.databaseName = "";
                            tagDest.tablename = "";
                            tagDest.resultsHeader = "";
                            tagDest.databaseType = -1;
                            formatter.Serialize(sw, tagDest);
                        }
                    }
                }
            }
            SetButtonStatus();
        }

        private void loadDatabaseConfiguration_Click(object sender, EventArgs e)
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.FileName = "DBConfig.dbc";
            fdlg.Filter = "DB Config|*.dbc|All Files (*.*)|*.*";
            fdlg.Title = "Select File Name to Save";

            // If the file name is not an empty string open it for saving.
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                if (fdlg.FileName != "")
                {
                    ClearDatabaseTreeView();

                    IFormatter formatter = new BinaryFormatter();
                    TagConnection2 tagSrc = new TagConnection2();
                    using (FileStream sw = new FileStream(fdlg.FileName, FileMode.Open, FileAccess.Read))
                    {
                        while (true)
                        {
                            try
                            {
                                tagSrc = (TagConnection2)formatter.Deserialize(sw);
                            }

                            catch (SerializationException err)
                            {
                                MessageBox.Show("Error:This does not appear to be a valid Configuration File.\n\n" + err.ToString(), "Load Configuration");
                                break;
                            }
                            TagConnection tagDest = new TagConnection();
                            tagDest.connection = tagSrc.connection;
                            //tagDest.connection = splitForm.aesEncrypt.Decrypt(tagSrc.connection);
                            tagDest.engine = tagSrc.engine;
                            tagDest.hostname = tagSrc.hostname;
                            tagDest.databaseName = tagSrc.databaseName;
                            tagDest.tablename = tagSrc.tablename;
                            tagDest.resultsHeader = tagSrc.resultsHeader;
                            tagDest.databaseType = tagSrc.databaseType;
                            tagDest.port = tagSrc.port;
                            switch (tagDest.databaseType)
                            {
                                case SYBASE_DB:
                                    if (tagDest.port == 0)
                                        tagDest.port = 2638;
                                    break;
                                case MSS_DB:
                                    if (tagDest.port == 0)
                                        tagDest.port = 1433;                                    
                                        break;
                                case SQLITE_DB:
                                    tagDest.port = 0;
                                    break;
                                case MYSQL_DB:
                                    if (tagDest.port == 0)
                                        tagDest.port = 3306;                                    
                                        break;
                                default:
                                    break;
                            }
                            if (tagDest.databaseType > 0)
                                ConnectSpecificDatabase(tagDest);
                            
                            if (sw.Length == sw.Position)
                                break;
                        }
                    }
                    this.bEnableToolsWindow = tagSrc.tooolsWindowEnabled;
                    this.bEnableStatusWindow = tagSrc.statusWindowEnabled;
                    resetQueries();
                    int i = 0;
                    if (tagSrc.qrytabs.Count > 0)
                    {
                        for (; i < tagSrc.qrytabs.Count; i++)
                        {
                            queryTabAddPage(null, tagSrc.qrytabs[i].name, tagSrc.qrytabs[i].text);
                            
                        }
                    }
                    queryTabAddEmptyPage();
                    queryTabControl.SelectedIndex = 0;
                }
            }
            SetButtonStatus();
        }

        private void EnableMySql(bool bEnable)
        {
            comboDBType.Items.Clear();
            comboDBType.Items.Add("Sybase");
            comboDBType.Items.Add("SQL Server");
            comboDBType.Items.Add("SQLite");

            if (bEnable)
                comboDBType.Items.Add("MySql");
            comboDBType.SelectedIndex = 0;
            SetButtonStatus();
        }
        private void printPreviewGridMenuItem_Click(object sender, EventArgs e)
        {
            printDataGrid(true);
        }

        private void printGrid_Click(object sender, EventArgs e)
        {
            printDataGrid(false);
        }

        // Use DGVPrinter (better formatting)
        private void printDataGrid(bool bPreview)
        {
            splitForm.globalPrintSettings.title = resultsTabControl.TabPages[resultsTabControl.SelectedIndex].Name;
            splitForm.globalPrintSettings.subTitle = resultsTabControl.TabPages[resultsTabControl.SelectedIndex].ToolTipText;

            GlobalPrinterSettings settings = new GlobalPrinterSettings(splitForm);
            if (settings.ShowDialog() == DialogResult.OK)
            {
                DGVPrinter printer = new DGVPrinter(splitForm.globalPrintSettings);
                ResultsTabPage tabPage = (ResultsTabPage)(resultsTabControl.TabPages[resultsTabControl.SelectedIndex]);
                if (bPreview)
                    printer.PrintPreviewDataGridView((DataGridView)tabPage.Controls[0]);
                else
                    printer.PrintDataGridView((DataGridView)tabPage.Controls[0]);
            }
        }

        private void disconnectAlltoolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeviewDB_DisconnectAll(sender, e);
        }

        private void viewForeignKeysForTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tablename = databaseTableView.SelectedNode.Text.Trim();
            TagConnection cnn = GetConnectionTag();
            if (cnn != null)
            {
                cnn.tablename = databaseTableView.SelectedNode.Text.Trim();
                if ((cnn.databaseType == SYBASE_DB) || (cnn.databaseType == MSS_DB))
                    GetSchema(cnn, "foreignkeys");
                else
                    GetSchemaCollection(4, GetConnectionTag());
            }
        }
    }

    public class WaitDlgParameters
    {
        public string hdr;
        public string msg;
        public Point location;
    };

    public class TableCache
    {
        public TableCache(string name, int itemType)
        {
            tableName = name;
            loaded = false;
            type = itemType;
        }
        public string tableName;    // Name of table
        public bool loaded;         // true=loaded, false=not loaded
        public int type;            // 0=undefined, 1=table.2=View,3=Stored Procedure,4=Trigger
    };

    /// <summary>
    /// This autocomplete item appears after dot
    /// </summary>
    public class MethodAutocompleteItem2 : MethodAutocompleteItem
    {
        string firstPart;
        string lastPart;
        public odbctestForm odbc;
        public MethodAutocompleteItem2(odbctestForm odbcIn, string text)
            : base(text)
        {
            odbc = odbcIn;
            var i = text.LastIndexOf('.');
            if (i < 0)
                firstPart = text;
            else
            {
                firstPart = text.Substring(0, i);
                lastPart = text.Substring(i + 1);
            }
        }

        public override CompareResult Compare(string fragmentText)
        {
            int i = fragmentText.LastIndexOf('.');

            if (i < 0)
            {
                if (firstPart.StartsWith(fragmentText.ToLower()) && string.IsNullOrEmpty(lastPart))
                    return CompareResult.VisibleAndSelected;
            }
            else
            {
                var fragmentFirstPart = fragmentText.Substring(0, i).ToLower();
                var fragmentLastPart = fragmentText.Substring(i + 1).ToLower();


                if (firstPart != fragmentFirstPart)
                    return CompareResult.Hidden;

                if (lastPart != null && lastPart.StartsWith(fragmentLastPart.ToLower()))
                    return CompareResult.VisibleAndSelected;

                if (lastPart != null && lastPart.ToLower().Contains(fragmentLastPart.ToLower()))
                    return CompareResult.Visible;
            }
            return CompareResult.Hidden;
        }

        public override string GetTextForReplace()
        {
            if (lastPart == null)
                return firstPart;

            return firstPart + "." + lastPart;
        }

        public override string ToString()
        {
            if (lastPart == null)
                return firstPart;

            return lastPart;
        }
        /// <summary>
        /// This method is called after item inserted into text
        /// </summary>
        public override void OnSelected(AutocompleteMenu popupMenu, SelectedEventArgs e)
        {
            if (odbc.connectionID >= 0)
            {
                TagConnection cnn = odbc.GetConnectionTag();
                int i = e.Item.Text.LastIndexOf('.');
                if (i < 0)  // Must be a table value and not a column value
                {
                    // See if table already loaded in cache
                    // If so, we do not need to load it again
                    foreach (TableCache item in cnn.tableCache)
                    {
                        if (item.tableName.ToLower() == e.Item.Text.ToLower())
                        {
                            // 0=table.1=View,2=Stored Procedure,3=Trigger
                            if ((!item.loaded) && (item.type == 0 || item.type == 1))
                            {
                                item.loaded = true;
                                odbc.AddSchemaNames(item.tableName.ToLower());
                            }
                            else
                                break;
                        }
                    }
                }
            }
        }
    }

    public partial class ResultsTabPage : TabPage
    {
        public OdbcConnection cnnODBC;
        public DataTable dt;
        public OdbcDataAdapter adapter;
        public OdbcCommandBuilder builder;

        public SQLiteConnection SQLitecnnODBC;
        public SQLiteDataAdapter SQLiteadapter;
        public SQLiteCommandBuilder SQLitebuilder;

        public MySqlConnection mysqlcnnODBC;
        public MySqlDataAdapter mysqladapter;
        public MySqlCommandBuilder mysqlbuilder;

        public TabPage queryTabPage;
        public string tip;
        public bool AllowEdit { get; set; }
        public const int SYBASE_DB = 1;
        public const int MSS_DB = 2;
        public const int SQLITE_DB = 3;
        public const int MYSQL_DB = 4;
        public int databaseType;
        public int gridTag;
        public ResultsTabPage(bool bAllowEdit, TabPage tabpage, string tipText)
        {
            AllowEdit = bAllowEdit;
            queryTabPage = tabpage;
            if (tipText.Length < 256)
                tip = tipText;
            else
                tip = tipText.Substring(0, 256)+"....";
        }

        public int SaveEditResults()
        {
            if (AllowEdit)
            {
                DataTable changeTable = dt.GetChanges();
                if (changeTable != null)
                {
                    try
                    {
                        switch (databaseType)
                        {
                            case SYBASE_DB:
                            case MSS_DB:
                                adapter.UpdateCommand = builder.GetUpdateCommand();
                                adapter.Update(dt);
                                break;
                            case MYSQL_DB:
                                mysqladapter.UpdateCommand = mysqlbuilder.GetUpdateCommand();
                                mysqladapter.Update(dt);
                                break;
                            case SQLITE_DB:
                                SQLiteadapter.UpdateCommand = SQLitebuilder.GetUpdateCommand();
                                SQLiteadapter.Update(dt);
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        if (MessageBox.Show(e.Message.ToString() + "\nDo you wish to close the Edit Window?", "Update Error", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            return 0;
                        else
                            return -1;
                    }
                }
            }
            return 0;
        }

        public int CloseAndSave()
        {
            int bReturnCode = 0;
            if (AllowEdit)
            {
                DataTable changeTable = dt.GetChanges();
                if (changeTable != null)
                {
                    DialogResult rsp = MessageBox.Show("\nDo you wish to save changes?", "Close Edit Window", MessageBoxButtons.YesNoCancel);
                    switch (rsp)
                    {
                        case DialogResult.Yes:
                            try
                            {
                                switch (databaseType)
                                {
                                    case SYBASE_DB:
                                    case MSS_DB:
                                        adapter.UpdateCommand = builder.GetUpdateCommand();
                                        adapter.Update(dt);
                                        break;
                                    case MYSQL_DB:
                                        mysqladapter.UpdateCommand = mysqlbuilder.GetUpdateCommand();
                                        mysqladapter.Update(dt);
                                        break;
                                    case SQLITE_DB:
                                        SQLiteadapter.UpdateCommand = SQLitebuilder.GetUpdateCommand();
                                        SQLiteadapter.Update(dt);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            catch (Exception e)
                            {
                                if (MessageBox.Show(e.Message.ToString() + "\nDo you wish to close the Edit Window?", "Update Error", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                    bReturnCode = 0;
                                else
                                    bReturnCode = -1;
                            }
                            break;
                        case DialogResult.No:
                            bReturnCode = 0;
                            break;
                        case DialogResult.Cancel:
                            bReturnCode = -1;
                            break;
                    }
                }

                if (bReturnCode == 0)
                {
                    switch (databaseType)
                    {
                        case SYBASE_DB:
                        case MSS_DB:
                            adapter.Dispose();
                            cnnODBC.Close();    // Close Connection
                            break;
                        case MYSQL_DB:
                            mysqladapter.Dispose();
                            mysqlcnnODBC.Close();    // Close Connection
                            break;
                        case SQLITE_DB:
                            SQLiteadapter.Dispose();
                            SQLitecnnODBC.Close();    // Close Connection
                            break;
                        default:
                            break;
                    }
                }
            }
            return bReturnCode;
        }
    }

    [Serializable]
    public class QueryTab
    {
        public string name;
        public string text;
        public QueryTab()
        {
        }
        public QueryTab(string hdrName, string body)
        {
            name=hdrName;
            text = body;
        }
    }

    [Serializable]
    public class TagConnection2
    {
        public string connection;
        public string engine;
        public string hostname;
        public string databaseName;
        public string tablename;
        public string resultsHeader;    // Custom header - use this if defined
        public int databaseType;
        public bool tooolsWindowEnabled;
        public bool statusWindowEnabled;
        public int port;
        public List<QueryTab> qrytabs;
    }

    public class TagConnection
    {
        public string connection;
        public string engine;
        public string hostname;
        public string databaseName;
        public string tablename;
        public string resultsHeader;    // Custom header - use this if defined
        public int databaseType;
        public int port;

        // AUTOCOMPLETE CODE
        public List<AutocompleteItem> autocompleteItems = new List<AutocompleteItem>();
        public List<TableCache> tableCache = new List<TableCache>();
    };
}
