﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class UDPListenerForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public ToolTip toolTip1 = new ToolTip();
        private bool formLoaded = false;

        public UDPListenerForm()
        {
            InitializeComponent();
        }
        public UDPListenerForm(SplitForm sf)
        {
       
            this.splitForm = sf;
            InitializeComponent();
            loadForm(sf);
        }

        public void loadForm(SplitForm sf)
        {
            udpFoldertxt.Text = splitForm.iniConfig.inifileData.UDP_Folder;
            udpTokentxt.Text = splitForm.iniConfig.inifileData.UDP_Token;
            udpUrltxt.Text = splitForm.iniConfig.inifileData.UDP_Url;
            if (Util.ParseValue(splitForm.iniConfig.inifileData.UDP_SaveUDPRSP) > 0)
                cbx_SaveUDPRsp.Checked = true;
            else
                cbx_SaveUDPRsp.Checked = false;

            if (Util.ParseValue(splitForm.iniConfig.inifileData.UDP_SaveUDPXML) > 0)
                cbx_SaveUDPXml.Checked = true;
            else
                cbx_SaveUDPXml.Checked = false;

            if (Util.ParseValue(splitForm.iniConfig.inifileData.UDP_TestOnly) > 0)
                cbx_UDPTestOnly.Checked = true;
            else
                cbx_UDPTestOnly.Checked = false;
            this.debugLevel.Value = Util.ParseValue(splitForm.iniConfig.inifileData.UDP_DebugLevel);
            formLoaded = true;
        }

        public void saveForm(Object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.UDP_Folder = udpFoldertxt.Text;
                splitForm.iniConfig.inifileData.UDP_Token = udpTokentxt.Text;
                splitForm.iniConfig.inifileData.UDP_Url = udpUrltxt.Text;
                splitForm.iniConfig.inifileData.UDP_SaveUDPRSP = (cbx_SaveUDPRsp.Checked ? "1" : "0");
                splitForm.iniConfig.inifileData.UDP_SaveUDPXML = (cbx_SaveUDPXml.Checked ? "1" : "0");
                splitForm.iniConfig.inifileData.UDP_TestOnly = (cbx_UDPTestOnly.Checked ? "1" : "0");
                splitForm.iniConfig.inifileData.UDP_DebugLevel = debugLevel.Value.ToString();
                splitForm.SetChanged();
            }
        }


        private void udpFolderBrowseBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openUDPFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            openUDPFolderDialog.SelectedPath = splitForm.globalSettings.gfiPath + "\\cnf";
            if (openUDPFolderDialog.ShowDialog() == DialogResult.OK)
                udpFoldertxt.Text = openUDPFolderDialog.SelectedPath;
        }

        private void wifiSettingsBtn_Click(object sender, EventArgs e)
        {
            splitForm.OpenFormByName("wifi general settings");
        }
    }
}
