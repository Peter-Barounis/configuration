﻿namespace ConfigurationWinForm
{
    partial class NetConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.ipAddressControl17 = new IPAddressControlLib.IPAddressControl();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.ipAddressControl15 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl14 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl13 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl12 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl11 = new IPAddressControlLib.IPAddressControl();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ipAddressControl10 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl9 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl8 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl7 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl6 = new IPAddressControlLib.IPAddressControl();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.ipAddressControl5 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl4 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl3 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl2 = new IPAddressControlLib.IPAddressControl();
            this.ipAddressControl1 = new IPAddressControlLib.IPAddressControl();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.gfipAddressControl = new IPAddressControlLib.IPAddressControl();
            this.label6 = new System.Windows.Forms.Label();
            this.gctcpPortTextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.groupBox10);
            this.groupBox1.Controls.Add(this.groupBox9);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(844, 907);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(13, 0);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(94, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Netconfig Settings";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label35);
            this.groupBox10.Controls.Add(this.textBox8);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.comboBox11);
            this.groupBox10.Controls.Add(this.label116);
            this.groupBox10.Controls.Add(this.textBox94);
            this.groupBox10.Controls.Add(this.textBox95);
            this.groupBox10.Controls.Add(this.textBox96);
            this.groupBox10.Controls.Add(this.textBox97);
            this.groupBox10.Controls.Add(this.textBox98);
            this.groupBox10.Controls.Add(this.textBox93);
            this.groupBox10.Controls.Add(this.label115);
            this.groupBox10.Controls.Add(this.label114);
            this.groupBox10.Controls.Add(this.label113);
            this.groupBox10.Controls.Add(this.label112);
            this.groupBox10.Controls.Add(this.label111);
            this.groupBox10.Controls.Add(this.label110);
            this.groupBox10.Controls.Add(this.textBox92);
            this.groupBox10.Controls.Add(this.textBox91);
            this.groupBox10.Controls.Add(this.textBox90);
            this.groupBox10.Controls.Add(this.textBox89);
            this.groupBox10.Controls.Add(this.textBox88);
            this.groupBox10.Controls.Add(this.textBox87);
            this.groupBox10.Controls.Add(this.label109);
            this.groupBox10.Controls.Add(this.label108);
            this.groupBox10.Controls.Add(this.label107);
            this.groupBox10.Controls.Add(this.label106);
            this.groupBox10.Controls.Add(this.label105);
            this.groupBox10.Controls.Add(this.label104);
            this.groupBox10.Location = new System.Drawing.Point(14, 671);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox10.Size = new System.Drawing.Size(806, 168);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 0);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(138, 13);
            this.label35.TabIndex = 96;
            this.label35.Text = "Cloud Configuration Section";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(557, 120);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(243, 20);
            this.textBox8.TabIndex = 11;
            this.textBox8.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(414, 128);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 13);
            this.label26.TabIndex = 95;
            this.label26.Text = "SW Upgrade Path:";
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "Enabled",
            "Disabled"});
            this.comboBox11.Location = new System.Drawing.Point(118, 144);
            this.comboBox11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(72, 21);
            this.comboBox11.TabIndex = 12;
            this.comboBox11.Text = "Enabled";
            this.comboBox11.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 148);
            this.label116.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(106, 13);
            this.label116.TabIndex = 94;
            this.label116.Text = "GenLink Http Status:";
            // 
            // textBox94
            // 
            this.textBox94.Location = new System.Drawing.Point(556, 99);
            this.textBox94.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(246, 20);
            this.textBox94.TabIndex = 9;
            this.textBox94.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox95
            // 
            this.textBox95.Location = new System.Drawing.Point(556, 78);
            this.textBox95.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(247, 20);
            this.textBox95.TabIndex = 7;
            this.textBox95.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox96
            // 
            this.textBox96.Location = new System.Drawing.Point(556, 57);
            this.textBox96.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(247, 20);
            this.textBox96.TabIndex = 5;
            this.textBox96.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox97
            // 
            this.textBox97.Location = new System.Drawing.Point(556, 36);
            this.textBox97.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(247, 20);
            this.textBox97.TabIndex = 3;
            this.textBox97.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox98
            // 
            this.textBox98.Location = new System.Drawing.Point(556, 15);
            this.textBox98.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(247, 20);
            this.textBox98.TabIndex = 1;
            this.textBox98.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox93
            // 
            this.textBox93.Location = new System.Drawing.Point(346, 144);
            this.textBox93.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(42, 20);
            this.textBox93.TabIndex = 13;
            this.textBox93.Text = "0xFFFF";
            this.textBox93.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(224, 144);
            this.label115.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(111, 13);
            this.label115.TabIndex = 28;
            this.label115.Text = "Capability flag (future):";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(413, 103);
            this.label114.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(101, 13);
            this.label114.TabIndex = 27;
            this.label114.Text = "Software credential:";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(413, 82);
            this.label113.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(122, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "SW upgrade from GDS :";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(413, 61);
            this.label112.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(87, 13);
            this.label112.TabIndex = 25;
            this.label112.Text = "GenLink Tenant:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(413, 40);
            this.label111.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(111, 13);
            this.label111.TabIndex = 24;
            this.label111.Text = "GenLink environment:";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(413, 19);
            this.label110.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(133, 13);
            this.label110.TabIndex = 23;
            this.label110.Text = "GenLink device auth path:";
            // 
            // textBox92
            // 
            this.textBox92.Location = new System.Drawing.Point(151, 122);
            this.textBox92.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(247, 20);
            this.textBox92.TabIndex = 10;
            this.textBox92.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox91
            // 
            this.textBox91.Location = new System.Drawing.Point(151, 101);
            this.textBox91.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(247, 20);
            this.textBox91.TabIndex = 8;
            this.textBox91.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox90
            // 
            this.textBox90.Location = new System.Drawing.Point(151, 80);
            this.textBox90.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(247, 20);
            this.textBox90.TabIndex = 6;
            this.textBox90.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox89
            // 
            this.textBox89.Location = new System.Drawing.Point(151, 58);
            this.textBox89.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(247, 20);
            this.textBox89.TabIndex = 4;
            this.textBox89.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox88
            // 
            this.textBox88.Location = new System.Drawing.Point(151, 37);
            this.textBox88.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(247, 20);
            this.textBox88.TabIndex = 2;
            this.textBox88.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox87
            // 
            this.textBox87.Location = new System.Drawing.Point(151, 16);
            this.textBox87.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(247, 20);
            this.textBox87.TabIndex = 0;
            this.textBox87.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(4, 124);
            this.label109.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(128, 13);
            this.label109.TabIndex = 5;
            this.label109.Text = "GenLink setup credential:";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(4, 103);
            this.label108.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(119, 13);
            this.label108.TabIndex = 4;
            this.label108.Text = "GenLink setup file path:";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(4, 82);
            this.label107.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(102, 13);
            this.label107.TabIndex = 3;
            this.label107.Text = "GenLink host name:";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(4, 61);
            this.label106.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(123, 13);
            this.label106.TabIndex = 2;
            this.label106.Text = "Mobile Ticket credential:";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(4, 40);
            this.label105.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(114, 13);
            this.label105.TabIndex = 1;
            this.label105.Text = "Mobile Ticket file path:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(4, 19);
            this.label104.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(128, 13);
            this.label104.TabIndex = 0;
            this.label104.Text = "Moblile Ticket host name:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Controls.Add(this.textBox7);
            this.groupBox9.Controls.Add(this.ipAddressControl17);
            this.groupBox9.Controls.Add(this.textBox86);
            this.groupBox9.Controls.Add(this.textBox85);
            this.groupBox9.Controls.Add(this.comboBox10);
            this.groupBox9.Controls.Add(this.textBox84);
            this.groupBox9.Controls.Add(this.comboBox9);
            this.groupBox9.Controls.Add(this.label103);
            this.groupBox9.Controls.Add(this.label102);
            this.groupBox9.Controls.Add(this.label101);
            this.groupBox9.Controls.Add(this.label100);
            this.groupBox9.Controls.Add(this.label99);
            this.groupBox9.Controls.Add(this.label98);
            this.groupBox9.Location = new System.Drawing.Point(16, 583);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox9.Size = new System.Drawing.Size(806, 83);
            this.groupBox9.TabIndex = 7;
            this.groupBox9.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 0);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(179, 13);
            this.label34.TabIndex = 82;
            this.label34.Text = "GPS Receiver Configuration Section";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(381, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "GPS Modem Status";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(493, 57);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 7;
            this.textBox7.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl17
            // 
            this.ipAddressControl17.AllowInternalTab = false;
            this.ipAddressControl17.AutoHeight = true;
            this.ipAddressControl17.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl17.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl17.Location = new System.Drawing.Point(24, 23);
            this.ipAddressControl17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl17.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl17.Name = "ipAddressControl17";
            this.ipAddressControl17.ReadOnly = false;
            this.ipAddressControl17.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl17.TabIndex = 0;
            this.ipAddressControl17.Text = "...";
            this.ipAddressControl17.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox86
            // 
            this.textBox86.Location = new System.Drawing.Point(317, 57);
            this.textBox86.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(43, 20);
            this.textBox86.TabIndex = 6;
            this.textBox86.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox85
            // 
            this.textBox85.Location = new System.Drawing.Point(122, 57);
            this.textBox85.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(43, 20);
            this.textBox85.TabIndex = 4;
            this.textBox85.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "NMEA",
            "TAIP"});
            this.comboBox10.Location = new System.Drawing.Point(494, 22);
            this.comboBox10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(55, 21);
            this.comboBox10.TabIndex = 3;
            this.comboBox10.Text = "NMEA";
            this.comboBox10.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox84
            // 
            this.textBox84.Location = new System.Drawing.Point(319, 23);
            this.textBox84.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(43, 20);
            this.textBox84.TabIndex = 2;
            this.textBox84.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.comboBox9.Location = new System.Drawing.Point(221, 22);
            this.comboBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(49, 21);
            this.comboBox9.TabIndex = 1;
            this.comboBox9.Text = "TCP";
            this.comboBox9.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(206, 59);
            this.label103.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(112, 13);
            this.label103.TabIndex = 5;
            this.label103.Text = "Max Trans Pos. Delta:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(11, 59);
            this.label102.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(114, 13);
            this.label102.TabIndex = 11;
            this.label102.Text = "Max Trans Time Delta:";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(428, 25);
            this.label101.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(65, 13);
            this.label101.TabIndex = 9;
            this.label101.Text = "Msg Format:";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(290, 25);
            this.label100.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(29, 13);
            this.label100.TabIndex = 7;
            this.label100.Text = "Port:";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(153, 25);
            this.label99.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(69, 13);
            this.label99.TabIndex = 5;
            this.label99.Text = "Net Protocol:";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(5, 25);
            this.label98.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(20, 13);
            this.label98.TabIndex = 81;
            this.label98.Text = "IP:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.textBox6);
            this.groupBox8.Controls.Add(this.textBox99);
            this.groupBox8.Controls.Add(this.comboBox12);
            this.groupBox8.Controls.Add(this.label93);
            this.groupBox8.Controls.Add(this.label92);
            this.groupBox8.Location = new System.Drawing.Point(17, 533);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox8.Size = new System.Drawing.Size(799, 44);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, -2);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(166, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Cell Modem Configuration Section";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(526, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Cell Modem Status";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(621, 18);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 2;
            this.textBox6.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox99
            // 
            this.textBox99.Location = new System.Drawing.Point(448, 18);
            this.textBox99.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(43, 20);
            this.textBox99.TabIndex = 1;
            this.textBox99.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "Inmotion",
            "Cisco",
            "SierraWireless",
            "CradlePoint"});
            this.comboBox12.Location = new System.Drawing.Point(104, 18);
            this.comboBox12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(118, 21);
            this.comboBox12.TabIndex = 0;
            this.comboBox12.Text = "Inmotion";
            this.comboBox12.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(242, 20);
            this.label93.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(188, 13);
            this.label93.TabIndex = 1;
            this.label93.Text = "Cell modem signal threshold (-1 to -70):";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(10, 18);
            this.label92.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(92, 13);
            this.label92.TabIndex = 0;
            this.label92.Text = "Cell Modem Type:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.ipAddressControl15);
            this.groupBox7.Controls.Add(this.ipAddressControl14);
            this.groupBox7.Controls.Add(this.ipAddressControl13);
            this.groupBox7.Controls.Add(this.ipAddressControl12);
            this.groupBox7.Controls.Add(this.ipAddressControl11);
            this.groupBox7.Controls.Add(this.textBox76);
            this.groupBox7.Controls.Add(this.label87);
            this.groupBox7.Controls.Add(this.label82);
            this.groupBox7.Controls.Add(this.label83);
            this.groupBox7.Controls.Add(this.label84);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this.label86);
            this.groupBox7.Controls.Add(this.comboBox8);
            this.groupBox7.Controls.Add(this.label67);
            this.groupBox7.Location = new System.Drawing.Point(16, 422);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox7.Size = new System.Drawing.Size(802, 103);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(9, 0);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(151, 13);
            this.label32.TabIndex = 53;
            this.label32.Text = "Ethernet Configuration Section";
            // 
            // ipAddressControl15
            // 
            this.ipAddressControl15.AllowInternalTab = false;
            this.ipAddressControl15.AutoHeight = true;
            this.ipAddressControl15.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl15.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl15.Location = new System.Drawing.Point(636, 76);
            this.ipAddressControl15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl15.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl15.Name = "ipAddressControl15";
            this.ipAddressControl15.ReadOnly = false;
            this.ipAddressControl15.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl15.TabIndex = 6;
            this.ipAddressControl15.Text = "...";
            this.ipAddressControl15.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl14
            // 
            this.ipAddressControl14.AllowInternalTab = false;
            this.ipAddressControl14.AutoHeight = true;
            this.ipAddressControl14.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl14.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl14.Location = new System.Drawing.Point(442, 76);
            this.ipAddressControl14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl14.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl14.Name = "ipAddressControl14";
            this.ipAddressControl14.ReadOnly = false;
            this.ipAddressControl14.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl14.TabIndex = 5;
            this.ipAddressControl14.Text = "...";
            this.ipAddressControl14.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl13
            // 
            this.ipAddressControl13.AllowInternalTab = false;
            this.ipAddressControl13.AutoHeight = true;
            this.ipAddressControl13.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl13.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl13.Location = new System.Drawing.Point(442, 47);
            this.ipAddressControl13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl13.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl13.Name = "ipAddressControl13";
            this.ipAddressControl13.ReadOnly = false;
            this.ipAddressControl13.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl13.TabIndex = 4;
            this.ipAddressControl13.Text = "...";
            this.ipAddressControl13.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl12
            // 
            this.ipAddressControl12.AllowInternalTab = false;
            this.ipAddressControl12.AutoHeight = true;
            this.ipAddressControl12.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl12.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl12.Location = new System.Drawing.Point(636, 20);
            this.ipAddressControl12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl12.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl12.Name = "ipAddressControl12";
            this.ipAddressControl12.ReadOnly = false;
            this.ipAddressControl12.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl12.TabIndex = 2;
            this.ipAddressControl12.Text = "...";
            this.ipAddressControl12.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl11
            // 
            this.ipAddressControl11.AllowInternalTab = false;
            this.ipAddressControl11.AutoHeight = true;
            this.ipAddressControl11.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl11.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl11.Location = new System.Drawing.Point(442, 20);
            this.ipAddressControl11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl11.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl11.Name = "ipAddressControl11";
            this.ipAddressControl11.ReadOnly = false;
            this.ipAddressControl11.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl11.TabIndex = 1;
            this.ipAddressControl11.Text = "...";
            this.ipAddressControl11.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox76
            // 
            this.textBox76.Location = new System.Drawing.Point(130, 47);
            this.textBox76.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(42, 20);
            this.textBox76.TabIndex = 3;
            this.textBox76.Text = "0xFFFF";
            this.textBox76.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(9, 50);
            this.label87.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(116, 13);
            this.label87.TabIndex = 23;
            this.label87.Text = "Capability flags (future):";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(572, 78);
            this.label82.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(64, 13);
            this.label82.TabIndex = 52;
            this.label82.Text = "[Secondary]";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(314, 78);
            this.label83.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(116, 13);
            this.label83.TabIndex = 51;
            this.label83.Text = "DNS address: [Primary]";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(346, 50);
            this.label84.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(87, 13);
            this.label84.TabIndex = 50;
            this.label84.Text = "Default gateway:";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(564, 22);
            this.label85.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(72, 13);
            this.label85.TabIndex = 49;
            this.label85.Text = "Subnet mask:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(390, 22);
            this.label86.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(43, 13);
            this.label86.TabIndex = 48;
            this.label86.Text = "FBX IP:";
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(82, 19);
            this.comboBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(92, 21);
            this.comboBox8.TabIndex = 0;
            this.comboBox8.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(9, 22);
            this.label67.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(70, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "DHCP Mode:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.ipAddressControl10);
            this.groupBox6.Controls.Add(this.ipAddressControl9);
            this.groupBox6.Controls.Add(this.ipAddressControl8);
            this.groupBox6.Controls.Add(this.ipAddressControl7);
            this.groupBox6.Controls.Add(this.ipAddressControl6);
            this.groupBox6.Controls.Add(this.comboBox6);
            this.groupBox6.Controls.Add(this.comboBox7);
            this.groupBox6.Controls.Add(this.textBox51);
            this.groupBox6.Controls.Add(this.textBox52);
            this.groupBox6.Controls.Add(this.label58);
            this.groupBox6.Controls.Add(this.label59);
            this.groupBox6.Controls.Add(this.label60);
            this.groupBox6.Controls.Add(this.label61);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Controls.Add(this.label63);
            this.groupBox6.Controls.Add(this.label64);
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Location = new System.Drawing.Point(14, 316);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox6.Size = new System.Drawing.Size(804, 101);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 0);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(208, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "WiFi Acces Point #2 Configuration Section";
            // 
            // ipAddressControl10
            // 
            this.ipAddressControl10.AllowInternalTab = false;
            this.ipAddressControl10.AutoHeight = true;
            this.ipAddressControl10.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl10.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl10.Location = new System.Drawing.Point(636, 72);
            this.ipAddressControl10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl10.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl10.Name = "ipAddressControl10";
            this.ipAddressControl10.ReadOnly = false;
            this.ipAddressControl10.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl10.TabIndex = 8;
            this.ipAddressControl10.Text = "...";
            this.ipAddressControl10.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl9
            // 
            this.ipAddressControl9.AllowInternalTab = false;
            this.ipAddressControl9.AutoHeight = true;
            this.ipAddressControl9.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl9.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl9.Location = new System.Drawing.Point(442, 68);
            this.ipAddressControl9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl9.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl9.Name = "ipAddressControl9";
            this.ipAddressControl9.ReadOnly = false;
            this.ipAddressControl9.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl9.TabIndex = 6;
            this.ipAddressControl9.Text = "...";
            this.ipAddressControl9.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl8
            // 
            this.ipAddressControl8.AllowInternalTab = false;
            this.ipAddressControl8.AutoHeight = true;
            this.ipAddressControl8.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl8.Location = new System.Drawing.Point(442, 45);
            this.ipAddressControl8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl8.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl8.Name = "ipAddressControl8";
            this.ipAddressControl8.ReadOnly = false;
            this.ipAddressControl8.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl8.TabIndex = 5;
            this.ipAddressControl8.Text = "...";
            this.ipAddressControl8.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl7
            // 
            this.ipAddressControl7.AllowInternalTab = false;
            this.ipAddressControl7.AutoHeight = true;
            this.ipAddressControl7.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl7.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl7.Location = new System.Drawing.Point(636, 18);
            this.ipAddressControl7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl7.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl7.Name = "ipAddressControl7";
            this.ipAddressControl7.ReadOnly = false;
            this.ipAddressControl7.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl7.TabIndex = 7;
            this.ipAddressControl7.Text = "...";
            this.ipAddressControl7.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl6
            // 
            this.ipAddressControl6.AllowInternalTab = false;
            this.ipAddressControl6.AutoHeight = true;
            this.ipAddressControl6.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl6.Location = new System.Drawing.Point(442, 10);
            this.ipAddressControl6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl6.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl6.Name = "ipAddressControl6";
            this.ipAddressControl6.ReadOnly = false;
            this.ipAddressControl6.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl6.TabIndex = 4;
            this.ipAddressControl6.Text = "...";
            this.ipAddressControl6.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(215, 72);
            this.comboBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(92, 21);
            this.comboBox6.TabIndex = 3;
            this.comboBox6.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(87, 69);
            this.comboBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(92, 21);
            this.comboBox7.TabIndex = 2;
            this.comboBox7.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(53, 19);
            this.textBox51.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(254, 20);
            this.textBox51.TabIndex = 0;
            this.textBox51.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(53, 42);
            this.textBox52.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(254, 20);
            this.textBox52.TabIndex = 1;
            this.textBox52.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(572, 72);
            this.label58.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(64, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "[Secondary]";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(314, 73);
            this.label59.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(116, 13);
            this.label59.TabIndex = 7;
            this.label59.Text = "DNS address: [Primary]";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(346, 47);
            this.label60.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(87, 13);
            this.label60.TabIndex = 6;
            this.label60.Text = "Default gateway:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(564, 22);
            this.label61.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(73, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "Subnet Mask:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(390, 22);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(43, 13);
            this.label62.TabIndex = 4;
            this.label62.Text = "FBX IP:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(182, 75);
            this.label63.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(37, 13);
            this.label63.TabIndex = 3;
            this.label63.Text = "Mode:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(3, 72);
            this.label64.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(71, 13);
            this.label64.TabIndex = 2;
            this.label64.Text = "Security type:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(3, 45);
            this.label65.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(50, 13);
            this.label65.TabIndex = 1;
            this.label65.Text = "Passkey:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(4, 22);
            this.label66.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(35, 13);
            this.label66.TabIndex = 0;
            this.label66.Text = "SSID:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.ipAddressControl5);
            this.groupBox5.Controls.Add(this.ipAddressControl4);
            this.groupBox5.Controls.Add(this.ipAddressControl3);
            this.groupBox5.Controls.Add(this.ipAddressControl2);
            this.groupBox5.Controls.Add(this.ipAddressControl1);
            this.groupBox5.Controls.Add(this.comboBox5);
            this.groupBox5.Controls.Add(this.comboBox4);
            this.groupBox5.Controls.Add(this.textBox18);
            this.groupBox5.Controls.Add(this.textBox17);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(15, 210);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Size = new System.Drawing.Size(804, 100);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(4, 0);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(208, 13);
            this.label30.TabIndex = 9;
            this.label30.Text = "WiFi Acces Point #1 Configuration Section";
            // 
            // ipAddressControl5
            // 
            this.ipAddressControl5.AllowInternalTab = false;
            this.ipAddressControl5.AutoHeight = true;
            this.ipAddressControl5.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl5.Location = new System.Drawing.Point(636, 67);
            this.ipAddressControl5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl5.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl5.Name = "ipAddressControl5";
            this.ipAddressControl5.ReadOnly = false;
            this.ipAddressControl5.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl5.TabIndex = 8;
            this.ipAddressControl5.Text = "...";
            this.ipAddressControl5.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl4
            // 
            this.ipAddressControl4.AllowInternalTab = false;
            this.ipAddressControl4.AutoHeight = true;
            this.ipAddressControl4.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl4.Location = new System.Drawing.Point(636, 24);
            this.ipAddressControl4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl4.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl4.Name = "ipAddressControl4";
            this.ipAddressControl4.ReadOnly = false;
            this.ipAddressControl4.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl4.TabIndex = 7;
            this.ipAddressControl4.Text = "...";
            this.ipAddressControl4.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl3
            // 
            this.ipAddressControl3.AllowInternalTab = false;
            this.ipAddressControl3.AutoHeight = true;
            this.ipAddressControl3.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl3.Location = new System.Drawing.Point(442, 72);
            this.ipAddressControl3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl3.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl3.Name = "ipAddressControl3";
            this.ipAddressControl3.ReadOnly = false;
            this.ipAddressControl3.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl3.TabIndex = 6;
            this.ipAddressControl3.Text = "...";
            this.ipAddressControl3.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl2
            // 
            this.ipAddressControl2.AllowInternalTab = false;
            this.ipAddressControl2.AutoHeight = true;
            this.ipAddressControl2.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl2.Location = new System.Drawing.Point(442, 42);
            this.ipAddressControl2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl2.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl2.Name = "ipAddressControl2";
            this.ipAddressControl2.ReadOnly = false;
            this.ipAddressControl2.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl2.TabIndex = 5;
            this.ipAddressControl2.Text = "...";
            this.ipAddressControl2.TextChanged += new System.EventHandler(this.saveData);
            // 
            // ipAddressControl1
            // 
            this.ipAddressControl1.AllowInternalTab = false;
            this.ipAddressControl1.AutoHeight = true;
            this.ipAddressControl1.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressControl1.Location = new System.Drawing.Point(442, 17);
            this.ipAddressControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ipAddressControl1.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressControl1.Name = "ipAddressControl1";
            this.ipAddressControl1.ReadOnly = false;
            this.ipAddressControl1.Size = new System.Drawing.Size(114, 20);
            this.ipAddressControl1.TabIndex = 4;
            this.ipAddressControl1.Text = "...";
            this.ipAddressControl1.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(214, 68);
            this.comboBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(92, 21);
            this.comboBox5.TabIndex = 3;
            this.comboBox5.TextChanged += new System.EventHandler(this.saveData);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(87, 68);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(92, 21);
            this.comboBox4.TabIndex = 2;
            this.comboBox4.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(52, 42);
            this.textBox18.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(254, 20);
            this.textBox18.TabIndex = 1;
            this.textBox18.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(52, 20);
            this.textBox17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(254, 20);
            this.textBox17.TabIndex = 0;
            this.textBox17.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(572, 72);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = "[Secondary]";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(314, 73);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(116, 13);
            this.label24.TabIndex = 7;
            this.label24.Text = "DNS address: [Primary]";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(346, 46);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Default gateway:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(564, 24);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "Subnet Mask:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(390, 22);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "FBX IP:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(182, 72);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Mode:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 72);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Security Type:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 45);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Passkey:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 22);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "SSID:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.numericUpDown4);
            this.groupBox4.Controls.Add(this.numericUpDown3);
            this.groupBox4.Controls.Add(this.numericUpDown2);
            this.groupBox4.Controls.Add(this.textBox12);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.numericUpDown1);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.comboBox3);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.comboBox2);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(14, 114);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(804, 92);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(4, 0);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(109, 13);
            this.label29.TabIndex = 13;
            this.label29.Text = "WIFI General Section";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(158, 41);
            this.numericUpDown4.Maximum = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.numericUpDown4.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(70, 20);
            this.numericUpDown4.TabIndex = 3;
            this.numericUpDown4.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.numericUpDown4.ValueChanged += new System.EventHandler(this.saveData);
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(429, 42);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            81,
            0,
            0,
            0});
            this.numericUpDown3.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown3.TabIndex = 5;
            this.numericUpDown3.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown3.ValueChanged += new System.EventHandler(this.saveData);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(429, 19);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(60, 20);
            this.numericUpDown2.TabIndex = 1;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.saveData);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(496, 64);
            this.textBox12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(68, 20);
            this.textBox12.TabIndex = 9;
            this.textBox12.Text = "0xFFFF";
            this.textBox12.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(242, 67);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(247, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Capability Flags for future implementation purposes:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(158, 64);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(70, 20);
            this.numericUpDown1.TabIndex = 7;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.saveData);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 67);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(139, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Wifi AP configuration count:";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "IBSS_JOINER",
            "IBSS_CREATOR"});
            this.comboBox3.Location = new System.Drawing.Point(596, 41);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 6;
            this.comboBox3.Text = "IBSS_JOINER";
            this.toolTip1.SetToolTip(this.comboBox3, "IBBS Mode not implemented. This is for future implementation purpose.");
            this.comboBox3.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(496, 44);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "IBBS Mode:";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "INFRASTRUCTURE",
            "IBSS_OPEN",
            "IBBS_SECURITY",
            "WIFI_DIRECT"});
            this.comboBox2.Location = new System.Drawing.Point(596, 19);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 2;
            this.comboBox2.Text = "INFRASTRUCTURE";
            this.toolTip1.SetToolTip(this.comboBox2, "Wifi network modes - only INFRASTRUCTURE mode is supported.\r\nRemaining modes are " +
        "for future implementation purpose.");
            this.comboBox2.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(496, 21);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Wifi network mode:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(242, 46);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(143, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Max. RSSI threshold (50-80):";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 44);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Min. RSI threshold (40-70):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(242, 21);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(176, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "AP scan timeout in minutes(1-1440):";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Enabled",
            "Disabled"});
            this.comboBox1.Location = new System.Drawing.Point(158, 18);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(71, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Text = "Enabled";
            this.toolTip1.SetToolTip(this.comboBox1, "ENABLED - Farebox will connect with an AP with mentioned Inservice / OutOfservice" +
        " Interval duration\r\nDISABLED - Farebox remains stay connected with an AP after p" +
        "robing\r\n");
            this.comboBox1.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 21);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "AP connection interval status:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gctcpPortTextBox);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.gfipAddressControl);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(12, 63);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(804, 46);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 0);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "GDS Section";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(87, 15);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(384, 20);
            this.textBox5.TabIndex = 0;
            this.toolTip1.SetToolTip(this.textBox5, "Garage Data system host name (64 character max)\r\n");
            this.textBox5.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 18);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "GC host name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 16);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(801, 41);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(83, 13);
            this.label27.TabIndex = 5;
            this.label27.Text = "General Section";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(634, 16);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(43, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(479, 16);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(43, 20);
            this.textBox3.TabIndex = 2;
            this.toolTip1.SetToolTip(this.textBox3, "Farebox Out Of Service probe interval duration in minutes, max upto 24 hrs");
            this.textBox3.TextChanged += new System.EventHandler(this.saveData);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(253, 16);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(43, 20);
            this.textBox2.TabIndex = 1;
            this.toolTip1.SetToolTip(this.textBox2, "Farebox In service probe interval duration in minutes, max upto 24 hrs");
            this.textBox2.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(556, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Probe timeout:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(330, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Out-Of-Service probe interval:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "In-Service probe interval:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(51, 16);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(43, 20);
            this.textBox1.TabIndex = 0;
            this.toolTip1.SetToolTip(this.textBox1, "Version number for NetConfig file. Maximum of 3 digit length supported \r\nfor majo" +
        "r and minor number.E.g., 1.0, 999.999");
            this.textBox1.TextChanged += new System.EventHandler(this.saveData);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Version:";
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // gfipAddressControl
            // 
            this.gfipAddressControl.AllowInternalTab = false;
            this.gfipAddressControl.AutoHeight = true;
            this.gfipAddressControl.BackColor = System.Drawing.SystemColors.Window;
            this.gfipAddressControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gfipAddressControl.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.gfipAddressControl.Location = new System.Drawing.Point(524, 16);
            this.gfipAddressControl.MinimumSize = new System.Drawing.Size(87, 20);
            this.gfipAddressControl.Name = "gfipAddressControl";
            this.gfipAddressControl.ReadOnly = false;
            this.gfipAddressControl.Size = new System.Drawing.Size(87, 20);
            this.gfipAddressControl.TabIndex = 32;
            this.gfipAddressControl.Text = "...";
            this.gfipAddressControl.TextChanged += new System.EventHandler(this.gfipAddressControl_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(486, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "GCIP:";
            // 
            // gctcpPortTextBox
            // 
            this.gctcpPortTextBox.Location = new System.Drawing.Point(704, 16);
            this.gctcpPortTextBox.Name = "gctcpPortTextBox";
            this.gctcpPortTextBox.Size = new System.Drawing.Size(87, 20);
            this.gctcpPortTextBox.TabIndex = 34;
            this.gctcpPortTextBox.TextChanged += new System.EventHandler(this.gctcpPortTextBox_TextChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(640, 20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(65, 13);
            this.label37.TabIndex = 33;
            this.label37.Text = "GCTCPPort:";
            // 
            // NetConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 860);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NetConfigForm";
            this.Text = "NetConfig (FF-FFE)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.ComboBox comboBox12;
        private IPAddressControlLib.IPAddressControl ipAddressControl1;
        private IPAddressControlLib.IPAddressControl ipAddressControl2;
        private IPAddressControlLib.IPAddressControl ipAddressControl15;
        private IPAddressControlLib.IPAddressControl ipAddressControl14;
        private IPAddressControlLib.IPAddressControl ipAddressControl13;
        private IPAddressControlLib.IPAddressControl ipAddressControl12;
        private IPAddressControlLib.IPAddressControl ipAddressControl11;
        private IPAddressControlLib.IPAddressControl ipAddressControl10;
        private IPAddressControlLib.IPAddressControl ipAddressControl9;
        private IPAddressControlLib.IPAddressControl ipAddressControl8;
        private IPAddressControlLib.IPAddressControl ipAddressControl7;
        private IPAddressControlLib.IPAddressControl ipAddressControl6;
        private IPAddressControlLib.IPAddressControl ipAddressControl5;
        private IPAddressControlLib.IPAddressControl ipAddressControl4;
        private IPAddressControlLib.IPAddressControl ipAddressControl3;
        private IPAddressControlLib.IPAddressControl ipAddressControl17;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private IPAddressControlLib.IPAddressControl gfipAddressControl;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox gctcpPortTextBox;
        private System.Windows.Forms.Label label37;

    }
}