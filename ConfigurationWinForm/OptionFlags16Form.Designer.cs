﻿namespace ConfigurationWinForm
{
    partial class OptionFlags16Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(23, 207);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(582, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Disable bill validator when it reaches the “Bill Full” value. (Odyssey /" +
    " Fast Fare)";
            this.toolTip2.SetToolTip(this.checkBox13, "Disable bill validator when it reaches the “Bill Full” value. \r\n(Odyssey / Fast F" +
        "are) 0 0 This uses the \"BillFull\" value defined in Farebox Configuration Options" +
        " above. ");
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label3);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown3);
            this.optionflagsGridTitle.Controls.Add(this.label2);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown2);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(813, 349);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(104, 147);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(521, 34);
            this.label3.TabIndex = 22;
            this.label3.Text = "0x0F00 - Change absolute expiration date on RIDE cards. Value is in months. 0 0 \r" +
    "\n0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 months \r\n";
            this.toolTip2.SetToolTip(this.label3, "0x0F00 - Change absolute expiration date on RIDE cards. Value is in months. 0 0 \r" +
        "\n0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 months \r\n\r" +
        "\n");
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(23, 149);
            this.numericUpDown3.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown3.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(72, 22);
            this.numericUpDown3.TabIndex = 21;
            this.toolTip2.SetToolTip(this.numericUpDown3, "0x00F0 - Change absolute expiration date on VALUE cards. \r\nValue is in months 0 0" +
        " 0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 months \r\n");
            this.numericUpDown3.ValueChanged += new System.EventHandler(this.numericUpDown3_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(104, 85);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(642, 34);
            this.label2.TabIndex = 20;
            this.label2.Text = "0x0010 - 0x00F0 : Change absolute expiration date on VALUE cards. \r\nValue is in m" +
    "onths 0 0 0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 m" +
    "onths \r\n";
            this.toolTip2.SetToolTip(this.label2, "0x0010 - 0x00F0 : Change absolute expiration date on VALUE cards. \r\nValue is in m" +
        "onths 0 0 0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 m" +
        "onths \r\n\r\n");
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(23, 86);
            this.numericUpDown2.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(72, 22);
            this.numericUpDown2.TabIndex = 19;
            this.toolTip2.SetToolTip(this.numericUpDown2, "0x00F0 - Change absolute expiration date on VALUE cards. \r\nValue is in months 0 0" +
        " 0 = 12 months. Example: 1 = 1 month, 2 = 2 months, etc … F (15) = 15 months \r\n");
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(628, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "0x0000 - 0x000F - Seconds (0-15) to add to display timeout (Odyssey + / Fast Fare" +
    " / Fast Fare -e) \r\n";
            this.toolTip2.SetToolTip(this.label1, "0x0000 - 0x000F - Seconds (0-15) to add to display timeout (Odyssey + / Fast Fare" +
        " / Fast Fare -e) \r\n\r\n");
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(23, 32);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(72, 22);
            this.numericUpDown1.TabIndex = 17;
            this.toolTip2.SetToolTip(this.numericUpDown1, "0x000F - Seconds (0-15) to add to display timeout (Odyssey + / Fast Fare / Fast F" +
        "are -e) \r\n");
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 314);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(23, 287);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(268, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Use Smart Card Profile date.";
            this.toolTip2.SetToolTip(this.checkBox16, "0x8000 – Use Smart Card Profile date.");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(23, 260);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(419, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 - Don’t display the low stock “countdown” on the OCU.";
            this.toolTip2.SetToolTip(this.checkBox15, "Don’t display the low stock “countdown” on the OCU. 0 0 \r\nThis is used with Optio" +
        "nFlags13. If this flag is set, then the lower nibble of \r\nOptionFlags13 ( 0x???X" +
        " ) should be set to 0. ");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(23, 234);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(601, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Disable coin validator when it reaches the “Coin Full” value. (Odyssey /" +
    " Fast Fare)";
            this.toolTip2.SetToolTip(this.checkBox14, "Disable coin validator when it reaches the “Coin Full” value.\r\n(Odyssey / Fast Fa" +
        "re) 0 0 This uses the \"CoinFull\" value defined in Farebox Configuration Options " +
        "above. ");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags16Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 374);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags16Form";
            this.Text = "OptionFlag16";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}