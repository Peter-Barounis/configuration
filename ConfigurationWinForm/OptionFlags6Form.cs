﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags6Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public uint optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        public OptionFlags6Form()
        {
            InitializeComponent();            
        }

        public OptionFlags6Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (uint)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[5]);
            setCheckboxChecked();
            formLoaded = true;
            setHexValue();
        }

        public void EnableWifi(bool bEnable)
        {
            if (bEnable)
                optionflag |= 0x80;
            else
                optionflag &= (uint)~((uint)0x80);
            checkBox8.Checked = bEnable;
        }

        private void setHexValue()
        {
            if (formLoaded)
            {
                optionflag &= 0x0fff;
                optionflag |= (uint)(((uint)numericUpDownJ1708ResetTime.Value) << 12);
                optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
                splitForm.iniConfig.inifileData.optionFlags[5] = "0x" + optionflag.ToString("X4");
            }
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (uint)(1 << cbx.TabIndex);
            else
                optionflag &= ~((uint)(1 << cbx.TabIndex));
            if (formLoaded)
                splitForm.SetChanged();
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag(((CheckBox)sender));
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0101;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            checkBox1.Checked = Convert.ToBoolean((optionflag >> 0) & 0x0001);
            checkBox2.Checked = Convert.ToBoolean((optionflag >> 1) & 0x0001);
            checkBox3.Checked = Convert.ToBoolean((optionflag >> 2) & 0x0001);
            checkBox4.Checked = Convert.ToBoolean((optionflag >> 3) & 0x0001);
            checkBox5.Checked = Convert.ToBoolean((optionflag >> 4) & 0x0001);
            checkBox6.Checked = Convert.ToBoolean((optionflag >> 5) & 0x0001);
            checkBox7.Checked = Convert.ToBoolean((optionflag >> 6) & 0x0001);
            checkBox8.Checked = Convert.ToBoolean((optionflag >> 7) & 0x0001);
            checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            checkBox10.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            checkBox11.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            checkBox12.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            numericUpDownJ1708ResetTime.Value = (optionflag & 0xf000) >> 12;
        }

        private void numericUpDownJ1708ResetTime_ValueChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                setHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
