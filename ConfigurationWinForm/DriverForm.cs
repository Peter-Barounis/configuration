﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class DriverForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        private CheckBox[] cbxArray = new CheckBox[8];
        public bool formLoaded;
        public uint LogonFlag;
        public DriverForm()
        {
            InitializeComponent();
        }
        public DriverForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;
            cbxArray[6] = this.checkBox7;
            cbxArray[7] = this.checkBox8;

            LogonFlag = Util.ParseValue(splitForm.iniConfig.inifileData.logon);
            for (int i = 0; i < 8; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((LogonFlag & 1 << i) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            SetLogonHexValue();
            formLoaded = true;
        }

        private void SetLogonHexValue()
        {
            driverLogonText.Text = "Logon - [0x" + LogonFlag.ToString("X2") + "]";
        }

        private void updateFlag(int index)
        {
            if (cbxArray[index].Checked)
                LogonFlag |= ((uint)1 << index);
            else
                LogonFlag &= ~((uint)1 << index);
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag(((CheckBox)sender).TabIndex);
                SetLogonHexValue();
                splitForm.iniConfig.inifileData.logon = "0x" + LogonFlag.ToString("X2");
                splitForm.SetChanged();
            }
        }
    }
}
