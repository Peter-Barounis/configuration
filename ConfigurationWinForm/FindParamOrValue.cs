﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ConfigurationWinForm
{
    public partial class FindParamOrValue : Form
    {
        SplitForm splitform;
        string assemblyFile = "Gficonfig.exe";
        int currentSearchRow = -1;
        string currentSearchFormText = "";
        
        public FindParamOrValue()
        {
            splitform = new SplitForm();
            InitializeComponent();
            Logger.Initialize("ConfigurationWinForm.FindParamOrValue",@"C:\GFI\");
        }
        public FindParamOrValue(SplitForm sf)
        {
            this.splitform = sf;
            InitializeComponent();
            splitform.findForm = this;
        }

        private void Find_button_Click(object sender, EventArgs e)
        {
            string nspace = "ConfigurationWinForm";
            string sname = this.textBox1.Text;
            this.Find_button.Enabled = false;   
            // Cleare Data Grid view before adding search string
            this.dataGridView1.Rows.Clear();
            this.dataGridView1.Refresh();
            // Check if the search string is not null

            if (sname.Trim() == "")
            {
                MessageBox.Show("Please enter a search word to start Searching ", "Find",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                Find_button.Enabled = true;
            }
            else if (sname.Trim().Length < 2)
            {
                MessageBox.Show("Please enter a search word with minimum of 2 characters", "Find",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                Find_button.Enabled = true;

            }
            else
            {
                // Set default color before finding some thing else.

                foreach (SplitForm.WindowList item in splitform.frmList)
                {
                    foreach (Control control in item.frm.Parent.Controls)
                    {
                        setDefaultBackColor(control);
                    }
                }

                var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(t => t.GetTypes())
                    .Where(t => t.IsClass && t.Namespace == @nspace)
                    .ToList();

                //types.ForEach(t => searchForm(t.Name,t));
                foreach (Type t in types)
                {
                    searchForm(t.FullName, t);
                }
                if (this.dataGridView1.Rows.Count <= 1)
                {
                    MessageBox.Show("No matching results found for string: " + this.textBox1.Text, "Find",
                           MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    splitform.SetArrowEnable(false);
                }
                else
                {
                    splitform.SetArrowEnable(true);
                    currentSearchRow = 0;
                    SelectCurrentForm();
                }
            }
        }

        private void highlightForm(string sname, Control mainControl)
        {

            foreach (Control control in mainControl.Controls)
            {
                if (Regex.Match(control.Name.ToString(), sname.ToString(), RegexOptions.IgnoreCase).Success ||
                    Regex.Match(control.Text.ToString(), sname.ToString(), RegexOptions.IgnoreCase).Success)
                {
                    control.BackColor = System.Drawing.Color.GreenYellow;
                }

                if (control.HasChildren)
                {

                    highlightForm(sname, control);
                }
            }
        }
        private void setDefaultBackColor(Control mainControl)
        {

            foreach (Control control in mainControl.Controls)
            {
                    control.BackColor = SystemColors.Control;

                if (control.HasChildren)
                {

                    setDefaultBackColor(control);
                }
            }
        }
        public IEnumerable<T> FindControls<T>(Control control) where T : Control
        {
            // we can't cast here because some controls in here will most likely not be <T>
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => FindControls<T>(ctrl))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == typeof(T)).Cast<T>();
        }

        public void SearchControls(Control mainControl,string node,string cstring,string className, string classType) 
        {
            // we can't cast here because some controls in here will most likely not be <T>
            foreach (Control control in mainControl.Controls)
            {
                if (control.HasChildren)
                {
                    SearchControls(control,node, cstring,className,classType);
                }
                else
                {
                    if (Regex.Match(control.Name.ToString(), cstring, RegexOptions.IgnoreCase).Success)
                    {
                        this.dataGridView1.Rows.Add(className, node, control.Name.ToString(), classType, control.GetType());
                    }
                    if (Regex.Match(control.Text.ToString(), cstring, RegexOptions.IgnoreCase).Success)
                    {
                        this.dataGridView1.Rows.Add(className,node, control.Text.ToString(), classType, control.GetType());
                    }
                }
            }
        }

        private void searchForm(string cname, Type typ)
        {
            try
            {
                string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                string searchString = this.textBox1.Text;
                List<PropertyInfo> PropertyInfoList = new List<PropertyInfo>();
                List<FieldInfo> FieldInfoList = new List<FieldInfo>();
                List<Control> controlList = new List<Control>();
                List<string> excludeFormList = new List<string>();

                
                //Add all the classes/Forms you want to exclude in exclludeFormList
                excludeFormList.Add("Program");
                excludeFormList.Add("SplitForm");
                excludeFormList.Add("FindParamOrValue");
                excludeFormList.Add("CompareFilesForm");
                excludeFormList.Add("ChooseFiles");
                excludeFormList.Add("UserLoginForm");
                excludeFormList.Add("SaveConfigurations");
                excludeFormList.Add("CrontabForm");
                
                

                //string className = Regex.Replace(typ.FullName, "[+]", ".");
                string className = typ.FullName;
                
                //Look for only Form classes and avoid all subclasses
                if (!Regex.Match(className, "[+]").Success && !excludeFormList.Exists(e => e.EndsWith(typ.Name.ToString()))) 
                    
                   //typ.Name != "Program" 
                   // && typ.Name != "SplitForm" && typ.Name != "FindParamOrValue" && typ.Name !="CompareFilesForm")
                {
                    Object obj1 = Activator.CreateInstanceFrom(assemblyFile, className).Unwrap();
                    object obj1Type = typ.BaseType;
                    

                    //Don't check the classes derived from Object base class
                    if (!Regex.Match(obj1Type.ToString(), "System.Object").Success)
                    {
                        object obj1Text = typ.GetProperty("Text").GetValue(obj1);
                        object obj1Name = typ.GetProperty("Name").GetValue(obj1);
                        /////////////////////////////////////////////////////////////////
                        // Do not currently need to search the form name
                        //string  test = typ.BaseType.ToString();
                        //try
                        //{
                        //if (obj1Text != null || obj1Name!=null)
                        //    {
                        //        Match matchName = Regex.Match(obj1Name.ToString(), searchString, RegexOptions.IgnoreCase);
                        //        Match matchValue = Regex.Match(obj1Text.ToString(), searchString, RegexOptions.IgnoreCase);
                                
                        //        if (matchName.Success) 
                        //        {
                        //            this.dataGridView1.Rows.Add(cname, obj1Text, obj1Name, obj1Type, obj1Type);
                        //        }
                        //        if (matchValue.Success)
                        //        {
                        //            this.dataGridView1.Rows.Add(cname, obj1Text, obj1Text, obj1Type, obj1Type);
                        //        }
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    Logger.LogEntry("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                        //"" + Environment.NewLine);                               
                        //}
                        /////////////////////////////////////////////////////////////////
                        foreach (FieldInfo prop in typ.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
                        {
                            try
                            {
                                object propName = null;
                                object propValue = null;
                                object propType = null;

                                propName = prop.Name;
                                propValue = prop.GetValue(obj1);
                                propType = prop.FieldType;

                                if (propValue != null)
                                {
                                    // No need to match the names of the fields
                                    //Match matchName = Regex.Match(propName.ToString(), searchString, RegexOptions.IgnoreCase);
                                    Match matchValue = Regex.Match(propValue.ToString(), searchString, RegexOptions.IgnoreCase);

                                    //if (matchName.Success || matchValue.Success)
                                    if (matchValue.Success)
                                    {
                                        FieldInfoList.Add(prop);
                                        //this.dataGridView1.Rows.Add(cname + "." + propName, propValue);
                                        this.dataGridView1.Rows.Add(cname,obj1Text, propValue, obj1Type, propType);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogEntry("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                           "" + Environment.NewLine);
                                continue;
                            }
                        }
                        // Get all controls and search controls text
                        if (obj1Type.ToString() == "System.Windows.Forms.Form")
                        {
                            Form frm = (Form)obj1;
                            foreach (Control control in frm.Controls)
                            {
                                SearchControls(control, frm.Text,searchString, cname, obj1Type);
                            }
                        }
                    }

                    dataGridView1.AutoResizeColumns();

                    dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    this.Find_button.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogEntry("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                     "" + Environment.NewLine);
            }         
        }

        private void SearchControls(Control control,string node, string searchString, string cname, object obj1Type)
        {
            SearchControls(control, node ,searchString, cname, obj1Type.ToString());
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string currentForm = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
            //string currentSearchValue = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string currentSearchValue = this.textBox1.Text.ToString();
            string currentClassType = this.dataGridView1.CurrentRow.Cells[3].Value.ToString();
            string currentPropType = this.dataGridView1.CurrentRow.Cells[4].Value.ToString();
            Form frm = new Form();


            switch (currentClassType)
            {
                case "System.Windows.Forms.Form":
                    frm = (Form)Activator.CreateInstanceFrom(assemblyFile, currentForm).Unwrap();

                    if (!splitform.SelectTreeviewNode(frm.Text))
                    {
                        MessageBox.Show("Could not open the form : " + currentForm, "Find",
                              MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        foreach (SplitForm.WindowList item in splitform.frmList)
                        {
                            //                if (string.Compare(node.FullPath, item.tabPage.Text, true) == 0)
                            if (string.Compare(frm.Name, item.frm.Name, true) == 0)
                            {
                                currentForm = item.frm.Name;
                                foreach (Control control in item.frm.Parent.Controls)
                                {
                                    highlightForm(currentSearchValue, control);
                                }

                                item.frm.Show();
                            }
                        }
                        currentSearchRow = 0;
                    }
                    break;
                default:
                    MessageBox.Show("This is not a valid form. Please chose the other search results to open a form : " + currentForm, "Find",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    break;
            }

        }

        public int SelectPrevForm()
        {
            int ret_cd = -2;
            if (dataGridView1.Rows.Count > 1)
            {
                while (ret_cd == -2)
                {
                    currentSearchRow--;
                    if (currentSearchRow < 0)
                    {
                        currentSearchRow = 0;
                        ret_cd = 0;
                        break;
                    }
                    ret_cd = SelectCurrentForm();
                    if (!splitform.toolSearchDown.Enabled)
                        splitform.toolSearchDown.Enabled = true;
                }
            }
            return ret_cd;
        }

        public int SelectNextForm()
        {
            int ret_cd = -2;
            if (dataGridView1.Rows.Count > 1)
            {
                while (ret_cd == -2)
                {
                    currentSearchRow++;
                    if (currentSearchRow >= dataGridView1.Rows.Count -1)
                    {
                        currentSearchRow = dataGridView1.Rows.Count - 1;
                        ret_cd = 0;
                        break;
                    }
                    ret_cd = SelectCurrentForm();
                    if (!splitform.toolSearchUp.Enabled)
                        splitform.toolSearchUp.Enabled = true;
                }
            }
            return ret_cd;
        }

        public int SelectCurrentForm()
        {
            int ret_cd=-1;  // -1=error, -2=duplicate, 0=not found, 1-n=found number
            // Set current row
            if (currentSearchRow < 0)
                return (ret_cd);
            else
                if (currentSearchRow > dataGridView1.Rows.Count - 1)
                    return (ret_cd);

            string currentForm = this.dataGridView1.Rows[currentSearchRow].Cells[0].Value.ToString();
            string currentSearchValue = this.textBox1.Text.ToString();
            string currentClassType = this.dataGridView1.Rows[currentSearchRow].Cells[3].Value.ToString();
            string currentPropType = this.dataGridView1.Rows[currentSearchRow].Cells[4].Value.ToString();
            Form frm = new Form();

            try
            {
                switch (currentClassType)
                {
                    case "System.Windows.Forms.Form":
                        frm = (Form)Activator.CreateInstanceFrom(assemblyFile, currentForm).Unwrap();

                        if (!splitform.SelectTreeviewNode(frm.Text))
                        {
                            //MessageBox.Show("Could not open the form : " + currentForm, "Find",
                            //      MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            foreach (SplitForm.WindowList item in splitform.frmList)
                            {
                                // Found form?
                                if (string.Compare(frm.Name, item.frm.Name, true) == 0)
                                {
                                    // Same as last form?
                                    if (string.Compare(frm.Name, currentSearchFormText, true) == 0)
                                    {
                                        ret_cd = -2;
                                        break;
                                    }
                                }
                                if (string.Compare(frm.Name, item.frm.Name, true) == 0)
                                {
                                    currentSearchFormText = item.frm.Name;
                                    currentForm = item.frm.Name;
                                    foreach (Control control in item.frm.Parent.Controls)
                                    {
                                        highlightForm(currentSearchValue, control);
                                    }
                                    item.frm.Show();
                                    ret_cd = currentSearchRow;
                                }
                            }
                        }
                        break;
                    default:
                        MessageBox.Show("This is not a valid form. Please chose the other search results to open a form : " + currentForm, "Find",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                        ret_cd = -1;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogEntry("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                          "" + Environment.NewLine);
            }
            return ret_cd;
        }

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            //// Set default color before finding some thing else.
            //foreach (SplitForm.WindowList item in splitform.frmList)
            //{
            //    foreach (Control control in item.frm.Parent.Controls)
            //    {
            //        setDefaultBackColor(control);
            //    }
            //}
            //splitform.SetArrowEnable(false);

            Close();
        }

        private void FindParamOrValue_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Set default color before finding some thing else.
            foreach (SplitForm.WindowList item in splitform.frmList)
            {
                foreach (Control control in item.frm.Parent.Controls)
                {
                    setDefaultBackColor(control);
                }
            }
            splitform.SetArrowEnable(false);
            splitform.findForm = null;
            splitform.UpdateMenuStatus();
        }
    }
}
