﻿namespace ConfigurationWinForm
{
    partial class OptionFlags29Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionFlags29Form));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(30, 98);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(742, 21);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "0x0100 – Enabling this feature provides the same functionality as the “ProcessFar" +
    "e” attribute in the fare structure.";
            this.toolTip2.SetToolTip(this.checkBox1, resources.GetString("checkBox1.ToolTip"));
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(30, 123);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(669, 21);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Text = "0x0200 – Show bus number on patron screen if Route Number is not shown. (Fast Far" +
    "e / Fast Fare -e)";
            this.toolTip2.SetToolTip(this.checkBox2, "Show bus number on patron screen if Route Number is not shown. (Fast Fare / Fast " +
        "Fare -e) ");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(30, 148);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(423, 21);
            this.checkBox3.TabIndex = 3;
            this.checkBox3.Text = "0x0400 – Hide the “SWIPE” icon on the Idle screen. (Fast Fare)";
            this.toolTip2.SetToolTip(this.checkBox3, "Hide the “SWIPE” icon on the Idle screen. (Fast Fare)");
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.checkBox4);
            this.optionflagsGridTitle.Controls.Add(this.checkBox5);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox7);
            this.optionflagsGridTitle.Controls.Add(this.checkBox6);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox3);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(806, 341);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(30, 271);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(607, 21);
            this.checkBox8.TabIndex = 8;
            this.checkBox8.Text = "0x8000 – “Transaction Time remaining” (Dump Time) will be displayed on the patron" +
    " display.";
            this.toolTip2.SetToolTip(this.checkBox8, "NOTE: OptionFlags13 bit 11 (0x0800) MUST be set to use the functionality defined " +
        "by thhis bit.");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(30, 246);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(538, 21);
            this.checkBox7.TabIndex = 7;
            this.checkBox7.Text = "0x4000 – Richmond Count Short Fare \"Count the Fare before Paying for it\" option";
            this.toolTip2.SetToolTip(this.checkBox7, "This is for Richmond - Record Transaction before paying for it.");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(30, 221);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(460, 21);
            this.checkBox6.TabIndex = 6;
            this.checkBox6.Text = "0x2000 – Send Driver & Route to Cubic Clipper over UART (Fast Fare)";
            this.toolTip2.SetToolTip(this.checkBox6, "Hide the “SWIPE” icon on the Idle screen. (Fast Fare)");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(673, 68);
            this.label1.TabIndex = 18;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(30, 37);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(75, 22);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(30, 197);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(277, 21);
            this.checkBox5.TabIndex = 5;
            this.checkBox5.Text = "0x1000 – Use Third Party Smart Cards. ";
            this.toolTip2.SetToolTip(this.checkBox5, "Functionality works same as Third Party magnetics. (Fast Fare / Fast Fare -e)");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(30, 173);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(229, 21);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Text = "0x0800 – ******** Unused  ********";
            this.toolTip2.SetToolTip(this.checkBox4, "Hide the “SWIPE” icon on the Idle screen. (Fast Fare)");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 300);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OptionFlags29Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 394);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags29Form";
            this.Text = "OptionFlag29";
            this.toolTip2.SetToolTip(this, "(Recommended: 0x0000, Default: 0x0000) Values");
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button button1;


    }
}