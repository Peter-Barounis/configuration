﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags21Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags21Form()
        {
            InitializeComponent();
         }
 
        public OptionFlags21Form(SplitForm sf)
        {
            this.splitForm = sf;
            
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[20].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[20]);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[20] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
            setHexValue();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();
        }
        private void setCheckboxChecked()
        {

            this.checkBox1.Checked = Convert.ToBoolean(optionflag & 1);
            this.checkBox2.Checked = Convert.ToBoolean(optionflag & 2);
            this.checkBox3.Checked = Convert.ToBoolean(optionflag & 4);
            this.checkBox4.Checked = Convert.ToBoolean(optionflag & 8);
            this.numericUpDown1.Value = ((optionflag >> 4) & 0x000F);
            this.numericUpDown2.Value = ((optionflag >> 8) & 0x000F) * 5;
            if (((optionflag >> 12) & 0x000F) == 0)
            {
                this.numericUpDown3.Value = 75;
            }
            else
            {
                this.numericUpDown3.Value = ((optionflag >> 12) & 0x000F) * 5 + 20;
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value);
            optionflag = ((optionflag & 0xFF0F) | ((lmt1 << 4) & 0x00F0));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }

        private void numericUpDown2_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown2_ValueChanged(sender, e);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            int lmt2 = ((int)numericUpDown2.Value/5);
            optionflag = ((optionflag & 0xF0FF) | ((lmt2 << 8) & 0x0F00));
            setHexValue();
        }
        private void numericUpDown3_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown3_ValueChanged(sender, e);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            int lmt3 = ((int)numericUpDown3.Value/5 - 4);
            optionflag = ((optionflag & 0x0FFF) | ((lmt3 << 12) & 0xF000));
            setHexValue();
        }

    }
}
