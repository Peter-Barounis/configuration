﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GfiUtilities;

namespace ConfigurationWinForm
{

    public class iniCommentsData
    {
        public string keyData;
        public string comment;
        public iniCommentsData()
        {

        }

        public iniCommentsData(string keyIn,string commentIn)
        {
            keyData=keyIn;
            comment=commentIn;
        }

    }


    public partial class IniFile
    {
        [DllImport("gfipb32")]
        private unsafe static extern int Encrypt(byte[] dst, byte[] src, int max);

        [DllImport("gfipb32")]
        private unsafe static extern int Decrypt(byte[] dst, byte[] src, int max);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        [DllImport("Kernel32.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        private static extern UInt32 GetPrivateProfileSection
            (
                [In] [MarshalAs(UnmanagedType.LPStr)] string strSectionName,
            // Note that because the key/value pars are returned as null-terminated
            // strings with the last string followed by 2 null-characters, we cannot
            // use StringBuilder.
                [In] IntPtr pReturnedString,
                [In] UInt32 nSize,
                [In] [MarshalAs(UnmanagedType.LPStr)] string strFileName
            );
        public bool bUseDefaults = true;
        public string IniFilePath { get; set; }
        public string IniFileName { get; set; }
        public SplitForm splitForm;
        //public string Path { get; set; }
        //public IniConfiguration iniConfig = new IniConfiguration();
        public IntPtr pBuffer = Marshal.AllocHGlobal(32767);
        public string EncryptString(string src, ref string str)
        {
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            src = src.PadRight(33);
            buffer2 = Encoding.ASCII.GetBytes(src);
            Encrypt(buffer1, buffer2, 32);
            Array.Resize(ref buffer1, 32);
            var table = (Encoding.Default.GetString(
                             buffer1,
                             0,
                             src.Trim().Length)).Split(new string[] { "\r\n", "\r", "\n" },
                             StringSplitOptions.None);
            str = table[0];
            return "\\x" + BitConverter.ToString(buffer1).Replace("-", "\\x");
        }

        public string DecryptString(string src)
        {
            int j;
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            buffer2 = Encoding.ASCII.GetBytes(src);
            src = src.PadRight(33);

            if (src.Trim().Length > 0)
            {
                Decrypt(buffer1, buffer2, 32);
                for (j=0;j<33;j++)
                {
                    if (buffer1[j] == 0x00)
                        break;
                }
                Array.Resize(ref buffer1, j);
                return System.Text.Encoding.Default.GetString(buffer1).Trim();
            }
            else
                return ("");
        }

        public bool DoesSectionExist(string sectionName)
        {
            string[] strArray = new string[0];
            UInt32 uiNumCharCopied = 0;
            uiNumCharCopied = GetPrivateProfileSection(sectionName, pBuffer, 256, IniFilePath + IniFileName);
            if (uiNumCharCopied > 0)
                return true;
            else
                return false;
        }
        
        public IniFile()
        {
            IniFilePath = "C:\\GFI\\cnf\\";
            IniFileName = "gfi.ini";// ConfigurationWinForm.SplitForm.Constants.GFIINI;
        }

        public IniFile(SplitForm sf, string path, string filename)
        {
            splitForm = sf;
            IniFilePath = path;
            IniFileName = filename;
        }

        
        //  /^      # start of string
        //  0x      # 0x prefix
        //  [0-9A-F]# a hex digit
        //  {1,4}   #    1 to 4 of them
        //  $       # end of string
        /// xi      # x = allow spaces & comments in regexp
        //          # i = ignore case
        public string ReadHex(string Key, string Section, string dflt)
        {
            var RetVal = new StringBuilder(255);
            if (GetPrivateProfileString(Section, Key, "", RetVal, 255, IniFilePath+IniFileName) > 0)
                dflt = RetVal.ToString();
            // Replace tabs and semi-colon with space
            // This is to ensure that any comments are stripped out
            dflt=dflt.Replace('\t', ' ');
            dflt=dflt.Replace(';', ' ');
            string[] substrings = dflt.Split(' ');
            dflt = substrings[0];
            Regex rgx = new Regex("/^0x[0-9A-F]{1,4}$/xi");
            dflt = rgx.Replace(dflt, "");
            return (dflt);
        }

        public string Read(string Key, string Section, string dflt)
        {
            var RetVal = new StringBuilder(255);
            if (!bUseDefaults)
                dflt = "";
            if (GetPrivateProfileString(Section, Key, "", RetVal, 255, IniFilePath + IniFileName) > 0)
                dflt =  RetVal.ToString();
            return dflt;
        }

        public string ReadLong(string Key, string Section, string dflt)
        {
            bool bSaveUseDefaults = bUseDefaults;    // Save defaults switch
            bUseDefaults = true;
            dflt = Read(Key, Section, dflt);
            dflt = dflt.Replace('\t', ' ');
            dflt = dflt.Replace(';', ' ');
            string[] substrings = dflt.Split(' ');
            dflt = substrings[0];
            bUseDefaults=bSaveUseDefaults;          // restore defaults switch
            return Regex.Match(dflt, @"-?\d+").Value;
        }
        public string ReadFloat(string Key, string Section, string dflt)
        {
            float floatValue;
            bool bSaveUseDefaults = bUseDefaults;    // Save defaults switch
            bUseDefaults = true;
            dflt = Read(Key, Section, dflt);
            dflt = dflt.Replace('\t', ' ');
            dflt = dflt.Replace(';', ' ');
            string[] substrings = dflt.Split(' ');
            dflt = substrings[0];
            bUseDefaults = bSaveUseDefaults;          // restore defaults switch
            if (!float.TryParse(dflt, out floatValue))
                dflt = "0";
            return dflt;
        }
        public void Write(string Key, string Value, string Section,string dflt)
        {
            if (!bUseDefaults)
                dflt = "";
            if ((Value == null) || Value.Trim().Length == 0)
            {
                Value = dflt;
            }
            WritePrivateProfileString(Section, Key, Value, IniFilePath + IniFileName);
        }

        public void DeleteKey(string Key, string Section)
        {
            Write(Key, null, Section,"");
        }

        public void DeleteSection(string Section)
        {
            Write(null, null, Section,"");
        }

        public bool KeyExists(string Key, string Section)
        {
            return Read(Key, Section,"").Length > 0;
        }

        public void AddIniFileComment(string comment)
        {
            StreamWriter sw = new StreamWriter(IniFilePath + IniFileName,true);
            sw.WriteLine(";");
            sw.WriteLine(";"+comment);
            sw.Close();
        }

        // Trim trailing blanks from all entries int he file
        public void TrimFile(string srcFile, gfiIniFileData inifileData, bool AddComments)
        {
            StreamReader sr;
            StreamWriter sw;
            string destFile = srcFile;
            srcFile += ".bak";
            // Backup file
            try
            {
                File.Delete(srcFile);
                File.Move(destFile, srcFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
                return;
            }
            try
            {
                sr = new StreamReader(srcFile,true);
                sw = new StreamWriter(destFile, false, Encoding.ASCII);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
                return;
            }

            string line;
            //Read the first line of text
            line = sr.ReadLine();

            //Continue to read until you reach end of file
            while (line != null)
            {
                int n = 0;
                line = line.Trim();
                if (AddComments)
                {
                    foreach (iniCommentsData s in inifileData.iniComments)
                    {
                        if ((n = string.Compare(s.keyData, 0, line, 0, s.keyData.Length, true)) == 0)
                        {
                            sw.WriteLine("");
                            if (s.comment.Length > 0)
                                sw.WriteLine(";" + s.comment);
                            else
                                sw.WriteLine("");
                            break;
                        }
                    }
                }
                //write the line to console window
                sw.WriteLine(line);
                //Read the next line
                line = sr.ReadLine();
            }
            //close the file
            sr.Close();
            sw.Close();
            File.Delete(srcFile);
        }

    }

    public class MuxCommon
    {
        public string Port = "COM1";                //  CommName,	PROFILE_STRING, CommName,
        public string PortEnbMask = "0x0F";         //	PROFILE_WORD,	&PortEnbMask,
        public string OldBaudrate = "9600";         //	PROFILE_LONG,	&OldBaudRate,
        public string Baudrate = "9600";            //	PROFILE_LONG,	&BaudRate,
        public string FbxBaudrate = "9600";         //	PROFILE_LONG,	&FbxBaudRate,
        public string ScanBaudRate = "0";           //	PROFILE_LONG,	&ScanBaudRate,
        public string MuxIsoBox = "1";              //	PROFILE_LONG,	&MuxIsoBox,
        public string ProbeType = "0";              //	PROFILE_LONG,	&ProbeType,

        public string WaitSync = "1";               //	PROFILE_LONG,	&WaitSync,
        //public string DumpSQL = "0";                //	PROFILE_LONG,	&DebugLevel,
        //public string AcctMinRecharge = "0";        //  PROFILE_LONG    &AcctMinRecharge,
        //                                            // only let them enter 0 (disabled) or 5 to 255 in Editor   
        //                                            // (1-4 is INVALID and same as 0)
        //public string PassportTTP = "0";        //  PROFILE_LONG    &AcctPassportTTP,
        //                                            // only let them enter 0 (disabled) or 1 to 240 in Editor   
        //                                            // (241-255 is INVALID and same as 0)


        public MuxCommon()
        {
        }
    }

    public class VltCommon
    {
        public string Port = "COM1";                //  CommName,	PROFILE_STRING, CommName,
        public string PortEnbMask = "0x0F";         //	PROFILE_WORD,	&PortEnbMask,
        public string LockOut = "0";
        public string DebugLevel = "0";             //	PROFILE_LONG,	&DebugLevel,
        public string VltFull = "10000";
        public string VltFlags = "0x00000000";      //VLTF_DEBUG_PROTOCOL  (ulong)0x00000002 - debug protocol comm. flag 
                                                    //VLTF_DEBUG_FRAMES    (ulong)0x00000004 - debug frames flag         
                                                    //VLTF_DEBUG_TDAY      (ulong)0x00000008 - debug T-day  flag         
                                                    //VLTF_NODATABASE      (ulong)0x10000000 - no database               

        public VltCommon()
        {
        }
    }

    public class NetconfigData
    {
        public bool bUseDefaults = true;
        ///////////////////////////////////////////////////////////////////////////////////////////
        // NetConfig.cfg
        public string Gen_Version = "1.1";
        public string Gen_ISPrbIntvlInMin = "5";
        public string Gen_OOSPrbIntvlInMin = "3";
        public string Gen_ProbeTimeOutInSec = "30";

        // sectionName = "GDS";
        public string GDS_GCHostName = "genfare";
        public string GDS_GCIP = "1.1.1.1";
        public string GDS_GCTCPPort = "3000";
        public string GDS_FBXTCPPort = "3000";

        // sectionName = "WIFI_GEN";
        public string WIFI_GEN_APConIntvlStatus = "ENABLED";
        public string WIFI_GEN_APScanTimoutInMin = "60";
        public string WIFI_GEN_Min_RSSIThreshold = "70";
        public string WIFI_GEN_Max_RSSIThreshold = "80";
        public string WIFI_WifiNetMode = "INFRASTRUCTURE";
        public string WIFI_GEN_IBBSMode = "NA";
        public string WIFI_GEN_WifiAPCount = "1";
        public string WIFI_GEN_CapabilityFlags1 = "0x01020304";

        // sectionName = "WIFI_AP1";
        public string WIFI_AP1_SSID = "";
        public string WIFI_AP1_Passkey = "";
        public string WIFI_AP1_SecType = "WPA2";
        public string WIFI_AP1_DHCPMode = "DYNAMIC";
        public string WIFI_AP1_FBXIP = "0.0.0.0";
        public string WIFI_AP1_SubNetMask = "0.0.0.0";
        public string WIFI_AP1_DefaultGateway = "0.0.0.0";
        public string WIFI_AP1_DNSPri = "10.0.0.0";
        public string WIFI_AP1_DNSSec = "10.0.0.0";

        // sectionName = "WIFI_AP2";
        public string WIFI_AP2_SSID = "";
        public string WIFI_AP2_Passkey = "";
        public string WIFI_AP2_SecType = "WPA2";
        public string WIFI_AP2_DHCPMode = "DYNAMIC";
        public string WIFI_AP2_FBXIP = "0.0.0.0";
        public string WIFI_AP2_SubNetMask = "0.0.0.0";
        public string WIFI_AP2_DefaultGateway = "0.0.0.0";
        public string WIFI_AP2_DNSPri = "10.0.0.0";
        public string WIFI_AP2_DNSSec = "10.0.0.0";

        // sectionName = "ETHERNET";
        public string ETHERNET_DHCPMode = "";
        public string FETHERNE_TBXIP = "";
        public string ETHERNET_SubNetMask = "";
        public string ETHERNET_DefaultGateway = "";
        public string ETHERNET_DNSPri = "";
        public string ETHERNET_DNSSec = "";
        public string ETHERNET_CapabilityFlags1 = "";

        // sectionName = "CELLMODEM";
        public string CellModem_Type = "Inmotion";
        public string CellSignal_Threshold = "-70";
        public string CellModem_Status = "ENABLED";

        // sectionName = "GPS";
        public string GPS_IP = "192.168.1.100";
        public string GPS_NetProto = "UDP";
        public string GPS_Port = "5067";
        public string GPS_MsgFormat = "NMEA";
        public string GPS_ModemStatus = "ENABLED";
        public string GPS_MaxTransTimeDelta = "10";
        public string GPS_MaxTransPositionDelta = "3";

        // sectionName = "CLOUD";
        //Mobile Ticketing
        public string MbTkHost = "";
        public string MbTkPath = "";
        public string MbTkCredential = "";
        //Genfare Link
        public string GenLink_Host = "";
        public string GenLink_SetupPath = "";
        public string GenLink_SetupCredential = "";
        public string GenLink_DeviceAuthPath = "";
        public string GenLink_Env = "";
        public string GenLink_Tenant = "PTVL";
        public string GenLink_HttpsStat = "DISABLED";
        public string GenLink_GDSUpgradeHost = "";
        public string GenLink_GDSUpgradePath = "";
        public string GenLink_GDSUpgradeCredential = "";
        public string GenLink_CapabilityFlags1 = "0x1234";
        
        //////////////////////////////////////////////////////////////////////////////
        // Read NetConfig
        public void ReadNetconfigFile(IniFile iniFile)
        {
            string sectionName;
            // Set file name
            //iniFile.IniFileName = iniFileIniFileName;
            //iniFile.IniFileName = ConfigurationWinForm.SplitForm.Constants.NETCONFIG;

            sectionName = "GEN";
            Gen_Version = iniFile.Read("Version", sectionName, "1.1");
            Gen_ISPrbIntvlInMin = iniFile.ReadLong("ISPrbIntvlInMin", sectionName, "5");
            Gen_OOSPrbIntvlInMin = iniFile.ReadLong("OOSPrbIntvlInMin", sectionName, "3");
            Gen_ProbeTimeOutInSec = iniFile.ReadLong("ProbeTimeOutInSec",sectionName,"30");

            sectionName = "GDS";
            GDS_GCHostName = iniFile.Read("GCHostName", sectionName, "genfare");
            GDS_GCIP = iniFile.Read("GCIP", sectionName, "1.1.1.1");
            GDS_GCTCPPort = iniFile.ReadLong("GCTCPPort", sectionName, "3000");
            GDS_FBXTCPPort = iniFile.ReadLong("FBXTCPPort", sectionName, "3000");

            sectionName = "WIFI_GEN";
            WIFI_GEN_APConIntvlStatus = iniFile.Read("APConIntvlStatus", sectionName, "ENABLED");
            WIFI_GEN_APScanTimoutInMin = iniFile.ReadLong("APScanTimoutInMin", sectionName, "60");
            WIFI_GEN_Min_RSSIThreshold = iniFile.ReadLong("MinRSSIThreshold", sectionName, "70");
            WIFI_GEN_Max_RSSIThreshold = iniFile.ReadLong("MaxRSSIThreshold", sectionName, "80");
            WIFI_WifiNetMode = iniFile.Read("WifiNetMode", sectionName, "INFRASTRUCTURE");
            WIFI_GEN_IBBSMode = iniFile.Read("IBBSMode", sectionName, "NA");
            WIFI_GEN_WifiAPCount = iniFile.ReadLong("WifiAPCount", sectionName, "1");
            WIFI_GEN_CapabilityFlags1 = iniFile.ReadHex("CapabilityFlags1", sectionName, "0x01020304");

            sectionName = "WIFI_AP1";
            WIFI_AP1_SSID = iniFile.Read("SSID", sectionName, "");
            WIFI_AP1_Passkey = iniFile.Read("Passkey", sectionName, "");
            WIFI_AP1_SecType = iniFile.Read("SecType", sectionName, "WPA2");
            WIFI_AP1_DHCPMode = iniFile.Read("DHCPMode", sectionName, "DYNAMIC");
            WIFI_AP1_FBXIP = iniFile.Read("FBXIP", sectionName, "0.0.0.0");
            WIFI_AP1_SubNetMask = iniFile.Read("SubNetMask", sectionName, "0.0.0.0");
            WIFI_AP1_DefaultGateway = iniFile.Read("DefaultGateway", sectionName, "0.0.0.0");
            WIFI_AP1_DNSPri = iniFile.Read("DNSPri", sectionName, "10.0.0.1");
            WIFI_AP1_DNSSec = iniFile.Read("DNSSec", sectionName, "10.0.0.2");

            sectionName = "WIFI_AP2";
            WIFI_AP2_SSID = iniFile.Read("SSID", sectionName, "");
            WIFI_AP2_Passkey = iniFile.Read("Passkey", sectionName, "");
            WIFI_AP2_SecType = iniFile.Read("SecType", sectionName, "WPA2");
            WIFI_AP2_DHCPMode = iniFile.Read("DHCPMode", sectionName, "DYNAMIC");
            WIFI_AP2_FBXIP = iniFile.Read("FBXIP", sectionName, "0.0.0.0");
            WIFI_AP2_SubNetMask = iniFile.Read("SubNetMask", sectionName, "0.0.0.0");
            WIFI_AP2_DefaultGateway = iniFile.Read("DefaultGateway", sectionName, "0.0.0.0");
            WIFI_AP2_DNSPri = iniFile.Read("DNSPri", sectionName, "10.0.0.1");
            WIFI_AP2_DNSSec = iniFile.Read("DNSSec", sectionName, "10.0.0.2");

            sectionName = "ETHERNET";
            ETHERNET_DHCPMode = iniFile.Read("DHCPMode", sectionName, "");
            FETHERNE_TBXIP = iniFile.Read("FBXIP", sectionName, "");
            ETHERNET_SubNetMask = iniFile.Read("SubNetMask", sectionName, "");
            ETHERNET_DefaultGateway = iniFile.Read("DefaultGateway", sectionName, "");
            ETHERNET_DNSPri = iniFile.Read("DNSPri", sectionName, "");
            ETHERNET_DNSSec = iniFile.Read("DNSSec", sectionName, "");
            ETHERNET_CapabilityFlags1 = iniFile.Read("CapabilityFlags1", sectionName, "");


            sectionName = "CELLMODEM";
            CellModem_Type = iniFile.Read("CellModemType", sectionName, "");
            CellSignal_Threshold = iniFile.ReadLong("CellSignalThreshold", sectionName, "-65");
            CellModem_Status = iniFile.Read("CellModemStatus", sectionName, "ENABLED");

            sectionName = "GPS";
            GPS_IP = iniFile.Read("GPSIP", sectionName, "192.168.1.100");
            GPS_NetProto = iniFile.Read("GPSNetProto", sectionName, "UDP");
            GPS_Port = iniFile.ReadLong("GPSPort", sectionName, "5067");
            GPS_MsgFormat = iniFile.Read("GPSMsgFormat", sectionName, "NMEA");
            GPS_ModemStatus = iniFile.Read("GPSModemStatus", sectionName, "ENABLED");
            GPS_MaxTransTimeDelta = iniFile.ReadLong("GPSMaxTransTimeDelta", sectionName, "10");
            GPS_MaxTransPositionDelta = iniFile.ReadLong("GPSMaxTransPositionDelta", sectionName, "3");


            sectionName = "CLOUD";
            MbTkHost = iniFile.Read("MbTkHost", sectionName, "");
            MbTkPath = iniFile.Read("MbTkPath", sectionName, "");
            MbTkCredential = iniFile.Read("MbTkCredential", sectionName, "");
                                  
            GenLink_Host = iniFile.Read("GenLinkHost", sectionName, "");
            GenLink_SetupPath = iniFile.Read("GenLinkSetupPath", sectionName, "");
            GenLink_SetupCredential = iniFile.Read("GenLinkSetupCredential", sectionName, "");
            GenLink_DeviceAuthPath = iniFile.Read("GenLinkDeviceAuthPath", sectionName, "");
            GenLink_Env = iniFile.Read("GenLinkEnv", sectionName, "");
            GenLink_Tenant = iniFile.Read("GenLinkTenant", sectionName, "");
            GenLink_HttpsStat = iniFile.Read("GenLinkHttpsStat", sectionName, "DISABLED");
            GenLink_GDSUpgradeHost = iniFile.Read("GDSUpgradeHost", sectionName, "");
            GenLink_GDSUpgradePath = iniFile.Read("GDSUpgradePath", sectionName, "");
            GenLink_GDSUpgradeCredential = iniFile.Read("GDSUpgradeCredential", sectionName, "");
            GenLink_CapabilityFlags1 = iniFile.ReadHex("CapabilityFlags1", sectionName, "0x1234");
        }

        //////////////////////////////////////////////////////////////////////////////
        // Write NetConfig
        public void writeNetconfig(IniFile iniFile)
        {
            string sectionName;
 
            // Set file name
            //iniFile.IniFileName = ConfigurationWinForm.SplitForm.Constants.NETCONFIG;
            sectionName = "GEN";
            iniFile.Write("Version", Gen_Version, sectionName,"1.1");
            iniFile.Write("ISPrbIntvlInMin", Gen_ISPrbIntvlInMin, sectionName,"5");
            iniFile.Write("OOSPrbIntvlInMin", Gen_OOSPrbIntvlInMin, sectionName,"3");
            iniFile.Write("ProbeTimeOutInSec", Gen_ProbeTimeOutInSec, sectionName,"30");

            sectionName = "GDS";
            iniFile.Write("GCHostName", GDS_GCHostName, sectionName, "genfare");
            iniFile.Write("GCIP", GDS_GCIP, sectionName,"1.1.1.1");
            iniFile.Write("GCTCPPort", GDS_GCTCPPort, sectionName,"3000");
            iniFile.Write("FBXTCPPort", GDS_FBXTCPPort, sectionName,"3000");

            sectionName = "WIFI_GEN";
            iniFile.Write("APConIntvlStatus", WIFI_GEN_APConIntvlStatus, sectionName,"ENABLED");
            iniFile.Write("APScanTimoutInMin", WIFI_GEN_APScanTimoutInMin, sectionName,"60");
            iniFile.Write("MinRSSIThreshold", WIFI_GEN_Min_RSSIThreshold, sectionName,"70");
            iniFile.Write("MaxRSSIThreshold", WIFI_GEN_Max_RSSIThreshold, sectionName,"80");
            iniFile.Write("WifiNetMode", WIFI_WifiNetMode, sectionName,"INFRASTRUCTURE");
            iniFile.Write("IBBSMode", WIFI_GEN_IBBSMode, sectionName,"NA");
            iniFile.Write("WifiAPCount", WIFI_GEN_WifiAPCount, sectionName,"1");
            iniFile.Write("CapabilityFlags1", WIFI_GEN_CapabilityFlags1, sectionName,"0x01020304");

            sectionName = "WIFI_AP1";
            iniFile.Write("SSID", WIFI_AP1_SSID, sectionName,"");
            iniFile.Write("Passkey", WIFI_AP1_Passkey, sectionName,"");
            iniFile.Write("SecType", WIFI_AP1_SecType, sectionName,"WPA2");
            iniFile.Write("DHCPMode", WIFI_AP1_DHCPMode, sectionName,"DYNAMIC");
            iniFile.Write("FBXIP", WIFI_AP1_FBXIP, sectionName,"0.0.0.0");
            iniFile.Write("SubNetMask", WIFI_AP1_SubNetMask, sectionName,"0.0.0.0");
            iniFile.Write("DefaultGateway", WIFI_AP1_DefaultGateway, sectionName, "0.0.0.0");
            iniFile.Write("DNSPri", WIFI_AP1_DNSPri, sectionName, "0.0.0.0");
            iniFile.Write("DNSSec", WIFI_AP1_DNSSec, sectionName, "0.0.0.0");

            sectionName = "WIFI_AP2";
            iniFile.Write("SSID", WIFI_AP2_SSID, sectionName,"");
            iniFile.Write("Passkey", WIFI_AP2_Passkey, sectionName,"");
            iniFile.Write("SecType", WIFI_AP2_SecType, sectionName,"WPA2");
            iniFile.Write("DHCPMode", WIFI_AP2_DHCPMode, sectionName,"DYNAMIC");
            iniFile.Write("FBXIP", WIFI_AP2_FBXIP, sectionName,"0.0.0.0");
            iniFile.Write("SubNetMask", WIFI_AP2_SubNetMask, sectionName,"0.0.0.0");
            iniFile.Write("DefaultGateway", WIFI_AP2_DefaultGateway, sectionName,"0.0.0.0");
            iniFile.Write("DNSPri", WIFI_AP2_DNSPri, sectionName,"0.0.0.0");
            iniFile.Write("DNSSec", WIFI_AP2_DNSSec, sectionName, "0.0.0.0");

            sectionName = "ETHERNET";
            iniFile.Write("DHCPMode", ETHERNET_DHCPMode, sectionName,"");
            iniFile.Write("FBXIP", FETHERNE_TBXIP, sectionName,"");
            iniFile.Write("SubNetMask", ETHERNET_SubNetMask, sectionName,"");
            iniFile.Write("DefaultGateway", ETHERNET_DefaultGateway, sectionName,"");
            iniFile.Write("DNSPri", ETHERNET_DNSPri, sectionName,"");
            iniFile.Write("DNSSec", ETHERNET_DNSSec, sectionName,"");
            iniFile.Write("CapabilityFlags1", ETHERNET_CapabilityFlags1, sectionName,"");


            sectionName = "CELLMODEM";
            iniFile.Write("CellModemType", CellModem_Type, sectionName,"");
            iniFile.Write("CellSignalThreshold", CellSignal_Threshold, sectionName,"-65");
            iniFile.Write("CellModemStatus", CellModem_Status, sectionName,"ENABLED");

            sectionName = "GPS";
            iniFile.Write("GPSIP", GPS_IP, sectionName,"192.168.1.100");
            iniFile.Write("GPSNetProto", GPS_NetProto, sectionName,"UDP");
            iniFile.Write("GPSPort", GPS_Port, sectionName,"5067");
            iniFile.Write("GPSMsgFormat", GPS_MsgFormat, sectionName,"NMEA");
            iniFile.Write("GPSModemStatus", GPS_ModemStatus, sectionName,"ENABLED");
            iniFile.Write("GPSMaxTransTimeDelta", GPS_MaxTransTimeDelta, sectionName,"10");
            iniFile.Write("GPSMaxTransPositionDelta", GPS_MaxTransPositionDelta, sectionName,"3");

            sectionName = "CLOUD";

            iniFile.Write("MbTkHost", MbTkHost, sectionName,"");
            iniFile.Write("MbTkPath", MbTkPath, sectionName,"");
            iniFile.Write("MbTkCredential", MbTkCredential, sectionName,"");

            iniFile.Write("GenLinkHost", GenLink_Host, sectionName,"");
            iniFile.Write("GenLinkSetupPath", GenLink_SetupPath, sectionName,"");
            iniFile.Write("GenLinkSetupCredential", GenLink_SetupCredential, sectionName, "");
            iniFile.Write("GenLinkDeviceAuthPath", GenLink_DeviceAuthPath, sectionName,"");
            iniFile.Write("GenLinkEnv", GenLink_Env, sectionName,"");
            iniFile.Write("GenLinkTenant", GenLink_Tenant, sectionName,"");
            iniFile.Write("GenLinkHttpsStat", GenLink_HttpsStat, sectionName,"DISABLED");

            iniFile.Write("GDSUpgradeHost", GenLink_GDSUpgradeHost, sectionName,"");
            iniFile.Write("GDSUpgradePath", GenLink_GDSUpgradePath, sectionName,"");
            iniFile.Write("GDSUpgradeCredential", GenLink_GDSUpgradeCredential, sectionName,"");
            iniFile.Write("CapabilityFlags1", GenLink_CapabilityFlags1, sectionName,"0x1234");
        }
    }

    public class gfiIniFileData
    {
        public List<iniCommentsData> iniComments = new List<iniCommentsData>();
        public static string[] UDA_Default = {
            "","","",
            @"&Dump Raw Data;gfiview.exe",
            @"&Restart Genfare INIT Service;RINIT",
            @"&Open Genfare Base Directory in Windows Explorer;GFIDIR",
            @"Edit &GFI.INI File;GFIINI",
            @"Edit &INITTAB File;INITTAB",
            @"ODBC Data Source &Administrator;odbcad32.exe",
            @"&Command Prompt;cmd.exe"
        };

        public bool[] muxEnabled = new bool[3];
        public bool[] vltEnabled = new bool[5];
        public bool enableDatabase = false; 
        public bool enableWifiProbing = false;
        public bool enableProbeServer = false;
        public bool enableRfsrvr = false;
        public bool enableRfdevmgr = false;
        public bool enableClnt = false;
        public bool enableVndldr = false;
        public bool enableMobileTicketing = false;
        public bool enableUDPListener = false;
        public bool enableUserDefinedActions = false;

        public MuxCommon[] mux;// = new MuxCommon[2];         // Mux2-Mux2
        public VltCommon[] vlt;// = new VltCommon[5];        // Cashbox id computers in use VLT1-VLT5
        
        // INIT section
        public string init_nVaultsPerController = "2";  // # Vaults epr cashbox ID computer (2 or 4)
        // MUX Section
        public string V4MaxFS = "10";              //	PROFILE_LONG,	&V4MaxFS,
        public string V4DLSTime = "0";             //	PROFILE_LONG,	&V4DLSTime,
        public string V4STDTime = "0";             //	PROFILE_LONG,	&V4STDTime,
        public string ConvertBadCashboxes = "0";   //	PROFILE_LONG,   &ConvertBadCashboxes,
        public string MuxFlags = "0";              //	PROFILE_LONG,	&_MuxFlags,
        public string MuxFlags2 = "0";             //	PROFILE_LONG,	&_MuxFlags2,
        public string MixFleetMode = "2";          //	PROFILE_LONG,	&_MixFleetMode,
        public string MuxDbgFlags = "0";           //	PROFILE_LONG,	&_MuxDbgFlags,

        public string DupFilterTime;               //	PROFILE_LONG,	&DupFilterTime,
        public string MaxEvents = "500";            //	PROFILE_LONG,	&MaxEvents,
        public string AdjustDirType = "0";         //	PROFILE_LONG,	&AdjustDirType,
        public string Sync = "0";                  //	PROFILE_LONG,	&Sync,

        public string MCP = "";                     // Memory Clear Password
        public string ProbeTRiM = "0";             //	PROFILE_LONG,	&ProbeTRiM,
        public string revstatus = "5";             //	PROFILE_CHAR,	&DefFbxCnf.revstatus,
        public string Passback = "5";              //	PROFILE_CHAR,	&DefFbxCnf.passback,
        public string logon = "0x00";                 //	PROFILE_CHAR,	&DefFbxCnf.logon,
        public string hourlyevent = "0x00";           //	PROFILE_CHAR,	&DefFbxCnf.hourlyevent,
        public string Dump = "30";                 //	PROFILE_CHAR,	&DefFbxCnf.dump,
        public string daylight = "0x01";              //	PROFILE_CHAR,	&DefFbxCnf.daylight,
        public string mf = "0xFFFF";                //	PROFILE_WORD,	&DefFbxCnf.mf,

        public string ver = "102";                 //	PROFILE_WORD,	&DefFbxCnf.ver, 
        public string AgencyCode = "0";            //	PROFILE_WORD,	&DefFbxCnf.AgencyCode, 
        public string OOS_Coin_Bill = "0x00";         //	PROFILE_CHAR,	&DefFbxCnf.OOS_Coin_Bill,
        public string LA_MetroOption = "0x00";        //	PROFILE_CHAR,	&DefFbxCnf.LA_MetroOption,
        public string MemNearFull = "0";           //	PROFILE_CHAR,	&DefFbxCnf.MemNearFull,
        public string BillFull = "0";              //	PROFILE_WORD,	&DefFbxCnf.BillFull,
        public string CoinFull = "0";              //	PROFILE_WORD,	&DefFbxCnf.CoinFull,
        public string BillTimeOut = "0";           //	PROFILE_CHAR,	&DefFbxCnf.BillTimeout,
        public string J1708MID = "0";              //	PROFILE_CHAR,	&DefFbxCnf.J1708MID,
        public string ChangeCardThreshold = "0";   //	PROFILE_WORD,	&ChangeCardThreshold,

        public string MagSecurityCodes = "0x00000000";      //	PROFILE_CHAR,	&MagSecurityCodes,
        public string SCardSecurityCodes = "0x00000000";    //	PROFILE_CHAR,	&SCardSecurityCodes, 
        public string ProbeType = "0";             //	PROFILE_LONG,	&ProbeType,
        public string PDUProbeNumber = "9";        //	PROFILE_LONG,	&PDUProbeNumber,
        public string ProbeID = "0";               //	PROFILE_LONG,	&ProbeID,
        public string UserID = "0";                //	PROFILE_LONG,	&UserID,
        public string VaultProbe = "1";            //	PROFILE_LONG,	&VaultProbe,
        public string StopPointID = "-1";          //	PROFILE_LONG,	&StopPointID,
        public string OperatorID = "0";            //	PROFILE_LONG,	&OperatorID,
        public string UseLockCodeFile = "0";       //   PROFILE_LONG,   &UseLockCodeFile
        public string LockFile = "";               //	PROFILE_LONG,	&LockFile,
        public string J1708Poll = "0x00";          //	PROFILE_CHAR,	&DefFbxCnf.J1708Poll,
        public string TransferPrintOption = "0x00";//	PROFILE_CHAR,	&DefFbxCnf.TransferPrintOption,

        public string OldAgencyCode = "0";         //	PROFILE_WORD,	&DefFbxCnf.OldAgencyCode,
        public string BillsTaken = "0";            //	PROFILE_CHAR,	&DefFbxCnf.BillsTaken,
        public string EvTime = "0";           //	PROFILE_CHAR,	&DefFbxCnf.EventPeriod,
        public string ProbeWatchDog = "0";         //	PROFILE_LONG,	&ProbeWatchdog,
        public string LEDTime = "5";               //	PROFILE_CHAR,	&LEDTime,
        public string FSMask = "0x83FF";           //	PROFILE_LONG,	&FSMaskV4, 
        public string FSMask2 = "0x03FFFFFF";      //	PROFILE_LONG,	&FSMaskV4, 
        public string DST = "0";                   //	PROFILE_WORD,	&DST,
        public string STD = "0";                   //	PROFILE_WORD,	&STD,
        public string DS_RF_NAME = "";             //	PROFILE_STRING,	DS_RF_NAME,
        public string GMTOffSet = "0";             //	PROFILE_CHAR,	&GMTOffset,
        public string CalcSummaryCount = "0";      //  PROFILE_LONG     &CalcSummaryCount 
        // General Wifi Settings
        public string FbxVersion = "0";            //	PROFILE_LONG,	&FbxVersion,
        public string PingBus = "0";               //	PROFILE_LONG,	&PingBus,
        public string FBXNetwork = "10.1";         //	PROFILE_STRING, FBXNetwork,
        public string DS_RF_IP = "0.0.0.0";               //	PROFILE_STRING, DS_RF_IP,
        public string UseDHCP = "0";               //	PROFILE_LONG,	&UseDHCP,
        public string HostBits = "16";             //	PROFILE_LONG,	&HostBits,
        public string Topology = "0";              //	PROFILE_LONG,	&Topology,
        public string Security = "2";              //	PROFILE_LONG,	&Security,
        public string NetworkMode = "0";           //	PROFILE_LONG,	&NetworkMode,
        public string FbxPort = "601";             //	PROFILE_WORD,	&FbxPort,
        public string SSID = "";                    // SSID
        public string KeyPhrase = "";               // KeyPhrase

        public string DsPort = "601";              //	PROFILE_WORD,	&DsPort,
        public string Encryption = "0";            //	PROFILE_LONG,	&Encryption,
        public string Authentication = "0";        //	PROFILE_LONG,	&Authentication,
        public string RfCnfMask = "0";             //	PROFILE_LONG,	&RfCnfMask,
        public string RFPowerCycleTime = "0";      //	PROFILE_LONG,	&RFPowerCycleTime,
        public string RFVersion = "1.0.0.1";       //  PROFILE_STRING, RFVersion,
        public string FtoTime = "2000";            //	PROFILE_LONG,	&FtoTime,
        public string InterProbeTime = "0";        //	PROFILE_LONG,	&InterProbeTime,
        public string PollTime = "0";              //	PROFILE_LONG,	&PollTime,
        public string MuxTime = "500";             //	PROFILE_LONG,	&MuxTime,

        public string MaxDiffRxByteCnt = "2";      //	PROFILE_LONG,	&MaxDiffRxByteCnt,
        public string MaxPrbFrmErr = "3";          //	PROFILE_LONG,	&MaxPrbFrmErr,
        public string MaxPrbBrkErr = "3";          //	PROFILE_LONG,	&MaxPrbBrkErr,

        public string THeader1 = "\\BGFI";         //  PROFILE_STRING, DefTrimCnf.line[0].s,
        public string THeader2 = "\\BGENFARE";     //  PROFILE_STRING, DefTrimCnf.line[1].s,
        public string THeader3 = "\\SA UNIT OF SPX CORPORATION"; // PROFILE_STRING, DefTrimCnf.line[2].s,
        public string TLayout = "_ I B R D E";	   //	PROFILE_STRING, DefTrimCnf.line[3].s,
        public string LCDHeader = "ODYSSEY";       //	PROFILE_STRING, DefTrimCnf.line[4].s,
        public string VErr_Prnt = "0304\\P\\N\\V"; //	PROFILE_STRING,	DefTrimCnf.VErr,
        public string LA_NoMultifareDetail = "0";  //	PROFILE_LONG,	&LA_NoMultifareDetail,
        public string AcctMinRecharge = "0";
        public string PassportTTP = "0";
        public string DumpSQL = "0";
        public string DebugLevel = "0";             //	PROFILE_LONG,	&DebugLevel,
        public string DumpTransactions = "0";       //	PROFILE_LONG,	&DumpTransactions,
        public string DirType = "1";                //	PROFILE_LONG,	&DirType,
        //public string FbxVersion = "0";             //	PROFILE_LONG,	&FbxVersion,

        // ProbeAgent
        public string PRBFlags = "0x00000000";
        public string PRBFlags2 = "0x00000000";
        public string GDS_GCIP = "";
        public string GDS_GCTCPPort = "3000";

        // Database Section
        public string ENG;                          // dtabase start command
        public string DBF;                          // dtabase start command
        public string START;                        // dtabase start command
        public string SecDBF;
        public string BkUp;

        // Databuild-157
        public string FBX_IP = "0.0.0.0";            //	PROFILE_STRING, Fbx_Ip,					
        public string Gateway = "0.0.0.0";           //  PROFILE_STRING, Gateway,				
        public string DNS_IP = "0.0.0.0";            //	PROFILE_STRING, DNS_Ip,					

        // Option Flags
        public string[] optionFlags = new string[32];
        public string[] def_optionFlags = {"0x8C6B","0x8001","0x8004","0x00","0x0006","0x0101","0x47E0","0x8000","0x0008","0x0030","0x0000",
                                              "0x0660","0x0800","0x0000","0x1000","0x0000","0x3F00","0x10C0","0x0001","0x0000","0x0000",
                                              "0x0000","0xFF00","0x0010","0x0000","0x0000","0x0000","0x0000","0x0000","0x0000","0x0000","0x0000"};
        // Client Section
        public string Clnt_DebugLevel="0";
        public string Clnt_WaitTime = "100000";
        public string Clnt_MaxConnRetry = "5";
        public string RAS_usr = "";
        public string RAS_domain = "";
        public string RAS_pwd = "";

        // Mobile Ticketing Section
        public string Mbtk_MbtkFlags = "0";
        public string Mbtk_DebugLevel = "1";
        public string Mbtk_DomainName = "api.genfaremobiledemo.com";
        public string Mbtk_PollInterval= "10";
        public string Mbtk_TransitID = "Genfare";
        public string Mbtk_Authorization = " ";
        public string Mbtk_TLS = "0";

        // UDPListener Section Settings
        public string UDP_Folder = "";
        public string UDP_DebugLevel ="0";
        public string UDP_TestOnly = "0";
        public string UDP_SaveUDPXML = "0";
        public string UDP_SaveUDPRSP = "0";
        public string UDP_Url = "";
        public string UDP_Token= "";

        // User Defined Actions
        public string UDA_Format = @"[Label;Program;Command Line Options] e.g. 1=PinBall Game;C:\Program Files\Windows NT\Pinball\PINBALL.EXE";
        public string[] UDA_Lines = new string[10];

        // ProbAgent Section
        public string Prb_DebugLevel="0";
        public string Prb_Command="probagent.exe";
        public string Prb_PrbEvntThreshold = "1";
        public string Prb_PrbTrnsThreshold = "1";
        public string Prb_NatTunnel ="0";
        public string Prb_ProbeMode = "0";
        public string Prb_GCIP = "";
        public string Prb_GCTCPPort = "";

        // INIT section
        public string Init_nVaultsPerController =  "2";
        public string Init_NoDatabase = "0";

        // Vndldr section
        public string VND_DebugLevel    = "0";
        public string VND_VndFlags      = "0x0000";
        public string VND_InConnStr     = "";
        public string VND_UID           = "";
        public string VND_PWD           = "";
        public string VND_SampleCount   = "2";
        public string VND_SampleTime    = "500";

        //rfdevmgr section
        public string RFDEVMGR_DebugLevel = "0";
        public string RFDEVMGR_InserviceProbe = "1";
        public string RFDEVMGR_SaveWifiStatus = "0";
        public string RFDEVMGR_PrbEvntThreshold = "1";
        public string RFDEVMGR_PrbTrnsThreshold = "1";
        public string RFDEVMGR_Command = "ds_webreq.exe";
        public string RFDEVMGR_GCWIFIFLAGS = "0x00000013";
        public string RFDEVMGR_AUTOLOADFILE = "gaci.bin";
        public string RFDEVMGR_FBXCBFWFILE = "27248";
        public string RFDEVMGR_FBXLIDFWFILE = "27240";
        public string RFDEVMGR_LanguageFontFile = "lfnt103.bin";
        public string RFDEVMGR_FBXFWUPGBUSES = "0";
        public string RFDEVMGR_RetryCount = "3";

        //rfsrvr section
        public string RFSRVR_FbxPollTime       = "300"  ;
        public string RFSRVR_PowerOffTime      = "60"  ;
        public string RFSRVR_PrbTimeThreshold  = "300"  ;
        public string RFSRVR_PrbEvntThreshold  = "20"  ;
        public string RFSRVR_PrbTrnsThreshold  = "20"  ;
        public string RFSRVR_KtFareThreshold   = "200"  ;
        public string RFSRVR_AlarmFlagsMask    = "0xf5" ;
        public string RFSRVR_DebugLevel        = "3"    ;
        public string RFSRVR_MaxWifiConnections = "240";
        //public string RFSRVR_SaveWifiStatus = "0";
        public string RFSRVR_BusProbeList = "";
        public string RFSRVR_RfPrbFlags = "0x00000001";
        public bool bUseDefaults = true;            // Clear dirty and enabled bit

        ///////////////////////////////////////////////////////
        // This reads the gfi.ini File
        public void ReadIniFile(IniFile iniFile)
        {
            string sectionName;
            for (int i = 0; i < 3; i++)
                muxEnabled[i] = false;
            for (int i = 0; i < 5; i++)
                vltEnabled[i] = false;

            enableWifiProbing = false;
            enableMobileTicketing=false;
            enableVndldr = false;
            enableDatabase = false;
            enableProbeServer = false;
            enableUDPListener = false;
            // Set file name
            //iniFile.IniFileName = ConfigurationWinForm.SplitForm.Constants.GFIINI;
            //iniFile.IniFileName = iniFile.fConfigurationWinForm.SplitForm.Constants.GFIINI;

            // Read INIT section
            sectionName = "INIT";
            Init_nVaultsPerController = iniFile.ReadLong("VaultCount", sectionName, "2");
            Init_NoDatabase = iniFile.ReadLong("NoDatabase", sectionName, "0");

            // Read Vndldr section
            sectionName = "Vndldr";
            enableVndldr = iniFile.DoesSectionExist(sectionName);
            VND_DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "0");
            VND_VndFlags = iniFile.ReadHex("VndFlags", sectionName, "0x0000");
            VND_InConnStr = iniFile.Read("InConnStr", sectionName, "None");

            VND_UID = iniFile.Read("UID", sectionName, "");
            if (VND_UID.Trim().Length>0)
                VND_UID = iniFile.DecryptString(VND_UID);

            VND_PWD = iniFile.Read("PWD ", sectionName, "");
            if (VND_PWD.Trim().Length > 0)
                VND_PWD = iniFile.DecryptString(VND_PWD);

            VND_SampleCount = iniFile.ReadLong("SampleCount", sectionName, "2");
            VND_SampleTime = iniFile.ReadLong("SampleTime", sectionName, "500");

            // Read the MUX1 Section
            muxEnabled[0] = true;

            sectionName = "MUX1";
            V4MaxFS = iniFile.ReadLong("BaudRate", sectionName, "10");                      //	PROFILE_LONG,	&V4MaxFS,
            V4DLSTime = iniFile.ReadLong("V4DLSTime", sectionName, "0");                    //	PROFILE_LONG,	&V4DLSTime,
            V4STDTime = iniFile.ReadLong("V4STDTime", sectionName, "0");                    //	PROFILE_LONG,	&V4STDTime,
            ConvertBadCashboxes = iniFile.ReadLong("ConvertBadCashboxes", sectionName, "0"); //	PROFILE_LONG,   &ConvertBadCashboxes,
            MuxFlags = iniFile.ReadHex("MuxFlags", sectionName, "0x00000000");              //	PROFILE_LONG,	&_MuxFlags,
            // Set Mux Flag Defaults
            // Force v2 FS and Config
            // Enable v2 FS and Config
            //MuxFlags |= 0x00005060;
            MuxFlags2 = iniFile.ReadHex("MuxFlags2", sectionName, "0x00000000");            //	PROFILE_LONG,	&_MuxFlags2,
            MixFleetMode = iniFile.ReadLong("MixFleetMode", sectionName, "2");              //	PROFILE_LONG,	&_MixFleetMode,
            MuxDbgFlags = iniFile.ReadHex("MuxDbgFlags", sectionName, "0x00000000");        //	PROFILE_LONG,	&_MuxDbgFlags,

            DupFilterTime = iniFile.ReadLong("DupFilterTime", sectionName, "0");            //	PROFILE_LONG,	&DupFilterTime,
            MaxEvents = iniFile.ReadLong("MaxEvents", sectionName, "500");                    //	PROFILE_LONG,	&MaxEvents,
            AdjustDirType = iniFile.ReadLong("AdjustDirType", sectionName, "0");            //	PROFILE_LONG,	&AdjustDirType,
            Sync = iniFile.ReadLong("Sync", sectionName, "0");                              //	PROFILE_LONG,	&Sync,
            MCP = iniFile.Read("MCP", sectionName, "");  

            ProbeTRiM = iniFile.ReadLong("ProbeTRiM", sectionName, "0");                    //	PROFILE_LONG,	&ProbeTRiM,
            revstatus = iniFile.ReadHex("revstatus", sectionName, "0x20");                    //	PROFILE_CHAR,	&DefFbxCnf.revstatus,
            Passback = iniFile.ReadLong("Passback", sectionName, "5");                      //	PROFILE_CHAR,	&DefFbxCnf.passback,
            logon = iniFile.ReadHex("logon", sectionName, "0x00");                            //	PROFILE_CHAR,	&DefFbxCnf.logon,
            hourlyevent = iniFile.ReadHex("hourlyevent", sectionName, "0x00");                //	PROFILE_CHAR,	&DefFbxCnf.hourlyevent,
            Dump = iniFile.ReadLong("Dump", sectionName, "30");                             //	PROFILE_CHAR,	&DefFbxCnf.dump,
            daylight = iniFile.ReadHex("daylight", sectionName, "0x01");                      //	PROFILE_CHAR,	&DefFbxCnf.daylight,
            mf = iniFile.ReadHex("mf", sectionName, "0xFFFF");                              //	PROFILE_WORD,	&DefFbxCnf.mf,

            ver = iniFile.ReadLong("ver", sectionName, "102");                              //	PROFILE_WORD,	&DefFbxCnf.ver, 
            AgencyCode = iniFile.ReadLong("AgencyCode", sectionName, "0");                  //	PROFILE_WORD,	&DefFbxCnf.AgencyCode, 
            OOS_Coin_Bill = iniFile.ReadHex("OOS_Coin_Bill", sectionName, "0x00");            //	PROFILE_CHAR,	&DefFbxCnf.OOS_Coin_Bill,
            LA_MetroOption = iniFile.ReadHex("LA_MetroOption", sectionName, "0x00");          //	PROFILE_CHAR,	&DefFbxCnf.LA_MetroOption,
            MemNearFull = iniFile.ReadLong("MemNearFull", sectionName, "0");                //	PROFILE_CHAR,	&DefFbxCnf.MemNearFull,
            BillFull = iniFile.ReadLong("BillFull", sectionName, "0");                      //	PROFILE_WORD,	&DefFbxCnf.BillFull,
            CoinFull = iniFile.ReadLong("CoinFull", sectionName, "0");                      //	PROFILE_WORD,	&DefFbxCnf.CoinFull,
            BillTimeOut = iniFile.ReadLong("BillTimeOut", sectionName, "0");                //	PROFILE_CHAR,	&DefFbxCnf.BillTimeout,
            J1708MID = iniFile.ReadLong("J1708MID", sectionName, "0");                      //	PROFILE_CHAR,	&DefFbxCnf.J1708MID,
            ChangeCardThreshold = iniFile.ReadLong("ChangeCardThreshold", sectionName, "0");//	PROFILE_WORD,	&ChangeCardThreshold,

            MagSecurityCodes = iniFile.ReadHex("MagSecurityCodes", sectionName, "0x00000000");      //	PROFILE_CHAR,	&MagSecurityCodes,
            SCardSecurityCodes = iniFile.ReadHex("SCardSecurityCodes", sectionName, "0x00000000");  //	PROFILE_CHAR,	&SCardSecurityCodes, 
            ProbeType = iniFile.ReadLong("ProbeType", sectionName, "0");                    //	PROFILE_LONG,	&ProbeType,
            PDUProbeNumber = iniFile.ReadLong("PDUProbeNumber", sectionName, "9");          //	PROFILE_LONG,	&PDUProbeNumber,
            ProbeID = iniFile.ReadLong("ProbeID", sectionName, "0");                        //	PROFILE_LONG,	&ProbeID,
            UserID = iniFile.ReadLong("UserID", sectionName, "0");                          //	PROFILE_LONG,	&UserID,
            VaultProbe = iniFile.ReadLong("VaultProbe", sectionName, "1");                  //	PROFILE_LONG,	&VaultProbe,
            StopPointID = iniFile.ReadLong("StopPointID", sectionName, "-1");               //	PROFILE_LONG,	&StopPointID,

            UseLockCodeFile = iniFile.ReadLong("UseLockCodeFile", sectionName, "0");        //	PROFILE_LONG,	&OperatorID,
            OperatorID = iniFile.ReadLong("OperatorID", sectionName, "0");                  //	PROFILE_LONG,	&OperatorID,
            LockFile = iniFile.Read("LockFile", sectionName, "");                           //	PROFILE_LONG,	&LockFile,

            J1708Poll = iniFile.ReadHex("J1708Poll", sectionName, "0x00");                    //	PROFILE_CHAR,	&DefFbxCnf.J1708Poll,
            TransferPrintOption = iniFile.ReadHex("TransferPrintOption", sectionName, "0x00");          //	PROFILE_CHAR,	&DefFbxCnf.TransferPrintOption,

            OldAgencyCode = iniFile.ReadLong("OldAgencyCode", sectionName, "0");            //	PROFILE_WORD,	&DefFbxCnf.OldAgencyCode,
            BillsTaken = iniFile.ReadHex("BillsTaken", sectionName, "0");                  //	PROFILE_CHAR,	&DefFbxCnf.BillsTaken,
            EvTime = iniFile.ReadLong("EvTime", sectionName, "0");                //	PROFILE_CHAR,	&DefFbxCnf.EventPeriod,
            ProbeWatchDog = iniFile.ReadLong("ProbeWatchDog", sectionName, "0");            //	PROFILE_LONG,	&ProbeWatchdog,
            LEDTime = iniFile.ReadLong("LEDTime", sectionName, "5");                        //	PROFILE_CHAR,	&LEDTime,
            FSMask = iniFile.ReadHex("FSMask", sectionName, "0x083FF");                        //	PROFILE_LONG,	&FSMaskV4, 
            FSMask2 = iniFile.ReadHex("FSMask2", sectionName, "0x03FFFFFF");                        //	PROFILE_LONG,	&FSMaskV4, 
            DST = iniFile.ReadHex("DST", sectionName, "0");                                 //	PROFILE_WORD,	&DST,
            STD = iniFile.ReadHex("STD", sectionName, "0");                                 //	PROFILE_WORD,	&STD,
            DS_RF_NAME = iniFile.Read("DS_RF_NAME", sectionName, "");                       //	PROFILE_STRING,	DS_RF_NAME,
            GMTOffSet = iniFile.ReadFloat("GMTOffSet", sectionName, "0");                    //	PROFILE_CHAR,	&GMTOffset,
            CalcSummaryCount = iniFile.ReadLong("CalcSummaryCount", sectionName, "0");      //
            FbxVersion = iniFile.ReadLong("FbxVersion", sectionName, "0");                  //	PROFILE_LONG,	&FbxVersion,
            PingBus = iniFile.ReadLong("PingBus", sectionName, "0");                        //	PROFILE_LONG,	&PingBus,
            FBXNetwork = iniFile.Read("FBXNetwork", sectionName, "10.1");                   //	PROFILE_STRING, FBXNetwork,
            DS_RF_IP = iniFile.Read("DS_RF_IP", sectionName, "0.0.0.0.");                           //	PROFILE_STRING, DS_RF_IP,
            UseDHCP = iniFile.ReadLong("UseDHCP", sectionName, "0");                            //	PROFILE_LONG,	&UseDHCP,
            HostBits = iniFile.ReadLong("HostBits", sectionName, "16");                     //	PROFILE_LONG,	&HostBits,
            Topology = iniFile.ReadLong("Topology", sectionName, "0");                      //	PROFILE_LONG,	&Topology,
            Security = iniFile.ReadLong("Security", sectionName, "2");                      //	PROFILE_LONG,	&Security,
            NetworkMode = iniFile.ReadLong("NetworkMode", sectionName, "0");                //	PROFILE_LONG,	&NetworkMode,
            FbxPort = iniFile.ReadLong("FbxPort", sectionName, "601");                      //	PROFILE_WORD,	&FbxPort,

            SSID = iniFile.Read("SSID", sectionName, "");                      //	PROFILE_WORD,	&FbxPort,
            if (SSID.Trim().Length > 0)
                SSID = iniFile.DecryptString(SSID);
            else
                SSID = "";
            KeyPhrase = iniFile.Read("KeyPhrase", sectionName, "");                      //	PROFILE_WORD,	&FbxPort,
            if (KeyPhrase.Trim().Length > 0)
                KeyPhrase = iniFile.DecryptString(KeyPhrase);
            else
                KeyPhrase = "";
            DsPort = iniFile.ReadLong("DsPort", sectionName, "601");                        //	PROFILE_WORD,	&DsPort,
            Encryption = iniFile.ReadLong("Encryption", sectionName, "0");                  //	PROFILE_LONG,	&Encryption,
            Authentication = iniFile.ReadLong("Authentication", sectionName, "0");          //	PROFILE_LONG,	&Authentication,
            RfCnfMask = iniFile.ReadLong("RfCnfMask", sectionName, "0");                    //	PROFILE_LONG,	&RfCnfMask,
            RFPowerCycleTime = iniFile.ReadLong("RFPowerCycleTime", sectionName, "0");      //	PROFILE_LONG,	&RFPowerCycleTime,
            RFVersion = iniFile.Read("RFVersion", sectionName, "1.0.0.1");                  //  PROFILE_STRING, RFVersion,
            FtoTime = iniFile.ReadLong("FtoTime", sectionName, "2000");                     //	PROFILE_LONG,	&FtoTime,
            InterProbeTime = iniFile.ReadLong("InterProbeTime", sectionName, "0");          //	PROFILE_LONG,	&InterProbeTime,
            PollTime = iniFile.ReadLong("PollTime", sectionName, "0");                      //	PROFILE_LONG,	&PollTime,
            MuxTime = iniFile.ReadLong("MuxTime", sectionName, "500");                      //	PROFILE_LONG,	&MuxTime,

            MaxDiffRxByteCnt = iniFile.ReadLong("MaxDiffRxByteCnt", sectionName, "2");      //	PROFILE_LONG,	&MaxDiffRxByteCnt,
            MaxPrbFrmErr = iniFile.ReadLong("MaxPrbFrmErr", sectionName, "3");              //	PROFILE_LONG,	&MaxPrbFrmErr,
            MaxPrbBrkErr = iniFile.ReadLong("MaxPrbBrkErr", sectionName, "3");              //	PROFILE_LONG,	&MaxPrbBrkErr,

            THeader1 = iniFile.Read("THeader1", sectionName, "\\BGFI");                     //  PROFILE_STRING, DefTrimCnf.line[0].s,
            THeader2 = iniFile.Read("THeader2", sectionName, "\\BGENFARE");                 //  PROFILE_STRING, DefTrimCnf.line[1].s,
            THeader3 = iniFile.Read("THeader3", sectionName, "\\SA UNIT OF SPX CORPORATION"); // PROFILE_STRING, DefTrimCnf.line[2].s,
            TLayout = iniFile.Read("TLayout", sectionName, "_ I B R D E");	                //	PROFILE_STRING, DefTrimCnf.line[3].s,
            LCDHeader = iniFile.Read("LCDHeader", sectionName, "ODYSSEY");                  //	PROFILE_STRING, DefTrimCnf.line[4].s,
            VErr_Prnt = iniFile.Read("VErr_Prnt", sectionName, "0304\\P\\N\\V");            //	PROFILE_STRING,	DefTrimCnf.VErr,
            LA_NoMultifareDetail = iniFile.Read("LA_NoMultifareDetail", sectionName, "0");  //	PROFILE_LONG,	&LA_NoMultifareDetail,

            //build-157
            FBX_IP = iniFile.Read("FBX_IP", sectionName, "0.0.0.0");                        //	PROFILE_STRING, Fbx_Ip,					
            Gateway = iniFile.Read("Gateway", sectionName, "0.0.0.0");                      //  PROFILE_STRING, Gateway,				
            DNS_IP = iniFile.Read("DNS_IP", sectionName, "0.0.0.0");                        //	PROFILE_STRING, DNS_Ip,					
            for (int i = 1; i <= 32; i++)
                optionFlags[i - 1] = iniFile.ReadHex("OptionFlags" + i.ToString(), sectionName, def_optionFlags[i-1]);
            AcctMinRecharge = iniFile.ReadLong("AcctMinRecharge", sectionName, "0");        //	PROFILE_LONG,	&DumpTransactions,
            PassportTTP = iniFile.ReadLong("PassportTTP", sectionName, "0");                //	PROFILE_LONG,	&DumpTransactions,
            DumpSQL = iniFile.ReadLong("DumpSQL", sectionName, "0");                        // DUmp SQL commands for debugging
            DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "0");                  //	PROFILE_LONG,	&DebugLevel,
            DumpTransactions = iniFile.ReadLong("DumpTransactions", sectionName, "0");      //	PROFILE_LONG,	&DumpTransactions,
            DirType = iniFile.ReadLong("DirType", sectionName, "0");                        //	PROFILE_LONG,	&DirType,
            FbxVersion = iniFile.ReadLong("FbxVersion", sectionName, "0");                  //	PROFILE_LONG,	&FbxVersion,

            bool bSaveUseDefaults = bUseDefaults;    // Save defaults switch
            bUseDefaults = true;
            // Read Mux2 and Mux3 sections
            for (int i = 0; i < 3; i++)
            {
                sectionName = "MUX" + (i+1).ToString();
                if (iniFile.DoesSectionExist(sectionName))
                    muxEnabled[i] = true;
                mux[i].Port = iniFile.Read("Port", sectionName, "None");
                if (mux[i].Port.Trim() == "")                                                         //  CommName,	PROFILE_STRING, CommName,
                    mux[i].Port = "None";
                mux[i].PortEnbMask = iniFile.ReadHex("PortEnbMask", sectionName, mux[0].PortEnbMask);   //	PROFILE_WORD,	&PortEnbMask,
                mux[i].OldBaudrate = iniFile.ReadLong("OldBaudrate", sectionName, mux[0].OldBaudrate);  //	PROFILE_LONG,	&OldBaudRate,
                mux[i].Baudrate = iniFile.ReadLong("Baudrate", sectionName, mux[0].Baudrate);           //	PROFILE_LONG,	&BaudRate,
                mux[i].FbxBaudrate = iniFile.ReadLong("FbxBaudrate", sectionName, mux[0].FbxBaudrate);  //	PROFILE_LONG,	&FbxBaudRate,
                mux[i].ScanBaudRate = iniFile.ReadLong("ScanBaudRate", sectionName, mux[0].ScanBaudRate);               //	PROFILE_LONG,	&ScanBaudRate,
                mux[i].MuxIsoBox = iniFile.ReadLong("MuxIsoBox", sectionName, mux[0].MuxIsoBox);        //	PROFILE_LONG,	&MuxIsoBox,
                mux[i].ProbeType = iniFile.ReadLong("ProbeType", sectionName, mux[0].ProbeType);        //	PROFILE_LONG,	&ProbeType,

                mux[i].WaitSync = iniFile.ReadLong("WaitSync", sectionName, mux[0].WaitSync);           //	PROFILE_LONG,	&WaitSync,
                

            }
            bUseDefaults = bSaveUseDefaults;
            // Read VLT1-Vlt5 sections
            for (int i = 0; i < 5; i++)
            {
                sectionName = "VLT" + (i+1).ToString();
                vltEnabled[i] = false;
                //vltDirty[i] = false;
                if (iniFile.DoesSectionExist(sectionName))
                    vltEnabled[i] = true;
                vlt[i].Port = iniFile.Read("Port", sectionName, "COM1");                //  CommName,	PROFILE_STRING, CommName,
                vlt[i].PortEnbMask = iniFile.ReadHex("PortEnbMask", sectionName, "0x0F");         //	PROFILE_WORD,	&PortEnbMask,
                vlt[i].LockOut = iniFile.ReadLong("LockOut", sectionName, "0");
                vlt[i].DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "0");             //	PROFILE_LONG,	&DebugLevel,
                vlt[i].VltFull = iniFile.ReadLong("VltFull", sectionName, "10000");
                vlt[i].VltFlags = iniFile.ReadHex("VltFlags", sectionName, "0x00000000");      //VLTF_DEBUG_PROTOCOL  (ulong)0x00000002 - debug protocol comm. flag 
            }
 
            //////////////////////////////////////////////////////////////////////////////
            // Read Database Section
            sectionName = "Database";
            enableDatabase = iniFile.DoesSectionExist(sectionName);
            ENG=iniFile.Read("ENG", sectionName, "");
            DBF=iniFile.Read("DBF", sectionName, "");
            SecDBF = iniFile.Read("SecDBF", sectionName, "");
            START = iniFile.Read("START", sectionName, "");
            BkUp = iniFile.Read("BkUp", "DATA", "");

            //////////////////////////////////////////////////////////////////////////////
            // Read Clnt/RAS Section
            sectionName = "Clnt";
            enableClnt = iniFile.DoesSectionExist(sectionName);
            Clnt_DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "0");
            Clnt_WaitTime = iniFile.ReadLong("WaitTime", sectionName, "100000");
            Clnt_MaxConnRetry = iniFile.ReadLong("MaxConnRetry", sectionName, "5");
            sectionName = "RAS";
            RAS_usr = iniFile.Read("usr", sectionName, "");
            RAS_domain = iniFile.Read("Clntdomain", sectionName, "");
            RAS_pwd = iniFile.Read("pwd", sectionName, "");

            //////////////////////////////////////////////////////////////////////////////
            // Read Mobil Ticket Section
            sectionName = "MobTckt";
            enableMobileTicketing = iniFile.DoesSectionExist(sectionName);
            Mbtk_MbtkFlags = iniFile.ReadLong("MbtkFlags", sectionName, "0");
            Mbtk_DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "1");
            Mbtk_DomainName = iniFile.Read("DomainName", sectionName, "api.genfaremobiledemo.com");
            Mbtk_PollInterval= iniFile.ReadLong("PollInterval", sectionName, "10");
            Mbtk_TransitID = iniFile.Read("TransitID", sectionName, "Genfare");
            Mbtk_Authorization = iniFile.Read("Authorization", sectionName, " ");
            Mbtk_TLS = iniFile.Read("TLS", sectionName, "0");

            //////////////////////////////////////////////////////////////////////////////
            // Read Mobil Ticket Section            
            sectionName = "UDPListener";
            enableUDPListener = iniFile.DoesSectionExist(sectionName);
            if (enableUDPListener)
            {
                UDP_Folder = iniFile.Read("UDPFolder", sectionName, ""); ;
                UDP_DebugLevel = iniFile.ReadLong("DebugLevel", sectionName, "0");
                UDP_TestOnly = iniFile.ReadLong("UDPTestOnly", sectionName, "0");
                UDP_SaveUDPXML = iniFile.ReadLong("SaveUDPXML", sectionName, "0");
                UDP_SaveUDPRSP = iniFile.ReadLong("SaveUDPRSP", sectionName, "0");
                UDP_Url = iniFile.Read("URL", sectionName, "");
                UDP_Token = iniFile.Read("Token", sectionName, "");
            }
            //////////////////////////////////////////////////////////////////////////////
            // Read User Defined Section
            sectionName = "User Defined Actions";
            enableUserDefinedActions = iniFile.DoesSectionExist(sectionName);
            UDA_Format = iniFile.Read("Format", sectionName, @"[Label;Program;Command Line Options] e.g. 1=PinBall Game;C:\Program Files\Windows NT\Pinball\PINBALL.EXE");
            for (int i = 1; i <= 10; i++)
            {
                UDA_Lines[i - 1] = iniFile.Read(i.ToString(), sectionName, UDA_Default[i-1]);
            }
            //////////////////////////////////////////////////////////////////////////////
            // Read ProbServer Section
            // Set file name
            sectionName = "ProbServer";
            if (iniFile.DoesSectionExist(sectionName))
            {
                this.enableWifiProbing = true;
                enableProbeServer = true;
            }
            Prb_DebugLevel=iniFile.ReadLong("DebugLevel", sectionName, "0");
            Prb_Command=iniFile.Read("Command", sectionName, "probagent.exe");
            Prb_PrbEvntThreshold = iniFile.ReadLong("PrbEvntThreshold", sectionName, "1");
            Prb_PrbTrnsThreshold = iniFile.ReadLong("PrbTrnsThreshold", sectionName, "1");
            Prb_NatTunnel = iniFile.ReadLong("NatTunnel", sectionName, "0");
            Prb_ProbeMode = iniFile.ReadLong("ProbeMode", sectionName, "0");
            PRBFlags = iniFile.ReadHex("PRBFlags", sectionName, "0x00000000");
            PRBFlags2 = iniFile.ReadHex("PRBFlags2", sectionName, "0x00000000");
            Prb_GCIP = iniFile.Read("GCIP", sectionName, "");
            Prb_GCTCPPort = iniFile.Read("GCTCPPort", sectionName, "");


            ////////////////////////////////////////////////////////////////////////////////
            // Read rfdevmgr section
            sectionName="rfdevmgr";
            if (iniFile.DoesSectionExist(sectionName))
            {
                enableWifiProbing = true;
                enableRfdevmgr = true;
            }
            RFDEVMGR_DebugLevel       = iniFile.ReadLong("DebugLevel",      sectionName,"0");
            RFDEVMGR_InserviceProbe = iniFile.ReadLong("InserviceProbe", sectionName, "1");
            RFDEVMGR_SaveWifiStatus = iniFile.ReadLong("SaveWifiStatus", sectionName, "0");
            RFDEVMGR_PrbEvntThreshold = iniFile.ReadLong("PrbEvntThreshold", sectionName, "1");
            RFDEVMGR_PrbTrnsThreshold = iniFile.ReadLong("PrbTrnsThreshold", sectionName, "1");
            RFDEVMGR_Command          = iniFile.Read("Command",         sectionName,"ds_webreq.exe");
            RFDEVMGR_GCWIFIFLAGS      = iniFile.ReadLong("GCWIFIFLAGS",      sectionName,"0x00000013");
            RFDEVMGR_AUTOLOADFILE     = iniFile.Read("AUTOLOADFILE",     sectionName,"gaci.bin");
            RFDEVMGR_FBXCBFWFILE      = iniFile.ReadLong("FBXCBFWFILE",     sectionName,"27248");
            RFDEVMGR_FBXLIDFWFILE     = iniFile.ReadLong("FBXLIDFWFILE",    sectionName,"27240");
            RFDEVMGR_LanguageFontFile = iniFile.Read("LanguageFontFile",sectionName,"lfnt103.bin");
            RFDEVMGR_FBXFWUPGBUSES = iniFile.Read("FBXFWUPGBUSES", sectionName, "0");
            RFDEVMGR_RetryCount = iniFile.ReadLong("RetryCount", sectionName, "3");

            //////////////////////////////////////////////////////////////////////////////////
            // Read rfsrvr section
            sectionName = "rfsrvr";
            if (iniFile.DoesSectionExist(sectionName))
            {
                enableWifiProbing = true;
                enableRfsrvr = true;
            }
            RFSRVR_FbxPollTime       = iniFile.ReadLong("FbxPollTime",sectionName,"300" ) ;
            RFSRVR_PowerOffTime     = iniFile.ReadLong("PowerOffTime", sectionName, "60");
            RFSRVR_PrbTimeThreshold = iniFile.ReadLong("PrbTimeThreshold", sectionName, "300");
            RFSRVR_PrbEvntThreshold = iniFile.ReadLong("PrbEvntThreshold", sectionName, "20");
            RFSRVR_PrbTrnsThreshold = iniFile.ReadLong("PrbTrnsThreshold", sectionName, "20");
            RFSRVR_KtFareThreshold  = iniFile.ReadLong("KtFareThreshold ", sectionName, "200");
            RFSRVR_AlarmFlagsMask   = iniFile.ReadHex("AlarmFlagsMask",sectionName,"0xf5") ;
            RFSRVR_DebugLevel       = iniFile.ReadLong("DebugLevel", sectionName, "3");
            RFSRVR_MaxWifiConnections = iniFile.ReadLong("MaxWifiConnections", sectionName, "240");
            //RFSRVR_SaveWifiStatus   = iniFile.ReadLong("SaveWifiStatus", sectionName, "0");
            RFSRVR_BusProbeList     = iniFile.Read("BusProbeList", sectionName, "");
            RFSRVR_RfPrbFlags       = iniFile.ReadHex("RfPrbFlags", sectionName, "0x00000001");

            SynchronizeFlags();            
        }

        public void SynchronizeFlags()
        {
            // If the RBBL flag in the mux is different from the RBBL flag in PRBFlags, 
            // then set PRBFlags RBBL bit to MUXFlags RBBL Bit
            uint muxflags = Util.ParseValue(MuxFlags);
            uint prbflags = Util.ParseValue(PRBFlags);
            prbflags &= (~((uint)0x00000800));          // Mask out bit
            prbflags |= muxflags & ((uint)0x00000800);  // set bit to value in muxflags
            PRBFlags = "0x" + prbflags.ToString("X8");
        }

        public void WriteIniFile(IniFile iniFile)
        {
            string str="";
            string sectionName;
            //iniFile.IniFileName = ConfigurationWinForm.SplitForm.Constants.GFIINI;    // Set file name

            // Write INIT section
            sectionName = "INIT";
            iniFile.Write("VaultCount", Init_nVaultsPerController, sectionName,"2");
            iniFile.Write("NoDatabase", Init_NoDatabase, sectionName,"0");

            // Write Vndldr section
            if (enableVndldr)
            {
                if ((iniFile.splitForm.globalSettings.locationType == iniFile.splitForm.NETWORK_MANAGER) ||
                    (iniFile.splitForm.globalSettings.locationType == iniFile.splitForm.LOCATION_BOTH))
                {
                    sectionName = "Vndldr";
                    iniFile.Write("DebugLevel", VND_DebugLevel, sectionName, "0");
                    iniFile.Write("VndFlags", VND_VndFlags, sectionName, "0x0000");
                    iniFile.Write("InConnStr", VND_InConnStr, sectionName, "");
                    if (VND_UID.Trim().Length > 0)
                    {
                        VND_UID = iniFile.EncryptString(VND_UID, ref str);
                        iniFile.Write("UID", VND_UID, sectionName, "");
                    }
                    if (VND_PWD.Trim().Length > 0)
                    {
                        VND_PWD = iniFile.EncryptString(VND_PWD, ref str);
                        iniFile.Write("PWD ", VND_PWD, sectionName, "");
                    }

                    iniFile.Write("SampleCount", VND_SampleCount, sectionName, "2");
                    iniFile.Write("SampleTime", VND_SampleTime, sectionName, "500");
                }
            }

            if (muxEnabled[0])
            {
                SynchronizeFlags();
                sectionName = "MUX1";               // Write the MUX1 Section
/*dup*/         iniFile.Write("Port", mux[0].Port, sectionName, "COM1");                 //  CommName,	PROFILE_STRING, CommName,
/*dup*/         iniFile.Write("PortEnbMask", mux[0].PortEnbMask, sectionName, "0x0F");  //	PROFILE_WORD,	&PortEnbMask,
                iniFile.Write("FBXNetwork", FBXNetwork, sectionName, "10.1");                   //	PROFILE_STRING, FBXNetwork,

                //;D a ta  S y s t e m  and F a r e b o x  C o m m u n i c a t i o n s   P o r t S e t t i n g s
                iniFile.Write("DS_RF_IP", DS_RF_IP, sectionName, "0.0.0.0");                           //	PROFILE_STRING, DS_RF_IP,
                iniFile.Write("DsPort", DsPort, sectionName, "601");                        //	PROFILE_WORD,	&DsPort,
                iniFile.Write("FbxPort", FbxPort, sectionName, "601");                      //	PROFILE_WORD,	&FbxPort,
                
                //iniFile.AddIniFileComment("C o m m u n i c a t i o n s   S e t t i n g s");
                //;C o m m u n i c a t i o n s   S e t t i n g s
                iniFile.Write("MuxIsoBox", mux[0].MuxIsoBox, sectionName, "1");       //	PROFILE_LONG,	&MuxIsoBox,
                iniFile.Write("ProbeType", mux[0].ProbeType, sectionName, "0");       //	PROFILE_LONG,	&ProbeType,
                iniFile.Write("OldBaudrate", mux[0].OldBaudrate, sectionName, "0");   //	PROFILE_LONG,	&OldBaudRate,
                iniFile.Write("Baudrate", mux[0].Baudrate, sectionName, "57600");         //	PROFILE_LONG,	&BaudRate,
                iniFile.Write("FbxBaudrate", mux[0].FbxBaudrate, sectionName, "57600");  //	PROFILE_LONG,	&FbxBaudRate,
                iniFile.Write("ScanBaudRate", mux[0].ScanBaudRate, sectionName, "0"); //	PROFILE_LONG,	&ScanBaudRate,
                iniFile.Write("WaitSync", mux[0].WaitSync, sectionName, "1");         //	PROFILE_LONG,	&WaitSync,

                //iniFile.AddIniFileComment("P R O B I N G   C U S T O M I Z A T I O N");
                //;P R O B I N G   C U S T O M I Z A T I O N
                iniFile.Write("DirType", DirType, sectionName, "1");             //	PROFILE_LONG,	&DirType,
                iniFile.Write("DebugLevel", DebugLevel, sectionName, "0");       //	PROFILE_LONG,	&DebugLevel,
                iniFile.Write("DumpTransactions", DumpTransactions, sectionName, "0");  //	PROFILE_LONG,	&DumpTransactions,
                iniFile.Write("DumpSQL", DumpSQL, sectionName, "0");             //	PROFILE_LONG,	&DumpTransactions,
                iniFile.Write("FbxVersion", FbxVersion, sectionName, "0");              //	PROFILE_LONG,	&FbxVersion,
                iniFile.Write("MuxFlags", MuxFlags, sectionName, "0x00000000");         //	PROFILE_LONG,	&_MuxFlags,
                iniFile.Write("MuxFlags2", MuxFlags2, sectionName, "0x00000000");       //	PROFILE_LONG,	&_MuxFlags2,
                iniFile.Write("MuxDbgFlags", MuxDbgFlags, sectionName, "0x00000000");   //	PROFILE_LONG,	&_MuxDbgFlags,
                iniFile.Write("MuxTime", MuxTime, sectionName, "500");                  //	PROFILE_LONG,	&MuxTime,
                iniFile.Write("AcctMinRecharge", AcctMinRecharge, sectionName, "0");    //	PROFILE_LONG,	&AcctMinRecharge,
                iniFile.Write("PassportTTP", PassportTTP, sectionName, "0");            //	PROFILE_LONG,	&PassportTTP,

                //iniFile.AddIniFileComment("F A R E B O X   C O N F I G U R A T I O N   P A R A M E T E R S");
                //;F A R E B O X   C O N F I G U R A T I O N   P A R A M E T E R S
                iniFile.Write("AgencyCode", AgencyCode, sectionName, "0");                  //	PROFILE_WORD,	&DefFbxCnf.AgencyCode, 
                iniFile.Write("OldAgencyCode", OldAgencyCode, sectionName, "0");            //	PROFILE_WORD,	&DefFbxCnf.OldAgencyCode,
                iniFile.Write("Passback", Passback, sectionName, "5");                        //	PROFILE_CHAR,	&DefFbxCnf.passback,
                iniFile.Write("Dump", Dump, sectionName, "30");                            //	PROFILE_CHAR,	&DefFbxCnf.dump,
                iniFile.Write("MaxEvents", MaxEvents, sectionName, "500");                       //	PROFILE_LONG,	&MaxEvents,
                iniFile.Write("LEDTime", LEDTime, sectionName, "5");                        //	PROFILE_CHAR,	&LEDTime,
                iniFile.Write("MemNearFull", MemNearFull, sectionName, "0");                //	PROFILE_CHAR,	&DefFbxCnf.MemNearFull,
                iniFile.Write("BillFull", BillFull, sectionName, "0");                      //	PROFILE_WORD,	&DefFbxCnf.BillFull,
                iniFile.Write("CoinFull", CoinFull, sectionName, "0");                      //	PROFILE_WORD,	&DefFbxCnf.CoinFull,
                iniFile.Write("BillTimeOut", BillTimeOut, sectionName, "0");                //	PROFILE_CHAR,	&DefFbxCnf.BillTimeout,
                iniFile.Write("ProbeWatchDog", ProbeWatchDog, sectionName, "0");            //	PROFILE_LONG,	&ProbeWatchdog,
                iniFile.Write("EvTime", EvTime, sectionName, "0");                          //	PROFILE_CHAR,	&DefFbxCnf.EventPeriod,
                iniFile.Write("StopPointID", StopPointID, sectionName, "-1");               //	PROFILE_LONG,	&StopPointID,


                iniFile.Write("UseLockCodeFile", UseLockCodeFile, sectionName, "0");        //	PROFILE_LONG,	&OperatorID,
                iniFile.Write("OperatorID", OperatorID, sectionName, "0");                  //	PROFILE_LONG,	&OperatorID,
                iniFile.Write("LockFile", LockFile, sectionName, "-1");                     //	PROFILE_LONG,	&LockFile,
                
                iniFile.Write("MCP", MCP, sectionName, "");                                 //  Memry Clear Password
                iniFile.Write("DST", DST, sectionName, "0");                                 //	PROFILE_WORD,	&DST,
                iniFile.Write("STD", STD, sectionName, "0");                                 //	PROFILE_WORD,	&STD,
                iniFile.Write("GMTOffSet", GMTOffSet, sectionName, "0");                    //	PROFILE_CHAR,	&GMTOffset,
                iniFile.Write("ChangeCardThreshold", ChangeCardThreshold, sectionName, "0");//	PROFILE_WORD,	&ChangeCardThreshold,
                iniFile.Write("MagSecurityCodes", MagSecurityCodes, sectionName, "0x00000000");      //	PROFILE_CHAR,	&MagSecurityCodes,
                iniFile.Write("SCardSecurityCodes", SCardSecurityCodes, sectionName, "0x00000000");  //	PROFILE_CHAR,	&SCardSecurityCodes, 
                iniFile.Write("BillsTaken", BillsTaken, sectionName, "0");                  //	PROFILE_CHAR,	&DefFbxCnf.BillsTaken,
                iniFile.Write("RevStatus", revstatus, sectionName, "0x20");                      //	PROFILE_CHAR,	&DefFbxCnf.revstatus,
                iniFile.Write("J1708Poll", J1708Poll, sectionName, "0x00");                    //	PROFILE_CHAR,	&DefFbxCnf.J1708Poll,
                iniFile.Write("Logon", logon, sectionName, "0x00");                              //	PROFILE_CHAR,	&DefFbxCnf.logon,
                iniFile.Write("HourlyEvent", hourlyevent, sectionName, "0x00");                  //	PROFILE_CHAR,	&DefFbxCnf.hourlyevent,
                iniFile.Write("Daylight", daylight, sectionName, "0x01");                        //	PROFILE_CHAR,	&DefFbxCnf.daylight,
                iniFile.Write("TransferPrintOption", TransferPrintOption, sectionName, "0x00");          //	PROFILE_CHAR,	&DefFbxCnf.TransferPrintOption,
                iniFile.Write("MF", mf, sectionName, "0xFFFF");                                   //	PROFILE_WORD,	&DefFbxCnf.mf,
                iniFile.Write("LA_MetroOption", LA_MetroOption, sectionName, "0x00");          //	PROFILE_CHAR,	&DefFbxCnf.LA_MetroOption,
                iniFile.Write("OOS_Coin_Bill", OOS_Coin_Bill, sectionName, "0x00");            //	PROFILE_CHAR,	&DefFbxCnf.OOS_Coin_Bill,
                iniFile.Write("FSMask", FSMask, sectionName, "0x083FF");                        //	PROFILE_LONG,	&FSMaskV4, 
                iniFile.Write("FSMask2", FSMask2, sectionName, "0x03FFFFFF");                        //	PROFILE_LONG,	&FSMaskV4, 
                iniFile.Write("V4DLSTime", V4DLSTime, sectionName, "10");                     //	PROFILE_LONG,	&V4DLSTime,
                iniFile.Write("V4STDTime", V4STDTime, sectionName,"0");                     //	PROFILE_LONG,	&V4STDTime,
                iniFile.Write("ConvertBadCashboxes", ConvertBadCashboxes, sectionName,"0"); //	PROFILE_LONG,   &ConvertBadCashboxes,
                iniFile.Write("MixFleetMode", MixFleetMode, sectionName,"2");               //	PROFILE_LONG,	&_MixFleetMode,
                iniFile.Write("DupFilterTime", DupFilterTime,sectionName,"0");              //	PROFILE_LONG,	&DupFilterTime,
                iniFile.Write("AdjustDirType", AdjustDirType,sectionName,"0");              //	PROFILE_LONG,	&AdjustDirType,
                iniFile.Write("Sync", Sync,sectionName,"0");                                //	PROFILE_LONG,	&Sync,
                iniFile.Write("ProbeTRiM", ProbeTRiM,sectionName,"0");                      //	PROFILE_LONG,	&ProbeTRiM,
                iniFile.Write("ver", ver,sectionName,"102");                              //	PROFILE_WORD,	&DefFbxCnf.ver, 
                iniFile.Write("J1708MID", J1708MID,sectionName,"0");                      //	PROFILE_CHAR,	&DefFbxCnf.J1708MID,
                iniFile.Write("ProbeType", ProbeType,sectionName,"0");                    //	PROFILE_LONG,	&ProbeType,
                iniFile.Write("PDUProbeNumber", PDUProbeNumber,sectionName,"0");          //	PROFILE_LONG,	&PDUProbeNumber,
                iniFile.Write("ProbeID", ProbeID,sectionName,"0");                        //	PROFILE_LONG,	&ProbeID,
                iniFile.Write("UserID", UserID,sectionName,"0");                          //	PROFILE_LONG,	&UserID,
                iniFile.Write("VaultProbe", VaultProbe,sectionName,"1");                  //	PROFILE_LONG,	&VaultProbe,
                iniFile.Write("DS_RF_NAME", DS_RF_NAME, sectionName, "");                       //	PROFILE_STRING,	DS_RF_NAME,
                iniFile.Write("CalcSummaryCount", CalcSummaryCount, sectionName, "0");                    //	PROFILE_CHAR,	&GMTOffset,

                iniFile.Write("PingBus", PingBus, sectionName,"0");                        //	PROFILE_LONG,	&PingBus,


                iniFile.Write("UseDHCP", UseDHCP, sectionName,"0");                            //	PROFILE_LONG,	&UseDHCP,
                iniFile.Write("HostBits", HostBits, sectionName,"16");                     //	PROFILE_LONG,	&HostBits,
                iniFile.Write("Topology", Topology, sectionName,"0");                      //	PROFILE_LONG,	&Topology,
                iniFile.Write("Security", Security, sectionName,"2");                      //	PROFILE_LONG,	&Security,
                iniFile.Write("NetworkMode", NetworkMode, sectionName,"0");                //	PROFILE_LONG,	&NetworkMode,
                iniFile.Write("FBX_IP", FBX_IP, sectionName, "0.0.0.0");                        //	PROFILE_STRING, Fbx_Ip,					
                iniFile.Write("Gateway", Gateway, sectionName, "0.0.0.0");                      //  PROFILE_STRING, Gateway,				
                iniFile.Write("DNS_IP", DNS_IP, sectionName, "0.0.0.0");                        //	PROFILE_STRING, DNS_Ip,					
                iniFile.Write("Encryption", Encryption, sectionName, "0");                  //	PROFILE_LONG,	&Encryption,
                iniFile.Write("Authentication", Authentication, sectionName, "0");          //	PROFILE_LONG,	&Authentication,

                if (SSID.Trim().Length > 0)
                {
                    SSID = iniFile.EncryptString(SSID, ref str);
                    iniFile.Write("SSID", SSID, sectionName, "");
                    SSID = iniFile.DecryptString(SSID);
                }
                else
                {
                    iniFile.Write("SSID", "", sectionName, "");
                }


                if (KeyPhrase.Trim().Length > 0)
                {
                    KeyPhrase = iniFile.EncryptString(KeyPhrase, ref str);
                    iniFile.Write("KeyPhrase", KeyPhrase,sectionName,"");                      //	PROFILE_WORD,	&FbxPort,
                    KeyPhrase = iniFile.DecryptString(KeyPhrase);
                }
                else
                    iniFile.Write("KeyPhrase", "", sectionName, "");                      //	PROFILE_WORD,	&FbxPort,
                

                iniFile.Write("RfCnfMask", RfCnfMask, sectionName,"0");                    //	PROFILE_LONG,	&RfCnfMask,
                iniFile.Write("RFPowerCycleTime", RFPowerCycleTime, sectionName,"0");      //	PROFILE_LONG,	&RFPowerCycleTime,
                iniFile.Write("RFVersion", RFVersion,sectionName,"0");                  //  PROFILE_STRING, RFVersion,
                iniFile.Write("FtoTime", FtoTime,sectionName,"2000");                     //	PROFILE_LONG,	&FtoTime,
                iniFile.Write("InterProbeTime", InterProbeTime, sectionName,"0");          //	PROFILE_LONG,	&InterProbeTime,
                iniFile.Write("PollTime", PollTime,sectionName,"0");                      //	PROFILE_LONG,	&PollTime,
                iniFile.Write("MaxDiffRxByteCnt", MaxDiffRxByteCnt, sectionName,"2");      //	PROFILE_LONG,	&MaxDiffRxByteCnt,
                iniFile.Write("MaxPrbFrmErr", MaxPrbFrmErr, sectionName,"3");              //	PROFILE_LONG,	&MaxPrbFrmErr,
                iniFile.Write("MaxPrbBrkErr", MaxPrbBrkErr, sectionName,"3");              //	PROFILE_LONG,	&MaxPrbBrkErr,
                iniFile.Write("THeader1", THeader1, sectionName,"\\BGFI");                     //  PROFILE_STRING, DefTrimCnf.line[0].s,
                iniFile.Write("THeader2", THeader2,sectionName,"\\BGENFARE");                 //  PROFILE_STRING, DefTrimCnf.line[1].s,
                iniFile.Write("THeader3", THeader3,sectionName,"\\SA UNIT OF SPX CORPORATION"); // PROFILE_STRING, DefTrimCnf.line[2].s,
                iniFile.Write("TLayout", TLayout,sectionName,"_ I B R D E");	                //	PROFILE_STRING, DefTrimCnf.line[3].s,
                iniFile.Write("LCDHeader", LCDHeader,sectionName,"ODYSSEY");                  //	PROFILE_STRING, DefTrimCnf.line[4].s,
                iniFile.Write("VErr_Prnt", VErr_Prnt,sectionName,"0304\\P\\N\\V");            //	PROFILE_STRING,	DefTrimCnf.VErr,
                iniFile.Write("LA_NoMultifareDetail", LA_NoMultifareDetail,sectionName,"0");  //	PROFILE_LONG,	&LA_NoMultifareDetail,
                //build-157
                for (int i = 1; i <= 32; i++)
                    iniFile.Write("OptionFlags" + i.ToString(), optionFlags[i - 1], sectionName,"0x0000");
            }

            // Write Mux2 and Mux3 sections
            for (int i = 1; i < 3; i++)
            {
                if (muxEnabled[i])
                {
                    sectionName = "MUX" + (i + 1).ToString();
                    iniFile.Write("Port", mux[i].Port, sectionName,"None");                 //  CommName,	PROFILE_STRING, CommName,
                    iniFile.Write("PortEnbMask", mux[i].PortEnbMask,sectionName,"0x0F");    //	PROFILE_WORD,	&PortEnbMask,
                    iniFile.Write("MuxIsoBox", mux[i].MuxIsoBox,sectionName,"1");           //	PROFILE_LONG,	&MuxIsoBox,
                    iniFile.Write("ProbeType", mux[i].ProbeType,sectionName,"0");           //	PROFILE_LONG,	&ProbeType,
                    iniFile.Write("OldBaudrate", mux[i].OldBaudrate, sectionName, "0");     //	PROFILE_LONG,	&OldBaudRate,
                    iniFile.Write("Baudrate", mux[i].Baudrate, sectionName, "57600");       //	PROFILE_LONG,	&BaudRate,
                    iniFile.Write("FbxBaudrate", mux[i].FbxBaudrate, sectionName, "57600"); //	PROFILE_LONG,	&FbxBaudRate,
                    iniFile.Write("ScanBaudRate", mux[i].ScanBaudRate,sectionName,"0");     //	PROFILE_LONG,	&ScanBaudRate,
                    iniFile.Write("WaitSync", mux[i].WaitSync,sectionName,"1");             //	PROFILE_LONG,	&WaitSync,
                }
            }

            // Write VLT1-Vlt5 sections
            for (int i = 0; i < 5; i++)
            {
                if (vltEnabled[i])
                {
                    sectionName = "VLT" + (i + 1).ToString();
                    //vltDirty[i] = false;
                    iniFile.Write("Port", vlt[i].Port, sectionName,"COM1");                 //  CommName,	PROFILE_STRING, CommName,
                    iniFile.Write("PortEnbMask", vlt[i].PortEnbMask, sectionName,"0x0F");   //	PROFILE_WORD,	&PortEnbMask,
                    iniFile.Write("LockOut", vlt[i].LockOut, sectionName,"0");
                    iniFile.Write("DebugLevel", vlt[i].DebugLevel, sectionName,"0");    //	PROFILE_LONG,	&DebugLevel,
                    iniFile.Write("VltFull", vlt[i].VltFull, sectionName,"10000");
                    iniFile.Write("VltFlags", vlt[i].VltFlags, sectionName,"0x00000000");        //VLTF_DEBUG_PROTOCOL  (ulong)0x00000002 - debug protocol comm. flag 
                }
            }

            //////////////////////////////////////////////////////////////////////////////
            // Write Database Section
            sectionName = "Database";
            if (enableDatabase)
            {
                iniFile.Write("ENG", ENG, sectionName, "");
                iniFile.Write("DBF", DBF, sectionName, "");
                iniFile.Write("SecDBF", SecDBF, sectionName, "");
                iniFile.Write("START", START, sectionName, "");
                iniFile.Write("BkUp", BkUp, "DATA", "");
            }
            //////////////////////////////////////////////////////////////////////////////
            // Write Clnt Section
            sectionName = "Clnt";
            iniFile.Write("DebugLevel", Clnt_DebugLevel,sectionName,"0");
            iniFile.Write("WaitTime", Clnt_WaitTime,sectionName,"100000");
            iniFile.Write("MaxConnRetry", Clnt_MaxConnRetry,sectionName,"5");
            
            //////////////////////////////////////////////////////////////////////////////
            // Write RAS Section
            sectionName = "RAS";
            iniFile.Write("usr", RAS_usr, sectionName,"");
            iniFile.Write("Clntdomain", RAS_domain,sectionName,"");
            iniFile.Write("pwd", RAS_pwd,sectionName,"");

            //////////////////////////////////////////////////////////////////////////////
            // Write Mobil Ticket Section
            if (enableMobileTicketing)
            {
                sectionName = "MobTckt";
                iniFile.Write("MbtkFlags", Mbtk_MbtkFlags, sectionName, "0");
                iniFile.Write("DebugLevel", Mbtk_DebugLevel, sectionName, "1");
                iniFile.Write("DomainName", Mbtk_DomainName, sectionName, "api.genfaremobiledemo.com");
                iniFile.Write("PollInterval", Mbtk_PollInterval, sectionName, "10");
                iniFile.Write("TransitID", Mbtk_TransitID, sectionName, "Genfare");
                iniFile.Write("Authorization", Mbtk_Authorization, sectionName, "");
                iniFile.Write("TLS", Mbtk_TLS, sectionName, "");
            }

            //////////////////////////////////////////////////////////////////////////////
            // Read Mobil Ticket Section      
            sectionName = "UDPListener";
            if (enableUDPListener)
            {
                iniFile.Write("UDPFolder", UDP_Folder, sectionName, ""); ;
                iniFile.Write("DebugLevel", UDP_DebugLevel, sectionName, "0");
                iniFile.Write("UDPTestOnly", UDP_TestOnly, sectionName, "0");
                iniFile.Write("SaveUDPXML", UDP_SaveUDPRSP, sectionName, "0");
                iniFile.Write("SaveUDPRSP", UDP_SaveUDPRSP, sectionName, "0");
                iniFile.Write("URL", UDP_Url, sectionName, "");
                iniFile.Write("Token", UDP_Token, sectionName, "");
            }

            //////////////////////////////////////////////////////////////////////////////
            // Write User Defined Section
            sectionName = "User Defined Actions";
            iniFile.Write("Format", UDA_Format, sectionName,"");
            for (int i = 1; i <= 10; i++)
            {
                iniFile.Write(i.ToString(), UDA_Lines[i - 1], sectionName, UDA_Default[i-1]);
            }

            if (enableWifiProbing)
            {
                //////////////////////////////////////////////////////////////////////////////
                // Write User ProbServer Section
                sectionName = "ProbServer";
                iniFile.Write("DebugLevel", Prb_DebugLevel, sectionName, "0");
                iniFile.Write("Command", Prb_Command, sectionName, "probagent.exe");
                iniFile.Write("PrbEvntThreshold", Prb_PrbEvntThreshold, sectionName, "1");
                iniFile.Write("PrbTrnsThreshold", Prb_PrbTrnsThreshold, sectionName, "1");
                iniFile.Write("NatTunnel", Prb_NatTunnel, sectionName, "0");
                iniFile.Write("ProbeMode", Prb_ProbeMode, sectionName, "0");
                iniFile.Write("PRBFlags", PRBFlags, sectionName, "0x00000000");
                iniFile.Write("PRBFlags2", PRBFlags2, sectionName, "0x00000000");
            iniFile.Write("GCIP", Prb_GCIP, sectionName, "");
            iniFile.Write("GCTCPPort", Prb_GCTCPPort, sectionName, "");     

                /////////////////////////////////////////////////////////////////////////////////
                // Write rfdevmgr Section
                sectionName = "rfdevmgr";
                iniFile.Write("DebugLevel", RFDEVMGR_DebugLevel, sectionName, "0");
                iniFile.Write("InserviceProbe", RFDEVMGR_InserviceProbe, sectionName, "1");
                iniFile.Write("SaveWifiStatus", RFDEVMGR_SaveWifiStatus, sectionName, "0");
                iniFile.Write("PrbEvntThreshold", RFDEVMGR_PrbEvntThreshold, sectionName, "1");
                iniFile.Write("PrbTrnsThreshold", RFDEVMGR_PrbTrnsThreshold, sectionName, "1");
                iniFile.Write("Command", RFDEVMGR_Command, sectionName, "ds_webreq.exe");
                iniFile.Write("GCWIFIFLAGS", RFDEVMGR_GCWIFIFLAGS, sectionName, "0x00000013");
                iniFile.Write("AUTOLOADFILE", RFDEVMGR_AUTOLOADFILE, sectionName, "gaci.bin");
                iniFile.Write("FBXCBFWFILE", RFDEVMGR_FBXCBFWFILE, sectionName, "27248");
                iniFile.Write("FBXLIDFWFILE", RFDEVMGR_FBXLIDFWFILE, sectionName, "27240");
                iniFile.Write("LanguageFontFile", RFDEVMGR_LanguageFontFile, sectionName, "lfnt103.bin");
                iniFile.Write("FBXFWUPGBUSES", RFDEVMGR_FBXFWUPGBUSES, sectionName, "0");
                iniFile.Write("RetryCount", RFDEVMGR_RetryCount, sectionName, "3");
                ////////////////////////////////////////////////////////////////////////////////
                // Write rfsrvr Section
                sectionName = "rfsrvr";
                iniFile.Write("FbxPollTime", RFSRVR_FbxPollTime, sectionName, "300");
                iniFile.Write("PowerOffTime", RFSRVR_PowerOffTime, sectionName, "60");
                iniFile.Write("PrbTimeThreshold", RFSRVR_PrbTimeThreshold, sectionName, "300");
                iniFile.Write("PrbEvntThreshold", RFSRVR_PrbEvntThreshold, sectionName, "20");
                iniFile.Write("PrbTrnsThreshold", RFSRVR_PrbTrnsThreshold, sectionName, "20");
                iniFile.Write("KtFareThreshold ", RFSRVR_KtFareThreshold, sectionName, "200");
                iniFile.Write("AlarmFlagsMask", RFSRVR_AlarmFlagsMask, sectionName, "0xF5");
                iniFile.Write("DebugLevel", RFSRVR_DebugLevel, sectionName, "3");
                iniFile.Write("MaxWifiConnections", RFSRVR_MaxWifiConnections, sectionName, "240");
                //iniFile.Write("SaveWifiStatus", RFSRVR_SaveWifiStatus, sectionName,"0");
                iniFile.Write("BusProbeList", RFSRVR_BusProbeList, sectionName, "");
                iniFile.Write("RfPrbFlags", RFSRVR_RfPrbFlags, sectionName, "0x00000001");
            }

            if (!enableWifiProbing)
            {
                iniFile.DeleteSection("rfsrvr");        // remove Odysset wifi
                iniFile.DeleteSection("rfdevmgr");      // remove Odyssey+ wifi
                iniFile.DeleteSection("ProbServer");    // remove FF/FFE wifi
            }

            if (!enableMobileTicketing)
                iniFile.DeleteSection("MobTckt");
            if (!enableVndldr)
                iniFile.DeleteSection("Vndldr");
            if (!enableDatabase)
                iniFile.DeleteSection("Database"); 
            if (!enableUDPListener)
                iniFile.DeleteSection("UDPListener"); 
            if (!muxEnabled[0])
                iniFile.DeleteSection("MUX1");
            if (!muxEnabled[1])
                iniFile.DeleteSection("MUX2");
            if (!muxEnabled[2])
                iniFile.DeleteSection("MUX3");     
     
            if (!vltEnabled[0])
                iniFile.DeleteSection("Vlt1");
            if (!vltEnabled[1])
                iniFile.DeleteSection("Vlt2");
            if (!vltEnabled[2])
                iniFile.DeleteSection("Vlt3");   
            if (!vltEnabled[3])
                iniFile.DeleteSection("Vlt4");            
            if (!vltEnabled[4])
                iniFile.DeleteSection("Vlt5"); 
        }

        public gfiIniFileData()
        {
            // Create mux and vlt objects
            mux = new MuxCommon[3];         // Mux2-Mux2
            vlt = new VltCommon[5];        // Cashbox id computers in use VLT1-VLT5
            for (int i=0;i<3;i++)
                mux[i]=new MuxCommon();
            for (int i=0;i<5;i++)
                vlt[i]=new VltCommon();
            // Create comments for ini file
            iniComments.Add(new iniCommentsData("DS_RF_IP=", "D a ta  S y s t e m / F a r e b o x  C o m m u n i c a t i o n s   P o r t  S e t t i n g s"));
            iniComments.Add(new iniCommentsData("DirType=","P R O B I N G   C U S T O M I Z A T I O N"));
            iniComments.Add(new iniCommentsData("MUXISOBox=","C O M M U N I C A T I O N   S E T T I N G S"));
            iniComments.Add(new iniCommentsData("OptionFlags1=", "O P T I O N   F L A G S"));
            iniComments.Add(new iniCommentsData("FBXVersion",""));
            iniComments.Add(new iniCommentsData("MuxFlags=",""));
            iniComments.Add(new iniCommentsData("MUXTime=", ""));
            iniComments.Add(new iniCommentsData("AgencyCode", "F A R E B O X   C O N F I G U R A T I O N   P A R A M E T E R S"));
            iniComments.Add(new iniCommentsData("PingBus", "O d y s s e y / O d y s s e y +   W i F i   P A R A M E T E R S "));
            iniComments.Add(new iniCommentsData("RfCnfMask", ""));
            iniComments.Add(new iniCommentsData("THeader1", ""));
            iniComments.Add(new iniCommentsData("[MOBTCKT]", ""));
            iniComments.Add(new iniCommentsData("[VNDLDR]", ""));
            iniComments.Add(new iniCommentsData("[RAS]", "")); 
            iniComments.Add(new iniCommentsData("[DATA]", ""));
            iniComments.Add(new iniCommentsData("[CLNT]", ""));
            iniComments.Add(new iniCommentsData("[SRVR]", ""));
            iniComments.Add(new iniCommentsData("[RFDEVMGR]", ""));
            iniComments.Add(new iniCommentsData("[rfsrvr]", ""));
            iniComments.Add(new iniCommentsData("[ProbServer]", ""));
            iniComments.Add(new iniCommentsData("[MUX1]", ""));
            iniComments.Add(new iniCommentsData("[MUX2]", ""));
            iniComments.Add(new iniCommentsData("[MUX3]", ""));
            iniComments.Add(new iniCommentsData("[MUX4]", ""));

            iniComments.Add(new iniCommentsData("[Database]", ""));
            iniComments.Add(new iniCommentsData("[VLT1]", ""));
            iniComments.Add(new iniCommentsData("[VLT2]", ""));
            iniComments.Add(new iniCommentsData("[VLT3]", ""));
            iniComments.Add(new iniCommentsData("[VLT4]", ""));
            iniComments.Add(new iniCommentsData("[VLT5]", ""));
            iniComments.Add(new iniCommentsData("[USER DEFINED ACTIONS]", ""));
        }
    }

    public class NetConfiguration
    {
        public string FileName { get; set; }
        public string IniFilePath;
        public NetconfigData inifileData;
        public IniFile iniFile;
        public SplitForm splitform;

        public NetConfiguration()
        {
            inifileData = new NetconfigData();
        }
        public NetConfiguration(SplitForm sf, string path, string fname)
        {            
            splitform = sf;
            IniFilePath = path;
            FileName = fname;
            inifileData = new NetconfigData();    
            iniFile = new IniFile(splitform, IniFilePath, FileName);
        }
        public void ReadConfig()
        {
            inifileData.ReadNetconfigFile(iniFile);            
        }


        public void WriteConfig()
        {
            // Only backup if this is a Host system
            if (splitform.globalSettings.location==0)
                splitform.BackupFile(IniFilePath + FileName);
            inifileData.writeNetconfig(iniFile);
            splitform.globalSettings.bSaveCleanIniFile = false;
            // Trim trailing blanks in file
            iniFile.TrimFile(IniFilePath + FileName, null, false );
            splitform.globalSettings.NetConfigChanged = false;
        }
    }

    public class IniConfiguration
    {
        //public string Path { get; set; }
        public string FileName { get; set; }
        public string IniFilePath;
        public List<clntSection> clnt = new List<clntSection>();
        public gfiIniFileData inifileData;
    
        public IniFile iniFile;
        public SplitForm splitform;

        public IniConfiguration()
        {
            inifileData = new gfiIniFileData();    
        }

        public IniConfiguration(SplitForm sf, string path, string fname)
        {
            splitform = sf;
            IniFilePath = path;
            FileName = fname;
            inifileData = new gfiIniFileData();    
            iniFile = new IniFile(splitform, IniFilePath, FileName);
        }
        

        public class clntSection
        {
            public bool dirty = false;
            public string sectionName = "CLNT]";
        }

       
        public void ReadConfig()
        {
            inifileData.ReadIniFile(iniFile);
        }

        public void WriteConfig()
        {
            string filename = iniFile.IniFilePath + iniFile.IniFileName;
            // Only backup if this is a Host system
            if (splitform.globalSettings.location == 0)
                splitform.BackupFile(filename);
            if (splitform.globalSettings.bSaveCleanIniFile)
                File.Delete(filename);
            inifileData.WriteIniFile(iniFile);
            // Trim trailing blanks in file
            iniFile.TrimFile(iniFile.IniFilePath + iniFile.IniFileName, inifileData, splitform.globalSettings.bSaveCleanIniFile);
            splitform.globalSettings.dirty = false;
            splitform.globalSettings.bSaveCleanIniFile = false;
        }
    }
}
