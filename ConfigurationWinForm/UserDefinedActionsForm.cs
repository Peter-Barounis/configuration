﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class UserDefinedActionsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        bool formLoaded = false;
        private TextBox[] cbxArray = new TextBox[10];
        public UserDefinedActionsForm()
        {
            InitializeComponent();
        }

        public UserDefinedActionsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = textBox1;
            cbxArray[1] = textBox2;
            cbxArray[2] = textBox3;
            cbxArray[3] = textBox4;
            cbxArray[4] = textBox5;
            cbxArray[5] = textBox6;
            cbxArray[6] = textBox7;
            cbxArray[7] = textBox8;
            cbxArray[8] = textBox9;
            cbxArray[9] = textBox10;
            for (int i = 0; i < 10; i++)
                cbxArray[i].Text = splitForm.iniConfig.inifileData.UDA_Lines[i];
            formLoaded = true;
        }

        private void udaLines_TextChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                for (int i = 0; i < 10; i++)
                    splitForm.iniConfig.inifileData.UDA_Lines[i] = cbxArray[i].Text;
                splitForm.SetChanged();
            }
        }
    }
}
