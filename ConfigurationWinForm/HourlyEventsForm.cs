﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class HourlyEventsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public uint HourlyEventFlag;
        private CheckBox[] cbxArray = new CheckBox[8];
        public bool formLoaded=false;
        public ToolTip toolTip1 = new ToolTip();
        public HourlyEventsForm()
        {
            InitializeComponent();
        }

        public HourlyEventsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;

            LoadData();
            SetEventHexValue();
            switch (HourlyEventFlag & 0x03)
            {
                case 2:
                    halfHourEvent.Checked = true;
                    break;
                case 3:
                    quarterHourEvent.Checked = true;
                    break;
                default:
                    hourlyEvent.Checked = true;
                    break;
            }

            for (int i = 0; i < 6; i++)
            {
                if ((HourlyEventFlag & (uint)0x04 << i) > 0)
                    cbxArray[i].Checked = true;
                else
                    cbxArray[i].Checked = false;
            }
            formLoaded = true;
        }

        public void LoadData()
        {
            HourlyEventFlag = Util.ParseValue(splitForm.iniConfig.inifileData.hourlyevent);
            SetEventHexValue();
        }

        private void SetEventHexValue()
        {
            hourlyEventText.Text = "Hourly Event - [0x" + HourlyEventFlag.ToString("X2") + "]";
            splitForm.iniConfig.inifileData.hourlyevent = "0x" + HourlyEventFlag.ToString("X2");
        }
        private void radioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (hourlyEvent.Checked)
                    HourlyEventFlag = (HourlyEventFlag & 0xFC) | 0x01;
                if (halfHourEvent.Checked)
                    HourlyEventFlag = (HourlyEventFlag & 0xFC) | 0x02;
                if (quarterHourEvent.Checked)
                    HourlyEventFlag = (HourlyEventFlag & 0xFC) | 0x03;
                SetEventHexValue();
                splitForm.SetChanged();
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                int index = ((CheckBox)sender).TabIndex;
                if (cbxArray[index].Checked)
                    HourlyEventFlag |= ((uint)0x04 << index);
                else
                    HourlyEventFlag &= ~((uint)0x04 << index);
                SetEventHexValue();
                splitForm.iniConfig.inifileData.logon = "0x" + HourlyEventFlag.ToString("X2");
                SetEventHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
