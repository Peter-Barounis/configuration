﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class VndldrForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formloaded = false;
        public ToolTip toolTip1 = new ToolTip();
        public uint VndFlags = 0;
        private CheckBox[] cbxArray = new CheckBox[12];

        public VndldrForm()
        {
            InitializeComponent();
        }

        public VndldrForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;
            cbxArray[6] = this.checkBox7;
            cbxArray[7] = this.checkBox8;
            cbxArray[8] = this.checkBox9;
            cbxArray[9] = this.checkBox10;
            cbxArray[10] = this.checkBox11; 
            cbxArray[11] = this.checkBox12;
            if (splitForm.iniConfig.inifileData.VND_DebugLevel.Trim().Length == 0)
                splitForm.iniConfig.inifileData.VND_DebugLevel = "0";
            DebugLevel.Value = Convert.ToInt16(splitForm.iniConfig.inifileData.VND_DebugLevel);
            InConnStr.Text = splitForm.iniConfig.inifileData.VND_InConnStr;
            UID.Text = splitForm.iniConfig.inifileData.VND_UID;
            PWD.Text = splitForm.iniConfig.inifileData.VND_PWD;
            SampleCount.Text = splitForm.iniConfig.inifileData.VND_SampleCount;
            SampleTime.Text = splitForm.iniConfig.inifileData.VND_SampleTime;
            VndFlags = Util.ParseValue(splitForm.iniConfig.inifileData.VND_VndFlags);
            for (int i = 0; i < 12; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((VndFlags & 1 << i) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            vndFlagsText.Text = "VndFlags-[0x" + VndFlags.ToString("X4") + "]";

            formloaded = true;
        }

        public void SaveData()
        {
            if (formloaded)
            {
                splitForm.iniConfig.inifileData.VND_DebugLevel = DebugLevel.Value.ToString();
                splitForm.iniConfig.inifileData.VND_InConnStr = InConnStr.Text;
                splitForm.iniConfig.inifileData.VND_UID = UID.Text;
                splitForm.iniConfig.inifileData.VND_PWD = PWD.Text;
                splitForm.iniConfig.inifileData.VND_SampleCount = SampleCount.Text;
                splitForm.iniConfig.inifileData.VND_SampleTime = SampleTime.Text;
                VndFlags = 0;
                for (int i = 0; i < 12; i++)
                {
                    if (cbxArray[i].Checked)
                        VndFlags |= ((uint)1 << i);
                }
                splitForm.iniConfig.inifileData.VND_VndFlags = "0x" + VndFlags.ToString("X4");
                vndFlagsText.Text = "VndFlags-[0x" + VndFlags.ToString("X4") + "]";
                splitForm.SetChanged();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SetChanged(object sender, EventArgs e)
        {
            SaveData();
        }

        public void ValidateNumericTextBox(TextBox textbox)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textbox.Text, "[^0-9]"))
            {
                System.Media.SystemSounds.Hand.Play();
                textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                textbox.SelectionStart = textbox.Text.Length;
            }
        }
        private void numeric_TextChanged(object sender, EventArgs e)
        {
            ValidateNumericTextBox((TextBox)sender);
            SaveData();
        }

    }
}
