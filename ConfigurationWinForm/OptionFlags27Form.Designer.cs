﻿namespace ConfigurationWinForm
{
    partial class OptionFlags27Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionFlags27Form));
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(17, 116);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(587, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – Accept Canadian $ 1 Should be 0 since ignored by Canadian MVU Bill Valid" +
    "ator";
            this.toolTip2.SetToolTip(this.checkBox9, "Accept Canadian $ 1 Should be 0 since ignored by Canadian MVU Bill Validator");
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(17, 142);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(587, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Accept Canadian $ 2 Should be 0 since ignored by Canadian MVU Bill Valid" +
    "ator";
            this.toolTip2.SetToolTip(this.checkBox10, "Accept Canadian $ 2 Should be 0 since ignored by Canadian MVU Bill Validator");
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(17, 169);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(219, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – Accept Canadian $ 5";
            this.toolTip2.SetToolTip(this.checkBox11, "Accept Canadian $ 5");
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(17, 196);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(227, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – Accept Canadian $ 10";
            this.toolTip2.SetToolTip(this.checkBox12, "Accept Canadian $ 10 ");
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(17, 224);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(227, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Accept Canadian $ 20";
            this.toolTip2.SetToolTip(this.checkBox13, "Accept Canadian $ 20");
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Controls.Add(this.checkBox10);
            this.optionflagsGridTitle.Controls.Add(this.checkBox11);
            this.optionflagsGridTitle.Controls.Add(this.checkBox12);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(820, 375);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(687, 85);
            this.label1.TabIndex = 18;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(17, 38);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(75, 22);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 331);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(17, 304);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(268, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Reserved Should be set to 0";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(17, 276);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(648, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 - Accept Canadian $100 Should be 0 since $100’s are not currently supporte" +
    "d by backend";
            this.toolTip2.SetToolTip(this.checkBox15, "Accept Canadian $100 Should be 0 since $100’s are not currently supported by back" +
        "end");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(17, 251);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(635, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Accept Canadian $ 50 Should be 0 since $50’s are not currently supported" +
    " by backend";
            this.toolTip2.SetToolTip(this.checkBox14, "Accept Canadian $ 50 Should be 0 since $50’s are not currently supported by backe" +
        "nd");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags27Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 402);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags27Form";
            this.Text = "OptionFlag27";
            this.toolTip2.SetToolTip(this, "(Recommended: U.S: 0x0000, Canada (non-Winnipeg): 0x1C00)");
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}