﻿namespace ConfigurationWinForm
{
    partial class BillsRevenueJ1708Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.billsAcceptedText = new System.Windows.Forms.Label();
            this.Bill100 = new System.Windows.Forms.CheckBox();
            this.Bill50 = new System.Windows.Forms.CheckBox();
            this.Bill20 = new System.Windows.Forms.CheckBox();
            this.Bill10 = new System.Windows.Forms.CheckBox();
            this.Bill5 = new System.Windows.Forms.CheckBox();
            this.Bill2 = new System.Windows.Forms.CheckBox();
            this.Bill1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.revenueStatusText = new System.Windows.Forms.Label();
            this.RevDriverLoginViaCard = new System.Windows.Forms.CheckBox();
            this.RevFbxHostToTrim = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.revenueStatusIntervalcombo = new System.Windows.Forms.ComboBox();
            this.RevUse24HourClock = new System.Windows.Forms.CheckBox();
            this.RevEnableJ1708 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.J1708Mid = new System.Windows.Forms.TextBox();
            this.J1708PollInterval = new System.Windows.Forms.ComboBox();
            this.J1708EnableSendReq = new System.Windows.Forms.CheckBox();
            this.J1708PollLatLong = new System.Windows.Forms.CheckBox();
            this.J1708PollStop = new System.Windows.Forms.CheckBox();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.j1708PollingText = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.billsAcceptedText);
            this.groupBox1.Controls.Add(this.Bill100);
            this.groupBox1.Controls.Add(this.Bill50);
            this.groupBox1.Controls.Add(this.Bill20);
            this.groupBox1.Controls.Add(this.Bill10);
            this.groupBox1.Controls.Add(this.Bill5);
            this.groupBox1.Controls.Add(this.Bill2);
            this.groupBox1.Controls.Add(this.Bill1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(791, 90);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // billsAcceptedText
            // 
            this.billsAcceptedText.AutoSize = true;
            this.billsAcceptedText.Location = new System.Drawing.Point(17, -3);
            this.billsAcceptedText.Name = "billsAcceptedText";
            this.billsAcceptedText.Size = new System.Drawing.Size(126, 17);
            this.billsAcceptedText.TabIndex = 7;
            this.billsAcceptedText.Text = "BillsTaken - [0x1D]";
            // 
            // Bill100
            // 
            this.Bill100.AutoSize = true;
            this.Bill100.Location = new System.Drawing.Point(684, 43);
            this.Bill100.Name = "Bill100";
            this.Bill100.Size = new System.Drawing.Size(82, 21);
            this.Bill100.TabIndex = 6;
            this.Bill100.Text = "$100.00";
            this.Bill100.UseVisualStyleBackColor = true;
            this.Bill100.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill50
            // 
            this.Bill50.AutoSize = true;
            this.Bill50.Location = new System.Drawing.Point(568, 43);
            this.Bill50.Name = "Bill50";
            this.Bill50.Size = new System.Drawing.Size(74, 21);
            this.Bill50.TabIndex = 5;
            this.Bill50.Text = "$50.00";
            this.Bill50.UseVisualStyleBackColor = true;
            this.Bill50.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill20
            // 
            this.Bill20.AutoSize = true;
            this.Bill20.Location = new System.Drawing.Point(452, 43);
            this.Bill20.Name = "Bill20";
            this.Bill20.Size = new System.Drawing.Size(74, 21);
            this.Bill20.TabIndex = 4;
            this.Bill20.Text = "$20.00";
            this.Bill20.UseVisualStyleBackColor = true;
            this.Bill20.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill10
            // 
            this.Bill10.AutoSize = true;
            this.Bill10.Location = new System.Drawing.Point(336, 43);
            this.Bill10.Name = "Bill10";
            this.Bill10.Size = new System.Drawing.Size(74, 21);
            this.Bill10.TabIndex = 3;
            this.Bill10.Text = "$10.00";
            this.Bill10.UseVisualStyleBackColor = true;
            this.Bill10.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill5
            // 
            this.Bill5.AutoSize = true;
            this.Bill5.Location = new System.Drawing.Point(228, 43);
            this.Bill5.Name = "Bill5";
            this.Bill5.Size = new System.Drawing.Size(66, 21);
            this.Bill5.TabIndex = 2;
            this.Bill5.Text = "$5.00";
            this.Bill5.UseVisualStyleBackColor = true;
            this.Bill5.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill2
            // 
            this.Bill2.AutoSize = true;
            this.Bill2.Location = new System.Drawing.Point(120, 43);
            this.Bill2.Name = "Bill2";
            this.Bill2.Size = new System.Drawing.Size(66, 21);
            this.Bill2.TabIndex = 1;
            this.Bill2.Text = "$2.00";
            this.Bill2.UseVisualStyleBackColor = true;
            this.Bill2.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // Bill1
            // 
            this.Bill1.AutoSize = true;
            this.Bill1.Location = new System.Drawing.Point(12, 43);
            this.Bill1.Name = "Bill1";
            this.Bill1.Size = new System.Drawing.Size(66, 21);
            this.Bill1.TabIndex = 0;
            this.Bill1.Text = "$1.00";
            this.Bill1.UseVisualStyleBackColor = true;
            this.Bill1.CheckedChanged += new System.EventHandler(this.Bill_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.revenueStatusText);
            this.groupBox2.Controls.Add(this.RevDriverLoginViaCard);
            this.groupBox2.Controls.Add(this.RevFbxHostToTrim);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.revenueStatusIntervalcombo);
            this.groupBox2.Controls.Add(this.RevUse24HourClock);
            this.groupBox2.Controls.Add(this.RevEnableJ1708);
            this.groupBox2.Location = new System.Drawing.Point(12, 122);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(791, 122);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // revenueStatusText
            // 
            this.revenueStatusText.AutoSize = true;
            this.revenueStatusText.Location = new System.Drawing.Point(17, 0);
            this.revenueStatusText.Name = "revenueStatusText";
            this.revenueStatusText.Size = new System.Drawing.Size(116, 17);
            this.revenueStatusText.TabIndex = 5;
            this.revenueStatusText.Text = "RevStatus-[0x00]";
            // 
            // RevDriverLoginViaCard
            // 
            this.RevDriverLoginViaCard.AutoSize = true;
            this.RevDriverLoginViaCard.Location = new System.Drawing.Point(301, 30);
            this.RevDriverLoginViaCard.Name = "RevDriverLoginViaCard";
            this.RevDriverLoginViaCard.Size = new System.Drawing.Size(225, 21);
            this.RevDriverLoginViaCard.TabIndex = 3;
            this.RevDriverLoginViaCard.Text = "0x80 -Driver login via card only";
            this.RevDriverLoginViaCard.UseVisualStyleBackColor = true;
            this.RevDriverLoginViaCard.CheckedChanged += new System.EventHandler(this.RevStatus_CheckedChanged);
            // 
            // RevFbxHostToTrim
            // 
            this.RevFbxHostToTrim.AutoSize = true;
            this.RevFbxHostToTrim.Location = new System.Drawing.Point(16, 57);
            this.RevFbxHostToTrim.Name = "RevFbxHostToTrim";
            this.RevFbxHostToTrim.Size = new System.Drawing.Size(208, 21);
            this.RevFbxHostToTrim.TabIndex = 1;
            this.RevFbxHostToTrim.Text = "0x20 - Farebox host to TRiM";
            this.RevFbxHostToTrim.UseVisualStyleBackColor = true;
            this.RevFbxHostToTrim.CheckedChanged += new System.EventHandler(this.RevStatus_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(298, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "0x0f - Revenue Status Interval:";
            // 
            // revenueStatusIntervalcombo
            // 
            this.revenueStatusIntervalcombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.revenueStatusIntervalcombo.FormattingEnabled = true;
            this.revenueStatusIntervalcombo.Items.AddRange(new object[] {
            "Disable",
            "5 minutes",
            "10 minutes",
            "15 minutes",
            "20 minutes",
            "25 minutes",
            "30 minutes",
            "35 minutes",
            "40 minutes",
            "45 minutes",
            "50 minutes",
            "55 minutes",
            "60 minutes",
            "65 minutes",
            "70 minutes",
            "75 minutes"});
            this.revenueStatusIntervalcombo.Location = new System.Drawing.Point(506, 84);
            this.revenueStatusIntervalcombo.Name = "revenueStatusIntervalcombo";
            this.revenueStatusIntervalcombo.Size = new System.Drawing.Size(121, 24);
            this.revenueStatusIntervalcombo.TabIndex = 4;
            this.revenueStatusIntervalcombo.SelectedIndexChanged += new System.EventHandler(this.revenueStatusIntervalcombo_SelectedIndexChanged);
            // 
            // RevUse24HourClock
            // 
            this.RevUse24HourClock.AutoSize = true;
            this.RevUse24HourClock.Location = new System.Drawing.Point(16, 83);
            this.RevUse24HourClock.Name = "RevUse24HourClock";
            this.RevUse24HourClock.Size = new System.Drawing.Size(274, 21);
            this.RevUse24HourClock.TabIndex = 2;
            this.RevUse24HourClock.Text = "0x40 - Use 24-hour time display format";
            this.RevUse24HourClock.UseVisualStyleBackColor = true;
            this.RevUse24HourClock.CheckedChanged += new System.EventHandler(this.RevStatus_CheckedChanged);
            // 
            // RevEnableJ1708
            // 
            this.RevEnableJ1708.AutoSize = true;
            this.RevEnableJ1708.Location = new System.Drawing.Point(18, 30);
            this.RevEnableJ1708.Name = "RevEnableJ1708";
            this.RevEnableJ1708.Size = new System.Drawing.Size(279, 21);
            this.RevEnableJ1708.TabIndex = 0;
            this.RevEnableJ1708.Text = "0x10 -Enable J1708 PID 501 (Montreal)";
            this.RevEnableJ1708.UseVisualStyleBackColor = true;
            this.RevEnableJ1708.CheckedChanged += new System.EventHandler(this.RevStatus_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.J1708Mid);
            this.groupBox3.Controls.Add(this.J1708PollInterval);
            this.groupBox3.Controls.Add(this.J1708EnableSendReq);
            this.groupBox3.Controls.Add(this.J1708PollLatLong);
            this.groupBox3.Controls.Add(this.J1708PollStop);
            this.groupBox3.Location = new System.Drawing.Point(12, 274);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(791, 150);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(422, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "(Used by farebox to communicate with the device via J1708/1587)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(143, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(540, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "0x1F - Polling interval: Example: 1 = 10 sec., 2 = 20 sec. … F = 150 sec, 1F = 31" +
    "0 sec";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Manufacturer ID:";
            // 
            // J1708Mid
            // 
            this.J1708Mid.Location = new System.Drawing.Point(416, 22);
            this.J1708Mid.Name = "J1708Mid";
            this.J1708Mid.Size = new System.Drawing.Size(100, 22);
            this.J1708Mid.TabIndex = 4;
            this.J1708Mid.TextChanged += new System.EventHandler(this.J1708ManufacId_TextChanged);
            // 
            // J1708PollInterval
            // 
            this.J1708PollInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.J1708PollInterval.FormattingEnabled = true;
            this.J1708PollInterval.Items.AddRange(new object[] {
            "Off",
            "10 Seconds",
            "20 Seconds",
            "30 Seconds",
            "40 Seconds",
            "50 Seconds",
            "60 Seconds",
            "70 Seconds",
            "80 Seconds",
            "90 Seconds",
            "100 Seconds",
            "110 Seconds",
            "120 Seconds",
            "130 Seconds",
            "140 Seconds",
            "150 Seconds",
            "160 Seconds",
            "170 Seconds",
            "180 Seconds",
            "190 Seconds",
            "200 Seconds",
            "210 Seconds",
            "220 Seconds",
            "230 Seconds",
            "240 Seconds",
            "250 Seconds",
            "260 Seconds",
            "270 Seconds",
            "280 Seconds",
            "290 Seconds",
            "300 Seconds",
            "310 Seconds"});
            this.J1708PollInterval.Location = new System.Drawing.Point(18, 105);
            this.J1708PollInterval.Name = "J1708PollInterval";
            this.J1708PollInterval.Size = new System.Drawing.Size(121, 24);
            this.J1708PollInterval.TabIndex = 3;
            this.J1708PollInterval.SelectedIndexChanged += new System.EventHandler(this.J1708PollInterval_SelectedIndexChanged);
            this.J1708PollInterval.SelectedValueChanged += new System.EventHandler(this.J1708PollInterval_SelectedIndexChanged);
            // 
            // J1708EnableSendReq
            // 
            this.J1708EnableSendReq.AutoSize = true;
            this.J1708EnableSendReq.Location = new System.Drawing.Point(20, 78);
            this.J1708EnableSendReq.Name = "J1708EnableSendReq";
            this.J1708EnableSendReq.Size = new System.Drawing.Size(204, 21);
            this.J1708EnableSendReq.TabIndex = 2;
            this.J1708EnableSendReq.Text = "0x80 - Enable send request";
            this.J1708EnableSendReq.UseVisualStyleBackColor = true;
            this.J1708EnableSendReq.CheckedChanged += new System.EventHandler(this.J1708EnableSendReq_CheckedChanged);
            // 
            // J1708PollLatLong
            // 
            this.J1708PollLatLong.AutoSize = true;
            this.J1708PollLatLong.Location = new System.Drawing.Point(20, 48);
            this.J1708PollLatLong.Name = "J1708PollLatLong";
            this.J1708PollLatLong.Size = new System.Drawing.Size(240, 21);
            this.J1708PollLatLong.TabIndex = 1;
            this.J1708PollLatLong.Text = "0x40 - Poll lattitude and longitude";
            this.J1708PollLatLong.UseVisualStyleBackColor = true;
            this.J1708PollLatLong.CheckedChanged += new System.EventHandler(this.J1708PollLatLong_CheckedChanged);
            // 
            // J1708PollStop
            // 
            this.J1708PollStop.AutoSize = true;
            this.J1708PollStop.Location = new System.Drawing.Point(20, 21);
            this.J1708PollStop.Name = "J1708PollStop";
            this.J1708PollStop.Size = new System.Drawing.Size(129, 21);
            this.J1708PollStop.TabIndex = 0;
            this.J1708PollStop.Text = "0x20 - Poll Stop";
            this.J1708PollStop.UseVisualStyleBackColor = true;
            this.J1708PollStop.CheckedChanged += new System.EventHandler(this.J1708PollStop_CheckedChanged);
            // 
            // j1708PollingText
            // 
            this.j1708PollingText.AutoSize = true;
            this.j1708PollingText.Location = new System.Drawing.Point(29, 274);
            this.j1708PollingText.Name = "j1708PollingText";
            this.j1708PollingText.Size = new System.Drawing.Size(217, 17);
            this.j1708PollingText.TabIndex = 8;
            this.j1708PollingText.Text = "J1708Poll and J1708MID - [0xC1]";
            // 
            // BillsRevenueJ1708Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 458);
            this.Controls.Add(this.j1708PollingText);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "BillsRevenueJ1708Form";
            this.Text = "Bills-Revenue-J1708";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox Bill100;
        private System.Windows.Forms.CheckBox Bill50;
        private System.Windows.Forms.CheckBox Bill20;
        private System.Windows.Forms.CheckBox Bill10;
        private System.Windows.Forms.CheckBox Bill5;
        private System.Windows.Forms.CheckBox Bill2;
        private System.Windows.Forms.CheckBox Bill1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox revenueStatusIntervalcombo;
        private System.Windows.Forms.CheckBox RevUse24HourClock;
        private System.Windows.Forms.CheckBox RevEnableJ1708;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.CheckBox RevDriverLoginViaCard;
        private System.Windows.Forms.CheckBox RevFbxHostToTrim;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox J1708Mid;
        private System.Windows.Forms.ComboBox J1708PollInterval;
        private System.Windows.Forms.CheckBox J1708EnableSendReq;
        private System.Windows.Forms.CheckBox J1708PollLatLong;
        private System.Windows.Forms.CheckBox J1708PollStop;
        private System.Windows.Forms.Label billsAcceptedText;
        private System.Windows.Forms.Label revenueStatusText;
        private System.Windows.Forms.Label j1708PollingText;
    }
}