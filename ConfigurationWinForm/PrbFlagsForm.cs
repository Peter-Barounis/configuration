﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class PrbFlagsForm : Form
    {
        public SplitForm splitForm;
        public uint PRBFlags = 0;
        bool formLoaded = false;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[32];
        public PrbFlagsForm()
        {
            InitializeComponent();
        }

        public PrbFlagsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;
            cbxArray[6] = this.checkBox7;
            cbxArray[7] = this.checkBox8;
            cbxArray[8] = this.checkBox9;
            cbxArray[9] = this.checkBox10;
            cbxArray[10] = this.checkBox11;
            cbxArray[11] = this.checkBox12;
            cbxArray[12] = this.checkBox13;
            cbxArray[13] = this.checkBox14;
            cbxArray[14] = this.checkBox15;
            cbxArray[15] = this.checkBox16;
            cbxArray[16] = this.checkBox17;
            cbxArray[17] = this.checkBox18;
            cbxArray[18] = this.checkBox19;
            cbxArray[19] = this.checkBox20;
            cbxArray[20] = this.checkBox21;
            cbxArray[21] = this.checkBox22;
            cbxArray[22] = this.checkBox23;
            cbxArray[23] = this.checkBox24;
            cbxArray[24] = this.checkBox25;
            cbxArray[25] = this.checkBox26;
            cbxArray[26] = this.checkBox27;
            cbxArray[27] = this.checkBox28;
            cbxArray[28] = this.checkBox29;
            cbxArray[29] = this.checkBox30;
            cbxArray[30] = this.checkBox31;
            cbxArray[31] = this.checkBox32;
            PRBFlags = Util.ParseValue(splitForm.iniConfig.inifileData.PRBFlags);
            for (int i = 0; i < 30; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((PRBFlags & 1 << i) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            cbxArray[30].Checked = false;
            cbxArray[31].Checked = false;
            setPRBFlagsHexValue();
            formLoaded = true;
        }

        private void setPRBFlagsHexValue()
        {
            PRBFlagsGridTitle.Text = "PRB Flags - [0x" + PRBFlags.ToString("X8") + "]";
        }

        private void updateFlag(int index)
        {
            if (cbxArray[index].Checked)
                PRBFlags |= ((uint)1 << index);
            else
                PRBFlags &= ~((uint)1 << index);
        }

        private void muxFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag(((CheckBox)sender).TabIndex);
                setPRBFlagsHexValue();
                splitForm.iniConfig.inifileData.PRBFlags = "0x" + PRBFlags.ToString("X8");
                splitForm.SetChanged();
            }
        }
    }
}
