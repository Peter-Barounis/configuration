﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags30Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;

        public OptionFlags30Form()
        {
            InitializeComponent();
        }

        private void setHexValue()
        {
            groupBox1.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[29] = "0x" + optionflag.ToString("X4");
            if (formLoaded)
                splitForm.SetChanged();
        }

        public OptionFlags30Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[29]);
            setCheckboxState();
            setHexValue();
            formLoaded = true;
        }

        public void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
            setHexValue();
        }

        private void checkedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
        }

        private void setCheckboxState()
        {
            checkBox1.Checked = Convert.ToBoolean((optionflag >> 0) & 0x0001);
            checkBox2.Checked = Convert.ToBoolean((optionflag >> 1) & 0x0001);
            checkBox3.Checked = Convert.ToBoolean((optionflag >> 2) & 0x0001);
            checkBox4.Checked = Convert.ToBoolean((optionflag >> 3) & 0x0001);

            checkBox5.Checked = Convert.ToBoolean((optionflag >> 4) & 0x0001);
            checkBox6.Checked = Convert.ToBoolean((optionflag >> 5) & 0x0001);
            checkBox7.Checked = Convert.ToBoolean((optionflag >> 6) & 0x0001);
            checkBox8.Checked = Convert.ToBoolean((optionflag >> 7) & 0x0001);

            checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            checkBox10.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            checkBox11.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            checkBox12.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);

            checkBox13.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            checkBox14.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            checkBox15.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            checkBox16.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);

        }
    }
}
