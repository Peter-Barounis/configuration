﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using GfiUtilities;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ConfigurationWinForm
{
    public partial class DBConnection : Form
    {
        [DllImport("gfipb32")]
        private unsafe static extern int Encrypt(byte[] dst, byte[] src, int max);

        [DllImport("gfipb32")]
        private unsafe static extern int Decrypt(byte[] dst, byte[] src, int max);

        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formloaded = false;
        public string dbType;
        public int currentDatabase = 0;
        public string globalKeyValue;
        public string connectKeyValue;

        public string[] DbConnectionTypes = {"ASA", "MSS", "ORA", "Unknown"};
        public string[] sysconEng = new string[26];
        public string[] sysconHost = new string[26];
        public long disableSyscon = 0;
        public int maxGarages = 0;

        public bool bEngine_locationChanges = false;
        public bool bHost_locationChanges = false;

        public DBConnection()
        {
            InitializeComponent();
        }
        public DBConnection(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            this.ConnectionType.SelectedIndex = 0;
            for (int i=0;i<26;i++)
            {
                sysconEng[i]="";
                sysconHost[i]="";
            }
            dirtyFlag = false;
            Save.Enabled = false;
            LoadData();
        }
        ///////////////////////////////////////////////////////////////////
        // This will get the SQL Server Driver Name
        ///////////////////////////////////////////////////////////////////
        public void LoadDrivers(int currentDatabase)
        {
            string keyValue = "";
            Driver.Items.Clear();
            switch (currentDatabase)
            {
                case 0: // Garage Computer (probably Sybase Database)
                    Driver.Items.Add("Adaptive Server Anywhere 9.0");
                    break;
                case 1: // Network Manager (Probably MSS)
                    RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ODBC\\ODBCINST.INI\\ODBC Drivers");
                    if (regKey != null)
                    {
                        foreach (var value in regKey.GetValueNames())
                        {
                            string data = Convert.ToString(regKey.GetValue(value)).ToLower();
                            if (data.CompareTo("installed") == 0)
                            {
                                keyValue = Convert.ToString(value);
                                if ( keyValue.ToLower().Contains("sql server native client") ||
                                     keyValue.ToLower().Contains("sql native client") )
                                    Driver.Items.Add(keyValue);
                            }
                        }
                    }
                    break;
                case 2:
                    Driver.Items.Add("(Currently unsupported)");
                    break;
            }
            Driver.SelectedIndex = 0;
        }

        private void LoadData()
        {
            string str;
            formloaded = false;
            currentDatabase = ConnectionType.SelectedIndex;
            globalKeyValue = splitForm.globalSettings.HKLM_GFI_GLOBAL;
            if (ConnectionType.SelectedIndex < 0 || ConnectionType.SelectedIndex > 2)
                dbType=DbConnectionTypes[0];
            else
                dbType = DbConnectionTypes[ConnectionType.SelectedIndex];
            connectKeyValue = splitForm.globalSettings.HKLM_GFI_CONNECT + "\\" + dbType;
            switch (ConnectionType.SelectedIndex)
            {
                case 1:     // SQL Server Database (MSS)
                    GDSGroup.Enabled = false;
                    NMGroup.Enabled = true;
                    GDSSysconEng.Text = "";
                    GDSSysconHost.Text="";
                    startCmdTxt.Text = "";
                    NMSysconDSN.Text = splitForm.GetGFIRegistryString(globalKeyValue, "DSN", "Localhost");
                    DBName.Text = splitForm.GetGFIRegistryString(connectKeyValue, "DBN", "gfi");
                    LoadGarages();
                    break;
                case 2:      //acle Database (ORA)
                    break;
                default:     // Sybase Database (ASA)
                    // Load GFI Database parameters
                    GDSGroup.Enabled = true;
                    NMGroup.Enabled = false;
                    //GDSSysconEng.Text = splitForm.GetGFIRegistryString(globalKeyValue, "SYSCON ENG", "garage1");
                    //GDSSysconHost.Text = splitForm.GetGFIRegistryString(globalKeyValue, "SYSCON HOST", "Localhost");
                    //string temp = splitForm.GetGFIRegistryString(globalKeyValue, "NM DBMS", "MSS");
                    GDSSysconEng.Text = splitForm.GetGFIRegistryString(connectKeyValue, "SYSCON ENG", "garage1");
                    GDSSysconHost.Text = splitForm.GetGFIRegistryString(connectKeyValue, "SYSCON HOST", "Localhost");
                    string temp = splitForm.GetGFIRegistryString(connectKeyValue, "NM DBMS", "MSS");

                    if (temp.ToUpper().CompareTo("MSS") == 0)
                        NMSysconDBMScombo.SelectedIndex = 0;
                    else
                        NMSysconDBMScombo.SelectedIndex = 1;
                    DBFText.Text = splitForm.GetGFIRegistryString(connectKeyValue, "DBF", "gfi.db");
                    //startCmdTxt.Text = splitForm.GetGFIRegistryString(globalKeyValue, "START", "dbsrv9.exe -ga -gd all -gl all -qi -qs -qw -z -x tcpip -os 5M");
                    startCmdTxt.Text = splitForm.GetGFIRegistryString(connectKeyValue, "START", "dbsrv9.exe -ga -gd all -gl all -qi -qs -qw -z -x tcpip -os 5M");
                    // Now load the Syscon parameters
                    SysconDBFText.Text = splitForm.GetGFIRegistryString(connectKeyValue, "SYSCON DBF", "garage1");
                    SysconUserID.Text = splitForm.GetGFIRegistryString(connectKeyValue, "SYSCON ID", "gfi");
                    SysconPassword.Text = splitForm.GetGFIRegistryString(connectKeyValue, "SYSCON PWD", "");
                    SysconPassword.Text = DecryptString(SysconPassword.Text);
                    break;
            }

            // Connection Parameters
            LoadDrivers(currentDatabase);
            UserID.Text = splitForm.GetGFIRegistryString(connectKeyValue, "UID", "gfi");
            Password.Text = splitForm.GetGFIRegistryString(connectKeyValue, "PWD", "");
            Password.Text = DecryptString(Password.Text);

            // Disable Syscon?
            str = splitForm.GetGFIRegistryString(globalKeyValue, "DisableSyscon", "No");
            if (str.Substring(0, 1).ToUpper() == "Y")
                DisableSysconDatabase.Checked = true;
            else
                DisableSysconDatabase.Checked = false;

            // Single Login
            str = splitForm.GetGFIRegistryString(globalKeyValue, "SingleLogin", "Yes");
            if (str.Substring(0, 1).ToUpper() == "Y")
                cbxSingleLogin.Checked = true;
            else
                cbxSingleLogin.Checked = false;

            UpdateMenuStatus(false);
            formloaded = true;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            if (dirtyFlag)
            {
                if (MessageBox.Show("Do want to Discard your changes?", "Exit Connection Information", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    UpdateMenuStatus(false);
            }
            if (!dirtyFlag)
                Close();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            string str;
            RegistryKey regKey=null;
            switch (ConnectionType.SelectedIndex)
            {
                case 0: // GDS
                    SaveGDS_SingleLogin();
                    break;
                case 1: // MSS
                    SaveNM_SingleLogin();
                    break;
            }
            try
            {
                regKey = Registry.LocalMachine.CreateSubKey(globalKeyValue, true);
                if (cbxSingleLogin.Checked)
                    str="Yes";
                else
                    str = "No";
                regKey.SetValue("SingleLogin", str);
                if (DisableSysconDatabase.Checked)
                    str = "Yes";
                else
                    str = "No";
                regKey.SetValue("DisableSyscon", str);
                regKey.Close();
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }
            UpdateMenuStatus(false);
        }

        //private void SaveNM()
        //{
        //}
        //private void SaveGDS()
        //{
        //}
        private void SaveNM_SingleLogin()
        {
            string pwd;
            string encryptedpwd="";
            RegistryKey regKey;
            try
            {
                 regKey = Registry.LocalMachine.CreateSubKey(connectKeyValue, true);
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }
            regKey.SetValue("DBN", DBName.Text);
            regKey.SetValue("UID", UserID.Text);
            pwd = this.Password.Text;
            pwd = EncryptString(pwd, ref encryptedpwd);
            regKey.SetValue("PWD", pwd);
            regKey.SetValue("Driver", this.Driver.Text);
            regKey.Close();
            SaveGarages();
        }

        private void SaveGDS_SingleLogin()
        {
            string pwd;
            string encryptedpwd = "";
            RegistryKey regKey;
            try
            {
                regKey = Registry.LocalMachine.CreateSubKey(connectKeyValue, true);
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }            
            // Save NM DBMS
            regKey.SetValue("NM DBMS", (NMSysconDBMScombo.SelectedIndex == 0?"MSS":"ASA"));
            regKey.SetValue("DBN", DBName.Text);

            // Connection Parameters
            regKey.SetValue("Driver", this.Driver.Text);
            regKey.SetValue("UID", UserID.Text);
            pwd = Password.Text;
            pwd = EncryptString(pwd, ref encryptedpwd);
            regKey.SetValue("PWD", pwd);

            regKey.SetValue("SYSCON UID", SysconUserID.Text.Trim());
            pwd = SysconPassword.Text;
            pwd = EncryptString(pwd, ref encryptedpwd);
            regKey.SetValue("SYSCON PWD", pwd);
            regKey.SetValue("SYSCON DBF", SysconDBFText.Text.Trim());

            // GDS System Parameters
            regKey.SetValue("SYSCON ENG", GDSSysconEng.Text.Trim());
            regKey.SetValue("SYSCON HOST", GDSSysconHost.Text.Trim());
            regKey.SetValue("DBF", DBFText.Text.Trim());
            string temp = this.startCmdTxt.Text.Trim() + " -o " + splitForm.globalSettings.gfiPath + "\\log\\" + GDSSysconEng.Text.Trim() + ".log" + " -oe " + splitForm.globalSettings.gfiPath + "\\log\\" + GDSSysconEng.Text.Trim() + ".log";
            regKey.SetValue("Start", temp);

            regKey.Close();
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            if (dirtyFlag)
            {
                if (MessageBox.Show("Do want to Discard your changes?", "Refresh Connection Information", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    UpdateMenuStatus(false);
            }
            if (!dirtyFlag)
                LoadData();
        }
        bool bIgnoreMessage = false;
        private void ConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (formloaded)
            {
                if (dirtyFlag && (!bIgnoreMessage))
                {
                    if (MessageBox.Show("Do want to Discard your changes?", "Select Connection", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        bIgnoreMessage = true;
                        ConnectionType.SelectedIndex = currentDatabase;
                        bIgnoreMessage = false;
                    }
                    else
                        UpdateMenuStatus(false);
                }
                if (!dirtyFlag)
                {
                    bIgnoreMessage = false;
                    formloaded = false;
                    LoadData();
                    UpdateMenuStatus(false);
                }
            }
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            if (formloaded)
                UpdateMenuStatus(true);
        }

        private void SaveGarages()
        {
            RegistryKey regKey;
            try
            {
                //regKey = Registry.LocalMachine.CreateSubKey(globalKeyValue, true);
                regKey = Registry.LocalMachine.CreateSubKey(connectKeyValue, true);
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }
            regKey.SetValue("DBN", DBName.Text);

            for (int garageId=1;garageId<27;garageId++)
            {
                if (sysconEng[garageId - 1].Trim().Length > 0)
                    regKey.SetValue("SYSCON ENG" + garageId.ToString(), sysconEng[garageId - 1]);
                else
                    regKey.DeleteValue("SYSCON ENG" + garageId.ToString(), false);
                if (sysconHost[garageId - 1].Trim().Length > 0)
                    regKey.SetValue("SYSCON HOST" + garageId.ToString(), sysconHost[garageId - 1]);
                else
                    regKey.DeleteValue("SYSCON HOST" + garageId.ToString(), false);
            }
            regKey.Close();
        }

        private void SaveCurrentGarage()
        {
            RegistryKey regKey;
            try
            {
                //regKey = Registry.LocalMachine.CreateSubKey(globalKeyValue, true);
                regKey = Registry.LocalMachine.CreateSubKey(connectKeyValue, true);
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }
            regKey.SetValue("SYSCON ENG", GDSSysconEng.Text.Trim());
            regKey.SetValue("SYSCON HOST", GDSSysconHost.Text.Trim());
            regKey.Close();
        }


        private void LoadGarages()
        {
            object value;
            RegistryKey regKey;
            try
            {
                //regKey = Registry.LocalMachine.CreateSubKey(globalKeyValue, true);
                regKey = Registry.LocalMachine.CreateSubKey(connectKeyValue, true);
                
            }
            catch
            {
                MessageBox.Show("Unable to read registry - Check user permissions.", "Loading Registry Failed");
                return;
            }
            maxGarages = 0;
            for (int garageId = 1; garageId < 27; garageId++)
            {
                value=regKey.GetValue("SYSCON ENG"+garageId.ToString());
                if (value != null)
                {
                    sysconEng[garageId - 1] = value.ToString();
                    maxGarages++;
                }
                else
                    sysconEng[garageId - 1] ="";
                value=regKey.GetValue("SYSCON HOST"+garageId.ToString()) ;
                if (value != null)
                    sysconHost[garageId - 1] = value.ToString();
                else
                    sysconEng[garageId - 1] = "";
            }
            regKey.Close();
            gdsLocation.Value = 1;
            NMGDSHost.Text = sysconHost[0];
            NMGDSEngine.Text = sysconEng[0];
        }

        private void updateLocationCount()
        {
            int nlocations = 0;
            bEngine_locationChanges = true;
            bHost_locationChanges = true;

            for (int garageId = 0; garageId < 26; garageId++)
            {
                if (sysconEng[garageId].Trim().Length>0)
                {
                    nlocations++;
                    if (nlocations > maxGarages)
                        UpdateMenuStatus(true);
                }
            }
            foundLocations.Text = nlocations.ToString() + " Garage Locations";
        }
        private void engine_Changed(object sender, EventArgs e)
        {
            int gdsid = Convert.ToInt16(gdsLocation.Value);
            sysconEng[gdsid-1] = NMGDSEngine.Text;
            if (!bEngine_locationChanges)
                UpdateMenuStatus(true);
            else
                bEngine_locationChanges = false;
        }

        private void Host_Changed(object sender, EventArgs e)
        {
            int gdsid = Convert.ToInt16(gdsLocation.Value);
            sysconHost[gdsid-1] = NMGDSHost.Text;
            if (!bHost_locationChanges)
                UpdateMenuStatus(true);
            else
                bHost_locationChanges = false;
        }

        private void gdsLocation_ValueChanged(object sender, EventArgs e)
        {
             int gdsid = Convert.ToInt16(gdsLocation.Value);
             bHost_locationChanges = true;
             bEngine_locationChanges = true;
             NMGDSHost.Text = sysconHost[gdsid-1];
             NMGDSEngine.Text = sysconEng[gdsid-1];

        }
        public string EncryptString(string src, ref string str)
        {
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            src = src.PadRight(33);
            buffer2 = Encoding.ASCII.GetBytes(src);
            Encrypt(buffer1, buffer2, 33);
            Array.Resize(ref buffer1, 33);
            var table = (Encoding.Default.GetString(
                             buffer1,
                             0,
                             src.Trim().Length)).Split(new string[] { "\r\n", "\r", "\n" },
                             StringSplitOptions.None);
            str = table[0];
            return "\\x" + BitConverter.ToString(buffer1).Replace("-", "\\x");
        }

        public string DecryptString(string src)
        {
            int j;
            byte[] buffer1 = new byte[128];
            byte[] buffer2 = new byte[128];
            buffer2 = Encoding.ASCII.GetBytes(src);
            src = src.PadRight(33);

            if (src.Trim().Length > 0)
            {
                Decrypt(buffer1, buffer2, 33);
                for (j = 0; j < 33; j++)
                {
                    if (buffer1[j] == 0x00)
                        break;
                }
                Array.Resize(ref buffer1, j);
                return System.Text.Encoding.Default.GetString(buffer1).Trim();
            }
            else
                return ("");
        }

        private void DisableSysconDatabase_CheckedChanged(object sender, EventArgs e)
        {
            UpdateMenuStatus(true);
        }

        private void Driver_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateMenuStatus(true);
        }

        private void NMSysconDBMScombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateMenuStatus(true);
        }
        void UpdateMenuStatus(bool flag)
        {
            if (formloaded)
            {
            dirtyFlag = flag;
            Save.Enabled = flag;
            }
        }

        private void DBFBrowse(TextBox text, string ext)
        {
            DBFopenFileDialog = new System.Windows.Forms.OpenFileDialog();
            DBFopenFileDialog.InitialDirectory = text.Text;
            DBFopenFileDialog.Filter = ext + "|All files (*.*)|*.*";
            DBFopenFileDialog.FilterIndex = 2;
            DBFopenFileDialog.RestoreDirectory = true;
            DBFopenFileDialog.Multiselect = false;
            DBFopenFileDialog.ValidateNames = false;
            DBFopenFileDialog.CheckFileExists = false;

            if (DBFopenFileDialog.ShowDialog() == DialogResult.OK)
                text.Text = DBFopenFileDialog.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBFBrowse(this.DBFText, "gfi db file (*.dat)|*.dat");
        }

        private void SysconDBFFolderSelect_Click(object sender, EventArgs e)
        {
            DBFBrowse(this.SysconDBFText, "gfi db file (*.dat)|*.dat");
        }

        private void cbxSingleLogin_CheckedChanged(object sender, EventArgs e)
        {
            UpdateMenuStatus(true);
        }
    }
}
