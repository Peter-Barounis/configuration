﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class CoinBillOptionsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public uint OOS_Coin_Bill= 0 ;
        public bool formLoaded = false;
        public CoinBillOptionsForm()
        {
            InitializeComponent();
        }
        public CoinBillOptionsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            loadForm(sf);
        }
        public void loadForm(SplitForm sf)
        {
            splitForm = sf;
            if (splitForm.iniConfig.inifileData.OOS_Coin_Bill.Length < 3)
                splitForm.iniConfig.inifileData.OOS_Coin_Bill = "0x00";
            this.OOS_Coin_Bill = uint.Parse(splitForm.iniConfig.inifileData.OOS_Coin_Bill.Substring(2), System.Globalization.NumberStyles.HexNumber);
            setCheckbox();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            CoinBillOptionsText.Text = "OOS_Coin_bill (Coin and Bill Options) - [0x" + OOS_Coin_Bill.ToString("X2") + "]";
            splitForm.iniConfig.inifileData.OOS_Coin_Bill = "0x" + OOS_Coin_Bill.ToString("X2");
        }

        public void setCheckbox()
        {
            if (Convert.ToBoolean(OOS_Coin_Bill & 0x01))
            {
                this.radioButton1.Checked = true;
            }
            else
            {
                this.radioButton2.Checked = true;
            }

            if (Convert.ToBoolean((OOS_Coin_Bill >> 1) & 0x01))
            {
                this.radioButton3.Checked = true;
            }
            else
            {
                this.radioButton4.Checked = true;
            }
            this.checkBox1.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 2) & 0x01);
            this.checkBox2.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 3) & 0x01);
            this.checkBox3.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 4) & 0x01);
            this.checkBox4.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 5) & 0x01);
            this.checkBox5.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 6) & 0x01);
            this.checkBox6.Checked = Convert.ToBoolean((OOS_Coin_Bill >> 7) & 0x01);            
        }
        public void saveForm(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                if (this.radioButton1.Checked)
                    OOS_Coin_Bill |= 0x01;
                else
                    OOS_Coin_Bill &= 0xFE;

                if (this.radioButton3.Checked)
                    OOS_Coin_Bill |= 0x02;
                else
                    OOS_Coin_Bill &= 0xFD;

                if (this.checkBox1.Checked)
                    OOS_Coin_Bill |= 0x04;
                else
                    OOS_Coin_Bill &= 0xFB;

                if (this.checkBox2.Checked)
                    OOS_Coin_Bill |= 0x08;
                else
                    OOS_Coin_Bill &= 0xF7;

                if (this.checkBox3.Checked)
                    OOS_Coin_Bill |= 0x10;
                else
                    OOS_Coin_Bill &= 0xEF;

                if (this.checkBox4.Checked)
                    OOS_Coin_Bill |= 0x20;
                else
                    OOS_Coin_Bill &= 0xDF;

                if (this.checkBox5.Checked)
                    OOS_Coin_Bill |= 0x40;
                else
                    OOS_Coin_Bill &= 0xBF;

                if (this.checkBox6.Checked)
                    OOS_Coin_Bill |= 0x80;
                else
                    OOS_Coin_Bill &= 0x7F;
                setHexValue();
                splitForm.SetChanged();
            }
        }
    }
}
