﻿namespace ConfigurationWinForm
{
    partial class GlobalPrinterSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.showPageNumbersCheckBox = new System.Windows.Forms.CheckBox();
            this.pageNumbersInHeaderTextBox = new System.Windows.Forms.CheckBox();
            this.proportionalSpacingCHeckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gridComboAlignment = new System.Windows.Forms.ComboBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.subTitleTextBox = new System.Windows.Forms.TextBox();
            this.footerTexBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.footerSpacingNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.CancelPrintButton = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.titleComboLCR = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.subTitleComboLCR = new System.Windows.Forms.ComboBox();
            this.footerComboLCR = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.footerSpacingNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Subtitle:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // showPageNumbersCheckBox
            // 
            this.showPageNumbersCheckBox.AutoSize = true;
            this.showPageNumbersCheckBox.Location = new System.Drawing.Point(204, 129);
            this.showPageNumbersCheckBox.Name = "showPageNumbersCheckBox";
            this.showPageNumbersCheckBox.Size = new System.Drawing.Size(126, 17);
            this.showPageNumbersCheckBox.TabIndex = 4;
            this.showPageNumbersCheckBox.Text = "Show Page Numbers";
            this.showPageNumbersCheckBox.UseVisualStyleBackColor = true;
            // 
            // pageNumbersInHeaderTextBox
            // 
            this.pageNumbersInHeaderTextBox.AutoSize = true;
            this.pageNumbersInHeaderTextBox.Location = new System.Drawing.Point(343, 129);
            this.pageNumbersInHeaderTextBox.Name = "pageNumbersInHeaderTextBox";
            this.pageNumbersInHeaderTextBox.Size = new System.Drawing.Size(145, 17);
            this.pageNumbersInHeaderTextBox.TabIndex = 5;
            this.pageNumbersInHeaderTextBox.Text = "Page Numbers in Header";
            this.pageNumbersInHeaderTextBox.UseVisualStyleBackColor = true;
            // 
            // proportionalSpacingCHeckBox
            // 
            this.proportionalSpacingCHeckBox.AutoSize = true;
            this.proportionalSpacingCHeckBox.Location = new System.Drawing.Point(15, 22);
            this.proportionalSpacingCHeckBox.Name = "proportionalSpacingCHeckBox";
            this.proportionalSpacingCHeckBox.Size = new System.Drawing.Size(131, 17);
            this.proportionalSpacingCHeckBox.TabIndex = 3;
            this.proportionalSpacingCHeckBox.Text = "Proportikonal Columns";
            this.proportionalSpacingCHeckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.proportionalSpacingCHeckBox);
            this.groupBox1.Location = new System.Drawing.Point(23, 164);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 55);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grid Parameters";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Alignment:";
            this.label7.Visible = false;
            // 
            // gridComboAlignment
            // 
            this.gridComboAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gridComboAlignment.FormattingEnabled = true;
            this.gridComboAlignment.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.gridComboAlignment.Location = new System.Drawing.Point(135, 225);
            this.gridComboAlignment.Name = "gridComboAlignment";
            this.gridComboAlignment.Size = new System.Drawing.Size(66, 21);
            this.gridComboAlignment.TabIndex = 20;
            this.gridComboAlignment.Visible = false;
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(76, 32);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(407, 20);
            this.titleTextBox.TabIndex = 0;
            // 
            // subTitleTextBox
            // 
            this.subTitleTextBox.Location = new System.Drawing.Point(76, 66);
            this.subTitleTextBox.Name = "subTitleTextBox";
            this.subTitleTextBox.Size = new System.Drawing.Size(407, 20);
            this.subTitleTextBox.TabIndex = 1;
            // 
            // footerTexBox
            // 
            this.footerTexBox.Location = new System.Drawing.Point(76, 100);
            this.footerTexBox.Name = "footerTexBox";
            this.footerTexBox.Size = new System.Drawing.Size(407, 20);
            this.footerTexBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Footer:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Footer Spacing:";
            // 
            // footerSpacingNumericUpDown
            // 
            this.footerSpacingNumericUpDown.Location = new System.Drawing.Point(112, 127);
            this.footerSpacingNumericUpDown.Name = "footerSpacingNumericUpDown";
            this.footerSpacingNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.footerSpacingNumericUpDown.TabIndex = 3;
            // 
            // CancelPrintButton
            // 
            this.CancelPrintButton.Location = new System.Drawing.Point(493, 167);
            this.CancelPrintButton.Name = "CancelPrintButton";
            this.CancelPrintButton.Size = new System.Drawing.Size(75, 23);
            this.CancelPrintButton.TabIndex = 6;
            this.CancelPrintButton.Text = "Cancel";
            this.CancelPrintButton.UseVisualStyleBackColor = true;
            this.CancelPrintButton.Click += new System.EventHandler(this.CancelPrintButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(493, 196);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 7;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // titleComboLCR
            // 
            this.titleComboLCR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.titleComboLCR.FormattingEnabled = true;
            this.titleComboLCR.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.titleComboLCR.Location = new System.Drawing.Point(493, 32);
            this.titleComboLCR.Name = "titleComboLCR";
            this.titleComboLCR.Size = new System.Drawing.Size(66, 21);
            this.titleComboLCR.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(503, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Alignment:";
            // 
            // subTitleComboLCR
            // 
            this.subTitleComboLCR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subTitleComboLCR.FormattingEnabled = true;
            this.subTitleComboLCR.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.subTitleComboLCR.Location = new System.Drawing.Point(493, 66);
            this.subTitleComboLCR.Name = "subTitleComboLCR";
            this.subTitleComboLCR.Size = new System.Drawing.Size(66, 21);
            this.subTitleComboLCR.TabIndex = 18;
            // 
            // footerComboLCR
            // 
            this.footerComboLCR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.footerComboLCR.FormattingEnabled = true;
            this.footerComboLCR.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.footerComboLCR.Location = new System.Drawing.Point(493, 100);
            this.footerComboLCR.Name = "footerComboLCR";
            this.footerComboLCR.Size = new System.Drawing.Size(66, 21);
            this.footerComboLCR.TabIndex = 19;
            // 
            // GlobalPrinterSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 245);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.footerComboLCR);
            this.Controls.Add(this.gridComboAlignment);
            this.Controls.Add(this.subTitleComboLCR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.titleComboLCR);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.CancelPrintButton);
            this.Controls.Add(this.footerSpacingNumericUpDown);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.footerTexBox);
            this.Controls.Add(this.subTitleTextBox);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pageNumbersInHeaderTextBox);
            this.Controls.Add(this.showPageNumbersCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GlobalPrinterSettings";
            this.Text = "Print Preferencesfor this Report";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.footerSpacingNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox showPageNumbersCheckBox;
        private System.Windows.Forms.CheckBox pageNumbersInHeaderTextBox;
        private System.Windows.Forms.CheckBox proportionalSpacingCHeckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.TextBox subTitleTextBox;
        private System.Windows.Forms.TextBox footerTexBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown footerSpacingNumericUpDown;
        private System.Windows.Forms.Button CancelPrintButton;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox gridComboAlignment;
        private System.Windows.Forms.ComboBox titleComboLCR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox subTitleComboLCR;
        private System.Windows.Forms.ComboBox footerComboLCR;
    }
}