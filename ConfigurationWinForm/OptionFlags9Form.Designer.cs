﻿namespace ConfigurationWinForm
{
    partial class OptionFlags9Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(17, 26);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(415, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - Set if a Hastus/Trip ID number is enabled at farebox";
            this.toolTip2.SetToolTip(this.checkBox1, "Set if a Hastus/Trip ID number is enabled at farebox");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(17, 53);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(421, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x0002 - Set if Hastus/Trip ID is selected as a TRIP ID number";
            this.toolTip2.SetToolTip(this.checkBox2, "Set if Hastus/Trip ID is selected as a TRIP ID number");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(17, 107);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(927, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x0008 - Set if farebox allow to run if cashbox is NOT detected but door is close" +
    "d.- Cashbox was detected, then “gone” but door never opened.";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(17, 80);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(464, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x0004 - Allow the display of the Hastus/Trip ID on OCU Main Screen";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(17, 134);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(512, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "0x0010 - Hastus number allowed to be entered even if comm. Is OK with AVL";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(17, 188);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(380, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "0x0040 - Two track reader enabled. Columbus, OH only";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(17, 217);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(269, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Map Hastus to Trip in events";
            this.toolTip2.SetToolTip(this.checkBox8, "Map Hastus to Trip in events");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(17, 242);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(494, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – Filter out non-emergency messages sent on J1708 when probed";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(17, 268);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(586, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Don’t display preset value if disabled and only display fare set enabled" +
    " in menu";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(17, 295);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(548, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – Don’t display the word “TRANSFER” on passenger LCD display when idle";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(17, 322);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(442, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – Peak times to be used on weekend. *Columbus, OH only";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(17, 350);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(917, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – TRiM Units are LUCC’s. Setting this bit forces the entire fleet to be se" +
    "t as \'LUCC\'.If not set, then device type is configured at farebox.";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox6);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox4);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox10);
            this.optionflagsGridTitle.Controls.Add(this.checkBox3);
            this.optionflagsGridTitle.Controls.Add(this.checkBox11);
            this.optionflagsGridTitle.Controls.Add(this.checkBox7);
            this.optionflagsGridTitle.Controls.Add(this.checkBox12);
            this.optionflagsGridTitle.Controls.Add(this.checkBox5);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(991, 489);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 457);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(17, 161);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(413, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "0x0020 - Change Poll to AVL system to PID 234 – Software ID";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(17, 430);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(589, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Enable display of error messages on patron display when in “Standalone” " +
    "mode";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(17, 402);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(832, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 – Long log off (double auto-logoff time to 4 hrs.)This will be overridden " +
    "by OptionFlags3 bit 14 and OptionFlags18 bit 12.";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(17, 377);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(548, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Enable \'Mini-TVM\' mode (no OCU, documents issued via patron buttons).";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags9Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 514);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags9Form";
            this.Text = "OptionFlag9";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button1;


    }
}