﻿namespace ConfigurationWinForm
{
    partial class DatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.START = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backupBrowseBtn = new System.Windows.Forms.Button();
            this.sysconBrowseBtn = new System.Windows.Forms.Button();
            this.databaseBrowseBtn = new System.Windows.Forms.Button();
            this.BkUp = new System.Windows.Forms.TextBox();
            this.SecDBF = new System.Windows.Forms.TextBox();
            this.DBF = new System.Windows.Forms.TextBox();
            this.ENG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.START);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.backupBrowseBtn);
            this.groupBox1.Controls.Add(this.sysconBrowseBtn);
            this.groupBox1.Controls.Add(this.databaseBrowseBtn);
            this.groupBox1.Controls.Add(this.BkUp);
            this.groupBox1.Controls.Add(this.SecDBF);
            this.groupBox1.Controls.Add(this.DBF);
            this.groupBox1.Controls.Add(this.ENG);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1108, 263);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // START
            // 
            this.START.Location = new System.Drawing.Point(235, 214);
            this.START.Name = "START";
            this.START.Size = new System.Drawing.Size(864, 22);
            this.START.TabIndex = 7;
            this.START.TextChanged += new System.EventHandler(this.SaveData);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "[START] Sybase Start Command:";
            // 
            // backupBrowseBtn
            // 
            this.backupBrowseBtn.Location = new System.Drawing.Point(826, 167);
            this.backupBrowseBtn.Name = "backupBrowseBtn";
            this.backupBrowseBtn.Size = new System.Drawing.Size(78, 23);
            this.backupBrowseBtn.TabIndex = 6;
            this.backupBrowseBtn.Text = "Browse...";
            this.backupBrowseBtn.UseVisualStyleBackColor = true;
            this.backupBrowseBtn.Click += new System.EventHandler(this.backupBrowseBtn_Click);
            // 
            // sysconBrowseBtn
            // 
            this.sysconBrowseBtn.Location = new System.Drawing.Point(826, 128);
            this.sysconBrowseBtn.Name = "sysconBrowseBtn";
            this.sysconBrowseBtn.Size = new System.Drawing.Size(78, 23);
            this.sysconBrowseBtn.TabIndex = 4;
            this.sysconBrowseBtn.Text = "Browse...";
            this.sysconBrowseBtn.UseVisualStyleBackColor = true;
            this.sysconBrowseBtn.Click += new System.EventHandler(this.sysconBrowseBtn_Click);
            // 
            // databaseBrowseBtn
            // 
            this.databaseBrowseBtn.Location = new System.Drawing.Point(826, 86);
            this.databaseBrowseBtn.Name = "databaseBrowseBtn";
            this.databaseBrowseBtn.Size = new System.Drawing.Size(78, 23);
            this.databaseBrowseBtn.TabIndex = 2;
            this.databaseBrowseBtn.Text = "Browse...";
            this.databaseBrowseBtn.UseVisualStyleBackColor = true;
            this.databaseBrowseBtn.Click += new System.EventHandler(this.databaseBrowseBtn_Click);
            // 
            // BkUp
            // 
            this.BkUp.Location = new System.Drawing.Point(235, 170);
            this.BkUp.Name = "BkUp";
            this.BkUp.Size = new System.Drawing.Size(569, 22);
            this.BkUp.TabIndex = 5;
            this.BkUp.TextChanged += new System.EventHandler(this.SaveData);
            // 
            // SecDBF
            // 
            this.SecDBF.Location = new System.Drawing.Point(235, 128);
            this.SecDBF.Name = "SecDBF";
            this.SecDBF.Size = new System.Drawing.Size(569, 22);
            this.SecDBF.TabIndex = 3;
            this.SecDBF.TextChanged += new System.EventHandler(this.SaveData);
            // 
            // DBF
            // 
            this.DBF.Location = new System.Drawing.Point(235, 86);
            this.DBF.Name = "DBF";
            this.DBF.Size = new System.Drawing.Size(569, 22);
            this.DBF.TabIndex = 1;
            this.DBF.TextChanged += new System.EventHandler(this.SaveData);
            // 
            // ENG
            // 
            this.ENG.Location = new System.Drawing.Point(235, 44);
            this.ENG.Name = "ENG";
            this.ENG.Size = new System.Drawing.Size(569, 22);
            this.ENG.TabIndex = 0;
            this.ENG.TextChanged += new System.EventHandler(this.SaveData);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "[BkUp] Backup Folder:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "[SecDBF] SYSCON database:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "[DBF] Genfare Database:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "[ENG] Sybase Server Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, -3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Database Parameters";
            // 
            // DatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 294);
            this.Controls.Add(this.groupBox1);
            this.Name = "DatabaseForm";
            this.Text = "Database";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox BkUp;
        private System.Windows.Forms.TextBox SecDBF;
        private System.Windows.Forms.TextBox DBF;
        private System.Windows.Forms.TextBox ENG;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button backupBrowseBtn;
        private System.Windows.Forms.Button sysconBrowseBtn;
        private System.Windows.Forms.Button databaseBrowseBtn;
        private System.Windows.Forms.TextBox START;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;

    }
}