﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    class HttpConfigClient
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        public string GetLocationFolder(int loc)
        {
            return ("a" + Convert.ToString(Convert.ToChar(0x60 + loc)) + ".cnf");
        }

        public bool RetrieveRemoteConfigurationFile(locationDetail loc, string gfiPath, string fn, int httpPort)
        {
            string dataPath = gfiPath + "\\" + GetLocationFolder(loc.locNumber);
            string filename = dataPath + "\\" + fn;
            if (!Directory.Exists(dataPath))
                Directory.CreateDirectory(dataPath);
            StreamWriter sw = new StreamWriter(filename);
            try
            {
                using (var httpClient = new HttpClient())
                {
                    Uri remoteurl = new Uri("http://" + loc.hostName + ":"+ httpPort.ToString()+ "/CNFGET+" + fn);
                    var result = httpClient.GetByteArrayAsync(remoteurl).Result;
                    string S = Encoding.UTF8.GetString(result);
                    sw.Write(S);
                    sw.Close();
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show("Error Retrieving File: " + exception.ToString());
                return false;
            }
            return true;
        }

        public void SendRemoteConfigurationFile(locationDetail loc, string gfiPath, string fn, int httpPort)
        {
            string filename = gfiPath + "\\" + GetLocationFolder(loc.locNumber) + "\\" + fn;
            if (File.Exists(filename))
            {
                FileInfo f = new FileInfo(filename);
                Stream input = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] buffer = new byte[f.Length + 100];
                input.Read(buffer, 0, (int)f.Length);
                input.Close();

                ByteArrayContent byteContent = new ByteArrayContent(buffer);
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                string url = "http://" + loc.hostName + ":" + httpPort.ToString() + "/CNFPUT+" + fn;
                httpClient.PutAsync(url, byteContent);
                //httpClient.PutAsync(loc.hostName + ":"+ httpPort.ToString() + "/CNFPUT+" + fn, byteContent);
            }
            else
                MessageBox.Show("Unable to open file " + filename, "File Open Error");
        }
    }
}
