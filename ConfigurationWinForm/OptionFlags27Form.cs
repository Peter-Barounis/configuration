﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags27Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags27Form()
        {
            InitializeComponent();
        }

        public OptionFlags27Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[26].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[26]);
            this.numericUpDown1.Value = ((optionflag >> 0) & 0x00FF);
            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[26] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0000;
            setCheckboxChecked();
            setHexValue();
        }
        private void setCheckboxChecked()
        {
            this.numericUpDown1.Value = (optionflag & 0x0090);
            this.checkBox9.Checked = Convert.ToBoolean((optionflag >> 8) & 0x0001);
            this.checkBox10.Checked = Convert.ToBoolean((optionflag >> 9) & 0x0001);
            this.checkBox11.Checked = Convert.ToBoolean((optionflag >> 10) & 0x0001);
            this.checkBox12.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            this.checkBox13.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox14.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox15.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox16.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);
        }

        private void checkBox16_CheckedChanged(object sender, EventArgs e)
        {
            checkBox16.Checked = false;
            checkBox16.Enabled = false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int lmt1 = ((int)numericUpDown1.Value/10);
            optionflag = ((optionflag & 0xFF00) | ((lmt1 << 0) & 0x00FF));
            setHexValue();
        }
        private void numericUpDown1_EnabledChanged(object sender, EventArgs e)
        {
            numericUpDown1_ValueChanged(sender, e);
        }
    }
}
