﻿namespace ConfigurationWinForm
{
    partial class OptionFlags24Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionFlags24Form));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.numericUpDownMIFARE = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMIFARE)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 21);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(398, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - Enable iCLASS card detection. (Odyssey + / Fast Fare / Fast Fare –e)";
            this.toolTip2.SetToolTip(this.checkBox1, "Enable iCLASS card detection. (Odyssey + / Fast Fare / Fast Fare –e)");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(29, 43);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(306, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x0002 - Enable CLOUD support. (Fast Fare / Fast Fare –e)";
            this.toolTip2.SetToolTip(this.checkBox2, "Enable CLOUD support. (Fast Fare / Fast Fare –e)");
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(29, 87);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(347, 17);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "0x0008 - Enable Remote GDS (Garage Data System) upgrade ability";
            this.toolTip2.SetToolTip(this.checkBox4, "Enable Remote GDS (Garage Data System) upgrade ability");
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(29, 65);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(244, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "0x0004 - Enable Remote Cloud upgrade ability";
            this.toolTip2.SetToolTip(this.checkBox3, "Enable Remote Cloud upgrade ability");
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(29, 138);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(714, 17);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0F00 – \"Mandatory\" Stored value slot. This will be used to indicate if there is" +
    " a 0 0 mandatory Stored Value product and if so, where is it located.";
            this.toolTip2.SetToolTip(this.checkBox9, resources.GetString("checkBox9.ToolTip"));
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(29, 158);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(268, 17);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Disable Fare Deal passback. 0 = Enabled";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDownMIFARE);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox4);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox3);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(11, 11);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(749, 281);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(29, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(29, 223);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(446, 17);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Enable passback for MAGNETIC Value cards. follow same rules as Period Pa" +
    "ss";
            this.toolTip2.SetToolTip(this.checkBox16, "Enable passback for MAGNETIC Value cards. follow same rules as Period Pass\r\nThis " +
        "setting is for MAGNETIC only. Smart cards are handled in OptionFlags18. ");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(29, 201);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(439, 17);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 - Enable passback for MAGNETIC Ride cards. follow same rules as Period Pas" +
    "s";
            this.toolTip2.SetToolTip(this.checkBox15, "Enable passback for MAGNETIC Ride cards. follow same rules as Period Pass.\r\nThis " +
        "setting is for MAGNETIC only. \r\nSmart cards are handled in OptionFlags18. ");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(29, 180);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(429, 17);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – If passback on Stored Value cards is enabled, set this bit to use re-cla" +
    "ssified";
            this.toolTip2.SetToolTip(this.checkBox14, resources.GetString("checkBox14.ToolTip"));
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // numericUpDownMIFARE
            // 
            this.numericUpDownMIFARE.Location = new System.Drawing.Point(29, 109);
            this.numericUpDownMIFARE.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownMIFARE.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMIFARE.Name = "numericUpDownMIFARE";
            this.numericUpDownMIFARE.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownMIFARE.TabIndex = 17;
            this.numericUpDownMIFARE.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMIFARE.ValueChanged += new System.EventHandler(this.numericUpDownMIFARE_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(448, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "-MIFARE Classic sector - indicates which sector the Genfare information is in (1-" +
    "15, Default=1)";
            // 
            // OptionFlags24Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 304);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "OptionFlags24Form";
            this.Text = "OptionFlag24";
            this.toolTip2.SetToolTip(this, "(Recommended: 0x0010, Default: 0x0010)");
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMIFARE)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMIFARE;


    }
}