﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;
namespace ConfigurationWinForm
{
    public class locationDetail
    {
        public int locNumber;     // Get Location
        public string locName;
        public string locTransitAuth;
        public string hostName;
    }
    
    public class GFILocations
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 msg,
                                            Int32 wParam, IntPtr lParam);
        //[DllImport("gfipb32")]
        //private unsafe static extern int BuildConnectionString(byte[] buffer);

        //[DllImport("gfipb32")]
        //private unsafe static extern int BuildDotNetConnectionString(byte[] buffer);


        public SplitForm splitForm;
        private int NumberOfLocations = 0;
        private int NumLocations = 0;
        public List<locationDetail> CurrentLocations = new List<locationDetail>();
        private byte[] buffer = new byte[2048];

        public string GetLocationFolder(int loc)
        {
            return ("a" + Convert.ToString(Convert.ToChar(0x60 + loc)) + ".cnf");
        }
        // Find all of the locations and build a location string
        public GFILocations(SplitForm sf)
        {
            splitForm = sf;
        }

        public void LoadNMLocations()
        {
            int i;
            for (i = 0; (buffer[i] != 0) && (i < 1024); i++) ;
            Array.Resize(ref buffer, i);
            //connstr = System.Text.Encoding.ASCII.GetString(buffer);
            splitForm.odbcConnect(null);
            if (splitForm.globalSettings.connected)
            {
                OdbcCommand DbCommand = splitForm.cnnODBC.CreateCommand();
                DbCommand.CommandText = "SELECT loc_n, loc_name, authority FROM cnf;";
                OdbcDataReader DbReader = DbCommand.ExecuteReader();

                NumLocations = 0;
                while (DbReader.Read())
                {
                    locationDetail currLoc = new locationDetail();
                    currLoc.locNumber = Convert.ToInt16(DbReader.GetString(0));
                    currLoc.locName = DbReader.GetString(1);
                    currLoc.locTransitAuth = DbReader.GetString(2);
                    CurrentLocations.Add(currLoc);
                    NumberOfLocations++;
                }
                DbReader.Close();
                DbCommand.Dispose();
                splitForm.odbcDisConnect(null);

                // Now get the IP Address or Computer Name for each location
                foreach (locationDetail loc in CurrentLocations)
                {
                    string hostname = "SYSCON HOST" + loc.locNumber.ToString();
                    // Get default folder - First try 32 bit Registry
                    RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Software\GFI Genfare\Global", false);
                    if (key == null)
                    {
                        key = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\GFI Genfare\Global", false);
                    }
                    try
                    {
                        loc.hostName = key.GetValue(hostname).ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error:" + ex.ToString() + "\n\nPlease repair registry. Location Loading Halted!",
                            "Registry Key Not Found for Location " +
                            loc.locNumber.ToString() + ":" + loc.hostName);
                        for (int loc_n = loc.locNumber; loc_n <= NumLocations; loc_n++)
                            CurrentLocations.RemoveAt(loc_n - 1);
                        break;
                    }
                }
            }
        }
    }
}
