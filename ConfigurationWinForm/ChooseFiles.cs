﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class ChooseFiles : Form
    {

        public SplitForm splitForm;
        //public string fileType;
        public ChooseFiles()
        {
            InitializeComponent();
        }
        public ChooseFiles(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
        }
        
        private void Source_button_Click(object sender, EventArgs e)
        {
            string txt = splitForm.SelectFile(textBox1.Text,null);
            if (txt != null)
                textBox1.Text = txt;
        }

        private void Target_button_Click(object sender, EventArgs e)
        {
            string txt = splitForm.SelectFile(textBox2.Text,null);
            if (txt != null)
                textBox2.Text = txt;
        }

        private void Compare_button_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text.Trim() != "" && this.textBox2.Text.Trim() != "")
            {
                CompareFilesForm compare = new CompareFilesForm(this.splitForm, textBox1.Text, textBox2.Text);
                compare.Show();
            }
            else
            {
                MessageBox.Show("Please choose both Source File and Target File to Compare", "Select Files",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }

        }

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
