﻿using System;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using FastColoredTextBoxNS;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ConfigurationWinForm
{
    public partial class PowerfulSample : Form
    {
        MarkerStyle SameWordsStyle = new MarkerStyle(new SolidBrush(Color.FromArgb(40, Color.Gray)));
        odbctestForm odbcForm;
        AutocompleteMenu popupMenu = null;
        int connectionID = -1;
        public PowerfulSample()
        {
            InitializeComponent();
        }
        public PowerfulSample(odbctestForm odbcIn, bool floated)
        {
            InitializeComponent();
            odbcForm = odbcIn;

            // AUTOCOMPLETE CODE with column names
            // create autocomplete popup menu
            //popupMenu = new AutocompleteMenu(fctb);
            //popupMenu.SearchPattern = @"[\w\.]";
            //popupMenu.Items.SetAutocompleteItems(odbcIn.GetAutoCompleteItems());
            //popupMenu.Items.Width = 200;
            //popupMenu.AllowTabKey = true;
            //popupMenu.MinFragmentLength = 1;
            //popupMenu.ImageList = new ImageList();
            //popupMenu.ImageList.Images.Add((Image)Properties.Resources.table);
            //popupMenu.ImageList.Images.Add((Image)Properties.Resources.tablefcn);
            //popupMenu.ImageList.Images.Add((Image)Properties.Resources.tableview);
            //popupMenu.ImageList.Images.Add((Image)Properties.Resources.procedure);
            //popupMenu.ImageList.Images.Add((Image)Properties.Resources.column);

            // Set language to SQL 
            fctb.ClearStylesBuffer();
            fctb.Range.ClearStyle(StyleIndex.All);
            InitStylesPriority();
            fctb.AutoIndentNeeded -= fctb_AutoIndentNeeded;
            fctb.Language = Language.SQL;
            fctb.PreferredLineWidth = 0;
            dockToolStripMenuItem.Enabled = floated;
            // TODO - Should we allow the database name to be displayed in the Query -?
            txtCurrentDatabase.Visible=false;
            // TODO - Should we allow the name in the Query tab?
            //txtCurrentDatabase.Text = odbcIn.GetDatabaseName();
        }

        public void SetAutoCompleteMenu(TagConnection cnn)
        {
            popupMenu = new AutocompleteMenu(fctb);
            popupMenu.SearchPattern = @"[\w\.]";
            if (cnn != null)
                popupMenu.Items.SetAutocompleteItems(cnn.autocompleteItems);
            popupMenu.Items.Width = 200;
            popupMenu.AllowTabKey = true;
            popupMenu.MinFragmentLength = 1;
            popupMenu.ImageList = new ImageList();
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.table);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.tablefcn);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.tableview);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.procedure);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.column);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.procedure);
            connectionID = odbcForm.connectionID;   // Save current connection used
        }

        public void ReloadAutocompleteItems(TagConnection cnn)
        {
            //List<AutocompleteItem> aci = new List<AutocompleteItem>();
            //foreach(AutocompleteItem item in cnn.autocompleteItems)
            //    aci.Add(item);
            
            popupMenu = new AutocompleteMenu(fctb);
            popupMenu.SearchPattern = @"[\w\.]";
            if (cnn != null)
                popupMenu.Items.SetAutocompleteItems(cnn.autocompleteItems);
            popupMenu.Items.Width = 200;
            popupMenu.AllowTabKey = true;
            popupMenu.MinFragmentLength = 1;
            popupMenu.ImageList = new ImageList();
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.table);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.tablefcn);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.tableview);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.procedure);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.column);
            popupMenu.ImageList.Images.Add((Image)Properties.Resources.procedure);
            connectionID = odbcForm.connectionID;   // Save current connection used
            // TODO - Should we allow the name in the Query Tab?
            //txtCurrentDatabase.Text = odbcForm.GetDatabaseName();
        }

        private void InitStylesPriority()
        {           
            //add this style explicitly for drawing under other styles
            fctb.AddStyle(SameWordsStyle);
        }

        public void DisableDockMenu()
        {
            dockToolStripMenuItem.Enabled = false;
        }

        public void SelectAll(object sender, EventArgs e)
        {
            fctb.SelectAll();
        }
        public void Copy(object sender, EventArgs e)
        {
            fctb.Copy();
        }
        public void Paste(object sender, EventArgs e)
        {
            fctb.Paste();
        }
        public void Cut(object sender, EventArgs e)
        {
            fctb.Cut();
        }
        public void ClearChanged()
        {
            fctb.IsChanged = false;
        }
        public bool IsChanged()
        {
            return fctb.IsChanged;
        }

        public string GetText()
        {
            return fctb.Text;
        }

        public string GetSelectedText()
        {
            return fctb.SelectedText;
        }
        public int GetTextLength()
        {
            return fctb.Text.Length;
        }
        public void SetText(string text)
        {
            fctb.Text = text;
        }
        public void InsertText(string text)
        {
            if (fctb.Selection.Length > 0)
            {
                int start = fctb.SelectionStart;
                int end = fctb.SelectionLength;
                int len = fctb.Text.Length;
                fctb.Text = fctb.Text.Substring(0, start) + text + fctb.Text.Substring(start+end, len-(start+end));
            }
            else
            {
                fctb.Text += text;
            }
        }
 
 
        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.ShowFindDialog();
        }

        private void replaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.ShowReplaceDialog();
        }

        private void collapseSelectedBlockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.CollapseBlock(fctb.Selection.Start.iLine, fctb.Selection.End.iLine);
        }

        private void increaseIndentSiftTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.IncreaseIndent();
        }

        private void decreaseIndentTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.DecreaseIndent();
        }

        private void hTMLToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = this.Parent.Text.Trim();
            sfd.Filter = "HTML with <PRE> tag|*.html|HTML without <PRE> tag|*.html";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string html = "";

                if (sfd.FilterIndex == 1)
                {
                    html = fctb.Html;
                }
                if (sfd.FilterIndex == 2)
                {
                    
                    ExportToHTML exporter = new ExportToHTML();
                    exporter.UseBr = true;
                    exporter.UseNbsp = false;
                    exporter.UseForwardNbsp = true;
                    exporter.UseStyleTag = true;
                    html = exporter.GetHtml(fctb);
                }
                File.WriteAllText(sfd.FileName, html);
            }
        }

        private void fctb_SelectionChangedDelayed(object sender, EventArgs e)
        {
            fctb.VisibleRange.ClearStyle(SameWordsStyle);
            if (!fctb.Selection.IsEmpty)
                return;//user selected diapason

            //get fragment around caret
            var fragment = fctb.Selection.GetFragment(@"\w");
            string text = fragment.Text;
            if (text.Length == 0)
                return;
            //highlight same words
            var ranges = fctb.VisibleRange.GetRanges("\\b" + text + "\\b").ToArray();
            if(ranges.Length>1)
            foreach(var r in ranges)
                r.SetStyle(SameWordsStyle);
        }

        private void goForwardCtrlShiftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.NavigateForward();
        }

        private void goBackwardCtrlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.NavigateBackward();
        }

        private void autoIndentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.DoAutoIndent();
        }

        const int maxBracketSearchIterations = 2000;

        void GoLeftBracket(FastColoredTextBox tb, char leftBracket, char rightBracket)
        {
            Range range = tb.Selection.Clone();//need to clone because we will move caret
            int counter = 0;
            int maxIterations = maxBracketSearchIterations;
            while (range.GoLeftThroughFolded())//move caret left
            {
                if (range.CharAfterStart == leftBracket) counter++;
                if (range.CharAfterStart == rightBracket) counter--;
                if (counter == 1)
                {
                    //found
                    tb.Selection.Start = range.Start;
                    tb.DoSelectionVisible();
                    break;
                }
                //
                maxIterations--;
                if (maxIterations <= 0) break;
            }
            tb.Invalidate();
        }

        void GoRightBracket(FastColoredTextBox tb, char leftBracket, char rightBracket)
        {
            var range = tb.Selection.Clone();//need clone because we will move caret
            int counter = 0;
            int maxIterations = maxBracketSearchIterations;
            do
            {
                if (range.CharAfterStart == leftBracket) counter++;
                if (range.CharAfterStart == rightBracket) counter--;
                if (counter == -1)
                {
                    //found
                    tb.Selection.Start = range.Start;
                    tb.Selection.GoRightThroughFolded();
                    tb.DoSelectionVisible();
                    break;
                }
                //
                maxIterations--;
                if (maxIterations <= 0) break;
            } while (range.GoRightThroughFolded());//move caret right

            tb.Invalidate();
        }

        private void goLeftBracketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoLeftBracket(fctb, '{', '}');
        }

        private void goRightBracketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoRightBracket(fctb, '{', '}');
        }

        private void fctb_AutoIndentNeeded(object sender, AutoIndentEventArgs args)
        {
            //block {}
            if (Regex.IsMatch(args.LineText, @"^[^""']*\{.*\}[^""']*$"))
                return;
            //start of block {}
            if (Regex.IsMatch(args.LineText, @"^[^""']*\{"))
            {
                args.ShiftNextLines = args.TabLength;
                return;
            }
            //end of block {}
            if (Regex.IsMatch(args.LineText, @"}[^""']*$"))
            {
                args.Shift = -args.TabLength;
                args.ShiftNextLines = -args.TabLength;
                return;
            }
            //label
            if (Regex.IsMatch(args.LineText, @"^\s*\w+\s*:\s*($|//)") &&
                !Regex.IsMatch(args.LineText, @"^\s*default\s*:"))
            {
                args.Shift = -args.TabLength;
                return;
            }
            //some statements: case, default
            if (Regex.IsMatch(args.LineText, @"^\s*(case|default)\b.*:\s*($|//)"))
            {
                args.Shift = -args.TabLength / 2;
                return;
            }
            //is unclosed operator in previous line ?
            if (Regex.IsMatch(args.PrevLineText, @"^\s*(if|for|foreach|while|[\}\s]*else)\b[^{]*$"))
                if (!Regex.IsMatch(args.PrevLineText, @"(;\s*$)|(;\s*//)"))//operator is unclosed
                {
                    args.Shift = args.TabLength;
                    return;
                }
        }

        private void miPrint_Click(object sender, EventArgs e)
        {
            fctb.Print(new PrintDialogSettings() { ShowPrintPreviewDialog = true });
        }

        Random rnd = new Random();

        private void miChangeColors_Click(object sender, EventArgs e)
        {
            var styles = new Style[] { fctb.SyntaxHighlighter.BlueBoldStyle, fctb.SyntaxHighlighter.BlueStyle, fctb.SyntaxHighlighter.BoldStyle, fctb.SyntaxHighlighter.BrownStyle, fctb.SyntaxHighlighter.GrayStyle, fctb.SyntaxHighlighter.GreenStyle, fctb.SyntaxHighlighter.MagentaStyle, fctb.SyntaxHighlighter.MaroonStyle, fctb.SyntaxHighlighter.RedStyle };
            fctb.SyntaxHighlighter.AttributeStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.ClassNameStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.CommentStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.CommentTagStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.KeywordStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.NumberStyle = styles[rnd.Next(styles.Length)];
            fctb.SyntaxHighlighter.StringStyle = styles[rnd.Next(styles.Length)];

            fctb.OnSyntaxHighlight(new TextChangedEventArgs(fctb.Range));
        }

        private void setSelectedAsReadonlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.Selection.ReadOnly = true;
        }

        private void setSelectedAsWritableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.Selection.ReadOnly = false;
        }

        private void startStopMacroRecordingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.MacrosManager.IsRecording = !fctb.MacrosManager.IsRecording;
        }

        private void executeMacroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.MacrosManager.ExecuteMacros();
        }

        private void changeHotkeysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new HotkeysEditorForm(fctb.HotkeysMapping);
            if(form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                fctb.HotkeysMapping = form.GetHotkeys();
        }

        private void rTFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = this.Parent.Text.Trim();
            sfd.Filter = "RTF|*.rtf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string rtf = fctb.Rtf;
                File.WriteAllText(sfd.FileName, rtf);
            }
        }

        private void fctb_CustomAction(object sender, CustomActionEventArgs e)
        {
            MessageBox.Show(e.Action.ToString());
        }

        private void commentSelectedLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.InsertLinePrefix(fctb.CommentPrefix);
        }

        private void uncommentSelectedLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fctb.RemoveLinePrefix(fctb.CommentPrefix);
        }

        private void dockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this != null)
                odbcForm.DockCurrentWindow(this);
        }

        private void btnExecuteSQL_Click(object sender, EventArgs e)
        {
            odbcForm.executeFromPSMenu(this);
        }

        //private void PowerfulSample_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    //List<FATabStripItem> list = new List<FATabStripItem>();
        //    //foreach (FATabStripItem tab in tsFiles.Items)
        //    //    list.Add(tab);
        //    //foreach (var tab in list)
        //    //{
        //    //    TabStripItemClosingEventArgs args = new TabStripItemClosingEventArgs(tab);
        //    //    tsFiles_TabStripItemClosing(args);
        //    //    if (args.Cancel)
        //    //    {
        //    //        e.Cancel = true;
        //    //        return;
        //    //    }
        //    //    tsFiles.RemoveTab(tab);
        //    //}
        //}

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog sfd = new OpenFileDialog();
            sfd.Filter = "SQL File|*.sql|All files (*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                fctb.Text= File.ReadAllText(sfd.FileName);
                if (this.Parent != null)
                    this.Parent.Text = sfd.SafeFileName;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = this.Parent.Text.Trim();
            sfd.Filter = "SQL|*.sql|All files (*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string txt = fctb.Text;
                File.WriteAllText(sfd.FileName, txt);
                if (this.Parent != null)
                    this.Parent.Text = Path.GetFileName(sfd.FileName);
            }
        }

        private void executeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            odbcForm.ExecuteQuery();
        }

        private void fctb_Enter(object sender, EventArgs e)
        {
            TagConnection cnn = odbcForm.GetConnectionTag();
            if ((cnn != null) && (connectionID != odbcForm.connectionID))
            {
                SetAutoCompleteMenu(cnn);
            }
        }
    }
}
