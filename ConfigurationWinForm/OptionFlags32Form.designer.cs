﻿namespace ConfigurationWinForm
{
    partial class OptionFlags32Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.option_Flag32txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Option Flag 32:";
            // 
            // option_Flag32txt
            // 
            this.option_Flag32txt.Location = new System.Drawing.Point(175, 57);
            this.option_Flag32txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.option_Flag32txt.Name = "option_Flag32txt";
            this.option_Flag32txt.Size = new System.Drawing.Size(57, 22);
            this.option_Flag32txt.TabIndex = 3;
            this.option_Flag32txt.Text = "0x0000";
            this.option_Flag32txt.TextChanged += new System.EventHandler(this.optionFlag32txt_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(495, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "This option Flag  is currently not used but you may set the value if necessary.";
            // 
            // OptionFlags32Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 110);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.option_Flag32txt);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags32Form";
            this.Text = "OptionFlag32";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox option_Flag32txt;
        private System.Windows.Forms.Label label4;        
    }
}