﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class OptionFlags10Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags10Form()
        {
            InitializeComponent();
         }

        public OptionFlags10Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[9]);
            setCheckboxChecked();
            formLoaded = true;
            setHexValue();
        }

        private void setHexValue()
        {
            if (formLoaded)
            {
                optionflag &= ~(0x0006);
                switch (clearCbxCombo.SelectedIndex)
                {
                    case 0:
                        optionflag |= 0x0006;
                        break;
                    case 1:
                        optionflag |= 0x0004;
                        break;
                    case 2:
                        optionflag |= 0x0002;
                        break;
                }

                option_flagsGridTitle.Text = "Option Flags-[" + optionflag.ToString("X4") + "]";
                splitForm.iniConfig.inifileData.optionFlags[9] = "0x" + optionflag.ToString("X4");
            }
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
            if (formLoaded)
                splitForm.SetChanged();
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
            setHexValue();         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x0030;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            int clearType;
            checkBox1.Checked = Convert.ToBoolean(optionflag & 0x0001);
            checkBox4.Checked = Convert.ToBoolean(optionflag & 0x0008);
            checkBox5.Checked = Convert.ToBoolean(optionflag & 0x0010);
            checkBox6.Checked = Convert.ToBoolean(optionflag & 0x0020);
            checkBox7.Checked = Convert.ToBoolean(optionflag & 0x0040);
            checkBox8.Checked = Convert.ToBoolean(optionflag & 0x0080);
            checkBox9.Checked = Convert.ToBoolean(optionflag & 0x0100);
            checkBox10.Checked = Convert.ToBoolean(optionflag & 0x0200);
            checkBox11.Checked = Convert.ToBoolean(optionflag & 0x0400);
            checkBox12.Checked = Convert.ToBoolean(optionflag & 0x0800);
            checkBox13.Checked = Convert.ToBoolean(optionflag & 0x1000);
            checkBox14.Checked = Convert.ToBoolean(optionflag & 0x2000);
            checkBox15.Checked = Convert.ToBoolean(optionflag & 0x4000);
            checkBox16.Checked = Convert.ToBoolean(optionflag & 0x8000);
            clearType = (optionflag & 0x0006)>>1;
            switch (clearType)
            {
                case 2:
                    clearCbxCombo.SelectedIndex = 2;
                    break;
                case 4:
                    clearCbxCombo.SelectedIndex = 1;                    
                    break;
                default:
                    clearCbxCombo.SelectedIndex = 0;
                    break;
            }
        }

        private void clearCbxCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.SetChanged();
                setHexValue();
            }
        }
    }
}
