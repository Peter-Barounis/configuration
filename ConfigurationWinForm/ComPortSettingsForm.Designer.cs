﻿namespace ConfigurationWinForm
{
    partial class ComPortSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComPortSettingsForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.portSettingsgpbx = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ProbeType = new System.Windows.Forms.CheckBox();
            this.IsoBox = new System.Windows.Forms.CheckBox();
            this.scanBaudRatecbx = new System.Windows.Forms.CheckBox();
            this.FbxBaudRate = new System.Windows.Forms.ComboBox();
            this.BaudRate = new System.Windows.Forms.ComboBox();
            this.OldBaudrate = new System.Windows.Forms.ComboBox();
            this.channel4cbx = new System.Windows.Forms.CheckBox();
            this.channel3cbx = new System.Windows.Forms.CheckBox();
            this.channel2cbx = new System.Windows.Forms.CheckBox();
            this.channel1cbx = new System.Windows.Forms.CheckBox();
            this.Port = new System.Windows.Forms.ComboBox();
            this.debugSettingsgpbx = new System.Windows.Forms.GroupBox();
            this.dumpSQLNote = new System.Windows.Forms.Label();
            this.dumpSQL = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.FbxVersion = new System.Windows.Forms.NumericUpDown();
            this.WaitSync = new System.Windows.Forms.NumericUpDown();
            this.debugLevel = new System.Windows.Forms.NumericUpDown();
            this.DirectionType = new System.Windows.Forms.ComboBox();
            this.DumpTransactions = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.DirectionTypeLabel = new System.Windows.Forms.Label();
            this.DumpTransactionsLabel = new System.Windows.Forms.Label();
            this.debugLevelLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.portSettingsgpbx.SuspendLayout();
            this.debugSettingsgpbx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FbxVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaitSync)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.debugLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "PortEnbMask:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "MUXISOBox:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 305);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "ProbeType:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "OldBaudRate:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "BaudRate:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "FBXBaudRate:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "ScanBaudRate:";
            // 
            // portSettingsgpbx
            // 
            this.portSettingsgpbx.Controls.Add(this.label9);
            this.portSettingsgpbx.Controls.Add(this.ProbeType);
            this.portSettingsgpbx.Controls.Add(this.IsoBox);
            this.portSettingsgpbx.Controls.Add(this.scanBaudRatecbx);
            this.portSettingsgpbx.Controls.Add(this.FbxBaudRate);
            this.portSettingsgpbx.Controls.Add(this.BaudRate);
            this.portSettingsgpbx.Controls.Add(this.OldBaudrate);
            this.portSettingsgpbx.Controls.Add(this.channel4cbx);
            this.portSettingsgpbx.Controls.Add(this.channel3cbx);
            this.portSettingsgpbx.Controls.Add(this.channel2cbx);
            this.portSettingsgpbx.Controls.Add(this.channel1cbx);
            this.portSettingsgpbx.Controls.Add(this.Port);
            this.portSettingsgpbx.Controls.Add(this.label5);
            this.portSettingsgpbx.Controls.Add(this.label8);
            this.portSettingsgpbx.Controls.Add(this.label1);
            this.portSettingsgpbx.Controls.Add(this.label7);
            this.portSettingsgpbx.Controls.Add(this.label2);
            this.portSettingsgpbx.Controls.Add(this.label6);
            this.portSettingsgpbx.Controls.Add(this.label3);
            this.portSettingsgpbx.Controls.Add(this.label4);
            this.portSettingsgpbx.Location = new System.Drawing.Point(12, 21);
            this.portSettingsgpbx.Name = "portSettingsgpbx";
            this.portSettingsgpbx.Size = new System.Drawing.Size(329, 344);
            this.portSettingsgpbx.TabIndex = 8;
            this.portSettingsgpbx.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "PORT Settings";
            // 
            // ProbeType
            // 
            this.ProbeType.AutoSize = true;
            this.ProbeType.Location = new System.Drawing.Point(133, 306);
            this.ProbeType.Name = "ProbeType";
            this.ProbeType.Size = new System.Drawing.Size(18, 17);
            this.ProbeType.TabIndex = 23;
            this.toolTip1.SetToolTip(this.ProbeType, "Select if this is a PDU probe");
            this.ProbeType.UseVisualStyleBackColor = true;
            this.ProbeType.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // IsoBox
            // 
            this.IsoBox.AutoSize = true;
            this.IsoBox.Location = new System.Drawing.Point(133, 267);
            this.IsoBox.Name = "IsoBox";
            this.IsoBox.Size = new System.Drawing.Size(18, 17);
            this.IsoBox.TabIndex = 22;
            this.toolTip1.SetToolTip(this.IsoBox, "Use Isolation Box");
            this.IsoBox.UseVisualStyleBackColor = true;
            this.IsoBox.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // scanBaudRatecbx
            // 
            this.scanBaudRatecbx.AutoSize = true;
            this.scanBaudRatecbx.Location = new System.Drawing.Point(133, 227);
            this.scanBaudRatecbx.Name = "scanBaudRatecbx";
            this.scanBaudRatecbx.Size = new System.Drawing.Size(18, 17);
            this.scanBaudRatecbx.TabIndex = 21;
            this.toolTip1.SetToolTip(this.scanBaudRatecbx, "Scan Baud Rate");
            this.scanBaudRatecbx.UseVisualStyleBackColor = true;
            this.scanBaudRatecbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // FbxBaudRate
            // 
            this.FbxBaudRate.FormattingEnabled = true;
            this.FbxBaudRate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "921600"});
            this.FbxBaudRate.Location = new System.Drawing.Point(133, 188);
            this.FbxBaudRate.Name = "FbxBaudRate";
            this.FbxBaudRate.Size = new System.Drawing.Size(92, 24);
            this.FbxBaudRate.TabIndex = 20;
            this.FbxBaudRate.Text = "9600";
            this.toolTip1.SetToolTip(this.FbxBaudRate, "Farebox Baud Rate");
            this.FbxBaudRate.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // BaudRate
            // 
            this.BaudRate.FormattingEnabled = true;
            this.BaudRate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "921600"});
            this.BaudRate.Location = new System.Drawing.Point(133, 149);
            this.BaudRate.Name = "BaudRate";
            this.BaudRate.Size = new System.Drawing.Size(92, 24);
            this.BaudRate.TabIndex = 19;
            this.BaudRate.Text = "9600";
            this.toolTip1.SetToolTip(this.BaudRate, "Actual Baud Rate");
            this.BaudRate.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // OldBaudrate
            // 
            this.OldBaudrate.FormattingEnabled = true;
            this.OldBaudrate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "921600"});
            this.OldBaudrate.Location = new System.Drawing.Point(133, 110);
            this.OldBaudrate.Name = "OldBaudrate";
            this.OldBaudrate.Size = new System.Drawing.Size(92, 24);
            this.OldBaudrate.TabIndex = 18;
            this.OldBaudrate.Text = "9600";
            this.toolTip1.SetToolTip(this.OldBaudrate, "Default Baud Rate");
            this.OldBaudrate.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel4cbx
            // 
            this.channel4cbx.AutoSize = true;
            this.channel4cbx.Location = new System.Drawing.Point(278, 69);
            this.channel4cbx.Name = "channel4cbx";
            this.channel4cbx.Size = new System.Drawing.Size(38, 21);
            this.channel4cbx.TabIndex = 17;
            this.channel4cbx.Text = "4";
            this.toolTip1.SetToolTip(this.channel4cbx, "Use Channel 4");
            this.channel4cbx.UseVisualStyleBackColor = true;
            this.channel4cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel3cbx
            // 
            this.channel3cbx.AutoSize = true;
            this.channel3cbx.Location = new System.Drawing.Point(230, 69);
            this.channel3cbx.Name = "channel3cbx";
            this.channel3cbx.Size = new System.Drawing.Size(38, 21);
            this.channel3cbx.TabIndex = 16;
            this.channel3cbx.Text = "3";
            this.toolTip1.SetToolTip(this.channel3cbx, "Use Channel 3");
            this.channel3cbx.UseVisualStyleBackColor = true;
            this.channel3cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel2cbx
            // 
            this.channel2cbx.AutoSize = true;
            this.channel2cbx.Location = new System.Drawing.Point(182, 69);
            this.channel2cbx.Name = "channel2cbx";
            this.channel2cbx.Size = new System.Drawing.Size(38, 21);
            this.channel2cbx.TabIndex = 15;
            this.channel2cbx.Text = "2";
            this.toolTip1.SetToolTip(this.channel2cbx, "Use Channel 2");
            this.channel2cbx.UseVisualStyleBackColor = true;
            this.channel2cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel1cbx
            // 
            this.channel1cbx.AutoSize = true;
            this.channel1cbx.Location = new System.Drawing.Point(134, 69);
            this.channel1cbx.Name = "channel1cbx";
            this.channel1cbx.Size = new System.Drawing.Size(38, 21);
            this.channel1cbx.TabIndex = 14;
            this.channel1cbx.Text = "1";
            this.toolTip1.SetToolTip(this.channel1cbx, "Use Channel 1");
            this.channel1cbx.UseVisualStyleBackColor = true;
            this.channel1cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // Port
            // 
            this.Port.FormattingEnabled = true;
            this.Port.Location = new System.Drawing.Point(132, 28);
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(124, 24);
            this.Port.TabIndex = 9;
            this.toolTip1.SetToolTip(this.Port, "Mux Serial POrt ");
            this.Port.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // debugSettingsgpbx
            // 
            this.debugSettingsgpbx.Controls.Add(this.dumpSQLNote);
            this.debugSettingsgpbx.Controls.Add(this.dumpSQL);
            this.debugSettingsgpbx.Controls.Add(this.label15);
            this.debugSettingsgpbx.Controls.Add(this.FbxVersion);
            this.debugSettingsgpbx.Controls.Add(this.WaitSync);
            this.debugSettingsgpbx.Controls.Add(this.debugLevel);
            this.debugSettingsgpbx.Controls.Add(this.DirectionType);
            this.debugSettingsgpbx.Controls.Add(this.DumpTransactions);
            this.debugSettingsgpbx.Controls.Add(this.label14);
            this.debugSettingsgpbx.Controls.Add(this.label13);
            this.debugSettingsgpbx.Controls.Add(this.DirectionTypeLabel);
            this.debugSettingsgpbx.Controls.Add(this.DumpTransactionsLabel);
            this.debugSettingsgpbx.Controls.Add(this.debugLevelLabel);
            this.debugSettingsgpbx.Location = new System.Drawing.Point(365, 21);
            this.debugSettingsgpbx.Name = "debugSettingsgpbx";
            this.debugSettingsgpbx.Size = new System.Drawing.Size(404, 344);
            this.debugSettingsgpbx.TabIndex = 9;
            this.debugSettingsgpbx.TabStop = false;
            // 
            // dumpSQLNote
            // 
            this.dumpSQLNote.AutoSize = true;
            this.dumpSQLNote.Location = new System.Drawing.Point(11, 237);
            this.dumpSQLNote.Name = "dumpSQLNote";
            this.dumpSQLNote.Size = new System.Drawing.Size(369, 85);
            this.dumpSQLNote.TabIndex = 31;
            this.dumpSQLNote.Text = resources.GetString("dumpSQLNote.Text");
            // 
            // dumpSQL
            // 
            this.dumpSQL.AutoSize = true;
            this.dumpSQL.Location = new System.Drawing.Point(9, 212);
            this.dumpSQL.Name = "dumpSQL";
            this.dumpSQL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dumpSQL.Size = new System.Drawing.Size(99, 21);
            this.dumpSQL.TabIndex = 30;
            this.dumpSQL.Text = ":DumpSQL";
            this.dumpSQL.UseVisualStyleBackColor = true;
            this.dumpSQL.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 17);
            this.label15.TabIndex = 28;
            this.label15.Text = "Debug Settings";
            // 
            // FbxVersion
            // 
            this.FbxVersion.Location = new System.Drawing.Point(151, 72);
            this.FbxVersion.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.FbxVersion.Name = "FbxVersion";
            this.FbxVersion.Size = new System.Drawing.Size(113, 22);
            this.FbxVersion.TabIndex = 27;
            this.toolTip1.SetToolTip(this.FbxVersion, "Farebox Version");
            this.FbxVersion.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // WaitSync
            // 
            this.WaitSync.Location = new System.Drawing.Point(151, 30);
            this.WaitSync.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.WaitSync.Name = "WaitSync";
            this.WaitSync.Size = new System.Drawing.Size(113, 22);
            this.WaitSync.TabIndex = 26;
            this.toolTip1.SetToolTip(this.WaitSync, "Channel Wait Time");
            this.WaitSync.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // debugLevel
            // 
            this.debugLevel.Location = new System.Drawing.Point(151, 109);
            this.debugLevel.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.debugLevel.Name = "debugLevel";
            this.debugLevel.Size = new System.Drawing.Size(43, 22);
            this.debugLevel.TabIndex = 23;
            this.toolTip1.SetToolTip(this.debugLevel, "Mux/Loader debug Level 0-9");
            this.debugLevel.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // DirectionType
            // 
            this.DirectionType.FormattingEnabled = true;
            this.DirectionType.Items.AddRange(new object[] {
            "0 = Inbound/Outbound",
            "1 = North/South/East/West"});
            this.DirectionType.Location = new System.Drawing.Point(151, 179);
            this.DirectionType.Name = "DirectionType";
            this.DirectionType.Size = new System.Drawing.Size(202, 24);
            this.DirectionType.TabIndex = 19;
            this.DirectionType.Text = "0 = Inbound/Outbound";
            this.toolTip1.SetToolTip(this.DirectionType, "Direction Type");
            this.DirectionType.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // DumpTransactions
            // 
            this.DumpTransactions.AutoSize = true;
            this.DumpTransactions.Location = new System.Drawing.Point(151, 149);
            this.DumpTransactions.Name = "DumpTransactions";
            this.DumpTransactions.Size = new System.Drawing.Size(18, 17);
            this.DumpTransactions.TabIndex = 18;
            this.DumpTransactions.UseVisualStyleBackColor = true;
            this.DumpTransactions.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "FbxVersion:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 17);
            this.label13.TabIndex = 12;
            this.label13.Text = "WaitSync:";
            // 
            // DirectionTypeLabel
            // 
            this.DirectionTypeLabel.AutoSize = true;
            this.DirectionTypeLabel.Location = new System.Drawing.Point(11, 182);
            this.DirectionTypeLabel.Name = "DirectionTypeLabel";
            this.DirectionTypeLabel.Size = new System.Drawing.Size(62, 17);
            this.DirectionTypeLabel.TabIndex = 11;
            this.DirectionTypeLabel.Text = "DirType:";
            // 
            // DumpTransactionsLabel
            // 
            this.DumpTransactionsLabel.AutoSize = true;
            this.DumpTransactionsLabel.Location = new System.Drawing.Point(11, 148);
            this.DumpTransactionsLabel.Name = "DumpTransactionsLabel";
            this.DumpTransactionsLabel.Size = new System.Drawing.Size(131, 17);
            this.DumpTransactionsLabel.TabIndex = 10;
            this.DumpTransactionsLabel.Text = "DumpTransactions:";
            this.toolTip1.SetToolTip(this.DumpTransactionsLabel, "Dump Transations to Log");
            // 
            // debugLevelLabel
            // 
            this.debugLevelLabel.AutoSize = true;
            this.debugLevelLabel.Location = new System.Drawing.Point(11, 110);
            this.debugLevelLabel.Name = "debugLevelLabel";
            this.debugLevelLabel.Size = new System.Drawing.Size(88, 17);
            this.debugLevelLabel.TabIndex = 9;
            this.debugLevelLabel.Text = "DebugLevel:";
            this.toolTip1.SetToolTip(this.debugLevelLabel, "DebugLevel (0-9)");
            // 
            // ComPortSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 377);
            this.Controls.Add(this.debugSettingsgpbx);
            this.Controls.Add(this.portSettingsgpbx);
            this.Name = "ComPortSettingsForm";
            this.Text = "[MUX1] COM-Debug Settings";
            this.portSettingsgpbx.ResumeLayout(false);
            this.portSettingsgpbx.PerformLayout();
            this.debugSettingsgpbx.ResumeLayout(false);
            this.debugSettingsgpbx.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FbxVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaitSync)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.debugLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox portSettingsgpbx;
        private System.Windows.Forms.CheckBox ProbeType;
        private System.Windows.Forms.CheckBox IsoBox;
        private System.Windows.Forms.CheckBox scanBaudRatecbx;
        private System.Windows.Forms.ComboBox FbxBaudRate;
        private System.Windows.Forms.ComboBox BaudRate;
        private System.Windows.Forms.ComboBox OldBaudrate;
        private System.Windows.Forms.CheckBox channel4cbx;
        private System.Windows.Forms.CheckBox channel3cbx;
        private System.Windows.Forms.CheckBox channel2cbx;
        private System.Windows.Forms.CheckBox channel1cbx;
        private System.Windows.Forms.ComboBox Port;
        private System.Windows.Forms.GroupBox debugSettingsgpbx;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label DirectionTypeLabel;
        private System.Windows.Forms.Label DumpTransactionsLabel;
        private System.Windows.Forms.Label debugLevelLabel;
        private System.Windows.Forms.ComboBox DirectionType;
        private System.Windows.Forms.CheckBox DumpTransactions;
        private System.Windows.Forms.NumericUpDown debugLevel;
        private System.Windows.Forms.NumericUpDown WaitSync;
        private System.Windows.Forms.NumericUpDown FbxVersion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label dumpSQLNote;
        private System.Windows.Forms.CheckBox dumpSQL;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}