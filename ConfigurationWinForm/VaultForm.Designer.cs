﻿namespace ConfigurationWinForm
{
    partial class VaultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VaultForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DebugLevel = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.VaultFull = new System.Windows.Forms.TextBox();
            this.LockOut = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.channel4cbx = new System.Windows.Forms.CheckBox();
            this.channel3cbx = new System.Windows.Forms.CheckBox();
            this.channel2cbx = new System.Windows.Forms.CheckBox();
            this.channel1cbx = new System.Windows.Forms.CheckBox();
            this.Port = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ChannelText = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.vaultFlagsText = new System.Windows.Forms.Label();
            this.DebugFrames = new System.Windows.Forms.CheckBox();
            this.DebugProtocol = new System.Windows.Forms.CheckBox();
            this.WriteRawFile = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DebugLevel)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.DebugLevel);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.VaultFull);
            this.groupBox1.Controls.Add(this.LockOut);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.channel4cbx);
            this.groupBox1.Controls.Add(this.channel3cbx);
            this.groupBox1.Controls.Add(this.channel2cbx);
            this.groupBox1.Controls.Add(this.channel1cbx);
            this.groupBox1.Controls.Add(this.Port);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ChannelText);
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(361, 197);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Vault Settings";
            // 
            // DebugLevel
            // 
            this.DebugLevel.Location = new System.Drawing.Point(110, 159);
            this.DebugLevel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DebugLevel.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.DebugLevel.Name = "DebugLevel";
            this.DebugLevel.Size = new System.Drawing.Size(28, 20);
            this.DebugLevel.TabIndex = 33;
            this.DebugLevel.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 161);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Debug Level (1-9):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(204, 126);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Full amount in dollars ($)";
            // 
            // VaultFull
            // 
            this.VaultFull.Location = new System.Drawing.Point(109, 124);
            this.VaultFull.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.VaultFull.Name = "VaultFull";
            this.VaultFull.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.VaultFull.Size = new System.Drawing.Size(92, 20);
            this.VaultFull.TabIndex = 29;
            this.VaultFull.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // LockOut
            // 
            this.LockOut.FormattingEnabled = true;
            this.LockOut.Items.AddRange(new object[] {
            "0 - None",
            "2 - Detroit/Electronic Vault Enabled",
            "3 - CTA shroud interface enabled "});
            this.LockOut.Location = new System.Drawing.Point(109, 93);
            this.LockOut.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LockOut.Name = "LockOut";
            this.LockOut.Size = new System.Drawing.Size(219, 21);
            this.LockOut.TabIndex = 28;
            this.LockOut.Text = "2 - Detroit/Electronic Vault Enabledd";
            this.LockOut.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 126);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Vault Full:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 93);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Lockout:";
            // 
            // channel4cbx
            // 
            this.channel4cbx.AutoSize = true;
            this.channel4cbx.Location = new System.Drawing.Point(282, 64);
            this.channel4cbx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.channel4cbx.Name = "channel4cbx";
            this.channel4cbx.Size = new System.Drawing.Size(32, 17);
            this.channel4cbx.TabIndex = 24;
            this.channel4cbx.Text = "4";
            this.channel4cbx.UseVisualStyleBackColor = true;
            this.channel4cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel3cbx
            // 
            this.channel3cbx.AutoSize = true;
            this.channel3cbx.Location = new System.Drawing.Point(224, 64);
            this.channel3cbx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.channel3cbx.Name = "channel3cbx";
            this.channel3cbx.Size = new System.Drawing.Size(32, 17);
            this.channel3cbx.TabIndex = 23;
            this.channel3cbx.Text = "3";
            this.channel3cbx.UseVisualStyleBackColor = true;
            this.channel3cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel2cbx
            // 
            this.channel2cbx.AutoSize = true;
            this.channel2cbx.Location = new System.Drawing.Point(166, 64);
            this.channel2cbx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.channel2cbx.Name = "channel2cbx";
            this.channel2cbx.Size = new System.Drawing.Size(32, 17);
            this.channel2cbx.TabIndex = 22;
            this.channel2cbx.Text = "2";
            this.channel2cbx.UseVisualStyleBackColor = true;
            this.channel2cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // channel1cbx
            // 
            this.channel1cbx.AutoSize = true;
            this.channel1cbx.Location = new System.Drawing.Point(109, 64);
            this.channel1cbx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.channel1cbx.Name = "channel1cbx";
            this.channel1cbx.Size = new System.Drawing.Size(32, 17);
            this.channel1cbx.TabIndex = 21;
            this.channel1cbx.Text = "1";
            this.channel1cbx.UseVisualStyleBackColor = true;
            this.channel1cbx.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // Port
            // 
            this.Port.FormattingEnabled = true;
            this.Port.Location = new System.Drawing.Point(109, 27);
            this.Port.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(94, 21);
            this.Port.TabIndex = 20;
            this.Port.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Port:";
            // 
            // ChannelText
            // 
            this.ChannelText.AutoSize = true;
            this.ChannelText.Location = new System.Drawing.Point(10, 64);
            this.ChannelText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ChannelText.Name = "ChannelText";
            this.ChannelText.Size = new System.Drawing.Size(81, 13);
            this.ChannelText.TabIndex = 19;
            this.ChannelText.Text = "Channel [0x0F]:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WriteRawFile);
            this.groupBox2.Controls.Add(this.vaultFlagsText);
            this.groupBox2.Controls.Add(this.DebugFrames);
            this.groupBox2.Controls.Add(this.DebugProtocol);
            this.groupBox2.Location = new System.Drawing.Point(9, 221);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(361, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // vaultFlagsText
            // 
            this.vaultFlagsText.AutoSize = true;
            this.vaultFlagsText.Location = new System.Drawing.Point(10, 0);
            this.vaultFlagsText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.vaultFlagsText.Name = "vaultFlagsText";
            this.vaultFlagsText.Size = new System.Drawing.Size(91, 13);
            this.vaultFlagsText.TabIndex = 2;
            this.vaultFlagsText.Text = "Vault Flags - [0xff]";
            // 
            // DebugFrames
            // 
            this.DebugFrames.AutoSize = true;
            this.DebugFrames.Location = new System.Drawing.Point(234, 25);
            this.DebugFrames.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DebugFrames.Name = "DebugFrames";
            this.DebugFrames.Size = new System.Drawing.Size(110, 17);
            this.DebugFrames.TabIndex = 1;
            this.DebugFrames.Text = "Debug Frame flag";
            this.DebugFrames.UseVisualStyleBackColor = true;
            this.DebugFrames.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // DebugProtocol
            // 
            this.DebugProtocol.AutoSize = true;
            this.DebugProtocol.Location = new System.Drawing.Point(9, 25);
            this.DebugProtocol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DebugProtocol.Name = "DebugProtocol";
            this.DebugProtocol.Size = new System.Drawing.Size(178, 17);
            this.DebugProtocol.TabIndex = 0;
            this.DebugProtocol.Text = "Debug protocol communications";
            this.DebugProtocol.UseVisualStyleBackColor = true;
            this.DebugProtocol.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // WriteRawFile
            // 
            this.WriteRawFile.AutoSize = true;
            this.WriteRawFile.Location = new System.Drawing.Point(9, 62);
            this.WriteRawFile.Name = "WriteRawFile";
            this.WriteRawFile.Size = new System.Drawing.Size(268, 17);
            this.WriteRawFile.TabIndex = 3;
            this.WriteRawFile.Text = "Write raw file - Do not update database immediately";
            this.toolTip1.SetToolTip(this.WriteRawFile, resources.GetString("WriteRawFile.ToolTip"));
            this.WriteRawFile.UseVisualStyleBackColor = true;
            this.WriteRawFile.CheckedChanged += new System.EventHandler(this.UpdateData);
            // 
            // VaultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 332);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "VaultForm";
            this.Text = "Vlt1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DebugLevel)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox VaultFull;
        private System.Windows.Forms.ComboBox LockOut;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox channel4cbx;
        private System.Windows.Forms.CheckBox channel3cbx;
        private System.Windows.Forms.CheckBox channel2cbx;
        private System.Windows.Forms.CheckBox channel1cbx;
        private System.Windows.Forms.ComboBox Port;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ChannelText;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox DebugFrames;
        private System.Windows.Forms.CheckBox DebugProtocol;
        private System.Windows.Forms.NumericUpDown DebugLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label vaultFlagsText;
        private System.Windows.Forms.CheckBox WriteRawFile;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}