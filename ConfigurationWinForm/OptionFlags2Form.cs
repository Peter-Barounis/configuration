﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class OptionFlags2Form : Form
    {
        public SplitForm splitForm;
        public bool formLoaded = false;
        public int optionflag = 0;
        public int securityCodeCount = 0;

        ToolTip tooltip1 = new ToolTip();
        private CheckBox[] cbxArray= new CheckBox[16];
        public OptionFlags2Form()
        {
            InitializeComponent();            
        }

        public OptionFlags2Form(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            //this.optionflag = int.Parse(splitForm.iniConfig.inifileData.optionFlags[1].Substring(2), System.Globalization.NumberStyles.HexNumber);
            optionflag = (int)Util.ParseValue(splitForm.iniConfig.inifileData.optionFlags[1]);
            securityCodeCount = (optionflag & 0x0700) >> 8;

            // Security Code Count out of range, then set to 2
            if (securityCodeCount < 0 || securityCodeCount > 7)
                securityCodeCount = 2;

            // Mask out security code 
            optionflag &= 0xf0ff;       

            setCheckboxChecked();
            setHexValue();
            formLoaded = true;
        }

        private void setHexValue()
        {
            // Add security count to option flag
            optionflag &= 0xF0FF;
            optionflag |= (securityCodeCount << 8);

            optionflagsGridTitle.Text = "Option Flags-[0x" + optionflag.ToString("X4") + "]";
            splitForm.iniConfig.inifileData.optionFlags[1] = "0x" + optionflag.ToString("X4");
            if(formLoaded)
                splitForm.SetChanged();
        }

        private void updateFlag(CheckBox cbx)
        {
            if (cbx.Checked)
                optionflag |= (1 << cbx.TabIndex);
            else
                optionflag &= ~(1 << cbx.TabIndex);
        }

        private void optionflag_CheckedChanged(object sender, EventArgs e)
        {
            updateFlag((CheckBox)sender);
            setHexValue();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            optionflag = 0x8001;
            setCheckboxChecked();
            setHexValue();
        }

        private void setCheckboxChecked()
        {
            this.checkBox1.Checked = Convert.ToBoolean((optionflag >> 0) & 0x0001);
            this.checkBox2.Checked = Convert.ToBoolean((optionflag >> 1) & 0x0001);
            this.checkBox3.Checked = Convert.ToBoolean((optionflag >> 2) & 0x0001);
            this.checkBox4.Checked = Convert.ToBoolean((optionflag >> 3) & 0x0001);
            this.checkBox5.Checked = Convert.ToBoolean((optionflag >> 4) & 0x0001);
            this.checkBox6.Checked = Convert.ToBoolean((optionflag >> 5) & 0x0001);
            this.checkBox7.Checked = Convert.ToBoolean((optionflag >> 6) & 0x0001);
            this.checkBox8.Checked = Convert.ToBoolean((optionflag >> 7) & 0x0001);
            this.checkBox12.Checked = Convert.ToBoolean((optionflag >> 11) & 0x0001);
            this.checkBox13.Checked = Convert.ToBoolean((optionflag >> 12) & 0x0001);
            this.checkBox14.Checked = Convert.ToBoolean((optionflag >> 13) & 0x0001);
            this.checkBox15.Checked = Convert.ToBoolean((optionflag >> 14) & 0x0001);
            this.checkBox16.Checked = Convert.ToBoolean((optionflag >> 15) & 0x0001);
            this.securityCodeCharacters.Value = securityCodeCount;
        }

        private void securityCodeCharacters_ValueChanged(object sender, EventArgs e)
        {
            securityCodeCount = (int)securityCodeCharacters.Value;
            setHexValue();
        }
    }
}
