﻿namespace ConfigurationWinForm
{
    partial class OptionFlags20Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionFlags20Form));
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lowStockIndicator = new System.Windows.Forms.NumericUpDown();
            this.defaultMask = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lowStockIndicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultMask)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 21);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(242, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "0x0001 - No manual entry of login parameters.";
            this.toolTip2.SetToolTip(this.checkBox1, resources.GetString("checkBox1.ToolTip"));
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(29, 43);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(157, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "0x0002 - Force a REBOOT ";
            this.toolTip2.SetToolTip(this.checkBox2, resources.GetString("checkBox2.ToolTip"));
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(29, 176);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(259, 17);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "0x0080 – Try customer MIFARE Classic keys first.\r\n";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(29, 197);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(276, 17);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – Allow multiple designators for function keys ";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(29, 218);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(373, 17);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Allow changing fareset via option keys, requires previous bit set ";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(29, 240);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(72, 17);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – ";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox12.Location = new System.Drawing.Point(30, 288);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(72, 17);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – ";
            this.checkBox12.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label5);
            this.optionflagsGridTitle.Controls.Add(this.label4);
            this.optionflagsGridTitle.Controls.Add(this.label3);
            this.optionflagsGridTitle.Controls.Add(this.label2);
            this.optionflagsGridTitle.Controls.Add(this.lowStockIndicator);
            this.optionflagsGridTitle.Controls.Add(this.defaultMask);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox8);
            this.optionflagsGridTitle.Controls.Add(this.checkBox1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Controls.Add(this.checkBox2);
            this.optionflagsGridTitle.Controls.Add(this.checkBox10);
            this.optionflagsGridTitle.Controls.Add(this.checkBox11);
            this.optionflagsGridTitle.Controls.Add(this.checkBox12);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(11, 11);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(440, 364);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 337);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(258, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "- Number of tickets to issue before “Low Stock” alarm";
            // 
            // lowStockIndicator
            // 
            this.lowStockIndicator.Location = new System.Drawing.Point(29, 335);
            this.lowStockIndicator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lowStockIndicator.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.lowStockIndicator.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lowStockIndicator.Name = "lowStockIndicator";
            this.lowStockIndicator.Size = new System.Drawing.Size(42, 20);
            this.lowStockIndicator.TabIndex = 19;
            this.toolTip2.SetToolTip(this.lowStockIndicator, "Note:  Using this will override the use of the TRiM sensors. \r\nIf used, then a “S" +
        "tock refilled” code needs to be entered\r\nwhenever stock is added.");
            this.lowStockIndicator.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lowStockIndicator.ValueChanged += new System.EventHandler(this.lowStockIndicator_ValueChanged);
            // 
            // defaultMask
            // 
            this.defaultMask.Location = new System.Drawing.Point(29, 69);
            this.defaultMask.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.defaultMask.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.defaultMask.Name = "defaultMask";
            this.defaultMask.Size = new System.Drawing.Size(38, 20);
            this.defaultMask.TabIndex = 18;
            this.defaultMask.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.defaultMask.ValueChanged += new System.EventHandler(this.defaultMask_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 91);
            this.label1.TabIndex = 17;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(278, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(91, 289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(315, 39);
            this.label3.TabIndex = 21;
            this.label3.Text = "DESFire recharge via other media. This will allow \"money\" from a \r\nnon-DESFire St" +
    "ored Value product to be transferred toa DESFire \r\nStored Value product.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(90, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(329, 39);
            this.label4.TabIndex = 22;
            this.label4.Text = "Take remaining value from smart card. This willl take whatever value\r\nremains on " +
    "the value card even if it is ot enough to satisfy the fare. \r\nNOTE: Allows multi" +
    "ple cards to be use.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(84, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "-";
            // 
            // OptionFlags20Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 384);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "OptionFlags20Form";
            this.Text = "OptionFlag20";
            this.toolTip2.SetToolTip(this, "(Recommended: 0x0000, Default: 0x0000) ");
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lowStockIndicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultMask)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown defaultMask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown lowStockIndicator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;


    }
}