﻿namespace ConfigurationWinForm
{
    partial class WifiGeneralSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.showPasswordCheckbox = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.keyphraseEncryptedBinaryText = new System.Windows.Forms.TextBox();
            this.keyphraseEncryptedHexText = new System.Windows.Forms.TextBox();
            this.ssidEncryptedBinaryText = new System.Windows.Forms.TextBox();
            this.ssidEncryptedHexText = new System.Windows.Forms.TextBox();
            this.FBX_IP = new IPAddressControlLib.IPAddressControl();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.useDHCPCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.KeyPhrase = new System.Windows.Forms.TextBox();
            this.SSID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DS_RF_IP = new IPAddressControlLib.IPAddressControl();
            this.FbxPort = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DSPort = new System.Windows.Forms.TextBox();
            this.Authentication = new System.Windows.Forms.TextBox();
            this.Encryption = new System.Windows.Forms.TextBox();
            this.HostBits = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.topologyCombo = new System.Windows.Forms.ComboBox();
            this.networkModeCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Security = new System.Windows.Forms.ComboBox();
            this.FBXNetwork = new IPAddressControlLib.IPAddressControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Gateway = new IPAddressControlLib.IPAddressControl();
            this.label21 = new System.Windows.Forms.Label();
            this.DNS_IP = new IPAddressControlLib.IPAddressControl();
            this.label22 = new System.Windows.Forms.Label();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "HostBits (Default=16):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Topology:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Security:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 112);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "NetworkMode:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 23);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "DSPort:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(248, 22);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Encryption (default=0):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 54);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Authentication (default=0):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(250, 83);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "FBXNetwork:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(307, 23);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "DS_RF_IP:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.showPasswordCheckbox);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.keyphraseEncryptedBinaryText);
            this.groupBox1.Controls.Add(this.keyphraseEncryptedHexText);
            this.groupBox1.Controls.Add(this.ssidEncryptedBinaryText);
            this.groupBox1.Controls.Add(this.ssidEncryptedHexText);
            this.groupBox1.Controls.Add(this.FBX_IP);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.useDHCPCombo);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.KeyPhrase);
            this.groupBox1.Controls.Add(this.SSID);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.DS_RF_IP);
            this.groupBox1.Controls.Add(this.FbxPort);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DSPort);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(716, 227);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // showPasswordCheckbox
            // 
            this.showPasswordCheckbox.AutoSize = true;
            this.showPasswordCheckbox.Location = new System.Drawing.Point(387, 127);
            this.showPasswordCheckbox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.showPasswordCheckbox.Name = "showPasswordCheckbox";
            this.showPasswordCheckbox.Size = new System.Drawing.Size(102, 17);
            this.showPasswordCheckbox.TabIndex = 37;
            this.showPasswordCheckbox.Text = "Show Password";
            this.showPasswordCheckbox.UseVisualStyleBackColor = true;
            this.showPasswordCheckbox.CheckedChanged += new System.EventHandler(this.showPasswordCheckbox_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 174);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Binary:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(33, 151);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Hex:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 98);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Binary:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(27, 76);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "Hex:";
            // 
            // keyphraseEncryptedBinaryText
            // 
            this.keyphraseEncryptedBinaryText.Location = new System.Drawing.Point(68, 170);
            this.keyphraseEncryptedBinaryText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.keyphraseEncryptedBinaryText.Name = "keyphraseEncryptedBinaryText";
            this.keyphraseEncryptedBinaryText.ReadOnly = true;
            this.keyphraseEncryptedBinaryText.Size = new System.Drawing.Size(486, 20);
            this.keyphraseEncryptedBinaryText.TabIndex = 34;
            // 
            // keyphraseEncryptedHexText
            // 
            this.keyphraseEncryptedHexText.Location = new System.Drawing.Point(68, 149);
            this.keyphraseEncryptedHexText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.keyphraseEncryptedHexText.Name = "keyphraseEncryptedHexText";
            this.keyphraseEncryptedHexText.ReadOnly = true;
            this.keyphraseEncryptedHexText.Size = new System.Drawing.Size(598, 20);
            this.keyphraseEncryptedHexText.TabIndex = 33;
            // 
            // ssidEncryptedBinaryText
            // 
            this.ssidEncryptedBinaryText.Location = new System.Drawing.Point(68, 96);
            this.ssidEncryptedBinaryText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ssidEncryptedBinaryText.Name = "ssidEncryptedBinaryText";
            this.ssidEncryptedBinaryText.ReadOnly = true;
            this.ssidEncryptedBinaryText.Size = new System.Drawing.Size(486, 20);
            this.ssidEncryptedBinaryText.TabIndex = 32;
            // 
            // ssidEncryptedHexText
            // 
            this.ssidEncryptedHexText.Location = new System.Drawing.Point(68, 73);
            this.ssidEncryptedHexText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ssidEncryptedHexText.Name = "ssidEncryptedHexText";
            this.ssidEncryptedHexText.ReadOnly = true;
            this.ssidEncryptedHexText.Size = new System.Drawing.Size(598, 20);
            this.ssidEncryptedHexText.TabIndex = 31;
            // 
            // FBX_IP
            // 
            this.FBX_IP.AllowInternalTab = false;
            this.FBX_IP.AutoHeight = true;
            this.FBX_IP.BackColor = System.Drawing.SystemColors.Window;
            this.FBX_IP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FBX_IP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.FBX_IP.Location = new System.Drawing.Point(560, 18);
            this.FBX_IP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FBX_IP.MinimumSize = new System.Drawing.Size(87, 20);
            this.FBX_IP.Name = "FBX_IP";
            this.FBX_IP.ReadOnly = false;
            this.FBX_IP.Size = new System.Drawing.Size(114, 20);
            this.FBX_IP.TabIndex = 30;
            this.FBX_IP.Text = "...";
            this.FBX_IP.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(497, 20);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "FBX_IP:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 204);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "UseDHCP";
            // 
            // useDHCPCombo
            // 
            this.useDHCPCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useDHCPCombo.FormattingEnabled = true;
            this.useDHCPCombo.Items.AddRange(new object[] {
            "0-Fixed IP (IP Addresses maintained in Data System) ",
            "1-Use DHCP",
            "2-All Fareboxes use same Fixed IP Address defined in FBX_IP"});
            this.useDHCPCombo.Location = new System.Drawing.Point(68, 202);
            this.useDHCPCombo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.useDHCPCombo.Name = "useDHCPCombo";
            this.useDHCPCombo.Size = new System.Drawing.Size(306, 21);
            this.useDHCPCombo.TabIndex = 27;
            this.useDHCPCombo.SelectedIndexChanged += new System.EventHandler(this.UpdateData);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Wifi Common Settings";
            // 
            // KeyPhrase
            // 
            this.KeyPhrase.Location = new System.Drawing.Point(68, 127);
            this.KeyPhrase.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.KeyPhrase.MaxLength = 33;
            this.KeyPhrase.Name = "KeyPhrase";
            this.KeyPhrase.PasswordChar = '*';
            this.KeyPhrase.Size = new System.Drawing.Size(306, 20);
            this.KeyPhrase.TabIndex = 5;
            this.KeyPhrase.TextChanged += new System.EventHandler(this.UpdateKeyPhrase);
            // 
            // SSID
            // 
            this.SSID.Location = new System.Drawing.Point(68, 50);
            this.SSID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SSID.MaxLength = 33;
            this.SSID.Name = "SSID";
            this.SSID.Size = new System.Drawing.Size(306, 20);
            this.SSID.TabIndex = 4;
            this.SSID.TextChanged += new System.EventHandler(this.UpdateSSID);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 129);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "KeyPhrase:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "SSID:";
            // 
            // DS_RF_IP
            // 
            this.DS_RF_IP.AllowInternalTab = false;
            this.DS_RF_IP.AutoHeight = true;
            this.DS_RF_IP.BackColor = System.Drawing.SystemColors.Window;
            this.DS_RF_IP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DS_RF_IP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DS_RF_IP.Location = new System.Drawing.Point(369, 19);
            this.DS_RF_IP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DS_RF_IP.MinimumSize = new System.Drawing.Size(87, 20);
            this.DS_RF_IP.Name = "DS_RF_IP";
            this.DS_RF_IP.ReadOnly = false;
            this.DS_RF_IP.Size = new System.Drawing.Size(114, 20);
            this.DS_RF_IP.TabIndex = 2;
            this.DS_RF_IP.Text = "...";
            this.DS_RF_IP.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // FbxPort
            // 
            this.FbxPort.Location = new System.Drawing.Point(207, 20);
            this.FbxPort.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FbxPort.Name = "FbxPort";
            this.FbxPort.Size = new System.Drawing.Size(76, 20);
            this.FbxPort.TabIndex = 1;
            this.FbxPort.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(158, 23);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "FbxPort:";
            // 
            // DSPort
            // 
            this.DSPort.Location = new System.Drawing.Point(68, 20);
            this.DSPort.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DSPort.Name = "DSPort";
            this.DSPort.Size = new System.Drawing.Size(76, 20);
            this.DSPort.TabIndex = 0;
            this.DSPort.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // Authentication
            // 
            this.Authentication.Location = new System.Drawing.Point(404, 51);
            this.Authentication.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Authentication.Name = "Authentication";
            this.Authentication.Size = new System.Drawing.Size(38, 20);
            this.Authentication.TabIndex = 5;
            this.Authentication.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // Encryption
            // 
            this.Encryption.Location = new System.Drawing.Point(406, 24);
            this.Encryption.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Encryption.Name = "Encryption";
            this.Encryption.Size = new System.Drawing.Size(36, 20);
            this.Encryption.TabIndex = 4;
            this.Encryption.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // HostBits
            // 
            this.HostBits.Location = new System.Drawing.Point(156, 18);
            this.HostBits.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.HostBits.Name = "HostBits";
            this.HostBits.Size = new System.Drawing.Size(36, 20);
            this.HostBits.TabIndex = 0;
            this.HostBits.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.topologyCombo);
            this.groupBox2.Controls.Add(this.networkModeCombo);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.Security);
            this.groupBox2.Controls.Add(this.FBXNetwork);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.Authentication);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.Encryption);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.HostBits);
            this.groupBox2.Location = new System.Drawing.Point(9, 241);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(460, 142);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // topologyCombo
            // 
            this.topologyCombo.AutoCompleteCustomSource.AddRange(new string[] {
            "0-Infrastructure (default)",
            "1-Add Hoc",
            "2-Bridging"});
            this.topologyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.topologyCombo.FormattingEnabled = true;
            this.topologyCombo.Items.AddRange(new object[] {
            "0=Infrastructure (Default)",
            "1=Ad Hoc ",
            "2=Bridging"});
            this.topologyCombo.Location = new System.Drawing.Point(86, 51);
            this.topologyCombo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.topologyCombo.Name = "topologyCombo";
            this.topologyCombo.Size = new System.Drawing.Size(144, 21);
            this.topologyCombo.TabIndex = 10;
            this.topologyCombo.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // networkModeCombo
            // 
            this.networkModeCombo.AutoCompleteCustomSource.AddRange(new string[] {
            "0-Wired",
            "1-Wireless Only",
            "2-Bridged"});
            this.networkModeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.networkModeCombo.FormattingEnabled = true;
            this.networkModeCombo.Items.AddRange(new object[] {
            "0=Wired",
            "1=Wireless Only",
            "2=Bridged"});
            this.networkModeCombo.Location = new System.Drawing.Point(86, 108);
            this.networkModeCombo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.networkModeCombo.Name = "networkModeCombo";
            this.networkModeCombo.Size = new System.Drawing.Size(144, 21);
            this.networkModeCombo.TabIndex = 9;
            this.networkModeCombo.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Odyssey Wifi Settings";
            // 
            // Security
            // 
            this.Security.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Security.FormattingEnabled = true;
            this.Security.Items.AddRange(new object[] {
            "0=None",
            "1=WEP",
            "2=WPA",
            "3=WPA2"});
            this.Security.Location = new System.Drawing.Point(86, 80);
            this.Security.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Security.Name = "Security";
            this.Security.Size = new System.Drawing.Size(144, 21);
            this.Security.TabIndex = 2;
            this.Security.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // FBXNetwork
            // 
            this.FBXNetwork.AllowInternalTab = false;
            this.FBXNetwork.AutoHeight = true;
            this.FBXNetwork.BackColor = System.Drawing.SystemColors.Window;
            this.FBXNetwork.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FBXNetwork.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.FBXNetwork.Location = new System.Drawing.Point(327, 80);
            this.FBXNetwork.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FBXNetwork.MinimumSize = new System.Drawing.Size(87, 20);
            this.FBXNetwork.Name = "FBXNetwork";
            this.FBXNetwork.ReadOnly = false;
            this.FBXNetwork.Size = new System.Drawing.Size(114, 20);
            this.FBXNetwork.TabIndex = 6;
            this.FBXNetwork.Text = "...";
            this.FBXNetwork.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Gateway);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.DNS_IP);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Location = new System.Drawing.Point(486, 253);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(278, 130);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Odyssey/Odyssey+ Settings";
            // 
            // Gateway
            // 
            this.Gateway.AllowInternalTab = false;
            this.Gateway.AutoHeight = true;
            this.Gateway.BackColor = System.Drawing.SystemColors.Window;
            this.Gateway.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Gateway.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Gateway.Location = new System.Drawing.Point(74, 66);
            this.Gateway.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Gateway.MinimumSize = new System.Drawing.Size(87, 20);
            this.Gateway.Name = "Gateway";
            this.Gateway.ReadOnly = false;
            this.Gateway.Size = new System.Drawing.Size(114, 20);
            this.Gateway.TabIndex = 34;
            this.Gateway.Text = "...";
            this.toolTip2.SetToolTip(this.Gateway, "IP of Gateway if DHCP is not used (ODY/ODY+ only)");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 68);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Gateway:";
            // 
            // DNS_IP
            // 
            this.DNS_IP.AllowInternalTab = false;
            this.DNS_IP.AutoHeight = true;
            this.DNS_IP.BackColor = System.Drawing.SystemColors.Window;
            this.DNS_IP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DNS_IP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DNS_IP.Location = new System.Drawing.Point(74, 27);
            this.DNS_IP.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.DNS_IP.MinimumSize = new System.Drawing.Size(87, 20);
            this.DNS_IP.Name = "DNS_IP";
            this.DNS_IP.ReadOnly = false;
            this.DNS_IP.Size = new System.Drawing.Size(114, 20);
            this.DNS_IP.TabIndex = 31;
            this.DNS_IP.Text = "...";
            this.toolTip2.SetToolTip(this.DNS_IP, "IP of DNS Server if DHCP is not use (ODY/ODY+ only)");
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 31);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "DNS_IP:";
            // 
            // WifiGeneralSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 402);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "WifiGeneralSettingsForm";
            this.Text = "Wifi General Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Authentication;
        private System.Windows.Forms.TextBox Encryption;
        private System.Windows.Forms.TextBox DSPort;
        private System.Windows.Forms.TextBox HostBits;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox FbxPort;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox KeyPhrase;
        private System.Windows.Forms.TextBox SSID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private IPAddressControlLib.IPAddressControl DS_RF_IP;
        private IPAddressControlLib.IPAddressControl FBXNetwork;
        private System.Windows.Forms.ComboBox Security;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox useDHCPCombo;
        private System.Windows.Forms.Label label15;
        private IPAddressControlLib.IPAddressControl FBX_IP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox networkModeCombo;
        private System.Windows.Forms.ComboBox topologyCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox keyphraseEncryptedBinaryText;
        private System.Windows.Forms.TextBox keyphraseEncryptedHexText;
        private System.Windows.Forms.TextBox ssidEncryptedBinaryText;
        private System.Windows.Forms.TextBox ssidEncryptedHexText;
        private System.Windows.Forms.CheckBox showPasswordCheckbox;
        private System.Windows.Forms.GroupBox groupBox3;
        private IPAddressControlLib.IPAddressControl Gateway;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Label label21;
        private IPAddressControlLib.IPAddressControl DNS_IP;
        private System.Windows.Forms.Label label22;
        private System.ServiceProcess.ServiceController serviceController1;

    }
}