﻿namespace ConfigurationWinForm
{
    partial class WaitDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.waitMsg = new System.Windows.Forms.Label();
            this.cancelQuery = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // waitMsg
            // 
            this.waitMsg.AutoSize = true;
            this.waitMsg.Location = new System.Drawing.Point(25, 9);
            this.waitMsg.Name = "waitMsg";
            this.waitMsg.Size = new System.Drawing.Size(76, 13);
            this.waitMsg.TabIndex = 0;
            this.waitMsg.Text = "Please Wait ...";
            // 
            // cancelQuery
            // 
            this.cancelQuery.Location = new System.Drawing.Point(430, 9);
            this.cancelQuery.Name = "cancelQuery";
            this.cancelQuery.Size = new System.Drawing.Size(60, 28);
            this.cancelQuery.TabIndex = 1;
            this.cancelQuery.Text = "Cancel";
            this.cancelQuery.UseVisualStyleBackColor = true;
            this.cancelQuery.Visible = false;
            this.cancelQuery.Click += new System.EventHandler(this.cancelQuery_Click);
            // 
            // WaitDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(502, 40);
            this.ControlBox = false;
            this.Controls.Add(this.cancelQuery);
            this.Controls.Add(this.waitMsg);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "WaitDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "WaitDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label waitMsg;
        private System.Windows.Forms.Button cancelQuery;
    }
}