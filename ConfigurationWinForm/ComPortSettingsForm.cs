﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class ComPortSettingsForm : Form
    {

        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public int muxid = 0;
        public uint hexValue;
        bool formLoaded = false;
        public ComPortSettingsForm()
        {
            InitializeComponent();
        }

        public ComPortSettingsForm(SplitForm sf, string section)
        {
            switch (section.ToLower())
            {
                case "mux2":
                    muxid = 1;
                    break;
                case "mux3":
                    muxid = 2;
                    break;
                case "mux1":
                default:
                    muxid = 0;
                    break;
            }
            InitializeComponent();
            splitForm = sf;
            splitForm.GetComPortList(Port);
            LoadData();

            // Disable all controls that do not apply to Mux2-n
            if (muxid > 0)
            {
                DirectionType.Hide();
                dumpSQL.Hide();
                DumpTransactions.Hide();
                debugLevel.Hide();

                dumpSQLNote.Hide();
                debugLevelLabel.Hide();
                DumpTransactionsLabel.Hide();
                DirectionTypeLabel.Hide();

            }
            formLoaded = true;
        }

        public void LoadData()
        {
            hexValue = Util.ParseValue(splitForm.iniConfig.inifileData.mux[muxid].PortEnbMask);
            channel1cbx.Checked = false;
            channel2cbx.Checked = false;
            channel3cbx.Checked = false;
            channel4cbx.Checked = false;
            dumpSQL.Checked     = false;

            if ((hexValue & 0x01) > 0)
                channel1cbx.Checked = true;
            if ((hexValue & 0x02) > 0)
                channel2cbx.Checked = true;
            if ((hexValue & 0x04) > 0)
                channel3cbx.Checked = true;
            if ((hexValue & 0x08) > 0)
                channel4cbx.Checked = true;

            Port.Text = splitForm.iniConfig.inifileData.mux[muxid].Port;
            OldBaudrate.Text = splitForm.iniConfig.inifileData.mux[muxid].OldBaudrate;
            BaudRate.Text = splitForm.iniConfig.inifileData.mux[muxid].Baudrate;
            FbxBaudRate.Text =splitForm.iniConfig.inifileData.mux[muxid].FbxBaudrate;

            scanBaudRatecbx.Checked = false;
            IsoBox.Checked = false;
            ProbeType.Checked = false;
            DumpTransactions.Checked = false;

            if (Util.ParseValue(splitForm.iniConfig.inifileData.mux[muxid].ScanBaudRate)> 0)
                scanBaudRatecbx.Checked = true;
            if (Util.ParseValue(splitForm.iniConfig.inifileData.mux[muxid].MuxIsoBox) > 0)
                IsoBox.Checked = true;
            if (Util.ParseValue(splitForm.iniConfig.inifileData.mux[muxid].ProbeType) > 0)
                ProbeType.Checked = true;
            if (Util.ParseValue(splitForm.iniConfig.inifileData.DumpTransactions) > 0)
                DumpTransactions.Checked = true;
            debugLevel.Value = Util.ParseValue(splitForm.iniConfig.inifileData.DebugLevel);
            if (Util.ParseValue(splitForm.iniConfig.inifileData.DirType) != 0)
                DirectionType.Text = "1 = North/South/East/West";
            else
                DirectionType.Text = "0 = Inbound/Outbound";

            WaitSync.Value = Util.ParseValue(splitForm.iniConfig.inifileData.mux[muxid].WaitSync);
            FbxVersion.Value = Util.ParseValue(splitForm.iniConfig.inifileData.FbxVersion);
            if (Util.ParseValue(splitForm.iniConfig.inifileData.DumpSQL) > 0)
                dumpSQL.Checked = true;
        }

        public void SaveData()
        {
            hexValue = 0;
            if (channel1cbx.Checked)
                hexValue |= 1;
            if (channel2cbx.Checked)
                hexValue |= 1 << 1;
            if (channel3cbx.Checked)
                hexValue |= 1 << 2;
            if (channel4cbx.Checked)
                hexValue |= 1 << 3;
            
            splitForm.iniConfig.inifileData.mux[muxid].Port = this.Port.Text;
            splitForm.iniConfig.inifileData.mux[muxid].PortEnbMask = "0x" + hexValue.ToString("X2");
            splitForm.iniConfig.inifileData.mux[muxid].OldBaudrate = this.OldBaudrate.Text;
            splitForm.iniConfig.inifileData.mux[muxid].Baudrate = this.BaudRate.Text;
            splitForm.iniConfig.inifileData.mux[muxid].FbxBaudrate = this.FbxBaudRate.Text;
            splitForm.iniConfig.inifileData.mux[muxid].ScanBaudRate = (this.scanBaudRatecbx.Checked ? 1 : 0).ToString();
            splitForm.iniConfig.inifileData.mux[muxid].MuxIsoBox = (this.IsoBox.Checked ? 1 : 0).ToString();
            splitForm.iniConfig.inifileData.mux[muxid].ProbeType = (this.ProbeType.Checked ? 1 : 0).ToString();

            splitForm.iniConfig.inifileData.DebugLevel = this.debugLevel.Value.ToString();
            splitForm.iniConfig.inifileData.DumpTransactions = (this.DumpTransactions.Checked ? 1 : 0).ToString();
            if (this.DirectionType.SelectedIndex == 0)
                splitForm.iniConfig.inifileData.DirType = "0";
            else
                splitForm.iniConfig.inifileData.DirType = "1";
            splitForm.iniConfig.inifileData.mux[muxid].WaitSync = this.WaitSync.Value.ToString();
            splitForm.iniConfig.inifileData.FbxVersion = this.FbxVersion.Value.ToString();
            splitForm.iniConfig.inifileData.DumpSQL = (dumpSQL.Checked ? 1 : 0).ToString();
        }


        public void UpdateData(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                splitForm.SetChanged();
                SaveData();
            }
        }
    }
}
