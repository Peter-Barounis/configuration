﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class UserLoginForm : Form
    {
        public string usr;
        public string pwd;
        public DialogResult OkCancel;

        public UserLoginForm()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            usr = this.username.Text;
            pwd = this.password.Text;
            OkCancel = DialogResult.OK;
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            usr = this.username.Text;
            pwd = this.password.Text;
            OkCancel = DialogResult.Cancel;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pie", "Login Hint");
        }
    }
}
