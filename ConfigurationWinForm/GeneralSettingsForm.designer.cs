﻿namespace ConfigurationWinForm
{
    partial class GeneralSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralSettingsForm));
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.AgencyCode = new System.Windows.Forms.TextBox();
            this.Passback = new System.Windows.Forms.TextBox();
            this.Dump = new System.Windows.Forms.TextBox();
            this.MaxEvents = new System.Windows.Forms.ComboBox();
            this.LEDTime = new System.Windows.Forms.TextBox();
            this.EventPeriod = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.DST = new System.Windows.Forms.TextBox();
            this.STD = new System.Windows.Forms.TextBox();
            this.MagSecurityCodes = new System.Windows.Forms.TextBox();
            this.SCardSecurityCodes = new System.Windows.Forms.TextBox();
            this.OldAgencyCode = new System.Windows.Forms.TextBox();
            this.MemNearFull = new System.Windows.Forms.TextBox();
            this.BillFull = new System.Windows.Forms.TextBox();
            this.CoinFull = new System.Windows.Forms.TextBox();
            this.BillTimeOut = new System.Windows.Forms.TextBox();
            this.ProbeWatchDog = new System.Windows.Forms.TextBox();
            this.StopPointID = new System.Windows.Forms.TextBox();
            this.MCP = new System.Windows.Forms.TextBox();
            this.ChangeCardThreshold = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.MixFleetMode = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ftoTime = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.gmtOffsetText = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.calcSummaryCount = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PassportTTPUpDown = new System.Windows.Forms.NumericUpDown();
            this.AcctMinRechargeUpDown = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.operatorIDText = new System.Windows.Forms.TextBox();
            this.lockFileBtn = new System.Windows.Forms.Button();
            this.lockFileText = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.useLockCodecbx = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PassportTTPUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcctMinRechargeUpDown)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 233);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "DST/STD:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 279);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.MaximumSize = new System.Drawing.Size(262, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "MagSecurityCodes:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 19);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.MaximumSize = new System.Drawing.Size(262, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "AgencyCode:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(296, 162);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.MaximumSize = new System.Drawing.Size(338, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "BillTimeout:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(296, 279);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "StopPointID:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 335);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.MaximumSize = new System.Drawing.Size(225, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "MCP:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(474, 279);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "(\"Hoime Garage\" ID)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(296, 305);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "MixedFleetMode:";
            // 
            // AgencyCode
            // 
            this.AgencyCode.Location = new System.Drawing.Point(123, 19);
            this.AgencyCode.Margin = new System.Windows.Forms.Padding(2);
            this.AgencyCode.Name = "AgencyCode";
            this.AgencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AgencyCode.Size = new System.Drawing.Size(66, 20);
            this.AgencyCode.TabIndex = 0;
            this.AgencyCode.Tag = "";
            this.toolTip2.SetToolTip(this.AgencyCode, "Agency Code: (This value is customer specific, Default: 4095)\r\nAgency ID\\nencoded" +
        " on media. Values are from 0001-4095 \r\n(a value of 4095 = disabled).\\nFor old sw" +
        "ipe format, use 100 to disable).");
            this.AgencyCode.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // Passback
            // 
            this.Passback.Location = new System.Drawing.Point(123, 50);
            this.Passback.Margin = new System.Windows.Forms.Padding(2);
            this.Passback.Name = "Passback";
            this.Passback.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Passback.Size = new System.Drawing.Size(66, 20);
            this.Passback.TabIndex = 2;
            this.Passback.Tag = "Passback Timer. Time between when same document may be used on the same bus.<r>Va" +
    "lue is in minutes, and can range from 1 minute to minutes (4 hours 15 minutes):";
            this.Passback.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.Passback.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // Dump
            // 
            this.Dump.Location = new System.Drawing.Point(123, 86);
            this.Dump.Margin = new System.Windows.Forms.Padding(2);
            this.Dump.Name = "Dump";
            this.Dump.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Dump.Size = new System.Drawing.Size(66, 20);
            this.Dump.TabIndex = 4;
            this.Dump.Tag = "Auto dump time. This is how long the current fare will remain pending Value<r>is " +
    "in seconds, and can range from 10 seconds to 255 seconds (4 minutes):";
            this.Dump.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.Dump.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // MaxEvents
            // 
            this.MaxEvents.FormattingEnabled = true;
            this.MaxEvents.Items.AddRange(new object[] {
            "100",
            "250",
            "500"});
            this.MaxEvents.Location = new System.Drawing.Point(123, 120);
            this.MaxEvents.Margin = new System.Windows.Forms.Padding(2);
            this.MaxEvents.Name = "MaxEvents";
            this.MaxEvents.Size = new System.Drawing.Size(66, 21);
            this.MaxEvents.TabIndex = 6;
            this.MaxEvents.Text = "500";
            this.toolTip2.SetToolTip(this.MaxEvents, "Agency Code: (This value is customer specific, Default: 4095)Agency ID<r>encoded " +
        "on media. Values are from 0001-4095 (a value of 4095 = disabled).<r>For old swip" +
        "e format, use 100 to disable)");
            this.MaxEvents.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // LEDTime
            // 
            this.LEDTime.Location = new System.Drawing.Point(123, 158);
            this.LEDTime.Margin = new System.Windows.Forms.Padding(2);
            this.LEDTime.Name = "LEDTime";
            this.LEDTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LEDTime.Size = new System.Drawing.Size(66, 20);
            this.LEDTime.TabIndex = 8;
            this.LEDTime.Tag = "LEDTime: (Recommended: 10) Amount of time in which OCU/Patron display messages an" +
    "d lights remain on.<r>Value is in seconds, and can range from 1 to 255 (4 minute" +
    "s 15 seconds):";
            this.LEDTime.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.LEDTime.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // EventPeriod
            // 
            this.EventPeriod.Location = new System.Drawing.Point(123, 197);
            this.EventPeriod.Margin = new System.Windows.Forms.Padding(2);
            this.EventPeriod.Name = "EventPeriod";
            this.EventPeriod.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.EventPeriod.Size = new System.Drawing.Size(35, 20);
            this.EventPeriod.TabIndex = 10;
            this.EventPeriod.Tag = resources.GetString("EventPeriod.Tag");
            this.EventPeriod.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.EventPeriod.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(161, 200);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "x 5 Min(s)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(82, 233);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Start:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(82, 251);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 13);
            this.label22.TabIndex = 27;
            this.label22.Text = "End:";
            // 
            // DST
            // 
            this.DST.Location = new System.Drawing.Point(123, 229);
            this.DST.Margin = new System.Windows.Forms.Padding(2);
            this.DST.Name = "DST";
            this.DST.Size = new System.Drawing.Size(50, 20);
            this.DST.TabIndex = 12;
            this.toolTip2.SetToolTip(this.DST, "Daylight Savings Time Start");
            this.DST.TextAlignChanged += new System.EventHandler(this.UpdateData);
            // 
            // STD
            // 
            this.STD.Location = new System.Drawing.Point(123, 252);
            this.STD.Margin = new System.Windows.Forms.Padding(2);
            this.STD.Name = "STD";
            this.STD.Size = new System.Drawing.Size(50, 20);
            this.STD.TabIndex = 13;
            this.toolTip2.SetToolTip(this.STD, "Standard Time (ENd of DST)");
            this.STD.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // MagSecurityCodes
            // 
            this.MagSecurityCodes.Location = new System.Drawing.Point(123, 279);
            this.MagSecurityCodes.Margin = new System.Windows.Forms.Padding(2);
            this.MagSecurityCodes.Name = "MagSecurityCodes";
            this.MagSecurityCodes.Size = new System.Drawing.Size(66, 20);
            this.MagSecurityCodes.TabIndex = 15;
            this.MagSecurityCodes.Tag = "Magnetic Security Codes: Current and Future security codes for magnetic documents" +
    ". 0xCCCCFFFF";
            this.MagSecurityCodes.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.MagSecurityCodes.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // SCardSecurityCodes
            // 
            this.SCardSecurityCodes.Location = new System.Drawing.Point(123, 306);
            this.SCardSecurityCodes.Margin = new System.Windows.Forms.Padding(2);
            this.SCardSecurityCodes.Name = "SCardSecurityCodes";
            this.SCardSecurityCodes.Size = new System.Drawing.Size(66, 20);
            this.SCardSecurityCodes.TabIndex = 17;
            this.SCardSecurityCodes.Tag = "Smart Card Security Codes. Current and Future security codes for smart card docum" +
    "ents. 0xCCCCFFFF";
            this.SCardSecurityCodes.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.SCardSecurityCodes.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // OldAgencyCode
            // 
            this.OldAgencyCode.Location = new System.Drawing.Point(427, 15);
            this.OldAgencyCode.Margin = new System.Windows.Forms.Padding(2);
            this.OldAgencyCode.Name = "OldAgencyCode";
            this.OldAgencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OldAgencyCode.Size = new System.Drawing.Size(76, 20);
            this.OldAgencyCode.TabIndex = 1;
            this.OldAgencyCode.Tag = "OldAgencyCode: (Recommended: 100, Default: 100) ID used for<r>old “swipe” formatt" +
    "ed passes. Values are from 0  100 (a value of 100 = disabled).";
            this.OldAgencyCode.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.OldAgencyCode.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // MemNearFull
            // 
            this.MemNearFull.Location = new System.Drawing.Point(427, 50);
            this.MemNearFull.Margin = new System.Windows.Forms.Padding(2);
            this.MemNearFull.Name = "MemNearFull";
            this.MemNearFull.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MemNearFull.Size = new System.Drawing.Size(76, 20);
            this.MemNearFull.TabIndex = 3;
            this.MemNearFull.Tag = resources.GetString("MemNearFull.Tag");
            this.MemNearFull.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.MemNearFull.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // BillFull
            // 
            this.BillFull.Location = new System.Drawing.Point(427, 86);
            this.BillFull.Margin = new System.Windows.Forms.Padding(2);
            this.BillFull.Name = "BillFull";
            this.BillFull.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BillFull.Size = new System.Drawing.Size(76, 20);
            this.BillFull.TabIndex = 5;
            this.BillFull.Tag = resources.GetString("BillFull.Tag");
            this.BillFull.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.BillFull.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // CoinFull
            // 
            this.CoinFull.Location = new System.Drawing.Point(427, 120);
            this.CoinFull.Margin = new System.Windows.Forms.Padding(2);
            this.CoinFull.Name = "CoinFull";
            this.CoinFull.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CoinFull.Size = new System.Drawing.Size(76, 20);
            this.CoinFull.TabIndex = 7;
            this.CoinFull.Tag = resources.GetString("CoinFull.Tag");
            this.CoinFull.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.CoinFull.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // BillTimeOut
            // 
            this.BillTimeOut.Location = new System.Drawing.Point(427, 158);
            this.BillTimeOut.Margin = new System.Windows.Forms.Padding(2);
            this.BillTimeOut.Name = "BillTimeOut";
            this.BillTimeOut.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BillTimeOut.Size = new System.Drawing.Size(76, 20);
            this.BillTimeOut.TabIndex = 9;
            this.BillTimeOut.Tag = "Bill escrow timeout. This is amount of time bill stays in viewing area. (CENTSaBI" +
    "LL)<r> Value is in seconds, and can range from 1 - 255 (4 minutes 15 seconds).";
            this.BillTimeOut.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.BillTimeOut.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // ProbeWatchDog
            // 
            this.ProbeWatchDog.Location = new System.Drawing.Point(427, 193);
            this.ProbeWatchDog.Margin = new System.Windows.Forms.Padding(2);
            this.ProbeWatchDog.Name = "ProbeWatchDog";
            this.ProbeWatchDog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ProbeWatchDog.Size = new System.Drawing.Size(76, 20);
            this.ProbeWatchDog.TabIndex = 11;
            this.ProbeWatchDog.Tag = resources.GetString("ProbeWatchDog.Tag");
            this.ProbeWatchDog.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.ProbeWatchDog.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // StopPointID
            // 
            this.StopPointID.Location = new System.Drawing.Point(427, 276);
            this.StopPointID.Margin = new System.Windows.Forms.Padding(2);
            this.StopPointID.Name = "StopPointID";
            this.StopPointID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StopPointID.Size = new System.Drawing.Size(42, 20);
            this.StopPointID.TabIndex = 16;
            this.StopPointID.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.StopPointID.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // MCP
            // 
            this.MCP.Location = new System.Drawing.Point(123, 335);
            this.MCP.Margin = new System.Windows.Forms.Padding(2);
            this.MCP.Name = "MCP";
            this.MCP.Size = new System.Drawing.Size(126, 20);
            this.MCP.TabIndex = 19;
            this.MCP.Tag = "Memory Clear Password. This is the code required in order to<r>Memory Clear the f" +
    "arebox. Requires external program to edit:";
            this.MCP.TextChanged += new System.EventHandler(this.UpdateData);
            this.MCP.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // ChangeCardThreshold
            // 
            this.ChangeCardThreshold.Location = new System.Drawing.Point(427, 231);
            this.ChangeCardThreshold.Margin = new System.Windows.Forms.Padding(2);
            this.ChangeCardThreshold.Name = "ChangeCardThreshold";
            this.ChangeCardThreshold.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ChangeCardThreshold.Size = new System.Drawing.Size(54, 20);
            this.ChangeCardThreshold.TabIndex = 14;
            this.ChangeCardThreshold.Tag = resources.GetString("ChangeCardThreshold.Tag");
            this.ChangeCardThreshold.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            this.ChangeCardThreshold.MouseHover += new System.EventHandler(this.loadToolTip_MouseHover);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(486, 233);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 43;
            this.label25.Text = "Cents";
            // 
            // MixFleetMode
            // 
            this.MixFleetMode.DisplayMember = "Motorola";
            this.MixFleetMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MixFleetMode.FormattingEnabled = true;
            this.MixFleetMode.IntegralHeight = false;
            this.MixFleetMode.Items.AddRange(new object[] {
            "0-Motorola",
            "1-Intel",
            "2-Both"});
            this.MixFleetMode.Location = new System.Drawing.Point(427, 305);
            this.MixFleetMode.Margin = new System.Windows.Forms.Padding(2);
            this.MixFleetMode.Name = "MixFleetMode";
            this.MixFleetMode.Size = new System.Drawing.Size(114, 21);
            this.MixFleetMode.TabIndex = 18;
            this.MixFleetMode.TextChanged += new System.EventHandler(this.UpdateData);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ftoTime);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.gmtOffsetText);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.calcSummaryCount);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.MixFleetMode);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ChangeCardThreshold);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.MCP);
            this.groupBox1.Controls.Add(this.StopPointID);
            this.groupBox1.Controls.Add(this.ProbeWatchDog);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.BillTimeOut);
            this.groupBox1.Controls.Add(this.CoinFull);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.BillFull);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.MemNearFull);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.OldAgencyCode);
            this.groupBox1.Controls.Add(this.SCardSecurityCodes);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.MagSecurityCodes);
            this.groupBox1.Controls.Add(this.AgencyCode);
            this.groupBox1.Controls.Add(this.Passback);
            this.groupBox1.Controls.Add(this.Dump);
            this.groupBox1.Controls.Add(this.STD);
            this.groupBox1.Controls.Add(this.MaxEvents);
            this.groupBox1.Controls.Add(this.DST);
            this.groupBox1.Controls.Add(this.LEDTime);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.EventPeriod);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Location = new System.Drawing.Point(9, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(597, 399);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // ftoTime
            // 
            this.ftoTime.Location = new System.Drawing.Point(427, 364);
            this.ftoTime.Margin = new System.Windows.Forms.Padding(2);
            this.ftoTime.Name = "ftoTime";
            this.ftoTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ftoTime.Size = new System.Drawing.Size(66, 20);
            this.ftoTime.TabIndex = 74;
            this.ftoTime.Tag = "LEDTime: (Recommended: 10) Amount of time in which OCU/Patron display messages an" +
    "d lights remain on.<r>Value is in seconds, and can range from 1 to 255 (4 minute" +
    "s 15 seconds):";
            this.toolTip2.SetToolTip(this.ftoTime, "Frame Timeout Time: Number of ms to wiait before \r\nHDLC Timeout (should be at lea" +
        "st 2000).");
            this.ftoTime.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(296, 368);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 13);
            this.label28.TabIndex = 73;
            this.label28.Text = "FTOTime:";
            // 
            // gmtOffsetText
            // 
            this.gmtOffsetText.Location = new System.Drawing.Point(123, 366);
            this.gmtOffsetText.Margin = new System.Windows.Forms.Padding(2);
            this.gmtOffsetText.Name = "gmtOffsetText";
            this.gmtOffsetText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gmtOffsetText.Size = new System.Drawing.Size(50, 20);
            this.gmtOffsetText.TabIndex = 72;
            this.gmtOffsetText.Tag = "";
            this.toolTip2.SetToolTip(this.gmtOffsetText, "Applies to: Fast Fare and Fast Fare -e only.\r\nTime offset to convert from GMT to " +
        "local standard time.\r\nEast Coast=-5.0, Central=-6.0, Mountain=-7.0, Pacific=-8.0" +
        " ");
            this.gmtOffsetText.TextChanged += new System.EventHandler(this.float_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 368);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 13);
            this.label27.TabIndex = 71;
            this.label27.Text = "GMTOffset:";
            // 
            // calcSummaryCount
            // 
            this.calcSummaryCount.Location = new System.Drawing.Point(427, 332);
            this.calcSummaryCount.Margin = new System.Windows.Forms.Padding(2);
            this.calcSummaryCount.Name = "calcSummaryCount";
            this.calcSummaryCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.calcSummaryCount.Size = new System.Drawing.Size(76, 20);
            this.calcSummaryCount.TabIndex = 70;
            this.calcSummaryCount.Tag = "";
            this.toolTip2.SetToolTip(this.calcSummaryCount, resources.GetString("calcSummaryCount.ToolTip"));
            this.calcSummaryCount.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(296, 335);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(126, 13);
            this.label24.TabIndex = 69;
            this.label24.Text = "CalcSummaryCount (0-n):";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, -2);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(126, 13);
            this.label18.TabIndex = 68;
            this.label18.Text = "Farebox General Settings";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(296, 233);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.MaximumSize = new System.Drawing.Size(338, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 13);
            this.label14.TabIndex = 67;
            this.label14.Text = "ChangeCardThreshold:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(296, 197);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.MaximumSize = new System.Drawing.Size(338, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "ProbeWatchDog:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(296, 126);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.MaximumSize = new System.Drawing.Size(338, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 65;
            this.label23.Text = "CoinFull:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(296, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.MaximumSize = new System.Drawing.Size(338, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "OldAgencyCode:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 305);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.MaximumSize = new System.Drawing.Size(262, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "SCardSecurityCodes:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(296, 90);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.MaximumSize = new System.Drawing.Size(338, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "BillFull:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(296, 54);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.MaximumSize = new System.Drawing.Size(338, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 13);
            this.label26.TabIndex = 61;
            this.label26.Text = "MemNearFull:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 197);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.MaximumSize = new System.Drawing.Size(262, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "EvTime:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 162);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.MaximumSize = new System.Drawing.Size(262, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "LEDTime:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 126);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.MaximumSize = new System.Drawing.Size(262, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "MaxEvents:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.MaximumSize = new System.Drawing.Size(262, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Dump:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.MaximumSize = new System.Drawing.Size(262, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 56;
            this.label2.Text = "Passback:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PassportTTPUpDown);
            this.groupBox2.Controls.Add(this.AcctMinRechargeUpDown);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Location = new System.Drawing.Point(9, 424);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 97);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // PassportTTPUpDown
            // 
            this.PassportTTPUpDown.Location = new System.Drawing.Point(221, 46);
            this.PassportTTPUpDown.Maximum = new decimal(new int[] {
            241,
            0,
            0,
            0});
            this.PassportTTPUpDown.Name = "PassportTTPUpDown";
            this.PassportTTPUpDown.Size = new System.Drawing.Size(63, 20);
            this.PassportTTPUpDown.TabIndex = 81;
            this.PassportTTPUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PassportTTPUpDown.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // AcctMinRechargeUpDown
            // 
            this.AcctMinRechargeUpDown.Location = new System.Drawing.Point(221, 19);
            this.AcctMinRechargeUpDown.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.AcctMinRechargeUpDown.Name = "AcctMinRechargeUpDown";
            this.AcctMinRechargeUpDown.Size = new System.Drawing.Size(63, 20);
            this.AcctMinRechargeUpDown.TabIndex = 80;
            this.AcctMinRechargeUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AcctMinRechargeUpDown.ValueChanged += new System.EventHandler(this.UpdateData);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(10, 50);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(186, 13);
            this.label31.TabIndex = 77;
            this.label31.Text = "PassportTTP (0=disabled or 1 to 240 :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(9, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(207, 13);
            this.label30.TabIndex = 76;
            this.label30.Text = "AcctMinRecharge(0=disabled or 5 to 255 :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 0);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(121, 13);
            this.label29.TabIndex = 75;
            this.label29.Text = "Account Based Settings";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.operatorIDText);
            this.groupBox3.Controls.Add(this.lockFileBtn);
            this.groupBox3.Controls.Add(this.lockFileText);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.useLockCodecbx);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Location = new System.Drawing.Point(314, 424);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(428, 97);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // operatorIDText
            // 
            this.operatorIDText.Location = new System.Drawing.Point(72, 69);
            this.operatorIDText.Name = "operatorIDText";
            this.operatorIDText.Size = new System.Drawing.Size(100, 20);
            this.operatorIDText.TabIndex = 88;
            this.operatorIDText.TextChanged += new System.EventHandler(this.numeric_TextChanged);
            // 
            // lockFileBtn
            // 
            this.lockFileBtn.Location = new System.Drawing.Point(336, 43);
            this.lockFileBtn.Name = "lockFileBtn";
            this.lockFileBtn.Size = new System.Drawing.Size(78, 23);
            this.lockFileBtn.TabIndex = 87;
            this.lockFileBtn.Text = "Browse ...";
            this.lockFileBtn.UseVisualStyleBackColor = true;
            this.lockFileBtn.Click += new System.EventHandler(this.lockFileBtn_Click);
            // 
            // lockFileText
            // 
            this.lockFileText.Location = new System.Drawing.Point(70, 44);
            this.lockFileText.Name = "lockFileText";
            this.lockFileText.Size = new System.Drawing.Size(251, 20);
            this.lockFileText.TabIndex = 86;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(8, 72);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 13);
            this.label34.TabIndex = 85;
            this.label34.Text = "OperatorID:";
            // 
            // useLockCodecbx
            // 
            this.useLockCodecbx.AutoSize = true;
            this.useLockCodecbx.Location = new System.Drawing.Point(8, 20);
            this.useLockCodecbx.Name = "useLockCodecbx";
            this.useLockCodecbx.Size = new System.Drawing.Size(274, 17);
            this.useLockCodecbx.TabIndex = 84;
            this.useLockCodecbx.Text = "UseLockCodeFile - Send Lock Code File to Farebox ";
            this.useLockCodecbx.UseVisualStyleBackColor = true;
            this.useLockCodecbx.CheckedChanged += new System.EventHandler(this.useLockCodecbx_CheckedChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 83;
            this.label33.Text = "LockFile:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(5, 0);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(116, 13);
            this.label32.TabIndex = 82;
            this.label32.Text = "Kontron Board Settings";
            // 
            // GeneralSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 527);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GeneralSettingsForm";
            this.Text = "General Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PassportTTPUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcctMinRechargeUpDown)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox AgencyCode;
        private System.Windows.Forms.TextBox Passback;
        private System.Windows.Forms.TextBox Dump;
        private System.Windows.Forms.ComboBox MaxEvents;
        private System.Windows.Forms.TextBox LEDTime;
        private System.Windows.Forms.TextBox EventPeriod;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox DST;
        private System.Windows.Forms.TextBox STD;
        private System.Windows.Forms.TextBox MagSecurityCodes;
        private System.Windows.Forms.TextBox SCardSecurityCodes;
        private System.Windows.Forms.TextBox OldAgencyCode;
        private System.Windows.Forms.TextBox MemNearFull;
        private System.Windows.Forms.TextBox BillFull;
        private System.Windows.Forms.TextBox CoinFull;
        private System.Windows.Forms.TextBox BillTimeOut;
        private System.Windows.Forms.TextBox ProbeWatchDog;
        private System.Windows.Forms.TextBox StopPointID;
        private System.Windows.Forms.TextBox MCP;
        private System.Windows.Forms.TextBox ChangeCardThreshold;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox MixFleetMode;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox calcSummaryCount;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox gmtOffsetText;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox ftoTime;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown PassportTTPUpDown;
        private System.Windows.Forms.NumericUpDown AcctMinRechargeUpDown;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox operatorIDText;
        private System.Windows.Forms.Button lockFileBtn;
        private System.Windows.Forms.TextBox lockFileText;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.CheckBox useLockCodecbx;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
    }
}