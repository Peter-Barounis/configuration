﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class GlobalPrinterSettings : Form
    {
        SplitForm splitForm;
        public GlobalPrinterSettings(SplitForm sf)
        {
            splitForm = sf;
            InitializeComponent();

            // Title/Subtitle and fotter text
            titleTextBox.Text = splitForm.globalPrintSettings.title;
            subTitleTextBox.Text = splitForm.globalPrintSettings.subTitle;
            footerTexBox.Text = splitForm.globalPrintSettings.footer;

            // Page Number formatting
            showPageNumbersCheckBox.Checked = splitForm.globalPrintSettings.pageNumbers;
            pageNumbersInHeaderTextBox.Checked = splitForm.globalPrintSettings.pageNumbersInHeader;
            proportionalSpacingCHeckBox.Checked = splitForm.globalPrintSettings.bProportionalColumns;

            footerSpacingNumericUpDown.Value = splitForm.globalPrintSettings.footerSpacing;

            // Header/SubHeader/Footer Alignment
            switch (splitForm.globalPrintSettings.titleComboLCR)
            {
                case StringAlignment.Near:
                    titleComboLCR.SelectedIndex = 0;
                    break;
                case StringAlignment.Center:
                    titleComboLCR.SelectedIndex = 1;
                    break;
                case StringAlignment.Far:
                    titleComboLCR.SelectedIndex = 2;
                    break;
            }

            switch (splitForm.globalPrintSettings.subTitleComboLCR)
            {
                case StringAlignment.Near:
                    subTitleComboLCR.SelectedIndex = 0;
                    break;
                case StringAlignment.Center:
                    subTitleComboLCR.SelectedIndex = 1;
                    break;
                case StringAlignment.Far:
                    subTitleComboLCR.SelectedIndex = 2;
                    break;
            }

            switch (splitForm.globalPrintSettings.footerComboLCR)
            {
                case StringAlignment.Near:
                    footerComboLCR.SelectedIndex = 0;
                    break;
                case StringAlignment.Center:
                    footerComboLCR.SelectedIndex = 1;
                    break;
                case StringAlignment.Far:
                    footerComboLCR.SelectedIndex = 2;
                    break;
            }

            gridComboAlignment.SelectedIndex = 0;
            switch (splitForm.globalPrintSettings.gridAlignment)
            {
                case StringAlignment.Near:
                    gridComboAlignment.SelectedIndex = 0;
                    break;
                case StringAlignment.Center:
                    gridComboAlignment.SelectedIndex = 1;
                    break;
                case StringAlignment.Far:
                    gridComboAlignment.SelectedIndex = 2;
                    break;
            }
            CenterToParent();
        }

        private void CancelPrintButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            // Title/Subheader/Footer text
            splitForm.globalPrintSettings.title = titleTextBox.Text;
            splitForm.globalPrintSettings.subTitle = subTitleTextBox.Text;
            splitForm.globalPrintSettings.footer = footerTexBox.Text;

            // Title/Subheader/Footer alignment
            switch (titleComboLCR.SelectedIndex)
            {
                case 0:
                    splitForm.globalPrintSettings.titleComboLCR = StringAlignment.Near;
                    break;
                case 1:
                    splitForm.globalPrintSettings.titleComboLCR = StringAlignment.Center;
                    break;
                case 2:
                    splitForm.globalPrintSettings.titleComboLCR = StringAlignment.Far;
                    break;
            }
            switch (subTitleComboLCR.SelectedIndex)
            {
                case 0:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Near;
                    break;
                case 1:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Center;
                    break;
                case 2:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Far;
                    break;
            }
            switch (subTitleComboLCR.SelectedIndex)
            {
                case 0:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Near;
                    break;
                case 1:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Center;
                    break;
                case 2:
                    splitForm.globalPrintSettings.subTitleComboLCR = StringAlignment.Far;
                    break;
            }

            // Page Number formatting
            splitForm.globalPrintSettings.pageNumbers = showPageNumbersCheckBox.Checked;
            splitForm.globalPrintSettings.pageNumbersInHeader = pageNumbersInHeaderTextBox.Checked;

            splitForm.globalPrintSettings.bProportionalColumns = proportionalSpacingCHeckBox.Checked;

            splitForm.globalPrintSettings.footerSpacing = Convert.ToInt32(footerSpacingNumericUpDown.Value);

            splitForm.globalPrintSettings.gridAlignment = StringAlignment.Near;
            switch (gridComboAlignment.SelectedIndex)
            {
                case 0:
                    splitForm.globalPrintSettings.gridAlignment = StringAlignment.Near;
                    break;
                case 1:
                    splitForm.globalPrintSettings.gridAlignment = StringAlignment.Center;
                    break;
                case 2:
                    splitForm.globalPrintSettings.gridAlignment = StringAlignment.Far;
                    break;
            }
            DialogResult = DialogResult.OK;
        }
    }
}
