﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
namespace ConfigurationWinForm
{
    public partial class MediaFlagsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public uint MediaFlags = 0;
        bool formLoaded = false;
        private CheckBox[] cbxArray = new CheckBox[16];
        public MediaFlagsForm()
        {
            InitializeComponent();
        }
        public MediaFlagsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            cbxArray[0] = this.checkBox1;
            cbxArray[1] = this.checkBox2;
            cbxArray[2] = this.checkBox3;
            cbxArray[3] = this.checkBox4;
            cbxArray[4] = this.checkBox5;
            cbxArray[5] = this.checkBox6;
            cbxArray[6] = this.checkBox7;
            cbxArray[7] = this.checkBox8;
            cbxArray[8] = this.checkBox9;
            cbxArray[9] = this.checkBox10;
            cbxArray[10] = this.checkBox11;
            cbxArray[11] = this.checkBox12;
            cbxArray[12] = this.checkBox13;
            cbxArray[13] = this.checkBox14;
            cbxArray[14] = this.checkBox15;
            cbxArray[15] = this.checkBox16;
            MediaFlags = Util.ParseValue(splitForm.iniConfig.inifileData.mf);

            for (int i = 0; i < 16; i++)
            {
                if (cbxArray[i] != null)
                {
                    if ((MediaFlags & 1 << i) != 0)
                        cbxArray[i].Checked = true;
                    else
                        cbxArray[i].Checked = false;
                }
            }
            setMediaFlagsHexValue();
            formLoaded = true;

        }

        private void setMediaFlagsHexValue()
        {
            mediaFlagsText.Text = "Media Flags- [0x" + MediaFlags.ToString("X4") + "]";
        }

        private void updateFlag(int index)
        {
            if (cbxArray[index].Checked)
                MediaFlags |= ((uint)1 << index);
            else
                MediaFlags &= ~((uint)1 << index);
        }
        private void muxFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                updateFlag(((CheckBox)sender).TabIndex);
                setMediaFlagsHexValue();
                splitForm.iniConfig.inifileData.mf = "0x" + MediaFlags.ToString("X4");
                splitForm.SetChanged();
            }
        }
    }
}
