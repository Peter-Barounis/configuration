﻿using IPAddressControlLib;

namespace ConfigurationWinForm
{
    partial class LocationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbMux3 = new System.Windows.Forms.CheckBox();
            this.cbMux2 = new System.Windows.Forms.CheckBox();
            this.cbMux1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbVlt5 = new System.Windows.Forms.CheckBox();
            this.cbVlt2 = new System.Windows.Forms.CheckBox();
            this.cbVlt4 = new System.Windows.Forms.CheckBox();
            this.cbVlt1 = new System.Windows.Forms.CheckBox();
            this.cbVlt3 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rbVault4 = new System.Windows.Forms.RadioButton();
            this.rbVault2 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbStartdb = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.enableWifiProbing = new System.Windows.Forms.CheckBox();
            this.fbxport = new System.Windows.Forms.TextBox();
            this.dsport = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ds_rf_ip = new IPAddressControlLib.IPAddressControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.enableMobileTicketing = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.locationNameText = new System.Windows.Forms.TextBox();
            this.locationNumberText = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.enableVndldr = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.enableDatabase = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbMux3);
            this.groupBox1.Controls.Add(this.cbMux2);
            this.groupBox1.Controls.Add(this.cbMux1);
            this.groupBox1.Location = new System.Drawing.Point(16, 61);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(680, 70);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "MUX - IR Probe(s)";
            // 
            // cbMux3
            // 
            this.cbMux3.AutoSize = true;
            this.cbMux3.Location = new System.Drawing.Point(368, 28);
            this.cbMux3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbMux3.Name = "cbMux3";
            this.cbMux3.Size = new System.Drawing.Size(160, 21);
            this.cbMux3.TabIndex = 2;
            this.cbMux3.Text = "MUX3 (Probes 9-12)";
            this.cbMux3.UseVisualStyleBackColor = true;
            this.cbMux3.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbMux2
            // 
            this.cbMux2.AutoSize = true;
            this.cbMux2.Location = new System.Drawing.Point(191, 28);
            this.cbMux2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbMux2.Name = "cbMux2";
            this.cbMux2.Size = new System.Drawing.Size(152, 21);
            this.cbMux2.TabIndex = 1;
            this.cbMux2.Text = "MUX2 (Probes 5-8)";
            this.cbMux2.UseVisualStyleBackColor = true;
            this.cbMux2.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbMux1
            // 
            this.cbMux1.AutoSize = true;
            this.cbMux1.Location = new System.Drawing.Point(13, 28);
            this.cbMux1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbMux1.Name = "cbMux1";
            this.cbMux1.Size = new System.Drawing.Size(152, 21);
            this.cbMux1.TabIndex = 0;
            this.cbMux1.Text = "MUX1 (Probes 1-4)";
            this.cbMux1.UseVisualStyleBackColor = true;
            this.cbMux1.CheckedChanged += new System.EventHandler(this.UpdateMux1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Location = new System.Drawing.Point(16, 146);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(680, 162);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Vaults";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.cbVlt5);
            this.groupBox5.Controls.Add(this.cbVlt2);
            this.groupBox5.Controls.Add(this.cbVlt4);
            this.groupBox5.Controls.Add(this.cbVlt1);
            this.groupBox5.Controls.Add(this.cbVlt3);
            this.groupBox5.Location = new System.Drawing.Point(13, 86);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(547, 66);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Enabled Vaults";
            // 
            // cbVlt5
            // 
            this.cbVlt5.AutoSize = true;
            this.cbVlt5.Location = new System.Drawing.Point(457, 30);
            this.cbVlt5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVlt5.Name = "cbVlt5";
            this.cbVlt5.Size = new System.Drawing.Size(64, 21);
            this.cbVlt5.TabIndex = 4;
            this.cbVlt5.Text = "VLT5";
            this.cbVlt5.UseVisualStyleBackColor = true;
            this.cbVlt5.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbVlt2
            // 
            this.cbVlt2.AutoSize = true;
            this.cbVlt2.Location = new System.Drawing.Point(121, 30);
            this.cbVlt2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVlt2.Name = "cbVlt2";
            this.cbVlt2.Size = new System.Drawing.Size(64, 21);
            this.cbVlt2.TabIndex = 1;
            this.cbVlt2.Text = "VLT2";
            this.cbVlt2.UseVisualStyleBackColor = true;
            this.cbVlt2.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbVlt4
            // 
            this.cbVlt4.AutoSize = true;
            this.cbVlt4.Location = new System.Drawing.Point(345, 30);
            this.cbVlt4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVlt4.Name = "cbVlt4";
            this.cbVlt4.Size = new System.Drawing.Size(64, 21);
            this.cbVlt4.TabIndex = 3;
            this.cbVlt4.Text = "VLT4";
            this.cbVlt4.UseVisualStyleBackColor = true;
            this.cbVlt4.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbVlt1
            // 
            this.cbVlt1.AutoSize = true;
            this.cbVlt1.Location = new System.Drawing.Point(9, 30);
            this.cbVlt1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVlt1.Name = "cbVlt1";
            this.cbVlt1.Size = new System.Drawing.Size(64, 21);
            this.cbVlt1.TabIndex = 0;
            this.cbVlt1.Text = "VLT1";
            this.cbVlt1.UseVisualStyleBackColor = true;
            this.cbVlt1.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // cbVlt3
            // 
            this.cbVlt3.AutoSize = true;
            this.cbVlt3.Location = new System.Drawing.Point(233, 30);
            this.cbVlt3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbVlt3.Name = "cbVlt3";
            this.cbVlt3.Size = new System.Drawing.Size(64, 21);
            this.cbVlt3.TabIndex = 2;
            this.cbVlt3.Text = "VLT3";
            this.cbVlt3.UseVisualStyleBackColor = true;
            this.cbVlt3.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.rbVault4);
            this.groupBox4.Controls.Add(this.rbVault2);
            this.groupBox4.Location = new System.Drawing.Point(13, 21);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(547, 63);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "No. Vaults/Controller";
            // 
            // rbVault4
            // 
            this.rbVault4.AutoSize = true;
            this.rbVault4.Location = new System.Drawing.Point(168, 33);
            this.rbVault4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbVault4.Name = "rbVault4";
            this.rbVault4.Size = new System.Drawing.Size(145, 21);
            this.rbVault4.TabIndex = 1;
            this.rbVault4.TabStop = true;
            this.rbVault4.Text = "4 Vaults/Controller";
            this.rbVault4.UseVisualStyleBackColor = true;
            this.rbVault4.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // rbVault2
            // 
            this.rbVault2.AutoSize = true;
            this.rbVault2.Location = new System.Drawing.Point(5, 33);
            this.rbVault2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbVault2.Name = "rbVault2";
            this.rbVault2.Size = new System.Drawing.Size(145, 21);
            this.rbVault2.TabIndex = 0;
            this.rbVault2.TabStop = true;
            this.rbVault2.Text = "2 Vaults/Controller";
            this.rbVault2.UseVisualStyleBackColor = true;
            this.rbVault2.CheckedChanged += new System.EventHandler(this.UpdateMuxVault);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cbStartdb);
            this.groupBox3.Location = new System.Drawing.Point(16, 326);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(198, 57);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Init Service Options";
            // 
            // cbStartdb
            // 
            this.cbStartdb.AutoSize = true;
            this.cbStartdb.Location = new System.Drawing.Point(17, 21);
            this.cbStartdb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbStartdb.Name = "cbStartdb";
            this.cbStartdb.Size = new System.Drawing.Size(176, 21);
            this.cbStartdb.TabIndex = 0;
            this.cbStartdb.Text = "Start Sybase Database";
            this.cbStartdb.UseVisualStyleBackColor = true;
            this.cbStartdb.CheckedChanged += new System.EventHandler(this.enableWifiProbing_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.enableWifiProbing);
            this.groupBox6.Controls.Add(this.fbxport);
            this.groupBox6.Controls.Add(this.dsport);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.ds_rf_ip);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(16, 471);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(680, 100);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "WiFi IP Address/Ports";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, -14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "WiFi IP Address/Ports";
            // 
            // enableWifiProbing
            // 
            this.enableWifiProbing.AutoSize = true;
            this.enableWifiProbing.Checked = true;
            this.enableWifiProbing.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableWifiProbing.Location = new System.Drawing.Point(13, 25);
            this.enableWifiProbing.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.enableWifiProbing.Name = "enableWifiProbing";
            this.enableWifiProbing.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.enableWifiProbing.Size = new System.Drawing.Size(162, 21);
            this.enableWifiProbing.TabIndex = 0;
            this.enableWifiProbing.Text = ":Enable WiFi Probing";
            this.enableWifiProbing.UseVisualStyleBackColor = true;
            this.enableWifiProbing.CheckedChanged += new System.EventHandler(this.enableWifiProbing_CheckedChanged);
            // 
            // fbxport
            // 
            this.fbxport.Location = new System.Drawing.Point(421, 64);
            this.fbxport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fbxport.Name = "fbxport";
            this.fbxport.Size = new System.Drawing.Size(65, 22);
            this.fbxport.TabIndex = 3;
            this.fbxport.TextChanged += new System.EventHandler(this.dsport_TextChanged);
            // 
            // dsport
            // 
            this.dsport.Location = new System.Drawing.Point(179, 66);
            this.dsport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dsport.Name = "dsport";
            this.dsport.Size = new System.Drawing.Size(65, 22);
            this.dsport.TabIndex = 2;
            this.dsport.TextChanged += new System.EventHandler(this.dsport_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(307, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Farebox Port:";
            // 
            // ds_rf_ip
            // 
            this.ds_rf_ip.AllowInternalTab = false;
            this.ds_rf_ip.AutoHeight = true;
            this.ds_rf_ip.BackColor = System.Drawing.SystemColors.Window;
            this.ds_rf_ip.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ds_rf_ip.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ds_rf_ip.Location = new System.Drawing.Point(421, 26);
            this.ds_rf_ip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ds_rf_ip.MinimumSize = new System.Drawing.Size(114, 22);
            this.ds_rf_ip.Name = "ds_rf_ip";
            this.ds_rf_ip.ReadOnly = false;
            this.ds_rf_ip.Size = new System.Drawing.Size(161, 22);
            this.ds_rf_ip.TabIndex = 1;
            this.ds_rf_ip.Text = "...";
            this.ds_rf_ip.TextChanged += new System.EventHandler(this.dsport_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Garage Computer Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Garage Computer IP Address: ";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.enableMobileTicketing);
            this.groupBox7.Location = new System.Drawing.Point(13, 389);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox7.Size = new System.Drawing.Size(207, 57);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Mobile Ticketing";
            // 
            // enableMobileTicketing
            // 
            this.enableMobileTicketing.AutoSize = true;
            this.enableMobileTicketing.Location = new System.Drawing.Point(18, 21);
            this.enableMobileTicketing.Margin = new System.Windows.Forms.Padding(4);
            this.enableMobileTicketing.Name = "enableMobileTicketing";
            this.enableMobileTicketing.Size = new System.Drawing.Size(180, 21);
            this.enableMobileTicketing.TabIndex = 0;
            this.enableMobileTicketing.Text = "Enable Mobile Ticketing";
            this.enableMobileTicketing.UseVisualStyleBackColor = true;
            this.enableMobileTicketing.CheckedChanged += new System.EventHandler(this.enableMobileTicket_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Location #:";
            // 
            // locationNameText
            // 
            this.locationNameText.Location = new System.Drawing.Point(317, 15);
            this.locationNameText.Name = "locationNameText";
            this.locationNameText.ReadOnly = true;
            this.locationNameText.Size = new System.Drawing.Size(273, 22);
            this.locationNameText.TabIndex = 7;
            this.locationNameText.TabStop = false;
            // 
            // locationNumberText
            // 
            this.locationNumberText.Location = new System.Drawing.Point(99, 15);
            this.locationNumberText.Name = "locationNumberText";
            this.locationNumberText.ReadOnly = true;
            this.locationNumberText.Size = new System.Drawing.Size(55, 22);
            this.locationNumberText.TabIndex = 8;
            this.locationNumberText.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(204, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "Location Name:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.enableVndldr);
            this.groupBox8.Location = new System.Drawing.Point(232, 327);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(464, 56);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            // 
            // enableVndldr
            // 
            this.enableVndldr.AutoSize = true;
            this.enableVndldr.Location = new System.Drawing.Point(11, 12);
            this.enableVndldr.Name = "enableVndldr";
            this.enableVndldr.Size = new System.Drawing.Size(239, 38);
            this.enableVndldr.TabIndex = 0;
            this.enableVndldr.Text = "Enable vndldr section to allow \r\nTVM/PEM transaction processing";
            this.enableVndldr.UseVisualStyleBackColor = true;
            this.enableVndldr.CheckedChanged += new System.EventHandler(this.enableVndldr_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, -3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "Enable Vndldr Section";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.enableDatabase);
            this.groupBox9.Location = new System.Drawing.Point(234, 389);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(462, 56);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, -3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(168, 17);
            this.label15.TabIndex = 12;
            this.label15.Text = "Enable Database Section";
            // 
            // enableDatabase
            // 
            this.enableDatabase.AutoSize = true;
            this.enableDatabase.Location = new System.Drawing.Point(11, 18);
            this.enableDatabase.Name = "enableDatabase";
            this.enableDatabase.Size = new System.Drawing.Size(442, 21);
            this.enableDatabase.TabIndex = 0;
            this.enableDatabase.Text = "Enable Database section (normally only for Garage Data System)";
            this.enableDatabase.UseVisualStyleBackColor = true;
            this.enableDatabase.CheckedChanged += new System.EventHandler(this.enableVndldr_CheckedChanged);
            // 
            // LocationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 597);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.locationNumberText);
            this.Controls.Add(this.locationNameText);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "LocationForm";
            this.Text = "Location";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LocationForm_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbMux3;
        private System.Windows.Forms.CheckBox cbMux2;
        private System.Windows.Forms.CheckBox cbMux1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbVault4;
        private System.Windows.Forms.RadioButton rbVault2;
        private System.Windows.Forms.CheckBox cbVlt5;
        private System.Windows.Forms.CheckBox cbVlt4;
        private System.Windows.Forms.CheckBox cbVlt3;
        private System.Windows.Forms.CheckBox cbVlt2;
        private System.Windows.Forms.CheckBox cbVlt1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbStartdb;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fbxport;
        private System.Windows.Forms.TextBox dsport;
        private System.Windows.Forms.Label label3;
        private IPAddressControlLib.IPAddressControl ds_rf_ip;
        private System.Windows.Forms.CheckBox enableWifiProbing;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox enableMobileTicketing;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox locationNameText;
        private System.Windows.Forms.TextBox locationNumberText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox enableVndldr;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox enableDatabase;
    }
}