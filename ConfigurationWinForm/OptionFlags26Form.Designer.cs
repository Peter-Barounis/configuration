﻿namespace ConfigurationWinForm
{
    partial class OptionFlags26Form

    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.optionflagsGridTitle = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.optionflagsGridTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(18, 113);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(590, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "0x0100 – Use the new \"2016\" GENFARE logo on the OCU. Requires OCU 233 and higher";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(18, 139);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(414, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "0x0200 – Mark stored value cards as empty when value is $0.";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(18, 166);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(524, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "0x0400 – Use “Dump Time” entered in the GFI.INI file for fare and key timeouts.";
            this.toolTip2.SetToolTip(this.checkBox11, "Use “Dump Time” entered in the GFI.INI file for fare and key timeouts. \r\nDump val" +
        "ue was 30 seconds for Full Fare and 30 or 60 for keys depending on action ");
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(18, 193);
            this.checkBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(593, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "0x0800 – Show the CASHBOX ID number on the 2nd page of the “Farebox Status” scree" +
    "n.";
            this.toolTip2.SetToolTip(this.checkBox12, "Show the CASHBOX ID number on the 2nd page of the “Farebox Status” screen. \r\nThis" +
        " option allows for showing the CBID number in a non-secure location. ");
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(18, 220);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(467, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "0x1000 – Save “Cashbox Inserted” transaction. If set, save transaction";
            this.toolTip2.SetToolTip(this.checkBox13, "Save “Cashbox Inserted” transaction. If set, save transaction");
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // optionflagsGridTitle
            // 
            this.optionflagsGridTitle.Controls.Add(this.label2);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown2);
            this.optionflagsGridTitle.Controls.Add(this.label1);
            this.optionflagsGridTitle.Controls.Add(this.numericUpDown1);
            this.optionflagsGridTitle.Controls.Add(this.button1);
            this.optionflagsGridTitle.Controls.Add(this.checkBox16);
            this.optionflagsGridTitle.Controls.Add(this.checkBox15);
            this.optionflagsGridTitle.Controls.Add(this.checkBox14);
            this.optionflagsGridTitle.Controls.Add(this.checkBox9);
            this.optionflagsGridTitle.Controls.Add(this.checkBox10);
            this.optionflagsGridTitle.Controls.Add(this.checkBox11);
            this.optionflagsGridTitle.Controls.Add(this.checkBox12);
            this.optionflagsGridTitle.Controls.Add(this.checkBox13);
            this.optionflagsGridTitle.Location = new System.Drawing.Point(15, 14);
            this.optionflagsGridTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Name = "optionflagsGridTitle";
            this.optionflagsGridTitle.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.optionflagsGridTitle.Size = new System.Drawing.Size(854, 369);
            this.optionflagsGridTitle.TabIndex = 0;
            this.optionflagsGridTitle.TabStop = false;
            this.optionflagsGridTitle.Text = "Option Flag[0x0000]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(544, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "DESFire Smart Card Printed ID, file 0 (Tucson 11), format version indicator.  (0x" +
    "00F0)";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(18, 71);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(62, 22);
            this.numericUpDown2.TabIndex = 19;
            this.numericUpDown2.ValueChanged += new System.EventHandler(this.desfireComboChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(574, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "DESFire Smart Card Transfer/PayGo/Bounus, file 3/4/5, format version indicator. (" +
    "0x000F)";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(18, 32);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(62, 22);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.desfireComboChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(18, 327);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 16;
            this.button1.Text = "Restore Default Values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(18, 300);
            this.checkBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(822, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "0x8000 – Enable use of 2nd MID for talking to 2 fareboxes on same J1708 bus (Odys" +
    "sey+ v152, Fast Fare / Fast Fare –e v428)";
            this.toolTip2.SetToolTip(this.checkBox16, "Enable use of 2nd MID for talking to 2 fareboxes on same J1708 bus (Odyssey+ v152" +
        ", Fast Fare / Fast Fare -e v428) ");
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(18, 273);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(681, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "0x4000 - Change “DRIVER” to “CAP ID” on login screen (Odyssey+ v152, Fast Fare / " +
    "Fast Fare –e v428)";
            this.toolTip2.SetToolTip(this.checkBox15, "Change “DRIVER” to “CAP ID” on login screen (Odyssey+ v152, Fast Fare / Fast Fare" +
        " -e v428) \r\nMake sure that option to change DRIVER  OPERATOR is not set (LAMetr" +
        "oOption (0x80) ");
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(18, 247);
            this.checkBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(368, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "0x2000 – Allow setting of Cashbox size at the Farebox.";
            this.toolTip2.SetToolTip(this.checkBox14, "Allow setting of Cashbox size at the Farebox. \r\nThis will override the Bill/Coin " +
        "full values defined in the .INI file. ");
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.optionflag_CheckedChanged);
            // 
            // OptionFlags26Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 394);
            this.Controls.Add(this.optionflagsGridTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "OptionFlags26Form";
            this.Text = "OptionFlag26";
            this.optionflagsGridTitle.ResumeLayout(false);
            this.optionflagsGridTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.GroupBox optionflagsGridTitle;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;


    }
}