﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GfiUtilities;

namespace ConfigurationWinForm
{
    public partial class WifiGeneralSettingsForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        bool formLoaded = false;
        public ToolTip toolTip1 = new ToolTip();
        public WifiGeneralSettingsForm()
        {
            InitializeComponent();
        }

        public WifiGeneralSettingsForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            LoadData();
            formLoaded = true;
        }

        public void LoadData()
        {
            int security,i;
            int usedhcpValue = 0;
            int networkModeValue = 0;
            int topologyValue = 0;
            DSPort.Text = splitForm.iniConfig.inifileData.DsPort;
            FbxPort.Text = splitForm.iniConfig.inifileData.FbxPort;
            DS_RF_IP.Text = splitForm.iniConfig.inifileData.DS_RF_IP;
            FBX_IP.Text = splitForm.iniConfig.inifileData.FBX_IP;
            DNS_IP.Text = splitForm.iniConfig.inifileData.DNS_IP;
            Gateway.Text = splitForm.iniConfig.inifileData.Gateway;

            usedhcpValue = (int)(Util.ParseValue(splitForm.iniConfig.inifileData.UseDHCP));
            if (usedhcpValue < 0 || usedhcpValue > 2)
                usedhcpValue=0;
            useDHCPCombo.SelectedIndex = usedhcpValue;
            if (usedhcpValue == 1)
            {
                FBX_IP.Text = "0.0.0.0";
                FBX_IP.Enabled = false;
            }
            SSID.Text = splitForm.iniConfig.inifileData.SSID;
            KeyPhrase.Text = splitForm.iniConfig.inifileData.KeyPhrase;
            HostBits.Text = splitForm.iniConfig.inifileData.HostBits;

            topologyValue = (int)Util.ParseValue(splitForm.iniConfig.inifileData.Topology);
            security = (int)Util.ParseValue(splitForm.iniConfig.inifileData.Security);
            networkModeValue = (int)(Util.ParseValue(splitForm.iniConfig.inifileData.NetworkMode));

            if (security < 0 || security > 3)
                security = 0;
            if (networkModeValue < 0 || networkModeValue > 2)
                networkModeValue = 0;
            if (topologyValue < 0 || topologyValue > 2)
                topologyValue = 0;

            Security.SelectedIndex = security;
            networkModeCombo.SelectedIndex = networkModeValue;
            topologyCombo.SelectedIndex = topologyValue;

            Encryption.Text = splitForm.iniConfig.inifileData.Encryption;
            Authentication.Text = splitForm.iniConfig.inifileData.Authentication;
            FBXNetwork.Text= splitForm.iniConfig.inifileData.FBXNetwork;
            i = (int)(Util.ParseValue(splitForm.iniConfig.inifileData.UseDHCP));
            if (i != 1)
                FBX_IP.Enabled = true;
            else
                FBX_IP.Enabled = false;
            SetSecurity();
        }

        public void SaveData()
        {
            int i;
            if (formLoaded)
            {
                splitForm.iniConfig.inifileData.DsPort = DSPort.Text;
                splitForm.iniConfig.inifileData.FbxPort = FbxPort.Text;
                splitForm.iniConfig.inifileData.DS_RF_IP = DS_RF_IP.Text;
                splitForm.iniConfig.inifileData.FBX_IP = FBX_IP.Text;
                splitForm.iniConfig.inifileData.UseDHCP = useDHCPCombo.SelectedIndex.ToString();
                splitForm.iniConfig.inifileData.SSID = SSID.Text;
                splitForm.iniConfig.inifileData.KeyPhrase = KeyPhrase.Text;
                splitForm.iniConfig.inifileData.HostBits = HostBits.Text;

                splitForm.iniConfig.inifileData.Security = Security.SelectedIndex.ToString();
                splitForm.iniConfig.inifileData.NetworkMode = networkModeCombo.SelectedIndex.ToString();
                splitForm.iniConfig.inifileData.Topology = topologyCombo.SelectedIndex.ToString();

                splitForm.iniConfig.inifileData.Encryption = Encryption.Text;
                splitForm.iniConfig.inifileData.Authentication = Authentication.Text;
                splitForm.iniConfig.inifileData.FBXNetwork = FBXNetwork.Text;

                splitForm.iniConfig.inifileData.DNS_IP = DNS_IP.Text;
                splitForm.iniConfig.inifileData.Gateway = Gateway.Text;
                i = (int)(Util.ParseValue(splitForm.iniConfig.inifileData.UseDHCP));

                if (i != 1)
                    FBX_IP.Enabled = true;
                else
                    FBX_IP.Enabled = false;
                splitForm.SetChanged();
            }
        }

        public void ValidateNumericTextBox(TextBox textbox)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textbox.Text, "[^0-9]"))
            {
                System.Media.SystemSounds.Hand.Play();
                textbox.Text = textbox.Text.Remove(textbox.Text.Length - 1);
                textbox.SelectionStart = textbox.Text.Length;
            }
        }
        private void numeric_TextChanged(object sender, EventArgs e)
        {
            ValidateNumericTextBox((TextBox)sender);
            SaveData();
        }

        public void UpdateData(object sender, EventArgs e)
        {
            if (formLoaded)
            {
                SetSecurity();
                SaveData();
            }
        }

        public void SetSecurity()
        {
            int topology = this.topologyCombo.SelectedIndex;
            int security = Security.SelectedIndex;


            if (topology == 1 && Security.Items.Count>2)
            {
                Security.Items.Clear(); 
                Security.Items.Add("0=None");
                Security.Items.Add("1=WEP");
                if (security < 0 || security > 1)
                    security=1;
                Security.SelectedIndex = security;
            }
            else
            {
                if (topology != 1 & Security.Items.Count < 4)
                {
                    Security.Items.Clear();
                    Security.Items.Add("0=None");
                    Security.Items.Add("1=WEP");
                    Security.Items.Add("2=WPA");
                    Security.Items.Add("3=WPA2");
                    if (security < 0 || security > 3)
                        security = 3;
                    Security.SelectedIndex = security;
                }
            }
        }

        private void UpdateSSID(object sender, EventArgs e)
        {
            string str="";
            ssidEncryptedHexText.Text = "";
            ssidEncryptedBinaryText.Text = "";
            if (SSID.Text.Length > 0)
            {
                splitForm.iniConfig.inifileData.SSID = SSID.Text;
                ssidEncryptedHexText.Text = splitForm.iniConfig.iniFile.EncryptString(SSID.Text, ref str);
                ssidEncryptedBinaryText.Text = str;
            }

            if (formLoaded)
            {
                SetSecurity();
                SaveData();
            }
        }

        private void UpdateKeyPhrase(object sender, EventArgs e)
        {
            string str="";
            keyphraseEncryptedHexText.Text = "";
            keyphraseEncryptedBinaryText.Text = "";
            if (KeyPhrase.Text.Length > 0)
            {
                splitForm.iniConfig.inifileData.KeyPhrase = KeyPhrase.Text;
                keyphraseEncryptedHexText.Text = splitForm.iniConfig.iniFile.EncryptString(KeyPhrase.Text, ref str);
                keyphraseEncryptedBinaryText.Text = str;
            }

            if (formLoaded)
            {
                SetSecurity();
                SaveData();
            }
        }

        private void showPasswordCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if ( ((CheckBox)sender).Checked)
                this.KeyPhrase.PasswordChar = '\0';
            else
                this.KeyPhrase.PasswordChar = '*';
        }
    }
}
