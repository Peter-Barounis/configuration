﻿using System;
using System.IO;
using System.Security;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigurationWinForm
{
    public partial class DatabaseForm : Form
    {
        public SplitForm splitForm;
        public bool dirtyFlag = false;
        public bool formloaded = false;
        public OpenFileDialog openFileDialog1;
        public DatabaseForm()
        {
            InitializeComponent();
        }

        public DatabaseForm(SplitForm sf)
        {
            this.splitForm = sf;
            InitializeComponent();
            LoadData();
            formloaded = true;
        }

        private void SelectFile(TextBox text, string ext)
        {
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.InitialDirectory = text.Text;
            openFileDialog1.Filter = ext+"|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = false;
            openFileDialog1.ValidateNames = false;
            openFileDialog1.CheckFileExists = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                text.Text = openFileDialog1.FileName;
        }

        private void databaseBrowseBtn_Click(object sender, EventArgs e)
        {
            SelectFile(this.DBF,"gfi db file (*.dat)|*.dat");
        }

        private void sysconBrowseBtn_Click(object sender, EventArgs e)
        {
            SelectFile(this.SecDBF,"syscon db file (*.dat)|*.dat");
        }

        private void backupBrowseBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.SelectedPath = BkUp.Text;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.BkUp.Text = dlg.SelectedPath;
            }
        }
        public void LoadData()
        {
            ENG.Text = splitForm.iniConfig.inifileData.ENG;
            DBF.Text = splitForm.iniConfig.inifileData.DBF;
            SecDBF.Text = splitForm.iniConfig.inifileData.SecDBF;
            START.Text = splitForm.iniConfig.inifileData.START;
            BkUp.Text = splitForm.iniConfig.inifileData.BkUp;
        }

        public void SaveData(object sender, EventArgs e)
        {
            if (formloaded)
            {
                splitForm.iniConfig.inifileData.ENG = ENG.Text;
                splitForm.iniConfig.inifileData.DBF = DBF.Text;
                splitForm.iniConfig.inifileData.SecDBF = SecDBF.Text;
                splitForm.iniConfig.inifileData.START = START.Text;
                splitForm.iniConfig.inifileData.BkUp = BkUp.Text;
                splitForm.SetChanged();
            }
        }
    }
}
