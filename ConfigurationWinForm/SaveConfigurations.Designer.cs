﻿namespace ConfigurationWinForm
{
    partial class SaveConfigurations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveConfigurations));
            this.inifileBtn = new System.Windows.Forms.CheckBox();
            this.inittabBtn = new System.Windows.Forms.CheckBox();
            this.netconfigBtn = new System.Windows.Forms.CheckBox();
            this.allBtn = new System.Windows.Forms.CheckBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.saveYes = new System.Windows.Forms.Button();
            this.saveNo = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.netconfigFileNameText = new System.Windows.Forms.TextBox();
            this.inittabFileNameText = new System.Windows.Forms.TextBox();
            this.iniFileNameText = new System.Windows.Forms.TextBox();
            this.tcpIpPortText = new System.Windows.Forms.TextBox();
            this.tcpIpPortLabel = new System.Windows.Forms.Label();
            this.saveCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.locationName = new System.Windows.Forms.TextBox();
            this.saveCleanConfiguration = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // inifileBtn
            // 
            this.inifileBtn.AutoSize = true;
            this.inifileBtn.Location = new System.Drawing.Point(5, 27);
            this.inifileBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inifileBtn.Name = "inifileBtn";
            this.inifileBtn.Size = new System.Drawing.Size(63, 21);
            this.inifileBtn.TabIndex = 0;
            this.inifileBtn.Text = "gfi.ini";
            this.inifileBtn.UseVisualStyleBackColor = true;
            // 
            // inittabBtn
            // 
            this.inittabBtn.AutoSize = true;
            this.inittabBtn.Location = new System.Drawing.Point(5, 60);
            this.inittabBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inittabBtn.Name = "inittabBtn";
            this.inittabBtn.Size = new System.Drawing.Size(68, 21);
            this.inittabBtn.TabIndex = 1;
            this.inittabBtn.Text = "Inittab";
            this.inittabBtn.UseVisualStyleBackColor = true;
            // 
            // netconfigBtn
            // 
            this.netconfigBtn.AutoSize = true;
            this.netconfigBtn.Location = new System.Drawing.Point(5, 93);
            this.netconfigBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.netconfigBtn.Name = "netconfigBtn";
            this.netconfigBtn.Size = new System.Drawing.Size(113, 21);
            this.netconfigBtn.TabIndex = 2;
            this.netconfigBtn.Text = "Netconfig.cfg";
            this.netconfigBtn.UseVisualStyleBackColor = true;
            // 
            // allBtn
            // 
            this.allBtn.AutoSize = true;
            this.allBtn.Location = new System.Drawing.Point(5, 126);
            this.allBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.allBtn.Name = "allBtn";
            this.allBtn.Size = new System.Drawing.Size(45, 21);
            this.allBtn.TabIndex = 3;
            this.allBtn.Text = "All";
            this.allBtn.UseVisualStyleBackColor = true;
            this.allBtn.CheckedChanged += new System.EventHandler(this.allBtn_CheckedChanged);
            // 
            // saveYes
            // 
            this.saveYes.Location = new System.Drawing.Point(13, 232);
            this.saveYes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveYes.Name = "saveYes";
            this.saveYes.Size = new System.Drawing.Size(88, 28);
            this.saveYes.TabIndex = 0;
            this.saveYes.Text = "Yes";
            this.saveYes.UseVisualStyleBackColor = true;
            this.saveYes.Click += new System.EventHandler(this.saveYes_Click);
            // 
            // saveNo
            // 
            this.saveNo.Location = new System.Drawing.Point(139, 232);
            this.saveNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveNo.Name = "saveNo";
            this.saveNo.Size = new System.Drawing.Size(88, 28);
            this.saveNo.TabIndex = 1;
            this.saveNo.Text = "No";
            this.saveNo.UseVisualStyleBackColor = true;
            this.saveNo.Click += new System.EventHandler(this.saveNo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.saveCleanConfiguration);
            this.groupBox1.Controls.Add(this.netconfigFileNameText);
            this.groupBox1.Controls.Add(this.inittabFileNameText);
            this.groupBox1.Controls.Add(this.iniFileNameText);
            this.groupBox1.Controls.Add(this.tcpIpPortText);
            this.groupBox1.Controls.Add(this.tcpIpPortLabel);
            this.groupBox1.Controls.Add(this.inittabBtn);
            this.groupBox1.Controls.Add(this.inifileBtn);
            this.groupBox1.Controls.Add(this.netconfigBtn);
            this.groupBox1.Controls.Add(this.allBtn);
            this.groupBox1.Location = new System.Drawing.Point(15, 57);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(672, 164);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select files";
            // 
            // netconfigFileNameText
            // 
            this.netconfigFileNameText.Location = new System.Drawing.Point(137, 92);
            this.netconfigFileNameText.Name = "netconfigFileNameText";
            this.netconfigFileNameText.ReadOnly = true;
            this.netconfigFileNameText.Size = new System.Drawing.Size(323, 22);
            this.netconfigFileNameText.TabIndex = 8;
            // 
            // inittabFileNameText
            // 
            this.inittabFileNameText.Location = new System.Drawing.Point(137, 59);
            this.inittabFileNameText.Name = "inittabFileNameText";
            this.inittabFileNameText.ReadOnly = true;
            this.inittabFileNameText.Size = new System.Drawing.Size(323, 22);
            this.inittabFileNameText.TabIndex = 7;
            // 
            // iniFileNameText
            // 
            this.iniFileNameText.Location = new System.Drawing.Point(137, 26);
            this.iniFileNameText.Name = "iniFileNameText";
            this.iniFileNameText.ReadOnly = true;
            this.iniFileNameText.Size = new System.Drawing.Size(323, 22);
            this.iniFileNameText.TabIndex = 6;
            // 
            // tcpIpPortText
            // 
            this.tcpIpPortText.Location = new System.Drawing.Point(231, 125);
            this.tcpIpPortText.Margin = new System.Windows.Forms.Padding(4);
            this.tcpIpPortText.Name = "tcpIpPortText";
            this.tcpIpPortText.Size = new System.Drawing.Size(107, 22);
            this.tcpIpPortText.TabIndex = 5;
            this.tcpIpPortText.TextChanged += new System.EventHandler(this.updatePort);
            // 
            // tcpIpPortLabel
            // 
            this.tcpIpPortLabel.AutoSize = true;
            this.tcpIpPortLabel.Location = new System.Drawing.Point(134, 128);
            this.tcpIpPortLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tcpIpPortLabel.Name = "tcpIpPortLabel";
            this.tcpIpPortLabel.Size = new System.Drawing.Size(89, 17);
            this.tcpIpPortLabel.TabIndex = 4;
            this.tcpIpPortLabel.Text = "TCP/IP Port: ";
            // 
            // saveCancel
            // 
            this.saveCancel.Location = new System.Drawing.Point(599, 232);
            this.saveCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveCancel.Name = "saveCancel";
            this.saveCancel.Size = new System.Drawing.Size(88, 28);
            this.saveCancel.TabIndex = 2;
            this.saveCancel.Text = "Cancel";
            this.saveCancel.UseVisualStyleBackColor = true;
            this.saveCancel.Click += new System.EventHandler(this.saveCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Location:";
            // 
            // locationName
            // 
            this.locationName.Location = new System.Drawing.Point(20, 30);
            this.locationName.Margin = new System.Windows.Forms.Padding(4);
            this.locationName.Name = "locationName";
            this.locationName.ReadOnly = true;
            this.locationName.Size = new System.Drawing.Size(667, 22);
            this.locationName.TabIndex = 9;
            // 
            // saveCleanConfiguration
            // 
            this.saveCleanConfiguration.AutoSize = true;
            this.saveCleanConfiguration.Location = new System.Drawing.Point(466, 26);
            this.saveCleanConfiguration.Name = "saveCleanConfiguration";
            this.saveCleanConfiguration.Size = new System.Drawing.Size(197, 21);
            this.saveCleanConfiguration.TabIndex = 9;
            this.saveCleanConfiguration.Text = "Save CLEAN configuration";
            this.toolTip1.SetToolTip(this.saveCleanConfiguration, resources.GetString("saveCleanConfiguration.ToolTip"));
            this.saveCleanConfiguration.UseVisualStyleBackColor = true;
            // 
            // SaveConfigurations
            // 
            this.AcceptButton = this.saveYes;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 273);
            this.Controls.Add(this.locationName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.saveCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.saveNo);
            this.Controls.Add(this.saveYes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SaveConfigurations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Save Configurations";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox inifileBtn;
        private System.Windows.Forms.CheckBox inittabBtn;
        private System.Windows.Forms.CheckBox netconfigBtn;
        private System.Windows.Forms.CheckBox allBtn;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button saveYes;
        private System.Windows.Forms.Button saveNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button saveCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox locationName;
        private System.Windows.Forms.TextBox tcpIpPortText;
        private System.Windows.Forms.Label tcpIpPortLabel;
        private System.Windows.Forms.TextBox netconfigFileNameText;
        private System.Windows.Forms.TextBox inittabFileNameText;
        private System.Windows.Forms.TextBox iniFileNameText;
        private System.Windows.Forms.CheckBox saveCleanConfiguration;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}