﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GfiUtilities;
using System.IO;
using System.Data.SQLite;

namespace TVMSQLiteEditor
{
    public partial class editSQLiteDataForm : Form
    {
        int heightPanel1;
        int heightPanel2;
        Size originalSize;
        Size singlePanelSize;
        bool bSinglePanel = false;
        public SQLiteConnection cnnSqlite = null;
        public DataTable schema=null;
        public bool bFormLoaded = false;
        private ResultsForm resultsForm;
        public editSQLiteDataForm()
        {
            InitializeComponent();
            heightPanel1 = splitContainer1.Panel1.Height;
            heightPanel2 = splitContainer1.Panel2.Height;
            singlePanelSize = originalSize = Size;
            singlePanelSize.Height -= heightPanel2;
            Size = singlePanelSize;
            bSinglePanel = true;
            databaseFolderPathTextBox.Text = Util.GetGfiKeyValueString("Global", "Base Directory", "C:\\GFI");
            if (databaseFolderPathTextBox.Text.Length>0)
                databaseFolderPathTextBox.Text += "\\cnf\\tvm";
            dbTypeCombo.SelectedIndex = 0;
            CenterToScreen();
            bFormLoaded = true;
            LoadSqliteFileNames();
        }

        public SQLiteConnection ConnectToDatabase(string dbName)
        {
            string connString;
            //if (globalSettings.sysconPassword.Length > 0)
            //    connString = @"Data Source=" + globalSettings.dbName + "; Version=3; Password=" + globalSettings.sysconPassword + "; FailIfMissing=True; Foreign Keys=True;";
            connString = @"Data Source=" + dbName + "; Version=3; FailIfMissing=True; Foreign Keys=True;";
            cnnSqlite = new SQLiteConnection(connString);
            try
            {
                cnnSqlite.Open();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.ToString(), "Database Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cnnSqlite = null;
            }
            return cnnSqlite;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            if (cnnSqlite != null)
            {
                cnnSqlite.Close();
                cnnSqlite = null;
            }
            Close();
        }


        private void viewSchemaButton_Click(object sender, EventArgs e)
        {
            if (bSinglePanel)
            {
                Size = originalSize;
                bSinglePanel = false;
            }
            GetSchemaSQLite();
        }

        private void viewDataButton_Click(object sender, EventArgs e)
        {
            if (bSinglePanel)
            {
                Size = originalSize;
                bSinglePanel = false;
            }
            SelectSQLiteData();
        }

        private void LoadTableNames()
        {
            int tablecnt = 0;
            try
            {
                Cursor = Cursors.WaitCursor;
                string sql = "SELECT name FROM sqlite_master WHERE type='table' order by name";
                using (SQLiteCommand cmd = new SQLiteCommand(sql, cnnSqlite))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        tableCombo.Items.Clear();
                        while (reader.Read())
                        {
                            string temp = reader.GetString(0);
                            tableCombo.Items.Add(temp);
                            tablecnt++;
                        }
                    }
                }
                if (tableCombo.Items.Count > 0)
                    tableCombo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ODBC Error: " + ex.Message);
            }

            finally
            {
                Cursor = Cursors.Default;
            }

            LoadColumns(tableCombo.Text);
        }

        private void LoadColumns(string tablename)
        {
            string query = "SELECT * FROM " + tablename + " LIMIT 1;";
            keyColumnCombo.Items.Clear();
            valueColumnCombo.Items.Clear();
            using (SQLiteCommand sqliteDbCommand = new SQLiteCommand(query, cnnSqlite))
            {
                //SQLiteCommand sqliteDbCommand = new SQLiteCommand(query, cnnSqlite);
                using (SQLiteDataReader sqliteDbReader = sqliteDbCommand.ExecuteReader())
                {
                    if (schema != null)
                        schema.Dispose();
                    schema = sqliteDbReader.GetSchemaTable();
                    foreach (DataRow row in schema.Rows)
                    {
                        Type dbtype = row.Field<Type>("DataType");
                        string ColumnName = row.Field<string>("ColumnName");
                        bool bAllowNull = row.Field<bool>("AllowDBNull");
                        bool bAutoIncrement = row.Field<bool>("IsAutoIncrement");
                        bool bIsKey = row.Field<bool>("IsKey");
                        keyColumnCombo.Items.Add(ColumnName);
                        valueColumnCombo.Items.Add(ColumnName);
                    }
                    if (keyColumnCombo.Items.Count > 0)
                    {
                        keyColumnCombo.SelectedIndex = 0;
                        valueColumnCombo.SelectedIndex = 0;
                    }
                }
            }
        }

        private void LoadSqliteFileNames()
        {
            string fname = "";
            if (dbTypeCombo.SelectedIndex == 0)     // gfi file
                fname = @"gfi.sqlite.1.???";
            else                                    // cfg file
                fname = @"cfg.sqlite.1.???";
            databaseNameCombo.Items.Clear();
            sourceTVMListBox.Items.Clear();
            targetTVMListBox.Items.Clear();
            if (!Directory.Exists(databaseFolderPathTextBox.Text))
                return;
            string[] sqliteFiles = Directory.GetFiles(databaseFolderPathTextBox.Text, fname);
            foreach (string item in sqliteFiles)
            {
                databaseNameCombo.Items.Add(item);
                sourceTVMListBox.Items.Add(item);
            }
            if (databaseNameCombo.Items.Count > 0)
            {
                databaseNameCombo.SelectedIndex = 0;
                if (cnnSqlite != null)
                {
                    cnnSqlite.Close();
                    cnnSqlite = null;
                }
                cnnSqlite = ConnectToDatabase(databaseNameCombo.Text);
                if (cnnSqlite != null)
                    LoadTableNames();
            }
        }

        private void browseFolderPathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.SelectedPath = databaseFolderPathTextBox.Text;
            if (dlg.ShowDialog() == DialogResult.OK)
                LoadSqliteFileNames();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Size = singlePanelSize;
            bSinglePanel = true;
        }

        private void addItem_Click(object sender, EventArgs e)
        {
            string strItem;
            foreach (Object selecteditem in sourceTVMListBox.SelectedItems)
            {
                strItem = selecteditem as String;
                targetTVMListBox.Items.Add(strItem);
            }

            for (int i = 0; i < targetTVMListBox.Items.Count; i++)
            {
                for (int j = 0; j < sourceTVMListBox.Items.Count; j++)
                {
                    if (targetTVMListBox.Items[i].ToString() == sourceTVMListBox.Items[j].ToString())
                    {
                        sourceTVMListBox.Items.RemoveAt(j);
                        break;
                    }
                }
            }
        }

        private void addAllButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < sourceTVMListBox.Items.Count; i++)
                sourceTVMListBox.SetSelected(i, true);
            addItem_Click(sender, e);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            string strItem;
            foreach (Object selecteditem in targetTVMListBox.SelectedItems)
            {
                strItem = selecteditem as String;
                sourceTVMListBox.Items.Add(strItem);
            }

            for (int i = 0; i < sourceTVMListBox.Items.Count; i++)
            {
                for (int j = 0; j < targetTVMListBox.Items.Count; j++)
                {
                    if (sourceTVMListBox.Items[i].ToString() == targetTVMListBox.Items[j].ToString())
                    {
                        targetTVMListBox.Items.RemoveAt(j);
                        break;
                    }
                }
            }
        }

        private void removeAllButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < targetTVMListBox.Items.Count; i++)
                targetTVMListBox.SetSelected(i, true);
            string strItem;
            foreach (Object selecteditem in targetTVMListBox.SelectedItems)
            {
                strItem = selecteditem as String;
                sourceTVMListBox.Items.Add(strItem);
            }

            for (int i = 0; i < sourceTVMListBox.Items.Count; i++)
            {
                for (int j = 0; j < targetTVMListBox.Items.Count; j++)
                {
                    if (sourceTVMListBox.Items[i].ToString() == targetTVMListBox.Items[j].ToString())
                    {
                        targetTVMListBox.Items.RemoveAt(j);
                        break;
                    }
                }
            }
        }

        private void tableCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadColumns(tableCombo.Text);
        }

        private void generateSQLButton_Click(object sender, EventArgs e)
        {
            string keyQuote = "";
            string valueQuote = "";

            if ( (keyColumnTypeTextBox.Text.Trim() == "System.String") || (keyColumnTypeTextBox.Text.Trim() == "System.Byte"))
                keyQuote = "'";
            if ((valueColumnTypeTextBox.Text.Trim() == "System.String") || (valueColumnTypeTextBox.Text.Trim() == "System.Byte"))
                valueQuote = "'";
            if (customQueryTextBox.Text.Length>0)
                customQueryTextBox.Text += Environment.NewLine;
            customQueryTextBox.Text +=   "UPDATE " + tableCombo.Text + " SET " + valueColumnCombo.Text + "=" + 
                                        valueQuote + valueTextBox.Text + valueQuote +
                                        " WHERE " + keyColumnCombo.Text + "=" +
                                        keyQuote+ keyTextBox.Text + keyQuote+ ";";
            
            customQueryTextBox.SelectionStart = customQueryTextBox.TextLength;
            customQueryTextBox.ScrollToCaret();
        }

        private void databaseNameCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bFormLoaded)
            {
                if (databaseNameCombo.Items.Count > 0)
                {
                    if (cnnSqlite != null)
                    {
                        cnnSqlite.Close();
                        cnnSqlite = null;
                    }

                    cnnSqlite = ConnectToDatabase(databaseNameCombo.Text);
                    if (cnnSqlite != null)
                        LoadTableNames();
                }
            }
        }

        private void keyColumnCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bFormLoaded)
            {
                for (int i = 0; i < schema.Rows.Count; i++)
                {
                    if (schema.Rows[i].Field<string>("ColumnName") == keyColumnCombo.Text)
                    {
                        keyColumnTypeTextBox.Text = schema.Rows[i].Field<Type>("DataType").ToString();
                        keyLengthTextBox.Text = schema.Rows[i].Field<int>("ColumnSize").ToString();
                        keyAllowNullsCheckBox.Checked = schema.Rows[i].Field<bool>("AllowDBNull");
                        break;
                    }
                }
            }
        }

        private void valueColumnCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bFormLoaded)
            {
                for (int i = 0; i < schema.Rows.Count; i++)
                {
                    if (schema.Rows[i].Field<string>("ColumnName") == valueColumnCombo.Text)
                    {
                        valueColumnTypeTextBox.Text = schema.Rows[i].Field<Type>("DataType").ToString();
                        valueLengthTextBox.Text = schema.Rows[i].Field<int>("ColumnSize").ToString();
                        valueAllowNullsCheckBox.Checked = schema.Rows[i].Field<bool>("AllowDBNull");
                        break;
                    }
                }
            }
        }

        void GetSchemaSQLite()
        {
            string query;
            Cursor = Cursors.WaitCursor;
            try
            {
                DataTable dt = new DataTable();

                // Add first row
                query = "SELECT * FROM " + tableCombo.Text + " LIMIT 1;";

                using (SQLiteCommand DbCommand = new SQLiteCommand(query, cnnSqlite))
                {
                    using (SQLiteDataReader DbReader = DbCommand.ExecuteReader())
                    {

                        dt = DbReader.GetSchemaTable();
                        DbCommand.Dispose();
                        DbReader.Close();
                        Cursor = Cursors.Default;
                        dataGridView1.DataSource = dt;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Schema Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        void RunSql()
        {
            using (SQLiteCommand sqliteDbCommand = new SQLiteCommand(customQueryTextBox.Text, cnnSqlite))
            {
                using (SQLiteDataReader sqliteDbReader = sqliteDbCommand.ExecuteReader())
                {
                    //sqliteDbCommand.Dispose();
                    //sqliteDbReader.Close();
                }
            }
        }

        void SelectSQLiteData()
        {
            int fCount = 0;
            bool bret = false;
            object[] values = null;
            string strName;
            SQLiteCommand sqliteDbCommand = null;
            SQLiteDataReader sqliteDbReader = null;
            DataTable dt = new DataTable();
            string query = "Select * from " + tableCombo.Text + ";";

            sqliteDbCommand = new SQLiteCommand(query, cnnSqlite);
            sqliteDbReader = sqliteDbCommand.ExecuteReader();
            fCount = sqliteDbReader.FieldCount;

            // Create the rest of the columns
            for (int i = 0; i < fCount; i++)
            {
                // Add first row
                strName = sqliteDbReader.GetName(i);
                sqliteDbReader.GetType();
                dt.Columns.Add(new DataColumn(strName, sqliteDbReader.GetFieldType(i)));
                fCount = sqliteDbReader.FieldCount;
            }
            if (fCount > 0)
            {
                values = new object[fCount];
                bret = true;
                while (bret)
                {
                    if (bret = sqliteDbReader.Read())
                    {
                        sqliteDbReader.GetValues(values);
                        dt.Rows.Add(values);
                    }
                }
                dataGridView1.DataSource = dt;
            }
            sqliteDbCommand.Dispose();
            sqliteDbReader.Close();
        }

        private void dbTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSqliteFileNames();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            resultsForm = new ResultsForm();
            resultsForm.Show();
            resultsForm.AddLine("----Query to be processed");
            for (int i=0;i<customQueryTextBox.Lines.Count();i++)
                resultsForm.AddLine(customQueryTextBox.Lines[i].ToString());
            resultsForm.AddLine("----End Query to be processed");
            resultsForm.AddLine("");
            BackupDatabaseFiles();
            for (int i = 0; i < targetTVMListBox.Items.Count; i++)
            {
                resultsForm.AddLine((i + 1).ToString() + ". Updating TVM Database: " + targetTVMListBox.Items[i].ToString());
                try
                {
                    cnnSqlite.Close();
                    cnnSqlite = null;
                    ConnectToDatabase(targetTVMListBox.Items[i].ToString());
                    RunSql();
                }
                catch (Exception ex)
                {
                    resultsForm.AddLine("---ERROR: " + ex.ToString());
                    return;
                }
                resultsForm.AddLine("---Sucess");
            }
            resultsForm.AddLine("----Done!");
        }

        private void BackupDatabaseFiles()
        {
            if (backupCheckBox.Checked)
            {
                string folder = databaseFolderPathTextBox.Text + "\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
                Directory.CreateDirectory(folder);
                foreach (object item in targetTVMListBox.Items)
                {
                    string filePath = item.ToString();
                    string filename = Path.GetFileName(filePath);
                    resultsForm.AddLine("----Backing up file [" + filePath + "] To [" + folder + "\\" + filename+"]");
                    try
                    {
                        File.Copy(filePath, folder + "\\" + filename);
                    }
                    catch (Exception ex)
                    {
                        resultsForm.AddLine("---ERROR: " + ex.ToString());
                    }
                }
            }
        }
    }
}
