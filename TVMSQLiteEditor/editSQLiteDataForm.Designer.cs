﻿namespace TVMSQLiteEditor
{
    partial class editSQLiteDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(editSQLiteDataForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label11 = new System.Windows.Forms.Label();
            this.customQueryTextBox = new System.Windows.Forms.TextBox();
            this.generateSQLButton = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.valueLengthTextBox = new System.Windows.Forms.TextBox();
            this.valueColumnTypeTextBox = new System.Windows.Forms.TextBox();
            this.valueAllowNullsCheckBox = new System.Windows.Forms.CheckBox();
            this.valueColumnCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.keyLengthTextBox = new System.Windows.Forms.TextBox();
            this.keyColumnTypeTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.keyAllowNullsCheckBox = new System.Windows.Forms.CheckBox();
            this.keyColumnCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.browseFolderPathButton = new System.Windows.Forms.Button();
            this.databaseFolderPathTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dbTypeCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.viewSchemaButton = new System.Windows.Forms.Button();
            this.backupCheckBox = new System.Windows.Forms.CheckBox();
            this.viewDataButton = new System.Windows.Forms.Button();
            this.tableCombo = new System.Windows.Forms.ComboBox();
            this.databaseNameCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.targetTVMListBox = new System.Windows.Forms.ListBox();
            this.removeAllButton = new System.Windows.Forms.Button();
            this.addAllButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.addItem = new System.Windows.Forms.Button();
            this.sourceTVMListBox = new System.Windows.Forms.ListBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.customQueryTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.generateSQLButton);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox6);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox5);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.closeButton);
            this.splitContainer1.Panel1.Controls.Add(this.updateButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(989, 608);
            this.splitContainer1.SplitterDistance = 343;
            this.splitContainer1.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Query:";
            // 
            // customQueryTextBox
            // 
            this.customQueryTextBox.Location = new System.Drawing.Point(65, 181);
            this.customQueryTextBox.Multiline = true;
            this.customQueryTextBox.Name = "customQueryTextBox";
            this.customQueryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.customQueryTextBox.Size = new System.Drawing.Size(888, 65);
            this.customQueryTextBox.TabIndex = 0;
            // 
            // generateSQLButton
            // 
            this.generateSQLButton.Location = new System.Drawing.Point(815, 280);
            this.generateSQLButton.Name = "generateSQLButton";
            this.generateSQLButton.Size = new System.Drawing.Size(156, 23);
            this.generateSQLButton.TabIndex = 30;
            this.generateSQLButton.Text = "Generate SQL Statement";
            this.generateSQLButton.UseVisualStyleBackColor = true;
            this.generateSQLButton.Click += new System.EventHandler(this.generateSQLButton_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.valueLengthTextBox);
            this.groupBox6.Controls.Add(this.valueColumnTypeTextBox);
            this.groupBox6.Controls.Add(this.valueAllowNullsCheckBox);
            this.groupBox6.Controls.Add(this.valueColumnCombo);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.valueTextBox);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Location = new System.Drawing.Point(422, 252);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(387, 86);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Value Column";
            // 
            // valueLengthTextBox
            // 
            this.valueLengthTextBox.Location = new System.Drawing.Point(220, 40);
            this.valueLengthTextBox.Name = "valueLengthTextBox";
            this.valueLengthTextBox.ReadOnly = true;
            this.valueLengthTextBox.Size = new System.Drawing.Size(68, 20);
            this.valueLengthTextBox.TabIndex = 33;
            // 
            // valueColumnTypeTextBox
            // 
            this.valueColumnTypeTextBox.Location = new System.Drawing.Point(54, 40);
            this.valueColumnTypeTextBox.Name = "valueColumnTypeTextBox";
            this.valueColumnTypeTextBox.ReadOnly = true;
            this.valueColumnTypeTextBox.Size = new System.Drawing.Size(100, 20);
            this.valueColumnTypeTextBox.TabIndex = 26;
            // 
            // valueAllowNullsCheckBox
            // 
            this.valueAllowNullsCheckBox.AutoSize = true;
            this.valueAllowNullsCheckBox.Enabled = false;
            this.valueAllowNullsCheckBox.Location = new System.Drawing.Point(304, 41);
            this.valueAllowNullsCheckBox.Name = "valueAllowNullsCheckBox";
            this.valueAllowNullsCheckBox.Size = new System.Drawing.Size(77, 17);
            this.valueAllowNullsCheckBox.TabIndex = 25;
            this.valueAllowNullsCheckBox.Text = "Allow Nulls";
            this.valueAllowNullsCheckBox.UseVisualStyleBackColor = true;
            // 
            // valueColumnCombo
            // 
            this.valueColumnCombo.FormattingEnabled = true;
            this.valueColumnCombo.Location = new System.Drawing.Point(54, 15);
            this.valueColumnCombo.Name = "valueColumnCombo";
            this.valueColumnCombo.Size = new System.Drawing.Size(323, 21);
            this.valueColumnCombo.Sorted = true;
            this.valueColumnCombo.TabIndex = 20;
            this.valueColumnCombo.SelectedIndexChanged += new System.EventHandler(this.valueColumnCombo_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(171, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Length:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Type:";
            // 
            // valueTextBox
            // 
            this.valueTextBox.Location = new System.Drawing.Point(54, 63);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.Size = new System.Drawing.Size(323, 20);
            this.valueTextBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Value:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.keyLengthTextBox);
            this.groupBox5.Controls.Add(this.keyColumnTypeTextBox);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.keyTextBox);
            this.groupBox5.Controls.Add(this.keyAllowNullsCheckBox);
            this.groupBox5.Controls.Add(this.keyColumnCombo);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Location = new System.Drawing.Point(18, 252);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(387, 86);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Key Column";
            // 
            // keyLengthTextBox
            // 
            this.keyLengthTextBox.Location = new System.Drawing.Point(204, 40);
            this.keyLengthTextBox.Name = "keyLengthTextBox";
            this.keyLengthTextBox.ReadOnly = true;
            this.keyLengthTextBox.Size = new System.Drawing.Size(68, 20);
            this.keyLengthTextBox.TabIndex = 32;
            // 
            // keyColumnTypeTextBox
            // 
            this.keyColumnTypeTextBox.Location = new System.Drawing.Point(51, 40);
            this.keyColumnTypeTextBox.Name = "keyColumnTypeTextBox";
            this.keyColumnTypeTextBox.ReadOnly = true;
            this.keyColumnTypeTextBox.Size = new System.Drawing.Size(100, 20);
            this.keyColumnTypeTextBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Key:";
            // 
            // keyTextBox
            // 
            this.keyTextBox.Location = new System.Drawing.Point(51, 63);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(323, 20);
            this.keyTextBox.TabIndex = 20;
            // 
            // keyAllowNullsCheckBox
            // 
            this.keyAllowNullsCheckBox.AutoSize = true;
            this.keyAllowNullsCheckBox.Enabled = false;
            this.keyAllowNullsCheckBox.Location = new System.Drawing.Point(297, 42);
            this.keyAllowNullsCheckBox.Name = "keyAllowNullsCheckBox";
            this.keyAllowNullsCheckBox.Size = new System.Drawing.Size(77, 17);
            this.keyAllowNullsCheckBox.TabIndex = 18;
            this.keyAllowNullsCheckBox.Text = "Allow Nulls";
            this.keyAllowNullsCheckBox.UseVisualStyleBackColor = true;
            // 
            // keyColumnCombo
            // 
            this.keyColumnCombo.FormattingEnabled = true;
            this.keyColumnCombo.Location = new System.Drawing.Point(51, 15);
            this.keyColumnCombo.Name = "keyColumnCombo";
            this.keyColumnCombo.Size = new System.Drawing.Size(323, 21);
            this.keyColumnCombo.Sorted = true;
            this.keyColumnCombo.TabIndex = 8;
            this.keyColumnCombo.SelectedIndexChanged += new System.EventHandler(this.keyColumnCombo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(164, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Length:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Type:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.browseFolderPathButton);
            this.groupBox3.Controls.Add(this.databaseFolderPathTextBox);
            this.groupBox3.Location = new System.Drawing.Point(18, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(387, 57);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Database Folder Path:";
            // 
            // browseFolderPathButton
            // 
            this.browseFolderPathButton.Location = new System.Drawing.Point(310, 18);
            this.browseFolderPathButton.Name = "browseFolderPathButton";
            this.browseFolderPathButton.Size = new System.Drawing.Size(64, 23);
            this.browseFolderPathButton.TabIndex = 10;
            this.browseFolderPathButton.Text = "Browse ...";
            this.browseFolderPathButton.UseVisualStyleBackColor = true;
            this.browseFolderPathButton.Click += new System.EventHandler(this.browseFolderPathButton_Click);
            // 
            // databaseFolderPathTextBox
            // 
            this.databaseFolderPathTextBox.Location = new System.Drawing.Point(12, 18);
            this.databaseFolderPathTextBox.Name = "databaseFolderPathTextBox";
            this.databaseFolderPathTextBox.Size = new System.Drawing.Size(292, 20);
            this.databaseFolderPathTextBox.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dbTypeCombo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.viewSchemaButton);
            this.groupBox2.Controls.Add(this.backupCheckBox);
            this.groupBox2.Controls.Add(this.viewDataButton);
            this.groupBox2.Controls.Add(this.tableCombo);
            this.groupBox2.Controls.Add(this.databaseNameCombo);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(18, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(387, 107);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // dbTypeCombo
            // 
            this.dbTypeCombo.FormattingEnabled = true;
            this.dbTypeCombo.Items.AddRange(new object[] {
            "gfi.sqlite",
            "cfg.sqlite"});
            this.dbTypeCombo.Location = new System.Drawing.Point(66, 77);
            this.dbTypeCombo.Name = "dbTypeCombo";
            this.dbTypeCombo.Size = new System.Drawing.Size(83, 21);
            this.dbTypeCombo.TabIndex = 29;
            this.dbTypeCombo.SelectedIndexChanged += new System.EventHandler(this.dbTypeCombo_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 81);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Type:";
            // 
            // viewSchemaButton
            // 
            this.viewSchemaButton.Location = new System.Drawing.Point(287, 18);
            this.viewSchemaButton.Name = "viewSchemaButton";
            this.viewSchemaButton.Size = new System.Drawing.Size(87, 23);
            this.viewSchemaButton.TabIndex = 13;
            this.viewSchemaButton.Text = "View Schema";
            this.viewSchemaButton.UseVisualStyleBackColor = true;
            this.viewSchemaButton.Click += new System.EventHandler(this.viewSchemaButton_Click);
            // 
            // backupCheckBox
            // 
            this.backupCheckBox.AutoSize = true;
            this.backupCheckBox.Location = new System.Drawing.Point(159, 79);
            this.backupCheckBox.Name = "backupCheckBox";
            this.backupCheckBox.Size = new System.Drawing.Size(221, 17);
            this.backupCheckBox.TabIndex = 27;
            this.backupCheckBox.Text = "Backup existing database prior to Update";
            this.backupCheckBox.UseVisualStyleBackColor = true;
            // 
            // viewDataButton
            // 
            this.viewDataButton.Location = new System.Drawing.Point(287, 47);
            this.viewDataButton.Name = "viewDataButton";
            this.viewDataButton.Size = new System.Drawing.Size(87, 23);
            this.viewDataButton.TabIndex = 12;
            this.viewDataButton.Text = "View Data";
            this.viewDataButton.UseVisualStyleBackColor = true;
            this.viewDataButton.Click += new System.EventHandler(this.viewDataButton_Click);
            // 
            // tableCombo
            // 
            this.tableCombo.FormattingEnabled = true;
            this.tableCombo.Location = new System.Drawing.Point(66, 48);
            this.tableCombo.Name = "tableCombo";
            this.tableCombo.Size = new System.Drawing.Size(206, 21);
            this.tableCombo.TabIndex = 7;
            this.tableCombo.SelectedIndexChanged += new System.EventHandler(this.tableCombo_SelectedIndexChanged);
            // 
            // databaseNameCombo
            // 
            this.databaseNameCombo.FormattingEnabled = true;
            this.databaseNameCombo.Location = new System.Drawing.Point(66, 19);
            this.databaseNameCombo.Name = "databaseNameCombo";
            this.databaseNameCombo.Size = new System.Drawing.Size(206, 21);
            this.databaseNameCombo.Sorted = true;
            this.databaseNameCombo.TabIndex = 6;
            this.databaseNameCombo.SelectedIndexChanged += new System.EventHandler(this.databaseNameCombo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Table:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Database:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.targetTVMListBox);
            this.groupBox1.Controls.Add(this.removeAllButton);
            this.groupBox1.Controls.Add(this.addAllButton);
            this.groupBox1.Controls.Add(this.removeButton);
            this.groupBox1.Controls.Add(this.addItem);
            this.groupBox1.Controls.Add(this.sourceTVMListBox);
            this.groupBox1.Location = new System.Drawing.Point(422, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(547, 172);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TVM Update List";
            // 
            // targetTVMListBox
            // 
            this.targetTVMListBox.AllowDrop = true;
            this.targetTVMListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.targetTVMListBox.FormattingEnabled = true;
            this.targetTVMListBox.ItemHeight = 16;
            this.targetTVMListBox.Location = new System.Drawing.Point(331, 19);
            this.targetTVMListBox.Name = "targetTVMListBox";
            this.targetTVMListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.targetTVMListBox.Size = new System.Drawing.Size(200, 148);
            this.targetTVMListBox.Sorted = true;
            this.targetTVMListBox.TabIndex = 38;
            // 
            // removeAllButton
            // 
            this.removeAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeAllButton.Location = new System.Drawing.Point(229, 143);
            this.removeAllButton.Name = "removeAllButton";
            this.removeAllButton.Size = new System.Drawing.Size(88, 20);
            this.removeAllButton.TabIndex = 37;
            this.removeAllButton.Text = "<- Remove All";
            this.removeAllButton.UseVisualStyleBackColor = true;
            this.removeAllButton.Click += new System.EventHandler(this.removeAllButton_Click);
            // 
            // addAllButton
            // 
            this.addAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAllButton.Location = new System.Drawing.Point(229, 42);
            this.addAllButton.Name = "addAllButton";
            this.addAllButton.Size = new System.Drawing.Size(88, 20);
            this.addAllButton.TabIndex = 36;
            this.addAllButton.Text = "Add All ->";
            this.addAllButton.UseVisualStyleBackColor = true;
            this.addAllButton.Click += new System.EventHandler(this.addAllButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(229, 117);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(88, 20);
            this.removeButton.TabIndex = 34;
            this.removeButton.Text = "<- Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addItem
            // 
            this.addItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItem.Location = new System.Drawing.Point(229, 19);
            this.addItem.Name = "addItem";
            this.addItem.Size = new System.Drawing.Size(88, 20);
            this.addItem.TabIndex = 33;
            this.addItem.Text = "Add ->";
            this.addItem.UseVisualStyleBackColor = true;
            this.addItem.Click += new System.EventHandler(this.addItem_Click);
            // 
            // sourceTVMListBox
            // 
            this.sourceTVMListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceTVMListBox.FormattingEnabled = true;
            this.sourceTVMListBox.ItemHeight = 16;
            this.sourceTVMListBox.Items.AddRange(new object[] {
            "TVM-100",
            "TVM-101",
            "TVM-102",
            "TVM-103",
            "TVM-104",
            "TVM-105",
            "TVM-106",
            "TVM-108",
            "TVM-109",
            "TVM-110",
            "TVM-111",
            "TVM-112",
            "TVM-113",
            "TVM-114",
            "TVM-115",
            "TVM-116",
            "TVM-117",
            "TVM-118",
            "TVM-119",
            "TVM-120",
            "TVM-121"});
            this.sourceTVMListBox.Location = new System.Drawing.Point(14, 19);
            this.sourceTVMListBox.Name = "sourceTVMListBox";
            this.sourceTVMListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.sourceTVMListBox.Size = new System.Drawing.Size(200, 148);
            this.sourceTVMListBox.Sorted = true;
            this.sourceTVMListBox.TabIndex = 26;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(896, 310);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 21;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(815, 310);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 20;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(989, 237);
            this.dataGridView1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(989, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editSQLiteDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 608);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "editSQLiteDataForm";
            this.Text = "TVM SQLite Database Editor";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button browseFolderPathButton;
        private System.Windows.Forms.TextBox databaseFolderPathTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button viewSchemaButton;
        private System.Windows.Forms.Button viewDataButton;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.ComboBox keyColumnCombo;
        private System.Windows.Forms.ComboBox tableCombo;
        private System.Windows.Forms.ComboBox databaseNameCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox keyAllowNullsCheckBox;
        private System.Windows.Forms.CheckBox backupCheckBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox valueAllowNullsCheckBox;
        private System.Windows.Forms.ComboBox valueColumnCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox customQueryTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ListBox targetTVMListBox;
        private System.Windows.Forms.Button removeAllButton;
        private System.Windows.Forms.Button addAllButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.ListBox sourceTVMListBox;
        private System.Windows.Forms.Button generateSQLButton;
        private System.Windows.Forms.TextBox valueColumnTypeTextBox;
        private System.Windows.Forms.TextBox keyColumnTypeTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox dbTypeCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox valueLengthTextBox;
        private System.Windows.Forms.TextBox keyLengthTextBox;

    }
}