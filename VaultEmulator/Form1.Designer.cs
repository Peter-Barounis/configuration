﻿namespace VaultEmulator
{
   partial class Vault
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.textBoxVaultLog1 = new System.Windows.Forms.TextBox();
            this.btnInsertVault = new System.Windows.Forms.Button();
            this.btnRemoveVault = new System.Windows.Forms.Button();
            this.btnInsertBin = new System.Windows.Forms.Button();
            this.btnRemoveBin = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbLogEnable1 = new System.Windows.Forms.CheckBox();
            this.cbLogEnable2 = new System.Windows.Forms.CheckBox();
            this.cbLogEnable3 = new System.Windows.Forms.CheckBox();
            this.cbLogEnable4 = new System.Windows.Forms.CheckBox();
            this.txtCbxID4 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBinID5 = new System.Windows.Forms.TextBox();
            this.txtBinID4 = new System.Windows.Forms.TextBox();
            this.txtBinID3 = new System.Windows.Forms.TextBox();
            this.txtBinID2 = new System.Windows.Forms.TextBox();
            this.txtBinID1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCbxID5 = new System.Windows.Forms.TextBox();
            this.cbVaultID5 = new System.Windows.Forms.ComboBox();
            this.cbLogEnable5 = new System.Windows.Forms.CheckBox();
            this.cbControllerEnable5 = new System.Windows.Forms.CheckBox();
            this.cbComPort5 = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCbxID3 = new System.Windows.Forms.TextBox();
            this.txtCbxID2 = new System.Windows.Forms.TextBox();
            this.txtCbxID1 = new System.Windows.Forms.TextBox();
            this.cbVaultID4 = new System.Windows.Forms.ComboBox();
            this.cbVaultID3 = new System.Windows.Forms.ComboBox();
            this.cbVaultID2 = new System.Windows.Forms.ComboBox();
            this.cbVaultID1 = new System.Windows.Forms.ComboBox();
            this.cbControllerEnable4 = new System.Windows.Forms.CheckBox();
            this.cbControllerEnable3 = new System.Windows.Forms.CheckBox();
            this.cbControllerEnable2 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbControllerEnable1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbComPort4 = new System.Windows.Forms.ComboBox();
            this.cbComPort3 = new System.Windows.Forms.ComboBox();
            this.cbComPort2 = new System.Windows.Forms.ComboBox();
            this.cbComPort1 = new System.Windows.Forms.ComboBox();
            this.radioBtnController4 = new System.Windows.Forms.RadioButton();
            this.radioBtnController3 = new System.Windows.Forms.RadioButton();
            this.radioBtnController2 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radioBtnController1 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.cbValutsPerController = new System.Windows.Forms.ComboBox();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.tabControlVaultLogs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxVaultLog2 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxVaultLog3 = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBoxVaultLog4 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBoxVaultLog5 = new System.Windows.Forms.TextBox();
            this.btnClearAllLogs = new System.Windows.Forms.Button();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.cbEnableDebug = new System.Windows.Forms.CheckBox();
            this.sendMisread = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabControlVaultLogs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxVaultLog1
            // 
            this.textBoxVaultLog1.Location = new System.Drawing.Point(3, 3);
            this.textBoxVaultLog1.Multiline = true;
            this.textBoxVaultLog1.Name = "textBoxVaultLog1";
            this.textBoxVaultLog1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxVaultLog1.Size = new System.Drawing.Size(291, 295);
            this.textBoxVaultLog1.TabIndex = 1;
            // 
            // btnInsertVault
            // 
            this.btnInsertVault.Location = new System.Drawing.Point(86, 291);
            this.btnInsertVault.Name = "btnInsertVault";
            this.btnInsertVault.Size = new System.Drawing.Size(83, 23);
            this.btnInsertVault.TabIndex = 3;
            this.btnInsertVault.Text = "Insert CBX";
            this.btnInsertVault.UseVisualStyleBackColor = true;
            this.btnInsertVault.Click += new System.EventHandler(this.btnInsertVault_Click);
            // 
            // btnRemoveVault
            // 
            this.btnRemoveVault.Location = new System.Drawing.Point(86, 320);
            this.btnRemoveVault.Name = "btnRemoveVault";
            this.btnRemoveVault.Size = new System.Drawing.Size(83, 23);
            this.btnRemoveVault.TabIndex = 4;
            this.btnRemoveVault.Text = "Remove CBX";
            this.btnRemoveVault.UseVisualStyleBackColor = true;
            this.btnRemoveVault.Click += new System.EventHandler(this.btnRemoveVault_Click);
            // 
            // btnInsertBin
            // 
            this.btnInsertBin.Location = new System.Drawing.Point(3, 291);
            this.btnInsertBin.Name = "btnInsertBin";
            this.btnInsertBin.Size = new System.Drawing.Size(83, 23);
            this.btnInsertBin.TabIndex = 5;
            this.btnInsertBin.Text = "Insert Bin";
            this.btnInsertBin.UseVisualStyleBackColor = true;
            this.btnInsertBin.Click += new System.EventHandler(this.btnInsertBin_Click);
            // 
            // btnRemoveBin
            // 
            this.btnRemoveBin.Location = new System.Drawing.Point(3, 320);
            this.btnRemoveBin.Name = "btnRemoveBin";
            this.btnRemoveBin.Size = new System.Drawing.Size(83, 23);
            this.btnRemoveBin.TabIndex = 6;
            this.btnRemoveBin.Text = "Remove Bin";
            this.btnRemoveBin.UseVisualStyleBackColor = true;
            this.btnRemoveBin.Click += new System.EventHandler(this.btnRemoveBin_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(420, 320);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(85, 23);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(169, 291);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(83, 23);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(169, 320);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(83, 23);
            this.btnDisconnect.TabIndex = 9;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(270, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Vault ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Log";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(341, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Cashbox ID";
            // 
            // cbLogEnable1
            // 
            this.cbLogEnable1.AutoSize = true;
            this.cbLogEnable1.Checked = true;
            this.cbLogEnable1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogEnable1.Location = new System.Drawing.Point(223, 69);
            this.cbLogEnable1.Name = "cbLogEnable1";
            this.cbLogEnable1.Size = new System.Drawing.Size(15, 14);
            this.cbLogEnable1.TabIndex = 18;
            this.cbLogEnable1.UseVisualStyleBackColor = true;
            this.cbLogEnable1.CheckedChanged += new System.EventHandler(this.cbLogEnable1_CheckedChanged);
            // 
            // cbLogEnable2
            // 
            this.cbLogEnable2.AutoSize = true;
            this.cbLogEnable2.Checked = true;
            this.cbLogEnable2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogEnable2.Location = new System.Drawing.Point(223, 104);
            this.cbLogEnable2.Name = "cbLogEnable2";
            this.cbLogEnable2.Size = new System.Drawing.Size(15, 14);
            this.cbLogEnable2.TabIndex = 19;
            this.cbLogEnable2.UseVisualStyleBackColor = true;
            this.cbLogEnable2.CheckedChanged += new System.EventHandler(this.cbLogEnable2_CheckedChanged);
            // 
            // cbLogEnable3
            // 
            this.cbLogEnable3.AutoSize = true;
            this.cbLogEnable3.Checked = true;
            this.cbLogEnable3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogEnable3.Location = new System.Drawing.Point(223, 140);
            this.cbLogEnable3.Name = "cbLogEnable3";
            this.cbLogEnable3.Size = new System.Drawing.Size(15, 14);
            this.cbLogEnable3.TabIndex = 20;
            this.cbLogEnable3.UseVisualStyleBackColor = true;
            this.cbLogEnable3.CheckedChanged += new System.EventHandler(this.cbLogEnable3_CheckedChanged);
            // 
            // cbLogEnable4
            // 
            this.cbLogEnable4.AutoSize = true;
            this.cbLogEnable4.Checked = true;
            this.cbLogEnable4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogEnable4.Location = new System.Drawing.Point(223, 177);
            this.cbLogEnable4.Name = "cbLogEnable4";
            this.cbLogEnable4.Size = new System.Drawing.Size(15, 14);
            this.cbLogEnable4.TabIndex = 21;
            this.cbLogEnable4.UseVisualStyleBackColor = true;
            this.cbLogEnable4.CheckedChanged += new System.EventHandler(this.cbLogEnable4_CheckedChanged);
            // 
            // txtCbxID4
            // 
            this.txtCbxID4.Location = new System.Drawing.Point(344, 174);
            this.txtCbxID4.Name = "txtCbxID4";
            this.txtCbxID4.Size = new System.Drawing.Size(55, 20);
            this.txtCbxID4.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBinID5);
            this.groupBox1.Controls.Add(this.txtBinID4);
            this.groupBox1.Controls.Add(this.txtBinID3);
            this.groupBox1.Controls.Add(this.txtBinID2);
            this.groupBox1.Controls.Add(this.txtBinID1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtCbxID5);
            this.groupBox1.Controls.Add(this.cbVaultID5);
            this.groupBox1.Controls.Add(this.cbLogEnable5);
            this.groupBox1.Controls.Add(this.cbControllerEnable5);
            this.groupBox1.Controls.Add(this.cbComPort5);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtCbxID4);
            this.groupBox1.Controls.Add(this.txtCbxID3);
            this.groupBox1.Controls.Add(this.txtCbxID2);
            this.groupBox1.Controls.Add(this.txtCbxID1);
            this.groupBox1.Controls.Add(this.cbVaultID4);
            this.groupBox1.Controls.Add(this.cbVaultID3);
            this.groupBox1.Controls.Add(this.cbVaultID2);
            this.groupBox1.Controls.Add(this.cbVaultID1);
            this.groupBox1.Controls.Add(this.cbLogEnable4);
            this.groupBox1.Controls.Add(this.cbLogEnable3);
            this.groupBox1.Controls.Add(this.cbLogEnable2);
            this.groupBox1.Controls.Add(this.cbLogEnable1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbControllerEnable4);
            this.groupBox1.Controls.Add(this.cbControllerEnable3);
            this.groupBox1.Controls.Add(this.cbControllerEnable2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbControllerEnable1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbComPort4);
            this.groupBox1.Controls.Add(this.cbComPort3);
            this.groupBox1.Controls.Add(this.cbComPort2);
            this.groupBox1.Controls.Add(this.cbComPort1);
            this.groupBox1.Controls.Add(this.radioBtnController4);
            this.groupBox1.Controls.Add(this.radioBtnController3);
            this.groupBox1.Controls.Add(this.radioBtnController2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.radioBtnController1);
            this.groupBox1.Location = new System.Drawing.Point(7, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(496, 250);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configure Vaults/Bins";
            // 
            // txtBinID5
            // 
            this.txtBinID5.Location = new System.Drawing.Point(426, 212);
            this.txtBinID5.Name = "txtBinID5";
            this.txtBinID5.Size = new System.Drawing.Size(55, 20);
            this.txtBinID5.TabIndex = 43;
            this.txtBinID5.Text = "21";
            // 
            // txtBinID4
            // 
            this.txtBinID4.Location = new System.Drawing.Point(426, 174);
            this.txtBinID4.Name = "txtBinID4";
            this.txtBinID4.Size = new System.Drawing.Size(55, 20);
            this.txtBinID4.TabIndex = 42;
            this.txtBinID4.Text = "21";
            // 
            // txtBinID3
            // 
            this.txtBinID3.Location = new System.Drawing.Point(426, 137);
            this.txtBinID3.Name = "txtBinID3";
            this.txtBinID3.Size = new System.Drawing.Size(55, 20);
            this.txtBinID3.TabIndex = 41;
            this.txtBinID3.Text = "21";
            // 
            // txtBinID2
            // 
            this.txtBinID2.Location = new System.Drawing.Point(426, 101);
            this.txtBinID2.Name = "txtBinID2";
            this.txtBinID2.Size = new System.Drawing.Size(55, 20);
            this.txtBinID2.TabIndex = 40;
            this.txtBinID2.Text = "21";
            // 
            // txtBinID1
            // 
            this.txtBinID1.Location = new System.Drawing.Point(426, 66);
            this.txtBinID1.Name = "txtBinID1";
            this.txtBinID1.Size = new System.Drawing.Size(55, 20);
            this.txtBinID1.TabIndex = 39;
            this.txtBinID1.Text = "21";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(437, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Bin ID";
            // 
            // txtCbxID5
            // 
            this.txtCbxID5.Location = new System.Drawing.Point(344, 212);
            this.txtCbxID5.Name = "txtCbxID5";
            this.txtCbxID5.Size = new System.Drawing.Size(55, 20);
            this.txtCbxID5.TabIndex = 37;
            // 
            // cbVaultID5
            // 
            this.cbVaultID5.FormattingEnabled = true;
            this.cbVaultID5.Location = new System.Drawing.Point(269, 212);
            this.cbVaultID5.Name = "cbVaultID5";
            this.cbVaultID5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbVaultID5.Size = new System.Drawing.Size(45, 21);
            this.cbVaultID5.TabIndex = 36;
            this.cbVaultID5.SelectedIndexChanged += new System.EventHandler(this.cbVaultID5_SelectedIndexChanged);
            // 
            // cbLogEnable5
            // 
            this.cbLogEnable5.AutoSize = true;
            this.cbLogEnable5.Checked = true;
            this.cbLogEnable5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLogEnable5.Location = new System.Drawing.Point(223, 215);
            this.cbLogEnable5.Name = "cbLogEnable5";
            this.cbLogEnable5.Size = new System.Drawing.Size(15, 14);
            this.cbLogEnable5.TabIndex = 35;
            this.cbLogEnable5.UseVisualStyleBackColor = true;
            this.cbLogEnable5.CheckedChanged += new System.EventHandler(this.cbLogEnable5_CheckedChanged);
            // 
            // cbControllerEnable5
            // 
            this.cbControllerEnable5.AutoSize = true;
            this.cbControllerEnable5.Location = new System.Drawing.Point(173, 215);
            this.cbControllerEnable5.Name = "cbControllerEnable5";
            this.cbControllerEnable5.Size = new System.Drawing.Size(15, 14);
            this.cbControllerEnable5.TabIndex = 34;
            this.cbControllerEnable5.UseVisualStyleBackColor = true;
            // 
            // cbComPort5
            // 
            this.cbComPort5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComPort5.FormattingEnabled = true;
            this.cbComPort5.Location = new System.Drawing.Point(75, 212);
            this.cbComPort5.Name = "cbComPort5";
            this.cbComPort5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbComPort5.Size = new System.Drawing.Size(80, 21);
            this.cbComPort5.TabIndex = 33;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 214);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButton1.Size = new System.Drawing.Size(31, 17);
            this.radioButton1.TabIndex = 32;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "5";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(221, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Enable";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(163, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Controller";
            // 
            // txtCbxID3
            // 
            this.txtCbxID3.Location = new System.Drawing.Point(344, 137);
            this.txtCbxID3.Name = "txtCbxID3";
            this.txtCbxID3.Size = new System.Drawing.Size(55, 20);
            this.txtCbxID3.TabIndex = 28;
            // 
            // txtCbxID2
            // 
            this.txtCbxID2.Location = new System.Drawing.Point(344, 101);
            this.txtCbxID2.Name = "txtCbxID2";
            this.txtCbxID2.Size = new System.Drawing.Size(55, 20);
            this.txtCbxID2.TabIndex = 27;
            // 
            // txtCbxID1
            // 
            this.txtCbxID1.Location = new System.Drawing.Point(344, 66);
            this.txtCbxID1.Name = "txtCbxID1";
            this.txtCbxID1.Size = new System.Drawing.Size(55, 20);
            this.txtCbxID1.TabIndex = 26;
            this.txtCbxID1.Text = "21";
            // 
            // cbVaultID4
            // 
            this.cbVaultID4.FormattingEnabled = true;
            this.cbVaultID4.Location = new System.Drawing.Point(269, 174);
            this.cbVaultID4.Name = "cbVaultID4";
            this.cbVaultID4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbVaultID4.Size = new System.Drawing.Size(45, 21);
            this.cbVaultID4.TabIndex = 25;
            this.cbVaultID4.SelectedIndexChanged += new System.EventHandler(this.cbVaultID4_SelectedIndexChanged);
            // 
            // cbVaultID3
            // 
            this.cbVaultID3.FormattingEnabled = true;
            this.cbVaultID3.Location = new System.Drawing.Point(269, 137);
            this.cbVaultID3.Name = "cbVaultID3";
            this.cbVaultID3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbVaultID3.Size = new System.Drawing.Size(45, 21);
            this.cbVaultID3.TabIndex = 24;
            this.cbVaultID3.SelectedIndexChanged += new System.EventHandler(this.cbVaultID3_SelectedIndexChanged);
            // 
            // cbVaultID2
            // 
            this.cbVaultID2.FormattingEnabled = true;
            this.cbVaultID2.Location = new System.Drawing.Point(269, 101);
            this.cbVaultID2.Name = "cbVaultID2";
            this.cbVaultID2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbVaultID2.Size = new System.Drawing.Size(45, 21);
            this.cbVaultID2.TabIndex = 23;
            this.cbVaultID2.SelectedIndexChanged += new System.EventHandler(this.cbVaultID2_SelectedIndexChanged);
            // 
            // cbVaultID1
            // 
            this.cbVaultID1.FormattingEnabled = true;
            this.cbVaultID1.Location = new System.Drawing.Point(269, 66);
            this.cbVaultID1.Name = "cbVaultID1";
            this.cbVaultID1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbVaultID1.Size = new System.Drawing.Size(45, 21);
            this.cbVaultID1.TabIndex = 22;
            this.cbVaultID1.Text = "1";
            this.cbVaultID1.SelectedIndexChanged += new System.EventHandler(this.cbVaultID1_SelectedIndexChanged);
            // 
            // cbControllerEnable4
            // 
            this.cbControllerEnable4.AutoSize = true;
            this.cbControllerEnable4.Location = new System.Drawing.Point(173, 177);
            this.cbControllerEnable4.Name = "cbControllerEnable4";
            this.cbControllerEnable4.Size = new System.Drawing.Size(15, 14);
            this.cbControllerEnable4.TabIndex = 14;
            this.cbControllerEnable4.UseVisualStyleBackColor = true;
            // 
            // cbControllerEnable3
            // 
            this.cbControllerEnable3.AutoSize = true;
            this.cbControllerEnable3.Location = new System.Drawing.Point(173, 140);
            this.cbControllerEnable3.Name = "cbControllerEnable3";
            this.cbControllerEnable3.Size = new System.Drawing.Size(15, 14);
            this.cbControllerEnable3.TabIndex = 13;
            this.cbControllerEnable3.UseVisualStyleBackColor = true;
            // 
            // cbControllerEnable2
            // 
            this.cbControllerEnable2.AutoSize = true;
            this.cbControllerEnable2.Location = new System.Drawing.Point(173, 104);
            this.cbControllerEnable2.Name = "cbControllerEnable2";
            this.cbControllerEnable2.Size = new System.Drawing.Size(15, 14);
            this.cbControllerEnable2.TabIndex = 12;
            this.cbControllerEnable2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Enable";
            // 
            // cbControllerEnable1
            // 
            this.cbControllerEnable1.AutoSize = true;
            this.cbControllerEnable1.Checked = true;
            this.cbControllerEnable1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbControllerEnable1.Location = new System.Drawing.Point(173, 69);
            this.cbControllerEnable1.Name = "cbControllerEnable1";
            this.cbControllerEnable1.Size = new System.Drawing.Size(15, 14);
            this.cbControllerEnable1.TabIndex = 10;
            this.cbControllerEnable1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "COM Port";
            // 
            // cbComPort4
            // 
            this.cbComPort4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComPort4.FormattingEnabled = true;
            this.cbComPort4.Location = new System.Drawing.Point(75, 174);
            this.cbComPort4.Name = "cbComPort4";
            this.cbComPort4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbComPort4.Size = new System.Drawing.Size(80, 21);
            this.cbComPort4.TabIndex = 8;
            // 
            // cbComPort3
            // 
            this.cbComPort3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComPort3.FormattingEnabled = true;
            this.cbComPort3.Location = new System.Drawing.Point(75, 137);
            this.cbComPort3.Name = "cbComPort3";
            this.cbComPort3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbComPort3.Size = new System.Drawing.Size(80, 21);
            this.cbComPort3.TabIndex = 7;
            // 
            // cbComPort2
            // 
            this.cbComPort2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComPort2.FormattingEnabled = true;
            this.cbComPort2.Location = new System.Drawing.Point(75, 101);
            this.cbComPort2.Name = "cbComPort2";
            this.cbComPort2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbComPort2.Size = new System.Drawing.Size(80, 21);
            this.cbComPort2.TabIndex = 6;
            // 
            // cbComPort1
            // 
            this.cbComPort1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComPort1.FormattingEnabled = true;
            this.cbComPort1.Location = new System.Drawing.Point(75, 66);
            this.cbComPort1.Name = "cbComPort1";
            this.cbComPort1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbComPort1.Size = new System.Drawing.Size(80, 21);
            this.cbComPort1.TabIndex = 5;
            this.cbComPort1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbComPort1_KeyDown);
            this.cbComPort1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbComPort1_KeyPress);
            // 
            // radioBtnController4
            // 
            this.radioBtnController4.AutoSize = true;
            this.radioBtnController4.Location = new System.Drawing.Point(17, 176);
            this.radioBtnController4.Name = "radioBtnController4";
            this.radioBtnController4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnController4.Size = new System.Drawing.Size(31, 17);
            this.radioBtnController4.TabIndex = 4;
            this.radioBtnController4.TabStop = true;
            this.radioBtnController4.Text = "4";
            this.radioBtnController4.UseVisualStyleBackColor = true;
            this.radioBtnController4.CheckedChanged += new System.EventHandler(this.radioBtnController4_CheckedChanged);
            // 
            // radioBtnController3
            // 
            this.radioBtnController3.AutoSize = true;
            this.radioBtnController3.Location = new System.Drawing.Point(17, 139);
            this.radioBtnController3.Name = "radioBtnController3";
            this.radioBtnController3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnController3.Size = new System.Drawing.Size(31, 17);
            this.radioBtnController3.TabIndex = 3;
            this.radioBtnController3.TabStop = true;
            this.radioBtnController3.Text = "3";
            this.radioBtnController3.UseVisualStyleBackColor = true;
            this.radioBtnController3.CheckedChanged += new System.EventHandler(this.radioBtnController3_CheckedChanged);
            // 
            // radioBtnController2
            // 
            this.radioBtnController2.AutoSize = true;
            this.radioBtnController2.Location = new System.Drawing.Point(17, 103);
            this.radioBtnController2.Name = "radioBtnController2";
            this.radioBtnController2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnController2.Size = new System.Drawing.Size(31, 17);
            this.radioBtnController2.TabIndex = 2;
            this.radioBtnController2.TabStop = true;
            this.radioBtnController2.Text = "2";
            this.radioBtnController2.UseVisualStyleBackColor = true;
            this.radioBtnController2.CheckedChanged += new System.EventHandler(this.radioBtnController2_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Controller ID";
            // 
            // radioBtnController1
            // 
            this.radioBtnController1.AutoSize = true;
            this.radioBtnController1.Location = new System.Drawing.Point(17, 68);
            this.radioBtnController1.Name = "radioBtnController1";
            this.radioBtnController1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioBtnController1.Size = new System.Drawing.Size(31, 17);
            this.radioBtnController1.TabIndex = 0;
            this.radioBtnController1.TabStop = true;
            this.radioBtnController1.Text = "1";
            this.radioBtnController1.UseVisualStyleBackColor = true;
            this.radioBtnController1.CheckedChanged += new System.EventHandler(this.radioBtnController1_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Number of Vaults/Controller:";
            // 
            // cbValutsPerController
            // 
            this.cbValutsPerController.DisplayMember = "2";
            this.cbValutsPerController.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbValutsPerController.FormattingEnabled = true;
            this.cbValutsPerController.Items.AddRange(new object[] {
            "2",
            "4"});
            this.cbValutsPerController.Location = new System.Drawing.Point(166, 10);
            this.cbValutsPerController.Name = "cbValutsPerController";
            this.cbValutsPerController.Size = new System.Drawing.Size(121, 21);
            this.cbValutsPerController.TabIndex = 11;
            this.cbValutsPerController.SelectedIndexChanged += new System.EventHandler(this.cbValutsPerController_SelectedIndexChanged);
            this.cbValutsPerController.SelectedValueChanged += new System.EventHandler(this.cbValutsPerController_SelectedValueChanged);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Location = new System.Drawing.Point(252, 291);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(83, 23);
            this.btnClearLog.TabIndex = 12;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // tabControlVaultLogs
            // 
            this.tabControlVaultLogs.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.tabControlVaultLogs.Controls.Add(this.tabPage1);
            this.tabControlVaultLogs.Controls.Add(this.tabPage2);
            this.tabControlVaultLogs.Controls.Add(this.tabPage3);
            this.tabControlVaultLogs.Controls.Add(this.tabPage4);
            this.tabControlVaultLogs.Controls.Add(this.tabPage5);
            this.tabControlVaultLogs.Location = new System.Drawing.Point(515, 10);
            this.tabControlVaultLogs.Name = "tabControlVaultLogs";
            this.tabControlVaultLogs.SelectedIndex = 0;
            this.tabControlVaultLogs.Size = new System.Drawing.Size(303, 333);
            this.tabControlVaultLogs.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxVaultLog1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(295, 307);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxVaultLog2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(295, 307);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxVaultLog2
            // 
            this.textBoxVaultLog2.Location = new System.Drawing.Point(3, 3);
            this.textBoxVaultLog2.Multiline = true;
            this.textBoxVaultLog2.Name = "textBoxVaultLog2";
            this.textBoxVaultLog2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxVaultLog2.Size = new System.Drawing.Size(296, 295);
            this.textBoxVaultLog2.TabIndex = 14;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBoxVaultLog3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(295, 307);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxVaultLog3
            // 
            this.textBoxVaultLog3.Location = new System.Drawing.Point(3, 3);
            this.textBoxVaultLog3.Multiline = true;
            this.textBoxVaultLog3.Name = "textBoxVaultLog3";
            this.textBoxVaultLog3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxVaultLog3.Size = new System.Drawing.Size(289, 299);
            this.textBoxVaultLog3.TabIndex = 15;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.textBoxVaultLog4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(295, 307);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // textBoxVaultLog4
            // 
            this.textBoxVaultLog4.Location = new System.Drawing.Point(3, 3);
            this.textBoxVaultLog4.Multiline = true;
            this.textBoxVaultLog4.Name = "textBoxVaultLog4";
            this.textBoxVaultLog4.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxVaultLog4.Size = new System.Drawing.Size(292, 296);
            this.textBoxVaultLog4.TabIndex = 15;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.textBoxVaultLog5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(295, 307);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBoxVaultLog5
            // 
            this.textBoxVaultLog5.Location = new System.Drawing.Point(4, 3);
            this.textBoxVaultLog5.Multiline = true;
            this.textBoxVaultLog5.Name = "textBoxVaultLog5";
            this.textBoxVaultLog5.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxVaultLog5.Size = new System.Drawing.Size(285, 295);
            this.textBoxVaultLog5.TabIndex = 15;
            // 
            // btnClearAllLogs
            // 
            this.btnClearAllLogs.Location = new System.Drawing.Point(252, 320);
            this.btnClearAllLogs.Name = "btnClearAllLogs";
            this.btnClearAllLogs.Size = new System.Drawing.Size(83, 23);
            this.btnClearAllLogs.TabIndex = 14;
            this.btnClearAllLogs.Text = "Clear All Logs";
            this.btnClearAllLogs.UseVisualStyleBackColor = true;
            this.btnClearAllLogs.Click += new System.EventHandler(this.btnClearAllLogs_Click);
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Location = new System.Drawing.Point(418, 291);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(85, 23);
            this.btnSaveSettings.TabIndex = 15;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // cbEnableDebug
            // 
            this.cbEnableDebug.AutoSize = true;
            this.cbEnableDebug.Location = new System.Drawing.Point(296, 12);
            this.cbEnableDebug.Name = "cbEnableDebug";
            this.cbEnableDebug.Size = new System.Drawing.Size(94, 17);
            this.cbEnableDebug.TabIndex = 16;
            this.cbEnableDebug.Text = "Enable Debug";
            this.cbEnableDebug.UseVisualStyleBackColor = true;
            this.cbEnableDebug.CheckedChanged += new System.EventHandler(this.debugFlag_CheckedChanged);
            // 
            // sendMisread
            // 
            this.sendMisread.Location = new System.Drawing.Point(335, 291);
            this.sendMisread.Name = "sendMisread";
            this.sendMisread.Size = new System.Drawing.Size(83, 23);
            this.sendMisread.TabIndex = 17;
            this.sendMisread.Text = "CBX Error";
            this.sendMisread.UseVisualStyleBackColor = true;
            this.sendMisread.Click += new System.EventHandler(this.sendMisread_Click);
            // 
            // Vault
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 349);
            this.Controls.Add(this.sendMisread);
            this.Controls.Add(this.cbEnableDebug);
            this.Controls.Add(this.btnSaveSettings);
            this.Controls.Add(this.btnClearAllLogs);
            this.Controls.Add(this.tabControlVaultLogs);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.cbValutsPerController);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnRemoveBin);
            this.Controls.Add(this.btnInsertBin);
            this.Controls.Add(this.btnRemoveVault);
            this.Controls.Add(this.btnInsertVault);
            this.Controls.Add(this.groupBox1);
            this.Name = "Vault";
            this.Text = "Vault Emulator";
            this.Load += new System.EventHandler(this.Vault_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControlVaultLogs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox textBoxVaultLog1;
      private System.Windows.Forms.Button btnInsertVault;
      private System.Windows.Forms.Button btnRemoveVault;
      private System.Windows.Forms.Button btnInsertBin;
      private System.Windows.Forms.Button btnRemoveBin;
      private System.Windows.Forms.Button btnExit;
      private System.Windows.Forms.Button btnConnect;
      private System.Windows.Forms.Button btnDisconnect;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.CheckBox cbLogEnable1;
      private System.Windows.Forms.CheckBox cbLogEnable2;
      private System.Windows.Forms.CheckBox cbLogEnable3;
      private System.Windows.Forms.CheckBox cbLogEnable4;
      private System.Windows.Forms.TextBox txtCbxID4;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox txtCbxID3;
      private System.Windows.Forms.TextBox txtCbxID2;
      private System.Windows.Forms.TextBox txtCbxID1;
      private System.Windows.Forms.ComboBox cbVaultID4;
      private System.Windows.Forms.ComboBox cbVaultID3;
      private System.Windows.Forms.ComboBox cbVaultID2;
      private System.Windows.Forms.ComboBox cbVaultID1;
      private System.Windows.Forms.CheckBox cbControllerEnable4;
      private System.Windows.Forms.CheckBox cbControllerEnable3;
      private System.Windows.Forms.CheckBox cbControllerEnable2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.CheckBox cbControllerEnable1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.ComboBox cbComPort4;
      private System.Windows.Forms.ComboBox cbComPort3;
      private System.Windows.Forms.ComboBox cbComPort2;
      private System.Windows.Forms.ComboBox cbComPort1;
      private System.Windows.Forms.RadioButton radioBtnController4;
      private System.Windows.Forms.RadioButton radioBtnController3;
      private System.Windows.Forms.RadioButton radioBtnController2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.RadioButton radioBtnController1;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.ComboBox cbValutsPerController;
      private System.Windows.Forms.TextBox txtCbxID5;
      private System.Windows.Forms.ComboBox cbVaultID5;
      private System.Windows.Forms.CheckBox cbLogEnable5;
      private System.Windows.Forms.CheckBox cbControllerEnable5;
      private System.Windows.Forms.ComboBox cbComPort5;
      private System.Windows.Forms.RadioButton radioButton1;
      private System.Windows.Forms.Button btnClearLog;
      private System.Windows.Forms.TabControl tabControlVaultLogs;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.TabPage tabPage4;
      private System.Windows.Forms.TabPage tabPage5;
      private System.Windows.Forms.TextBox textBoxVaultLog2;
      private System.Windows.Forms.TextBox textBoxVaultLog3;
      private System.Windows.Forms.TextBox textBoxVaultLog4;
      private System.Windows.Forms.TextBox textBoxVaultLog5;
      private System.Windows.Forms.Button btnClearAllLogs;
      private System.Windows.Forms.Button btnSaveSettings;
      private System.Windows.Forms.TextBox txtBinID5;
      private System.Windows.Forms.TextBox txtBinID4;
      private System.Windows.Forms.TextBox txtBinID3;
      private System.Windows.Forms.TextBox txtBinID2;
      private System.Windows.Forms.TextBox txtBinID1;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.CheckBox cbEnableDebug;
      private System.Windows.Forms.Button sendMisread;
   }
}

