﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using INI;

namespace VaultEmulator
{
   public partial class Vault : Form
   {
      string section = "VAULT";
      IniFileName INI;
      Int32 noVaults = 4;

      string[] comPortList = new string[5];
      bool[] comPortLog = new bool[5];
      bool[] vaultState = new bool[20];
      bool[] binState = new bool[20];
      int[] vaultID = new int[5];
      int[] binID = new int[5];
      int[] cbxID = new int[5];
      int[] binInserted = new int[20];
      int[] vaultInserted = new int[20];

      bool debugFlag;
      // Logic al IDs
      int currentController = 0;    // 0-3
      int currentVault = 0;         // 0 to 19
      int msgID = 0;                // Value for either bin or cbs to send

      // UI Components
      int currentCBXId = 0;
      int currentBinId = 0;
      int currentVaultId = 0;
      int currentTransactionID = 0;
      int currentTransactionType = 0;

      // Required in order to modify controls on the main thread
      delegate void SetAddLogCallback(int vltID, string text);

      // List of Ports Currently in USE
      LinkedList<string> usedPorts = new LinkedList<string>();

      // List of messages to be sent
      LinkedList<string> messagesOUT = new LinkedList<string>();

      // List of messages in the log
      LinkedList<string> messagesIN = new LinkedList<string>();

      // Set killThreads to true when the user wants to exit the application
      public bool killThreads = true;
      public Thread tConnect;
      public Thread tCBXThread1;
      public Thread tCBXThread2;
      public Thread tCBXThread3;
      public Thread tCBXThread4;
      public Thread tCBXThread5;
      public Vault()
      {
         InitializeComponent();
      }
      /// <summary>
      /// Fill the com port selector with a list of available ports.
      /// </summary>
      /// 
      private void FillAvailable(ComboBox PortBox)
      {
          string[] PortNames;
          SerialPort port = new SerialPort();
          PortBox.Items.Add("None");
          PortNames = SerialPort.GetPortNames();
          if (PortNames.Count() > 0)
          {
              foreach (string str in PortNames)
              {
                  //if (!PortInUse(str))
                  PortBox.Items.Add(str);
              }
          }


         //for (int i = 1; i <= Constants.MAX_PORTS; i++)
         //{
         //   port.PortName = "COM" + i.ToString();
         //   try
         //   {
         //      port.Open();

         //      if (port.IsOpen)
         //      {
         //          port.Close();
         //          PortBox.Items.Add("COM" + i.ToString());
         //      }
         //   }
         //   catch
         //   {
         //       //MessageBox.Show("Error Opening Port", port.PortName);
         //       //return;
         //   }
         //}
      }

      public class Constants
      {
         public const int BAUD_RATE = 9600;
         public const int DATA_BITS = 8;
         public const Parity PARITY = Parity.None;
         public const StopBits STOP_BITS = StopBits.One;
         public const Handshake HAND_SHAKE = Handshake.None;
         public const int BUFFER_SIZE = 512;
         public const int MAX_PORTS = 40;
         public const int MAX_RETRY = 3;
         public const int WRITE_TIMEOUT = 3000;
         public const byte STX  = 0x02;         // start of text 
         public const byte ETX = 0x03;          // end of text 
         public const byte EOT = 0x04;          // end of transmission 
         public const byte ENQ = 0x05;          // enquiry 
         public const byte ACK = 0x06;          // acknowllege 
         public const byte NAK = 0x15;          // negative acknowllege 
         public const byte SYN = 0x16;          // start of msg. from D.S. 

         // VAULT and BIN states
         public const bool VAULT_IN = true;
         public const bool VAULT_OUT = false;

         public const bool BIN_IN = true;
         public const bool BIN_OUT = false;

         // Transactions
         // 0 = no transaction ready
         public const int TR_VAULT_IN  = 2;        // Vault IN 
         public const int TR_VAULT_OUT = 3;        // vault out
         public const int TR_NO_CBX_OUT = 6;       // Can't Read cashbix ID
         public const int TR_VLT_STATUS = 7;       // Notify vault status

         public const int TR_BIN_IN = 4;        // Bin IN 
         public const int TR_BIN_OUT   = 5;        // Bin out
         
         public const int DEV_VAULT = 0;
         public const int DEV_BIN = 1;

         public const int FAULT_CHANNEL_CABLE = 4044; // FCC or Cable disconnected

         // Vlt protocol error testing, set to true for testing
         public const bool VLT_TEST = false;
         public const int  MIN_BIN_NUMBER = 1;
         public const int  MAX_BIN_NUMBER = 9999;
         public const uint VLT_STATUS_INTERVAL     = 10;
         public const uint VLT_FCC_ERR_INTERVAL    = 30;
         public const uint VLT_BIN_ID_ERR_INTERVAL = 40;

         //#define  uncharToBCD(x)    ( (unsigned char) (((x) / 10) << 4) + ((x) % 10 ) )
      }

      public class ControllerParameters : SerialPort
      {
         public int id;
         public int writeTimeout;
         public int bufferSize;
         public byte[] inputBuffer;
         public byte[] outputBuffer;
         public int inputLen;
         public int outputLen;
         public bool deviceEnabled;
         public bool deviceLog;

         public ControllerParameters()
         {
            inputBuffer = new byte[Constants.BUFFER_SIZE];
            outputBuffer = new byte[Constants.BUFFER_SIZE];
         }
      }
      private void UpdateView()
      {
         if (!killThreads)
         {
            if (!debugFlag)
            {
               btnInsertVault.Enabled = !vaultState[currentVault];
               btnRemoveVault.Enabled = vaultState[currentVault];

               btnInsertBin.Enabled = !binState[currentVault];
               btnRemoveBin.Enabled = binState[currentVault];

               if (currentCBXId == 0)
               {
                  btnInsertVault.Enabled = true;
                  btnRemoveVault.Enabled = false;
               }
            }
            else
            {
               btnInsertVault.Enabled = true;
               btnRemoveVault.Enabled = true;

               btnInsertBin.Enabled = true;
               btnRemoveBin.Enabled = true;

            }
         }
         else
         {
            btnInsertVault.Enabled = false;
            btnRemoveVault.Enabled = false;
            btnInsertBin.Enabled = false;
            btnRemoveBin.Enabled = false;
         }

         cbComPort1.Enabled = killThreads;
         cbComPort2.Enabled = killThreads;
         cbComPort3.Enabled = killThreads;
         cbComPort4.Enabled = killThreads;
         cbComPort5.Enabled = killThreads;

         cbControllerEnable1.Enabled = killThreads;
         cbControllerEnable2.Enabled = killThreads;
         cbControllerEnable3.Enabled = killThreads;
         cbControllerEnable4.Enabled = killThreads;
         cbControllerEnable5.Enabled = killThreads;

         btnDisconnect.Enabled = !killThreads;
         btnConnect.Enabled = killThreads;
         btnExit.Enabled = killThreads;

         // Enable logging for port selected?
         comPortLog[0] = cbLogEnable1.Checked;
         comPortLog[1] = cbLogEnable2.Checked;
         comPortLog[2] = cbLogEnable3.Checked;
         comPortLog[3] = cbLogEnable4.Checked;
         comPortLog[4] = cbLogEnable5.Checked;

         txtBinID1.Enabled = false;
         txtBinID2.Enabled = false;
         txtBinID3.Enabled = false;
         txtBinID4.Enabled = false;
         txtBinID5.Enabled = false;

         txtCbxID1.Enabled = false;
         txtCbxID2.Enabled = false;
         txtCbxID3.Enabled = false;
         txtCbxID4.Enabled = false;
         txtCbxID5.Enabled = false;

         cbVaultID1.Enabled = false;
         cbVaultID2.Enabled = false;
         cbVaultID3.Enabled = false;
         cbVaultID4.Enabled = false;
         cbVaultID5.Enabled = false;

         cbComPort1.Enabled = false;
         cbComPort2.Enabled = false;
         cbComPort3.Enabled = false;
         cbComPort4.Enabled = false;
         cbComPort5.Enabled = false;

         switch (currentController)
         {
            case 0:
               txtBinID1.Enabled = true;
               txtCbxID1.Enabled = true;
               cbVaultID1.Enabled = true;
               cbComPort1.Enabled = true;
               break;
            case 1:
               txtBinID2.Enabled = true;
               txtCbxID2.Enabled = true;
               cbVaultID2.Enabled = true;
               cbComPort2.Enabled = true;
               break;
            case 2:
               txtBinID3.Enabled = true;
               txtCbxID3.Enabled = true;
               cbVaultID3.Enabled = true;
               cbComPort3.Enabled = true;
               break;
            case 3:
               txtBinID4.Enabled = true;
               txtCbxID4.Enabled = true;
               cbVaultID4.Enabled = true;
               cbComPort4.Enabled = true;
               break;
            case 4:
               txtBinID5.Enabled = true;
               txtCbxID5.Enabled = true;
               cbVaultID5.Enabled = true;
               cbComPort5.Enabled = true;
               break;
         }


      }

      private void btnInsRemVault(int controller)
      {
         switch (currentController)
         {
            case 0:
               currentVaultId = Convert.ToInt32(cbVaultID1.Text);
               currentCBXId = Convert.ToInt32(txtCbxID1.Text);
               currentBinId = Convert.ToInt32(txtBinID1.Text);
               break;
            case 1:
               currentVaultId = Convert.ToInt32(cbVaultID2.Text);
               currentCBXId = Convert.ToInt32(txtCbxID2.Text);
               currentBinId = Convert.ToInt32(txtBinID2.Text);
               break;
            case 2:
               currentVaultId = Convert.ToInt32(cbVaultID3.Text);
               currentCBXId = Convert.ToInt32(txtCbxID3.Text);
               currentBinId = Convert.ToInt32(txtBinID3.Text);
               break;
            case 3:
               currentVaultId = Convert.ToInt32(cbVaultID4.Text);
               currentCBXId = Convert.ToInt32(txtCbxID4.Text);
               currentBinId = Convert.ToInt32(txtBinID4.Text);
               break;
            case 4:
               currentVaultId = Convert.ToInt32(cbVaultID5.Text);
               currentCBXId = Convert.ToInt32(txtCbxID5.Text);
               currentBinId = Convert.ToInt32(txtBinID5.Text);
               break;
         }
         //msgID = currentCBXId;
         //currentTransactionID = currentController + 1;
      }
/*
      private void btnInsRemBin(int controller)
      {
         switch (currentController)
         {
            case 0:
               currentVaultId = Convert.ToInt16(cbVaultID1.Text);
               currentCBXId = Convert.ToInt16(txtCbxID1.Text);
               currentBinId = Convert.ToInt16(txtBinID1.Text);
               break;
            case 1:
               currentVaultId = Convert.ToInt16(cbVaultID2.Text);
               currentCBXId = Convert.ToInt16(txtCbxID2.Text);
               currentBinId = Convert.ToInt16(txtBinID2.Text);
               break;
            case 2:
               currentVaultId = Convert.ToInt16(cbVaultID3.Text);
               currentCBXId = Convert.ToInt16(txtCbxID3.Text);
               currentBinId = Convert.ToInt16(txtBinID3.Text);
               break;
            case 3:
               currentVaultId = Convert.ToInt16(cbVaultID4.Text);
               currentCBXId = Convert.ToInt16(txtCbxID4.Text);
               currentBinId = Convert.ToInt16(txtBinID4.Text);
               break;
            case 4:
               currentVaultId = Convert.ToInt16(cbVaultID5.Text);
               currentCBXId = Convert.ToInt16(txtCbxID5.Text);
               currentBinId = Convert.ToInt16(txtBinID5.Text);
               break;
         }
         msgID = currentBinId;
         currentTransactionID = currentController + 1;
      }
*/
      private void btnInsertVault_Click(object sender, EventArgs e)
      {
         btnInsRemVault(currentController);
         vaultState[currentVault] = Constants.VAULT_IN;
         vaultInserted[currentVault] = currentCBXId;

         msgID = currentCBXId;
         // If cashbox ID can't be read, then send Warning (6)
         if (currentCBXId == 0)
            currentTransactionType = Constants.TR_NO_CBX_OUT;
         else
            currentTransactionType = Constants.TR_VAULT_IN;

         currentTransactionID = currentController + 1;

         UpdateView();
      }

      private void btnRemoveVault_Click(object sender, EventArgs e)
      {
         btnInsRemVault(currentController);
         vaultState[currentVault] = Constants.VAULT_OUT;
         currentCBXId = vaultInserted[currentVault];
         vaultInserted[currentVault] = 0;
         msgID = currentCBXId;
         // If cashbox ID cabn't be read, then send Warning (6)
         if (currentCBXId==0)
            currentTransactionType = Constants.TR_NO_CBX_OUT;         
         else
            currentTransactionType = Constants.TR_VAULT_OUT;         
         currentTransactionID = currentController + 1;

         UpdateView();
      }

      private void btnInsertBin_Click(object sender, EventArgs e)
      {
         btnInsRemVault(currentController);
         //btnInsRemBin(currentController);
         binState[currentVault] = Constants.BIN_IN;
         binInserted[currentVault] = currentBinId;
         currentTransactionType = Constants.TR_BIN_IN;

         msgID = currentBinId;
         currentTransactionID = currentController + 1;

         UpdateView();
      }

      private void btnRemoveBin_Click(object sender, EventArgs e)
      {
         btnInsRemVault(currentController);
         //btnInsRemBin(currentController);
         binState[currentVault] = Constants.BIN_OUT;
         currentBinId = binInserted[currentVault];
         binInserted[currentVault] = 0;
         currentTransactionType = Constants.TR_BIN_OUT;

         msgID = currentBinId;
         currentTransactionID = currentController + 1;

         UpdateView();
      }

      private void btnConnect_Click(object sender, EventArgs e)
      {
         killThreads = false;
         UpdateView();
         Connecthread();
      }

      private void btnDisconnect_Click(object sender, EventArgs e)
      {
         killThreads = true;
         UpdateView();
      }

      private void btnExit_Click(object sender, EventArgs e)
      {
         killThreads = true;
         Close();
      }

      private void cbComPort1_KeyDown(object sender, KeyEventArgs e)
      {

      }

      private void cbComPort1_KeyPress(object sender, KeyPressEventArgs e)
      {
      }

      //private bool PortInUse(string strport)
      //{
      //    if (bypassport == 1)
      //    {
      //        foreach (string strcbport in cbComPort1)
      //        {
      //            if (String.Compare(strobject, strport) == 0)
      //                return true;
      //            else
      //                return false;
      //        }
      //    }
      //}

      // Update COM Port DropDown controls with existing COM ports
      private void UpdateComPorts()
      {
         cbComPort1.Items.Clear();
         cbComPort2.Items.Clear();
         cbComPort3.Items.Clear();
         cbComPort4.Items.Clear();
         cbComPort5.Items.Clear();
         FillAvailable(cbComPort1);
         FillAvailable(cbComPort2);
         FillAvailable(cbComPort3);
         FillAvailable(cbComPort4);
         FillAvailable(cbComPort5);
      }

      // Update DropDown Controller controls with existing vault and controller IDs
      private void UpdateVaultID()
      {
         // Get # vaults per Cashbox Controller
         noVaults = Convert.ToInt32(cbValutsPerController.Text);

         // Clear Vault data.
         cbVaultID1.Items.Clear();
         cbVaultID2.Items.Clear();
         cbVaultID3.Items.Clear();
         cbVaultID4.Items.Clear();
         cbVaultID5.Items.Clear();

         for (int i = 0; i < noVaults; i++)
         {
            cbVaultID1.Items.Add((i).ToString());
            cbVaultID2.Items.Add((i).ToString());
            cbVaultID3.Items.Add((i).ToString());
            cbVaultID4.Items.Add((i).ToString());
            cbVaultID5.Items.Add((i).ToString());
         }
         UpdateComPorts();

      }
      // Update controls with number of vaults per controller
      private void Vault_Load(object sender, EventArgs e)
      {
         string path = Environment.GetEnvironmentVariable("GFI") + @"\cnf\VaultEmulator.ini";
         INI = new INI.IniFileName(path);

         // Initaially set to 4 vaults
         cbValutsPerController.Text = "4";

         // Set COM port drop downs
         UpdateComPorts();

         // Update vault IDs
         UpdateVaultID();
         LoadDefaults();

         // Update View
         UpdateView();
         for (int i=0;i<20;i++)
         {
            vaultState[i]=Constants.VAULT_OUT;
            binState[i]=Constants.BIN_OUT;
            binInserted[i] = 0;
            vaultInserted[i] = 0;
         }

         for (int i = 0; i < 5; i++)
         {
            vaultID[i] = 0;
            cbxID[i] = 0;
            binID[i] = 0;
         }

         // Clear current transaction
         currentTransactionID=0;
         currentTransactionType=0;
         tabControlVaultLogs.TabPages[0].Text = "CBX-1";
         tabControlVaultLogs.TabPages[1].Text = "CBX-2";
         tabControlVaultLogs.TabPages[2].Text = "CBX-3";
         tabControlVaultLogs.TabPages[3].Text = "CBX-4";
         tabControlVaultLogs.TabPages[4].Text = "CBX-5";

         cbVaultID1.Text = "0";
         cbVaultID2.Text = "0";
         cbVaultID3.Text = "0";
         cbVaultID4.Text = "0";
         cbVaultID5.Text = "0";

      }

      private void cbValutsPerController_SelectedValueChanged(object sender, EventArgs e)
      {
         UpdateVaultID();
      }

      // Message coming from the Vault application on the PC
      public void ReadMessage(ControllerParameters cbxDevice)
      {
         int pos = 0;
         string txt;
         cbxDevice.Read(cbxDevice.inputBuffer, pos, 1);
         switch (cbxDevice.inputBuffer[pos])
         {
            case Constants.ACK:
               pos++;
               break;
            case Constants.STX:
               pos++;
               break;
            case Constants.ENQ:
               pos++;
               break;
            case Constants.EOT:
               pos++;
               break;
            case Constants.SYN:  // Doc says tyhis should be an ENQ but I don't think so
               pos++;
               cbxDevice.Read(cbxDevice.inputBuffer, pos, 5);
               pos += 5;
               break;
            default:
               break;
         }

         if (cbxDevice.deviceLog)
         {
            txt = "";
            for (int i=0;i<pos;i++)
               txt += string.Format("{0:X2} ", cbxDevice.inputBuffer[i]);
            txt += "\n";
            AddLogToListWithTimeStamp(cbxDevice.id, "<--", txt, "");
         }
      }
      private void AddLogToList(int id, string str)
      {
         // InvokeRequired required compares the thread ID of the
         // calling thread to the thread ID of the creating thread.
         // If these threads are different, it returns true.
         switch(id)
         {
            case 0:
               if (this.textBoxVaultLog1.InvokeRequired)
               {
                  SetAddLogCallback d = new SetAddLogCallback(AddLogToList);
                  this.Invoke(d, new object[] {id,  str });
               }
               else
               {
                  textBoxVaultLog1.Text += str + System.Environment.NewLine;
               }
               break;
            case 1:
               if (this.textBoxVaultLog2.InvokeRequired)
               {
                  SetAddLogCallback d = new SetAddLogCallback(AddLogToList);
                  this.Invoke(d, new object[] {id, str });
               }
               else
               {
                  textBoxVaultLog2.Text += str + System.Environment.NewLine;
               }
               break;
            case 2:
               if (this.textBoxVaultLog3.InvokeRequired)
               {
                  SetAddLogCallback d = new SetAddLogCallback(AddLogToList);
                  this.Invoke(d, new object[] {id,  str });
               }
               else
               {
                  textBoxVaultLog3.Text += str + System.Environment.NewLine;
               }
               break;
            case 3:
               if (this.textBoxVaultLog4.InvokeRequired)
               {
                  SetAddLogCallback d = new SetAddLogCallback(AddLogToList);
                  this.Invoke(d, new object[] {id,  str });
               }
               else
               {
                  textBoxVaultLog4.Text += str + System.Environment.NewLine;
               }
               break;
            case 4:
               if (this.textBoxVaultLog5.InvokeRequired)
               {
                  SetAddLogCallback d = new SetAddLogCallback(AddLogToList);
                  this.Invoke(d, new object[] {id,  str });
               }
               else
               {
                  textBoxVaultLog5.Text += str + System.Environment.NewLine;
               }
               break;
         }
      }

      void AddLogToListWithTimeStamp(int vltID, string hdr, string str, string trailer)
      {
         string logEntry = string.Format("VLT{0}", vltID+1) + hdr + DateTime.Now.ToString() + ": " + str;
         AddLogToList(vltID, logEntry + trailer);
      }

      // Send Message from Vault to Vault application on PC
      // Note: id = vault ID
      //       Type is:  Messge Type{H]/Channel[L]
      public void SendMessage(ControllerParameters cbxDevice)
      {
         int chksum;
         string txt;

         int fbx1 = msgID/100;              // Get high order value
         int fbx2 = msgID - (fbx1 * 100);   // Get low order value
         //int fbx1 = currentCBXId / 100;              // Get high order value
         //int fbx2 = currentCBXId - (fbx1 * 100);   // Get low order value

         currentTransactionID = 0;
         DateTime time = DateTime.Now;
         int hh = time.Hour;
         int mm = time.Minute;
         int ss = time.Second;
         cbxDevice.outputBuffer[0] = Constants.STX;
         cbxDevice.outputBuffer[1] = (byte)(currentVaultId << 4 | currentTransactionType);
         cbxDevice.outputBuffer[2] = (byte)(((hh / 10) << 4) + (byte)hh % 10);
         cbxDevice.outputBuffer[3] = (byte)(((mm / 10) << 4) + (byte)mm % 10);
         cbxDevice.outputBuffer[4] = (byte)(((ss / 10) << 4) + (byte)ss % 10);
         cbxDevice.outputBuffer[5] = (byte)(((fbx1 / 10) << 4) + (byte)fbx1 % 10);
         cbxDevice.outputBuffer[6] = (byte)(((fbx2 / 10) << 4) + (byte)fbx2 % 10);
         cbxDevice.outputBuffer[7] = Constants.ETX;

         // Compute checksum
         chksum = cbxDevice.outputBuffer[1] +
                  cbxDevice.outputBuffer[2] +
                  cbxDevice.outputBuffer[3] +
                  cbxDevice.outputBuffer[4] +
                  cbxDevice.outputBuffer[5] +
                  cbxDevice.outputBuffer[6];

         cbxDevice.outputBuffer[8] = (byte)chksum;

         // Write data
         cbxDevice.Write(cbxDevice.outputBuffer, 0, 9);

         // Log message
         if (cbxDevice.deviceLog)
         {
            txt = "";
            for (int i = 0; i < 9; i++)
               txt += string.Format("{0:X2} ", cbxDevice.outputBuffer[i]);
            AddLogToListWithTimeStamp(cbxDevice.id, "-->", txt, "");
         }
         ReadMessage(cbxDevice);    // Wait for ACK
         SendEOTMessage(cbxDevice); // Send EOT
      }

      // Send Message from Vault to Vault application on PC      
      public void SendEOTMessage(ControllerParameters cbxDevice)
      {
         string txt;
         cbxDevice.outputBuffer[0] = Constants.EOT;
         cbxDevice.Write(cbxDevice.outputBuffer, 0, 1);

         // Log message
         if (cbxDevice.deviceLog)
         {
            txt = "";
            txt += string.Format("{0:X2}", cbxDevice.outputBuffer[0]);
            AddLogToListWithTimeStamp(cbxDevice.id, "-->", txt, "");
         }
      }

      // Send Vault Status Message from Vault to Vault application on PC
      // Note: id = bin ID or 0 (bin not present) or 0x0FCC Fail Channel Cable
      //       Type is:  Messge Type{H]/Channel[L]
      //
      public void SendVaultStatusMessage(ControllerParameters cbxDevice, uint count)
      {
          // Save previous values
          int saveTransactionType = currentTransactionType;
          int saveMsgID = msgID;

          currentTransactionType = Constants.TR_VLT_STATUS;
          msgID = currentBinId;
          
          //
          // Bin out state occurs on button press         
          if (binState[currentVault] == Constants.BIN_OUT)  
          {
             msgID = 0;
             //Thread.Sleep(1000);
          }
        
          // Test vault unexpected states
          // Class variables save and retore protect against testing modifications
          if (binState[currentVault] == Constants.VLT_TEST)
          {
             TestVaultStatusErrorMessage(cbxDevice, count);
          }

          SendMessage(cbxDevice);

          // Restore previous values
          currentTransactionType = saveTransactionType;
          msgID = saveMsgID;

          ReadMessage(cbxDevice);    // Wait for ACK
          SendEOTMessage(cbxDevice); // Send EOT
      }

      // Test Vault Status Error Message from Vault to Vault application on PC
      // Note: id = bin ID or 0 (bin not present) or 0x0FCC Fail Channel Cable
      //       Type is:  Messge Type{H]/Channel[L]
      //
      // Testing: Activate VLT_TEST to periodically test cases:
      //                       a) unexpected binid
      //                       b) unexpected bin out, enter bin id=3999 in Form1
      //                       c) channel cable disconnected
      //          For test case binid out of range, in VaultEmulator dialog
      //          window manually enter binid number out of range.
      //          
      public void TestVaultStatusErrorMessage(ControllerParameters cbxDevice, uint count)
      {

          //
          // Test Vlt status error leggs
          //
          // Test code start

          // Send bad bin number whether bin in or out
          if (count % Constants.VLT_BIN_ID_ERR_INTERVAL == 0)
          {
              if (msgID < Constants.MAX_BIN_NUMBER)
              {
                  msgID++;        // Signal unexpected Bin ID  
              }
              else if (binState[currentVault] == Constants.BIN_IN)
              {
                  msgID = 0;      // Signal unexpected Bin out ID when bin is present, via bin id=3999 in Form1
              }
              else
              {
                  msgID = 22;    // Signal unexpected Bin ID when bin is not present, via bin id=3999 in Form1
              }
          }

          // Test fail channel cable 0x0FCC
          if (count % Constants.VLT_FCC_ERR_INTERVAL == 0)
          {
              msgID = Constants.FAULT_CHANNEL_CABLE; // Signal channel cable disconnect
          }

          //
          // Test code end
          //
      }

      public void Connecthread()
      {
         // Enable appropriate ports
         comPortList[0] = cbComPort1.Text;
         comPortList[1] = cbComPort2.Text;
         comPortList[2] = cbComPort3.Text;
         comPortList[3] = cbComPort4.Text;
         comPortList[4] = cbComPort5.Text;

         // Enable logging for port selected?
         comPortLog[0] = cbLogEnable1.Checked;
         comPortLog[1] = cbLogEnable2.Checked;
         comPortLog[2] = cbLogEnable3.Checked;
         comPortLog[3] = cbLogEnable4.Checked;
         comPortLog[4] = cbLogEnable5.Checked;

         if (cbControllerEnable1.Checked)
         {
            tCBXThread1 = new Thread(ProcessCBXThread);
            tCBXThread1.Start(0);
         }

         if (cbControllerEnable2.Checked)
         {
            tCBXThread2 = new Thread(ProcessCBXThread);
            tCBXThread2.Start(1);
         }
         if (cbControllerEnable3.Checked)
         {
            tCBXThread3 = new Thread(ProcessCBXThread);
            tCBXThread3.Start(2);
         }
         if (cbControllerEnable4.Checked)
         {
            tCBXThread4 = new Thread(ProcessCBXThread);
            tCBXThread4.Start(3);
         }
         if (cbControllerEnable5.Checked)
         {
            tCBXThread5 = new Thread(ProcessCBXThread);
            tCBXThread5.Start(4);
         }
      }


      // This thread will process a single Controller board
      private void ProcessCBXThread(object ID)
      {
         int id = (int)ID;
         uint count = 0;

         // Create a controller parameter object
         ControllerParameters cbxDevice = new ControllerParameters();

         // Set COM Port parameters
         cbxDevice.id = id;
         cbxDevice.BaudRate=Constants.BAUD_RATE;
         cbxDevice.DataBits=Constants.DATA_BITS;
         cbxDevice.bufferSize=Constants.BUFFER_SIZE;
         cbxDevice.Handshake=Constants.HAND_SHAKE;
         cbxDevice.Parity=Constants.PARITY;
         cbxDevice.deviceEnabled = true;
         cbxDevice.deviceLog = true;
         cbxDevice.writeTimeout = Constants.WRITE_TIMEOUT;

         cbxDevice.PortName = comPortList[id];
         //cbxDevice.ReadTimeout = 10000;
         cbxDevice.ReadTimeout = 3000;

         try
         {
             // Open device
             cbxDevice.Open();
         }
         catch
         {
               if (cbxDevice.deviceLog)
               {
                  MessageBox.Show("Unable to open Port:"+comPortList[id].ToString()+ ". Please Disconnect\nand Fix Port Number in gfi.ini configuration file.", "COM Port Error");
                  cbxDevice.deviceEnabled =false;
                  killThreads=true;
                  return;
               }
         }

         // Wait for thread termination
         while (!killThreads)
         {
            // Send time to controller
            //SendTime(cbxDevice); // This is only done by the Vault Application
            try
            {
               cbxDevice.deviceLog = comPortLog[id];
               ReadMessage(cbxDevice);

               // See if a transaction is required on this ID
               // Note that Current Transaction ID's start at 1 and not 0
               //
               if (currentTransactionID == (cbxDevice.id + 1))
               {
                   SendMessage(cbxDevice);    // Send data
               }
               else
               {   
                   count++;                  // assume endless loop

                   // Notify current Vault Channel Status
                   if ((count % Constants.VLT_STATUS_INTERVAL) == 0)
                   {
                       //SendVaultStatusMessage(cbxDevice, count);
                   }
                   else
                   {
                       // Response to Polling with Time
                       SendEOTMessage(cbxDevice);
                   }
               }
            }
            catch (TimeoutException ex)
            {
               if (cbxDevice.deviceLog)
               {
                  AddLogToListWithTimeStamp(cbxDevice.id, "ERROR:", ex.ToString(), "");
               }
            }
            Thread.Sleep(1000);
         }

         cbxDevice.Close();
         // Set device status and open device
      }

      private void radioBtnController1_CheckedChanged(object sender, EventArgs e)
      {
         currentController = 0;
         currentVault = Convert.ToInt32(cbVaultID1.Text);
         tabControlVaultLogs.SelectTab(currentController);
         UpdateView();
      }

      private void radioBtnController2_CheckedChanged(object sender, EventArgs e)
      {
         currentController = 1;
         currentVault = (noVaults * 1) + Convert.ToInt32(cbVaultID2.Text);
         tabControlVaultLogs.SelectTab(currentController); 
         UpdateView();
      }

      private void radioBtnController3_CheckedChanged(object sender, EventArgs e)
      {
         currentController = 2;
         currentVault = (noVaults * 2) + Convert.ToInt32(cbVaultID3.Text);
         tabControlVaultLogs.SelectTab(currentController);
         UpdateView();
      }

      private void radioBtnController4_CheckedChanged(object sender, EventArgs e)
      {
         currentController = 3;
         currentVault = (noVaults * 3) + Convert.ToInt32(cbVaultID4.Text);
         tabControlVaultLogs.SelectTab(currentController);
         UpdateView();
      }

      private void radioButton1_CheckedChanged(object sender, EventArgs e)
      {
         currentController = 4;
         currentVault = (noVaults * 4) + Convert.ToInt32(cbVaultID5.Text);
         UpdateView();
      }

      private void btnClearLog_Click(object sender, EventArgs e)
      {
         switch (tabControlVaultLogs.SelectedIndex)
         {
            case 0:
               textBoxVaultLog1.Clear();
               break;
            case 1:
               textBoxVaultLog2.Clear();
               break;
            case 2:
               textBoxVaultLog3.Clear();
               break;
            case 3:
               textBoxVaultLog4.Clear();
               break;
            case 4:
               textBoxVaultLog5.Clear();
               break;
         }
      }

      private void btnClearAllLogs_Click(object sender, EventArgs e)
      {
         textBoxVaultLog1.Clear();
         textBoxVaultLog2.Clear();
         textBoxVaultLog3.Clear();
         textBoxVaultLog4.Clear();
         textBoxVaultLog5.Clear();
      }

      public void LoadDefaults()
      {
         string cbxPort, logEnable, vltId, cbxId, binid;

         // Get Number of Vaults Per COntroller
         cbValutsPerController.Text = INI.GetEntryValue(section, "CBXCOUNT").ToString();
         string strVal;
         for (int i=0;i<5;i++)
         {
            cbxPort = "CBXPORT"+(i+1).ToString();           
            vltId = "VLTID"+(i+1).ToString();           
            cbxId = "CBXID"+(i+1).ToString();
            binid = "BINID" + (i + 1).ToString();
            logEnable = "LOGENABLE" + (i + 1).ToString();           
            vaultID[i]=0;

            comPortList[i] = INI.GetEntryValue(section, cbxPort).ToString();
            if (comPortList[i].Length < 4 || comPortList[i].Length > 6)
               comPortList[i] = "None";
            // Get vault ID
            strVal = INI.GetEntryValue(section, vltId).ToString();
            if (strVal.Length > 0)
                vaultID[i] = Convert.ToInt32(strVal);
            // Get Cashbox ID
            strVal = INI.GetEntryValue(section, cbxId).ToString();
            if (strVal.Length > 0)
                cbxID[i] = Convert.ToInt32(strVal.ToString());
            // Get Bin ID
            strVal = INI.GetEntryValue(section, binid).ToString();
            if (strVal.Length > 0)
                binID[i] = Convert.ToInt32(strVal.ToString());
            // Enable Logging
            strVal = INI.GetEntryValue(section, logEnable).ToString();
            if (strVal.Length > 0)
            {
                if (Convert.ToInt32(strVal.ToString()) > 0)
                  comPortLog[i] = true;
               else
                  comPortLog[i] = false;
            }
            else
                  comPortLog[i] = false;
         }

         cbLogEnable1.Checked = comPortLog[0];
         cbLogEnable2.Checked = comPortLog[1];
         cbLogEnable3.Checked = comPortLog[2];
         cbLogEnable4.Checked = comPortLog[3];
         cbLogEnable5.Checked = comPortLog[4];

         strVal = INI.GetEntryValue(section, "CBXENABLE1").ToString();
         if (strVal.Length > 0)
             cbControllerEnable1.Checked = Convert.ToBoolean(Convert.ToInt32(strVal));

         strVal = INI.GetEntryValue(section, "CBXENABLE2").ToString();
         if (strVal.Length > 0)
             cbControllerEnable2.Checked = Convert.ToBoolean(Convert.ToInt32(strVal));

         strVal = INI.GetEntryValue(section, "CBXENABLE3").ToString();
         if (strVal.Length > 0)
             cbControllerEnable3.Checked = Convert.ToBoolean(Convert.ToInt32(strVal));

         strVal = INI.GetEntryValue(section, "CBXENABLE4").ToString();
         if (strVal.Length > 0)
             cbControllerEnable4.Checked = Convert.ToBoolean(Convert.ToInt32(strVal));

         strVal = INI.GetEntryValue(section, "CBXENABLE5").ToString();
         if (strVal.Length > 0)
             cbControllerEnable5.Checked = Convert.ToBoolean(Convert.ToInt32(strVal));

         cbComPort1.Text=comPortList[0];
         cbComPort2.Text=comPortList[1];
         cbComPort3.Text=comPortList[2];
         cbComPort4.Text=comPortList[3];
         cbComPort5.Text=comPortList[4];


         cbVaultID1.Text=vaultID[0].ToString();
         cbVaultID2.Text=vaultID[1].ToString();
         cbVaultID3.Text=vaultID[2].ToString();
         cbVaultID4.Text=vaultID[3].ToString();
         cbVaultID5.Text=vaultID[4].ToString();

         txtCbxID1.Text = cbxID[0].ToString();
         txtCbxID2.Text = cbxID[1].ToString();
         txtCbxID3.Text = cbxID[2].ToString();
         txtCbxID4.Text = cbxID[3].ToString();
         txtCbxID5.Text = cbxID[4].ToString();

         txtBinID1.Text = binID[0].ToString();
         txtBinID2.Text = binID[1].ToString();
         txtBinID3.Text = binID[2].ToString();
         txtBinID4.Text = binID[3].ToString();
         txtBinID5.Text = binID[4].ToString();
      }

      public void UpdateDefaults()
      {
         INI.IniWriteValue(section, "CBXCOUNT", cbValutsPerController.Text);

         INI.IniWriteValue(section, "CBXENABLE1", cbControllerEnable1.Checked ? "1" : "0");
         INI.IniWriteValue(section, "CBXENABLE2", cbControllerEnable2.Checked ? "1" : "0");
         INI.IniWriteValue(section, "CBXENABLE3", cbControllerEnable3.Checked ? "1" : "0");
         INI.IniWriteValue(section, "CBXENABLE4", cbControllerEnable4.Checked ? "1" : "0");
         INI.IniWriteValue(section, "CBXENABLE5", cbControllerEnable5.Checked ? "1" : "0");

         INI.IniWriteValue(section, "LOGENABLE1", cbLogEnable1.Checked ? "1" : "0");
         INI.IniWriteValue(section, "LOGENABLE2", cbLogEnable2.Checked ? "1" : "0");
         INI.IniWriteValue(section, "LOGENABLE3", cbLogEnable3.Checked ? "1" : "0");
         INI.IniWriteValue(section, "LOGENABLE4", cbLogEnable4.Checked ? "1" : "0");
         INI.IniWriteValue(section, "LOGENABLE5", cbLogEnable5.Checked ? "1" : "0");

         INI.IniWriteValue(section, "CBXPORT1", cbComPort1.Text);
         INI.IniWriteValue(section, "CBXPORT2", cbComPort2.Text);
         INI.IniWriteValue(section, "CBXPORT3", cbComPort3.Text);
         INI.IniWriteValue(section, "CBXPORT4", cbComPort4.Text);
         INI.IniWriteValue(section, "CBXPORT5", cbComPort5.Text);

         INI.IniWriteValue(section, "VLTID1", cbVaultID1.Text);
         INI.IniWriteValue(section, "VLTID2", cbVaultID2.Text);
         INI.IniWriteValue(section, "VLTID3", cbVaultID3.Text);
         INI.IniWriteValue(section, "VLTID4", cbVaultID4.Text);
         INI.IniWriteValue(section, "VLTID5", cbVaultID5.Text);

         INI.IniWriteValue(section, "CBXID1", txtCbxID1.Text);
         INI.IniWriteValue(section, "CBXID2", txtCbxID2.Text);
         INI.IniWriteValue(section, "CBXID3", txtCbxID3.Text);
         INI.IniWriteValue(section, "CBXID4", txtCbxID4.Text);
         INI.IniWriteValue(section, "CBXID5", txtCbxID5.Text);

         INI.IniWriteValue(section, "BINID1", txtBinID1.Text);
         INI.IniWriteValue(section, "BINID2", txtBinID2.Text);
         INI.IniWriteValue(section, "BINID3", txtBinID3.Text);
         INI.IniWriteValue(section, "BINID4", txtBinID4.Text);
         INI.IniWriteValue(section, "BINID5", txtBinID5.Text);
      
      }

      private void btnSaveSettings_Click(object sender, EventArgs e)
      {
         UpdateDefaults();
      }

      private void cbValutsPerController_SelectedIndexChanged(object sender, EventArgs e)
      {
         UpdateVaultID();
      }

      private void cbVaultID1_SelectedIndexChanged(object sender, EventArgs e)
      {
          currentVault = Convert.ToInt32(cbVaultID1.Text);
         if (vaultInserted[currentVault] > 0)
            txtCbxID1.Text = vaultInserted[currentVault].ToString();
         if (binInserted[currentVault] > 0)
            txtBinID1.Text = binInserted[currentVault].ToString();
         UpdateView();
      }

      private void cbVaultID2_SelectedIndexChanged(object sender, EventArgs e)
      {
          currentVault = (noVaults * 1) + Convert.ToInt32(cbVaultID2.Text);
         if (vaultInserted[currentVault] > 0)
            txtCbxID2.Text = vaultInserted[currentVault].ToString();
         if (binInserted[currentVault] > 0)
            txtBinID2.Text = binInserted[currentVault].ToString();
         UpdateView();
      }

      private void cbVaultID3_SelectedIndexChanged(object sender, EventArgs e)
      {
          currentVault = (noVaults * 2) + Convert.ToInt32(cbVaultID3.Text);
         if (vaultInserted[currentVault] > 0)
            txtCbxID3.Text = vaultInserted[currentVault].ToString();
         if (binInserted[currentVault] > 0)
            txtBinID3.Text = binInserted[currentVault].ToString();
         UpdateView();
      }

      private void cbVaultID4_SelectedIndexChanged(object sender, EventArgs e)
      {
          currentVault = (noVaults * 3) + Convert.ToInt32(cbVaultID4.Text);
         if (vaultInserted[currentVault] > 0)
            txtCbxID4.Text = vaultInserted[currentVault].ToString();
         if (binInserted[currentVault] > 0)
            txtBinID4.Text = binInserted[currentVault].ToString();
         UpdateView();
      }

      private void cbVaultID5_SelectedIndexChanged(object sender, EventArgs e)
      {
          currentVault = (noVaults * 4) + Convert.ToInt32(cbVaultID5.Text);
         if (vaultInserted[currentVault] > 0)
            txtCbxID5.Text = vaultInserted[currentVault].ToString();
         if (binInserted[currentVault] > 0)
            txtBinID5.Text = binInserted[currentVault].ToString();
         UpdateView();
      }

      private void cbLogEnable1_CheckedChanged(object sender, EventArgs e)
      {
         comPortLog[0] = cbLogEnable1.Checked;
      }
      private void cbLogEnable2_CheckedChanged(object sender, EventArgs e)
      {
         comPortLog[1] = cbLogEnable2.Checked;
      }

      private void cbLogEnable3_CheckedChanged(object sender, EventArgs e)
      {
         comPortLog[2] = cbLogEnable3.Checked;
      }

      private void cbLogEnable4_CheckedChanged(object sender, EventArgs e)
      {
         comPortLog[3] = cbLogEnable4.Checked;
      }

      private void cbLogEnable5_CheckedChanged(object sender, EventArgs e)
      {
         comPortLog[4] = cbLogEnable5.Checked;
      }

      private void debugFlag_CheckedChanged(object sender, EventArgs e)
      {
         debugFlag = this.cbEnableDebug.Checked;
         UpdateView();
      }

      private void sendMisread_Click(object sender, EventArgs e)
      {
          int saveCurrentCBXId = currentCBXId;

          btnInsRemVault(currentController);
          vaultState[currentVault] = Constants.VAULT_IN;
          vaultInserted[currentVault] = 0;
          msgID = currentCBXId;
          // If cashbox ID can't be read, then send Warning (6)
          currentTransactionType = Constants.TR_VAULT_IN;

          currentTransactionID = currentController + 1;
          // restore current cashbox ID
          currentCBXId = saveCurrentCBXId;
          vaultInserted[currentVault] = currentCBXId;
          UpdateView();
      }
   }
}
